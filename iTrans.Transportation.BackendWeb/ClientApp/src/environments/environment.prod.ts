// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  //baseWebApiUrl: 'https://localhost:44383/api',
  baseWebApiUrl: 'https://itrans-test-api.humantix.cloud/api',
  baseWebContentUrl: 'https://itrans-test-content.humantix.cloud',
  baseAuth: '',
  version: '0.0.1',
  baseURL: '',
  serverSocket: {
    baseURL: 'https://itrans-test-api.humantix.cloud/micro/chat/',
    apiURL: '',
    fileUpload: 'api/upload/',
    fileGroup: 'api/group/',
    defualtImagePath: '../web/assets/jsons/master-image-default.json',
    fileNote: {
      fileGet: 'api/Note/Get',
      fileCreate: 'api/Note/Create',
      fileEdit: 'api/Note/Edit',
      fileDelete: 'api/Note/Delete'
    }
  },

};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
