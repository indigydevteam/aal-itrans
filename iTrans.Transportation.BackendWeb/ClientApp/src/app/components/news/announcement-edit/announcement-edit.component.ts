import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AngularEditorConfig } from '@kolkov/angular-editor';
import { NgbDatepicker } from '@ng-bootstrap/ng-bootstrap';
import * as moment from 'moment';
import { DatepickerOptions } from 'ng2-datepicker';
import { NewsService } from 'src/app/services/news/news.service';

@Component({
  selector: 'app-announcement-edit',
  templateUrl: './announcement-edit.component.html',
  styleUrls: ['./announcement-edit.component.scss']
})
export class AnnouncementEditComponent implements OnInit {
  @ViewChild('dp') dp: NgbDatepicker;
  options: DatepickerOptions = {
    minDate: new Date(moment().add(-1, 'days').format("")), // minimum available and selectable year
    // scrollBarColor: '#ffffff',
    format: 'dd/MM/yyyy', // date format to display in inputม
    position: 'bottom',
    inputClass: 'form-control datepicker-blue',
  };
  editorConfig: AngularEditorConfig = {
    editable: true,
    spellcheck: true,
    height: '200px',

    width: 'auto',
    minWidth: '0',
    translate: 'yes',
    enableToolbar: true,
    showToolbar: true,
    placeholder: 'Enter text here...',
    defaultParagraphSeparator: '',
    defaultFontName: '',
    defaultFontSize: '',
    fonts: [
      { class: 'arial', name: 'Arial' },
      { class: 'times-new-roman', name: 'Times New Roman' },
      { class: 'calibri', name: 'Calibri' },
      { class: 'comic-sans-ms', name: 'Comic Sans MS' }
    ],
    customClasses: [
      {
        name: 'quote',
        class: 'quote',
      },
      {
        name: 'redText',
        class: 'redText'
      },
      {
        name: 'titleText',
        class: 'titleText',
        tag: 'h1',
      },
    ],
    uploadWithCredentials: false,
    sanitize: true,
    toolbarPosition: 'top',
    toolbarHiddenButtons: [
      ['bold', 'italic'],
      [
        'fontSize',
        'textColor',
        'backgroundColor',
        'customClasses',
        'link',
        'unlink',
        'insertImage',
        'insertVideo',
        'insertHorizontalRule',
        'removeFormat',
        'toggleEditorMode'
      ]
    ]
  };
  form = {
    id: '0',
    title: "Test create news",
    detail: "Test create news",
    start_date: new Date(),
    end_date: new Date(moment().add(31, 'days').format("")),
    start_time: '08:00:00',
    end_time: '23:59:00',
    image: null,
    image_url: null,
    url: null,
    url_video: null,
    is_active: true,
    is_noti: false,
    is_all: true,
    is_hl: true,
    is_guest: false,
    file: null,
    video: null,
    target_customer: null,
    target_driver: null,
    hl_start_date: new Date(),
    hl_end_date: new Date(moment().add(31, 'days').format("")),
    hl_start_time: '08:00:00',
    hl_end_time: '23:59:00',
    video_url: null,
    file_url: '',
    video_delete: false,
    file_delete: false,
    image_delete: false,
    file_name: '',
  }
  body = {
    is_target: true
  }
  isFormSubmit = false;
  image = null;
  isAll = true;
  constructor(
    public fb: FormBuilder,
    private api: NewsService,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) { }
  ngOnInit(): void {
    setTimeout(() => {

    }, 0);
    setTimeout(() => {

    }, 0);
    setTimeout(() => {

    }, 0);
    ;
    this.fetchData();
  }
  async fetchData(): Promise<any> {
    const body = {
      id: this.activatedRoute.snapshot.paramMap.get('id'),
    }
    const resp = await this.api.getNewsById(body);

    if (resp) {
      this.image = resp.data.imageFileUrl;
      this.form = {
        id: resp.data.id,
        title: resp.data.title,
        detail: resp.data.detail,
        start_date: new Date(moment(resp.data.startDate).format("")),
        end_date: new Date(moment(resp.data.endDate).format("")),
        start_time: (moment(resp.data.startDate).format("HH:mm")),
        end_time: (moment(resp.data.endDate).format("HH:mm")),
        image: null,
        image_url: resp.data.imageFileUrl,
        url: resp.data.url,
        url_video: resp.data.videoUrl,
        is_active: resp.data.isActive,
        is_noti: resp.data.isNotification,
        is_all: resp.data.isAll,
        is_hl: resp.data.isHighlight,
        is_guest: resp.data.isGuest,
        file: null,
        file_url: resp.data.fileUrl,
        video: null,
        video_url: resp.data.videoEmbedUrl,
        target_customer: resp.data.targetCustomerType,
        target_driver: resp.data.targetDriverType,
        hl_start_date: new Date(moment(this.checkinValidDate(resp.data.hlStartDate)).format("")),
        hl_end_date: new Date(moment(this.checkinValidDate(resp.data.hlEndDate)).format("")),
        hl_start_time: (moment(resp.data.hlStartDate).format("HH:mm")),
        hl_end_time: (moment(resp.data.hlEndDate).format("HH:mm")),
        video_delete: false,
        file_delete: false,
        image_delete: false,
        file_name: resp.data.fileName
      }
    }

  }

  checkinValidDate(date) {
    if ((new Date(date).getTime() < 1)) {
      return (new Date());
    }
    return date;
  }
  async save(): Promise<any> {
    this.isFormSubmit = true;

    if (
      this.form.title == null

      || this.form.start_date == null
      || this.form.end_date == null
      || (this.form.is_hl
        && (this.form.hl_start_date == null || this.form.hl_start_date == null))
    ) {

      alert("Please alert")
      return null;
    }

    const start = moment(this.form.start_date).format('YYYY/MM/DD') + " " + this.form.start_time;
    const end = moment(this.form.end_date).format('YYYY/MM/DD') + " " + this.form.end_time;
    const formData = new FormData();

    formData.append('Id', this.form.id);
    formData.append('Title', this.form.title);
    formData.append('Detail', this.form.detail);
    formData.append('StartDate', start);
    formData.append('EndDate', end);

    // formData.append('HLStartDate', this.form.st);
    // formData.append('HLEndDate', file.data);
    formData.append('IsActive', this.form.is_active.toString());
    formData.append('IsHighlight', this.form.is_hl.toString());
    formData.append('IsNotification', this.form.is_noti.toString());

    if (this.form.image) {
      formData.append('ImageFile', this.form.image);
    }
    if (this.form.video) {
      formData.append('VideoFile', this.form.video);
    }


    if (this.form.file) {
      formData.append('DocumentFile', this.form.file);
    }

    if (this.form.url) {
      formData.append('Url', this.form.url);
    }
    if (this.form.video_url) {
      formData.append('VideoUrl', this.form.video_url);
    }
    formData.append('NewsType', '2');
    formData.append('IsAll', this.form.is_all.toString());
    if (!this.form.is_all) {
      formData.append('TargetCustomerType', this.form.target_customer);
      formData.append('TargetDriverType', this.form.target_driver);
    }
    if (this.form.is_hl) {
      const start = moment(this.form.hl_start_date).format('YYYY/MM/DD') + " " + this.form.hl_start_time;
      const end = moment(this.form.hl_end_date).format('YYYY/MM/DD') + " " + this.form.hl_end_time;
      formData.append('HLStartDate', start);
      formData.append('HLEndDate', end);
    }
    formData.append('IsRemoveVideo', this.form.video_delete.toString());
    formData.append('IsRemoveDocument', this.form.file_delete.toString());
    formData.append('IsGuest', this.form.is_guest.toString());
    try {
      const resp = await this.api.updateNews(formData);
      if (resp.succeeded) {
        this.router.navigateByUrl('announcement');
      }
    } catch (error) {

    }

  }
  back() {
    this.router.navigateByUrl('announcement');
  }
  handleFileInput(text, files: FileList) {
    const file = files[0];
    if (text === 'image') {
      const reader = new FileReader();
      reader.onload = e => this.image = reader.result;
      reader.readAsDataURL(file);
      this.form.image = file;
    } else if (text === 'file') {
      this.form.file = file;
    } else if (text === 'video') {
      this.form.video = file;
    }
  }
}
