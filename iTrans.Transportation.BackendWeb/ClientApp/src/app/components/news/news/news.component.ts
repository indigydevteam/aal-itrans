import { Component, OnInit } from '@angular/core';
import { DialogService } from 'src/app/services/dialog/dialog.service';
import { NewsService } from 'src/app/services/news/news.service';

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.scss']
})
export class NewsComponent implements OnInit {
  body = {
    "pageNumber": 1,
    "pageSize": 10,
    "category": 1,
    "keyword": "",
    "isHighlight": false,
    "count": 0
  }
  items: any = [];
  constructor(
    private api: NewsService,
    private dialog: DialogService
  ) { }

  ngOnInit(): void {
    this.fetchData();
  }

  async previous(): Promise<any> {
    this.body.pageNumber--;
    this.fetchData();
  }
  async next(): Promise<any> {
    this.body.pageNumber++;
    this.fetchData();
  }
  async selectedPage(event): Promise<any> {
    this.body.pageNumber = event;
    this.fetchData();
  }

  async fetchData(): Promise<any> {
    const resp = await this.api.getNews(this.body);
    this.items = resp;

   

    this.body.count = resp.data.itemCount;
  }
  async onDelete(id): Promise<any> {
    const resp = await this.dialog.confirm("Are you sure to delete ", "", false);
    if (resp) {
      const body = {
        id: id
      }
      try {
        const res = await this.api.deleteNews(body);
        this.fetchData();
      } catch (error) {

      }

    }
  }
}
