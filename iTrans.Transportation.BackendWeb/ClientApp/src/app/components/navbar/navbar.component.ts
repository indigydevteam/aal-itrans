import { Component, OnInit } from '@angular/core';
import { ResolveStart, Router } from '@angular/router';
import { DialogService } from 'src/app/services/dialog/dialog.service';
import { EventService } from 'src/app/services/event/event.service';
import { ProfileService } from 'src/app/services/profile/profile.service';
import { SignalRService } from 'src/app/services/SignalR/signal-r.service';
import { StorageService } from 'src/app/services/storage/storage.service';
import { UserService } from 'src/app/services/user/user.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  page = null;
  chatBage = 0;
  profile: any;
  permission: any = [];
  checknew: boolean = false;
  checkannount: boolean = false;
  constructor(
    public router: Router,
    public signalRServ: SignalRService,
    public storage: StorageService,
    public event: EventService,
    public profileSevice: ProfileService,
    public userservice: UserService,
    public dialog: DialogService
  ) {

    // var ecy = this.signalRServ.encrypt("P@assw0rd", "chat");
    // var decy = this.signalRServ.decrypt("0m3y91QjPNEE0OnCkt9FVw==", "chat");

    // console.log(ecy)
    // console.log(decy)

    this.router.events.subscribe((e: ResolveStart) => {
      const p = e.url.split('/');
      this.page = p[1];
    })
    this.event.onChatBadge.subscribe(res => {
      this.getBadge();
    });
    this.event.onProfileGet.subscribe(res => {
      this.profile = res;
    })
    setInterval(() => {
      this.chatBage = this.chatBage;
    }, 1000)
  }

  ngOnInit(): void {
    setTimeout(() => {
      this.fetchData();
    }, 500);
  }

  async fetchData() {
    const usr = await this.storage.getItem('token');
    this.profile = usr;
    this.countChatBage();
    this.setProfile();
    this.getUserProfile();


  }
  async singout() {
    this.dialog.confirm("Logout", "").then(res => {
      if (res) {
        window.localStorage.clear();
        this.router.navigateByUrl('auth/signin')
      }
    })
  }
  async setProfile() {
    await this.profileSevice.GetProfile();
  }
  async countChatBage() {
    try {

      await this.signalRServ.disconnectChatBadge();
      await this.signalRServ.connectChatBadgeAsync()
      this.getBadge();
      this.signalRServ.hubConnectionChatBadge.off('badgeResult');
      this.signalRServ.hubConnectionChatBadge.on('badgeResult', (res: any) => {
        this.chatBage = res?.totalUnRead ?? 0;
      });
    } catch (error) {


      console.log(error);
      // this.coreService.services.logEvent.log('##badgeResult##', error, null);
    }
  }
  async getBadge() {
    this.signalRServ.hubConnectionChatBadge.invoke('GetBadge', this.profile.Id).then(res => {
      this.chatBage = res?.totalUnRead ?? 0;

    }).catch(err => {
      console.log(err)
    })
  }

  async getUserProfile() {
    this.userservice.GetUserProfile().subscribe(permission => {
      this.permission = permission.data;
      if (this.permission.menuPermission[2].path == "news" && this.permission.menuPermission[2].isShow === true) {
        this.checknew = true;
      }
      if (this.permission.menuPermission[3].path == "announcement" && this.permission.menuPermission[3].isShow === true) {
        this.checkannount = true;
      }
    });
  }
  btnClick = function (link) {
    console.log(link);
    link = "/" + link;
    this.router.navigateByUrl(link);
  };
}
