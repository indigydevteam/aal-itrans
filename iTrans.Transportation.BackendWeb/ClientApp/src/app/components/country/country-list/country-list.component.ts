import { CountryService } from './../../../services/country/country.service';
import { Component, OnInit } from '@angular/core';
import { NgxRangeModule } from 'ngx-range';
import { first } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { CountryList } from '../../../models/country.model';
@Component({
  selector: 'app-country-list',
  templateUrl: './country-list.component.html',
  styleUrls: ['./country-list.component.scss']
})
export class CountryListComponent implements OnInit {

  public countrys: Observable<CountryList> | any;
  public page: number = 0;
  public pageSize: number = 10;
  public pageNumber: number = 1;
  public search: string = "";

  constructor(private countryservice: CountryService) { }
  
  ngOnInit(): void {
    this.getAllCountry();
  }

  public getAllCountry() {
    this.countryservice.getAllCountry(this.search, this.pageNumber, this.pageSize).subscribe(countrys => {
      this.countrys = countrys;
      this.page = countrys.itemCount / (countrys.pageSize == 0 ? 1 : countrys.pageSize);
      if (this.page == 0) this.page = 1;
    });
  }

  public filter(event) {
    this.search = event.target.value;
    this.pageNumber = 1;
    this.getAllCountry();
  }
  public pageChange(pageNumber: number ) {
    console.log(pageNumber);
    this.pageNumber = pageNumber;
    this.getAllCountry();
  }
}
