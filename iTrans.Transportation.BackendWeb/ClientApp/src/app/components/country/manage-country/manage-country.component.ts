import { CountryService } from './../../../services/country/country.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { MustMatch } from './../../../shared/must-match.validator';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-manage-country',
  templateUrl: './manage-country.component.html',
  styleUrls: ['./manage-country.component.scss']
})
export class ManageCountryComponent implements OnInit {

  form!: FormGroup;
  id!: string;
  isAddMode!: boolean;
  submitted = false;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private countryService: CountryService,
  ) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.params.id;
    this.isAddMode = !this.id;
    this.form = this.formBuilder.group({
        Id: this.id,
        name_TH: ['', Validators.required],
        name_ENG: ['', Validators.required],
        code: ['', Validators.required],
        active: [true]
      }
    );
    if (!this.isAddMode) {
      this.countryService.getCountryById(this.id)
        .pipe(first())
        .subscribe(x => this.form.patchValue(x));
    }
  }
  get formControls() { return this.form.controls; }

  private createCountry() {

    this.countryService.createCountry(this.form.value)
      .pipe(first())
      .subscribe({
        next: () => {
          this.router.navigate(['/country'], { relativeTo: this.route });
        }
      });
  }
  private updateCountry() {
    this.countryService.updateCountry(this.form.value)
      .pipe(first())
      .subscribe({
        next: () => {
          this.router.navigate(['/country'], { relativeTo: this.route });
        }
      });
  }

  onSubmit() {
    this.submitted = true;
    if (this.form.invalid) {
      return;
    }

    if (this.isAddMode) {
      this.createCountry();
    } else {
      this.updateCountry();
    }

  }
}
