import { ProvinceService } from './../../../services/province/province.service';
import { Component, OnInit } from '@angular/core';
import { NgxRangeModule } from 'ngx-range';
import { first } from 'rxjs/operators';
import { Province, ProvinceList } from 'src/app/models/province.model';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-province-list',
  templateUrl: './province-list.component.html',
  styleUrls: ['./province-list.component.scss']
})
export class ProvinceListComponent implements OnInit {

  public provinces: Observable<ProvinceList> | any;
  public page: number = 0;
  constructor(private provinceservice: ProvinceService) { }
  public pageSize: number = 10;
  public pageNumber: number = 1;
  public search: string = "";
  ngOnInit(): void {
    this.getAllProvince();
  }

  public getAllProvince() {
    this.provinceservice.getAllProvince(this.search, this.pageNumber, this.pageSize).subscribe(provinces => {
      console.log(provinces);
      this.provinces = provinces;
      this.page = provinces.itemCount / (provinces.pageSize == 0 ? 1 : provinces.pageSize);
      if (this.page == 0) this.page = 1;
    });
  }

  public filter(event) {
    console.log("You entered: ", event.target.value);
    this.search = event.target.value;
    this.pageNumber = 1;
    this.getAllProvince();
  }
  public pageChange(pageNumber: number ) {
    console.log(pageNumber);
    this.pageNumber = pageNumber;
    this.getAllProvince();
  }
}
