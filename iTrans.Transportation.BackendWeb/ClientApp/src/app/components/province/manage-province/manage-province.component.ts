import { CountryService } from './../../../services/country/country.service';
import { RegionService } from './../../../services/region/region.service';
import { ProvinceService } from './../../../services/province/province.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { MustMatch } from './../../../shared/must-match.validator';
import { first } from 'rxjs/operators';
import { Country } from '../../../models/country.model';
import { Region } from '../../../models/region.model';

@Component({
  selector: 'app-manage-province',
  templateUrl: './manage-province.component.html',
  styleUrls: ['./manage-province.component.scss']
})
export class ManageProvinceComponent implements OnInit {

  form!: FormGroup;
  id!: string;
  isAddMode!: boolean;
  submitted = false;
  countrys: Country[] | any;
  regions: Region[] | any;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private provinceService: ProvinceService,
    private regionService: RegionService,
    private countryService: CountryService,
  ) { }

  display: any = "d-none";
  ngOnInit(): void {
    this.getAllCountry();
    this.id = this.route.snapshot.params.id;
    this.isAddMode = !this.id;
    this.form = this.formBuilder.group({
      Id: this.id,
      countryId: ['', Validators.required],
      regionId: ['', Validators.required],
      name_TH: ['', Validators.required],
      name_ENG: ['', Validators.required],
      active: [true]
    }
    );
    if (!this.isAddMode) {
      this.display = "";
      this.provinceService.getProvinceById(this.id)
        .pipe(first())
        .subscribe(x => {
          this.form.patchValue(x);
          console.log(this.form);
          this.regionService.GetRegionByCountry(x.countryId.toString()).subscribe(regions => {
            this.regions = regions;
          });
        });

      
      
    }
  }
  get formControls() { return this.form.controls; }

  private createProvince() {
    console.log(this.form.value);
    this.provinceService.createProvince(this.form.value)
      .pipe(first())
      .subscribe({
        next: () => {
          this.router.navigate(['/province'], { relativeTo: this.route });
        }
      });
  }
  private updateProvince() {
    this.provinceService.updateProvince(this.form.value)
      .pipe(first())
      .subscribe({
        next: () => {
          this.router.navigate(['/province'], { relativeTo: this.route });
        }
      });
  }

  onSubmit() {
    
    this.submitted = true;
    if (this.form.invalid) {
      return;
    }

    if (this.isAddMode) {
      this.createProvince();
    } else {
      this.updateProvince();
    }

  }

  public getAllCountry() {
    this.countryService.getAllCountry("", 0, 0).subscribe(countrys => {
      this.countrys = countrys.data;
      console.log(this.countrys);
    });
  }

  onChangeCountry() {
    this.display = "";
    this.regions = null;
    console.log(this.form.value.countryId);
    this.regionService.GetRegionByCountry(this.form.value.countryId).subscribe(regions => {
      this.regions = regions;
    });
  }

}
