import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { first } from 'rxjs/operators';
import { CustomerLevelCharacteristic, CustomerLevelCharacteristicList } from 'src/app/models/customerlevelcharacteristic.model';
import { DialogService } from 'src/app/services/dialog/dialog.service';
import { CustomerLevelCharacteristicService } from 'src/app/services/customerlevelcharacteristic/customerlevelcharacteristic.service';

@Component({
  selector: 'app-customer-level-characteristic-list',
  templateUrl: './customer-level-characteristic-list.component.html',
  styleUrls: ['./customer-level-characteristic-list.component.scss']
})
export class CustomerLevelCharacteristicListComponent implements OnInit {
  public customerLevelCharacteristics: Observable<CustomerLevelCharacteristic> | any;
  public count: number = 0;
  public pageSize: number = 10;
  public pageNumber: number = 1;
  public search: string = "";
  constructor(private customerlevelcharacteristicservice: CustomerLevelCharacteristicService, private dialog: DialogService) { }
  active:number;
  column:string;
  ngOnInit(): void {
    this.all()
  }
  public CustomerLevelCharacteristic() {
    this.customerlevelcharacteristicservice.getAll(this.search, this.pageNumber, this.pageSize, this.column, this.active).subscribe(customerLevelCharacteristics => {
      this.customerLevelCharacteristics = customerLevelCharacteristics;
    });
  }
  all() {
    this.active = 1;
    this.column = "all";
    this.CustomerLevelCharacteristic();
  }

  orderBy(value) {
    if (value == this.column) {
      if (this.active == 1) {
        this.active = 2;
      } else if (this.active == 2) {
        this.active = 3;
      } else {
        this.active = 1;
      }
    } else {
      this.active = 2;
    }
    this.column = value;
    this.CustomerLevelCharacteristic();
  }
  public filter(search) {
    this.search = search;
    this.pageNumber = 1;
    this.CustomerLevelCharacteristic();
  }
  previous() {
    this.pageNumber--;
    this.CustomerLevelCharacteristic();
  }
  next() {
    this.pageNumber++;
    this.CustomerLevelCharacteristic();
  }
  selectedPage(event) {
    this.pageNumber = event;
    this.CustomerLevelCharacteristic();
  }
  async onDelete(id) {
    const resp = await this.dialog.confirm("Are you sure to delete ", "", false);
    if (resp) {
      this.customerlevelcharacteristicservice.delete(id)
        .pipe(first())
        .subscribe(customerLevelCharacteristics => {
          this.customerLevelCharacteristics = customerLevelCharacteristics;
          if (this.customerLevelCharacteristics.data !== null)
            this.CustomerLevelCharacteristic();
        });
    }

  }
}
