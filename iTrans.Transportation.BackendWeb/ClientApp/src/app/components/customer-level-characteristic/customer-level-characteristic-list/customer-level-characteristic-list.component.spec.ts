import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomerLevelCharacteristicListComponent } from './customer-level-characteristic-list.component';

describe('CustomerLevelCharacteristicListComponent', () => {
  let component: CustomerLevelCharacteristicListComponent;
  let fixture: ComponentFixture<CustomerLevelCharacteristicListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CustomerLevelCharacteristicListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomerLevelCharacteristicListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
