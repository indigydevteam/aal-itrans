import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { first } from 'rxjs/operators';
import { CustomerLevelCharacteristic, CustomerLevelCharacteristicList } from 'src/app/models/customerlevelcharacteristic.model';
import { DialogService } from 'src/app/services/dialog/dialog.service';
import { CustomerLevelCharacteristicService } from 'src/app/services/customerlevelcharacteristic/customerlevelcharacteristic.service';

@Component({
  selector: 'app-manage-customer-level-characteristic',
  templateUrl: './manage-customer-level-characteristic.component.html',
  styleUrls: ['./manage-customer-level-characteristic.component.scss']
})
export class ManageCustomerLevelCharacteristicComponent implements OnInit {
  form!: FormGroup;
  id!: string;
  isAddMode!: boolean;
  submitted = false;
  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private service: CustomerLevelCharacteristicService) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.params.id;
    this.isAddMode = !this.id;
    this.form = this.formBuilder.group({
      id: this.id,
      level: ['', Validators.required],
      star: ['', Validators.required],
      requestCar: ['', Validators.required],
      cancelAmount: ['', Validators.required],
      orderValue: ['', Validators.required]
    }
    );
    if (!this.isAddMode) {
      this.service.getById(this.id)
        .pipe(first())
        .subscribe(x => this.form.patchValue(x));

    }
  }
  get formControls() { return this.form.controls; }

  private create() {

    this.service.create(this.form.value)
      .pipe(first())
      .subscribe({
        next: () => {
          this.router.navigate(['/customerlevelcharacteristic'], { relativeTo: this.route });
        }
      });
  }
  private update() {
    this.service.update(this.form.value)
      .pipe(first())
      .subscribe({
        next: () => {
          this.router.navigate(['/customerlevelcharacteristic'], { relativeTo: this.route });
        }
      });
  }
  onSubmit() {
    this.submitted = true;
    if (this.form.invalid) {
      return;
    }

    if (this.isAddMode) {
      this.create();
    } else {
      this.update();
    }

  }
}
