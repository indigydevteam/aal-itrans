import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageCustomerLevelCharacteristicComponent } from './manage-customer-level-characteristic.component';

describe('MangeCustomerLevelCharacteristicComponent', () => {
  let component: ManageCustomerLevelCharacteristicComponent;
  let fixture: ComponentFixture<ManageCustomerLevelCharacteristicComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ManageCustomerLevelCharacteristicComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageCustomerLevelCharacteristicComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
