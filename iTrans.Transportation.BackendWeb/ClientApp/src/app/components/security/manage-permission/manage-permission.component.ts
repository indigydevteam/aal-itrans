import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { first } from 'rxjs/operators';
import { DialogService } from 'src/app/services/dialog/dialog.service';
import { SecurityService } from 'src/app/services/security/security.service';

@Component({
  selector: 'app-manage-permission',
  templateUrl: './manage-permission.component.html',
  styleUrls: ['./manage-permission.component.scss']
})
export class ManagePermissionComponent implements OnInit {



  constructor(
    private securityservice: SecurityService,
    private dialog: DialogService,
    private route: ActivatedRoute,
    private router: Router,



  ) { }

  isAddMode!: boolean;
  Id!: string;



  selectedItemsList = [];
  selectedItems = [];
  permission: any = [];

  Name:any=[];


  ngOnInit(): void {
    this.Id = this.route.snapshot.params.id;
    this.isAddMode = !this.Id;

    this.GetAllMenuPermission();
    this.fetchSelectedItems();
  }

 
  public GetAllMenuPermission() {
    this.selectedItems = [];
    this.securityservice.GetAllMenuPermission(this.Id).subscribe(permission => {
      this.permission = permission;
      this.Name = permission[0].rolename;
      this.permission.forEach(element => {
        element.roleMenuPermission.forEach(e => {
          this.selectedItems.push(e);
        });
      });
    });
  }
  checkddata(event: any) {
    console.log(event);
  }
   onSubmit() {

    if (this.isAddMode) {
      this.createRoleManu();
    } else {
      this.updateRoleManu();
    }
  }
  async updateRoleManu() {
    this.fetchSelectedItems();
    console.log(this.selectedItemsList);

    const resp = await this.dialog.confirm("Are you sure to update ", "", false);
    const formData = new FormData();
    
    formData.append("Id", this.Id);
    formData.append("NameTH", this.Name);
    formData.append("NameEN", this.Name);
    formData.append("MenuPermissions", JSON.stringify(this.selectedItemsList));
    if (resp) {
      this.securityservice.updateRoleManu(formData)
        .pipe(first())
        .subscribe({
          next: () => {
            window.location.href = '/permission';
            // this.router.navigate(['/permission'], { relativeTo: this.route });
            

          }
        });
    }
  }

async createRoleManu() {
    this.fetchSelectedItems();
    console.log(this.selectedItemsList);
    const formData = new FormData();

    formData.append("NameTH", this.Name);
    formData.append("NameEN", this.Name);
    formData.append("MenuPermissions", JSON.stringify(this.selectedItemsList));


      this.securityservice.createRoleManu(formData)
        .pipe(first())
        .subscribe({
          next: () => {
            this.router.navigate(['/permission'], { relativeTo: this.route });
          }
        });
  }
  
  changeSelection() {
    this.fetchSelectedItems()
  }

  fetchSelectedItems() {

    this.selectedItemsList = this.selectedItems.filter((value, index) => {
      return value.is_Check
    });
    console.log(this.selectedItemsList);
  }


}
