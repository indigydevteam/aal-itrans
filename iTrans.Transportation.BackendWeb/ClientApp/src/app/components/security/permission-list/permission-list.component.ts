import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs/operators';
import { DialogService } from 'src/app/services/dialog/dialog.service';
import { SecurityService } from 'src/app/services/security/security.service';

@Component({
  selector: 'app-permission-list',
  templateUrl: './permission-list.component.html',
  styleUrls: ['./permission-list.component.scss']
})
export class PermissionListComponent implements OnInit {

  public count: number = 0;
  public pageSize: number = 10;
  public pageNumber: number = 1;
  public search: string = "";

    constructor(
      private securityservice: SecurityService,    
      private dialog: DialogService,
      
      ) { }

  roles:any=[];
  
  ngOnInit(): void {
    this.getAllRole();
  }

  public getAllRole() {
    this.securityservice.getAllRole(this.search, this.pageNumber, this.pageSize).subscribe(roles => {
      this.roles = roles;
      this.count = roles.data.itemCount;
    });
  }
  public filter(search) {
    console.log("You search: "+search);
    this.search = search;
    this.pageNumber = 1;
    this.getAllRole();
  }
  previous() {
    this.pageNumber--;
    this.getAllRole();
  }
   next() {
    this.pageNumber++;
    this.getAllRole();
  }
   selectedPage(event) {
    this.pageNumber = event;
    this.getAllRole();
  }
  
   async onDelete(id) {
    const resp = await this.dialog.confirm("Are you sure to delete ", "", false);
    if (resp) {
        this.securityservice.DeleteRole(id)
        .pipe(first())
        .subscribe(roles => {
          this.roles = roles;
          if(this.roles.data !== null)
          this.getAllRole();      
        });
    }
   
  }

  
}