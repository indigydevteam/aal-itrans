import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { first } from 'rxjs/operators';
import { DialogService } from 'src/app/services/dialog/dialog.service';
import { SecurityService } from 'src/app/services/security/security.service';
import { StorageService } from 'src/app/services/storage/storage.service';

@Component({
  selector: 'app-changepassword',
  templateUrl: './changepassword.component.html',
  styleUrls: ['./changepassword.component.scss']
})
export class ChangepasswordComponent implements OnInit {

 

  constructor(
    private securityservice: SecurityService,
    private dialog: DialogService,
    private router: Router,
    private route: ActivatedRoute,




  ) { }
  Password=""
  password: any = [];
  form={
    type:null,
    newPassword:null,
    confirmNewPassword :null
  }

  ngOnInit(): void {
 
    this.form={
      type:"admin",
      newPassword:"",
      confirmNewPassword :""
    }

  }

  onsubmit() {

  this.securityservice.CheckPassword(this.Password).subscribe(password => {
    console.log(password.data)
    if(password.message === "sucess") {

      this.SetPassword(); 
  }else{
    const resp =  this.dialog.error("Password Not Found.");
  }
    });

  }

  async SetPassword() {
  

    const resp = await this.dialog.confirm("Are you sure to update ", "",);
    if (resp) {

      this.securityservice.SetPassword(this.form)
      .pipe(first())
      .subscribe({
        next: () => {
          this.router.navigate(['/adminprofile'], { relativeTo: this.route });
        }
        });
    }

  }
}
