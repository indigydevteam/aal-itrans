import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { RegisterInformationList } from 'src/app/models/register.model';
import { RegisterService } from 'src/app/services/register/register.service';
import { DialogService } from '../../../services/dialog/dialog.service';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-registerinformation-list',
  templateUrl: './registerinformation-list.component.html',
  styleUrls: ['./registerinformation-list.component.scss']
})
export class RegisterinformationListComponent implements OnInit  {

  public registerinformations: Observable<RegisterInformationList> | any;
  public pageSize: number = 10;
  public pageNumber: number = 1;
  public search: string = "";
  active: number;
  column: string;

  constructor(private registerservice: RegisterService, private dialog: DialogService) { }
  
  ngOnInit(): void {
    this.all();
  }

  public getAllRegisterInformation() {
    this.registerservice.getAllRegisterInformation(this.search, this.pageNumber, this.pageSize, this.column, this.active).subscribe(registerinformations => {
      console.log(registerinformations);
      this.registerinformations = registerinformations;
    });
  }

  all() {
    this.active = 1;
    this.column = "all";
    this.getAllRegisterInformation();
  }

  orderBy(value) {
    if (value == this.column) {
      if (this.active == 1) {
        this.active = 2;
      } else if (this.active == 2) {
        this.active = 3;
      } else {
        this.active = 1;
      }
    } else {
      this.active = 2;
    }
    this.column = value;
    this.getAllRegisterInformation();
  }

  public filter(search) {
    this.search = search;
    this.pageNumber = 1;
    this.getAllRegisterInformation();
  }
  previous() {
    this.pageNumber--;
    this.getAllRegisterInformation();
  }
  next() {
    this.pageNumber++;
    this.getAllRegisterInformation();
  }
  selectedPage(event) {
    this.pageNumber = event;
    this.getAllRegisterInformation();
  }

  async onDelete(id) {
    const resp = await this.dialog.confirm("Are you sure to delete ", "", false);
    if (resp) {
      //this.registerservice.DeleteWelcomeinformationById(id)
      //  .pipe(first())
      //  .subscribe(welcomeinformations => {
      //    this.welcomeinformations = welcomeinformations;
      //    if (this.welcomeinformations.data !== null)
      //      this.getAllWelcomeInformation();
      //  });
    }

  }

}
