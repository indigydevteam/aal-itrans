import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisterinformationListComponent } from './registerinformation-list.component';

describe('RegisterinformationListComponent', () => {
  let component: RegisterinformationListComponent;
  let fixture: ComponentFixture<RegisterinformationListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RegisterinformationListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterinformationListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
