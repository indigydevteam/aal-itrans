import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageRegisterinformationComponent } from './manage-registerinformation.component';

describe('ManageRegisterinformationComponent', () => {
  let component: ManageRegisterinformationComponent;
  let fixture: ComponentFixture<ManageRegisterinformationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ManageRegisterinformationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageRegisterinformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
