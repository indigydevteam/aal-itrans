import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { first } from 'rxjs/operators';
import { RegisterService } from 'src/app/services/register/register.service';

@Component({
  selector: 'app-manage-registerinformation',
  templateUrl: './manage-registerinformation.component.html',
  styleUrls: ['./manage-registerinformation.component.scss']
})
export class ManageRegisterinformationComponent implements OnInit{

  config = {
    placeholder: '',
    tabsize: 2,
    height: '200px',
    uploadImagePath: '/api/upload',
    toolbar: [
        ['misc', ['codeview', 'undo', 'redo']],
        ['style', ['bold', 'italic', 'underline', 'clear']],
        ['fontsize', ['fontname', 'fontsize', 'color']],
        ['para', ['style', 'ul', 'ol', 'paragraph', 'height']],
    ],
    fontNames: ['Helvetica', 'Arial', 'Arial Black', 'Comic Sans MS', 'Courier New', 'Roboto', 'Times']
  }
  
  form!: FormGroup;
  id!: string;
  isAddMode!: boolean;
  submitted = false;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private registerservice: RegisterService
    ) { }
  ngOnInit(): void {

    this.id = this.route.snapshot.params.id;
    this.isAddMode = !this.id;
    this.form = this.formBuilder.group({
        Id: this.id,
        module: ['', Validators.required],
        title_TH: ['', Validators.required],
        title_ENG: ['', Validators.required],
        information_TH: ['', Validators.required],
      information_ENG: ['', Validators.required],
      sequence: ['', Validators.required],
        picture: [''],
        active: [true]
      }
    );
    if (!this.isAddMode) {
      this.registerservice.getRegisterInformationById(this.id)
        .pipe(first())
        .subscribe(x => this.form.patchValue(x));

    }
  }
  get formControls() { return this.form.controls; }

  private async creatRegisterInformation() {

    const formData = new FormData();
    formData.append('Module', this.form.value.module);
    formData.append('Title_TH', this.form.value.title_TH);
    formData.append('Information_TH', this.form.value.information_TH);
    formData.append('Title_ENG', this.form.value.title_ENG);
    formData.append('Information_ENG', this.form.value.information_ENG);
    formData.append('Sequence', this.form.value.sequence);
    formData.append('Active', this.form.value.active);
     
    const resp = await this.registerservice.createRegisterInformation(formData);
    if (resp.succeeded) {
      this.router.navigateByUrl('registerinformation');
    }
  }
  private async updateRegisterInformation() {
    const formData = new FormData();
    formData.append('Id', this.id);
    formData.append('Module', this.form.value.module);
    formData.append('Title_TH', this.form.value.title_TH);
    formData.append('Information_TH', this.form.value.information_TH);
    formData.append('Title_ENG', this.form.value.title_ENG);
    formData.append('Information_ENG', this.form.value.information_ENG);
    formData.append('Sequence', this.form.value.sequence);
    formData.append('Active', this.form.value.active);

    
    const resp = await this.registerservice.updateRegisterInformation(formData);
    if (resp.succeeded) {
      this.router.navigateByUrl('registerinformation');
    }
  }

  onSubmit() {
    this.submitted = true;
    if (this.form.invalid) {
      return;
    }
    

    if (this.isAddMode) {
      this.creatRegisterInformation();
    } else {
      this.updateRegisterInformation();
    }

  }

}
