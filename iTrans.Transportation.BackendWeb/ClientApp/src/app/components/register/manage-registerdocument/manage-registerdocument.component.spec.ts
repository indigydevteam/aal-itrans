import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageRegisterdocumentComponent } from './manage-registerdocument.component';

describe('ManageRegisterdocumentComponent', () => {
  let component: ManageRegisterdocumentComponent;
  let fixture: ComponentFixture<ManageRegisterdocumentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ManageRegisterdocumentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageRegisterdocumentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
