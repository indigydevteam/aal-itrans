import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { first } from 'rxjs/operators';
import { RegisterService } from 'src/app/services/register/register.service';

@Component({
  selector: 'app-manage-registerdocument',
  templateUrl: './manage-registerdocument.component.html',
  styleUrls: ['./manage-registerdocument.component.scss']
})
export class ManageRegisterdocumentComponent implements OnInit {

  config = {
    placeholder: '',
    tabsize: 2,
    height: '200px',
    uploadImagePath: '/api/upload',
    toolbar: [
        ['misc', ['codeview', 'undo', 'redo']],
        ['style', ['bold', 'italic', 'underline', 'clear']],
        ['fontsize', ['fontname', 'fontsize', 'color']],
        ['para', ['style', 'ul', 'ol', 'paragraph', 'height']],
    ],
    fontNames: ['Helvetica', 'Arial', 'Arial Black', 'Comic Sans MS', 'Courier New', 'Roboto', 'Times']
  }

  form!: FormGroup;
  id!: string;
  isAddMode!: boolean;
  submitted = false;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private registerservice: RegisterService
    ) { }
  ngOnInit(): void {

    this.id = this.route.snapshot.params.id;
    this.isAddMode = !this.id;
    this.form = this.formBuilder.group({
        Id: this.id,
        module: ['', Validators.required],
        information_TH: ['', Validators.required],
        information_ENG: ['', Validators.required],
        picture: [''],
        active: [true]
      }
    );
    if (!this.isAddMode) {
      this.registerservice.getRegisterDocumentById(this.id)
        .pipe(first())
        .subscribe(x => this.form.patchValue(x));

    }
  }
  get formControls() { return this.form.controls; }

  private creatRegisterDocument() {

    this.registerservice.creatRegisterDocument(this.form.value)
      .pipe(first())
      .subscribe({
        next: () => {
          this.router.navigate(['/registerdocument'], { relativeTo: this.route });
        }
      });
  }
  private updateRegisterDocument() {
    this.registerservice.updateRegisterDocument(this.form.value)
      .pipe(first())
      .subscribe({
        next: () => {
          this.router.navigate(['/registerdocument'], { relativeTo: this.route });
        }
      });
  }

  onSubmit() {
    this.submitted = true;
    if (this.form.invalid) {
      return;
    }
    

    if (this.isAddMode) {
      this.creatRegisterDocument();
    } else {
      this.updateRegisterDocument();
    }

  }

}
