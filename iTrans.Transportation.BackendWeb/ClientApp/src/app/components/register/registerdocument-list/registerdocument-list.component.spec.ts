import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisterdocumentListComponent } from './registerdocument-list.component';

describe('RegisterdocumentListComponent', () => {
  let component: RegisterdocumentListComponent;
  let fixture: ComponentFixture<RegisterdocumentListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RegisterdocumentListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterdocumentListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
