import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { RegisterDocumentList } from 'src/app/models/register.model';
import { RegisterService } from 'src/app/services/register/register.service';

@Component({
  selector: 'app-registerdocument-list',
  templateUrl: './registerdocument-list.component.html',
  styleUrls: ['./registerdocument-list.component.scss']
})
export class RegisterdocumentListComponent implements OnInit  {

  public registerdocuments: Observable<RegisterDocumentList> | any;
  public page: number = 0;
  public pageSize: number = 10;
  public pageNumber: number = 1;
  public search: string = "";

  constructor(private registerservice: RegisterService) { }
  
  ngOnInit(): void {
    this.getAllRegisterDocument();
  }

  public getAllRegisterDocument() {
    this.registerservice.getAllRegisterDocument(this.search, this.pageNumber, this.pageSize).subscribe(registerdocuments => {
      this.registerdocuments = registerdocuments;
      this.page = registerdocuments.itemCount / (registerdocuments.pageSize == 0 ? 1 : registerdocuments.pageSize);
      if (this.page == 0) this.page = 1;
    });
  }
  public filter(event) {
    console.log("You entered: ", event.target.value);
    this.search = event.target.value;
    this.pageNumber = 1;
    this.getAllRegisterDocument();
  }
  public pageChange(pageNumber: number ) {
    console.log(pageNumber);
    this.pageNumber = pageNumber;
    this.getAllRegisterDocument();
  }

}
