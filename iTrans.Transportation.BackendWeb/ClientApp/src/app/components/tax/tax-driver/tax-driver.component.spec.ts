import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TaxDriverComponent } from './tax-driver.component';

describe('TaxDriverComponent', () => {
  let component: TaxDriverComponent;
  let fixture: ComponentFixture<TaxDriverComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TaxDriverComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TaxDriverComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
