import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs/operators';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { DriverService } from 'src/app/services/driver/driver.service';
import { DialogService } from 'src/app/services/dialog/dialog.service';



@Component({
  selector: 'app-tax-driver',
  templateUrl: './tax-driver.component.html',
  styleUrls: ['./tax-driver.component.scss']
})
export class TaxDriverComponent implements OnInit {

  // form!: FormGroup;
  id!: string;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private driverservice: DriverService,
    private dialog: DialogService

  ) { }
  countries: any = [];
  provinces: any = [];
  districts: any = [];
  subdistricts: any = [];
  countries2: any = [];
  provinces2: any = [];
  districts2: any = [];
  subdistricts2: any = [];
  displaybox = "";
  countryid: any;
  road = "";
  alley = "";
  address = "";
  postcode = "";
  addressId: any;
  country: any;
  province: any;
  district: any;
  subdistrict: any;
  driver: any = [];
  isFormSubmit = false;

  form = {
    location: [
      {
        id: '0',
        driverId: '0',
        branch: null,
        countryId: null,
        provinceId: null,
        districtId: null,
        subdistrictId: null,
        address: null,
        road: null,
        alley: null,
        postCode: null,
        isMainData: true,
        addressType: null,
      }
    ],

  }

  ngOnInit(): void {
    this.id = this.route.snapshot.params.id;
    this.GetDriverTaxById();
    this.getAllCountry();
    this.getAllCountry2();



  }
  public GetDriverTaxById() {
    this.driverservice.GetDriverTaxById(this.id).subscribe(driver => {

      console.log(driver);
      this.driver = driver.data;


      if (this.driver.corporate !== null) {
        if (this.driver.corporate.length > 1) {
          this.form = {
            location: [
              {
                id: this.driver.corporate[0].id,
                driverId: this.id,
                branch: this.driver.corporate[0].branch,
                countryId: this.driver ? this.driver.corporate[0].country.id : null,
                provinceId: this.driver ? this.driver.corporate[0].province.id : null,
                districtId: this.driver ? this.driver.corporate[0].district.id : null,
                subdistrictId: this.driver ? this.driver.corporate[0].subdistrict.id : null,
                address: this.driver.corporate[0].address,
                road: this.driver.corporate[0].road,
                alley: this.driver.corporate[0].alley,
                postCode: this.driver.corporate[0].postCode,
                isMainData: this.driver.corporate[0].isMainData,
                addressType: this.driver.corporate[0].addressType,
              },
              {
                id: this.driver.corporate[1].id,
                driverId: this.id,
                branch: this.driver.corporate[1].branch,
                countryId: this.driver ? this.driver.corporate[1].country.id : null,
                provinceId: this.driver ? this.driver.corporate[1].province.id : null,
                districtId: this.driver ? this.driver.corporate[1].district.id : null,
                subdistrictId: this.driver ? this.driver.corporate[1].subdistrict.id : null,
                address: this.driver.corporate[1].address,
                road: this.driver.corporate[1].road,
                alley: this.driver.corporate[1].alley,
                postCode: this.driver.corporate[1].postCode,
                isMainData: this.driver.corporate[1].isMainData,
                addressType: this.driver.corporate[1].addressType,
              }
            ],
          }
        } else {
          this.form = {
            location: [
              {
                id: this.driver.corporate[0].id,
                driverId: this.id,
                branch: this.driver.corporate[0].branch,
                countryId: this.driver ? this.driver.corporate[0].country.id : null,
                provinceId: this.driver ? this.driver.corporate[0].province.id : null,
                districtId: this.driver ? this.driver.corporate[0].district.id : null,
                subdistrictId: this.driver ? this.driver.corporate[0].subdistrict.id : null,
                address: this.driver.corporate[0].address,
                road: this.driver.corporate[0].road,
                alley: this.driver.corporate[0].alley,
                postCode: this.driver.corporate[0].postCode,
                isMainData: this.driver.corporate[0].isMainData,
                addressType: this.driver.corporate[0].addressType,
              },
            ],
          }
        }

        console.log(this.form);
      }

      if (this.driver.personal !== null) {
        this.form = {
          location: [
            {
              id: this.driver.personal.id,
              driverId: this.id,
              branch: this.driver.personal.branch,
              countryId: this.driver ? this.driver.personal.country.id : null,
              provinceId: this.driver ? this.driver.personal.province.id : null,
              districtId: this.driver ? this.driver.personal.district.id : null,
              subdistrictId: this.driver ? this.driver.personal.subdistrict.id : null,
              address: this.driver.personal.address,
              road: this.driver.personal.road,
              alley: this.driver.personal.alley,
              postCode: this.driver.personal.postCode,
              isMainData: this.driver.personal.isMainData,
              addressType: this.driver.personal.addressType,
            }
          ],
        }

        console.log(this.form);
      }

      this.driverservice.getAllProvince(this.form.location[0].countryId).subscribe(province => {
        this.provinces = province;
      });
      this.driverservice.getAllDistrict(this.form.location[0].provinceId).subscribe(district => {
        this.districts = district;
      });

      this.driverservice.getAllSubdistrict(this.form.location[0].districtId).subscribe(subdistrict => {
        this.subdistricts = subdistrict;
      });

      if (this.driver.corporate.length > 1) {
        this.driverservice.getAllProvince(this.form.location[1].countryId).subscribe(province => {
          this.provinces2 = province;
        });
        this.driverservice.getAllDistrict(this.form.location[1].provinceId).subscribe(district => {
          this.districts2 = district;
        });

        this.driverservice.getAllSubdistrict(this.form.location[1].districtId).subscribe(subdistrict => {
          this.subdistricts2 = subdistrict;
        });
      }


    });

  }
  public getAllCountry() {
    this.driverservice.getAllCountry().subscribe(country => {
      this.countries = country;
    });

  }
  public getAllProvince(id) {
    id = id.split(' ').pop();
    this.provinces = [];
    this.driverservice.getAllProvince(id).subscribe(province => {
      this.provinces = province;


    });
    this.form.location[0].provinceId = null;
    this.form.location[0].districtId = null;
    this.form.location[0].subdistrictId = null;
    this.form.location[0].postCode = null;
    this.districts = [];
    this.subdistricts = [];

  }

  public getAllDistrict(id) {
    id = id.split(' ').pop();
    this.districts = [];
    this.driverservice.getAllDistrict(id).subscribe(district => {
      this.districts = district;
    });
    this.form.location[0].districtId = null;
    this.form.location[0].subdistrictId = null;
    this.form.location[0].postCode = null;
    this.subdistricts = [];

  }

  public getAllSubdistrict(id) {
    id = id.split(' ').pop();
    this.subdistricts = [];
    this.driverservice.getAllSubdistrict(id).subscribe(subdistrict => {
      this.subdistricts = subdistrict;
      this.form.location[0].postCode = this.subdistricts[0].postCode;

    });
    this.form.location[0].subdistrictId = null;
  }
  public getAllCountry2() {
    this.driverservice.getAllCountry().subscribe(country => {
      this.countries2 = country;
    });

  }
  public getAllProvince2(id) {
    id = id.split(' ').pop();
    this.provinces2 = [];
    this.driverservice.getAllProvince(id).subscribe(province => {
      this.provinces2 = province;
    });
    this.form.location[1].provinceId = null;
    this.form.location[1].districtId = null;
    this.form.location[1].postCode = null;
    this.form.location[1].subdistrictId = null;
    this.districts2 = [];
    this.subdistricts2 = [];

  }

  public getAllDistrict2(id) {
    id = id.split(' ').pop();
    this.districts2 = [];
    this.driverservice.getAllDistrict(id).subscribe(district => {
      this.districts2 = district;
    });
    this.form.location[1].districtId = null;
    this.form.location[1].subdistrictId = null;
    this.form.location[1].postCode = null;
    this.subdistricts2 = [];
  }

  public getAllSubdistrict2(id) {
    id = id.split(' ').pop();
    this.subdistricts2 = [];
    this.driverservice.getAllSubdistrict(id).subscribe(subdistrict => {
      this.subdistricts2 = subdistrict;
      this.form.location[1].postCode = this.subdistricts2[0].postCode;

    });
    this.form.location[1].subdistrictId = null;
  }
  public checkData() {

    if (this.driver.driverType == "corporate") {
      if (this.driver.corporate.length > 1) {
        if (this.form.location[0].alley != "" && this.form.location[0].alley != null && this.form.location[0].address != "" && this.form.location[0].address != null
          && this.form.location[0].road != "" && this.form.location[0].road && this.form.location[0].postCode != "" && this.form.location[0].postCode
          && this.form.location[1].alley != "" && this.form.location[1].alley != null && this.form.location[1].address != "" && this.form.location[1].address != null
          && this.form.location[1].road != "" && this.form.location[1].road && this.form.location[1].postCode != "" && this.form.location[1].postCode
          && this.form.location[0].countryId != null && this.form.location[0].countryId != "" && this.form.location[0].provinceId != null && this.form.location[0].provinceId != ""
          && this.form.location[0].subdistrictId != null && this.form.location[0].subdistrictId != "" && this.form.location[0].districtId != null && this.form.location[0].districtId != ""
          && this.form.location[1].countryId != null && this.form.location[1].countryId != "" && this.form.location[1].provinceId != null && this.form.location[1].provinceId != ""
          && this.form.location[1].subdistrictId != null && this.form.location[1].subdistrictId != "" && this.form.location[1].districtId != null && this.form.location[1].districtId != ""
        ) {
          return true;
        }
      } else {
        if (this.form.location[0].alley != "" && this.form.location[0].alley != null && this.form.location[0].address != "" && this.form.location[0].address != null
          && this.form.location[0].road != "" && this.form.location[0].road && this.form.location[0].postCode != "" && this.form.location[0].postCode
          && this.form.location[0].countryId != null && this.form.location[0].countryId != "" && this.form.location[0].provinceId != null && this.form.location[0].provinceId != ""
          && this.form.location[0].subdistrictId != null && this.form.location[0].subdistrictId != "" && this.form.location[0].districtId != null && this.form.location[0].districtId != ""

        ) {
          return true;
        } else {
          return false;
        }
      }
    } else if (this.driver.driverType == "personal") {
      if (this.form.location[0].alley != "" && this.form.location[0].alley != null && this.form.location[0].address != "" && this.form.location[0].address != null
        && this.form.location[0].road != "" && this.form.location[0].road && this.form.location[0].postCode != "" && this.form.location[0].postCode
        && this.form.location[0].countryId != null && this.form.location[0].countryId != "" && this.form.location[0].provinceId != null && this.form.location[0].provinceId != ""
        && this.form.location[0].subdistrictId != null && this.form.location[0].subdistrictId != "" && this.form.location[0].districtId != null && this.form.location[0].districtId != ""

      ) {
        return true;
      }
    } else {
      return false;
    }






  }
  public async updateDriverAddress() {
    this.isFormSubmit = true;
    if (this.checkData()) {
      const resp = await this.dialog.confirm("Are you sure to update ", "", false);
      if (resp) {
        this.driverservice.updateDriverAddress(this.form)
          .pipe(first())
          .subscribe({
            next: () => {
              this.router.navigate(['/driverprofile'], { relativeTo: this.route });
            }
          });
      }
    }
  }
}
