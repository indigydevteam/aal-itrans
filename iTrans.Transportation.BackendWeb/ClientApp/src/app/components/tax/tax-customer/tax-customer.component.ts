// import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { first } from 'rxjs/operators';
import { CustomerService } from 'src/app/services/customer/customer.service';
import { DialogService } from 'src/app/services/dialog/dialog.service';

@Component({
  selector: 'app-tax-customer',
  templateUrl: './tax-customer.component.html',
  styleUrls: ['./tax-customer.component.scss']
})
export class TaxCustomerComponent implements OnInit {

  // form!: FormGroup;
  id!: string;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private customerservice: CustomerService,
    private dialog: DialogService

  ) { }
  customer: any = [];
  countries: any = [];
  provinces: any = [];
  districts: any = [];
  subdistricts: any = [];
  countries2: any = [];
  provinces2: any = [];
  districts2: any = [];
  subdistricts2: any = [];
  displaybox = "";
  countryid: any;
  road = "";
  alley = "";
  address = "";
  postcode = "";
  addressId: any;
  country: any;
  province: any;
  district: any;
  subdistrict: any;
  isFormSubmit = false;

  form = {
    location: [
      {
        id: '0',
        customerId: '0',
        branch: null,
        countryId: null,
        provinceId: null,
        districtId: null,
        subdistrictId: null,
        address: null,
        road: null,
        alley: null,
        postCode: null,
        isMainData: true,
        addressType: null,
      }
    ],

  }

  ngOnInit(): void {
    this.id = this.route.snapshot.params.id;
    this.getCustomerTaxById();
    this.getAllCountry();
    this.getAllCountry2();

  }
  public getCustomerTaxById() {
    this.customerservice.getCustomerTaxById(this.id).subscribe(customer => {


      this.customer = customer.data;
      console.log(this.customer);


      if (this.customer.customerType == "corporate") {
        if (this.customer.corporate.length > 1) {
          this.form = {
            location: [
              {
                id: this.customer.corporate[0].id,
                customerId: this.id,
                branch: this.customer.corporate[0].branch,
                countryId: this.customer ? this.customer.corporate[0]?.country.id : null,
                provinceId: this.customer ? this.customer.corporate[0]?.province.id : null,
                districtId: this.customer ? this.customer.corporate[0].district.id : null,
                subdistrictId: this.customer ? this.customer.corporate[0].subdistrict.id : null,
                address: this.customer.corporate[0].address,
                road: this.customer.corporate[0].road,
                alley: this.customer.corporate[0].alley,
                postCode: this.customer.corporate[0].postCode,
                isMainData: this.customer.corporate[0].isMainData,
                addressType: this.customer.corporate[0].addressType,
              },
              {
                id: this.customer.corporate[1].id,
                customerId: this.id,
                branch: this.customer.corporate[1].branch,
                countryId: this.customer ? this.customer.corporate[1]?.country.id : null,
                provinceId: this.customer ? this.customer.corporate[1]?.province.id : null,
                districtId: this.customer ? this.customer.corporate[1].district.id : null,
                subdistrictId: this.customer ? this.customer.corporate[1].subdistrict.id : null,
                address: this.customer.corporate[1].address,
                road: this.customer.corporate[1].road,
                alley: this.customer.corporate[1].alley,
                postCode: this.customer.corporate[1].postCode,
                isMainData: this.customer.corporate[1].isMainData,
                addressType: this.customer.corporate[1].addressType,
              }
            ],
          }

        } else {
          this.form = {
            location: [
              {
                id: this.customer.corporate[0].id,
                customerId: this.id,
                branch: this.customer.corporate[0].branch,
                countryId: this.customer ? this.customer.corporate[0]?.country.id : null,
                provinceId: this.customer ? this.customer.corporate[0]?.province.id : null,
                districtId: this.customer ? this.customer.corporate[0].district.id : null,
                subdistrictId: this.customer ? this.customer.corporate[0].subdistrict.id : null,
                address: this.customer.corporate[0].address,
                road: this.customer.corporate[0].road,
                alley: this.customer.corporate[0].alley,
                postCode: this.customer.corporate[0].postCode,
                isMainData: this.customer.corporate[0].isMainData,
                addressType: this.customer.corporate[0].addressType,
              },
            ],
          }
        }

        console.log(this.form);
      }

      if (this.customer.customerType == "personal") {
        this.form = {
          location: [
            {
              id: this.customer.personal.id,
              customerId: this.id,
              branch: this.customer.personal.branch,
              countryId: this.customer.personal.country.id,
              provinceId: this.customer.personal.province.id,
              districtId: this.customer.personal.district.id,
              subdistrictId: this.customer.personal.subdistrict.id,
              address: this.customer.personal.address,
              road: this.customer.personal.road,
              alley: this.customer.personal.alley,
              postCode: this.customer.personal.postCode,
              isMainData: this.customer.personal.isMainData,
              addressType: this.customer.personal.addressType,
            }
          ],
        }

        console.log(this.form);
      }

      this.customerservice.getAllProvince(this.form.location[0].countryId).subscribe(province => {
        this.provinces = province;
      });
      this.customerservice.getAllDistrict(this.form.location[0].provinceId).subscribe(district => {
        this.districts = district;
      });

      this.customerservice.getAllSubdistrict(this.form.location[0].districtId).subscribe(subdistrict => {
        this.subdistricts = subdistrict;
      });
      if (this.customer.corporate.length > 1) {
        this.customerservice.getAllProvince(this.form.location[1].countryId).subscribe(province => {
          this.provinces2 = province;
        });
        this.customerservice.getAllDistrict(this.form.location[1].provinceId).subscribe(district => {
          this.districts2 = district;
        });

        this.customerservice.getAllSubdistrict(this.form.location[1].districtId).subscribe(subdistrict => {
          this.subdistricts2 = subdistrict;
        });
      }


    });

  }
  public getAllCountry() {
    this.customerservice.getAllCountry().subscribe(country => {
      this.countries = country;
    });

  }
  public getAllProvince(id) {
    id = id.split(' ').pop();
    this.provinces = [];
    this.customerservice.getAllProvince(id).subscribe(province => {
      this.provinces = province;


    });
    this.form.location[0].provinceId = null;
    this.form.location[0].districtId = null;
    this.form.location[0].subdistrictId = null;
    this.districts = [];
    this.subdistricts = [];
    this.form.location[0].postCode = null;

  }

  public getAllDistrict(id) {
    id = id.split(' ').pop();
    this.districts = [];
    this.customerservice.getAllDistrict(id).subscribe(district => {
      this.districts = district;
    });
    this.form.location[0].districtId = null;
    this.form.location[0].subdistrictId = null;
    this.form.location[0].postCode = null;
    this.subdistricts = [];

  }

  public getAllSubdistrict(id) {
    id = id.split(' ').pop();
    this.subdistricts = [];
    this.customerservice.getAllSubdistrict(id).subscribe(subdistrict => {
      this.subdistricts = subdistrict;
      this.form.location[0].postCode = this.subdistricts[0].postCode;

    });
    this.form.location[0].subdistrictId = null;
  }
  public getAllCountry2() {
    this.customerservice.getAllCountry().subscribe(country => {
      this.countries2 = country;
    });

  }
  public getAllProvince2(id) {
    id = id.split(' ').pop();
    this.provinces2 = [];
    this.customerservice.getAllProvince(id).subscribe(province => {
      this.provinces2 = province;
    });
    this.form.location[1].provinceId = null;
    this.form.location[1].districtId = null;
    this.form.location[1].subdistrictId = null;
    this.districts2 = [];
    this.subdistricts2 = [];
    this.form.location[1].postCode = null;

  }

  public getAllDistrict2(id) {
    id = id.split(' ').pop();
    this.districts2 = [];
    this.customerservice.getAllDistrict(id).subscribe(district => {
      this.districts2 = district;
    });
    this.form.location[1].districtId = null;
    this.form.location[1].subdistrictId = null;
    this.form.location[1].postCode = null;

    this.subdistricts2 = [];
  }

  public getAllSubdistrict2(id) {
    id = id.split(' ').pop();
    this.subdistricts2 = [];
    this.customerservice.getAllSubdistrict(id).subscribe(subdistrict => {
      this.subdistricts2 = subdistrict;
    this.form.location[1].postCode = this.subdistricts2[0].postCode;


    });
    this.form.location[1].subdistrictId = null;
  }

  public checkData() {

    if (this.customer.customerType == "corporate") {
      if (this.customer.corporate.length > 1) {
        if (this.form.location[0].alley != "" && this.form.location[0].alley != null && this.form.location[0].address != "" && this.form.location[0].address != null
          && this.form.location[0].road != "" && this.form.location[0].road && this.form.location[0].postCode != "" && this.form.location[0].postCode
          && this.form.location[1].alley != "" && this.form.location[1].alley != null && this.form.location[1].address != "" && this.form.location[1].address != null
          && this.form.location[1].road != "" && this.form.location[1].road && this.form.location[1].postCode != "" && this.form.location[1].postCode
          && this.form.location[0].countryId != null && this.form.location[0].countryId != "" && this.form.location[0].provinceId != null && this.form.location[0].provinceId != ""
          && this.form.location[0].subdistrictId != null && this.form.location[0].subdistrictId != "" && this.form.location[0].districtId != null && this.form.location[0].districtId != ""
          && this.form.location[1].countryId != null && this.form.location[1].countryId != "" && this.form.location[1].provinceId != null && this.form.location[1].provinceId != ""
          && this.form.location[1].subdistrictId != null && this.form.location[1].subdistrictId != "" && this.form.location[1].districtId != null && this.form.location[1].districtId != ""
        ) {
          return true;
        }
      } else {
        if (this.form.location[0].alley != "" && this.form.location[0].alley != null && this.form.location[0].address != "" && this.form.location[0].address != null
          && this.form.location[0].road != "" && this.form.location[0].road && this.form.location[0].postCode != "" && this.form.location[0].postCode
          && this.form.location[0].countryId != null && this.form.location[0].countryId != "" && this.form.location[0].provinceId != null && this.form.location[0].provinceId != ""
          && this.form.location[0].subdistrictId != null && this.form.location[0].subdistrictId != "" && this.form.location[0].districtId != null && this.form.location[0].districtId != ""

        ) {
          return true;
        } else {
          return false;
        }
      }
    } else if (this.customer.customerType == "personal") {
      if (this.form.location[0].alley != "" && this.form.location[0].alley != null && this.form.location[0].address != "" && this.form.location[0].address != null
        && this.form.location[0].road != "" && this.form.location[0].road && this.form.location[0].postCode != "" && this.form.location[0].postCode
        && this.form.location[0].countryId != null && this.form.location[0].countryId != "" && this.form.location[0].provinceId != null && this.form.location[0].provinceId != ""
        && this.form.location[0].subdistrictId != null && this.form.location[0].subdistrictId != "" && this.form.location[0].districtId != null && this.form.location[0].districtId != ""

      ) {

        return true;
      }
    }
       else if (this.customer.customerType == "corporate") {
        if (this.form.location[0].alley != "" && this.form.location[0].alley != null && this.form.location[0].address != "" && this.form.location[0].address != null
          && this.form.location[0].road != "" && this.form.location[0].road && this.form.location[0].postCode != "" && this.form.location[0].postCode
          && this.form.location[0].countryId != null && this.form.location[0].countryId != "" && this.form.location[0].provinceId != null && this.form.location[0].provinceId != ""
          && this.form.location[0].subdistrictId != null && this.form.location[0].subdistrictId != "" && this.form.location[0].districtId != null && this.form.location[0].districtId != ""

        ) {
          return true;
        }
      }
      else {
        return false;
      }

    
  }

  public async UpdateCustomerTaxById() {

    this.isFormSubmit = true;


    if (this.checkData()) {

      const resp = await this.dialog.confirm("Are you sure to update ", "", false);
      if (resp) {
        this.customerservice.UpdateCustomerTaxById(this.form)
          .pipe(first())
          .subscribe({
            next: () => {
              this.router.navigate(['/customerprofile'], { relativeTo: this.route });
            }
          });
      }
    }
  }
}
