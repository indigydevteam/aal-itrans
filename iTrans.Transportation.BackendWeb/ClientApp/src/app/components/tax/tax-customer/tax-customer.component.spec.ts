import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TaxCustomerComponent } from './tax-customer.component';

describe('TaxCustomerComponent', () => {
  let component: TaxCustomerComponent;
  let fixture: ComponentFixture<TaxCustomerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TaxCustomerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TaxCustomerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
