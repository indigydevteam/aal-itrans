import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WelcomeinformationListComponent } from './welcomeinformation-list.component';

describe('WelcomeinformationListComponent', () => {
  let component: WelcomeinformationListComponent;
  let fixture: ComponentFixture<WelcomeinformationListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WelcomeinformationListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WelcomeinformationListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
