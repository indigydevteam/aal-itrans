import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { WelcomeinformationList } from 'src/app/models/welcomeinformation';
import { WelcomeInformationService } from 'src/app/services/welcomeinformation/welcomeinformation.service';
import { DialogService } from 'src/app/services/dialog/dialog.service';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-welcomeinformation-list',
  templateUrl: './welcomeinformation-list.component.html',
  styleUrls: ['./welcomeinformation-list.component.scss']
})
export class WelcomeinformationListComponent implements OnInit {

  public welcomeinformations: Observable<WelcomeinformationList> | any;

  public pageSize: number = 10;
  public pageNumber: number = 1;
  public search: string = "";
  active: number;
  column: string;

  constructor(private welcomeinformationservice: WelcomeInformationService, private dialog: DialogService) { }
  
  ngOnInit(): void {
    this.all();
  }

  public getAllWelcomeInformation() {
    this.welcomeinformationservice.getAllWelcomeInformation(this.search, this.pageNumber, this.pageSize, this.column, this.active).subscribe(welcomeinformations => {
      console.log(welcomeinformations);
      this.welcomeinformations = welcomeinformations;
    });
  }

  all() {
    this.active = 1;
    this.column = "all";
    this.getAllWelcomeInformation();
  }

  orderBy(value) {
    if (value == this.column) {
      if (this.active == 1) {
        this.active = 2;
      } else if (this.active == 2) {
        this.active = 3;
      } else {
        this.active = 1;
      }
    } else {
      this.active = 2;
    }
    this.column = value;
    this.getAllWelcomeInformation();
  }

  public filter(search) {
    this.search = search;
    this.pageNumber = 1;
    this.getAllWelcomeInformation();
  }
  previous() {
    this.pageNumber--;
    this.getAllWelcomeInformation();
  }
  next() {
    this.pageNumber++;
    this.getAllWelcomeInformation();
  }
  selectedPage(event) {
    this.pageNumber = event;
    this.getAllWelcomeInformation();
  }

  async onDelete(id) {
    const resp = await this.dialog.confirm("Are you sure to delete ", "", false);
    if (resp) {
      this.welcomeinformationservice.DeleteWelcomeinformationById(id)
        .pipe(first())
        .subscribe(welcomeinformations => {
          this.welcomeinformations = welcomeinformations;
          if (this.welcomeinformations.data !== null)
            this.getAllWelcomeInformation();
        });
    }

  }

}
