import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { first } from 'rxjs/operators';
import { WelcomeInformationService } from 'src/app/services/welcomeinformation/welcomeinformation.service';
import { environment } from '../../../../environments/environment';

@Component({
  selector: 'app-manage-welcomeinformation',
  templateUrl: './manage-welcomeinformation.component.html',
  styleUrls: ['./manage-welcomeinformation.component.scss']
})
export class ManageWelcomeinformationComponent implements OnInit {

  config = {
    placeholder: '',
    tabsize: 2,
    height: '200px',
    uploadImagePath: '/api/upload',
    toolbar: [
      ['misc', ['codeview', 'undo', 'redo']],
      ['style', ['bold', 'italic', 'underline', 'clear']],
      ['fontsize', ['fontname', 'fontsize', 'color']],
      ['para', ['style', 'ul', 'ol', 'paragraph', 'height']],
    ],
    fontNames: ['Helvetica', 'Arial', 'Arial Black', 'Comic Sans MS', 'Courier New', 'Roboto', 'Times']
  }


  form!: FormGroup;
  id!: string;
  isAddMode!: boolean;
  submitted = false;
  image: any;
  uploadedFile: any;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private welcomeinformationservice: WelcomeInformationService
  ) { }
  ngOnInit(): void {

    this.id = this.route.snapshot.params.id;
    this.isAddMode = !this.id;
    this.form = this.formBuilder.group({
      Id: this.id,
      module: ['', Validators.required],
      title_TH: ['', Validators.required],
      title_ENG: ['', Validators.required],
      information_TH: ['', Validators.required],
      information_ENG: ['', Validators.required],
      sequence: ['', Validators.required],
      picture: [''],
      active: [true]
    }
    );
    if (!this.isAddMode) {
      this.welcomeinformationservice.getWelcomeinformationById(this.id)
        .pipe(first())
        .subscribe(x => {
          console.log(x);
          this.form.patchValue(x);
          this.form.value.picture = x.Picture != null ? environment.baseWebContentUrl + '/' + x.Picture : "";
          this.image = this.form.value.picture;
        });
    }
  }
  get formControls() { return this.form.controls; }

  private async createWelcomeInformation() {
    const formData = new FormData();
    formData.append('Module', this.form.value.module);
    formData.append('Title_TH', this.form.value.title_TH);
    formData.append('Information_TH', this.form.value.information_TH);
    formData.append('Title_ENG', this.form.value.title_ENG);
    formData.append('Information_ENG', this.form.value.information_ENG);
    formData.append('Sequence', this.form.value.sequence);
    formData.append('Active', this.form.value.active);
    if (this.uploadedFile)
      formData.append('Picture', this.uploadedFile);
    const resp = await this.welcomeinformationservice.createWelcomeinformation(formData);
    if (resp.succeeded) {
      this.router.navigateByUrl('welcomeinformation');
    }

    //this.welcomeinformationservice.createWelcomeinformation(this.form.value)
    //  .pipe(first())
    //  .subscribe({
    //    next: () => {
    //      this.router.navigate(['/welcomeinformation'], { relativeTo: this.route });
    //    }
    //  });
  }
  private async updateWelcomeInformation() {
    const formData = new FormData();
    formData.append('Id', this.id);
    formData.append('Module', this.form.value.module);
    formData.append('Title_TH', this.form.value.title_TH);
    formData.append('Information_TH', this.form.value.information_TH);
    formData.append('Title_ENG', this.form.value.title_ENG);
    formData.append('Information_ENG', this.form.value.information_ENG);
    formData.append('Sequence', this.form.value.sequence);
    formData.append('Active', this.form.value.active);
 
    if (this.uploadedFile)
      formData.append('Picture', this.uploadedFile);
    const resp = await this.welcomeinformationservice.updateWelcomeinformation(formData);
    if (resp.succeeded) {
      this.router.navigateByUrl('welcomeinformation');
    }
    //this.welcomeinformationservice.updateWelcomeinformation(this.form.value)
    //  .pipe(first())
    //  .subscribe({
    //    next: () => {
    //      this.router.navigate(['/welcomeinformation'], { relativeTo: this.route });
    //    }
    //  });
  }

  onSubmit() {
    this.submitted = true;
    if (this.form.invalid) {
      return;
    }

    if (this.isAddMode) {
      this.createWelcomeInformation();
    } else {
      this.updateWelcomeInformation();
    }
  }

  handleFileInput(text, files: FileList) {
    const file = files[0];
    if (text === 'image') {
      console.log(file);
      const reader = new FileReader();
      reader.onload = e => this.image = reader.result;
      reader.readAsDataURL(file);
      this.uploadedFile = file;
      //this.image = file;
    }
  }
}
