import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageWelcomeinformationComponent } from './manage-welcomeinformation.component';

describe('ManageWelcomeinformationComponent', () => {
  let component: ManageWelcomeinformationComponent;
  let fixture: ComponentFixture<ManageWelcomeinformationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ManageWelcomeinformationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageWelcomeinformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
