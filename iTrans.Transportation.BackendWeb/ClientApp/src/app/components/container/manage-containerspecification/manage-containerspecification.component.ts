import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { first } from 'rxjs/operators';
import { ContainerService } from 'src/app/services/container/container.service';

@Component({
  selector: 'app-manage-containerspecification',
  templateUrl: './manage-containerspecification.component.html',
  styleUrls: ['./manage-containerspecification.component.scss']
})
export class ManageContainerspecificationComponent implements OnInit   {
  form!: FormGroup;
  id!: string;
  isAddMode!: boolean;
  submitted = false;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private containerservice: ContainerService,
  ) { }
  ngOnInit(): void {

    this.id = this.route.snapshot.params.id;
    this.isAddMode = !this.id;
    this.form = this.formBuilder.group({
        Id: this.id,
        name_TH: ['', Validators.required],
        name_ENG: ['', Validators.required],
        sequence: ['', Validators.required],
        active: [true]
      }
    );
    if (!this.isAddMode) {
      this.containerservice.getContainerSpecificationById(this.id)
        .pipe(first())
        .subscribe(x => this.form.patchValue(x));

    }
  }
  get formControls() { return this.form.controls; }

  private createContainerSpecification() {

    this.containerservice.createContainerSpecification(this.form.value)
      .pipe(first())
      .subscribe({
        next: () => {
          this.router.navigate(['/containerspecification'], { relativeTo: this.route });
        }
      });
  }
  private updateContainerSpecification() {
    this.containerservice.updateContainerSpecification(this.form.value)
      .pipe(first())
      .subscribe({
        next: () => {
          this.router.navigate(['/containerspecification'], { relativeTo: this.route });
        }
      });
  }

  onSubmit() {
    this.submitted = true;
    if (this.form.invalid) {
      return;
    }
    

    if (this.isAddMode) {
      this.createContainerSpecification();
    } else {
      this.updateContainerSpecification();
    }

  }

}
