import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageContainerspecificationComponent } from './manage-containerspecification.component';

describe('ManageContainerspecificationComponent', () => {
  let component: ManageContainerspecificationComponent;
  let fixture: ComponentFixture<ManageContainerspecificationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ManageContainerspecificationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageContainerspecificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
