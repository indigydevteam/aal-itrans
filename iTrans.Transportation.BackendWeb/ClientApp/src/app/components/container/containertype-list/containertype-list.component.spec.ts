import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ContainertypeListComponent } from './containertype-list.component';

describe('ContainertypeListComponent', () => {
  let component: ContainertypeListComponent;
  let fixture: ComponentFixture<ContainertypeListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ContainertypeListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ContainertypeListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
