import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { first } from 'rxjs/operators';
import { ContainerTypeList } from 'src/app/models/container.model';
import { ContainerService } from 'src/app/services/container/container.service';
import { DialogService } from 'src/app/services/dialog/dialog.service';

@Component({
  selector: 'app-containertype-list',
  templateUrl: './containertype-list.component.html',
  styleUrls: ['./containertype-list.component.scss']
})
export class ContainertypeListComponent implements OnInit {
  public containers: Observable<ContainerTypeList> | any;

  public count: number = 0;
  public pageSize: number = 10;
  public pageNumber: number = 1;
  public search: string = "";
  constructor(
    private containersservice: ContainerService,
    private dialog: DialogService
    ) { }
    active: number ;
    column: string ;
    ngOnInit(): void  {
       this.all();
    }
    public getAllContainerType() {
      this.containersservice.getAllContainerType(this.search, this.pageNumber, this.pageSize, this.column, this.active).subscribe(containers => {
        this.containers = containers;
      });
    }
    all() {
      this.active = 1;
      this.column = "all";
      this.getAllContainerType();
    }
  
   
    orderBy(value) {
      if (value == this.column) {
        if (this.active == 1) {
          this.active = 2;
        } else if (this.active == 2) {
          this.active = 3;
        } else {
          this.active = 1;
        }
      } else {
        this.active = 2;
      }
      this.column = value;
      this.getAllContainerType();
    }
      
  public filter(search) {
    this.search = search;
    this.pageNumber = 1;
    this.getAllContainerType();
  }
    previous() {
      this.pageNumber--;
      this.getAllContainerType();
    }
     next() {
      this.pageNumber++;
      this.getAllContainerType();
    }
     selectedPage(event) {
      this.pageNumber = event;
      this.getAllContainerType();
    }
    async  onDelete(id){
      const resp = await this.dialog.confirm("Are you sure to delete ", "", false);
      if (resp) {
          this.containersservice.deleteContainerTypeById(id)
          .pipe(first())
          .subscribe(containers => {
            this.containers = containers;
            if(this.containers.data !== null)
            this.getAllContainerType();      
          });
      }
     
    }
  }
  
  