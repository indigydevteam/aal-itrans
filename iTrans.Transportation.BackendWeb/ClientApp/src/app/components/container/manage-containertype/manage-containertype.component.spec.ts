import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageCaontainertypeComponent } from './manage-containertype.component';

describe('ManageCaontainertypeComponent', () => {
  let component: ManageCaontainertypeComponent;
  let fixture: ComponentFixture<ManageCaontainertypeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ManageCaontainertypeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageCaontainertypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
