import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { first } from 'rxjs/operators';
import { ContainerService } from 'src/app/services/container/container.service';
@Component({
  selector: 'app-manage-containertype',
  templateUrl: './manage-containertype.component.html',
  styleUrls: ['./manage-containertype.component.scss']
})
export class ManageCaontainertypeComponent implements OnInit  {
  form!: FormGroup;
  id!: string;
  isAddMode!: boolean;
  submitted = false;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private containerservice: ContainerService,
  ) { }
  ngOnInit(): void {

    this.id = this.route.snapshot.params.id;
    this.isAddMode = !this.id;
    this.form = this.formBuilder.group({
        Id: this.id,
        name_TH: ['', Validators.required],
        name_ENG: ['', Validators.required],
        sequence: ['', Validators.required],
        active: [true]
      }
    );
    if (!this.isAddMode) {
      this.containerservice.getContainerTypeById(this.id)
        .pipe(first())
        .subscribe(x => this.form.patchValue(x));

    }
  }
  get formControls() { return this.form.controls; }

  private createContainerType() {

    this.containerservice.createContainerType(this.form.value)
      .pipe(first())
      .subscribe({
        next: () => {
          this.router.navigate(['/containertype'], { relativeTo: this.route });
        }
      });
  }
  private updateContainerType() {
    this.containerservice.updateContainerType(this.form.value)
      .pipe(first())
      .subscribe({
        next: () => {
          this.router.navigate(['/containertype'], { relativeTo: this.route });
        }
      });
  }

  onSubmit() {
    this.submitted = true;
    if (this.form.invalid) {
      return;
    }
    

    if (this.isAddMode) {
      this.createContainerType();
    } else {
      this.updateContainerType();
    }

  }

}
