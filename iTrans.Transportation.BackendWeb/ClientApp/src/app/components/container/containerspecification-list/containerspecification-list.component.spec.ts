import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ContainerspecificationListComponent } from './containerspecification-list.component';

describe('ContainerspecificationListComponent', () => {
  let component: ContainerspecificationListComponent;
  let fixture: ComponentFixture<ContainerspecificationListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ContainerspecificationListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ContainerspecificationListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
