import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { first } from 'rxjs/operators';
import { ContainerSpecification } from 'src/app/models/container.model';
import { ContainerService } from 'src/app/services/container/container.service';
import { DialogService } from 'src/app/services/dialog/dialog.service';

@Component({
  selector: 'app-containerspecification-list',
  templateUrl: './containerspecification-list.component.html',
  styleUrls: ['./containerspecification-list.component.scss']
})
export class ContainerspecificationListComponent implements OnInit  {
  public containers: Observable<ContainerSpecification> | any;

  public pageSize: number = 10;
  public pageNumber: number = 1;
  public search: string = "";
  constructor(
    private containersservice: ContainerService,
    private dialog: DialogService
    ) { }  
    
    active: number ;
    column: string ;
    ngOnInit(): void  {
       this.all();
    }
    public getAllContainerSpecification() {
      this.containersservice.getAllContainerSpecification(this.search, this.pageNumber, this.pageSize, this.column, this.active).subscribe(containers => {
        this.containers = containers;
      });
    }
    all() {
      this.active = 1;
      this.column = "all";
      this.getAllContainerSpecification();
    }
  
  
    orderBy(value) {
      if (value == this.column) {
        if (this.active == 1) {
          this.active = 2;
        } else if (this.active == 2) {
          this.active = 3;
        } else {
          this.active = 1;
        }
      } else {
        this.active = 2;
      }
      this.column = value;
      this.getAllContainerSpecification();
    }
  
    public filter() {
  
      this.pageNumber = 1;
      this.getAllContainerSpecification();
    }
    previous() {
      this.pageNumber--;
      this.getAllContainerSpecification();
    }
     next() {
      this.pageNumber++;
      this.getAllContainerSpecification();
    }
     selectedPage(event) {
      this.pageNumber = event;
      this.getAllContainerSpecification();
    }
    async  onDelete(id){
      const resp = await this.dialog.confirm("Are you sure to delete ", "", false);
      if (resp) {
          this.containersservice.deleteContainerSpecificationId(id)
          .pipe(first())
          .subscribe(containers => {
            this.containers = containers;
            if(this.containers.data !== null)
            this.getAllContainerSpecification();      
          });
      }
     
    }
  
  }
  
  