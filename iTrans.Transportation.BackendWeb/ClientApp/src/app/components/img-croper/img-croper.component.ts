import { Component, OnInit, ViewChild } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { ImageCroppedEvent } from 'ngx-image-cropper';
import { AlertService } from 'src/app/services/alert/alert.service';
import { EventService } from 'src/app/services/event/event.service';
import { UrlDetectService } from 'src/app/services/UrlDetect/url-detect.service';

@Component({
  selector: 'app-img-croper',
  templateUrl: './img-croper.component.html',
  styleUrls: ['./img-croper.component.scss']
})
export class ImgCroperComponent implements OnInit {

  @ViewChild('imageCroperModal', { static: false }) modal;
  modalRef: BsModalRef;
  configmodal = {
    class: 'modal-dialog-centered modal-lg',
    ignoreBackdropClick: true,
    keyboard: false
  };
  resovle: Function;
  imageChangedEvent: any = '';
  croppedImage: any = '';
  img: any;
  imageSrc: any;
  isCrop = false;
  selectFile: File;

  constructor(
    private ent: EventService,
    private modalService: BsModalService,
    private alert: AlertService,
    private utility: UrlDetectService
  ) { }

  ngOnInit() {
    this.ent.onImgCroperModalEvent.subscribe(async (data: any) => {
      this.resovle = data?.resovle;

      this.show();
    });
  }

  show() {
    this.isCrop = false;
    this.imageSrc = null;
    this.img = null;
    this.croppedImage = null;
    this.modalRef = this.modalService.show(this.modal, this.configmodal);
  }

  hide() {
    this.modalRef.hide();
  }


  fileChangeEvent(event: any): void {
    let size = (event.srcElement.files[0].size / 1000) / 1000;
    this.selectFile = event.srcElement.files[0];
    if (size >= 19.07) {
      this.img = null;
      this.alert.alert('You can only select image up to 19.07 MB in size');
      return;
    } else {
    
      this.imageChangedEvent = event;
      const reader = new FileReader();
      reader.onload = e => this.imageSrc = reader.result;
      reader.readAsDataURL(this.selectFile);
    }
  }

  imageCropped(event: ImageCroppedEvent) {
    try {

      this.croppedImage = this.isCrop ? event.base64 : this.imageSrc;

    } catch (err) {
      // console.log(err);
    }
  }

  imageLoaded() {
    // show cropper
  }

  cropperReady() {
    // cropper ready
  
  }

  loadImageFailed(e) {
    // show message
  }

  onConfirm() {
    if (this.isCrop) {
      this.hide();
      this.resovle({ base64String: this.croppedImage, selectFile: this.selectFile });
    } else {
      setTimeout(async () => {
        this.croppedImage = await this.utility.getBase64(this.selectFile);
        this.hide();
        this.resovle({ base64String: this.croppedImage, selectFile: this.selectFile });
      }, 100);
    }
  }

  onCancel() {
    this.img = null;
    this.croppedImage = null;
    this.hide();
  }

}
