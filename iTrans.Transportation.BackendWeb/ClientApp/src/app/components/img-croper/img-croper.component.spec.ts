import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ImgCroperComponent } from './img-croper.component';

describe('ImgCroperComponent', () => {
  let component: ImgCroperComponent;
  let fixture: ComponentFixture<ImgCroperComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ImgCroperComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ImgCroperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
