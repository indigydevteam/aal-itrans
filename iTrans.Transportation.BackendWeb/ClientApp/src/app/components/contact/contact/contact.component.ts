import { Component, OnInit } from '@angular/core';
import { ContactService } from 'src/app/services/contact/contact.service';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit{


  constructor(
    private contactservice: ContactService,
    ) { }
  carTypeName = "";
  data:any = [];
  contact:any=[];

  ngOnInit(): void {
    this.getAlltermscondition();

  }
  public getAlltermscondition() {
    this.contactservice.getAllContact().subscribe(contact => {
      this.contact = contact.data;

     
      console.log(this.contact);


    });
  }
  
 
}