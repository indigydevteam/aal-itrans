import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs/operators';
import { CustomerService } from 'src/app/services/customer/customer.service';
import { DialogService } from 'src/app/services/dialog/dialog.service';

@Component({
  selector: 'app-customerprofile-list',
  templateUrl: './customerprofile-list.component.html',
  styleUrls: ['./customerprofile-list.component.scss']
})
export class CustomerprofileComponent implements OnInit {
  public count: number = 0;
  public pageSize: number = 10;
  public pageNumber: number = 1;
  public search: string = "";
  public profilepicture: string = "";
  public customertype: string = "";

  customers: any = [];

  constructor(
    private customerservice: CustomerService,
    private dialog: DialogService
  ) { }
  active:number;
  column:string;
  ngOnInit(): void {
    this.all();
  }

  public getAllCustomerProfile() {
    this.customerservice.getAllCustomerProfile(this.search, this.customertype, this.pageNumber, this.pageSize, this.column, this.active).subscribe(customers => {
      this.customers = customers;
      console.log(this.customers);
      this.customers.files.filter(element => {
        if( element.documentType === "profilepicture"){
          this.profilepicture = element;
        } 
      });
    });
  }

  all() {
    this.active = 1;
    this.column = "all";
    this.getAllCustomerProfile();
  }
  orderBy(value) {
    if (value == this.column) {
      if (this.active == 1) {
        this.active = 2;
      } else if (this.active == 2) {
        this.active = 3;
      } else {
        this.active = 1;
      }
    } else {
      this.active = 2;
    }
    this.column = value;
    this.getAllCustomerProfile();
  }
  public filter() {
    this.pageNumber = 1;
    this.getAllCustomerProfile();
  }
  previous() {
    this.pageNumber--;
    this.getAllCustomerProfile();
  }
  next() {
    this.pageNumber++;
    this.getAllCustomerProfile();
  }
   selectedPage(event) {
    this.pageNumber = event;
    this.getAllCustomerProfile();
  }

  async onDelete(id) {
    const resp = await this.dialog.confirm("Are you sure to delete ", "", false);
    if (resp) {
        this.customerservice.DeleteCustomer(id)
        .pipe(first())
        .subscribe(customers => {
          this.customers = customers;
          if(this.customers.data !== null)
          this.getAllCustomerProfile();      
        });
    }
   
  }


}