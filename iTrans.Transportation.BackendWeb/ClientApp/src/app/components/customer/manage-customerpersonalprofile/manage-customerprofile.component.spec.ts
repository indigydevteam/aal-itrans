import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ManageCustomerprofileComponent } from './manage-customerprofile.component';


describe('ManageCustomerprofileComponent', () => {
  let component: ManageCustomerprofileComponent;
  let fixture: ComponentFixture<ManageCustomerprofileComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ManageCustomerprofileComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageCustomerprofileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
