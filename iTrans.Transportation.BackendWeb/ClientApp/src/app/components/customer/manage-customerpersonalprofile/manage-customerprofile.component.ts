import { DatePipe } from '@angular/common';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbDatepicker } from '@ng-bootstrap/ng-bootstrap';
import * as moment from 'moment';
import { DatepickerOptions } from 'ng2-datepicker';
import { first, flatMap } from 'rxjs/operators';
import { CorporateTypeService } from 'src/app/services/corporatetype/corporatetype';
import { CustomerService } from 'src/app/services/customer/customer.service';
import { DialogService } from 'src/app/services/dialog/dialog.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-manage-customerprofile',
  templateUrl: './manage-customerprofile.component.html',
  styleUrls: ['./manage-customerprofile.component.scss']
})
export class ManageCustomerprofileComponent implements OnInit {
  @ViewChild('dp') dp: NgbDatepicker;
  options: DatepickerOptions = {
    
    // scrollBarColor: '#ffffff',
    format: 'dd/MM/yyyy', // date format to display in inputม
    position: 'bottom',
    inputClass: 'form-date icon',
    
  };
  id!: string;
  submitted = false;
  date: string;
  termCondition: any = [];
  contactpersontitles: any = [];
  customerlevels: any = [];
  customer: any = [];
  countries: any = [];
  provinces: any = [];
  districts: any = [];
  subdistricts: any = [];
  road = "";
  alley = "";
  address = "";
  postcode = "";
  addressId: any;
  country: any;
  province: any;
  district: any;
  subdistrict: any;
  displaybox = "";
  corporateType: any = [];
  isFormSubmit = false;
  isformatphone = true;
  isformatidentityNumber= true;
  isformatemail= true;


  form = {
    id: '0',
    wallet: null,
    titleId: null,
    corporateTypeId: null,
    contactPersonFirstName: null,
    contactPersonLastName: null,
    contactPersonTitleId: null,
    name: null,
    firstName: null,
    middleName: null,
    lastName: null,
    birthday: new Date(),
    identityNumber: null,
    phoneNumber: null,
    phoneCode: null,
    email: null,
    level: null,
    countryId: null,
    provinceId: null,
    districtId: null,
    subdistrictId: null,
    address: null,
    road: null,
    alley: null,
    postcode: null,
    facbook: null,
    line: null,
    twitter: null,
    whatapp: null,
    wechat: null,
    verifyStatus: null,
    status:null,
    isPassVerifyDoc: false,
    isNotPassVerifyDoc: false,
    imageprofile:null
  }
  image = null;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private customerservice: CustomerService,
    private corporateservice: CorporateTypeService,
    private dialog: DialogService

  ) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.params.id;

    this.getCustomerProfileById();
    this.getAllUserLevel();
    this.getAllCountry();
    this.getAllContactPersonTitle();
    this.getTermCondition();
    this.getAllCorporateType();

  }


  public getCustomerProfileById() {
    this.customerservice.getCustomerProfileById(this.id).subscribe(customer => {
      if (customer.succeeded) {
        this.customer = customer.data;
        console.log(this.customer);
        if (this.customer.customerType == "corporate") {
          this.displaybox = "color-box";
        }

        this.form = {
          id: this.customer.id,
          wallet: null,
          titleId: this.customer.title ? this.customer.title.id : null,
          corporateTypeId: this.customer.corporateTypeId,
          contactPersonFirstName: this.customer.contactPersonFirstName,
          contactPersonLastName: this.customer.contactPersonLastName,
          name: this.customer.name,
          contactPersonTitleId: this.customer.contactPersonTitle ? this.customer.contactPersonTitle.id : null,
          firstName: this.customer.firstName,
          middleName: this.customer.middleName,
          lastName: this.customer.lastName,
          birthday:  new Date(moment(this.customer.birthday).format("")),
          identityNumber: this.customer.identityNumber,
          phoneNumber: this.customer.phoneNumber,
          phoneCode: this.customer.phoneCode,
          email: this.customer.email,
          level: this.customer.customerLevel ? this.customer.customerLevel.level : null,
          countryId: this.customer.addresses.length > 0 ? (this.customer.addresses[0].country ? this.customer.addresses[0].country.id : null) : null,
          provinceId: this.customer.addresses.length > 0 ? (this.customer.addresses[0].province ? this.customer.addresses[0].province.id : null) : null,
          districtId: this.customer.addresses.length > 0 ? (this.customer.addresses[0].district ? this.customer.addresses[0].district.id : null) : null,
          subdistrictId: this.customer.addresses.length > 0 ? (this.customer.addresses[0].subdistrict ? this.customer.addresses[0].subdistrict.id : null) : null,
          address: this.customer.addresses.length > 0 ? this.customer.addresses[0].address : null,
          road: this.customer.addresses.length > 0 ? this.customer.addresses[0].road : null,
          alley: this.customer.addresses.length > 0 ? this.customer.addresses[0].alley : null,
          postcode: this.customer.addresses.length > 0 ? this.customer.addresses[0].postCode : null,
          facbook: this.customer.facbook,
          line: this.customer.line,
          twitter: this.customer.twitter,
          whatapp: this.customer.whatapp,
          wechat: this.customer.wechat,
          verifyStatus: this.customer.verifyStatus,
          status: this.customer.status,
          isPassVerifyDoc: this.customer.verifyStatus == 2 ? true : false,
          isNotPassVerifyDoc: this.customer.verifyStatus == 1 ? true : false,
          imageprofile: environment.baseWebContentUrl +'/'+ this.setImgProfile()
        }
        this.image = this.form.imageprofile;

        this.customerservice.getAllProvince(this.form.countryId).subscribe(province => {
          this.provinces = province;
        });
        this.customerservice.getAllDistrict(this.form.provinceId).subscribe(district => {
          this.districts = district;
        });
        this.customerservice.getAllSubdistrict(this.form.districtId).subscribe(subdistrict => {
          this.subdistricts = subdistrict;
        });



      }
    });
  }
  setImgProfile(){
   
    this.image = this.customer.files.filter(
      file=>file.documentType =="profilepicture"
    )
    if(this.image.length>0){
      return this.image[0].filePath;

    }else{
      return "";
    }
    
  }
  
  public getAllUserLevel() {
    this.customerservice.getAllUserLevel(this.id).subscribe(level => {
      this.customerlevels = level;
    });
  }
  
  public getAllCorporateType() {
    this.corporateservice.getAllCorporateTypes().subscribe(corporateType => {
      this.corporateType = corporateType;
    });
  }

  public getAllContactPersonTitle() {
    this.customerservice.getAllContactPersonTitle().subscribe(title => {
      this.contactpersontitles = title;
    });
  }

  public getAllCountry() {
    this.customerservice.getAllCountry().subscribe(country => {
      this.countries = country;
    });
   
  }

  public getAllProvince(id) {
    id = id.split(' ').pop();
    this.provinces = [];
    this.customerservice.getAllProvince(id).subscribe(province => {
      this.provinces = province;
    });
    this.form.provinceId =null;
    this.form.districtId =null;
    this.form.subdistrictId =null;
    this.provinces = [];
    this.districts = [];
    this.subdistricts = [];
    this.form.postcode =null;

    }

  public getAllDistrict(id) {
    id = id.split(' ').pop();
    this.districts = [];
    this.customerservice.getAllDistrict(id).subscribe(district => {
      this.districts = district;
    });
    this.form.districtId =null;
    this.form.subdistrictId =null;
    this.districts = [];
    this.subdistricts = [];  
    this.form.postcode =null;

  }

  public getAllSubdistrict(id) {
    id = id.split(' ').pop();

    this.subdistricts = [];

    this.customerservice.getAllSubdistrict(id).subscribe(subdistrict => {
      this.subdistricts = subdistrict;
      console.log(this.subdistricts);
      this.form.postcode =this.subdistricts[0].postCode;

    });
   
    this.form.subdistrictId =null;

  
  }
  
  public checkidentityNumber() {

    if(this.form.identityNumber.length == 13){
     this.isformatidentityNumber = true;
     return true;
    }else{
      this.isformatidentityNumber = false;
     return false;

    } 
  }
  public checkPhone() {
    // if (/^\w+([\.-]?\w+)*@@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail.trim()))
    // this.form.phoneNumber = this.form.phoneNumber.replace(/[^0-9]/g, '');
    if(/^0([8|9|6])[0-9]+$/ig.test(this.form.phoneNumber) && this.form.phoneNumber.length == 10){
     this.isformatphone = true;
     return true;
    }else{
      this.isformatphone = false;
     return false;

    } 
  }
  public checkEmail() {
    if(/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(this.form.email)){
     this.isformatemail = true;
     return true;
    }else{
      this.isformatemail = false;
     return false;

    } 
  }

  public async updateCustomer() {
    
    // var check = this.checkPhone();
    // var identityNumber = this.checkidentityNumber();
    var email = this.checkEmail();
    
    
    

    if(this.form.name != "" && this.form.name != null && this.customer.customerType == "corporate"){
      this.isFormSubmit = true;
    } else if( this.customer.customerType == "personal"){
      this.isFormSubmit = true;
    }
    else{
    this.isFormSubmit = false;
    }

    if (this.form.isPassVerifyDoc) {
      this.form.verifyStatus = 2;
    }
    if (this.form.isNotPassVerifyDoc) {
      this.form.verifyStatus = 1;
    }
    if(!this.form.isPassVerifyDoc && !this.form.isNotPassVerifyDoc)
    {
      this.form.verifyStatus = 0;
    }
   
    if (
      this.form.alley != "" && this.form.alley != null && this.form.address != "" && this.form.address != null
      && this.form.road != "" && this.form.road && this.form.postcode != "" && this.form.postcode
      && this.form.districtId != "" && this.form.districtId && this.form.subdistrictId != "" && this.form.subdistrictId
      && this.form.countryId != "" && this.form.countryId && this.form.firstName != "" && this.form.firstName && this.form.lastName != "" && this.form.lastName
      && this.form.level != "" && this.form.level &&  this.form.email != "" && this.form.email &&  this.form.identityNumber != "" && this.form.identityNumber 
      &&  email == true && this.isFormSubmit == true &&  (this.form.verifyStatus == 1 || this.form.verifyStatus == 2)
    ) {
      
      const resp = await this.dialog.confirm("Are you sure to update ", "", false);

      if (resp) {
        const formData = new FormData();
        formData.append("Id", this.form.id);
        formData.append("CorporateTypeId", this.form.corporateTypeId ? this.form.corporateTypeId : 0);
        formData.append("TitleId", this.form.titleId ? this.form.titleId : 0);
        formData.append("FirstName", this.form.firstName ? this.form.firstName : " ");
        formData.append("MiddleName", this.form.middleName ? this.form.middleName : " ");
        formData.append("LastName", this.form.lastName ? this.form.lastName : "");
        formData.append("IdentityNumber", this.form.identityNumber ? this.form.identityNumber : " ");
        const birthday = moment(this.form.birthday).format('YYYY/MM/DD')
        formData.append("Birthday", birthday);
        formData.append("PhoneNumber", this.form.phoneNumber ? this.form.phoneNumber : " ");
        formData.append("Email", this.form.email ? this.form.email : " ");
        formData.append("Facbook", this.form.facbook ? this.form.facbook : " ");
        formData.append("Line", this.form.line ? this.form.line : " ");
        formData.append("Twitter", this.form.twitter ? this.form.twitter : " ");
        formData.append("Whatapp", this.form.whatapp ? this.form.whatapp : " " );
        formData.append("Wechat", this.form.wechat ? this.form.wechat : " " );
        formData.append("Level", this.form.level ? this.form.level : 0);
        formData.append("CountryId", this.form.countryId ? this.form.countryId : 0);
        formData.append("ProvinceId", this.form.provinceId ? this.form.provinceId : 0);
        formData.append("DistrictId", this.form.districtId ? this.form.districtId : 0);
        formData.append("SubdistrictId", this.form.subdistrictId ? this.form.subdistrictId : 0);
        formData.append("PostCode", this.form.postcode ? this.form.postcode : " ");
        formData.append("Road", this.form.road ? this.form.road : " ");
        formData.append("Alley", this.form.alley ? this.form.alley : " ");
        formData.append("Address", this.form.address ? this.form.address : "");
        formData.append("ContactPersonTitleId", this.form.contactPersonTitleId ? this.form.contactPersonTitleId : 0);
        formData.append("ContactPersonFirstName", this.form.contactPersonFirstName ? this.form.contactPersonFirstName : " ");
        formData.append("ContactPersonLastName", this.form.contactPersonLastName ? this.form.contactPersonLastName : " ");
        // formData.append("Name", (this.form.firstName ? this.form.firstName : "") + (this.form.middleName ? " " + this.form.middleName + " " : " ") + (this.form.lastName ? this.form.lastName : ""));
        formData.append("Name", this.form.name );
        formData.append("VerifyStatus", this.form.verifyStatus);
        formData.append("Status", this.form.status);
        formData.append("ProfilePicture", this.form.imageprofile );

        
        this.customerservice.updateCustomer(formData)
          .pipe(first())
          .subscribe({
            next: () => {
              this.router.navigate(['/customerprofile'], { relativeTo: this.route });
            }
          });
      }
    }
  }

  btnClick = function () {
    this.router.navigateByUrl('/taxcustomer/' + this.id);
  };

  isPassVerifyDocClick = function () {
    if (this.form.isPassVerifyDoc) {
      this.form.isPassVerifyDoc = false;
    }
    else {
      this.form.isPassVerifyDoc = true;
    }
    this.form.isNotPassVerifyDoc = false;
  };
  isNotPassVerifyDocClick = function () {
    if (this.form.isNotPassVerifyDoc) {
      this.form.isNotPassVerifyDoc = false;
    }
    else {
      this.form.isNotPassVerifyDoc = true;
    }
    this.form.isPassVerifyDoc = false;
  };

  public getTermCondition() {
    this.customerservice.getTermCondition(this.id).subscribe(res => {
      this.termCondition = res.data;
    });
  }
  handleFileInput(text, files: FileList) {
    const file = files[0];
    if (text === 'image') {
      const reader = new FileReader();
      reader.onload = e => this.image = reader.result;
      reader.readAsDataURL(file);
      this.form.imageprofile = file;
    } 
  }
}
