import { Component, OnInit } from '@angular/core';
import { CustomerService } from 'src/app/services/customer/customer.service';
import { DeliveryService } from 'src/app/services/delivery/delivery.service';
import { DriverService } from 'src/app/services/driver/driver.service';
import { OrderingService } from 'src/app/services/ordering/ordering.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {



  public barChartOptions = {
    responsive: true,

  };
  public barChartLabels =  [' ','ม.ค.', 'ก.พ.', 'มี.ค.', 'เม.ษ.', 'พ.ค.', 'มิ.ย.', 'ก.ค.', 'ส.ค.', 'ก.ย.', 'ต.ค.', 'พ.ย.', 'ธ.ค.'];
  public barChartType = 'bar';
  public barChartLegend = true;
  public barChartData = [];

  public count: number = 0;
  public pageSize: number = 5;
  public pageNumber: number = 1;

  public search: string = "";
  public status: number = 99;

  public selecttype: string = "M";



  constructor(
    private deliveryservice: DeliveryService,
    private driverservice: DriverService,
    private customerservice: CustomerService,
    private orderingservice: OrderingService
  ) { }
  ordering: any = [];
  driver: any = [];
  customer: any = [];
  provinces: any = [];
  region: any = [];
  type: any = [];
  chartdata: any = [];
  myChart: any = [];





  ngOnInit(): void {
    this.GetOrderingChart();
    this.getAllDelivery();
    this.GetCustomerByVerifyStatus();
    this.GetDriverByVerifyStatus();
    this.GetOrderingProvincesPopular();
    this.GetOrderingRegionsPopular();
    this.GetCountOrderingByType();
  }
  public GetOrderingChart() {
    this.orderingservice.GetOrderingChart(this.selecttype).subscribe(chartdata => {
      this.chartdata = chartdata.data;

      this.barChartData = [
        {
          data: this.chartdata.sucess,
          label: 'ง่านเสร็จสิ้น',
          backgroundColor: '#3AC95D',
          hoverBackgroundColor: '#3AC95D',
          borderColor: '#3AC95D',
        },
        {
          data: this.chartdata.failed,
          label: 'งานไม่สำเร็จ',
          backgroundColor: '#E42222',
          hoverBackgroundColor: '#E42222',
          borderColor: '#E42222'
        },


      ];
      if(this.chartdata.type =="D"){
        this.barChartLabels = this.chartdata.date;
      }
       
    });
  }
  public GetCustomerByVerifyStatus() {
    this.customerservice.GetCustomerByVerifyStatus().subscribe(customer => {
      this.customer = customer.data;
    });
  }
  public GetDriverByVerifyStatus() {
    this.driverservice.GetDriverByVerifyStatus().subscribe(driver => {
      this.driver = driver.data;
    });
  }
  public getAllDelivery() {
    this.deliveryservice.getAllDelivery(this.search, this.status, this.pageNumber, this.pageSize,"",1).subscribe(ordering => {
      this.ordering = ordering;
      this.count = ordering.data.itemCount;
    });
  }
  public filterChart() {
    this.GetOrderingChart();
  }

  public filter() {
    this.pageNumber = 1;
    this.getAllDelivery();
  }

  previous() {
    this.pageNumber--;
    this.getAllDelivery();
  }
  next() {
    this.pageNumber++;
    this.getAllDelivery();
  }
  selectedPage(event) {
    this.pageNumber = event;
    this.getAllDelivery();
  }
  public GetOrderingProvincesPopular() {
    this.orderingservice.GetOrderingProvincesPopular().subscribe(provinces => {
      this.provinces = provinces.data;
    });
  }
  public GetOrderingRegionsPopular() {
    this.orderingservice.GetOrderingRegionsPopular().subscribe(region => {
      this.region = region.data;
    });
  }
  public GetCountOrderingByType() {
    this.orderingservice.GetCountOrderingByType().subscribe(type => {
      this.type = type.data;
    });
  }


}
