import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { DeliveryService } from 'src/app/services/delivery/delivery.service';

@Component({
  selector: 'app-delivery-list',
  templateUrl: './delivery-list.component.html',
  styleUrls: ['./delivery-list.component.scss']
})
export class DeliveryListComponent implements OnInit {
 
  public pageSize: number = 10;
  public pageNumber: number = 1;
  public search: string = "";
  public status:number=99;

  constructor(
    private deliveryservice: DeliveryService) 
    { }
  ordering:any=[];
  active:number;
  column:string;
  ngOnInit(): void {
    this.all();
  }

  public getAllDelivery() {
    this.deliveryservice.getAllDelivery(this.search, this.status, this.pageNumber, this.pageSize, this.column, this.active).subscribe(ordering => {
      this.ordering = ordering;
    });
  }
  all() {
    this.active = 1;
    this.column = "all";
    this.getAllDelivery();
  }
  orderBy(value) {
    if (value == this.column) {
      if (this.active == 1) {
        this.active = 2;
      } else if (this.active == 2) {
        this.active = 3;
      } else {
        this.active = 1;
      }
    } else {
      this.active = 2;
    }
    this.column = value;
    this.getAllDelivery();
  }
  
  public filter() {
    this.pageNumber = 1;
    this.getAllDelivery();
  }

   previous() {
    this.pageNumber--;
    this.getAllDelivery();
  }
   next() {
    this.pageNumber++;
    this.getAllDelivery();
  }
   selectedPage(event) {
    this.pageNumber = event;
    this.getAllDelivery();
  }
  
}
