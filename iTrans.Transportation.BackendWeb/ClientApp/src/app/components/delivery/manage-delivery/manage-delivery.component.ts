import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DeliveryService } from 'src/app/services/delivery/delivery.service';
import { environment } from '../../../../environments/environment';
import Swal from 'sweetalert2'
import { first } from 'rxjs/operators';
import { OrderingAddressStatusForUpdate, OrderingStatusForUpdate } from '../../../models/ordering.model';
import { OrderingService } from '../../../services/ordering/ordering.service';

@Component({
  selector: 'app-manage-delivery',
  templateUrl: './manage-delivery.component.html',
  styleUrls: ['./manage-delivery.component.scss']
})
export class ManageDeliveryComponent implements OnInit {

  id!: string;
  public count: number = 0;


  public page: number = 0;
  public pageSize: number = 5;
  public pageNumber: number = 1;
  public search: string = "";
  public env: any = environment;
  orderings: any = [];
  yearregistration: any;
  image: any = [];
  orderingaddress: any = [];
  orderingStatus: any = [];
  orderingAddressStatus: any = [];
  orderingCancelStatus: any = [];

  constructor(
    private route: ActivatedRoute,
    private deliveryservice: DeliveryService,
    private orderingService: OrderingService
  ) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.params.id;
    this.getDeliveryById();
    this.getAllOrderingAddressByOrderingId();
    this.deliveryservice.getOrderingStatus().subscribe(orderingStatus => {
      this.orderingStatus = orderingStatus.data;
    });
    this.deliveryservice.getOrderingAddressStatus().subscribe(orderingAddressStatuses => {
      this.orderingAddressStatus = orderingAddressStatuses.data;
    });

    this.orderingService.getOrderingCancelStatuses().subscribe(orderingCancelStatuses => {
      this.orderingCancelStatus = orderingCancelStatuses;
    });
  }

  public getDeliveryById() {
    this.deliveryservice.getDeliveryById(this.id).subscribe(orderings => {
      this.orderings = orderings.data;
      this.orderings.products.forEach((res: any) => {
        if (this.image.length < 3) {
          this.image = this.image.concat(res.files);
        }
      });
    });
  }

  public getAllOrderingAddressByOrderingId() {
    this.deliveryservice.getAllOrderingAddressByOrderingId(this.id, this.search, this.pageNumber, this.pageSize).subscribe(orderingaddress => {
      this.orderingaddress = orderingaddress;
      console.log(this.orderingaddress);
    });
  }

  public filter() {
    this.pageNumber = 1;
    this.getAllOrderingAddressByOrderingId();
  }

  previous() {
    this.pageNumber--;
    this.getAllOrderingAddressByOrderingId();
  }
  next() {
    this.pageNumber++;
    this.getAllOrderingAddressByOrderingId();
  }

  selectedPage(event) {
    this.pageNumber = event;
    this.getAllOrderingAddressByOrderingId();
  }
  public async onChangeStatus() {
    var selectItem = {};
    var cancelStatus = {};
    this.orderingCancelStatus.forEach(function (item) {
      cancelStatus["cal_" + item.id] = item.name_TH;
    });
    this.orderingStatus.forEach(function (item) {
      if (item.id == 15) {
        selectItem[item.th] = cancelStatus;
      } else {
        selectItem[item.id] = item.th;
      }
    });
    Swal.fire({
      title: 'เลือกสถานะ',
      input: 'select',
      inputOptions: selectItem,
      inputPlaceholder: 'เลือกสถานะ',
      showCancelButton: true,
      inputValidator: (value) => {
        return new Promise((resolve) => {
          if (value === '') {
            resolve('You need to select status :)')
          } else {
            try {
              var statusId: string = value;
              var cancelStatus: string = "0";
              if (statusId.search("cal_") >= 0) {
                cancelStatus = statusId.substring(4);
                statusId = "15";
              }
              var ordering: OrderingStatusForUpdate = {
                OrderingId: this.id,
                StatusId: statusId,
                CancelStatus: cancelStatus
              };
              this.deliveryservice.updateOrderingStatus(ordering)
                .pipe(first())
                .subscribe({
                  next: () => {
                    window.location.reload();
                    //this.router.navigate(['/cartype'], { relativeTo: this.route });
                  }
                });
            }
            catch (e) {
              console.log(e);
            }
            resolve('')
          }
        })
      }
    })

  }

  public async onChangeAddressStatus(addressid, addressType) {
    console.log(this.orderingAddressStatus);
    var selectItem = {};
    this.orderingAddressStatus.forEach(function (item) {
      if (addressType == "pickuppoint") {
        if (item.id <= 8)
        selectItem[item.id] = item.th;
      }
      else if (addressType == "deliverypoint") {
        if (item.id <= 9)
        selectItem[item.id] = item.th;
      }
    });
    Swal.fire({
      title: 'เลือกสถานะ',
      input: 'select',
      inputOptions: selectItem,
      inputPlaceholder: 'เลือกสถานะ',
      showCancelButton: true,
      inputValidator: (value) => {
        return new Promise((resolve) => {
          if (value === '') {
            resolve('You need to select status :)')
          } else {
            try {
              var statusId: string = value;
              var cancelStatus: string = "0";
              if (statusId.search("cal_") >= 0) {
                cancelStatus = statusId.substring(4);
                statusId = "15";
              }
              var ordering: OrderingAddressStatusForUpdate = {
                OrderingId: this.id,
                OrderAddressingId: addressid,
                StatusId: statusId,
                CancelStatus: cancelStatus
              };
              this.deliveryservice.updateOrderingAddressStatus(ordering)
                .pipe(first())
                .subscribe({
                  next: () => {
                    window.location.reload();
                    //this.router.navigate(['/cartype'], { relativeTo: this.route });
                  }
                });
            }
            catch (e) {
              console.log(e);
            }
            resolve('')
          }
        })
      }
    })

  }
}
