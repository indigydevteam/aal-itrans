import { Component, OnInit } from '@angular/core';
import { DialogService } from 'src/app/services/dialog/dialog.service';
import { HelpService } from 'src/app/services/help/help.service';

@Component({
  selector: 'app-personalprivacy-list',
  templateUrl: './personalprivacy-list.component.html',
  styleUrls: ['./personalprivacy-list.component.scss']
})
export class PersonalprivacyListComponent implements OnInit {


  public pageSize: number = 10;
  public pageNumber: number = 1;
  public search: string = "";
  constructor(
    private helpservice: HelpService,
    private dialog: DialogService
    ) { }
  carTypeName = "";
  data:any = [];
  module:string="pd/pa";
  active:number;
  column:string;
  personalprivacys:any=[];

  ngOnInit(): void {
    this.all();

  }
  public getAllpersonalprivacy() {
    this.personalprivacys=[];
    this.helpservice.getAllTermsConditions(this.search, this.pageNumber, this.pageSize, this.column, this.active,this.module).subscribe(data => {
      this.personalprivacys = data;
    });
  }
   
  all() {
    this.active = 1;
    this.column = "all";
    this.getAllpersonalprivacy();
  }

  orderBy(value) {
    if (value == this.column) {
      if (this.active == 1) {
        this.active = 2;
      } else if (this.active == 2) {
        this.active = 3;
      } else {
        this.active = 1;
      }
    } else {
      this.active = 2;
    }
    this.column = value;
    this.getAllpersonalprivacy();
  }
  
  public filter(search) {
    this.search = search;
    this.pageNumber = 1;
    this.getAllpersonalprivacy();
  }
  
  previous() {
    this.pageNumber--;
    this.getAllpersonalprivacy();
  }
   next() {
    this.pageNumber++;
    this.getAllpersonalprivacy();
  }
   selectedPage(event) {
    this.pageNumber = event;
    this.getAllpersonalprivacy();
  }
  
  
}
