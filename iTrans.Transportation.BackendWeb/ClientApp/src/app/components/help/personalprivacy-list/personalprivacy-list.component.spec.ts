import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonalprivacyListComponent } from './personalprivacy-list.component';

describe('PersonalprivacyListComponent', () => {
  let component: PersonalprivacyListComponent;
  let fixture: ComponentFixture<PersonalprivacyListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PersonalprivacyListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonalprivacyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
