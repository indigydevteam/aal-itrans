import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs/operators';
import { DialogService } from 'src/app/services/dialog/dialog.service';
import { HelpService } from 'src/app/services/help/help.service';

@Component({
  selector: 'app-complain',
  templateUrl: './complain.component.html',
  styleUrls: ['./complain.component.scss']
})
export class ComplainComponent implements OnInit {

  public count: number = 0;
  public pageSize: number = 10;
  public pageNumber: number = 1;
  public search: string = "";
  complains:any=[];
  constructor(
    private helpservice: HelpService,
    private dialog: DialogService
    ) { }


    column:string;
    active:number;

  ngOnInit(): void {
    this.all();

  }
  public getAllComplain() {
    this.helpservice.getAllComplain(this.search, this.pageNumber, this.pageSize, this.column, this.active).subscribe(complains => {
      this.complains = complains;
      this.count = complains.data.itemCount;
      console.log(this.complains);

    });
  }
  all() {
    this.active = 1;
    this.column = "all";
    this.getAllComplain();
  }

  orderBy(value) {
    if (value == this.column) {
      if (this.active == 1) {
        this.active = 2;
      } else if (this.active == 2) {
        this.active = 3;
      } else {
        this.active = 1;
      }
    } else {
      this.active = 2;
    }
    this.column = value;
    this.getAllComplain();
  }
  
  public filter() {
    this.pageNumber = 1;
    this.getAllComplain();
  }
  
  previous() {
    this.pageNumber--;
    this.getAllComplain();
  }
   next() {
    this.pageNumber++;
    this.getAllComplain();
  }
   selectedPage(event) {
    this.pageNumber = event;
    this.getAllComplain();
  }
  async onDelete(id) {
    const resp = await this.dialog.confirm("Are you sure to delete ", "", false);
    if (resp) {
        this.helpservice.DeleteComplain(id)
        .pipe(first())
        .subscribe(drivers => {
          this.complains = drivers;
          if(this.complains.data !== null)
          this.getAllComplain();      
        });
    }
  }
}
