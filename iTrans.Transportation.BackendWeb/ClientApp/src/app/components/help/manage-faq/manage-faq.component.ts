import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { first } from 'rxjs/operators';
import { DialogService } from 'src/app/services/dialog/dialog.service';
import { HelpService } from 'src/app/services/help/help.service';

@Component({
  selector: 'app-manage-faq',
  templateUrl: './manage-faq.component.html',
  styleUrls: ['./manage-faq.component.scss']
})
export class ManageFAQComponent implements OnInit {


  form!: FormGroup;
  id!: string;
  isAddMode!: boolean;
  submitted = false;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private helpservice: HelpService,
    private dialog: DialogService

  ) { }
  title_TH: any;
  module: any;
  detail_TH: any;
  sequence: any;


  ngOnInit(): void {
    this.id = this.route.snapshot.params.id;
    this.isAddMode = !this.id;
    this.form = this.formBuilder.group({
      Id: this.id,
      title_TH: ['', Validators.required],
      title_ENG: [''],
      module: ['', Validators.required],
      detail_TH: ['', Validators.required],
      sequence: [''],
      detail_ENG: [''],
      active: [true]
    }
    );
    if (!this.isAddMode) {
      this.helpservice.getFAQById(this.id)
        .pipe(first())
        .subscribe(x => this.form.patchValue(x));

    }
  }

  get formControls() { return this.form.controls; }


  private createFAQ() {
    this.helpservice.createFAQ(this.form.value)
      .pipe(first())
      .subscribe({
        next: () => {
          this.router.navigate(['/FAQ'], { relativeTo: this.route });
        }
      });
  }
  async updateFAQ() {
    const resp = await this.dialog.confirm("Are you sure to edit ", "", false);
    if (resp) {
      this.helpservice.updateFAQ(this.form.value)
        .pipe(first())
        .subscribe({
          next: () => {
            this.router.navigate(['/FAQ'], { relativeTo: this.route });
          }
        });
    }

  }

  onSubmit() {

    this.title_TH = this.form.value.title_TH;
    this.module = this.form.value.module;
    this.detail_TH = this.form.value.detail_TH;

    this.submitted = true;
    if (this.module != null && this.module != "" && this.title_TH != null && this.title_TH != "" && this.detail_TH != null && this.detail_TH != "") {

      if (this.isAddMode) {
        this.createFAQ();
      } else {
        this.updateFAQ();
      }
    }
  }
}
