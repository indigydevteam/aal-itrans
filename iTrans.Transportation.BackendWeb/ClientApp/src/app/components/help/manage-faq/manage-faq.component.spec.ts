import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageFAQComponent } from './manage-faq.component';

describe('ManageFAQComponent', () => {
  let component: ManageFAQComponent;
  let fixture: ComponentFixture<ManageFAQComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ManageFAQComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageFAQComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
