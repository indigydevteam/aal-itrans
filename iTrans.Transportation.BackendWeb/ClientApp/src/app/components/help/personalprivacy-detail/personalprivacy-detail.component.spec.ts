import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonalprivacyDetailComponent } from './personalprivacy-detail.component';

describe('PersonalprivacyDetailComponent', () => {
  let component: PersonalprivacyDetailComponent;
  let fixture: ComponentFixture<PersonalprivacyDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PersonalprivacyDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonalprivacyDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
