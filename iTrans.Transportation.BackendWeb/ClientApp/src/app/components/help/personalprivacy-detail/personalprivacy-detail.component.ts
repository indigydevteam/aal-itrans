import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { first } from 'rxjs/operators';
import { DialogService } from 'src/app/services/dialog/dialog.service';
import { HelpService } from 'src/app/services/help/help.service';

@Component({
  selector: 'app-personalprivacy-detail',
  templateUrl: './personalprivacy-detail.component.html',
  styleUrls: ['./personalprivacy-detail.component.scss']
})
export class PersonalprivacyDetailComponent implements OnInit {
  id!: string;
  data:any=[];
  text:any;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private helpservice: HelpService,
     private dialog: DialogService

    ) { }
  ngOnInit(): void {

    this.id = this.route.snapshot.params.id;
   
this.getTermAndConditionById();


  }

  public getTermAndConditionById() {
    this.helpservice.getTermAndConditionById(this.id).subscribe(data => {
      this.data = data;
      console.log(this.data);
    });
  }
  
 
  async setisPublic() {

    const resp = await this.dialog.confirm("Are you sure to update ", "", false);
    if(resp){
    this.helpservice.setisPublic(this.id)
      .pipe(first())
      .subscribe({
        next: () => {
          this.router.navigate(['/personalprivacy'], { relativeTo: this.route });
        }
      });
  }
}


}
