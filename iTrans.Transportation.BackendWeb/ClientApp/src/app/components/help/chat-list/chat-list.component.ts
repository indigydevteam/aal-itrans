import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs/operators';
import { DialogService } from 'src/app/services/dialog/dialog.service';
import { HelpService } from 'src/app/services/help/help.service';
@Component({
  selector: 'app-chat-list',
  templateUrl: './chat-list.component.html',
  styleUrls: ['./chat-list.component.scss']
})
export class ChatListComponent implements OnInit {

  public count: number = 0;
  public pageSize: number = 10;
  public pageNumber: number = 1;
  public search: string = "";
  chat:any=[];
  constructor(
    private helpservice: HelpService,
    private dialog: DialogService
    ) { }
  ngOnInit(): void {
    
    this.getAllFAQ();

  }
  public getAllFAQ() {
    const today = new Date();
    this.helpservice.getChatLog(this.search,today,this.pageNumber, this.pageSize).subscribe(chat => {
      this.chat = chat;
      this.count = chat.data.itemCount;
      console.log(this.chat);

    });
  }
  
  public filter() {
    this.pageNumber = 1;
    this.getAllFAQ();
  }
  
  previous() {
    this.pageNumber--;
    this.getAllFAQ();
  }
   next() {
    this.pageNumber++;
    this.getAllFAQ();
  }
   selectedPage(event) {
    this.pageNumber = event;
    this.getAllFAQ();
  }
  // async onDelete(id) {
  //   const resp = await this.dialog.confirm("Are you sure to delete ", "", false);
  //   if (resp) {
  //       this.helpservice.DeleteFAQ(id)
  //       .pipe(first())
  //       .subscribe(drivers => {
  //         this.chat = drivers;
  //         if(this.chat.data !== null)
  //         this.getAllFAQ();      
  //       });
  //   }
  // }
}
