import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs/operators';
import { DialogService } from 'src/app/services/dialog/dialog.service';
import { HelpService } from 'src/app/services/help/help.service';

@Component({
  selector: 'app-terms-conditions',
  templateUrl: './terms-conditions.component.html',
  styleUrls: ['./terms-conditions.component.scss']
})
export class TermsConditionsComponent implements OnInit{

  public pageSize: number = 10;
  public pageNumber: number = 1;
  public search: string = "";
  constructor(
    private helpservice: HelpService,
    private dialog: DialogService
    ) { }
  carTypeName = "";
  data:any = [];
  termsconditions:any=[];
  module:string="term & condition";
  active:number;
  column:string;
  ngOnInit(): void {
    this.all();
  }
  
  public getAlltermscondition() {
 
    this.helpservice.getAllTermsConditions(this.search, this.pageNumber, this.pageSize, this.column, this.active,this.module).subscribe(data => {
      this.termsconditions = data;

    });
  }
  
  all() {
    this.active = 1;
    this.column = "all";
    this.getAlltermscondition();
  }

  orderBy(value) {
    if (value == this.column) {
      if (this.active == 1) {
        this.active = 2;
      } else if (this.active == 2) {
        this.active = 3;
      } else {
        this.active = 1;
      }
    } else {
      this.active = 2;
    }
    this.column = value;
    this.getAlltermscondition();
  }
  
  public filter(search) {
    this.search = search;
    this.pageNumber = 1;
    this.getAlltermscondition();
  }
  
  previous() {
    this.pageNumber--;
    this.getAlltermscondition();
  }
   next() {
    this.pageNumber++;
    this.getAlltermscondition();
  }
   selectedPage(event) {
    this.pageNumber = event;
    this.getAlltermscondition();
  }
  
}