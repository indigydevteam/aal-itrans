import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs/operators';
import { DialogService } from 'src/app/services/dialog/dialog.service';
import { HelpService } from 'src/app/services/help/help.service';

@Component({
  selector: 'app-faq',
  templateUrl: './faq.component.html',
  styleUrls: ['./faq.component.scss']
})
export class FAQComponent implements OnInit{

  public pageSize: number = 10;
  public pageNumber: number = 1;
  public search: string = "";
  FAQs:any=[];
  constructor(
    private helpservice: HelpService,
    private dialog: DialogService
    ) { }
    active:number;
    column:string;

  ngOnInit(): void {
    this.all();

  }
  public getAllFAQ() {
    this.helpservice.getAllFAQ(this.search, this.pageNumber, this.pageSize, this.column, this.active).subscribe(FAQs => {
      this.FAQs = FAQs;
     

    });
  }
  all() {
    this.active = 1;
    this.column = "all";
    this.getAllFAQ();
  }

  orderBy(value) {
    if (value == this.column) {
      if (this.active == 1) {
        this.active = 2;
      } else if (this.active == 2) {
        this.active = 3;
      } else {
        this.active = 1;
      }
    } else {
      this.active = 2;
    }
    this.column = value;
    this.getAllFAQ();
  }
  
  public filter(search) {
    this.search = search;
    this.pageNumber = 1;
    this.getAllFAQ();
  }
  
  previous() {
    this.pageNumber--;
    this.getAllFAQ();
  }
   next() {
    this.pageNumber++;
    this.getAllFAQ();
  }
   selectedPage(event) {
    this.pageNumber = event;
    this.getAllFAQ();
  }
  async onDelete(id) {
    const resp = await this.dialog.confirm("Are you sure to delete ", "", false);
    if (resp) {
        this.helpservice.DeleteFAQ(id)
        .pipe(first())
        .subscribe(drivers => {
          this.FAQs = drivers;
          if(this.FAQs.data !== null)
          this.getAllFAQ();      
        });
    }
  }
}
