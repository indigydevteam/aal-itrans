import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailTermsconditionComponent } from './detail-termscondition.component';

describe('DetailTermsconditionComponent', () => {
  let component: DetailTermsconditionComponent;
  let fixture: ComponentFixture<DetailTermsconditionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DetailTermsconditionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailTermsconditionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
