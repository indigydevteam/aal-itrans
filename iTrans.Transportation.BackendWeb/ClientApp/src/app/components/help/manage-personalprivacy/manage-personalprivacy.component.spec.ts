import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ManagePersonalprivacyComponent } from './manage-personalprivacy.component';

describe('ManagePersonalprivacyComponent', () => {
  let component: ManagePersonalprivacyComponent;
  let fixture: ComponentFixture<ManagePersonalprivacyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ManagePersonalprivacyComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ManagePersonalprivacyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
