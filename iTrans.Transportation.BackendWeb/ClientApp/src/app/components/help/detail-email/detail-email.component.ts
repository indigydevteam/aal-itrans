import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HelpService } from 'src/app/services/help/help.service';

@Component({
  selector: 'app-detail-email',
  templateUrl: './detail-email.component.html',
  styleUrls: ['./detail-email.component.scss']
})
export class DetailEmailComponent implements OnInit  {

  
  id!: string;
  problemTopicId: string;


  emails:any=[];
  constructor(
    private route: ActivatedRoute,
    private helpservice: HelpService,
  ) { }
  ngOnInit(): void {
    this.id = this.route.snapshot.params.id;
    this.problemTopicId = this.route.snapshot.params.problemTopicId;

    this.getComplainBtId();
  }
  public getComplainBtId() {
    this.helpservice.getEmailById(this.id,this.problemTopicId).subscribe(emails => {
      this.emails = emails;
      console.log(this.emails);

    });
  }
}
