import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { first } from 'rxjs/operators';
import { HelpService } from 'src/app/services/help/help.service';

@Component({
  selector: 'app-manage-termscondition',
  templateUrl: './manage-termscondition.component.html',
  styleUrls: ['./manage-termscondition.component.scss']
})
export class ManageTermsconditionComponent implements OnInit {
  config = {
    placeholder: '',
    tabsize: 2,
    height: '200px',
    uploadImagePath: '/api/upload',
    toolbar: [
      ['misc', ['codeview', 'undo', 'redo']],
      ['style', ['bold', 'italic', 'underline', 'clear']],
      ['fontsize', ['fontname', 'fontsize', 'color']],
      ['para', ['style', 'ul', 'ol', 'paragraph', 'height']],
    ],
    fontNames: ['Helvetica', 'Arial', 'Arial Black', 'Comic Sans MS', 'Courier New', 'Roboto', 'Times']
  }

  form!: FormGroup;
  id!: string;
  isAddMode!: boolean;
  isFormSubmit = false;
  data: any = [];
  text: any;
  // version: any;
  name_TH: any;
  name_ENG: any;
  module: any;
  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private helpservice: HelpService
  ) { }
  ngOnInit(): void {

    this.id = this.route.snapshot.params.id;
    this.isAddMode = !this.id;
    this.form = this.formBuilder.group({
      Id: this.id,
      name_TH: ['', Validators.required],
      name_ENG: ['', Validators.required],
      module: ['', Validators.required],
      // version: ['', Validators.required],
      section: ['term & condition', Validators.required],
      sequence: [0, Validators.required],
      active: [false],
      IsAccept: [false],
      isPublic: [false]

    }
    );

    if (!this.isAddMode) {
      this.helpservice.getTermAndConditionById(this.id)
        .pipe(first())
        .subscribe(x => this.form.patchValue(x));

    }


  }

  get formControls() { return this.form.controls; }


  private createTermAndCondition() {
    this.helpservice.createTermAndCondition(this.form.value)
      .pipe(first())
      .subscribe({
        next: () => {
          this.router.navigate(['/termsconditions'], { relativeTo: this.route });
        }
      });


  }
  private updateTermAndCondition() {
    // this.version = this.form.controls['version'].value;
    this.name_TH = this.form.controls['name_TH'].value;
    this.name_ENG = this.form.controls['name_ENG'].value;
    this.module = this.form.controls['module'].value;
    this.helpservice.updateTermAndCondition(this.form.value)
      .pipe(first())
      .subscribe({
        next: () => {
          this.router.navigate(['/termsconditions'], { relativeTo: this.route });
        }
      });
  }

  onSubmit() {
    // this.version = this.form.controls['version'].value;
    this.name_TH = this.form.controls['name_TH'].value;
    this.name_ENG = this.form.controls['name_ENG'].value;
    this.module = this.form.controls['module'].value;

    this.isFormSubmit = true;

    if ( this.module != null && this.module != ""
      && this.name_TH != null && this.name_TH != "" && this.name_ENG != null && this.name_ENG != ""
    ) {

      if (this.isAddMode) {
        this.createTermAndCondition();
      } else {
        this.updateTermAndCondition();
      }
    }
  }
}
