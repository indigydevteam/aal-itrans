import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageTermsconditionComponent } from './manage-termscondition.component';

describe('ManageTermsconditionComponent', () => {
  let component: ManageTermsconditionComponent;
  let fixture: ComponentFixture<ManageTermsconditionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ManageTermsconditionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageTermsconditionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
