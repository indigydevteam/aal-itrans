import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { first } from 'rxjs/operators';
import { HelpService } from 'src/app/services/help/help.service';

@Component({
  selector: 'app-detail-complain',
  templateUrl: './detail-complain.component.html',
  styleUrls: ['./detail-complain.component.scss']
})
export class DetailComplainComponent implements OnInit  {

  
  id!: string;
  module:string;

  complains:any=[];
  constructor(
    private route: ActivatedRoute,
    private helpservice: HelpService,
  ) { }
  ngOnInit(): void {
    this.id = this.route.snapshot.params.id;
    this.module = this.route.snapshot.params.module;

    this.getComplainBtId();
    
  }
  public getComplainBtId() {
    this.helpservice.getComplainById(this.id,this.module).subscribe(complains => {
      this.complains = complains;
      console.log(this.complains);

    });
  }
}
