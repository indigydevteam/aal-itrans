import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailComplainComponent } from './detail-complain.component';

describe('DetailComplainComponent', () => {
  let component: DetailComplainComponent;
  let fixture: ComponentFixture<DetailComplainComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DetailComplainComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailComplainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
