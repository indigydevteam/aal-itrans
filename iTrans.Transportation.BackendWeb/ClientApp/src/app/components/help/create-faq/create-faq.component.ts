import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { first } from 'rxjs/operators';
import { DialogService } from 'src/app/services/dialog/dialog.service';
import { HelpService } from 'src/app/services/help/help.service';

@Component({
  selector: 'app-create-faq',
  templateUrl: './create-faq.component.html',
  styleUrls: ['./create-faq.component.scss']
})
export class CreateFaqComponent implements OnInit {
  submitted = false;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private helpservice: HelpService,
  ) { }
  faq =  {
      title_TH: null,
      detail_TH: null,
      module: null,
      active: false,
    };

  form:any=[];
  ngOnInit(): void {
    
    this.form.push(this.faq);
   
    
  }
  
  addform() {
    this.faq =  {
      title_TH: null,
      detail_TH: null,
      module: null,
      active: false,
    };
    this.form.push(this.faq);
  
  }



   createFAQ() {
    console.log(this.form);

    this.helpservice.createFAQ(this.form)
      .pipe(first())
      .subscribe({
        next: () => {
          this.router.navigate(['/FAQ'], { relativeTo: this.route });
        }
      });
  }


  onSubmit() {

    // this.title_TH = this.form.title_TH;
    // this.module = this.form.module;
    // this.detail_TH = this.form.detail_TH;

    // this.submitted = true;
    // if (this.module != null && this.module != "" && this.title_TH != null && this.title_TH != "" && this.detail_TH != null && this.detail_TH != "") {


    //     this.createFAQ();

    // }
  }
}
