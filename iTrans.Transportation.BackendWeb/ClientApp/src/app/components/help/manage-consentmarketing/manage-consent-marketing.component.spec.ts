import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageConsentMarketingComponent } from './manage-consent-marketing.component';

describe('ManageConsentMarketingComponent', () => {
  let component: ManageConsentMarketingComponent;
  let fixture: ComponentFixture<ManageConsentMarketingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ManageConsentMarketingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageConsentMarketingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
