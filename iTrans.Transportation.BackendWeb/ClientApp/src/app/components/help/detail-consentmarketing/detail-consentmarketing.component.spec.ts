import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailConsentmarketingComponent } from './detail-consentmarketing.component';

describe('DetailConsentmarketingComponent', () => {
  let component: DetailConsentmarketingComponent;
  let fixture: ComponentFixture<DetailConsentmarketingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DetailConsentmarketingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailConsentmarketingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
