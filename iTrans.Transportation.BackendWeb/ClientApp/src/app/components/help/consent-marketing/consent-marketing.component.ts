import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs/operators';
import { DialogService } from 'src/app/services/dialog/dialog.service';
import { HelpService } from 'src/app/services/help/help.service';

@Component({
  selector: 'app-consent-marketing',
  templateUrl: './consent-marketing.component.html',
  styleUrls: ['./consent-marketing.component.scss']
})
export class ConsentMarketingComponent implements OnInit{



  

  public pageSize: number = 10;
  public pageNumber: number = 1;
  public search: string = "";
  constructor(
    private helpservice: HelpService,
    private dialog: DialogService
    ) { }
  carTypeName = "";
  data:any = [];
  module:string="marketing";
  active:number;
  column:string;
 
  consentMarketings:any=[];
  ngOnInit(): void {
    this.all();

  }
  public getAllConsentMarketing() {
    this.helpservice.getAllTermsConditions(this.search, this.pageNumber, this.pageSize, this.column, this.active,this.module).subscribe(data => {
      this.consentMarketings = data;
    });
  }
   
  all() {
    this.active = 1;
    this.column = "all";
    this.getAllConsentMarketing();
  }

  orderBy(value) {
    if (value == this.column) {
      if (this.active == 1) {
        this.active = 2;
      } else if (this.active == 2) {
        this.active = 3;
      } else {
        this.active = 1;
      }
    } else {
      this.active = 2;
    }
    this.column = value;
    this.getAllConsentMarketing();
  }
  
  public filter(search) {
    this.search = search;
    this.pageNumber = 1;
    this.getAllConsentMarketing();
  }
  
  previous() {
    this.pageNumber--;
    this.getAllConsentMarketing();
  }
   next() {
    this.pageNumber++;
    this.getAllConsentMarketing();
  }
   selectedPage(event) {
    this.pageNumber = event;
    this.getAllConsentMarketing();
  }
  
  
}
 