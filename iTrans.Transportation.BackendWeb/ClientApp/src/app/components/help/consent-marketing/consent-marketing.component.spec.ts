import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsentMarketingComponent } from './consent-marketing.component';

describe('ConsentMarketingComponent', () => {
  let component: ConsentMarketingComponent;
  let fixture: ComponentFixture<ConsentMarketingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConsentMarketingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsentMarketingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
