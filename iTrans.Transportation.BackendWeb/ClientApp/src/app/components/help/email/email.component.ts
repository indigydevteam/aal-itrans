import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs/operators';
import { DialogService } from 'src/app/services/dialog/dialog.service';
import { HelpService } from 'src/app/services/help/help.service';

@Component({
  selector: 'app-email',
  templateUrl: './email.component.html',
  styleUrls: ['./email.component.scss']
})
export class EmailComponent implements OnInit{

  public count: number = 0;
  public pageSize: number = 10;
  public pageNumber: number = 1;
  public search: string = "";
  emails:any=[];
  constructor(
    private helpservice: HelpService,
    private dialog: DialogService
    ) { }
    active:number;
    column:string;
  ngOnInit(): void {
    this.all();
  }
  public getAllEmail() {
    this.helpservice.getAllEmail(this.search, this.pageNumber, this.pageSize, this.column, this.active).subscribe(emails => {
      this.emails = emails;
    });
  }
  all() {
    this.active = 1;
    this.column = "all";
    this.getAllEmail();
  }

  orderBy(value) {
    if (value == this.column) {
      if (this.active == 1) {
        this.active = 2;
      } else if (this.active == 2) {
        this.active = 3;
      } else {
        this.active = 1;
      }
    } else {
      this.active = 2;
    }
    this.column = value;
    this.getAllEmail();
  }
  
  public filter(search) {
    this.search = search;
    this.pageNumber = 1;
    this.getAllEmail();
  }
  
  previous() {
    this.pageNumber--;
    this.getAllEmail();
  }
   next() {
    this.pageNumber++;
    this.getAllEmail();
  }
   selectedPage(event) {
    this.pageNumber = event;
    this.getAllEmail();
  }
  async onDelete(id,TopicId) {
    const resp = await this.dialog.confirm("Are you sure to delete ", "", false);
    if (resp) {
        this.helpservice.DeleteEmail(id,TopicId)
        .pipe(first())
        .subscribe(drivers => {
          this.emails = drivers;
          if(this.emails.data !== null)
          this.getAllEmail();      
        });
    }
  }
}
