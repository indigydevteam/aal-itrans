import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { OrderingService } from 'src/app/services/ordering/ordering.service';
@Component({
  selector: 'app-mange-payment',
  templateUrl: './mange-payment.component.html',
  styleUrls: ['./mange-payment.component.scss']
})
export class MangePaymentComponent implements OnInit{
  
  id!: string;

  
    constructor(private orderingservice: OrderingService,  private router: Router,private route: ActivatedRoute,) { }

  orderingpayments:any = []
  
  ngOnInit(): void  { 
    this.id = this.route.snapshot.params.id;
    this.getAllOrderingPayment();
  }
  public getAllOrderingPayment() {

      this.orderingservice.getOrderingPaymentById(this.id).subscribe(orderingpayments => {
        this.orderingpayments = orderingpayments.data;
        console.log(this.orderingpayments);

      });
    }
   
  
  }