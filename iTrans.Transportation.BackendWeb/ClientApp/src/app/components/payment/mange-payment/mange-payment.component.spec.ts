import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MangePaymentComponent } from './mange-payment.component';

describe('MangePaymentComponent', () => {
  let component: MangePaymentComponent;
  let fixture: ComponentFixture<MangePaymentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MangePaymentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MangePaymentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
