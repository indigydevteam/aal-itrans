import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs/operators';
import { DialogService } from 'src/app/services/dialog/dialog.service';
import { OrderingService } from 'src/app/services/ordering/ordering.service';

@Component({
  selector: 'app-payment-list',
  templateUrl: './payment-list.component.html',
  styleUrls: ['./payment-list.component.scss']
})
export class PaymentListComponent implements OnInit {



  public pageSize: number = 10;
  public pageNumber: number = 1;
  public search: string = "";

  constructor(private orderingservice: OrderingService, private dialog: DialogService) { }

  orderingpayments: any = []
  active: number;
  column: string ;
  ngOnInit(): void {
    console.log(this.active);
    this.all()
  }
  public getAllOrderingPayment() {

    this.orderingservice.getAllOrderingPayment(this.search, this.pageNumber, this.pageSize, this.column, this.active).subscribe(orderingpayments => {
      this.orderingpayments = orderingpayments;

    });
  }

  all() {
    this.active= 1 ;
    this.column = "all";
    this.getAllOrderingPayment();
  }
  orderBy(value) {
    if (value == this.column) {
      if (this.active == 1) {
        this.active = 2;
      } else if (this.active == 2) {
        this.active = 3;
      } else {
        this.active = 1;
      }
    } else {
      this.active = 2;
    }
    this.column = value;
    this.getAllOrderingPayment();
  }


  public filter() {
    this.pageNumber = 1;
    this.getAllOrderingPayment();
  }
  public pageChange(pageNumber: number) {
    console.log(pageNumber);
    this.pageNumber = pageNumber;
    this.getAllOrderingPayment();
  }
  previous() {
    this.pageNumber--;
    this.getAllOrderingPayment();
  }
  next() {
    this.pageNumber++;
    this.getAllOrderingPayment();
  }
  selectedPage(event) {
    this.pageNumber = event;
    this.getAllOrderingPayment();
  }

  onDelete(id) {

    console.log(id);
    this.orderingservice.DeleteOrderingPayment(id)
      .pipe(first())
      .subscribe(orderingpayments => {
        this.orderingpayments = orderingpayments;
        if (this.orderingpayments.data !== null)
          this.getAllOrderingPayment();
      });


  }
}

