import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderingcancelstatusListComponent } from './orderingcancelstatus-list.component';

describe('OrderingcancelstatusListComponent', () => {
  let component: OrderingcancelstatusListComponent;
  let fixture: ComponentFixture<OrderingcancelstatusListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OrderingcancelstatusListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderingcancelstatusListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
