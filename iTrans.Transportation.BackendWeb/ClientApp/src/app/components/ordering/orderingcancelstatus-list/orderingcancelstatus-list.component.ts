import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { first } from 'rxjs/operators';
import { OrderingCancelStatusList } from 'src/app/models/ordering.model';
import { DialogService } from 'src/app/services/dialog/dialog.service';
import { OrderingService } from 'src/app/services/ordering/ordering.service';

@Component({
  selector: 'app-orderingcancelstatus-list',
  templateUrl: './orderingcancelstatus-list.component.html',
  styleUrls: ['./orderingcancelstatus-list.component.scss']
})
export class OrderingcancelstatusListComponent implements OnInit {
  public orderingcancelstatuses: Observable<OrderingCancelStatusList> | any;
  
  public pageSize: number = 10;
  public pageNumber: number = 1;
  public search: string = "";
  constructor(
    private orderingservice: OrderingService,
    private dialog: DialogService
    ) { }
  
    active: number;
    column: string;
    ngOnInit(): void  { this.all();
    }

  public getAllOrderingCancelStatus() {
      this.orderingservice.getAllOrderingCancelStatus(this.search, this.pageNumber, this.pageSize, this.column, this.active).subscribe(orderingcancelstatuses => {
        this.orderingcancelstatuses = orderingcancelstatuses;

      });
    }
    all() {
      this.active = 1;
      this.column = "all";
      this.getAllOrderingCancelStatus();
    }
  
    orderBy(value) {
      if (value == this.column) {
        if (this.active == 1) {
          this.active = 2;
        } else if (this.active == 2) {
          this.active = 3;
        } else {
          this.active = 1;
        }
      } else {
        this.active = 2;
      }
      this.column = value;
      this.getAllOrderingCancelStatus();
    }
  
    public filter() {
      this.pageNumber = 1;
      this.getAllOrderingCancelStatus();
    }
    public pageChange(pageNumber: number ) {
      console.log(pageNumber);
      this.pageNumber = pageNumber;
      this.getAllOrderingCancelStatus();
    }
    previous() {
      this.pageNumber--;
      this.getAllOrderingCancelStatus();
    }
     next() {
      this.pageNumber++;
      this.getAllOrderingCancelStatus();
    }
     selectedPage(event) {
      this.pageNumber = event;
      this.getAllOrderingCancelStatus();
    }
    async  onDelete(id){
      const resp = await this.dialog.confirm("Are you sure to delete ", "", false);
      if (resp) {
          this.orderingservice.deleteOrderingCancelStatus(id)
          .pipe(first())
          .subscribe(orderingcancelstatuses => {
            this.orderingcancelstatuses = orderingcancelstatuses;
            if(this.orderingcancelstatuses.data !== null)
            this.getAllOrderingCancelStatus();      
          });
      }
     
    }
  
  }
  
