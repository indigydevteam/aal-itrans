import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageOrderingcancelstatusComponent } from './manage-orderingcancelstatus.component';

describe('ManageOrderingcancelstatusComponent', () => {
  let component: ManageOrderingcancelstatusComponent;
  let fixture: ComponentFixture<ManageOrderingcancelstatusComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ManageOrderingcancelstatusComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageOrderingcancelstatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
