import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { first } from 'rxjs/operators';
import { OrderingService } from 'src/app/services/ordering/ordering.service';
@Component({
  selector: 'app-manage-orderingcancelstatus',
  templateUrl: './manage-orderingcancelstatus.component.html',
  styleUrls: ['./manage-orderingcancelstatus.component.scss']
})
export class ManageOrderingcancelstatusComponent implements OnInit {

  form!: FormGroup;
  id!: string;
  isAddMode!: boolean;
  submitted = false;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private orderingservice: OrderingService,
  ) { }
  ngOnInit(): void {

    this.id = this.route.snapshot.params.id;
    this.isAddMode = !this.id;
    this.form = this.formBuilder.group({
        Id: this.id,
        module: ['', Validators.required],
        name_TH: ['', Validators.required],
        name_ENG: ['', Validators.required],
        sequence: ['', Validators.required],
        active: [true]
      }
    );
    if (!this.isAddMode) {
      this.orderingservice.getOrderingCancelStatusById(this.id)
        .pipe(first())
        .subscribe(x => this.form.patchValue(x));

    }
  }
  get formControls() { return this.form.controls; }

  private createOrderingCancelStatus() {

    this.orderingservice.createOrderingCancelStatus(this.form.value)
      .pipe(first())
      .subscribe({
        next: () => {
          this.router.navigate(['/orderingcancelstatus'], { relativeTo: this.route });
        }
      });
  }
  private updateOrderingCancelStatus() {
    this.orderingservice.updateOrderingCancelStatus(this.form.value)
      .pipe(first())
      .subscribe({
        next: () => {
          this.router.navigate(['/orderingcancelstatus'], { relativeTo: this.route });
        }
      });
  }

  onSubmit() {
    this.submitted = true;
    if (this.form.invalid) {
      return;
    }
    

    if (this.isAddMode) {
      this.createOrderingCancelStatus();
    } else {
      this.updateOrderingCancelStatus();
    }

  }

}
