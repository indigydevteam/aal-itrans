import { ProvinceService } from './../../../services/province/province.service';
import { DistrictService } from './../../../services/district/district.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { MustMatch } from './../../../shared/must-match.validator';
import { first } from 'rxjs/operators';
import { Province } from '../../../models/province.model';

@Component({
  selector: 'app-manage-district',
  templateUrl: './manage-district.component.html',
  styleUrls: ['./manage-district.component.scss']
})
export class ManageDistrictComponent implements OnInit {

  form!: FormGroup;
  id!: string;
  isAddMode!: boolean;
  submitted = false;
  provinces: Province[] | any;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private provinceService: ProvinceService,
    private districtService: DistrictService,
  ) { }

  ngOnInit(): void {
    this.getAllProvince();
    this.id = this.route.snapshot.params.id;
    this.isAddMode = !this.id;
    this.form = this.formBuilder.group({
      Id: this.id,
      provinceId: ['', Validators.required],
      name_TH: ['', Validators.required],
      name_ENG: ['', Validators.required],
      active: [true]
    }
    );
    if (!this.isAddMode) {
      this.districtService.getDistrictById(this.id)
        .pipe(first())
        .subscribe(x => {
          this.form.patchValue(x);
          console.log(this.form);
        });
    }
  }
  get formControls() { return this.form.controls; }

  private createDistrict() {
    console.log(this.form.value);
    this.districtService.createDistrict(this.form.value)
      .pipe(first())
      .subscribe({
        next: () => {
          this.router.navigate(['/district'], { relativeTo: this.route });
        }
      });
  }
  private updateDistrict() {
    this.districtService.updateDistrict(this.form.value)
      .pipe(first())
      .subscribe({
        next: () => {
          this.router.navigate(['/district'], { relativeTo: this.route });
        }
      });
  }

  onSubmit() {
    
    this.submitted = true;
    if (this.form.invalid) {
      return;
    }

    if (this.isAddMode) {
      this.createDistrict();
    } else {
      this.updateDistrict();
    }

  }

  public getAllProvince() {
    this.provinceService.getAllProvince("", 0, 0).subscribe(provinces => {
      this.provinces = provinces.data;
      console.log(this.provinces);
    });
  }

}
