import { DistrictService } from './../../../services/district/district.service';
import { Component, OnInit } from '@angular/core';
import { NgxRangeModule } from 'ngx-range';
import { first } from 'rxjs/operators';
import { District, DistrictList } from 'src/app/models/district.model';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-district-list',
  templateUrl: './district-list.component.html',
  styleUrls: ['./district-list.component.scss']
})
export class DistrictListComponent implements OnInit {

  public districts: Observable<DistrictList> | any;
  public page: number = 0;
  constructor(private districtservice: DistrictService) { }
  public pageSize: number = 10;
  public pageNumber: number = 1;
  public search: string = "";
  ngOnInit(): void {
    this.getAllDistrict();
  }

  public getAllDistrict() {
    this.districtservice.getAllDistrict(this.search, this.pageNumber, this.pageSize).subscribe(districts => {
      console.log(districts);
      this.districts = districts;
      this.page = districts.itemCount / (districts.pageSize == 0 ? 1 : districts.pageSize);
      if (this.page == 0) this.page = 1;
    });
  }

  public filter(event) {
    console.log("You entered: ", event.target.value);
    this.search = event.target.value;
    this.pageNumber = 1;
    this.getAllDistrict();
  }
  public pageChange(pageNumber: number ) {
    console.log(pageNumber);
    this.pageNumber = pageNumber;
    this.getAllDistrict();
  }
}
