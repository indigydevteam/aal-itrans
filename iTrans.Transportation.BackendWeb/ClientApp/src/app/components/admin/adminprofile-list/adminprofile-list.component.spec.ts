import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminprofileListComponent } from './adminprofile-list.component';

describe('AdminprofileListComponent', () => {
  let component: AdminprofileListComponent;
  let fixture: ComponentFixture<AdminprofileListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdminprofileListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminprofileListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
