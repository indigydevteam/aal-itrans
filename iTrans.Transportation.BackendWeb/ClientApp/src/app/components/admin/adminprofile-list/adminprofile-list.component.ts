import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs/operators';
import { DialogService } from 'src/app/services/dialog/dialog.service';
import { UserService } from 'src/app/services/user/user.service';

@Component({
  selector: 'app-adminprofile-list',
  templateUrl: './adminprofile-list.component.html',
  styleUrls: ['./adminprofile-list.component.scss']
})
export class AdminprofileListComponent implements OnInit{

  

  public user: any=[];
  public count: number =0;
  public pageSize: number = 10;
  public pageNumber: number = 1;
  public search: string = "";
  constructor(
    private userservice: UserService,
    private dialog: DialogService,
    ) { }
    active: number;
    column: string;
  ngOnInit(): void {
    this.all() ;
  }
  public getAllUser() {
    this.userservice.GetAllUser(this.search, this.pageNumber, this.pageSize, this.column, this.active).subscribe(user => {
      this.user = user;
      this.count = user.data.itemCount;

    });
  }
  all() {
    this.active = 1;
    this.column = "all";
    this.getAllUser();
  }

  orderBy(value) {
    if (value == this.column) {
      if (this.active == 1) {
        this.active = 2;
      } else if (this.active == 2) {
        this.active = 3;
      } else {
        this.active = 1;
      }
    } else {
      this.active = 2;
    }
    this.column = value;
    this.getAllUser();
  }

  public filter() {
    this.pageNumber = 1;
    this.getAllUser();
  }
  previous() {
    this.pageNumber--;
    this.getAllUser();
  }
   next() {
    this.pageNumber++;
    this.getAllUser();
  }
   selectedPage(event) {
    this.pageNumber = event;
    this.getAllUser();
  }
  async onDelete(id) {
    const resp = await this.dialog.confirm("Are you sure to delete ", "", false);
    if (resp) {
      this.userservice.DeleteUserById(id)
        .pipe(first())
        .subscribe(drivers => {
          this.user = drivers;
          if (this.user.data !== null)
            this.getAllUser();
        });
    }

  }
  


}