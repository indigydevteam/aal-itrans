import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageAdminprofileComponent } from './manage-adminprofile.component';

describe('ManageAdminprofileComponent', () => {
  let component: ManageAdminprofileComponent;
  let fixture: ComponentFixture<ManageAdminprofileComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ManageAdminprofileComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageAdminprofileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
