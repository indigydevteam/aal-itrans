import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbDatepicker } from '@ng-bootstrap/ng-bootstrap';
import * as moment from 'moment';
import { DatepickerOptions } from 'ng2-datepicker';
import { first } from 'rxjs/operators';
import { DialogService } from 'src/app/services/dialog/dialog.service';
import { DriverService } from 'src/app/services/driver/driver.service';
import { SecurityService } from 'src/app/services/security/security.service';
import { UserService } from 'src/app/services/user/user.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-manage-adminprofile',
  templateUrl: './manage-adminprofile.component.html',
  styleUrls: ['./manage-adminprofile.component.scss']
})
export class ManageAdminprofileComponent implements OnInit {
  @ViewChild('dp') dp: NgbDatepicker;
  options: DatepickerOptions = {
    
    // scrollBarColor: '#ffffff',
    format: 'dd/MM/yyyy', // date format to display in inputม
    position: 'bottom',
    inputClass: 'form-date icon',
    
  };
  id!: string;
  isAddMode!: boolean;


  constructor(
    private userservice: UserService,
    private route: ActivatedRoute,
    private router: Router,
    private securityservice: SecurityService,
    private dialog: DialogService,
    private driverservice: DriverService,


  ) { }
  users: any = [];
  role: any = [];
  contactpersontitles: any = [];
  image = null;

  form = {
    Id: null,
    role: null,
    firstNameTH: null,
    firstNameEN: null,
    lastNameTH: null,
    lastNameEN: null,
    middleNameTH: null,
    middleNameEN: null,
    phoneNumber: null,
    email: null,
    title: null,
    birthday: new Date(),
    identityNumber: null,
    imageprofile: null,
    activated: new Date(),
    expiredDate: new Date(),
  }

  ngOnInit(): void {
    this.id = this.route.snapshot.params.id;
    this.isAddMode = !this.id;
    console.log(this.isAddMode);
    this.GetUserById();

    this.GetAllRole();
    this.getAllContactPersonTitle();
  }
  public GetUserById() {
    this.userservice.GetUserById(this.id).subscribe(users => {
      this.users = users.data;
      this.image = this.users.imageprofile;

      this.form = {
        Id: this.users ? this.users.emplID : null,
        role: this.users ? this.users.role.id : null,
        firstNameTH: this.users ? this.users.firstNameTH : null,
        firstNameEN: this.users ? this.users.firstNameEN : null,
        lastNameTH: this.users ? this.users.lastNameTH : null,
        lastNameEN: this.users ? this.users.lastNameEN : null,
        middleNameTH: this.users ? this.users.middleNameTH : null,
        middleNameEN: this.users ? this.users.middleNameEN : null,
        phoneNumber: this.users ? this.users.phoneNumber : null,
        email: this.users ? this.users.email : null,
        title: this.users ? this.users.title.id : null,
        birthday: new Date(moment(this.users.birthday).format("")),
        identityNumber: this.users ? this.users.identityNumber : null,
        activated: new Date(moment(this.users.activated).format("")),
        expiredDate: new Date(moment(this.users.expiredDate).format("")),
        imageprofile: environment.baseWebContentUrl +'/'+ this.users.imageprofile
      }
    
      this.image = this.form.imageprofile;

    });
  }
  checkinValidDate(date) {
    if ((new Date(date).getTime() < 1)) {
      return (new Date());
    }
    return date;
  }
  public GetAllRole() {
    this.securityservice.getAllRoles().subscribe(role => {
      this.role = role.data;

    });
  }
  public getAllContactPersonTitle() {
    this.driverservice.getAllContactPersonTitle(this.id).subscribe(title => {
      this.contactpersontitles = title;
    });
  }
  async onSubmit() {

    console.log(this.isAddMode);
    if (this.isAddMode) {
      this.CreateUser();
    } else {
      this.UpdateUserById();
    }

  }
  async CreateUser() {


    const formData = new FormData();
    formData.append("Id", this.form.Id);
    formData.append("RoleId", this.form.role);
    formData.append("FirstNameTH", this.form.firstNameTH);
    formData.append("FirstNameEN", this.form.firstNameEN);
    formData.append("LastNameTH", this.form.lastNameTH);
    formData.append("LastNameEN", this.form.lastNameEN);
    formData.append("MiddleNameTH", this.form.middleNameTH);
    formData.append("MiddleNameEN", this.form.middleNameEN);
    formData.append("PhoneNumber", this.form.phoneNumber);
    formData.append("Email", this.form.email);
    formData.append("TitleId", this.form.title);
    const birthday = moment(this.form.birthday).format('YYYY/MM/DD')
     formData.append("Birthday", birthday);
    formData.append("IdentityNumber", this.form.identityNumber);
    formData.append("Imageprofile", this.form.imageprofile);
    const expiredDate = moment(this.form.expiredDate).format('YYYY/MM/DD')
    formData.append("ExpiredDate", expiredDate); 
    const activated = moment(this.form.activated).format('YYYY/MM/DD')
    formData.append("Activated", activated);

  
      this.userservice.CreateUser(formData)
        .pipe(first())
        .subscribe({
          next: () => {
            this.router.navigate(['/adminprofile'], { relativeTo: this.route });
          }
        });
    
  }
  async UpdateUserById() {

    const formData = new FormData();
    formData.append("Id", this.form.Id);
    formData.append("RoleId", this.form.role);
    formData.append("FirstNameTH", this.form.firstNameTH);
    formData.append("FirstNameEN", this.form.firstNameEN);
    formData.append("LastNameTH", this.form.lastNameTH);
    formData.append("LastNameEN", this.form.lastNameEN);
    formData.append("MiddleNameTH", this.form.middleNameTH);
    formData.append("MiddleNameEN", this.form.middleNameEN);
    formData.append("PhoneNumber", this.form.phoneNumber);
    formData.append("Email", this.form.email);
    formData.append("TitleId", this.form.title);
    const birthday = moment(this.form.birthday).format('YYYY/MM/DD')
     formData.append("Birthday", birthday);
    formData.append("IdentityNumber", this.form.identityNumber);
    formData.append("Imageprofile", this.form.imageprofile);
    const expiredDate = moment(this.form.expiredDate).format('YYYY/MM/DD')
    formData.append("ExpiredDate", expiredDate);
    const activated = moment(this.form.activated).format('YYYY/MM/DD')
    formData.append("Activated", activated);

    const resp = await this.dialog.confirm("Are you sure to update ", "", false);
    if (resp) {
      this.userservice.UpdateUserById(formData)
        .pipe(first())
        .subscribe({
          next: () => {
            this.router.navigate(['/adminprofile'], { relativeTo: this.route });
          }
        });
    }
  }

  handleFileInput(text, files: FileList) {
    const file = files[0];
    if (text === 'image') {
      const reader = new FileReader();
      reader.onload = e => this.image = reader.result;
      reader.readAsDataURL(file);
      this.form.imageprofile = file;
    } 
  }

}
