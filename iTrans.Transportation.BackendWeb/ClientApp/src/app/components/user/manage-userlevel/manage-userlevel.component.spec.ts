import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageUserlevelComponent } from './manage-userlevel.component';

describe('ManageUserlevelComponent', () => {
  let component: ManageUserlevelComponent;
  let fixture: ComponentFixture<ManageUserlevelComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ManageUserlevelComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageUserlevelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
