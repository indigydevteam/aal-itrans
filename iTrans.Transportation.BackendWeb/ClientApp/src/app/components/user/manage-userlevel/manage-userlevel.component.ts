import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { catchError, first } from 'rxjs/operators';
import { UserLevelCondition } from '../../../models/user.model';
import { UserService } from './../../../services/user/user.service';

@Component({
  selector: 'app-manage-userlevel',
  templateUrl: './manage-userlevel.component.html',
  styleUrls: ['./manage-userlevel.component.scss']
})
export class ManageUserlevelComponent implements OnInit {

  form!: FormGroup;
  conditions: UserLevelCondition[] = [];
  id!: string;
  isAddMode!: boolean;
  submitted = true;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private userservice: UserService,
  ) { }
  module: any;
  star: any;
  level: any;
  name_TH: any;
  name_ENG: any;
  active: boolean = true;

  //customerStar: number | undefined;
  customerRequestCarPerWeek: number | undefined;
  customerRequestCarPerMonth: number | undefined;
  customerRequestCarPerYear: number | undefined;
  customerCancelPerWeek: number | undefined;
  customerCancelPerMonth: number | undefined;
  customerCancelPerYear: number | undefined;
  customerOrderingValuePerWeek: number | undefined;
  customerOrderingValuePerMonth: number | undefined;
  customerOrderingValuePerYear: number | undefined;
  customerDiscount: number | undefined;
  customerFine: number | undefined;

  driverAcceptJobPerWeek: number | undefined;
  driverAcceptJobPerMonth: number | undefined;
  driverAcceptJobPerYear: number | undefined;
  driverCancelPerWeek: number | undefined;
  driverCancelPerMonth: number | undefined;
  driverCancelPerYear: number | undefined;
  driverComplaintPerWeek: number | undefined;
  driverComplaintPerMonth: number | undefined;
  driverComplaintPerYear: number | undefined;
  driverRejectPerWeek: number | undefined;
  driverRejectPerMonth: number | undefined;
  driverRejectPerYear: number | undefined;
  driverInsuranceValue: number | undefined;
  driverCommission: number | undefined;
  driverDiscount: number | undefined;
  driverFine: number | undefined;


  //driverAnnouncementPerYear: any;
  //driverRejectPerMonth: any;
  //driverComplaintPerYear: any;
  //driverRejectPerYear: any;
  //driverCancelPerYear: any;
  //driverInsuranceValue: any;

  ngOnInit(): void {

    this.id = this.route.snapshot.params.id;
    this.isAddMode = !this.id;
    this.module = "customer";
    this.form = this.formBuilder.group({
      Id: this.id,
      module: ["customer", Validators.required],
      level: ['', Validators.required],
      star: ['', Validators.required],
      name_TH: ['', Validators.required],
      name_ENG: ['', Validators.required],
      active: [this.active],
      customerRequestCarPerWeek: 0,
      customerRequestCarPerMonth: 0,
      customerCancelPerMonth: 0,
      customerOrderingValuePerMonth: 0,
      customerDiscount: 0,
      customerFine: 0,

      driverAcceptJobPerWeek:0,
      driverAcceptJobPerMonth: 0,
      driverCancelPerMonth: 0,
      driverCancelPerYear: 0,
      driverInsuranceValue: 0,
      driverCommission: 0,
      driverDiscount:0,
      driverFine: 0,
      conditions: [],
    }
    );
    if (!this.isAddMode) {
      this.userservice.getUserlevelById(this.id)
        .pipe(first())
        .subscribe(x => {
          console.log(x);
          this.module = x.module;
          this.form.patchValue(x);
          this.conditions = x.conditions;
          console.log(this.conditions);
        });

    }
  }
  get formControls() { return this.form.controls; }

  private createUserlevel(para: any) {
    this.userservice.createUserlevel(para)
      
      .pipe(first())
      .subscribe({
        next: () => {
          this.router.navigate(['/userlevel'], { relativeTo: this.route });
        }
      })
      ;
  }
  private updateUserlevel(para: any) {
    this.userservice.updateUserlevel(para)
      .pipe(first()
        ,
        catchError(err => {
          console.log(err);
          throw 'error in source. Details: ' + err;
        }))
      
      .subscribe({
        next: () => {
          this.router.navigate(['/userlevel'], { relativeTo: this.route });
        }
      });
  }

  onSubmit(event: any) {
    console.log(this.form.value.customerRequestCarPerWeek);
    this.module = this.form.value.module;
    this.level = this.form.value.level;
    this.star = this.form.value.star;
    this.name_TH = this.form.value.name_TH;
    this.name_ENG = this.form.value.name_ENG;
    this.active = this.form.value.active;
    this.customerRequestCarPerWeek = this.form.value.customerRequestCarPerWeek ? this.form.value.customerRequestCarPerWeek : 0;
    this.customerRequestCarPerMonth = this.form.value.customerRequestCarPerMonth ? this.form.value.customerRequestCarPerMonth : 0;
    this.customerCancelPerMonth = this.form.value.customerCancelPerMonth ? this.form.value.customerCancelPerMonth : 0;
    this.customerOrderingValuePerMonth = this.form.value.customerOrderingValuePerMonth ? this.form.value.customerOrderingValuePerMonth : 0;
    this.customerDiscount = this.form.value.customerDiscount ? this.form.value.customerDiscount : 0;
    this.customerFine = this.form.value.customerFine ? this.form.value.customerFine : 0;

    this.driverAcceptJobPerWeek = this.form.value.driverAcceptJobPerWeek ? this.form.value.driverAcceptJobPerWeek : 0;
    this.driverAcceptJobPerMonth = this.form.value.driverAcceptJobPerMonth ? this.form.value.driverAcceptJobPerMonth : 0;
    this.driverCancelPerMonth = this.form.value.driverCancelPerMonth ? this.form.value.driverCancelPerMonth : 0;
    this.driverCancelPerYear = this.form.value.driverCancelPerYear ? this.form.value.driverCancelPerYear : 0;
    this.driverCommission = this.form.value.driverCommission ? this.form.value.driverCommission : 0;
    this.driverDiscount = this.form.value.driverDiscount ? this.form.value.driverDiscount : 0;
    this.driverFine = this.form.value.driverFine ? this.form.value.driverFine : 0;
    
    //this.driverAnnouncementPerYear = this.form.value.driverAcceptJobPerMonth;
    //this.driverRejectPerMonth = this.form.value.driverRejectPerMonth;
    //this.driverComplaintPerYear = this.form.value.driverComplaintPerYear;
    //this.driverRejectPerYear = this.form.value.driverRejectPerYear;
    //this.driverCancelPerYear = this.form.value.driverCancelPerYear;
    //this.driverInsuranceValue = this.form.value.driverInsuranceValue;

    this.form.value.conditions = this.conditions;
    this.submitted = false;

    if (this.module != null && this.module != "" && this.level != null && this.level != "" && this.star > 0
      && this.name_TH != null && this.name_TH != "" && this.name_ENG != null && this.name_ENG != ""
    ) {
      this.submitted = true;
      //if (this.module == "customer" && this.customerCancelAmount > 0 && this.customerOrderValue > 0
      //  && this.customerRequestCar > 0 && this.customerRequestCar >= 0) {
      //  this.submitted = true;
      //}
      //if (this.module == "driver" && this.driverAcceptJobPerMonth > 0 && this.driverCancelPerMonth > 0 && this.driverAnnouncementPerMonth > 0 && this.driverComplaintPerMonth > 0
      //  && this.driverCommission >= 0 && this.driverFine >= 0    ) {
      //  this.submitted = true;
      //}
    }
    console.log(this.submitted);
    if (this.submitted) {
      const formData = new FormData();
      formData.append("Module", this.module);
      formData.append("Level", this.level);
      formData.append("Star", this.star);
      formData.append("Name_TH", this.name_TH);
      formData.append("Name_ENG", this.name_ENG);
      formData.append("Active", this.active ? "true" : "false");
      formData.append("CustomerRequestCarPerWeek", this.customerRequestCarPerWeek.toString());
      formData.append("CustomerRequestCarPerMonth", this.customerRequestCarPerMonth.toString());
      formData.append("CustomerCancelPerMonth", this.customerCancelPerMonth.toString());
      formData.append("CustomerOrderingValuePerMonth", this.customerOrderingValuePerMonth.toString());
      formData.append("CustomerDiscount", this.customerDiscount.toString());
      formData.append("CustomerFine", this.customerFine.toString());
      console.log("9999999999999");
      formData.append("DriverAcceptJobPerWeek", this.driverAcceptJobPerWeek.toString());
      formData.append("DriverAcceptJobPerMonth", this.driverAcceptJobPerMonth.toString());
      formData.append("DriverCancelPerMonth", this.driverCancelPerMonth.toString());
      formData.append("DriverCancelPerYear", this.driverCancelPerYear.toString());
      formData.append("DriverCommission", this.driverCommission.toString());
      formData.append("DriverDiscount", this.driverDiscount.toString());
      formData.append("DriverFine", this.driverFine.toString());

      console.log(JSON.stringify(this.conditions));
      formData.append("Conditions", JSON.stringify(this.conditions));
      if (this.isAddMode) {
        this.createUserlevel(formData);
      } else {
        formData.append("Id", this.id);
        this.updateUserlevel(formData);
      }
    }
  }

  public moduleChange(value) {
    this.module = value;

  }
  addCondition() {
    console.log(this.conditions);
    let condition: UserLevelCondition = {
      characteristics_ENG: "",
      characteristics_TH: "",
      active: true,
      id: 0,
      userLevelId: 0,
      sequence: 0
    };
    this.conditions.push(condition);
  }
  onDelete(index) {
    if (index !== -1) {
      this.conditions.splice(index, 1);
    }
  }
  focusOutFunctionTH(event: any, index) {
    if (index !== -1) {
      this.conditions[index].characteristics_TH = event.target.value;
    }
  }
  focusOutFunctionEN(event: any, index) {
    if (index !== -1) {
      this.conditions[index].characteristics_ENG = event.target.value;
    }
  }
}
