import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { first } from 'rxjs/operators';
import { UserLevelList } from '../../../models/user.model';
import { DialogService } from './../../../services/dialog/dialog.service';
import { UserService } from './../../../services/user/user.service';

@Component({
  selector: 'app-userlevel-list',
  templateUrl: './userlevel-list.component.html',
  styleUrls: ['./userlevel-list.component.scss']
})
export class UserlevelListComponent implements OnInit {

  public userlevels: Observable<UserLevelList> | any;
  public count: number = 0;
  public pageSize: number = 10;
  public pageNumber: number = 1;
  public search: string = "";

  constructor(
    private userservice: UserService,
    private dialog: DialogService
    ) { }
  
    active: number;
    column: string ;
    ngOnInit(): void  { 
      this.all();
    }
  public getUserLevels() {
      this.userservice.getUserLevels(this.search, this.pageNumber, this.pageSize, this.column, this.active).subscribe(userlevels => {
        this.userlevels = userlevels;
      });
    }
    all() {
      this.active = 1;
      this.column = "all";
      this.getUserLevels();
    }
  
    orderBy(value) {
      if (value == this.column) {
        if (this.active == 1) {
          this.active = 2;
        } else if (this.active == 2) {
          this.active = 3;
        } else {
          this.active = 1;
        }
      } else {
        this.active = 2;
      }
      this.column = value;
      this.getUserLevels();
    }
  public filter(search) {
    this.search = search;
    this.pageNumber = 1;
    this.getUserLevels();
  }
  previous() {
    this.pageNumber--;
    this.getUserLevels();
  }
   next() {
    this.pageNumber++;
     this.getUserLevels();
  }
   selectedPage(event) {
    this.pageNumber = event;
     this.getUserLevels();
  }
  async  onDelete(id){
    const resp = await this.dialog.confirm("Are you sure to delete ", "", false);
    if (resp) {
        this.userservice.deleteUserLevelById(id)
        .pipe(first())
        .subscribe(userlevels => {
          this.userlevels = userlevels;
          if(this.userlevels.data !== null)
            this.getUserLevels();      
        });
    }
  }
}
