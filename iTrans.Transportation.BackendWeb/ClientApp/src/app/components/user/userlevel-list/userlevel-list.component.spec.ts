import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserlevelListComponent } from './userlevel-list.component';

describe('UserlevelListComponent', () => {
  let component: UserlevelListComponent;
  let fixture: ComponentFixture<UserlevelListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UserlevelListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UserlevelListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
