import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { first } from 'rxjs/operators';
import { ProductPackagingList,ProductPackaging } from 'src/app/models/product.model';
import { DialogService } from 'src/app/services/dialog/dialog.service';
import { ProductService } from 'src/app/services/product/product.service';

@Component({
  selector: 'app-productpackaging-list',
  templateUrl: './productpackaging-list.component.html',
  styleUrls: ['./productpackaging-list.component.scss']
})
export class ProductpackagingListComponent implements OnInit{
  public productpackagings: Observable<ProductPackagingList> | any;
  
  
  public count: number = 0;
  public pageSize: number = 10;
  public pageNumber: number = 1;
  public search: string = "";
  constructor(
    private productservice: ProductService,
    private dialog: DialogService
    ) { }  
    active: number;
    column: string ;
    ngOnInit(): void  { 
      this.all();
    }
    public getAllProductPackaging() {
      this.productservice.getAllProductPackaging(this.search, this.pageNumber, this.pageSize, this.column, this.active).subscribe(productpackagings => {
        this.productpackagings = productpackagings;
      });
    }
    all() {
      this.active = 1;
      this.column = "all";
      this.getAllProductPackaging();
    }
  
  
    orderBy(value) {
      if (value == this.column) {
        if (this.active == 1) {
          this.active = 2;
        } else if (this.active == 2) {
          this.active = 3;
        } else {
          this.active = 1;
        }
      } else {
        this.active = 2;
      }
      this.column = value;
      this.getAllProductPackaging();
    }
    public filter(search) {
      this.search = search;
      this.pageNumber = 1;
      this.getAllProductPackaging();
    }
    previous() {
      this.pageNumber--;
      this.getAllProductPackaging();
    }
     next() {
      this.pageNumber++;
      this.getAllProductPackaging();
    }
     selectedPage(event) {
      this.pageNumber = event;
      this.getAllProductPackaging();
    }
    async  onDelete(id){
      const resp = await this.dialog.confirm("Are you sure to delete ", "", false);
      if (resp) {
          this.productservice.deleteProductPackagingById(id)
          .pipe(first())
          .subscribe(productpackagings => {
            this.productpackagings = productpackagings;
            if(this.productpackagings.data !== null)
            this.getAllProductPackaging();      
          });
      }
     
    }
  
  
  }
