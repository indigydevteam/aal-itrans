import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductpackagingListComponent } from './productpackaging-list.component';

describe('ProductpackagingListComponent', () => {
  let component: ProductpackagingListComponent;
  let fixture: ComponentFixture<ProductpackagingListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProductpackagingListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductpackagingListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
