import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageProductpackagingComponent } from './manage-productpackaging.component';

describe('ManageProductpackagingComponent', () => {
  let component: ManageProductpackagingComponent;
  let fixture: ComponentFixture<ManageProductpackagingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ManageProductpackagingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageProductpackagingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
