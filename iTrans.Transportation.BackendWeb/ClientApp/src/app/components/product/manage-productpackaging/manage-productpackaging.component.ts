import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { first } from 'rxjs/operators';
import { ProductService } from 'src/app/services/product/product.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-manage-productpackaging',
  templateUrl: './manage-productpackaging.component.html',
  styleUrls: ['./manage-productpackaging.component.scss']
})
export class ManageProductpackagingComponent implements OnInit {

  form!: FormGroup;
  id!: string;
  isAddMode!: boolean;
  submitted = false;
  image: any;
  uploadedFile: any;
  producPackaging: any = [];

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private productservice: ProductService,
  ) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.params.id;
    this.isAddMode = !this.id;
    this.form = this.formBuilder.group({
      Id: this.id,
      name_TH: ['', Validators.required],
      name_ENG: ['', Validators.required],
      sequence: ['', Validators.required],
      packagingImage: [''],
      specified: [false],
      active: [true],
    }
    );
    if(!this.isAddMode){
      this.productservice.getProductPackagingById(this.id).pipe(first())
       .subscribe(x => {
         this.form.patchValue(x);
         this.form.value.packagingImage = x.file != null ? environment.baseWebContentUrl + '/' + x.file.filePath : "";
         this.image = this.form.value.packagingImage;
         //});
       });
    }
  }

  private async createProductPackaging() {

    const formData = new FormData();
    formData.append("Name_TH", this.form.value.name_TH);
    formData.append("Name_ENG", this.form.value.name_ENG);
    formData.append("Sequence", this.form.value.sequence);
    formData.append("Specified", this.form.value.specified.toString());
    formData.append("Active", this.form.value.active.toString());
    if (this.uploadedFile)
      formData.append("PackagingImage", this.uploadedFile);
    const resp = await this.productservice.createProductPackaging(formData);
    if (resp.succeeded) {
      this.router.navigateByUrl('productpackaging');
    }
  }
  private async updateProductPackaging() {
    const formData = new FormData();
    formData.append("Id", this.id);
    formData.append("Name_TH", this.form.value.name_TH);
    formData.append("Name_ENG", this.form.value.name_ENG);
    formData.append("Sequence", this.form.value.sequence);
    formData.append("Specified", this.form.value.specified.toString());
    formData.append("Active", this.form.value.active.toString());
    if (this.uploadedFile)
      formData.append("PackagingImage", this.uploadedFile);

    const resp = await this.productservice.updateProductPackaging(formData);
    if (resp.succeeded) {
      this.router.navigateByUrl('productpackaging');
    }

    //this.productservice.updateProductPackaging(formData)
    //  .pipe(first())
    //  .subscribe({
    //    next: () => {
    ///      this.router.navigate(['/productpackaging'], { relativeTo: this.route });
    //    }
    //  });
  }

  onSubmit() {
    this.submitted = true;

    if (this.isAddMode) {
      this.createProductPackaging();
    } else {
      this.updateProductPackaging();
    }

  }
  handleFileInput(text, files: FileList) {
    const file = files[0];
    if (text === 'image') {
      const reader = new FileReader();
      reader.onload = e => this.image = reader.result;
      reader.readAsDataURL(file);
      this.uploadedFile = file;
    } 
  }


}
