import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { first } from 'rxjs/operators';
import { ProductTypeList } from '../../../models/product.model';
import { DialogService } from '../../../services/dialog/dialog.service';
import { ProductService } from '../../../services/product/product.service';

@Component({
  selector: 'app-producttype-list',
  templateUrl: './producttype-list.component.html',
  styleUrls: ['./producttype-list.component.scss']
})
export class ProducttypeListComponent implements OnInit {
  public producttypes: Observable<ProductTypeList> | any;

  public count: number = 0;
  public pageSize: number = 10;
  public pageNumber: number = 1;
  public search: string = "";
  constructor(
    private productservice: ProductService,
    private dialog: DialogService
    ) { }
  
    active: number;
    column: string;
    ngOnInit(): void  { 
      this.all();
    }
    public getAllProductType() {
      this.productservice.getAllProductType(this.search, this.pageNumber, this.pageSize, this.column, this.active).subscribe(producttypes => {
        
        this.producttypes = producttypes;
      });
    }
    all() {
      this.active = 1;
      this.column = "all";
      this.getAllProductType();
    }
  
    orderBy(value) {
      if (value == this.column) {
        if (this.active == 1) {
          this.active = 2;
        } else if (this.active == 2) {
          this.active = 3;
        } else {
          this.active = 1;
        }
      } else {
        this.active = 2;
      }
      this.column = value;
      this.getAllProductType();
    }
    public filter(search) {
      this.search = search;
      this.pageNumber = 1;
      this.getAllProductType();
    }
    previous() {
      this.pageNumber--;
      this.getAllProductType();
    }
     next() {
      this.pageNumber++;
      this.getAllProductType();
    }
     selectedPage(event) {
      this.pageNumber = event;
      this.getAllProductType();
    }
     
  async  onDelete(id){
    const resp = await this.dialog.confirm("Are you sure to delete ", "", false);
    if (resp) {
        this.productservice.deleteProductTypeById(id)
        .pipe(first())
        .subscribe(producttypes => {
          this.producttypes = producttypes;
          if(this.producttypes.data !== null)
          this.getAllProductType();      
        });
    }
   
  }
  }
  
