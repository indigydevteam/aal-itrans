import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageProducttypeComponent } from './manage-producttype.component';

describe('ManageProducttypeComponent', () => {
  let component: ManageProducttypeComponent;
  let fixture: ComponentFixture<ManageProducttypeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ManageProducttypeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageProducttypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
