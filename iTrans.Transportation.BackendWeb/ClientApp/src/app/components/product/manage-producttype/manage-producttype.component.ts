import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { first } from 'rxjs/operators';
import { ProductType } from 'src/app/models/product.model';
import { ProductService } from 'src/app/services/product/product.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-manage-producttype',
  templateUrl: './manage-producttype.component.html',
  styleUrls: ['./manage-producttype.component.scss']
})
export class ManageProducttypeComponent implements OnInit {

  form!: FormGroup;
  id!: string;
  isAddMode!: boolean;
  submitted = false;
  image: any;
  uploadedFile: any;
  productType: any = [];
  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private productservice: ProductService,
  ) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.params.id;
    this.isAddMode = !this.id;
    this.form = this.formBuilder.group({
      Id: this.id,
      name_TH: ['', Validators.required],
      name_ENG: ['', Validators.required],
      sequence: ['', Validators.required],
      productImage: [''],
      specified: [false],
      active: [true],
    }
    );
    if (!this.isAddMode) {
      this.productservice.getProductTypeById(this.id).pipe(first())
        .subscribe(x => {
          this.form.patchValue(x);
          this.form.value.productImage = x.file != null ? environment.baseWebContentUrl + '/' + x.file.filePath : "";
          this.image = this.form.value.productImage;
          //});
        });
    }
  }

  private async createProductType() {
    const formData = new FormData();
    formData.append("Name_TH", this.form.value.name_TH);
    formData.append("Name_ENG", this.form.value.name_ENG);
    formData.append("Sequence", this.form.value.sequence);
    formData.append("Active", this.form.value.active.toString());
    formData.append('Specified', this.form.value.specified);
    if (this.uploadedFile)
      formData.append("ProductImage", this.uploadedFile);
    const resp = await this.productservice.createProductType(formData);
    if (resp.succeeded) {
      this.router.navigateByUrl('producttype');
    }
    //this.productservice.createProductType(formData)
    //  .pipe(first())
    //  .subscribe({
    //    next: () => {
    //      this.router.navigate(['/producttype'], { relativeTo: this.route });
    //    }
    //  });
  }
  private async updateProductType() {
    const formData = new FormData();
    formData.append("Id", this.id);
    formData.append("Name_TH", this.form.value.name_TH);
    formData.append("Name_ENG", this.form.value.name_ENG);
    formData.append("Sequence", this.form.value.sequence);
    formData.append("Active", this.form.value.active.toString());
    if (this.uploadedFile)
      formData.append("ProductImage", this.uploadedFile);
    const resp = await this.productservice.updateProductType(formData);
    if (resp.succeeded) {
      this.router.navigateByUrl('carspecification');
    }

    //this.productservice.updateProductType(formData)
    //  .pipe(first())
    //  .subscribe({
    //    next: () => {
    //      this.router.navigate(['/producttype'], { relativeTo: this.route });
    //    }
    //  });
  }

  onSubmit() {
    this.submitted = true;
    if (this.isAddMode) {
      this.createProductType();
    } else {
      this.updateProductType();
    }

  }
  handleFileInput(text, files: FileList) {
    const file = files[0];
    if (text === 'image') {
      const reader = new FileReader();
      reader.onload = e => this.image = reader.result;
      reader.readAsDataURL(file);
      this.uploadedFile = file;
    }
  }

}
