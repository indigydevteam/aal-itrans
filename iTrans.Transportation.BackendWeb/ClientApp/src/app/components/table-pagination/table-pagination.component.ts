import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-table-pagination',
  templateUrl: './table-pagination.component.html',
  styleUrls: ['./table-pagination.component.scss']
})
export class TablePaginationComponent implements OnInit {
  item: any;
  @Input() count: any;
  @Input() page: any;
  @Input() limit: any = 2;
  // tslint:disable-next-line: no-output-on-prefix
  @Output() onPrevious: EventEmitter<any> = new EventEmitter<any>();
  // tslint:disable-next-line: no-output-on-prefix
  @Output() onNext: EventEmitter<any> = new EventEmitter<any>();
  // tslint:disable-next-line: no-output-on-prefix
  @Output() onSelectPage: EventEmitter<any> = new EventEmitter<any>();
  lastPage = 0;
  constructor() { }

  ngOnInit(): void {

  }

  p() {
    this.page--;
    this.onPrevious.emit();
  }

  n() {
    this.page++;
    this.onNext.emit();
  }

  s(page: any) {
    this.page = page;
    this.onSelectPage.emit(this.page);
  }

  getPageNext() {
    // tslint:disable-next-line: radix
    const calc = (Number(this.count) / Number(this.limit)) !== Infinity ? (Number(this.count) / Number(this.limit)) : 0;
    return calc > this.page ? true : false;
  }

  getPageArray() {
    const pages = [];
    // tslint:disable-next-line: radix
    let calc = parseInt((Number(this.count) / Number(this.limit)).toString());
    calc = Number(this.count) / Number(this.limit) > calc ? calc + 1 : calc;
    for (let i = 0; i < calc; i++) {
      pages.push(i + 1);
    }

    return pages;
  }

}