import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DeviceDetectorService } from 'ngx-device-detector';
import { AuthenService } from 'src/app/services/authen/authen.service';
import { ProfileService } from 'src/app/services/profile/profile.service';
import { SignalRService } from 'src/app/services/SignalR/signal-r.service';
import { StorageService } from 'src/app/services/storage/storage.service';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.scss']
})
export class SigninComponent implements OnInit {
  username: any = '';
  password: any = '';
  isSubmit = false;
  device: any;
  constructor(
    private router: Router,
    private api: AuthenService,
    private deviceDetect: DeviceDetectorService,
    private storage: StorageService,
    private profile: ProfileService,
    private signalRServ: SignalRService
  ) { }

  ngOnInit(): void {
  }
  async submit() {
    this.device = this.deviceDetect.getDeviceInfo();
    if (!this.username || !this.password) {
      this.isSubmit = true;
      return null;
    }
    const body: any = {
      grant_type: 'password', // - delegation(O365) password(Local)
      scope: 'profileapi',
      client_id: 'iTrans',
      user_type: 'admin',
      username: this.username,
      password: this.signalRServ.encrypt(this.password, 'chat'),
    };

    try {
      const resp = await this.api.login(body);
      resp['AdminId'] = resp['Id'];
      this.storage.setItem('token', resp);
      await this.profile.GetProfile();
      this.router.navigateByUrl('news');
    } catch (error) {
      alert(error.error.message_en)
    }

  }
}
