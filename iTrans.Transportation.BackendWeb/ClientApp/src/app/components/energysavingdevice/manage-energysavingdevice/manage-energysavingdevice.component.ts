import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { first } from 'rxjs/operators';
import { EnergySavingDeviceService } from 'src/app/services/energysavingdevice/energysavingdevice.service';

@Component({
  selector: 'app-manage-energysavingdevice',
  templateUrl: './manage-energysavingdevice.component.html',
  styleUrls: ['./manage-energysavingdevice.component.scss']
})
export class ManageEnergysavingdeviceComponent implements OnInit {

  form!: FormGroup;
  id!: string;
  isAddMode!: boolean;
  submitted = false;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private energysavingdeviceservice: EnergySavingDeviceService,
  ) { }
  ngOnInit(): void {

    this.id = this.route.snapshot.params.id;
    this.isAddMode = !this.id;
    this.form = this.formBuilder.group({
        Id: this.id,
        name_TH: ['', Validators.required],
        name_ENG: ['', Validators.required],
        sequence: ['', Validators.required],
        specified: [true],
        active: [true]
      }
    );
    if (!this.isAddMode) {
      this.energysavingdeviceservice.getEnergySavingDeviceById(this.id)
        .pipe(first())
        .subscribe(x => this.form.patchValue(x));

    }
  }
  get formControls() { return this.form.controls; }

  private createEnergySavingDevice() {

    this.energysavingdeviceservice.createEnergySavingDevice(this.form.value)
      .pipe(first())
      .subscribe({
        next: () => {
          this.router.navigate(['/energysavingdevice'], { relativeTo: this.route });
        }
      });
  }
  private updateEnergySavingDevice() {
    this.energysavingdeviceservice.updateEnergySavingDevice(this.form.value)
      .pipe(first())
      .subscribe({
        next: () => {
          this.router.navigate(['/energysavingdevice'], { relativeTo: this.route });
        }
      });
  }

  onSubmit() {
    this.submitted = true;
    if (this.form.invalid) {
      return;
    }
    

    if (this.isAddMode) {
      this.createEnergySavingDevice();
    } else {
      this.updateEnergySavingDevice();
    }

  }

}
