import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageEnergysavingdeviceComponent } from './manage-energysavingdevice.component';

describe('ManageEnergysavingdeviceComponent', () => {
  let component: ManageEnergysavingdeviceComponent;
  let fixture: ComponentFixture<ManageEnergysavingdeviceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ManageEnergysavingdeviceComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageEnergysavingdeviceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
