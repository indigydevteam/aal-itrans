import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EnergysavingdeviceListComponent } from './energysavingdevice-list.component';

describe('EnergysavingdeviceListComponent', () => {
  let component: EnergysavingdeviceListComponent;
  let fixture: ComponentFixture<EnergysavingdeviceListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EnergysavingdeviceListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EnergysavingdeviceListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
