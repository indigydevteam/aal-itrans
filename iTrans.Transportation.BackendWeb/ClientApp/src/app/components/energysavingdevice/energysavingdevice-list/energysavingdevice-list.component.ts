import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { first } from 'rxjs/operators';
import { DialogService } from 'src/app/services/dialog/dialog.service';
import { EnergySavingDeviceService } from 'src/app/services/energysavingdevice/energysavingdevice.service';
import { EnergySavingDeviceList } from '../../../models/energysavingdevice.model';

@Component({
  selector: 'app-energysavingdevice-list',
  templateUrl: './energysavingdevice-list.component.html',
  styleUrls: ['./energysavingdevice-list.component.scss']
})
export class EnergysavingdeviceListComponent implements OnInit {
  public energysavingdevices: Observable<EnergySavingDeviceList> | any;
  public count: number = 0;
  public pageSize: number = 10;
  public pageNumber: number = 1;
  public search: string = "";
  active: number;
  column: string;

  constructor(
    private energysavingdeviceservice: EnergySavingDeviceService,
    private dialog: DialogService

    ) { }
    ngOnInit(): void  { 
      this.all();
  }

  all() {
    this.active = 1;
    this.column = "all";
    this.getAllEnergySavingDevice();
  }

    public getAllEnergySavingDevice() {
      this.energysavingdeviceservice.getAllEnergySavingDevice(this.search, this.pageNumber, this.pageSize, this.column, this.active).subscribe(energysavingdevices => {
        this.energysavingdevices = energysavingdevices;
      });
    }
  
    orderBy(value) {
      if (value == this.column) {
        if (this.active == 1) {
          this.active = 2;
        } else if (this.active == 2) {
          this.active = 3;
        } else {
          this.active = 1;
        }
      } else {
        this.active = 2;
      }
      this.column = value;
      this.getAllEnergySavingDevice();
    }
    public filter(search) {
      this.search = search;
      this.pageNumber = 1;
      this.getAllEnergySavingDevice();
    }
    previous() {
      this.pageNumber--;
      this.getAllEnergySavingDevice();
    }
     next() {
      this.pageNumber++;
      this.getAllEnergySavingDevice();
    }
     selectedPage(event) {
      this.pageNumber = event;
      this.getAllEnergySavingDevice();
    }
    async  onDelete(id){
      const resp = await this.dialog.confirm("Are you sure to delete ", "", false);
      if (resp) {
          this.energysavingdeviceservice.deleteEnergySavingDevice(id)
          .pipe(first())
          .subscribe(energysavingdevices => {
            this.energysavingdevices = energysavingdevices;
            if(this.energysavingdevices.data !== null)
            this.getAllEnergySavingDevice();      
          });
      }
     
    }
  
  
  }
  
