import { Component, OnInit } from '@angular/core';
import { AudittailService } from 'src/app/services/audittail/audittail.service';

@Component({
  selector: 'app-audittail-list',
  templateUrl: './audittail-list.component.html',
  styleUrls: ['./audittail-list.component.scss']
})
export class AudittailListComponent implements OnInit  {

  public pageSize: number = 10;
  public pageNumber: number = 1;
  constructor(private audittailservice: AudittailService) { }
  modifiedBy: string = "";
  module:string = "";
  audittail:any=[];
  start: Date;
  end: Date;
  selectModule:any=[];
  selectModifiedBy:any=[];
  active:number;
  column:string;
  ngOnInit(): void {
    this.all();
    this.getAllApplicationLogSelectModifiedby();
    this.getAllApplicationLogSelectModule();
  }
  public getAllApplicationLog() {
    this.audittailservice.getAllApplicationLog(this.module,this.start,this.end,this.modifiedBy, this.pageNumber, this.pageSize, this.column, this.active).subscribe(audittail => {
      this.audittail = audittail;
     console.log(this.selectModule);
    });
  }
  all() {
    this.active = 1;
    this.column = "all";
   
    this.getAllApplicationLog();  
   
    }
  orderBy(value) {
    if (value == this.column) {
      if (this.active == 1) {
        this.active = 2;
      } else if (this.active == 2) {
        this.active = 3;
      } else {
        this.active = 1;
      }
    } else {
      this.active = 2;
    }
    this.column = value;
 
      this.getAllApplicationLog();
    
  }
  public getAllApplicationLogSelectModifiedby() {
    this.audittailservice.getAllApplicationLogSelectModifiedby().subscribe(audittail => {
      this.selectModifiedBy = audittail.data;
      console.log(this.selectModule);
  
    });
  }
  public getAllApplicationLogSelectModule() {
    this.audittailservice.getAllApplicationLogSelectModule().subscribe(audittail => {
      this.selectModule = audittail.data;
      console.log(this.selectModule);
  
    });
  }
  
  public filter() {
    this.pageNumber = 1;
    this.getAllApplicationLog();

  }
  previous() {
    this.pageNumber--;
    this.getAllApplicationLog();
  }
   next() {
    this.pageNumber++;
    this.getAllApplicationLog();
  }
   selectedPage(event) {
    this.pageNumber = event;
    this.getAllApplicationLog();
  }
}
