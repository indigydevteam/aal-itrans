import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AudittailListComponent } from './audittail-list.component';

describe('AudittailListComponent', () => {
  let component: AudittailListComponent;
  let fixture: ComponentFixture<AudittailListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AudittailListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AudittailListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
