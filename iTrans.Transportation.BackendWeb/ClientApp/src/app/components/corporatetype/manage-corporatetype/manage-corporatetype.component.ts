import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { first } from 'rxjs/operators';
import { CorporateTypeService } from 'src/app/services/corporatetype/corporatetype';

@Component({
  selector: 'app-manage-corporatetype',
  templateUrl: './manage-corporatetype.component.html',
  styleUrls: ['./manage-corporatetype.component.scss']
})
export class ManageCorporatetypeComponent implements OnInit {
  form!: FormGroup;
  id!: string;
  isAddMode!: boolean;
  submitted = false;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private corporateservice: CorporateTypeService,
  ) { }
  ngOnInit(): void {

    this.id = this.route.snapshot.params.id;
    this.isAddMode = !this.id;
    this.form = this.formBuilder.group({
      Id: this.id,
      name_TH: ['', Validators.required],
      name_ENG: ['', Validators.required],
      // sequence: ['', Validators.required],
      active: [true],
      specified: [true]
    }
    );
    if (!this.isAddMode) {
      this.corporateservice.getCorporateTypeById(this.id)
        .pipe(first())
        .subscribe(x => this.form.patchValue(x));

    }
  }
  get formControls() { return this.form.controls; }

  private createCorporateType() {

    this.corporateservice.createCorporateType(this.form.value)
      .pipe(first())
      .subscribe({
        next: () => {
          this.router.navigate(['/corporatetype'], { relativeTo: this.route });
        }
      });
  }
  private updateCorporateType() {
    this.corporateservice.updateCorporateType(this.form.value)
      .pipe(first())
      .subscribe({
        next: () => {
          this.router.navigate(['/corporatetype'], { relativeTo: this.route });
        }
      });
  }

  onSubmit() {
    this.submitted = true;
    if (this.form.invalid) {
      return;
    }


    if (this.isAddMode) {
      this.createCorporateType();
    } else {
      this.updateCorporateType();
    }

  }

}
