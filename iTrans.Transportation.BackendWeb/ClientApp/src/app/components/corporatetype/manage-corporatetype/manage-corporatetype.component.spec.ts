import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageCorporatetypeComponent } from './manage-corporatetype.component';

describe('ManageCorporatetypeComponent', () => {
  let component: ManageCorporatetypeComponent;
  let fixture: ComponentFixture<ManageCorporatetypeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ManageCorporatetypeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageCorporatetypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
