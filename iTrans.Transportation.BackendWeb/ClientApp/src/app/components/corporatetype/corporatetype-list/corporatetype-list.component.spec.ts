import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CorporatetypeListComponent } from './corporatetype-list.component';

describe('CorporatetypeListComponent', () => {
  let component: CorporatetypeListComponent;
  let fixture: ComponentFixture<CorporatetypeListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CorporatetypeListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CorporatetypeListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
