import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { first } from 'rxjs/operators';
import { CorporateTypeList } from 'src/app/models/corporatetype.model';
import { CorporateTypeService } from 'src/app/services/corporatetype/corporatetype';
import { DialogService } from 'src/app/services/dialog/dialog.service';

@Component({
  selector: 'app-corporatetype-list',
  templateUrl: './corporatetype-list.component.html',
  styleUrls: ['./corporatetype-list.component.scss']
})
export class CorporatetypeListComponent implements OnInit {

  public corporatetypes: Observable<CorporateTypeList> | any;
  public count: number = 0;
  public pageSize: number = 10;
  public pageNumber: number = 1;
  public search: string = "";

  constructor(
    private corporatetypeservice: CorporateTypeService,
    private dialog: DialogService
  ) { }
  active: number;
  column: string;
  ngOnInit(): void {
    this.all();
  }

  all() {
    this.active = 1;
    this.column = "all";
    this.getAllCorporateType();
  }
  public getAllCorporateType() {
    this.corporatetypeservice.getAllCorporateType(this.search, this.pageNumber, this.pageSize, this.column, this.active).subscribe(corporatetypes => {
      this.corporatetypes = corporatetypes;
      console.log( this.corporatetypes);
    });
  }

  orderByType(value) {
    if (value == this.column) {
      if (this.active == 1) {
        this.active = 2;
      } else if (this.active == 2) {
        this.active = 3;
      } else {
        this.active = 1;
      }
    } else {
      this.active = 2;
    }
    this.column = value;
    this.getAllCorporateType();
  }

  orderByDate(value) {
    if (value == this.column) {
      if (this.active == 1) {
        this.active = 2;
      } else if (this.active == 2) {
        this.active = 3;
      } else {
        this.active = 1;
      }
    } else {
      this.active = 2;
    }
    this.column = value;
    this.getAllCorporateType();
  }

  public filter() {
    this.pageNumber = 1;
    this.getAllCorporateType();
  }
  previous() {
    this.pageNumber--;
    this.getAllCorporateType();
  }
  next() {
    this.pageNumber++;
    this.getAllCorporateType();
  }
  selectedPage(event) {
    this.pageNumber = event;
    this.getAllCorporateType();
  }
  async onDelete(id) {
    const resp = await this.dialog.confirm("Are you sure to delete ", "", false);
    if (resp) {
      this.corporatetypeservice.deleteCorporateTypeById(id)
        .pipe(first())
        .subscribe(orderingcancelstatuses => {
          this.corporatetypes = orderingcancelstatuses;
          if (this.corporatetypes.data !== null)
            this.getAllCorporateType();
        });
    }

  }

}
