import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { first } from 'rxjs/operators';
import { CarList, CarLists } from 'src/app/models/car.model';
import { CarService } from 'src/app/services/car/car.service';
import { DialogService } from 'src/app/services/dialog/dialog.service';
import { ColDef } from 'ag-grid-community';
@Component({
  selector: 'app-carlist-list',
  templateUrl: './carlist-list.component.html',
  styleUrls: ['./carlist-list.component.scss']
})
export class CarlistListComponent implements OnInit {
  
  public carlists: Observable<CarList> | any;

  public count: number = 0;
  public pageSize: number = 10;
  public pageNumber: number = 1;
  public search: string = "";
  public defaultColDef:any;
  constructor(
    private cartypeservice: CarService,
    private dialog: DialogService
    ) { }
  
    active: number;
    column: string ;

    ngOnInit(): void {
    this.all();
  }
  public getAllCarList() {
    this.cartypeservice.getAllCarList(this.search, this.pageNumber, this.pageSize, this.column, this.active).subscribe(carlists => {
      this.carlists = carlists;
    });
  }
  all() {
    this.active = 1;
    this.column = "all";
    this.getAllCarList();
  }

  orderBy(value) {
    if (value == this.column) {
      if (this.active == 1) {
        this.active = 2;
      } else if (this.active == 2) {
        this.active = 3;
      } else {
        this.active = 1;
      }
    } else {
      this.active = 2;
    }
    this.column = value;
    this.getAllCarList();
  }


  public filter() {
    this.pageNumber = 1;
    this.getAllCarList();
  }
  
  previous() {
    this.pageNumber--;
    this.getAllCarList();
  }
   next() {
    this.pageNumber++;
    this.getAllCarList();
  }
   selectedPage(event) {
    this.pageNumber = event;
    this.getAllCarList();
  }
  
  
  async  onDelete(id){
    const resp = await this.dialog.confirm("Are you sure to delete ", "", false);
    if (resp) {
        this.cartypeservice.deleteCarListById(id)
        .pipe(first())
        .subscribe(carlists => {
          this.carlists = carlists;
          if(this.carlists.data !== null)
          this.getAllCarList();      
        });
    }
   
  }
}
