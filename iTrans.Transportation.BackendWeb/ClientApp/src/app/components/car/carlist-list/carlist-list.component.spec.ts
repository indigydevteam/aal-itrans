import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CarlistListComponent } from './carlist-list.component';

describe('CarlistListComponent', () => {
  let component: CarlistListComponent;
  let fixture: ComponentFixture<CarlistListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CarlistListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CarlistListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
