import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageCarspecificationComponent } from './manage-carspecification.component';

describe('ManageCarspecificationComponent', () => {
  let component: ManageCarspecificationComponent;
  let fixture: ComponentFixture<ManageCarspecificationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ManageCarspecificationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageCarspecificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
