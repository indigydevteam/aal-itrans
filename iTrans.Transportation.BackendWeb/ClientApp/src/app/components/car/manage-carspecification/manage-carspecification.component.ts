import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { first } from 'rxjs/operators';
import { CarList, CarType } from 'src/app/models/car.model';
import { CarService } from 'src/app/services/car/car.service';
import { environment } from '../../../../environments/environment';

@Component({
  selector: 'app-manage-carspecification',
  templateUrl: './manage-carspecification.component.html',
  styleUrls: ['./manage-carspecification.component.scss']
})
export class ManageCarspecificationComponent implements OnInit {

  form!: FormGroup;
  id!: string;
  isAddMode!: boolean;
  submitted = false;
  //carlist: CarList[] | any;
  cartypes: CarType[] | any;
  image: any;
  uploadedFile: any;
  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private carservice: CarService,
  ) { }
  ngOnInit(): void {

    this.id = this.route.snapshot.params.id;
    this.isAddMode = !this.id;
    this.form = this.formBuilder.group({
      Id: this.id,
      // carListId:['', Validators.required],
      carTypeId: [''],
      name_TH: ['', Validators.required],
      name_ENG: ['', Validators.required],
      sequence: ['', Validators.required],
      specifiedTemperature: [false],
      specifiedEnergySavingDevice: [false],
      carImage: [''],
      active: [true],
    }
    );
    this.GetCarTypes();
    if (!this.isAddMode) {
      this.carservice.getCarSpecificationById(this.id)
        .pipe(first())
        .subscribe(x => {
          this.form.patchValue(x);
          //this.onChangeCarType();
          this.form.value.carImage = x.file != null ? environment.baseWebContentUrl + '/' + x.file.filePath : "";
          this.image = this.form.value.carImage;
          //});
        });
    }
  }
  get formControls() { return this.form.controls; }

  private async createCarSpecification() {
    const formData = new FormData();
    formData.append('Name_TH', this.form.value.name_TH);
    formData.append('Name_ENG', this.form.value.name_ENG);
    formData.append('CarTypeId', this.form.value.carTypeId);
    formData.append('Sequence', this.form.value.sequence);
    formData.append('Active', this.form.value.active);
    formData.append('SpecifiedTemperature', this.form.value.specifiedTemperature);
    formData.append('SpecifiedEnergySavingDevice', this.form.value.specifiedEnergySavingDevice);
    if (this.uploadedFile)
      formData.append('ImageFile', this.uploadedFile);
    const resp = await this.carservice.createCarSpecification(formData);
    if (resp.succeeded) {
      this.router.navigateByUrl('carspecification');
    }

  }
  private async updateCarSpecification() {
    const formData = new FormData();
    formData.append('Id', this.id);
    formData.append('Name_TH', this.form.value.name_TH);
    formData.append('Name_ENG', this.form.value.name_ENG);
    formData.append('CarTypeId', this.form.value.carTypeId);
    formData.append('Sequence', this.form.value.sequence);
    formData.append('Active', this.form.value.active);
    formData.append('SpecifiedTemperature', this.form.value.specifiedTemperature);
    formData.append('SpecifiedEnergySavingDevice', this.form.value.specifiedEnergySavingDevice);
    if (this.uploadedFile)
      formData.append('ImageFile', this.uploadedFile);
    const resp = await this.carservice.updateCarSpecification(formData);
    if (resp.succeeded) {
      this.router.navigateByUrl('carspecification');
    }
    //this.carservice.updateCarSpecification(this.form.value)
    //  .pipe(first())
    //  .subscribe({
    //    next: () => {
    //      this.router.navigate(['/carspecification'], { relativeTo: this.route });
    //    }
    //  });

  }
  onSubmit() {
    this.submitted = true;
    if (this.form.invalid) {
      return;
    }

    if (this.isAddMode) {
      this.createCarSpecification();
    } else {
      this.updateCarSpecification();
    }
  }
  public GetCarTypes() {
    this.carservice.GetAllCarTypeQuery().subscribe(cartypes => {
      this.cartypes = cartypes;
    });
  }
  //onChangeCarType() {
  //  this.carlist = null;
  //  this.carservice.GetCarListByType(this.form.value.carTypeId).subscribe(carlist => {
  //  this.carlist = carlist;
  //  });
  //}
  handleFileInput(text, files: FileList) {
    const file = files[0];
    if (text === 'image') {
      const reader = new FileReader();
      reader.onload = e => this.image = reader.result;
      reader.readAsDataURL(file);
      this.uploadedFile = file;
      //this.image = file;
    }
  }
}
