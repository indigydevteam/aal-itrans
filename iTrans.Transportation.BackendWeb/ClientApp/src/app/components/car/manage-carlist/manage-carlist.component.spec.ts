import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageCarlistComponent } from './manage-carlist.component';

describe('ManageCarlistComponent', () => {
  let component: ManageCarlistComponent;
  let fixture: ComponentFixture<ManageCarlistComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ManageCarlistComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageCarlistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
