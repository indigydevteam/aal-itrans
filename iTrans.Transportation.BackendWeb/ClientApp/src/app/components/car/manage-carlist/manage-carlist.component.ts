import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { first } from 'rxjs/operators';
import { CarList } from 'src/app/models/car.model';
import { CarType } from 'src/app/models/car.model';
import { CarService } from 'src/app/services/car/car.service';

@Component({
  selector: 'app-manage-carlist',
  templateUrl: './manage-carlist.component.html',
  styleUrls: ['./manage-carlist.component.scss']
})
export class ManageCarlistComponent implements OnInit {

  
  form!: FormGroup;
  id!: string;
  isAddMode!: boolean;
  submitted = false;
  cartype: CarType[] | any;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private carservice: CarService,
  ) { }
  ngOnInit(): void {
    this.getAllCarType();
    this.id = this.route.snapshot.params.id;
    this.isAddMode = !this.id;
    this.form = this.formBuilder.group({
        Id: this.id,
        carTypeId:['', Validators.required],
        name_TH: ['', Validators.required],
        name_ENG: ['', Validators.required],
        sequence: ['', Validators.required],
        active: [true]
      }
    );
    if (!this.isAddMode) {
      this.carservice.getCarListById(this.id)
        .pipe(first())
        .subscribe(x => this.form.patchValue(x));

    }
  }
  get formControls() { return this.form.controls; }

  private createCarList() {

    this.carservice.createCarList(this.form.value)
      .pipe(first())
      .subscribe({
        next: () => {
          this.router.navigate(['/carlist'], { relativeTo: this.route });
        }
      });
  }
  private updateCarList() {
    this.carservice.updateCarList(this.form.value)
      .pipe(first())
      .subscribe({
        next: () => {
          this.router.navigate(['/carlist'], { relativeTo: this.route });
        }
      });
  }

  onSubmit() {
    this.submitted = true;
    if (this.form.invalid) {
      return;
    }

    if (this.isAddMode) {
      this.createCarList();
    } else {
      this.updateCarList();
    }

  }
  public getAllCarType() {
    this.carservice.GetAllCarTypeQuery().subscribe(cartype => {
      this.cartype = cartype;
    });
  }
}
