import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CarfeatureListComponent } from './carfeature-list.component';

describe('CarfeatureListComponent', () => {
  let component: CarfeatureListComponent;
  let fixture: ComponentFixture<CarfeatureListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CarfeatureListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CarfeatureListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
