import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { first } from 'rxjs/operators';
import { CarFeature, CarFeatureList, CarSpecification, CarSpecificationList } from 'src/app/models/car.model';
import { CarService } from 'src/app/services/car/car.service';
import { DialogService } from 'src/app/services/dialog/dialog.service';

@Component({
  selector: 'app-carfeature-list',
  templateUrl: './carfeature-list.component.html',
  styleUrls: ['./carfeature-list.component.scss']
})
export class CarfeatureListComponent implements OnInit {
  public carfeatures: Observable<CarFeatureList> | any;

  public count: number= 0;
  public pageSize: number = 10;
  public pageNumber: number = 1;
  public search: string = "";
  constructor(
    private carservice: CarService,
    private dialog: DialogService
    ) { }

  active: number;
  column: string ;

  ngOnInit(): void {
  this.all();
}
public getAllCarFeature() {
  this.carservice.getAllCarFeature(this.search, this.pageNumber, this.pageSize, this.column, this.active).subscribe(carfeatures => {
    this.carfeatures = carfeatures;
  });
}
all() {
  this.active = 1;
  this.column = "all";
  this.getAllCarFeature();
}

orderBy(value) {
  if (value == this.column) {
    if (this.active == 1) {
      this.active = 2;
    } else if (this.active == 2) {
      this.active = 3;
    } else {
      this.active = 1;
    }
  } else {
    this.active = 2;
  }
  this.column = value;
  this.getAllCarFeature();
}

  public filter(search) {
    this.search = search;
    this.pageNumber = 1;
    this.getAllCarFeature();

  }
  
  previous() {
    this.pageNumber--;
    this.getAllCarFeature();
  }
   next() {
    this.pageNumber++;
    this.getAllCarFeature();
  }
   selectedPage(event) {
    this.pageNumber = event;
    this.getAllCarFeature();
  }
  
  async  onDelete(id){
    const resp = await this.dialog.confirm("Are you sure to delete ", "", false);
    if (resp) {
        this.carservice.deleteCarFeatureById(id)
        .pipe(first())
        .subscribe(carfeatures => {
          this.carfeatures = carfeatures;
          if(this.carfeatures.data !== null)
          this.getAllCarFeature();      
        });
    }
   
  }


}
