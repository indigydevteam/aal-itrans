import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { first } from 'rxjs/operators';
import { DialogService } from '../../../services/dialog/dialog.service';
import { CarTypeList } from '../../../models/car.model';
import { CarService } from '../../../services/car/car.service';

@Component({
  selector: 'app-cartype-list',
  templateUrl: './cartype-list.component.html',
  styleUrls: ['./cartype-list.component.scss']
})
export class CartypeListComponent implements OnInit {

  public cartypes: Observable<CarTypeList> | any;
  public count: number =0;
  public pageSize: number = 10;
  public pageNumber: number = 1;
  public search: string = "";
  constructor(
    private cartypeservice: CarService,
    private dialog: DialogService
    ) { }
    active: number;
    column: string ;
  ngOnInit(): void {
    this.all();
  }
  public getAllCarType() {
    this.cartypeservice.getAllCarType(this.search, this.pageNumber, this.pageSize, this.column, this.active).subscribe(cartypes => {
      this.cartypes = cartypes;
      this.count = cartypes.data.itemCount;

    });
  }
  all() {
    this.active = 1;
    this.column = "all";
    this.getAllCarType();
  }
  
  orderBy(value) {
    if (value == this.column) {
      if (this.active == 1) {
        this.active = 2;
      } else if (this.active == 2) {
        this.active = 3;
      } else {
        this.active = 1;
      }
    } else {
      this.active = 2;
    }
    this.column = value;
    this.getAllCarType();
  }
  public filter(search) {
    this.search = search;
    this.pageNumber = 1;
    this.getAllCarType();
  }
  previous() {
    this.pageNumber--;
    this.getAllCarType();
  }
   next() {
    this.pageNumber++;
    this.getAllCarType();
  }
   selectedPage(event) {
    this.pageNumber = event;
    this.getAllCarType();
  }
  async  onDelete(id){
    const resp = await this.dialog.confirm("Are you sure to delete ", "", false);
    if (resp) {
        this.cartypeservice.deleteTypeById(id)
        .pipe(first())
        .subscribe(cartypes => {
          this.cartypes = cartypes;
          if(this.cartypes.data !== null)
          this.getAllCarType();      
        });
    }
   
  }


}
