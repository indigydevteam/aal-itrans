import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CardescriptionListComponent } from './cardescription-list.component';

describe('CardescriptionListComponent', () => {
  let component: CardescriptionListComponent;
  let fixture: ComponentFixture<CardescriptionListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CardescriptionListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CardescriptionListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
