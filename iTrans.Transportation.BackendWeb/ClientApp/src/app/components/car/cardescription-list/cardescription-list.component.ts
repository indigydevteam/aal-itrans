import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { first } from 'rxjs/operators';
import { CarDescription, CarDescriptionList, CarList } from 'src/app/models/car.model';
import { CarService } from 'src/app/services/car/car.service';
import { DialogService } from 'src/app/services/dialog/dialog.service';

@Component({
  selector: 'app-cardescription-list',
  templateUrl: './cardescription-list.component.html',
  styleUrls: ['./cardescription-list.component.scss']
})
export class CardescriptionListComponent implements OnInit {
  public cardescriptions: Observable<CarDescriptionList> | any;
  public count: number = 0;
  public pageSize: number = 10;
  public pageNumber: number = 1;
  public search: string = "";
  constructor(
    private carservice: CarService,
    private dialog: DialogService
    ) { }
    active: number;
    column: string ;

    ngOnInit(): void {
    this.all();
  }
  public getAllCarDescription() {
    this.carservice.getAllCarDescription(this.search, this.pageNumber, this.pageSize, this.column, this.active).subscribe(cardescriptions => {
      this.cardescriptions = cardescriptions;
    });
  }
  all() {
    this.active = 1;
    this.column = "all";
    this.getAllCarDescription();
  }
  orderBy(value) {
    if (value == this.column) {
      if (this.active == 1) {
        this.active = 2;
      } else if (this.active == 2) {
        this.active = 3;
      } else {
        this.active = 1;
      }
    } else {
      this.active = 2;
    }
    this.column = value;
    this.getAllCarDescription();
  }
  
  public filter(search) {
    this.search = search;
    this.pageNumber = 1;
    this.getAllCarDescription();

  }
  previous() {
    this.pageNumber--;
    this.getAllCarDescription();
  }
   next() {
    this.pageNumber++;
    this.getAllCarDescription();
  }
   selectedPage(event) {
    this.pageNumber = event;
    this.getAllCarDescription();
  }

  async  onDelete(id){
    const resp = await this.dialog.confirm("Are you sure to delete ", "", false);
    if (resp) {
        this.carservice.DeleteCarDescription(id)
        .pipe(first())
        .subscribe(cardescriptions => {
          this.cardescriptions = cardescriptions;
          if(this.cardescriptions.data !== null)
          this.getAllCarDescription();      
        });
    }
   
  }
}
