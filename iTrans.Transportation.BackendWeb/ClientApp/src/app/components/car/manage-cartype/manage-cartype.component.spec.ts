import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageCartypeComponent } from './manage-cartype.component';

describe('ManageCartypeComponent', () => {
  let component: ManageCartypeComponent;
  let fixture: ComponentFixture<ManageCartypeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ManageCartypeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageCartypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
