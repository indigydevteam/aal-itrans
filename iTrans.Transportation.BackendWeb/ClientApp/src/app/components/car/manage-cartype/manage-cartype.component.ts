import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { first } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { CarService } from '../../../services/car/car.service';

@Component({
  selector: 'app-manage-cartype',
  templateUrl: './manage-cartype.component.html',
  styleUrls: ['./manage-cartype.component.scss']
})
export class ManageCartypeComponent implements OnInit {

  // form!: FormGroup;
  id!: string;
  isAddMode!: boolean;
  submitted = false;
  
  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private cartypeservice: CarService,
  ) { }

  form: FormGroup;
  carType:any=[];
  image:any;
  ngOnInit(): void {
    this.id = this.route.snapshot.params.id;
    this.isAddMode = !this.id;
    this.form = this.formBuilder.group({
      Id: this.id,
      name_TH: ['', Validators.required],
      name_ENG: ['', Validators.required],
      sequence: ['', Validators.required],
      isContainer: [false],
      active: [true]
    }
    );
    if (!this.isAddMode) {
      this.cartypeservice.getCarTypeById(this.id)
        .pipe(first())
        .subscribe(x => this.form.patchValue(x));
      //this.cartypeservice.getCarTypeById(this.id).subscribe(carType => {
      //  this.carType = carType;
      //  this.form = {
      //    id:this.id,
      //    name_TH : this.carType.name_TH,
      //    name_ENG : this.carType.name_ENG,
      //    sequence : this.carType.sequence,
      //    active : this.carType.active,
      //    carImage : this.carType.file != null ? environment.baseWebContentUrl+'/'+this.carType.file.filePath:"",
      //  }; 
      // this.image =this.form.carImage;
      //});
    }
  }

  get formControls() { return this.form.controls; }

  private createCarType() {
    console.log(this.form);
    this.cartypeservice.createCarType(this.form.value)
      .pipe(first())
      .subscribe({
        next: () => {
          this.router.navigate(['/cartype'], { relativeTo: this.route });
        }
      });
    //const formData = new FormData();
    //formData.append("Name_TH", this.form.name_TH);
    //formData.append("Name_ENG", this.form.name_ENG);
    //formData.append("Sequence", this.form.sequence);
    //formData.append("Active", this.form.active.toString());
    ////formData.append("CarTypeImage", this.form.carImage);
    //this.cartypeservice.createCarType(formData)
    //  .pipe(first())
    //  .subscribe({
    //    next: () => {
    //      this.router.navigate(['/cartype'], { relativeTo: this.route });
    //    }
    //  });
  }
  private updateCarType() {
    this.cartypeservice.updateCarType(this.form.value)
      .pipe(first())
      .subscribe({
        next: () => {
          this.router.navigate(['/cartype'], { relativeTo: this.route });
        }
      });
    //const formData = new FormData();
    //formData.append("Id", this.id);
    //formData.append("Name_TH", this.form.name_TH);
    //formData.append("Name_ENG", this.form.name_ENG);
    //formData.append("Sequence", this.form.sequence);
    //formData.append("Active", this.form.active.toString());
    ////formData.append("CarTypeImage", this.form.carImage);
    //this.cartypeservice.updateCarType(formData)
    //  .pipe(first())
    //  .subscribe({
    //    next: () => {
    //      this.router.navigate(['/cartype'], { relativeTo: this.route });
    //    }
    //  });
  }

  onSubmit() {
    this.submitted = true;

    if (this.isAddMode) {
      this.createCarType();
    } else {
      this.updateCarType();
    }

  }
  //handleFileInput(text, files: FileList) {
  //  const file = files[0];
  //  if (text === 'image') {
  //    const reader = new FileReader();
  //    reader.onload = e => this.image = reader.result;
  //    reader.readAsDataURL(file);
  //    this.form.carImage = file;
  //  } 
  //}
}
