import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { first } from 'rxjs/operators';
import { CarSpecification, CarSpecificationList } from 'src/app/models/car.model';
import { CarService } from 'src/app/services/car/car.service';
import { DialogService } from 'src/app/services/dialog/dialog.service';

@Component({
  selector: 'app-carspecification-list',
  templateUrl: './carspecification-list.component.html',
  styleUrls: ['./carspecification-list.component.scss']
})
export class CarspecificationListComponent implements OnInit {

  public carspecifications: Observable<CarSpecificationList> | any;

  public pageSize: number = 10;
  public pageNumber: number = 1;
  public search: string = "";
  active: number;
  column: string;
  constructor(
    private carservice: CarService,
    private dialog: DialogService

    ) { }
  
    ngOnInit(): void {
    this.all();
  }
  public getAllCarSpecification() {
    this.carservice.getAllCarSpecification(this.search, this.pageNumber, this.pageSize, this.column, this.active).subscribe(carspecifications => {
      console.log(carspecifications);
      this.carspecifications = carspecifications;
    });
  }
  all() {
    this.active = 1;
    this.column = "all";
    this.getAllCarSpecification();
  }
  
  orderBy(value) {
    if (value == this.column) {
      if (this.active == 1) {
        this.active = 2;
      } else if (this.active == 2) {
        this.active = 3;
      } else {
        this.active = 1;
      }
    } else {
      this.active = 2;
    }
    this.column = value;
    this.getAllCarSpecification();
  }
 
  public filter(search) {
    this.search = search;
    this.pageNumber = 1;
    this.getAllCarSpecification();
  }
  previous() {
    this.pageNumber--;
    this.getAllCarSpecification();
  }
   next() {
    this.pageNumber++;
    this.getAllCarSpecification();
  }
   selectedPage(event) {
    this.pageNumber = event;
    this.getAllCarSpecification();
  }

  
  async  onDelete(id){
    const resp = await this.dialog.confirm("Are you sure to delete ", "", false);
    if (resp) {
        this.carservice.DeleteCarSpecificationById(id)
        .pipe(first())
        .subscribe(carspecifications => {
          this.carspecifications = carspecifications;
          if(this.carspecifications.data !== null)
          this.getAllCarSpecification();      
        });
    }
   
  }
}
