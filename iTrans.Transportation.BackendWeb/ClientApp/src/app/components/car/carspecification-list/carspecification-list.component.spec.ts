import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CarspecificationListComponent } from './carspecification-list.component';

describe('CarspecificationListComponent', () => {
  let component: CarspecificationListComponent;
  let fixture: ComponentFixture<CarspecificationListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CarspecificationListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CarspecificationListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
