import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CarprofileListComponent } from './carprofile-list.component';

describe('CarprofileListComponent', () => {
  let component: CarprofileListComponent;
  let fixture: ComponentFixture<CarprofileListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CarprofileListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CarprofileListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
