import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs/operators';
import { DialogService } from 'src/app/services/dialog/dialog.service';
import { DriverService } from 'src/app/services/driver/driver.service';

@Component({
  selector: 'app-carprofile-list',
  templateUrl: './carprofile-list.component.html',
  styleUrls: ['./carprofile-list.component.scss']
})
export class CarprofileListComponent implements OnInit{

 
  public pageSize: number = 10;
  public pageNumber: number = 1;
  public search: string = "";
  public cartype: string = "";
  public show: number = 1;


  constructor(
    private drivercarservice: DriverService,
    private dialog: DialogService
    ) { }
  drivercars:any=[];
  active:number;
  column:string;
  id:string="";
  ngOnInit(): void {
    this.all();
  }

  public getAllDriverCar() {
    this.drivercarservice.getAllDriverCar(this.search, this.cartype,this.pageNumber, this.pageSize, this.column, this.active).subscribe(drivercars => {
      this.drivercars = drivercars;

    });
  }
  public getDriverCarByOwnerId(id) {
    this.id = id;
    this.search="";
    this.drivercarservice.getDriverCarByOwnerId(this.id,this.search,this.pageNumber, this.pageSize, this.column, this.active).subscribe(drivercars => {
      this.drivercars = drivercars;
      this.cartype="";
      
      this.show = 2;

    });
  }

  
  all() {
    this.active = 1;
    this.column = "all";
    if(this.show == 2){
      this.getDriverCarByOwnerId(this.id);  
    }else{
      this.getAllDriverCar();
    }
    }
  orderBy(value) {
    if (value == this.column) {
      if (this.active == 1) {
        this.active = 2;
      } else if (this.active == 2) {
        this.active = 3;
      } else {
        this.active = 1;
      }
    } else {
      this.active = 2;
    }
    this.column = value;
    if(this.show == 2){
      this.getDriverCarByOwnerId(this.id);  
    }else{
      this.getAllDriverCar();
    }
  }
  public filter() {
    console.log(this.show)
    this.pageNumber = 1;
    if(this.show == 2){
      this.getDriverCarByOwnerId(this.id);  
    }else{
      this.getAllDriverCar();
    }

  }
  previous() {
    this.pageNumber--;
    this.getAllDriverCar();
  }
   next() {
    this.pageNumber++;
    this.getAllDriverCar();
  }
   selectedPage(event) {
    this.pageNumber = event;
    this.getAllDriverCar();
  }
  async onDelete(id) {

    const resp = await this.dialog.confirm("Are you sure to delete ", "", false);
    if (resp) {

        this.drivercarservice.DeleteDriverCar(id)
        .pipe(first())
        .subscribe(drivercars => {
          this.drivercars = drivercars;
          if(this.drivercars.data !== null)
          this.getAllDriverCar();      
        });
    }
   
  }
}
