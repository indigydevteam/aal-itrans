import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { first } from 'rxjs/operators';
import { CarList, CarType } from 'src/app/models/car.model';
import { CarService } from 'src/app/services/car/car.service';

@Component({
  selector: 'app-manage-cardescription',
  templateUrl: './manage-cardescription.component.html',
  styleUrls: ['./manage-cardescription.component.scss']
})
export class ManageCardescriptionComponent implements OnInit {
  
  form!: FormGroup;
  id!: string;
  isAddMode!: boolean;
  submitted = false;
  cartype: CarType[] | any;
  carlist: CarList[] | any;


  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private carservice: CarService,
  ) { }
  ngOnInit(): void {
    this.getAllCarType();
    this.id = this.route.snapshot.params.id;
    this.isAddMode = !this.id;
    this.form = this.formBuilder.group({
        Id: this.id,
        carListId:['', Validators.required],
        carTypeId:[''],
        name_TH: ['', Validators.required],
        name_ENG: ['', Validators.required],
        sequence: ['', Validators.required],
        active: [true],
        specified: [true],

      }
    );
    if (!this.isAddMode) {
      this.carservice.getCarDescriptionById(this.id)
        .pipe(first())
        .subscribe(x => {
          this.form.patchValue(x);
          this.onChangeCarType();
        });
    }
  }
  get formControls() { return this.form.controls; }

  private createCarDescription() {

    this.carservice.createCarDescription(this.form.value)
      .pipe(first())
      .subscribe({
        next: () => {
          this.router.navigate(['/cardescription'], { relativeTo: this.route });
        }
      });
  }
  private updateCarDescription() {
    this.carservice.updateCarDescription(this.form.value)
      .pipe(first())
      .subscribe({
        next: () => {
          this.router.navigate(['/cardescription'], { relativeTo: this.route });

        }
      });
  }
  onSubmit() {
    this.submitted = true;
    if (this.form.invalid) {
      return;
    }

    if (this.isAddMode) {
      this.createCarDescription();
    } else {
      this.updateCarDescription();
    }

  }
  public getAllCarType() {
    this.carservice.GetAllCarTypeQuery().subscribe(cartype => {
      this.cartype = cartype;
      console.log(this.cartype);
    });
  }
  onChangeCarType() {
    this.carlist = null;
    this.carservice.GetCarListByType(this.form.value.carTypeId).subscribe(carlist => {
    this.carlist = carlist;
    });  
  }
}
