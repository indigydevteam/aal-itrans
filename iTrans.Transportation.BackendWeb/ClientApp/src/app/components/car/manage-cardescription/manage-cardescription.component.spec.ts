import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageCardescriptionComponent } from './manage-cardescription.component';

describe('ManageCardescriptionComponent', () => {
  let component: ManageCardescriptionComponent;
  let fixture: ComponentFixture<ManageCardescriptionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ManageCardescriptionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageCardescriptionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
