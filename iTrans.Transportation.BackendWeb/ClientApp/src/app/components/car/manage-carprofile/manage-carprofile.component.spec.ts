import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageCarprofileComponent } from './manage-carprofile.component';

describe('ManageCarprofileComponent', () => {
  let component: ManageCarprofileComponent;
  let fixture: ComponentFixture<ManageCarprofileComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ManageCarprofileComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageCarprofileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
