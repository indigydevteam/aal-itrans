
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { first } from 'rxjs/operators';
import { CarService } from 'src/app/services/car/car.service';
import { CountryService } from 'src/app/services/country/country.service';
import { RegionService } from 'src/app/services/region/region.service';
import { ProvinceService } from 'src/app/services/province/province.service';
import { DistrictService } from 'src/app/services/district/district.service';
import { DriverService } from 'src/app/services/driver/driver.service';
import { environment } from 'src/environments/environment';
import { DialogService } from 'src/app/services/dialog/dialog.service';
import { CustomerService } from 'src/app/services/customer/customer.service';
import * as moment from 'moment';
import { NgbDatepicker } from '@ng-bootstrap/ng-bootstrap';
import { DatepickerOptions } from 'ng2-datepicker';

@Component({
  selector: 'app-manage-carprofile',
  templateUrl: './manage-carprofile.component.html',
  styleUrls: ['./manage-carprofile.component.scss']
})
export class ManageCarprofileComponent implements OnInit {
  @ViewChild('dp') dp: NgbDatepicker;
  options: DatepickerOptions = {
    
    // scrollBarColor: '#ffffff',
    format: 'dd/MM/yyyy', // date format to display in inputม
    position: 'bottom',
    inputClass: 'form-date icon',
    
  };
  id!: string;
  provinces: any = [];
  districts: any = [];
  car: any = [];
  cartypes: any = [];
  carLists: any = [];
  cardescriptions: any = [];
  carspecifications: any = [];
  workingregion: any = [];
  nonworkingregion: any = [];
  workingprovince: any = [];
  nonworkingprovince: any = [];
  workingdistrict: any = [];
  nonworkingdistrict: any = [];
  region: any = [];
  countrys: any = [];
  env = environment;
  nonWorkingArea: any = [];
  workingArea: any = [];
  countryIdx = 10;
  isformatphone = true
  isFormSubmit = false;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private carservice: CarService,
    private Driverservice: DriverService,
    private countryservice: CountryService,
    private regisionservice: RegionService,
    private provinceservice: ProvinceService,
    private districtservice: DistrictService,
    private dialog: DialogService,
    private customerservice: CustomerService,

  ) { }
  location = "";
  form = {
    id: "0",
    carRegistration: null,
    actExpiry: new Date(),
    carTypeId: null,
    carListId: null,
    carDescriptionId: null,
    carDescriptionDetail: null,
    carFeatureId: null,
    carFeatureDetail: null,
    carSpecificationId: null,
    carSpecificationDetail: null,
    width: null,
    length: null,
    height: null,
    driverName: null,
    driverPhoneCode: null,
    driverPhoneNumber: null,
    productInsurance: null,
    productInsuranceAmount: null,
    allLocation: null,
    note: null,
    status:null,
    imageprofile:null,
    location: [
      {
        countryId: null,
        regionId: null,
        provinceId: null,
        districtId: null,
        locationType: null,
      },
      {
        countryId: null,
        regionId: null,
        provinceId: null,
        districtId: null,
        locationType: null
      }
    ]
  }
  image=null;

  ngOnInit(): void {
    this.id = this.route.snapshot.params.id;

    this.getDriverCarById(this.id);
  }

  public getDriverCarById(id) {
    this.Driverservice.getDriverCarById(id).subscribe(cartype => {
      if (cartype.succeeded) {
        this.car = cartype.data;
        console.log(this.car);
        this.form = {
          id: this.id,
          carRegistration: this.car?.carRegistration,
          actExpiry:  new Date(moment(this.car.actExpiry).format("")),
          carTypeId: this.car.carType ? this.car.carType.id : null,
          carListId: this.car.carList ? this.car.carList.id : null,
          carDescriptionId: this.car.carDescription ? this.car.carDescription.id : null,
          carDescriptionDetail: this.car ? this.car.carDescriptionDetail : " ",
          carFeatureId: this.car.carFeature ? this.car.carFeature.id : null,
          carFeatureDetail: this.car.carFeatureDetail,
          carSpecificationId: this.car.carSpecification ? this.car.carSpecification?.id : null,
          carSpecificationDetail: this.car ? this.car.carSpecificationDetail : " ",
          width: this.car.width,
          length: this.car.length,
          height: this.car.height,
          driverName: this.car.driverName,
          driverPhoneCode: this.car.driverPhoneCode,
          driverPhoneNumber: this.car.driverPhoneNumber,
          productInsurance: this.car.productInsurance,
          productInsuranceAmount: this.car.productInsuranceAmount,
          allLocation: this.car.allLocation,
          note: this.car.note,
          status:this.car.status,
          imageprofile: environment.baseWebContentUrl +'/'+ this.setImgProfile(),
          location: [
            {
              countryId: this.car?.worklocations ? this.car?.worklocations.countryId : null,
              regionId: this.car?.worklocations ? this.car?.worklocations.regionId : null,
              provinceId: this.car?.worklocations ? this.car?.worklocations.provinceId : null,
              districtId: this.car?.worklocations ? this.car?.worklocations.districtId : null,
              locationType: "working-area"
            },
            {
              countryId: this.car?.nonworklocations ? this.car?.nonworklocations.countryId : null,
              regionId: this.car?.nonworklocations ? this.car?.nonworklocations.regionId : null,
              provinceId: this.car?.nonworklocations ? this.car?.nonworklocations.provinceId : null,
              districtId: this.car?.nonworklocations ? this.car?.nonworklocations.districtId : null,
              locationType: "non-working-area"
            }
          ]
        }
        this.image = this.form.imageprofile;
        this.getAllCarType();
        this.carservice.GetCarListByType(this.car.carType.id).subscribe(carList => {
          this.carLists = carList;
        });
        this.carservice.GetCarDescriptionByCarList(this.car.carList.id).subscribe(cardescription => {
          this.cardescriptions = cardescription;
        }); 
        this.carservice.GetCarSpecificationByCarList(this.car.carList.id).subscribe(carspecifications => {
          this.carspecifications = carspecifications;
        });
        
  
        this.getAllCountry();
        this.GetRegionByCountry(this.car.locations);
        this.GetProvinceByCountry(this.car.locations);
        this.GetDistrictByProvince(this.car.locations);
      }

    });
  }
  
  setImgProfile(){
   
    this.image = this.car.carFiles.filter(
      file=>file.documentType =="CarPicture"
    )
    console.log(this.image);

    if(this.image.length>0){
      return this.image[0].filePath;

    }else{
      return "";
    }
  }
  async onSubmit() {
    this.isFormSubmit = true;

    if (this.form.carRegistration != null && this.form.carRegistration != "" && this.form.carTypeId != null && this.form.carTypeId != "" 
        && this.form.carDescriptionId != null && this.form.carDescriptionId != "" && this.form.carDescriptionDetail != null && this.form.carDescriptionDetail != "" 
        && this.form.carListId != null && this.form.carListId != ""  &&  this.form.carSpecificationId != null && this.form.carSpecificationId != ""  
        &&  this.form.width != null && this.form.width != ""  && this.form.length != null && this.form.length != ""  && this.form.height != null && this.form.height != "" 
        && this.form.driverName != null && this.form.driverName != "" 
        && this.form.location[0].countryId != null && this.form.location[0].countryId !="" && this.form.location[0].provinceId != null && this.form.location[0].provinceId !="" 
        && this.form.location[0].regionId != null && this.form.location[0].regionId !="" && this.form.location[0].districtId != null && this.form.location[0].districtId !="" 
        && this.form.location[1].countryId != null && this.form.location[1].countryId !="" && this.form.location[1].provinceId != null && this.form.location[1].provinceId !="" 
        && this.form.location[1].regionId != null && this.form.location[1].regionId !="" && this.form.location[1].districtId != null && this.form.location[1].districtId !="" 
    ) {

      const formData = new FormData();
      formData.append("Id", this.form.id);
      formData.append("carRegistration", this.form.carRegistration ? this.form.carRegistration : " ");
      const actExpiry = moment(this.form.actExpiry).format('YYYY/MM/DD')
      formData.append("actExpiry", actExpiry);
      formData.append("carTypeId", this.form.carTypeId ? this.form.carTypeId : 0);
      formData.append("carListId", this.form.carListId ? this.form.carListId : 0);
      formData.append("carDescriptionId", this.form.carDescriptionId ? this.form.carDescriptionId : 0);
      formData.append("carFeatureId", this.form.carFeatureId ? this.form.carFeatureId : 0);
      formData.append("carSpecificationId", this.form.carSpecificationId ? this.form.carSpecificationId : 0);
      formData.append("carDescriptionDetail", this.form.carDescriptionDetail ? this.form.carDescriptionDetail : " ");
      formData.append("carSpecificationDetail", this.form.carSpecificationDetail ? this.form.carSpecificationDetail : " ");
      formData.append("carFeatureDetail", this.form.carFeatureDetail ? this.form.carFeatureDetail : " ");
      formData.append("carListId", this.form.carListId ? this.form.carListId : 0);
      formData.append("width", this.form.width ? this.form.width : 0);
      formData.append("length", this.form.length ? this.form.length : 0);
      formData.append("height", this.form.height ? this.form.height : 0);
      formData.append("driverPhoneCode", this.form.driverPhoneCode ? this.form.driverPhoneCode : " ");
      formData.append("driverName", this.form.driverName ? this.form.driverName : " ");
      formData.append("driverPhoneNumber", this.form.driverPhoneNumber ? this.form.driverPhoneNumber : " ");
      formData.append("productInsurance", this.form.productInsurance);
      formData.append("productInsuranceAmount", this.form.productInsuranceAmount ? this.form.productInsuranceAmount : "0");
      formData.append("allLocation", this.form.allLocation);
      formData.append("note", this.form.note ? this.form.note : " ");
      formData.append("ProfilePicture", this.form.imageprofile );
      formData.append("Status", this.form.status);
      formData.append("location", JSON.stringify(this.form.location));

      const resp = await this.dialog.confirm("Are you sure to update ", "", false);
      if (resp) {
        this.carservice.updateDriverCarById(formData)
          .pipe(first())
          .subscribe({
            next: () => {
              this.router.navigate(['/carprofile'], { relativeTo: this.route });
            }
          });
      }
    }
  }
  public getAllCarType() {
    this.carservice.GetAllCarTypeQuery().subscribe(cartype => {
      this.cartypes = cartype;
      console.log(this.cartypes);
    });
  }
  public GetCarListByCarType(id) {
    id = id.split(' ').pop();
    this.carservice.GetCarListByType(id).subscribe(carList => {
      this.carLists = carList;
    });
    this.form.carListId=null;
    this.form.carDescriptionId=null;
    this.form.carSpecificationId=null;
  }

  public getCarDescriptionByCarList(id) {
    id = id.split(' ').pop();
    this.carservice.GetCarDescriptionByCarList(id).subscribe(cardescription => {
      this.cardescriptions = cardescription;
    });
    this.carservice.GetCarSpecificationByCarList(id).subscribe(carspecifications => {
      this.carspecifications = carspecifications;
    });
    this.form.carDescriptionId=null;
    this.form.carSpecificationId=null;
  }
  public GetCarSpecificationByCarList(id) {
    id = id.split(' ').pop();
    this.carservice.GetCarSpecificationByCarList(id).subscribe(carspecifications => {
      this.carspecifications = carspecifications;
    });
    this.form.carSpecificationId=null;
  }
  public getAllCountry() {
    this.countryservice.getAllCountry("", 0, 0).subscribe(country => {
      this.countrys = country.data;
    });
  }
  public GetRegionByCountry(id) {
    this.workingregion = [];
    this.nonworkingregion = [];
    id.forEach(element => {
      this.regisionservice.GetRegionByCountry(element.countryId).subscribe(region => {
        this.region = region;
        if (element.locationType === "working-area") {
          this.workingregion.push(this.region);
        }
        if (element.locationType === "non-working-area") {
          this.nonworkingregion.push(this.region);
        }
      });
    });
  }

  public GetProvinceByCountry(id) {
    id.forEach(element => {
      this.provinceservice.GetProvinceByCountry(element.countryId).subscribe(province => {
        if (element.locationType === "working-area") {
          this.workingprovince.push(province);
        }
        if (element.locationType === "non-working-area") {
          this.nonworkingprovince.push(province);
        }
      });
    });
  }

  public GetDistrictByProvince(id) {
    this.nonworkingdistrict = [];
    id.forEach(element => {
      this.districtservice.GetDistrictByProvince(element.provinceId).subscribe(district => {
        if (element.locationType === "working-area") {
          this.workingdistrict.push(district);
        }
        if (element.locationType === "non-working-area") {
          this.nonworkingdistrict.push(district);
        }

        console.log(this.nonworkingdistrict);
      });
    });
  }

  public getRegionByCountry(id) {
    id = id.split(' ').pop();

    this.workingregion = [];
    this.customerservice.getRegionByCountry(id).subscribe(province => {
      this.workingregion.push(province);
    });
    this.form.location[0].regionId = null
    this.form.location[0].districtId = null
    this.form.location[0].provinceId = null
    this.workingprovince = [];
    this.workingdistrict = [];
  }
  public getAllProvince(id) {
    id = id.split(' ').pop();
    this.workingprovince = [];
    this.customerservice.getProvinceByRegion(id).subscribe(province => {
      this.workingprovince.push(province);
    });
    this.form.location[0].districtId = null
    this.form.location[0].provinceId = null
    this.workingdistrict = [];
  }

  public getAllDistrict(id) {
    id = id.split(' ').pop();
    this.workingdistrict = [];
    this.customerservice.getAllDistrict(id).subscribe(district => {
      this.workingdistrict.push(district);
    });
    this.form.location[0].districtId = null

  }
  public getRegionByCountrynon(id) {
    id = id.split(' ').pop();
    this.nonworkingregion = [];
    this.customerservice.getRegionByCountry(id).subscribe(province => {
      this.nonworkingregion.push(province);

    });
    this.form.location[1].regionId = null
    this.form.location[1].districtId = null
    this.form.location[1].provinceId = null
    this.nonworkingprovince = [];
    this.nonworkingdistrict = [];
  }

  public getAllProvincenon(id) {
    id = id.split(' ').pop();
    this.nonworkingprovince = [];
    this.customerservice.getProvinceByRegion(id).subscribe(province => {
      this.nonworkingprovince.push(province);
    });
    this.form.location[1].districtId = null
    this.form.location[1].provinceId = null
    this.nonworkingdistrict = [];
  }

  public getAllDistrictnon(id) {
    id = id.split(' ').pop();
    this.nonworkingdistrict = [];
    this.customerservice.getAllDistrict(id).subscribe(district => {
      this.nonworkingdistrict.push(district);
    });
    this.form.location[1].districtId = null
  }
  handleFileInput(text, files: FileList) {
    const file = files[0];
    if (text === 'image') {
      const reader = new FileReader();
      reader.onload = e => this.image = reader.result;
      reader.readAsDataURL(file);
      this.form.imageprofile = file;
    } 
  }
}
