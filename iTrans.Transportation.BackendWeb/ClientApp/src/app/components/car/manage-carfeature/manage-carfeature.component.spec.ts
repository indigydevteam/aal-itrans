import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageCarfeatureComponent } from './manage-carfeature.component';

describe('ManageCarfeatureComponent', () => {
  let component: ManageCarfeatureComponent;
  let fixture: ComponentFixture<ManageCarfeatureComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ManageCarfeatureComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageCarfeatureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
