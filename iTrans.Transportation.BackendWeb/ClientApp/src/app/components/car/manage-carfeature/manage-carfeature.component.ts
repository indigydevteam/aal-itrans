import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { first } from 'rxjs/operators';
import { CarList, CarType } from 'src/app/models/car.model';
import { CarService } from 'src/app/services/car/car.service';

@Component({
  selector: 'app-manage-carfeature',
  templateUrl: './manage-carfeature.component.html',
  styleUrls: ['./manage-carfeature.component.scss']
})
export class ManageCarfeatureComponent implements OnInit {

  form!: FormGroup;
  id!: string;
  isAddMode!: boolean;
  submitted = false;
  carlist: CarList[] | any;
  cartype: CarType[] | any;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private carservice: CarService,
  ) { }
  display:any = "d-none";
  ngOnInit(): void {
    this.getAllCarType();
    this.id = this.route.snapshot.params.id;
    this.isAddMode = !this.id;
    this.form = this.formBuilder.group({
        Id: this.id,
        carTypeId:['',Validators.required],
        carListId:['', Validators.required],
        name_TH: ['', Validators.required],
        name_ENG: ['', Validators.required],
        sequence: ['', Validators.required],
        active: [true],
        specified: [true]

      }
    );
    if (!this.isAddMode) {
      this.carservice.getCarFeatureById(this.id)
        .pipe(first())
        .subscribe(x => {
          this.form.patchValue(x);
          this.onChangeCarType();
        });

    }
  }
  get formControls() { return this.form.controls; }

  private createCarFeature() {

    this.carservice.createCarFeature(this.form.value)
      .pipe(first())
      .subscribe({
        next: () => {
          this.router.navigate(['/carfeature'], { relativeTo: this.route });
        }
      });
  }
  private updateCarFeature() {

    this.carservice.updateCarFeature(this.form.value)
      .pipe(first())
      .subscribe({
        next: () => {
          this.router.navigate(['/carfeature'], { relativeTo: this.route });
        }
      });
    
  }
  onSubmit() {
    this.submitted = true;
    if (this.form.invalid) {
      return;
    }

    if (this.isAddMode) {
      this.createCarFeature();
    } else {
      this.updateCarFeature();
    }
  }
  public getAllCarType() {
    this.carservice.GetAllCarTypeQuery().subscribe(cartype => {
      this.cartype = cartype;
    });
  }
  onChangeCarType() {
    this.display = "d-inline";
    this.carlist = null;
    this.carservice.GetCarListByType(this.form.value.carTypeId).subscribe(carlist => {
    this.carlist = carlist;
    });  
  }
 
}
