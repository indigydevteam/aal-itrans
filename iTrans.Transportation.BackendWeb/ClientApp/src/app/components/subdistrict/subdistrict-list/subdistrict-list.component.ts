import { SubdistrictService } from './../../../services/subdistrict/subdistrict.service';
import { Component, OnInit } from '@angular/core';
import { NgxRangeModule } from 'ngx-range';
import { first } from 'rxjs/operators';
import { Subdistrict, SubdistrictList } from 'src/app/models/subdistrict.model';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-subdistrict-list',
  templateUrl: './subdistrict-list.component.html',
  styleUrls: ['./subdistrict-list.component.scss']
})
export class SubdistrictListComponent implements OnInit {

  public subdistricts: Observable<SubdistrictList> | any;
  public page: number = 0;
  constructor(private subdistrictservice: SubdistrictService) { }
  public pageSize: number = 10;
  public pageNumber: number = 1;
  public search: string = "";
  ngOnInit(): void {
    this.getAllSubdistrict();
  }

  public getAllSubdistrict() {
    this.subdistrictservice.getAllSubdistrict(this.search, this.pageNumber, this.pageSize).subscribe(subdistricts => {
      console.log(subdistricts);
      this.subdistricts = subdistricts;
      this.page = subdistricts.itemCount / (subdistricts.pageSize == 0 ? 1 : subdistricts.pageSize);
      if (this.page == 0) this.page = 1;
    });
  }

  public filter(event) {
    console.log("You entered: ", event.target.value);
    this.search = event.target.value;
    this.pageNumber = 1;
    this.getAllSubdistrict();
  }
  public pageChange(pageNumber: number ) {
    console.log(pageNumber);
    this.pageNumber = pageNumber;
    this.getAllSubdistrict();
  }
}
