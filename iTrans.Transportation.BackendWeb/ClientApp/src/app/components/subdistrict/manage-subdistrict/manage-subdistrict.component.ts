import { DistrictService } from './../../../services/district/district.service';
import { SubdistrictService } from './../../../services/subdistrict/subdistrict.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { MustMatch } from './../../../shared/must-match.validator';
import { first } from 'rxjs/operators';
import { District } from '../../../models/district.model';

@Component({
  selector: 'app-manage-subdistrict',
  templateUrl: './manage-subdistrict.component.html',
  styleUrls: ['./manage-subdistrict.component.scss']
})
export class ManageSubdistrictComponent implements OnInit {

  form!: FormGroup;
  id!: string;
  isAddMode!: boolean;
  submitted = false;
  districts: District[] | any;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private subdistrictService: SubdistrictService,
    private districtService: DistrictService,
  ) { }

  ngOnInit(): void {
    this.getAllDistrict();
    this.id = this.route.snapshot.params.id;
    this.isAddMode = !this.id;
    this.form = this.formBuilder.group({
      Id: this.id,
      districtId: ['', Validators.required],
      name_TH: ['', Validators.required],
      name_ENG: ['', Validators.required],
      postCode: ['', Validators.required],
      active: [true]
    }
    );
    if (!this.isAddMode) {
      this.subdistrictService.getSubdistrictById(this.id)
        .pipe(first())
        .subscribe(x => {
          this.form.patchValue(x);
          console.log(this.form);
        });
    }
  }
  get formControls() { return this.form.controls; }

  private createSubdistrict() {
    console.log(this.form.value);
    this.subdistrictService.createSubdistrict(this.form.value)
      .pipe(first())
      .subscribe({
        next: () => {
          this.router.navigate(['/subdistrict'], { relativeTo: this.route });
        }
      });
  }
  private updateSubdistrict() {
    this.subdistrictService.updateSubdistrict(this.form.value)
      .pipe(first())
      .subscribe({
        next: () => {
          this.router.navigate(['/subdistrict'], { relativeTo: this.route });
        }
      });
  }

  onSubmit() {
    
    this.submitted = true;
    if (this.form.invalid) {
      return;
    }

    if (this.isAddMode) {
      this.createSubdistrict();
    } else {
      this.updateSubdistrict();
    }

  }

  public getAllDistrict() {
    this.districtService.getAllDistrict("", 0, 0).subscribe(districts => {
      this.districts = districts.data;
      console.log(this.districts);
    });
  }

}
