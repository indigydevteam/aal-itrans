import { Component, OnInit } from '@angular/core';
import { HelpService } from 'src/app/services/help/help.service';

@Component({
  selector: 'app-condition',
  templateUrl: './condition.component.html',
  styleUrls: ['./condition.component.scss']
})
export class ConditionComponent implements OnInit{

  public pageSize: number = 10;
  public pageNumber: number = 1;
  public search: string = "";
  constructor(
    private helpservice: HelpService,
    ) { }

  termsconditions:any=[];
  active:number;
  column:string;
  ngOnInit(): void {
    this.all();

  }
  public getAlltermscondition() {
    this.helpservice.getAllTermsConditions(this.search, this.pageNumber, this.pageSize, this.column, this.active,"").subscribe(termsconditions => {
      this.termsconditions = termsconditions.data;

     
      console.log(this.termsconditions);


    });
  }
  all() {
    this.active = 1;
    this.column = "all";
    this.getAlltermscondition();
  }

  orderBy(value) {
    if (this.active == 1) {
      this.active = 2;
    } else if (this.active == 2) {
      this.active = 3;
    } else {
      this.active = 1;
    }
    this.column = value;
    this.getAlltermscondition();
  }
  
  public filter(search) {
    this.search = search;
    this.pageNumber = 1;
    this.getAlltermscondition();
  }
  
  previous() {
    this.pageNumber--;
    this.getAlltermscondition();
  }
   next() {
    this.pageNumber++;
    this.getAlltermscondition();
  }
   selectedPage(event) {
    this.pageNumber = event;
    this.getAlltermscondition();
  }
 
}
