import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DriverprofileListComponent } from './driverprofile-list.component';

describe('DriverprofileListComponent', () => {
  let component: DriverprofileListComponent;
  let fixture: ComponentFixture<DriverprofileListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DriverprofileListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DriverprofileListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
