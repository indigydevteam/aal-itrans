import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs/operators';
import { DialogService } from 'src/app/services/dialog/dialog.service';
import { DriverService } from 'src/app/services/driver/driver.service';

@Component({
  selector: 'app-driverprofile-list',
  templateUrl: './driverprofile-list.component.html',
  styleUrls: ['./driverprofile-list.component.scss']
})
export class DriverprofileListComponent implements OnInit {

  public pageSize: number = 10;
  public pageNumber: number = 1;
  public search: string = "";
  public drivertype: string = "";


  constructor(
    private driverservice: DriverService,
    private dialog: DialogService,

  ) { }

  drivers: any = [];
  active:number;
  column:string
  ngOnInit(): void {
    this.all();
  }

  public getAllDriverProfile() {
    this.driverservice.getAllDriverProfile(this.search, this.drivertype, this.pageNumber, this.pageSize, this.column, this.active).subscribe(drivers => {
      this.drivers = drivers;
    });
  }
  all() {
    this.active = 1;
    this.column = "all";
    this.getAllDriverProfile();
  }
  orderBy(value) {
    if (value == this.column) {
      if (this.active == 1) {
        this.active = 2;
      } else if (this.active == 2) {
        this.active = 3;
      } else {
        this.active = 1;
      }
    } else {
      this.active = 2;
    }
    this.column = value;
    this.getAllDriverProfile();
  }

  public filter() {
    this.pageNumber = 1;
    this.getAllDriverProfile();
  }
  previous() {
    this.pageNumber--;
    this.getAllDriverProfile();
  }
  next() {
    this.pageNumber++;
    this.getAllDriverProfile();
  }
  selectedPage(event) {
    this.pageNumber = event;
    this.getAllDriverProfile();
  }

  async onDelete(id) {
    const resp = await this.dialog.confirm("Are you sure to delete ", "", false);
    if (resp) {
      this.driverservice.DeleteDriver(id)
        .pipe(first())
        .subscribe(drivers => {
          this.drivers = drivers;
          if (this.drivers.data !== null)
            this.getAllDriverProfile();
        });
    }

  }


}