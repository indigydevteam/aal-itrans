import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageDriverprofileComponent } from './manage-driverprofile.component';

describe('ManageDriverprofileComponent', () => {
  let component: ManageDriverprofileComponent;
  let fixture: ComponentFixture<ManageDriverprofileComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ManageDriverprofileComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageDriverprofileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
