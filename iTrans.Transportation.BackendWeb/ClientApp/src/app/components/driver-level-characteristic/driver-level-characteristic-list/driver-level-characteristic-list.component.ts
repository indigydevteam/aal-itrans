import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { first } from 'rxjs/operators';
import { DriverLevelCharacteristic, DriverLevelCharacteristicList } from 'src/app/models/driverlevelcharacteristic.model';
import { DialogService } from 'src/app/services/dialog/dialog.service';
import { DriverLevelCharacteristicService } from 'src/app/services/driverlevelcharacteristic/driverlevelcharacteristic.service';

@Component({
  selector: 'app-driver-level-characteristic-list',
  templateUrl: './driver-level-characteristic-list.component.html',
  styleUrls: ['./driver-level-characteristic-list.component.scss']
})
export class DriverLevelCharacteristicListComponent implements OnInit {
  public driverLevelCharacteristics: Observable<DriverLevelCharacteristic> | any;
  public count: number = 0;
  public pageSize: number = 10;
  public pageNumber: number = 1;
  public search: string = "";
  constructor(private driverlevelcharacteristicservice: DriverLevelCharacteristicService, private dialog: DialogService) { }
  active: number;
  column: string;
  ngOnInit(): void {
    this.all()
  }
  public DriverLevelCharacteristic() {
    this.driverlevelcharacteristicservice.getAll(this.search, this.pageNumber, this.pageSize,this.column ,this.active).subscribe(driverLevelCharacteristics => {
      this.driverLevelCharacteristics = driverLevelCharacteristics;
    });
  }
  all() {
    this.active = 1;
    this.column = "all";
    this.DriverLevelCharacteristic();
  }

  orderBy(value) {
    if (value == this.column) {
      if (this.active == 1) {
        this.active = 2;
      } else if (this.active == 2) {
        this.active = 3;
      } else {
        this.active = 1;
      }
    } else {
      this.active = 2;
    }
    this.column = value;
    this.DriverLevelCharacteristic();
  }
  public filter(search) {
    this.search = search;
    this.pageNumber = 1;
    this.DriverLevelCharacteristic();
  }
  previous() {
    this.pageNumber--;
    this.DriverLevelCharacteristic();
  }
  next() {
    this.pageNumber++;
    this.DriverLevelCharacteristic();
  }
  selectedPage(event) {
    this.pageNumber = event;
    this.DriverLevelCharacteristic();
  }
  async onDelete(id) {
    const resp = await this.dialog.confirm("Are you sure to delete ", "", false);
    if (resp) {
      this.driverlevelcharacteristicservice.delete(id)
        .pipe(first())
        .subscribe(driverLevelCharacteristics => {
          this.driverLevelCharacteristics = driverLevelCharacteristics;
          if (this.driverLevelCharacteristics.data !== null)
            this.DriverLevelCharacteristic();
        });
    }

  }
}
