import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { first } from 'rxjs/operators';
import { DriverLevelCharacteristic, DriverLevelCharacteristicList } from 'src/app/models/driverlevelcharacteristic.model';
import { DialogService } from 'src/app/services/dialog/dialog.service';
import { DriverLevelCharacteristicService } from 'src/app/services/driverlevelcharacteristic/driverlevelcharacteristic.service';

@Component({
  selector: 'app-manage-driver-level-characteristic',
  templateUrl: './manage-driver-level-characteristic.component.html',
  styleUrls: ['./manage-driver-level-characteristic.component.scss']
})
export class ManageDriverLevelCharacteristicComponent implements OnInit {
  form!: FormGroup;
  id!: string;
  isAddMode!: boolean;
  submitted = false;
  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private service: DriverLevelCharacteristicService) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.params.id;
    this.isAddMode = !this.id;
    this.form = this.formBuilder.group({
      id: this.id,
      level: ['', Validators.required],
      star: ['', Validators.required],
      complaintPerMonth: ['', Validators.required],
      rejectPerMonth: ['', Validators.required],
      cancelPerMonth: ['', Validators.required],
      complaintPerYear: ['', Validators.required],
      rejectPerYear: ['', Validators.required],
      cancelPerYear: ['', Validators.required],
      insuranceValue: ['', Validators.required]
    }
    );
    if (!this.isAddMode) {
      this.service.getById(this.id)
        .pipe(first())
        .subscribe(x => this.form.patchValue(x));

    }
  }
  get formControls() { return this.form.controls; }

  private create() {

    this.service.create(this.form.value)
      .pipe(first())
      .subscribe({
        next: () => {
          this.router.navigate(['/driverlevelcharacteristic'], { relativeTo: this.route });
        }
      });
  }
  private update() {
    this.service.update(this.form.value)
      .pipe(first())
      .subscribe({
        next: () => {
          this.router.navigate(['/driverlevelcharacteristic'], { relativeTo: this.route });
        }
      });
  }
  onSubmit() {
    this.submitted = true;
    if (this.form.invalid) {
      return;
    }

    if (this.isAddMode) {
      this.create();
    } else {
      this.update();
    }

  }
}
