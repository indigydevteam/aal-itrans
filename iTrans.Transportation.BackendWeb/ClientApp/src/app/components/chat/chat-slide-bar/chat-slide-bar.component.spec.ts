import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChatSlideBarComponent } from './chat-slide-bar.component';

describe('ChatSlideBarComponent', () => {
  let component: ChatSlideBarComponent;
  let fixture: ComponentFixture<ChatSlideBarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChatSlideBarComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChatSlideBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
