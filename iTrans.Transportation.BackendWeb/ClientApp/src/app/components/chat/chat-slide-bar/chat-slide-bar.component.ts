import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import * as moment from 'moment';

import _ from 'underscore';

import { Subject } from 'rxjs';
import { environment } from '../../../../environments/environment';
import { $, element } from 'protractor';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { EventService } from 'src/app/services/event/event.service';
import { SignalRService } from 'src/app/services/SignalR/signal-r.service';
import { StorageService } from 'src/app/services/storage/storage.service';
import { ChatService } from 'src/app/services/chat/chat.service';
import { ToastrService } from 'ngx-toastr';
import { AlertService } from 'src/app/services/alert/alert.service';
import { SignalrJoinChatroomService } from 'src/app/services/SignalrJoinChatroom/signalr-join-chatroom.service';
import { UrlDetectService } from 'src/app/services/UrlDetect/url-detect.service';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { TranslateLanguageService } from 'src/app/services/translateLanguage/translate-language.service';


@Component({
  selector: 'app-chat-slide-bar',
  templateUrl: './chat-slide-bar.component.html',
  styleUrls: ['./chat-slide-bar.component.scss']
})
export class ChatSlideBarComponent implements OnInit {

  @ViewChild('chatRoom', { static: true }) chatRoom: ElementRef;
  @ViewChild('messageStr', { static: true }) messageEl: ElementRef;
  galleryOptions = [{ previewArrows: false, previewArrowsAutoHide: false, 'previewDownload': true, 'thumbnails': false, 'image': false, 'height': '0px', 'width': '0px' }];

  acceptFile = 'image/gif,image/jpeg,image/png,text/plain,application/vnd.ms-powerpoint,application/vnd.openxmlformats-officedocument.presentationml.presentation,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
  messageList: Array<any> = [];
  isConnected = false;
  companyCode: any;
  profile: any;
  isReady = false;
  roomList: any[] = [];
  tempRoomList: any[] = [];
  chatPublic: any[] = [];
  chatInvite: any[] = [];
  chatMyGroup: any[] = [];
  chatAllFriend: any[] = [];
  isChatPublic = false;
  isChatInvite = false;
  isChatMyGroup = true;
  roomOpen = false;
  page = 1;
  pageSize = 130;
  isInfinite = false;
  ln = window.localStorage.getItem('languages');
  isChatBadgeCount = 0;
  isChatGroupBadgeCount = 0;
  receiver: any;
  isChatGroup = false;
  socketName: any;
  isLoading = false;
  isConnecting = false;
  chatRoomSelected: any;
  fileUpload: any;
  messageText: any = '';
  groupId: any;
  groupName: any;
  senderId: any;
  userQuestionUpdate = new Subject<string>();
  qText: any;
  roomListLoading = false;
  imageFile: any;
  anyFile: any;
  isLobby = false;
  currentTab: any;
  loadMsgHided = false;
  newRoomNameMyCreate: any;
  current_id = 0;
  // config room chat
  roomChatType = 1;
  fileList = [];
  fileListTemp = [];
  fileNotePage = 1;
  fileNotePageSize = 30;
  note = '';
  roomName = '';
  roomId = 0;
  noteSearch = '';
  constructor(
    private ent: EventService,
    private signalRServ: SignalRService,
    private storage: StorageService,
    private api: ChatService,
    // private utility: UtilityService,
    private toast: ToastrService,
    private myAlert: AlertService,
    private signaRjoinRoom: SignalrJoinChatroomService,
    private loader: NgxUiLoaderService,
    private urlDetect: UrlDetectService,
    // private pageNavigateService: PageNavigateService,
    private ngxSmartModalService: NgxSmartModalService,
    private translate: TranslateLanguageService,
    private event: EventService
  ) {

  }



  async ngOnInit() {
    const usr = await this.storage.getItem('token');
    this.companyCode = usr.CompanyCode;
    this.profile = usr;

    await this.connectionHub();
    await this.getChatAllList();

  }

  onCreateGroupChat() {
    this.ent.onCreateGroupModalEvent.emit({
      resovle: async (data: any) => {
        this.newRoomNameMyCreate = data;
        this.storage.setItem('newRoomNameMyCreate', JSON.stringify(this.newRoomNameMyCreate));
        await this.getChatAllList();
        this.getLobbyList();
        const i = this.roomList.findIndex((chatMyGroup: any) => {
          return data.groupName == chatMyGroup.groupName;
        });
        setTimeout(() => {
          // this.chatRoomSelected = this.roomList[i];
        }, 300);

      }
    });
  }

  onCreateChat() {
    this.ent.onSelectFriendModalEvent.emit({
      resovle: (res: any) => {
        if (res) {
          this.loader.start();
          const socketNameArr = [this.profile.Id, res.profiles[0].UserId].sort();
          this.socketName = socketNameArr[0] + '|' + socketNameArr[1];
          this.senderId = res.profiles[0].UserId;
          this.messageList = [];

          const user = {
            content: "",
            contentTimeStamp: Date.now(),
            contentType: "text",
            creator: this.profile.Id,
            groupId: 0,
            groupName: this.socketName,
            groupNameText: res.profiles[0].FullName,
            imageUrl: res.profiles[0].ImageFileURL,
            inviteDate: Date.now(),
            isAccept: true,
            isPrivateGroup: true,
            senderId: res.profiles[0].UserId,
            senderName: res.profiles[0].FullName,
            senderNameAlt: res.profiles[0].FullNameAlt,
            totalBadge: 0,
            totalMember: 1,
            // fromUserId: this.senderId,
          };
          let hasUserId = _.find(this.roomList, x => {
            return x.fromUserId == res.profiles[0].UserId;
          });

          if (hasUserId) {
            let index = _.findIndex(this.roomList, { fromUserId: res.profiles[0].UserId });
            this.roomList.splice(index, 1);
            this.roomList.unshift(hasUserId);
          } else {
            this.roomList.unshift(user);
          }

          this.chatRoomSelected = user;

          setTimeout(async () => {
            await this.getReceivedProfile();
            await this.onConnectJoinChat();
            await this.joinRoom();
          }, 0);
          this.loader.stopAll();
        }
      }
    });
  }

  private async connectionHub() {
    try {
      await this.signalRServ.connectChatAsync().catch(err => {

      });

      await this.signalRServ.connectChatGroupAsync();
      await this.signalRServ.connectChatListAsync();
      this.isConnected = true;
    } catch (error) {
      // console.log('chat-list: connectionHubs', error);
    }
  }

  async setFormNotiClick() {
    let notiData = this.storage.getItem('notichat');
    if (!notiData) return;
    // console.log(a)
    await this.getChatListResult();
    let chat = JSON.parse(notiData);
    let action = JSON.parse(chat.Action);

    if (action) {
      const i = this.roomList.findIndex((chatMyGroup: any) => {
        return action.RoomId == chatMyGroup.groupId;
      });

      if (i != -1) {
        await this.signaRjoinRoom.initData(this.roomList[i]);
        let perm = await this.signaRjoinRoom.checkYouCanJoin(); //this.signaRjoinRoom.onJoinChatRoom();
        if (perm) {
          this.isChatGroup = true;
          this.chatRoomSelected = this.roomList[i];
          this.chatRoomSelected.isCanDecline = true;
          this.newRoomNameMyCreate = { groupName: this.roomList[i].groupName, isChatGroup: true };
          this.storage.setItem('newRoomNameMyCreate', JSON.stringify(this.newRoomNameMyCreate));
          // this.toast.toastWarning({text: msgError});
        }
      }
    }
    this.storage.removeItem('notichat');
  }

  private async fromNotiClick() {
    this.setFormNotiClick();
    this.ent.onNotiChatclickEvent.subscribe(async data => {

      this.setFormNotiClick();
    });
  }

  private getChatAllList(): Promise<any> {
    return new Promise((resovle) => {
      try {
        this.roomListLoading = true;
        this.signalRServ.hubConnectionChatLastMessage
          .invoke('GetLastMessageV2', this.profile.Id, this.pageSize, this.page, this.profile.CompanyCode)
          .then(async (res) => {
            if (!res) return;
            this.isInfinite = false;
            if (this.page == 1) {
              this.roomList = res;
          
            } else {
              if (res?.length > 0) {
                res.forEach(x => {
                  this.roomList.push(x);
                });
              }
            }
            this.cotomizeRoom(this.roomList);
            this.tempRoomList = this.roomList;
            this.roomListLoading = false;

            // await this.setChatUserProfile();
            this.isReady = true;
            if (this.roomList.length >= (this.pageSize * this.page)) {
              this.isInfinite = true;
            }

            let a = this.storage.getItem('notichat');

            this.fromNotiClick();
            // console.log(this.newRoomNameMyCreate);
            this.setNewGroup();
            this.onSocketMessage();


            this.signalRServ.hubConnectionChatLastMessage.off('lastMessageResult');
            this.signalRServ.hubConnectionChatLastMessage.on('lastMessageResult', () => {
              this.getChatListResult();
              this.scrollToBottom();
            });

            resovle(true);
         
         
          })
          .catch(err => {
            console.error(err)
            resovle(false);
          });
      } catch (error) {
        // console.log('##Error2', error);
        resovle(false);
      }
    });
  }

  private cotomizeRoom(roomList: any) {
    let sender = '';

    roomList.forEach(async msg => {
      if (msg.senderId === this.profile.Id) {
        // sender = await this.translate.getText('WORKPLUS.SHARED.YOU');
      } else {
        sender = this.ln === 'th' ? msg.senderNameAlt : msg.senderName;
      }

      if (msg.isPrivate) {
        // msg.fromUserId = (msg.groupName.split('|'))[0];
        msg.filterBy = msg.groupNameText + '' + msg.groupNameTextAlt;
      } else {
        msg.filterBy = msg.groupName;
      }

      switch (msg?.contentType?.toLowerCase()) {
        case 'text':
          msg.content = this.signalRServ.decrypt(msg.content as string, 'chat');
          break;
        case 'image':
          // msg.content = await this.translate.getText('WORKPLUS.CHAT.SEND_A_PHOTO', { replaces: [{ k: '__name__', v: sender }] });
          break;
        case 'file':
          // msg.content = await this.translate.getText('WORKPLUS.CHAT.SEND_A_FILE', { replaces: [{ k: '__name__', v: sender }] });
          break;
        case 'location':
          // msg.content = await this.translate.getText('WORKPLUS.CHAT.SEND_A_LOCATION', { replaces: [{ k: '__name__', v: sender }] });
          break;
        case 'note':
          msg.content = this.signalRServ.decrypt(msg.content as string, 'chat');
          break;
      }
    });
  }

  public onSocketMessage() {
    this.signalRServ.hubConnectionChatLastMessage.off('lastMessageResult');
    this.signalRServ.hubConnectionChatLastMessage.on('lastMessageResult', () => {
      //// console.log('##lastMessageResult');
      this.getChatListResult();
    });

    this.signalRServ.hubConnectionChatLastMessage.off('lobbyResult');
    this.signalRServ.hubConnectionChatGroup.on('lobbyResult', async (messageView: any) => {

      this.getChatListResult();
      this.getChatAllList();
    });
  }

  async newMessageOneToOne(messageView) {
    let msgs = [];
    let message: any = {};
    await this.getReceivedProfile();

    this.setMessage(messageView, message);
    if (messageView?.contentType?.toLowerCase() != 'none') {
      message.UserId = messageView.to;
      message.FullNameAlt = messageView.displayName;
      message.FullName = messageView.displayName;
      message.isMe = (messageView.to === this.profile.Id) ? true : false;
      message.totalRead = messageView.totalRead;
      message.isRead = messageView.isRead;

      msgs.unshift(message);
    }

    if (this.messageList?.length > 0) {
      let lastDate: any = this.messageList[this.messageList.length - 1];
      if (lastDate && lastDate?.messageById.length > 0) {
        let lastId = lastDate?.messageById[lastDate.messageById.length - 1];
        if (lastId.id == messageView.to) {
          let isEmty = true;
          for (let j = 0; j < lastId.messageByTime.length; j++) {
            if (lastId.messageByTime[j].time == moment(messageView.timestamp).format('YYYY/MM/DD HH:mm:ss')) {
              lastId.messageByTime[j].message.push(message);
              isEmty = false;
              break;
            }
          }

          if (isEmty) {

            lastId.messageByTime.push({
              time: moment(messageView.timestamp).format('YYYY/MM/DD HH:mm:ss'),
              isMe: messageView.to != this.profile.Id ? true : false,
              ProfileImageURL: messageView.to != this.profile.Id ? this.profile.ProfileImageURL : this.chatRoomSelected.imageUrl,
              message: msgs
            });
          }

        } else {

          let ids = {
            id: messageView.to,
            messageByTime: [
              {
                time: moment(messageView.timestamp).format('YYYY/MM/DD HH:mm:ss'),
                isMe: messageView.to != this.profile.Id ? true : false,
                ProfileImageURL: messageView.to != this.profile.Id ? this.profile.ProfileImageURL : (this.receiver?.ProfileImageURL || this.chatRoomSelected.imageUrl),
                message: msgs
              }
            ]
          }
          lastDate.messageById.push(ids);
        }
      } else {
        let ids = {
          id: messageView.to,
          messageByTime: [
            {
              time: moment(messageView.timestamp).format('YYYY/MM/DD HH:mm'),
              isMe: messageView.to != this.profile.Id ? true : false,
              ProfileImageURL: messageView.to != this.profile.Id ? this.profile.ProfileImageURL : (this.receiver?.ProfileImageURL || this.chatRoomSelected.imageUrl),
              message: msgs
            }
          ]
        };

        if (lastDate) {
          lastDate.messageById.push(ids);
        } else {
          this.messageList.push({
            date: moment(messageView.timestamp).format('YYYY/MM/DD'),
            messageById: ids
          });
        }
      }

      this.scrollToBottom();
    } else {
      let ids = {
        id: messageView.to,
        messageByTime: [
          {
            time: moment(messageView.timestamp).format('YYYY/MM/DD HH:mm:ss'),
            isMe: messageView.to != this.profile.Id ? true : false,
            ProfileImageURL: messageView.to != this.profile.Id ? this.profile.ProfileImageURL : (this.receiver?.ProfileImageURL || this.chatRoomSelected.imageUrl),
            message: msgs
          }
        ]
      }

      this.messageList.push({
        date: moment(messageView.timestamp).format('YYYY/MM/DD'),
        messageById: [ids]
      });
    }//console.log(this.messageList);

  }

  async newMessageOneToMeny(messageView) {
    let msgs = [];
    let message: any = {};
    this.senderId = messageView.senderId;
    this.setMessage(messageView, message);
    if (messageView?.contentType?.toLowerCase() != 'none') {
      message.UserId = messageView.senderId;
      message.FullNameAlt = messageView.displayName;
      message.FullName = messageView.displayName;
      message.isMe = messageView.senderId == this.profile.Id ? true : false;
      message.totalRead = messageView.totalRead;
      message.isRead = messageView.isRead;

      msgs.unshift(message);
    }

    if (this.messageList?.length > 0) {
      let lastDate: any = this.messageList[this.messageList.length - 1];
      if (lastDate && lastDate?.messageById.length > 0) {
        let lastId = lastDate?.messageById[lastDate.messageById.length - 1];
        if (lastId.id == messageView.senderId) {
          let isEmty = true;
          for (let j = 0; j < lastId.messageByTime.length; j++) {
            if (lastId.messageByTime[j].time == moment(messageView.timestamp).format('YYYY/MM/DD HH:mm:ss')) {
              lastId.messageByTime[j].message.push(message);
              isEmty = false;
              break;
            }
          }

          if (isEmty) {
            lastId.messageByTime.push({
              time: moment(messageView.timestamp).format('YYYY/MM/DD HH:mm:ss'),
              isMe: messageView.senderId == this.profile.Id ? true : false,
              ProfileImageURL: messageView.senderId == this.profile.Id ? this.profile.ProfileImageURL : messageView.displayImage,
              message: msgs
            });
          }

        } else {
          let ids = {
            id: messageView.senderId,
            messageByTime: [
              {
                time: moment(messageView.timestamp).format('YYYY/MM/DD HH:mm:ss'),
                isMe: messageView.senderId == this.profile.Id ? true : false,
                ProfileImageURL: messageView.senderId == this.profile.Id ? this.profile.ProfileImageURL : messageView.displayImage,
                message: msgs
              }
            ]
          }
          lastDate.messageById.push(ids);
        }
      } else {
        let ids = {
          id: messageView.senderId,
          messageByTime: [
            {
              time: moment(messageView.timestamp).format('YYYY/MM/DD HH:mm:ss'),
              isMe: messageView.senderId == this.profile.Id ? true : false,
              ProfileImageURL: messageView.senderId == this.profile.Id ? this.profile.ProfileImageURL : messageView.displayImage,
              message: msgs
            }
          ]
        };

        if (lastDate) {
          lastDate.messageById.push(ids);
        } else {
          this.messageList.push({
            date: moment(messageView.timestamp).format('YYYY/MM/DD'),
            messageById: ids
          });
        }
      }
    } else {
      let ids = {
        id: messageView.senderId,
        messageByTime: [
          {
            time: moment(messageView.timestamp).format('YYYY/MM/DD HH:mm:ss'),
            isMe: messageView.senderId == this.profile.Id ? true : false,
            ProfileImageURL: messageView.senderId == this.profile.Id ? this.profile.ProfileImageURL : messageView.displayImage,
            message: msgs
          }
        ]
      }

      this.messageList.push({
        date: moment(messageView.timestamp).format('YYYY/MM/DD'),
        messageById: [ids]
      });

    }
    this.scrollToBottom();
  }

  public async onSocketRoomMessage() {
    this.signalRServ.hubConnectionChat.off('newMessage');
    this.signalRServ.hubConnectionChat.on('newMessage', (messageView: any) => {

      if (this.isChatGroup) {
        this.newMessageOneToMeny(messageView);
      } else {
        this.newMessageOneToOne(messageView);
      }
      this.scrollToBottom();
      this.loader.stopAll();
    });

    this.signalRServ.hubConnectionChat.on('removedMember', async (messageView: any) => {

      if (messageView) {
        this.receiver.totalMember--;
      }
    });

    this.signalRServ.hubConnectionChat.on('chatResult', async (res: any) => {

    });

    this.signalRServ.hubConnectionChat.on('memberTotal', (count: any) => {

      this.receiver.totalMember = count;
    });

    this.signalRServ.hubConnectionChat.on('onRoomDeleted', async (res: any) => {

    });

    this.signalRServ.hubConnectionChat.on('onError', (error: any) => {

    });

  }

  private getChatListResult(): Promise<any> {
    return new Promise((resolve) => {
      const paging = (this.roomList.length >= this.pageSize ? this.roomList.length : this.pageSize);
      this.signalRServ.hubConnectionChatLastMessage
        .invoke('GetLastMessageV2', this.profile.Id, paging, 1, this.profile.CompanyCode)
        .then((res) => {
          this.customizeChatList(this.roomList, res);
          if (this.isChatGroup && this.chatRoomSelected) {
            const i = this.roomList.findIndex((chatMyGroup: any) => {
              return this.chatRoomSelected.groupId == chatMyGroup.groupId;
            });
            if (i == -1) {
              this.chatRoomSelected = null;
            }
          }
          // await this.setChatUserProfile();
          resolve(true);
          //// console.log('##GetLastMessageV2##', res, paging, 1);
        }).catch(err => {
          console.error(err)
          resolve(false);
        });
    });

  }

  public setNewGroup() {
    this.newRoomNameMyCreate = JSON.parse(this.storage.getItem('newRoomNameMyCreate'));
    // console.log(this.newRoomNameMyCreate);
    if (this.newRoomNameMyCreate?.groupName) {
      let room = _.find(this.roomList, x => {
        return this.newRoomNameMyCreate.isChatGroup ?
          x.groupName == this.newRoomNameMyCreate.groupName :
          x.groupNameText == this.newRoomNameMyCreate.groupName;
      });

      if (room) {
        this.onSelectChatRoom(room, true);
      } else {
        setTimeout(async () => {
          // let msgError = await this.translate.getText('WORKPLUS.CHAT_JOIN.LEAVE_GROUP');
          // this.toast.warning(msgError);


        }, 0);
      }

      // this.chatRoomSelected.isCanDecline = true;

      this.storage.removeItem('newRoomNameMyCreate');
    }
  }

  private customizeChatList(chatListClent: any[], chatListServer: any[]) {
    chatListServer.forEach((myGroup: any, index: number) => {
      // console.log(myGroup);
      if (myGroup.isPrivate) {
        myGroup.fromUserId = (myGroup.groupName.split('|'))[0];
      }

      const i = chatListClent.findIndex((chatMyGroup: any) => {
        // return (myGroup.id === chatMyGroup.id)
        return (myGroup.isPrivate && chatMyGroup.isPrivate) ?
          myGroup.fromUserId === chatMyGroup.fromUserId : myGroup.groupId ===
          chatMyGroup.groupId;
      });




      // console.log(chatListClent[i].imageUrl, myGroup.imageUrl);

      // if (chatListClent[i]) {
      //   myGroup.groupNameText = chatListClent[i].groupNameText;
      //   myGroup.imageUrl = chatListClent[i].imageUrl;
      // } else {
      //   myGroup.groupNameText = myGroup.groupName;
      //   myGroup.imageUrl = myGroup.imageUrl;
      // }

      const n = chatListClent.filter((item: any) => {
        return !item.isAccept;
      });
      if (i !== -1 && new Date(myGroup.contentTimeStamp).toString() !== new Date(chatListClent[i].contentTimeStamp).toString() && moment().diff(myGroup.contentTimeStamp, 'second') <= 1) {
        chatListClent.splice(n.length, 0, myGroup);

        let dup = _.where(chatListClent, { 'groupName': myGroup.groupName });
        const ix = chatListClent.findIndex((x: any) => {
          return x.id == dup[1].id;
        });
        chatListClent.splice(ix, 1);
      } else if (i === -1 && (new Date().getTime() - new Date(myGroup.contentTimeStamp).getTime() > 5000) && myGroup.isAccept) {

        chatListClent.push(myGroup);
      } else if (i === -1 && new Date(myGroup.contentTimeStamp).getTime() === 0 && !myGroup.isAccept) {
        //// console.log('*****3')
        chatListClent.unshift(myGroup);
      } else if (i === -1) {
        //// console.log('*****4')
        chatListClent.unshift(myGroup);
      } else {
        //// console.log('*****5')
        chatListClent[i].totalBadge = chatListServer[index].totalBadge;
        setTimeout(() => {
          if (!chatListServer[index].isPrivate && chatListServer[index].groupName !== chatListClent[i].groupNameText && myGroup.isAccept) {
            chatListClent[i].groupNameText = chatListServer[index].groupName;
          }
        }, 100);
      }
    });

    chatListClent.forEach((chatMyGroup: any, index: number) => {
      // console.log(chatMyGroup);
      const i = chatListServer.findIndex((myGroup: any) => {
        return (myGroup.isPrivate && chatMyGroup.isPrivate) ?
          myGroup.fromUserId === chatMyGroup.fromUserId : myGroup.groupId === chatMyGroup.groupId;
      });
      // console.log(i);
      if (i === -1) {
        chatListClent.splice(index, 1);
      }
    });
    this.cotomizeRoom(this.roomList);
  }


  async onSelectChatRoom(item: any, isCanDecline = false) {
    this.roomOpen = true;
    this.roomId = item.groupId;

    if (this.current_id === item?.id) {
      // const doc = document.getElementById('chat-box');
      // doc.classList.add('chat-user-active');
      return null;
    } else {
      this.current_id = item.id;
    }

    this.chatRoomSelected = item;
    this.chatRoomSelected.totalBadge = 0;
    this.chatRoomSelected.isCanDecline = isCanDecline;
    this.isChatGroup = !this.chatRoomSelected.isPrivate;
    // console.log(this.chatRoomSelected);

    this.scrollToBottom();
    if (this.signalRServ.hubConnectionChat.state != this.signalRServ.getConnecttionState().Connected) {
      await this.signalRServ.connectChatAsync().catch(err => {

      });
    }

    //// console.log('hubConnectionChatGroup', this.signalRServ.hubConnectionChatGroup.state);
    if (this.signalRServ.hubConnectionChatGroup.state != this.signalRServ.getConnecttionState().Connected) {
      await this.signalRServ.connectChatGroupAsync();
    }

    if (item.isAccept) {
      this.onJoinChatRoom();
    }
    // const doc = document.getElementById('chat-box');
    // doc.classList.add('chat-user-active');
    setTimeout(() => {
      this.event.onChatBadge.emit(true);
    }, 1000);
   
  }

  async onJoinChatRoom() {
    // console.log(this.chatRoomSelected)
    //groupId = 0 is frined chat
    this.isChatGroup = !this.chatRoomSelected.isPrivate ? true : false;

    if (!this.isChatGroup) {

      // const socketNameArr = [this.profile.Id, this.chatRoomSelected.fromUserId].sort();
      this.socketName = this.chatRoomSelected.groupName; //socketNameArr[0] + '|' + socketNameArr[1];
      this.senderId = (this.chatRoomSelected.groupName.split('|'))[0];

      await this.getReceivedProfile();
    } else {
      this.socketName = this.chatRoomSelected.groupId;
      this.groupName = this.chatRoomSelected.groupName;
    }
    console.log(this.socketName)
    this.groupId = this.groupName;
    this.messageList = [];
    await this.onConnectJoinChat();
    this.joinRoom();
  }

  async declineGroup() {
    try {
      const res = await this.myAlert.confirm('WORKPLUS.SHARED.DECLINE_CONFIRM');
      if (res) {
        this.loader.start();
        // console.log(this.groupId, this.companyCode, this.profile.Id)
        this.signalRServ.hubConnectionChat
          .invoke('RejectGroupChat', this.chatRoomSelected.groupId, this.companyCode, this.profile.Id)
          .then(async (res) => {
            //// console.log('##RejectGroupChat', res);
            this.chatRoomSelected = null;
            this.getChatAllList();
            this.getLobbyList();
            this.loader.stopAll();
          }).catch(err => {
            this.loader.stopAll();
            console.error(err);
          });
      }

    } catch (error) {
      // console.log('##Error', error);
    }
  }

  private getReceivedProfile(): Promise<any> {
    return new Promise((resovle) => {
      if (!this.isChatGroup) {
        // this.api.getDigitalProfile(this.senderId).subscribe(res => {
        //   if (res.Code == 200) {
        //     this.receiver = res.Results;
        //     resovle(res.Results);
        //   }
        //   resovle(null);
        // }, err => {
        //   console.error(err);
        //   resovle(null);
        // });
        this.receiver = this.profile.Id;
        resovle(null);
      }
    });
  }

  async onConnectJoinChat() {
    if (!this.isConnecting) {
      this.isConnecting = true;
      this.page = 1;
      if (this.isChatGroup) {
        if (this.signalRServ.hubConnectionChatGroup.state != this.signalRServ.getConnecttionState().Connected) {
          this.onConnectJoinChat();
          return;
        }

        await this.signalRServ.hubConnectionChat.invoke('JoinGroupChat', this.socketName, this.companyCode, this.profile.Id);
      } else {
        if (this.signalRServ.hubConnectionChat.state != this.signalRServ.getConnecttionState().Connected) {
          this.onConnectJoinChat();
          return;
        }
        await this.signalRServ.hubConnectionChat.invoke('JoinPrivateRoom', this.socketName, this.profile.Id);
      }
    }
    this.ent.onCountChatBageEvent.emit();
    this.isConnecting = false;
  }

  public async joinRoom() {
    try {

      if (this.isChatGroup) {
        // console.log(this.socketName, this.companyCode, this.profile.Id)
        // console.log(this.socketName);
        this.signalRServ.hubConnectionChat
          .invoke('JoinGroupChat', this.socketName, this.companyCode, this.profile.Id)
          .then((res) => {
            //// console.log('##JoinGroupChat', res);
            this.onSocketRoomMessage();
            this.getHistoryGroupChat();
            // this.signalRServ.hubConnectionChat.invoke('memberTotal', this.groupId).then((count: number) => {
            // this.receiver.totalMember = count;
            // })
          })
          .catch(err => console.error(err));
      } else {
        this.signalRServ.hubConnectionChat
          .invoke('JoinPrivateRoom', this.socketName, this.profile.Id)
          .then(() => {
            this.onSocketRoomMessage();
            this.getMessageHistory();
          })
          .catch(err => console.error(err));
      }
    } catch (error) {
      console.error('##GetLastMessage##', error);
    }
  }

  private getHistoryGroupChat(): void {
    this.signalRServ.hubConnectionChat
      .invoke('GetMessageGroupHistory', this.socketName, this.profile.Id, this.companyCode, this.pageSize, this.page)
      .then((res) => {
        alert(1)
        if (!Array.isArray(res) || (Array.isArray(res) && res.length === 0)) {
          this.isLoading = true;
          this.isReady = true;
          return;
        }

        setTimeout(() => {
          this.isLoading = false;
        }, 1000);

        this.setOneToManyMsg(res);

        if (this.page === 1) {
          this.scrollToBottom();
        }

      }).catch(err => console.error(err));
  }

  private getMessageHistory(): void {
    this.signalRServ.hubConnectionChat
      .invoke('GetMessageHistory', this.senderId, this.pageSize, this.page, this.socketName)
      .then((res) => {
        //// console.log('##GetMessageHistory##', res, this.pageSize, this.page);

        if (!Array.isArray(res) || (Array.isArray(res) && res.length === 0)) {
          this.isLoading = true;
          this.isReady = true;
          return;
        }

        setTimeout(() => {
          this.isLoading = false;
        }, 1000);

        this.setOneToOneMsg(res);
        // setTimeout(() => {
        //   this.coreService.services.hyperLink.detectURL(this.element);
        // }, 100);
      })
      .catch(err => console.error(err));
  }

  async setOneToOneMsg(item: any) {

    this.loadMsgHided = false;
    await this.getReceivedProfile();
    // console.log(this.receiver);
    const msgGroupByDate = _.groupBy(item, function (d) {
      var dateMoment = moment(d.timestamp).format('YYYY/MM/DD');
      return dateMoment;
    });//console.log('By Date ', msgGroupByDate);

    _.each(msgGroupByDate, (dateVal: any, dateKey: any) => {
      const msgGroupByTime = _.groupBy(dateVal, function (t) {
        let dateMoment = moment(t.timestamp).format('YYYY/MM/DD HH:mm:ss');
        return dateMoment;
      });//console.log('By Time ', msgGroupByTime);

      let msgById = [];
      _.each(msgGroupByTime, (timeVal: any, timeKey: any) => {
        let msgByTime = [];

        const msgGroupById = _.groupBy(timeVal, function (id) {
          return id.to;
        });//console.log('By Id ', msgGroupById);

        _.each(msgGroupById, (idVal: any, idKey: any) => {
          // console.log(idVal);
          let msgs = [];
          for (let i = 0; i < idVal.length; i++) {
            let msg = idVal[i];
            let message: any = {};
            this.setMessage(msg, message);
            if (msg?.contentType?.toLowerCase() != 'none') {
              message.UserId = msg.to;
              message.FullNameAlt = msg.senderNameAlt;
              message.FullName = msg.senderName;
              message.isMe = (msg.to != this.profile.Id) ? true : false;
              message.totalRead = msg.totalRead;
              message.isRead = msg.isRead;
              msgs.unshift(message);
            }
          }

          msgByTime.unshift({
            time: timeKey,
            isMe: idKey != this.profile.Id ? true : false,
            ProfileImageURL: idKey != this.profile.Id ? this.profile.ProfileImageURL : this.chatRoomSelected.imageUrl,
            message: msgs
          });

          msgById.unshift({
            id: idKey,
            messageByTime: msgByTime
          });

        });

      });

      this.messageList.unshift(
        {
          date: dateKey,
          messageById: msgById
        }
      );


    });

    if (this.messageList[0].messageById.length == 1 && this.messageList[0].messageById[0].messageByTime.length == 1 && this.messageList[0].messageById[0].messageByTime[0].message.length == 0) {
      this.messageList.splice(0, 1);
    }

    if (this.page === 1) {
      this.scrollToBottom();
      setTimeout(() => {
        this.loadMsgHided = true;
      }, 300);
    }
  }

  private setOneToManyMsg(item: any) {
    this.loadMsgHided = false;

    const msgGroupByDate = _.groupBy(item, function (d) {
      var dateMoment = moment(d.timestamp).format('YYYY/MM/DD');
      return dateMoment;
    });//console.log('By Date ', msgGroupByDate);

    _.each(msgGroupByDate, (dateVal: any, dateKey: any) => {
      const msgGroupByTime = _.groupBy(dateVal, function (t) {
        let dateMoment = moment(t.timestamp).format('YYYY/MM/DD HH:mm:ss');
        return dateMoment;
      });//console.log('By Time ', msgGroupByTime);
      let msgById = [];
      let ms
      _.each(msgGroupByTime, (timeVal: any, timeKey: any) => {
        let msgByTime = [];

        const msgGroupById = _.groupBy(timeVal, function (id) {
          return id.senderId;
        });//console.log('By Id ', msgGroupById);

        _.each(msgGroupById, (idVal: any, idKey: any) => {
          // console.log(idVal);
          let msgs = [];
          for (let i = 0; i < idVal.length; i++) {
            let msg = idVal[i];
            let message: any = {};
            this.setMessage(msg, message);
            if (msg?.contentType?.toLowerCase() != 'none') {
              message.UserId = msg.senderId;
              message.FullNameAlt = msg.displayName;
              message.FullName = msg.displayName;
              message.isMe = (msg.senderId == this.profile.Id) ? true : false;
              message.totalRead = msg.totalRead;
              message.isRead = msg.isRead;
              msgs.unshift(message);
            }
          }

          msgByTime.unshift({
            time: timeKey,
            isMe: idKey == this.profile.Id ? true : false,
            ProfileImageURL: idKey == this.profile.Id ? this.profile.ProfileImageURL : idVal[0].displayImage,
            message: msgs
          });

          msgById.unshift({
            id: idKey,
            messageByTime: msgByTime
          });

        });
      });

      this.messageList.unshift(
        {
          date: dateKey,
          messageById: msgById
        }
      );

    });

    if (this.messageList[0].messageById.length == 1 &&
      this.messageList[0].messageById[0].messageByTime.length == 1 && this.messageList[0].messageById[0].messageByTime[0].message.length == 0) {
      this.messageList.splice(0, 1);
    }


    if (this.page === 1) {
      this.scrollToBottom();
      setTimeout(() => {
        this.loadMsgHided = true;
      }, 300);
    }
  }

  private setMessage(msg: any, message: any) {
    switch (msg?.contentType?.toLowerCase()) {
      case 'text':
        // message.content = msg.content as string;
        message.content = this.urlDetect.detectToUrlNewTab(this.signalRServ.decrypt(msg.content as string, 'chat'));
        break;
      case 'image':
        message.content = msg.content as string;
        message.imageFileName = this.urlDetect.getFileNameFormURL(msg.content);
        break;
      case 'file':
        message.content = this.urlDetect.getFileNameFormURL(msg.content);
        message.contentFile = msg.content;
        break;
      case 'location':
        // let attrs = this.signalRServ.decrypt(this.urlDetect.detectToUrlNewTab(msg.content as string), 'chat').split(',');
        let attrs = this.signalRServ.decrypt(msg.content as string, 'chat').split(',');
        message.content = this.ln == 'th' ? attrs[0] : attrs[1];
        message.lat = attrs[2];
        message.long = attrs[3]
        break;
    }

    if (msg?.contentType?.toLowerCase() !== 'none') {
      message.ProfileImageURL = msg.displayImage;
      message.ClientRefKey = '',
        message.Type = msg.contentType.toLowerCase(),
        message.Message = msg.contentHtml;
      message.ImageURL = msg.content;
      message.documentUrl = msg.content;
      message.IsSuccess = true;
      message.IsReceivedRead = false;
    }
  }

  private scrollToBottom() {
    try {

    } catch (err) { }
    setTimeout(() => {
      const doc = document.getElementById('chatRoom');
      if (doc) {
        doc.scroll({
          top: doc.scrollHeight,
          left: 0,
          // behavior: 'smooth'
        });
      }

    }, 150);
  }

  public openLocation(item: any) {
    window.open('https://www.google.com/maps/search/?api=1&query=' + item.lat + ',' + item.long, '_blank');
  }

  async sendMessage(type: any, element: any) {
    this.messageText = element.value.trim();

    if ((!this.messageText || this.messageText === '') && !this.fileUpload) {
      return;
    }

    if (this.fileUpload) {
      // this.sendImage();
    } else {

      await this.onConnectJoinChat();

      this.signalRServ
        .hubConnectionChat
        .invoke('SendMessage',
          this.roomId,
          this.profile.CompanyCode,
          this.profile.Id,
          this.signalRServ.encrypt(this.messageText, 'chat'),
          true,
          type,
          'ChatAdmin',
          "")
        .then(() => {
          this.getChatListResult();
        })
        .catch(err => {
          // this.onJoinChatRoom();
          // console.log('Sent ok.');
          console.error(err);
        });
    }

    this.messageText = null;
  }

  async sendFile(file: any) {
    this.loader.start();
    this.fileUpload = await this.urlDetect.getBase64(file);
    const body = {
      file: this.fileUpload as string,
      roomId: this.roomId,
      userId: this.profile.Id,
      isPrivate: !this.isChatGroup,
      type: "File",
      fileName: file.name,
      serviceCode: 'ChatAdmin'
    };

    this.fileUpload = null;
    this.anyFile = null;
    await this.onConnectJoinChat();
    await this.api.sendChatWithFile(body).then(res => {
      // console.log(res);
      this.getChatListResult();
      this.loader.stopAll();
    }).catch(err => {
      if (err.status !== 200) {
        this.toast.error(err.error)
        this.getChatListResult();
        this.loader.stopAll();
      }
    });

    // this.getChatListResult();
    // this.loader.stopAll();
  }

  async sendImage(image: any, fileName: any) {
    this.loader.start();
    this.fileUpload = image; // await this.utility.getBase64(image);
    const body = {
      file: this.fileUpload as string,
      roomName: this.socketName.toString(),
      userId: this.profile.Id,
      isPrivate: !this.isChatGroup,
      type: "Image",
      fileName: fileName,
      roomId: this.roomId,
      serviceCode: 'ChatAdmin',
    };

    this.fileUpload = null;
    this.imageFile = null;



    await this.onConnectJoinChat();
    this.api.sendChatWithFile(body).then(res => {
      // console.log(res);
      this.loader.stopAll();
      this.getChatListResult();
    }).catch(err => {
      console.log(err);
      this.loader.stopAll();
      this.getChatListResult();
    });

    // this.getChatListResult();
    // this.loader.stopAll();
  }

  public fileChangeEvent(event: any) {
    if (this.checkUploadFileLimit(event)) return;

    for (let i = 0; i < event.target.files.length; i++) {
      const file: File = event.target.files[i];
      let size = (file.size / 1000) / 1000;
      if (size >= 19.07) {
        this.toast.error('File is oversize(19.07).');
        break;
      }

      this.sendFile(file);
    }
  }

  public checkUploadFileLimit(file: any) {
    if (file.target.files.length > 5) {
      this.myAlert.alert('You can only select 5 files.');
      return true;
    }
    return false;
  }

  async joinChat() {
    if (this.chatRoomSelected) {
      let isJoin = await this.signaRjoinRoom.onJoinChatRoom(this.chatRoomSelected);
      if (isJoin) {

        this.chatRoomSelected.isAccept = true;
        this.onJoinChatRoom();
        let res = await this.getChatAllList();
        this.getLobbyList();
        // let index = this.roomList.findIndex()
        const i = this.roomList.findIndex((x: any) => {
          return (x.isPrivate === 0) ? x.fromUserId === this.chatRoomSelected.fromUserId : x.groupId === this.chatRoomSelected.groupId;
        });

        if (i != -1) {
          const n = this.roomList.filter((item: any) => {
            return !item.isAccept;
          });

          this.roomList.splice(i, 1);
          this.roomList.splice(n.length, 0, this.chatRoomSelected);
        }

      } else {
        this.toast.error('You are not permission.');
      }
    }
  }

  public openInfomation() {
    this.ent.onPersonalInfomationpModalEvent.emit({ phone: this.receiver?.PersonalPhone });
  }

  public tabChange($event: any) {
    this.currentTab = 'panelId';
    if ($event.nextState) {
      this.currentTab = $event.panelId;
    } else {
      this.currentTab = '';
    }
  }

  async getLobbyList() {
    this.getAllFriend();
    try {
      this.signalRServ.hubConnectionChatGroup
        .invoke('GetLobbyResult', this.profile.Id, this.profile.CompanyCode)
        .then((res) => {


          this.customizeLobbyChatList(this.chatInvite, res.invitationGroup, this.chatInvite.length === 0 ? 'push' : 'unshift');
          this.customizeLobbyChatList(this.chatPublic, res.publicGroup, this.chatPublic.length === 0 ? 'push' : 'unshift');
          this.customizeLobbyChatList(this.chatMyGroup, res.myGroup, this.chatMyGroup.length === 0 ? 'push' : 'unshift');

          // console.log(this.chatPublic);
          // this.chatPublic.forEach(x => x.isAccept = true);
          this.chatMyGroup.forEach(x => x.isAccept = true);

        }).catch(err => console.error(err));
    } catch (error) {
      console.error(error);
      // this.coreService.services.logEvent.log('##GetLobbyResult##', error, null);
    }
  }

  private customizeLobbyChatList(chatListClent: any[], chatListServer: any[], state: string = 'unshift') {

    chatListServer.forEach((myGroup: any, index: number) => {

      const i = chatListClent.findIndex((chatMyGroup: any) => {
        return (myGroup.isPrivate && chatMyGroup.isPrivate) ? myGroup.creator === chatMyGroup.creator : myGroup.groupId === chatMyGroup.groupId;
      });

      if (chatListClent[i]) {

        myGroup.groupNameText = chatListClent[i].groupNameText;
        myGroup.imageUrl = chatListClent[i].imageUrl;
      }

      if (i !== -1 && new Date(myGroup.contentTimeStamp).toString() !== new Date(chatListClent[i].contentTimeStamp).toString()) {
        chatListClent.splice(i, 1);
        chatListClent[state](myGroup);
      } else if (i === -1) {
        chatListClent[state](myGroup);
      } else {
        chatListClent[i].totalBadge = chatListServer[index].totalBadge;

        setTimeout(() => {
          if (chatListServer[index].groupId !== 0 && chatListServer[index].groupName !== chatListClent[i].groupNameText) {
            chatListClent[i].groupNameText = chatListServer[index].groupName;
          }
        }, 100);
      }

    });

    chatListClent.forEach((chatMyGroup: any, index: number) => {
      const i = chatListServer.findIndex((myGroup: any) => {
        return (myGroup.isPrivate === 0 && chatMyGroup.isPrivate === 0) ? myGroup.creator === chatMyGroup.creator : myGroup.groupId === chatMyGroup.groupId;
      });

      if (i === -1) {
        chatListClent.splice(index, 1);
      }
    });
  }

  async getAllFriend() {
    try {
      this.signalRServ.hubConnectionChat
        .invoke('GetAllFriends', this.profile.Id, this.profile.CompanyCode)
        .then((res) => {
          this.chatAllFriend = res;
          this.chatAllFriend.forEach(x => {
            x.content = '';
            x.groupId = 0;
            x.groupNameText = x.fullName;
            x.imageUrl = x.profileImageURL;
            x.isAccept = true;
            x.isPrivateGroup = true;
            x.senderId = x.userId;
            x.senderName = x.fullName;
            x.senderNameAlt = x.fullNameAlt;
            x.totalMember = 2;
          });

        });
    } catch (error) {
      // console.log('##ErrorTryAllFriend', error);
    }
  }

  public openEditGroup() {
    this.ent.onChatEditGrroupModalEvent.emit({
      data: this.chatRoomSelected,
      resovle: async (res: any) => {
        if (res?.isEdit) {
          await this.getChatAllList();
          this.chatRoomSelected = _.find(this.roomList, { groupId: this.chatRoomSelected.groupId });
          await this.getChatAllList();
          this.getLobbyList();
        }

        if (res?.isDelete) {
          this.chatRoomSelected = null;
          await this.getChatAllList();
          this.getLobbyList();
        }
      }
    });
  }

  async downloadImage(msg: any) {
    this.api.downloadImageByUrl(msg.content).subscribe(res => {
      if (res.Code == 200) {
        // Create a new link
        const anchor = document.createElement('a');
        anchor.href = 'data:' + res.Results;
        anchor.download = msg.imageFileName;

        // Append to the DOM
        document.body.appendChild(anchor);

        // Trigger `click` event
        anchor.click();

        // Remove element from DOM
        document.body.removeChild(anchor);
      } else {
        this.toast.error(res);
      }
    });
  }

  public onPreviewImage() {
    this.ent.onImgCroperModalEvent.emit({
      resovle: (res) => {
        this.sendImage(res.base64String, res.selectFile.name);
      }
    });
  }

  closeMobile() {
    const doc = document.getElementById('chat-box');
    doc.classList.add('chat-user-inactive');
    setTimeout(() => {
      doc.classList.remove('chat-user-active');
      doc.classList.remove('chat-user-inactive');
    }, 300);
  }

  toLobby() {
    if (this.roomChatType !== 1) {
      this.roomChatType = 1;
      this.fileNotePage = 1;
    } else {
      this.roomOpen = false;
    }
    this.leaveChat();
  }

  gotoChat() {
    // this.pageNavigateService.navigateToPage('chat');
  }

  chatRoomActive(i) {
    this.roomChatType = i;
    this.fetchfileType();
  }

  fetchfileType() {
    let fileType = '';
    if (this.roomChatType === 2) {
      fileType = 'Text';
    } else if (this.roomChatType === 3) {
      fileType = 'Image';
    } else if (this.roomChatType === 4) {
      fileType = 'File';
    }
    this.api.getFileNote(this.fileNotePage, this.fileNotePageSize, this.roomId, fileType).then(res => {
      this.fileList = res;
      this.fileListTemp = JSON.parse(JSON.stringify(res));
    });
  }
  fileAdd() {
    if (this.roomChatType === 2) {
      this.ngxSmartModalService.getModal('create').open();
    } else if (this.roomChatType === 3) {
      document.getElementById('file').click();
    } else if (this.roomChatType === 4) {
      document.getElementById('file').click();
    }
  }

  addFileNote(content, files) {
    // let fileType = '';
    // const profile = JSON.parse(this.storage.getItem(config.baseURL + '/profile') ?? {});
    // if (this.roomChatType === 2) {
    //   fileType = 'Text';
    // } else if (this.roomChatType === 3) {
    //   fileType = 'Image';
    // } else if (this.roomChatType === 4) {
    //   fileType = 'File';
    // }
    // const body = {
    //   roomId: this.roomId,
    //   content: content,
    //   noteType: fileType,
    //   userId: profile.Id,
    //   noteId: null,
    //   file: null,
    // };
    // if (files) {
    //   body.file = files;
    // }
    // this.api.createFileNote(body).then(res => {
    //   this.fetchfileType();
    //   this.note = '';
    // });
  }

  fileChange(event) {
    const file = event.target.files[0];
    this.addFileNote(file.name, file);
  }

  async leaveChat() {
    await this.signalRServ.hubConnectionChat
      .invoke('LeaveGroupRoom', this.profile.Id, this.socketName.toString());
  }
  onNoteSearch() {
    if (this.noteSearch) {
      this.fileList = this.fileList.filter(el => {
        return el.content.toLowerCase().includes(this.noteSearch.toLowerCase())
          || el.fullName.toLowerCase().includes(this.noteSearch.toLowerCase())
          || el.fullNameAlt.toLowerCase().includes(this.noteSearch.toLowerCase());
      });
    } else {
      this.noteintial();
    }
  }
  noteintial() {
    this.fileList = this.fileListTemp;
  }
  download(filePath) {
    window.open(filePath, '_blank');
  }
}
