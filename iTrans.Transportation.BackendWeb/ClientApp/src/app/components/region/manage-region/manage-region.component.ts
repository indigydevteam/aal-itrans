import { CountryService } from './../../../services/country/country.service';
import { RegionService } from './../../../services/region/region.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { MustMatch } from './../../../shared/must-match.validator';
import { first } from 'rxjs/operators';
import { Country } from '../../../models/country.model';

@Component({
  selector: 'app-manage-region',
  templateUrl: './manage-region.component.html',
  styleUrls: ['./manage-region.component.scss']
})
export class ManageRegionComponent implements OnInit {

  form!: FormGroup;
  id!: string;
  isAddMode!: boolean;
  submitted = false;
  countrys: Country[] | any;
  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private regionService: RegionService,
    private countryService: CountryService,
  ) { }

  ngOnInit(): void {
    this.getAllCountry();
    this.id = this.route.snapshot.params.id;
    this.isAddMode = !this.id;
    this.form = this.formBuilder.group({
      Id: this.id,
      countryId: [0, Validators.required],
      name_TH: ['', Validators.required],
      name_ENG: ['', Validators.required],
      sequence: ['', Validators.required],
      active: [true]
    }
    );
    if (!this.isAddMode) {
      this.regionService.getRegionById(this.id)
        .pipe(first())
        .subscribe(x => this.form.patchValue(x));
    }
  }
  get formControls() { return this.form.controls; }

  private createRegion() {
    console.log(this.form.value);
    this.regionService.createRegion(this.form.value)
      .pipe(first())
      .subscribe({
        next: () => {
          this.router.navigate(['/region'], { relativeTo: this.route });
        }
      });
  }
  private updateRegion() {
    this.regionService.updateRegion(this.form.value)
      .pipe(first())
      .subscribe({
        next: () => {
          this.router.navigate(['/region'], { relativeTo: this.route });
        }
      });
  }

  onSubmit() {
    
    this.submitted = true;
    if (this.form.invalid) {
      return;
    }

    if (this.isAddMode) {
      this.createRegion();
    } else {
      this.updateRegion();
    }

  }

  public getAllCountry() {
    this.countryService.getAllCountry("", 0, 0).subscribe(countrys => {
      this.countrys = countrys.data;
      console.log(this.countrys);
    });
  }
}
