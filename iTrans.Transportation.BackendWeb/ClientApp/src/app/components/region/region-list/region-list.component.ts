import { RegionService } from './../../../services/region/region.service';
import { Component, OnInit } from '@angular/core';
import { NgxRangeModule } from 'ngx-range';
import { first } from 'rxjs/operators';
import { Region, RegionList } from 'src/app/models/region.model';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-region-list',
  templateUrl: './region-list.component.html',
  styleUrls: ['./region-list.component.scss']
})
export class RegionListComponent implements OnInit {

  public regions: Observable<RegionList> | any;
  public page: number = 0;
  constructor(private regionservice: RegionService) { }
  public pageSize: number = 10;
  public pageNumber: number = 1;
  public search: string = "";
  ngOnInit(): void {
    this.getAllRegion();
  }

  public getAllRegion() {
    this.regionservice.getAllRegion(this.search, this.pageNumber, this.pageSize).subscribe(regions => {
      console.log(regions);
      this.regions = regions;
      this.page = regions.itemCount / (regions.pageSize == 0 ? 1 : regions.pageSize);
      if (this.page == 0) this.page = 1;
    });
  }

  public filter(event) {
    console.log("You entered: ", event.target.value);
    this.search = event.target.value;
    this.pageNumber = 1;
    this.getAllRegion();
  }
  public pageChange(pageNumber: number ) {
    console.log(pageNumber);
    this.pageNumber = pageNumber;
    this.getAllRegion();
  }
}
