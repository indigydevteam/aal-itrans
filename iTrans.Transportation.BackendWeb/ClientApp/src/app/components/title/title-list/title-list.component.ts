import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { first } from 'rxjs/operators';
import { TitleList } from './../../../models/title.model';
import { TitleService } from './../../../services/title/title.service';
import { DialogService } from './../../../services/dialog/dialog.service';

@Component({
  selector: 'app-title-list',
  templateUrl: './title-list.component.html',
  styleUrls: ['./title-list.component.scss']
})
export class TitleListComponent implements OnInit {
  public titles: Observable<TitleList> | any;
  public count: number = 0;
  public pageSize: number = 10;
  public pageNumber: number = 1;
  public search: string = "";
  active: number;
  column: string;

  constructor(private titleservice: TitleService, private dialog: DialogService) {}
  ngOnInit(): void {
    this.all();
  }

  all() {
    this.active = 1;
    this.column = "all";
    this.getAllTitle();
  }

  public getAllTitle() {
    this.titleservice.getAllTitle(this.search, this.pageNumber, this.pageSize, this.column, this.active).subscribe(titles => {
      this.titles = titles;
    });
  }

  orderByName(value) {
    if (value == this.column) {
      if (this.active == 1) {
        this.active = 2;
      } else if (this.active == 2) {
        this.active = 3;
      } else {
        this.active = 1;
      }
    } else {
      this.active = 2;
    }
    this.column = value;
    this.getAllTitle();
  }

  orderBySequence(value) {
    if (value == this.column) {
      if (this.active == 1) {
        this.active = 2;
      } else if (this.active == 2) {
        this.active = 3;
      } else {
        this.active = 1;
      }
    } else {
      this.active = 2;
    }
    this.column = value;
    this.getAllTitle();
  }
  orderByDate(value) {
    if (value == this.column) {
      if (this.active == 1) {
        this.active = 2;
      } else if (this.active == 2) {
        this.active = 3;
      } else {
        this.active = 1;
      }
    } else {
      this.active = 2;
    }
    this.column = value;
    this.getAllTitle();
  }

  public filter(event) {
    this.pageNumber = 1;
    this.getAllTitle();
  }
  previous() {
    this.pageNumber--;
    this.getAllTitle();
  }
  next() {
    this.pageNumber++;
    this.getAllTitle();
  }
  selectedPage(event) {
    this.pageNumber = event;
    this.getAllTitle();
  }

  async onDelete(id) {
    const resp = await this.dialog.confirm("Are you sure to delete ", "", false);
    if (resp) {
      this.titleservice.deleteTitleById(id)
        .pipe(first())
        .subscribe(orderingcancelstatuses => {
          this.titles = orderingcancelstatuses;
          if (this.titles.data !== null)
            this.getAllTitle();
        });
    }

  }
}
