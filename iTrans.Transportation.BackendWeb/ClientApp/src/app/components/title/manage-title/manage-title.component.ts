import { TitleService } from './../../../services/title/title.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { MustMatch } from './../../../shared/must-match.validator';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-manage-title',
  templateUrl: './manage-title.component.html',
  styleUrls: ['./manage-title.component.scss']
})
export class ManageTitleComponent implements OnInit {

  form!: FormGroup;
  id!: string;
  isAddMode!: boolean;
  submitted = false;
  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private titleService: TitleService,
  ) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.params.id;
    this.isAddMode = !this.id;
    this.form = this.formBuilder.group({
      Id: this.id,
      name_TH: ['', Validators.required],
      name_ENG: ['', Validators.required],
      //specified: [false],
      //sequence: ['', Validators.required],
      active: [true]
    }
    );
    if (!this.isAddMode) {
      this.titleService.getTitleById(this.id)
        .pipe(first())
        .subscribe(x => this.form.patchValue(x));
    }
  }
  get formControls() { return this.form.controls; }

  private createTitle() {
    console.log(this.form.value);
    this.titleService.createTitle(this.form.value)
      .pipe(first())
      .subscribe({
        next: () => {
          this.router.navigate(['/title'], { relativeTo: this.route });
        }
      });
  }
  private updateTitle() {
    this.titleService.updateTitle(this.form.value)
      .pipe(first())
      .subscribe({
        next: () => {
          this.router.navigate(['/title'], { relativeTo: this.route });
        }
      });
  }

  onSubmit() {
    
    this.submitted = true;
    if (this.form.invalid) {
      return;
    }

    if (this.isAddMode) {
      this.createTitle();
    } else {
      this.updateTitle();
    }

  }

}
