import { Pipe, PipeTransform } from '@angular/core';
import { TranslatePipe, TranslateService } from '@ngx-translate/core';
import { defaultLanguage } from 'src/app/app.default.language';

@Pipe({
  name: 'translateLanguage',
})

export class translateLanguagePipe implements PipeTransform {

  constructor(public transalte: TranslateService) { }

  transform(th: any, en: any) {
    return this.transalte.currentLang === 'th' ? th : en;
  }
}

@Pipe({
  name: 'customTranslate',
})

export class CustomTranslatePipe extends TranslatePipe implements PipeTransform {

  transform(query: string, ...args: any[]): any { 
    const modifiedQuery = query;
    const result = super.transform(modifiedQuery, args);
    return result !== modifiedQuery ? result : defaultLanguage[result] ? defaultLanguage[result] : result;
  }
}

