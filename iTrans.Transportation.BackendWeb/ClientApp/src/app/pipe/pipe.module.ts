import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { CustomTranslatePipe, translateLanguagePipe } from './CustomTranslate/custom-translate.pipe';
import { MomentAgoPipe } from './momentAgo/moment-ago.pipe';
import { MyfilterPipe } from './myfilter/myfilter.pipe';
import { StripHtmlPipe } from './stripHtml/strip-html.pipe';
import { ImageFilterPipe } from './imageFilter/image-filter.pipe';
import { PermissionPipe } from './permissionFilter/permission-filter.pipe';

@NgModule({
  declarations: [
    CustomTranslatePipe,
    translateLanguagePipe,
    MomentAgoPipe,
    MyfilterPipe,
    StripHtmlPipe,
    ImageFilterPipe,
    PermissionPipe,
  ],
  imports: [

  ],
  exports: [
    CustomTranslatePipe,
    translateLanguagePipe,
    MomentAgoPipe,
    MyfilterPipe,
    StripHtmlPipe,
    ImageFilterPipe,
    PermissionPipe,

  ]
})
export class PipeModule { }
