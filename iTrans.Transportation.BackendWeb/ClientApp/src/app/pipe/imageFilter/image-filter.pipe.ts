import { Pipe, PipeTransform } from '@angular/core';
import { environment } from '../../../environments/environment';
@Pipe({
  name: 'imageFilter'
})
export class ImageFilterPipe implements PipeTransform {
  transform(items: any[], documentType: string): any {
    if(items && items.length > 0){
      var item = items.filter(x => {
        if(documentType){
          return x.documentType == documentType;
        } else {
          return true;
        }
      });
      return environment.baseWebContentUrl +'/'+ item[item.length-1].filePath;
    } else {
      return items;
    }
  }

}
