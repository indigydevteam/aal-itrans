import { Pipe, PipeTransform } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import * as moment from 'moment';

@Pipe({
  name: 'momentAgo'
})
export class MomentAgoPipe implements PipeTransform {

  constructor(
    private translate: TranslateService
  ) { }
  
  transform(value: any): any {
    let ln = this.translate.currentLang || 'en';
    return value ? moment(value).lang(ln).fromNow() : '';
  }

}
