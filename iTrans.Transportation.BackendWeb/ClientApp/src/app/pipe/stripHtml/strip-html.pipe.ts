import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'stripHtml'
})
export class StripHtmlPipe implements PipeTransform {

  transform(value: string): unknown {
    if(!value){
      return "-";
    }
    let doc = new DOMParser().parseFromString(value, 'text/html');
   
    return (doc.body.textContent || doc.body.textContent == "null") || "";

  }

}
