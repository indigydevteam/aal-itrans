
export interface ApiResponse {
  succeeded: boolean;
  message: string;
  errors: string;
  data: any;
}

export interface ApiPagedResponse {
  succeeded: boolean;
  message: string;
  errors: string;
  itemCount: number;
  pageNumber: number;
  pageSize: number;
  data: any;
}
