export interface ProductType {
  Id: number | undefined;
  Name_TH: string | undefined;
  Name_ENG: string | undefined;
  Sequence: number | undefined;
  Active: boolean | undefined;
  Specified: boolean | undefined;
  file: any | undefined;
  Created: Date | undefined;
  Modified: Date | undefined;
}
export interface ProductTypeList {
  data: ProductType[] | any;
  pageNumber: number | undefined;
  pageSize: number | undefined;
  itemCount: number | undefined;
}

export interface ProductPackaging {
  Id: number | undefined;
  Name_TH: string | undefined;
  Name_ENG: string | undefined;
  Sequence: number | undefined;
  Active: boolean | undefined;
  Specified: boolean | undefined;
  file: any | undefined;
  Created: Date | undefined;
  Modified: Date | undefined;
}

export interface ProductPackagingList {
  data: ProductPackaging[] | any;
  pageNumber: number | undefined;
  pageSize: number | undefined;
  itemCount: number | undefined;
}
