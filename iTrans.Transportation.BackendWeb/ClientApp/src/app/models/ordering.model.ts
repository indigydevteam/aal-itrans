
export interface OrderingCancelStatus {
  Id: number | undefined;
  Module: string | undefined;
  Name_TH: string | undefined;
  Name_ENG: string | undefined;
  Sequence: string | undefined;
  Active: boolean | undefined;
}

export interface OrderingStatusForUpdate {
  OrderingId: string | undefined;
  StatusId: string | undefined;
  CancelStatus: string | undefined;
}

export interface OrderingAddressStatusForUpdate {
  OrderingId: string | undefined;
  OrderAddressingId: string | undefined;
  StatusId: string | undefined;
  CancelStatus: string | undefined;
}

export interface OrderingCancelStatusList {
  data: OrderingCancelStatus[] | any;
  pageNumber: number | undefined;
  pageSize: number | undefined;
  itemCount: number | undefined;
}
