export interface Region {
  Id: number | undefined;
  countryId: number | undefined;
  countryName: string | undefined;
  Name_TH: string | undefined;
  Name_ENG: string | undefined;
  sequence: number | undefined;
  active: boolean | undefined;
}

export interface RegionList {
  data: Region[] | any;
  pageNumber: number | undefined;
  pageSize: number | undefined;
  itemCount: number | undefined;
}
export interface Region {
  Id: number | undefined;
  countryId: number | undefined;
  countryName: string | undefined;
  Name_TH: string | undefined;
  Name_ENG: string | undefined;
  sequence: number | undefined;
  active: boolean | undefined;
}

export interface RegionList {
  data: Region[] | any;
  pageNumber: number | undefined;
  pageSize: number | undefined;
  itemCount: number | undefined;
}
