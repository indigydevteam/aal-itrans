export interface RegisterInformation {
  Id: number | undefined;
  Module: string | undefined;
  Title_TH: string | undefined;
  Title_ENG: string | undefined;
  Information_TH: string | undefined;
  Information_ENG: string | undefined;
  Picture: string | undefined;
  Sequence: string | undefined;
  Active: boolean | undefined;
  Created: Date | undefined;
  Modified: Date | undefined;
}
export interface RegisterInformationList {
  data: RegisterInformation[] | any;
  pageNumber: number | undefined;
  pageSize: number | undefined;
  itemCount: number | undefined;
}
export interface RegisterDocument {
  Id: number | undefined;
  Module: string | undefined;
  Information_TH: string | undefined;
  Information_ENG: string | undefined;
  Picture: string | undefined;
  Sequence: string | undefined;
  Active: boolean | undefined;
}
export interface RegisterDocumentList {
  data: RegisterDocument[] | any;
  pageNumber: number | undefined;
  pageSize: number | undefined;
  itemCount: number | undefined;
}
