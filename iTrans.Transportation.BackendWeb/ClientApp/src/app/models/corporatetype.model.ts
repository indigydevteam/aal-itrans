
  export interface CorporateType {
    Id: number | undefined;
    Name_TH: string | undefined;
    Name_ENG: string | undefined;
    Sequence: number | undefined;
    Specified: boolean | undefined;
    Active: boolean | undefined;
    Created: Date | undefined;
    Modified: Date | undefined;
  }
  
  export interface CorporateTypeList {
    data: CorporateType[] | any;
    pageNumber: number | undefined;
    pageSize: number | undefined;
    itemCount: number | undefined;
  }
