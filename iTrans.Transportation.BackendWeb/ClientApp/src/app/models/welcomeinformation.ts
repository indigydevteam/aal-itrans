
  export interface Welcomeinformation {
    Id: number | undefined;
    Module: string | undefined;
    Title_TH: string | undefined;
    Title_ENG: string | undefined;
    Information_TH: string | undefined;
    Information_ENG: string | undefined;
    Picture: string | undefined;
    Sequence: number | undefined;
    Active: boolean | undefined;
    Created: Date | undefined;
    Modified: Date | undefined;
  }
  
  export interface WelcomeinformationList {
    data: Welcomeinformation[] | any;
    pageNumber: number | undefined;
    pageSize: number | undefined;
    itemCount: number | undefined;
  }
