export interface CustomerLevelCharacteristic {
  id: number | undefined;
  level: number | undefined;
  requestCar: number | undefined;
  star: number | undefined;
  cancelAmount: number | undefined;
  orderValue: number | undefined;
}
export interface CustomerLevelCharacteristicList {
  data: CustomerLevelCharacteristic[] | any;
  pageNumber: number | undefined;
  pageSize: number | undefined;
  itemCount: number | undefined;
}
