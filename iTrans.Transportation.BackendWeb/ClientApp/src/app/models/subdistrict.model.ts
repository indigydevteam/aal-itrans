export interface Subdistrict {
  Id: number | undefined;
  districtId: number | undefined;
  districtName: string | undefined;
  Name_TH: string | undefined;
  Name_ENG: string | undefined;
  postCode: string | undefined;
  active: boolean | undefined;
}

export interface SubdistrictList {
  data: Subdistrict[] | any;
  pageNumber: number | undefined;
  pageSize: number | undefined;
  itemCount: number | undefined;
}
