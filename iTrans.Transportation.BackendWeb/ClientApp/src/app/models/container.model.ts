
  export interface ContainerType {
    Id: number | undefined;
    Name_TH: string | undefined;
    Name_ENG: string | undefined;
    Specified: boolean | undefined;
    Active: boolean | undefined;
    Sequence: number | undefined;

  }
  
  export interface ContainerTypeList {
    data: ContainerType[] | any;
    pageNumber: number | undefined;
    pageSize: number | undefined;
    itemCount: number | undefined;
  }
  
  
  export interface ContainerSpecification {
    Id: number | undefined;
    Name_TH: string | undefined;
    Name_ENG: string | undefined;
    Specified: boolean | undefined;
    Active: boolean | undefined;
    Sequence: number | undefined;

  }
  
  export interface ContainerSpecificationList {
    data: ContainerSpecification[] | any;
    pageNumber: number | undefined;
    pageSize: number | undefined;
    itemCount: number | undefined;
  }
  