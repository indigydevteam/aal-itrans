export interface District {
  Id: number | undefined;
  provinceId: number | undefined;
  provinceName: string | undefined;
  Name_TH: string | undefined;
  Name_ENG: string | undefined;
  active: boolean | undefined;
}

export interface DistrictList {
  data: District[] | any;
  pageNumber: number | undefined;
  pageSize: number | undefined;
  itemCount: number | undefined;
}
