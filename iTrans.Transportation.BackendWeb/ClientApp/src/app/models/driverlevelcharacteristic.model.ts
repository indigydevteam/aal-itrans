export interface DriverLevelCharacteristic {
  id: number | undefined;
  star: number | undefined;
  acceptJobPerMonth: number | undefined;
  complaintPerMonth: number | undefined;
  rejectPerMonth: number | undefined;
  announcementPerMonth: number | undefined;
  announcementPerYear: number | undefined;
  cancelPerMonth: number | undefined;
  complaintPerYear: number | undefined;
  rejectPerYear: number | undefined;
  cancelPerYear: number | undefined;
  InsuranceValue: number | undefined;
}
export interface DriverLevelCharacteristicList {
  data: DriverLevelCharacteristic[] | any;
  pageNumber: number | undefined;
  pageSize: number | undefined;
  itemCount: number | undefined;
}
