export interface Country {
  Id: number | undefined;
  Name_TH: string | undefined;
  Name_ENG: string | undefined;
  Code: string | undefined;
  Active: boolean | undefined;
}

export interface CountryList {
  data: Country[] | any;
  pageNumber: number | undefined;
  pageSize: number | undefined;
  itemCount: number | undefined;
}
