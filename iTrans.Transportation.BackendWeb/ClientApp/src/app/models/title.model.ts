export interface Title {
  Id: number | undefined;
  Name_TH: string | undefined;
  Name_ENG: string | undefined;
  Specified: boolean | undefined;
  Sequence: number | undefined;
  Active: boolean | undefined;
  Created: Date | undefined;
  Modified: Date | undefined;
}

export interface TitleList {
  data: Title[] | any;
  pageNumber: number | undefined;
  pageSize: number | undefined;
  itemCount: number | undefined;
}
