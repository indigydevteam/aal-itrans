export interface CarType {
  Id: number | undefined;
  Name_TH: string | undefined;
  Name_ENG: string | undefined;
  Sequence: string | undefined;
  IsContainer: boolean | undefined;
  Active: boolean | undefined;
  Created: Date | undefined;
  Modified: Date | undefined;
}
export interface CarTypeList {
  data: CarType[] | any;
  pageNumber: number | undefined;
  pageSize: number | undefined;
  itemCount: number | undefined;
}
export interface CarList {
  Id: number | undefined;
  //CarTypeId: number | undefined;
  //CarTypeName: string | undefined,
  CarType: CarType | undefined;
  Name_TH: string | undefined;
  Name_ENG: string | undefined;
  Sequence: string | undefined;
  Active: boolean | undefined;
  Created: Date | undefined;
  Modified: Date | undefined;
}
export interface CarLists {
  data: CarList[] | any;
  pageNumber: number | undefined;
  pageSize: number | undefined;
  itemCount: number | undefined;
}
export interface CarDescription {
  Id: number | undefined;
  CarType: CarType | undefined;
  CarList: CarList | undefined;
  Name_TH: string | undefined;
  Name_ENG: string | undefined;
  Sequence: string | undefined;
  Active: boolean | undefined;
  Specified: boolean | undefined;
  Created: Date | undefined;
  Modified: Date | undefined;
}
export interface CarDescriptionList {
  data: CarDescription[] | any;
  pageNumber: number | undefined;
  pageSize: number | undefined;
  itemCount: number | undefined;
}
export interface CarSpecification {
  Id: number | undefined;
  CarType: CarType | undefined;
  //CarList: CarList | undefined;
  Name_TH: string | undefined;
  Name_ENG: string | undefined;
  Sequence: string | undefined;
  Active: boolean | undefined;
  Specified: boolean | undefined;
  SpecifiedTemperature: boolean | undefined;
  SpecifiedEnergySavingDevice: boolean | undefined;
  file: any | undefined;
  Created: Date | undefined;
  Modified: Date | undefined;
}
export interface CarSpecificationList {
  data: CarSpecification[] | any;
  pageNumber: number | undefined;
  pageSize: number | undefined;
  itemCount: number | undefined;
}
export interface CarFeature {
  Id: number | undefined;
  CarType: CarType | undefined;
  CarList: CarList | undefined;
  Name_TH: string | undefined;
  Name_ENG: string | undefined;
  Sequence: string | undefined;
  Active: boolean | undefined;
  Specified: boolean | undefined;
  Created: Date | undefined;
  Modified: Date | undefined;
}
export interface CarFeatureList {
  data: CarFeature[] | any;
  pageNumber: number | undefined;
  pageSize: number | undefined;
  itemCount: number | undefined;
}
