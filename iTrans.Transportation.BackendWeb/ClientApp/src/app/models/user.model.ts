
export interface UserLevel {
  id: number | undefined;
  star: number | undefined;
  module: string | undefined;
  level: number | undefined;
  name_TH: string | undefined;
  name_ENG: string | undefined;
  active: boolean | undefined;
  customerStar: number | undefined;
  customerRequestCarPerWeek: number | undefined;
  customerRequestCarPerMonth: number | undefined;
  customerRequestCarPerYear: number | undefined;
  customerCancelPerWeek: number | undefined;
  customerCancelPerMonth: number | undefined;
  customerCancelPerYear: number | undefined;
  customerOrderingValuePerWeek: number | undefined;
  customerOrderingValuePerMonth: number | undefined;
  customerOrderingValuePerYear: number | undefined;
  customerDiscount: number | undefined;
  customerFine: number | undefined;

  driverAcceptJobPerWeek: number | undefined;
  driverAcceptJobPerMonth: number | undefined;
  driverAcceptJobPerYear: number | undefined;
  driverCancelPerWeek: number | undefined;
  driverCancelPerMonth: number | undefined;
  driverCancelPerYear: number | undefined;
  driverComplaintPerWeek: number | undefined;
  driverComplaintPerMonth: number | undefined;
  driverComplaintPerYear: number | undefined;
  driverRejectPerWeek: number | undefined;
  driverRejectPerMonth: number | undefined;
  driverRejectPerYear: number | undefined;
  driverInsuranceValue: number | undefined;
  driverCommission: number | undefined;
  driverDiscount: number | undefined;
  driverFine: number | undefined;
  conditions: UserLevelCondition[] | undefined;
}
export interface UserLevelCondition {
  id: number | undefined;
  userLevelId: number | undefined;
  characteristics_TH: string | undefined;
  characteristics_ENG: string | undefined;
  sequence: number | undefined;
  active: boolean | undefined;
}
export interface UserLevelList {
  data: UserLevel[] | any;
  pageNumber: number | undefined;
  pageSize: number | undefined;
  itemCount: number | undefined;
}

