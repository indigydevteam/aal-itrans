export interface Province {
  Id: number | undefined;
  countryId: number | undefined;
  countryName: string | undefined;
  regionId: number | undefined;
  regionName: string | undefined;
  Name_TH: string | undefined;
  Name_ENG: string | undefined;
  active: boolean | undefined;
}

export interface ProvinceList {
  data: Province[] | any;
  pageNumber: number | undefined;
  pageSize: number | undefined;
  itemCount: number | undefined;
}
