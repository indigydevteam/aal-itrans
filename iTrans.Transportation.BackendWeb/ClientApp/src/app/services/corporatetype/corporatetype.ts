import { environment } from './../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiResponse, ApiPagedResponse } from '../../models/api-response';
import { map } from 'rxjs/operators';
import { CorporateType, CorporateTypeList } from '../../models/corporatetype.model';
import { HttpService } from '../http/http.service';
import { Column } from 'ag-grid-community';

@Injectable({
  providedIn: 'root'
})
export class CorporateTypeService {
  public corporatrtypes: CorporateTypeList | any;
  constructor(private http: HttpService) { }

  public getAllCorporateType(search: string, pageNumber: number, pageSize: number, column: string, active: number): Observable<CorporateTypeList> {
 
    return this.http.post(`/CorporateType/Backoffice/GetAll`, {
      search: search,
      pageNumber: pageNumber,
      pageSize: pageSize,
      column:column,
      active:active
    }).pipe(
      map(
        (response: ApiPagedResponse) => {
          return response;
        }
      )
    );
  }
 
  
  public getCorporateTypeById(id: string): Observable<CorporateType> {

    return this.http.post(`/CorporateType/Backoffice/GetCorporateTypeById`, { Id: id }).pipe(
      map(
        (response: ApiResponse) => {
          return response.data;
        }
      )
    );
  }
  public deleteCorporateTypeById(id: string): Observable<CorporateType> {

    return this.http.post(`/CorporateType/Backoffice/DeleteCorporateTypeById`, { Id: id }).pipe(
      map(
        (response: ApiResponse) => {
          return response.data;
        }
      )
    );
  }
  public createCorporateType(params: any): Observable<CorporateType> {

    return this.http.post(`/CorporateType/Backoffice/CreateCorporateType`, params).pipe(
      map(
        (response: ApiResponse) => {
          return response.data;
        }
      )
    );

  }

  public getAllCorporateTypes(){
 
    return this.http.post(`/CorporateType/Backoffice/GetAllCorporateType`, {}).pipe(
      map(
        (response: ApiPagedResponse) => {
          return response.data;
        }
      )
    );
  }

  public updateCorporateType(params: any) {
    return this.http.put('/CorporateType/Backoffice/UpdateCorporateTypeById',params);
  }
  
}
