import { Country, CountryList } from './../../models/country.model';
import { environment } from './../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiResponse, ApiPagedResponse } from '../../models/api-response';
import { map } from 'rxjs/operators';
import { HttpService } from '../http/http.service';
import { OrderingAddressStatusForUpdate, OrderingStatusForUpdate } from '../../models/ordering.model';

@Injectable({
  providedIn: 'root'
})
export class DeliveryService {
  constructor(
    private http: HttpClient,
    private httpx: HttpService
  ) { }
  public getAllDelivery(search: string, status: number, pageNumber: number, pageSize: number, column: string, active: number) {

    return this.httpx.post('/Ordering/Backoffice/GetAllOrdering', {
      search: search,
      filter: status,
      pageNumber: pageNumber,
      pageSize: pageSize,
      column:column,
      active:active
    });
  }

  public getOrderingStatus() {

    return this.httpx.post('/Ordering/Backoffice/GetOrderingStatus', {
    });
  }
  public getOrderingAddressStatus() {

    return this.httpx.post('/Ordering/Backoffice/GetOrderingAddressStatus', {
    });
  }

  public getDeliveryById(id: string) {

    return this.httpx.post('/Ordering/Backoffice/GetOrderingById', {
      Id: id
    });

  }
  public getAllOrderingAddressByOrderingId(id: string, search: string, pageNumber: number, pageSize: number) {

    return this.httpx.post('/OrderingAddress/Backoffice/GetAllOrderingAddressByOrderingId', {
      orderingId: id,
      search: search,
      pageNumber: pageNumber,
      pageSize: pageSize
    });
  }

  public updateOrderingStatus(params: any): Observable<OrderingStatusForUpdate> {
    console.log(params);
    return this.httpx.post(`/Ordering/Backoffice/UpdateOrderingStatus`, params).pipe(
      map(
        (response: ApiResponse) => {
          return response.data;
        }
      )
    );
  }
  public updateOrderingAddressStatus(params: any): Observable<OrderingAddressStatusForUpdate> {
    console.log(params);
    return this.httpx.post(`/Ordering/Backoffice/UpdateAddressStatus`, params).pipe(
      map(
        (response: ApiResponse) => {
          return response.data;
        }
      )
    );

  }
}
