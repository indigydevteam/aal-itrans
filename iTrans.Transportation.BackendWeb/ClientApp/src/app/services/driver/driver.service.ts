

import { Country, CountryList} from '../../models/country.model';
import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiResponse, ApiPagedResponse } from '../../models/api-response';
import { map } from 'rxjs/operators';
import { HttpService } from '../http/http.service';

@Injectable({
  providedIn: 'root'
})
export class DriverService {
  constructor(
    private http: HttpClient,
    private httpx: HttpService
  ) { }
  
  public getAllDriverProfile(search: string,drivertype:string, pageNumber: number, pageSize: number, column: string, active: number){
    return this.httpx.post('/Driver/Backoffice/GetAllDriver',{
      search: search,
      drivertype:drivertype,
      pageNumber: pageNumber,
      pageSize: pageSize,
      column: column,
      active: active,
    });
  }

  public getDriverProfileById(id: string) {
    return this.httpx.post('/Driver/Backoffice/GetDriverById', { Id: id });
  }

  public getAllContactPersonTitle(id: string) {
    return this.http.post<ApiResponse>(`${environment.baseWebApiUrl}/Title/GetAllTitle`, { Id: id }).pipe(
      map(
        (response: ApiResponse) => {
          return response.data;
        }
      )
    );
  }
  public getAllUserLevel(id: string) {
    return this.httpx.post(`/UserLevel/GetAllDriverLevel`, { Id: id }).pipe(
      map(
        (response: ApiResponse) => {
          return response.data;
        }
      )
    );
  }

  public getAllCountry() {
    return this.httpx.post(`/Country/GetAllCountry`, { }).pipe(
      map(
        (response: ApiResponse) => {
          return response.data;
        }
      )
    );
  }

  public getAllProvince(id: string) {
    return this.http.post<ApiResponse>(`${environment.baseWebApiUrl}/Province/GetProvinceByCountry`, { countryId: id }).pipe(
      map(
        (response: ApiResponse) => {
          return response.data;
        }
      )
    );
  }

  public getAllDistrict(id: string) {
    return this.http.post<ApiResponse>(`${environment.baseWebApiUrl}/District/GetDistrictByProvince`, { provinceId: id }).pipe(
      map(
        (response: ApiResponse) => {
          return response.data;
        }
      )
    );
  }

  public getAllSubdistrict(id: string) {
    return this.http.post<ApiResponse>(`${environment.baseWebApiUrl}/Subdistrict/GetSubdistrictByDistrict`, { districtId: id }).pipe(
      map(
        (response: ApiResponse) => {
          return response.data;
        }
      )
    );
  }

  public updateDriver(params: any) {
    return this.httpx.put('/Driver/Backoffice/UpdateDriverById',params);
  }

  public DeleteDriver(id: any) {
    return this.httpx.post('/Driver/Backoffice/DeleteDriverById', { Id: id });
  }

  public DeleteDriverCar(id: any) {
    return this.http.post<ApiResponse>(`${environment.baseWebApiUrl}/DriverCar/Backoffice/DeleteDriverCarById`, { Id: id }).pipe(
      map(
        (response: ApiResponse) => {
          return response;
        }
      )
    );
  }

  public getAllDriverCar(search: string,drivertype:string, pageNumber: number, pageSize: number, column: string, active: number){
    return this.httpx.post('/DriverCar/Backoffice/GetAllDriverCar', {
      search: search,
      drivertype:drivertype,
      pageNumber: pageNumber,
      pageSize: pageSize,
      column: column,
      active: active,
    });
  }

  public getDriverCarById(id: string) {
    return this.httpx.post('/DriverCar/Backoffice/GetDriverCarById', { Id: id });
  }
  public getDriverCarByOwnerId(id: string,search: string, pageNumber: number, pageSize: number,column: string, active: number) {
    return this.httpx.post('/DriverCar/Backoffice/GetDriverCarByOwnerId', {
       ownerId: id,
       search: search,
       pageNumber: pageNumber,
       pageSize: pageSize,
       column: column,
      active: active,
     });
  }


  public getTermCondition(id: string) {
    return this.httpx.post('/Driver/Backoffice/CheckTermAndCondition', { Id: id });
  }

  public GetDriverTaxById(id:string) {
    return this.httpx.post('/Driver/Backoffice/GetDriverTaxById', { driverId: id });
  }

  public updateDriverAddress(location: any) {
    return this.httpx.put('/Driver/Backoffice/UpdataDriverTaxById',location);
  }

  public GetDriverByVerifyStatus() {
    return this.httpx.post('/Driver/Backoffice/GetDriverByVerifyStatus', {});
  }

}
