import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { NgxCoolDialogsService } from 'ngx-cool-dialogs';
import { defaultLanguage } from 'src/app/app.default.language';

@Injectable({
  providedIn: 'root'
})
export class AlertService {

  constructor(
    private coolAlt: NgxCoolDialogsService,
    private translateService: TranslateService
  ) { }

  async confirm(msgKey: any, customText = { titleKey: '', okButtonKey: '', cancelButtonKey: '' }): Promise<any> {
    let delMsg = await this.translateService.get(msgKey).toPromise();
    if (delMsg == msgKey) {
      delMsg = defaultLanguage[delMsg];
    }

    return new Promise(async (resovle) => {
      let alt = this.coolAlt.confirm(delMsg, {
        okButtonText: await this.getOkText(customText.okButtonKey),
        cancelButtonText: await this.getCancelText(customText.cancelButtonKey),
        title: await this.getConfirmText(customText.titleKey)
      }).subscribe(res => {
        alt.unsubscribe();
        resovle(res);
      });
    });
  }

  async alert(msgKey: any, config?: ReplaceValue): Promise<any> {
    let delMsg = await this.translateService.get(msgKey).toPromise();
    if (delMsg == msgKey) {
      delMsg = defaultLanguage[delMsg];
    }

    config?.replaces.forEach(n => {
      delMsg = delMsg.replace(n.k, n.v);
    });

    return new Promise(async (resovle) => {
      let alt = this.coolAlt.confirm(delMsg, {
        title: await this.getConfirmText(config.titleKey)
      }).subscribe(res => {
        alt.unsubscribe();
        resovle(res);
      });
    });
  }

  private getOkText(okButtonKey: any): Promise<any> {
    return new Promise(async (resovle) => {
      let strKey = okButtonKey ? okButtonKey : 'WORKPLUS.SHARED.OK';
      let msg = await this.translateService.get(strKey).toPromise();
      if (msg == strKey) {
        msg = defaultLanguage[msg];
      }
      resovle(msg);
    });
  }

  private getCancelText(cancelButtonKey: any): Promise<any> {
    return new Promise(async (resovle) => {
      let strKey = cancelButtonKey ? cancelButtonKey : 'WORKPLUS.SHARED.CANCEL';
      let msg = await this.translateService.get(strKey).toPromise();
      if (msg == strKey) {
        msg = defaultLanguage[msg];
      }
      resovle(msg);
    });
  }

  private getConfirmText(titleKey: any): Promise<any> {
    return new Promise(async (resovle) => {
      let strKey = titleKey ? titleKey : 'WORKPLUS.SHARED.CONFIRM';
      let msg = await this.translateService.get(strKey).toPromise();
      if (msg == strKey) {
        msg = defaultLanguage[msg];
      }
      resovle(msg);
    });
  }

}

export interface ReplaceValue {
  titleKey: any;
  replaces: Array<{ k, v }>;
}
