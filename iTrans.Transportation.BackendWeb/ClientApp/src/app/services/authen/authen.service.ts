import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { promise } from 'selenium-webdriver';
import { HttpService } from '../http/http.service';
import { StorageService } from '../storage/storage.service';
import { environment } from '../../../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class AuthenService {

  constructor(
    private http: HttpService,
    private storage: StorageService,
    private httpclient: HttpClient
  ) { }

  login(body: any) {
    return new Promise((resolve, reject) => {
      const params = this.jsonToQueryString(body);
      resolve(this.httpclient.post(environment.baseWebAuth + '/connect/token', params, {
        headers: new HttpHeaders({
          'Content-Type': 'application/x-www-form-urlencoded'
        })
      }).toPromise());
    });
  }



  isLoggedIn() {
    return this.storage.getItem('token') !== null;
  }

  jsonToQueryString(body) {
    return '' +
      Object.keys(body).map((key: any) => { return encodeURIComponent(key) + "=" + encodeURIComponent(body[key]); }).join('&');
  }
}
