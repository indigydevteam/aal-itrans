import { environment } from './../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiResponse, ApiPagedResponse } from '../../models/api-response';
import { map } from 'rxjs/operators';
import { EnergySavingDevice, EnergySavingDeviceList } from 'src/app/models/energysavingdevice.model';
import { HttpService } from '../http/http.service';

@Injectable({
  providedIn: 'root'
})
export class EnergySavingDeviceService {
  constructor(private http: HttpService) { }
  
  public getAllEnergySavingDevice(search: string, pageNumber: number, pageSize:number, column: string, active:number): Observable<EnergySavingDeviceList> {
 
    return this.http.post(`/EnergySavingDevice/Backoffice/GetAllEnergySavingDevice`, {
      search: search,
      pageNumber: pageNumber,
      pageSize: pageSize,
      column:column,
      active:active
    }).pipe(
      map(
        (response: ApiPagedResponse) => {
          return response;
        }
      )
    );
  }

  public getEnergySavingDeviceById(id: string): Observable<EnergySavingDevice> {

    return this.http.post(`/EnergySavingDevice/Backoffice/GetEnergySavingDeviceById`, { Id: id }).pipe(
      map(
        (response: ApiResponse) => {
          return response.data;
        }
      )
    );
  }
  public deleteEnergySavingDevice(id: string) {

    return this.http.post(`/EnergySavingDevice/Backoffice/DeleteEnergySavingDeviceById`, { Id: id }).pipe(
      map(
        (response: ApiResponse) => {
          return response;
        }
      )
    );
  }
  public createEnergySavingDevice(params: any): Observable<EnergySavingDevice> {

    return this.http.post(`/EnergySavingDevice/Backoffice/CreateEnergySavingDevice`, params).pipe(
      map(
        (response: ApiResponse) => {
          return response.data;
        }
      )
    );

  }
  public updateEnergySavingDevice(params: any): Observable<EnergySavingDevice> {

    return this.http.put(`/EnergySavingDevice/Backoffice/UpdateEnergySavingDeviceById`, params).pipe(
      map(
        (response: ApiResponse) => {
          return response.data;
        }
      )
    );

  }
  
}
