import { Title, TitleList } from './../../models/title.model';
import { environment } from './../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiResponse, ApiPagedResponse } from '../../models/api-response';
import { map } from 'rxjs/operators';
import { HttpService } from '../http/http.service';
import { Column } from 'ag-grid-community';

@Injectable({
  providedIn: 'root'
})
export class TitleService {
  public titlelist: TitleList | any;
  constructor(private http: HttpService) { }

  public getAllTitle(search: string, pageNumber: number, pageSize: number, column: string, active: number): Observable<TitleList> {
    return this.http.post(`/Title/Backoffice/GetAll`, {
      search: search,
      pageNumber: pageNumber,
      pageSize: pageSize,
      column: column,
      active: active
    }).pipe(
      map(
        (response: ApiPagedResponse) => {
          return response;
        }
      )
    );
  }

  public getTitleById(id: string): Observable<Title> {
    return this.http.post(`/Title/Backoffice/GetById`, { Id: id }).pipe(
      map(
        (response: ApiResponse) => {
          return response.data;
        }
      )
    );
  }
  public createTitle(params: any): Observable<Title> {
    return this.http.post(`/Title/Backoffice/Create`, params).pipe(
      map(
        (response: ApiResponse) => {
          return response.data;
        }
      )
    );
  }
  public updateTitle(params: any): Observable<Title> {
    return this.http.put('/Title/Backoffice/UpdateById', params);
  }
  public deleteTitleById(id: string): Observable<Title> {

    return this.http.post(`/Title/Backoffice/DeleteTitleById`, { Id: id }).pipe(
      map(
        (response: ApiResponse) => {
          return response.data;
        }
      )
    );
  }
}
