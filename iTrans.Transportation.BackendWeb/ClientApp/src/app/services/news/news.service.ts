import { Injectable } from '@angular/core';
import { HttpService } from '../http/http.service';

@Injectable({
  providedIn: 'root'
})
export class NewsService {

  constructor(private http: HttpService) { }
  async getNews(body): Promise<any> {
    const endpoint = `/News/GetNews`;
    return this.http.post(endpoint, body).toPromise();
  }
  async insertNews(body): Promise<any> {
    const endpoint = `/News/CreateNews`;
    return this.http.post(endpoint, body).toPromise();
  }
  async updateNews(body): Promise<any> {
    const endpoint = `/News/EditNews`;
    return this.http.post(endpoint, body).toPromise();
  }
  async getNewsById(body): Promise<any> {
    const endpoint = `/News/GetNewsByID`;
    return this.http.post(endpoint, body).toPromise();
  }
  async deleteNews(body): Promise<any> {
    const endpoint = `/News/DeleteNews`;
    return this.http.post(endpoint, body).toPromise();
  }
}
