import { Country, CountryList} from '../../models/country.model';
import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiResponse, ApiPagedResponse } from '../../models/api-response';
import { map } from 'rxjs/operators';
import { HttpService } from '../http/http.service';

@Injectable({
  providedIn: 'root'
})
export class HelpService {
  constructor(
    private http: HttpService,
    ) { }
  
  public getAllComplain(search: string, pageNumber: number, pageSize: number, column: string, active: number){
 
    return this.http.post(`/CustomerComplainAndRecommend/Backoffice/GetAllCustomerComplainAndRecommend`, {
      search: search,
      pageNumber: pageNumber,
      pageSize: pageSize,
      column:column,
      active:active
    }).pipe(
      map(
        (response: ApiPagedResponse) => {
          return response;
        }
      )
    );
  }
  public getComplainById(id: any,module:string) {

    return this.http.post(`/CustomerComplainAndRecommend/Backoffice/GetCustomerComplainAndRecommendById`, { Id: id ,module:module}).pipe(
      map(
        (response: ApiResponse) => {
          return response.data;
        }
      )
    );
  }
  public getFAQById(id: any) {

    return this.http.post(`/FAQ/GetFAQById`, { Id: id }).pipe(
      map(
        (response: ApiResponse) => {
          return response.data;
        }
      )
    );
  }
  public createFAQ(faqData: any) {

    return this.http.post(`/FAQ/BackOffice/CreateFAQ`, {faqData:faqData}).pipe(
      map(
        (response: ApiResponse) => {
          return response.data;
        }
      )
    );

  }

  public updateFAQ(params: any) {

    return this.http.put(`/FAQ/UpdateFAQById`, params).pipe(
      map(
        (response: ApiResponse) => {
          return response.data;
        }
      )
    );

  }
  public DeleteFAQ(id: any) {

    return this.http.post(`/FAQ/BackOffice/DeleteFAQ`, { Id: id }).pipe(
      map(
        (response: ApiResponse) => {
          console.log(response.data);
          return response;
        }
      )
    );
  }

  public DeleteComplain(id: any) {

    return this.http.post(`/CustomerComplainAndRecommend/Backoffice/DeleteCustomerComplainAndRecommendeById`, { Id: id }).pipe(
      map(
        (response: ApiResponse) => {
          console.log(response.data);
          return response;
        }
      )
    );
  }

  public getAllEmail(search: string, pageNumber: number, pageSize: number, column: string, active: number){
 
    return this.http.post(`/CustomerProblem/Backoffice/GetAllCustomerProblem`, {
      search: search,
      pageNumber: pageNumber,
      pageSize: pageSize,
      column:column,
      active:active
    }).pipe(
      map(
        (response: ApiPagedResponse) => {
          return response;
        }
      )
    );
  }
  public getEmailById(id: any,TopicId:any) {

    return this.http.post(`/CustomerProblem/GetCustomerProblemById`, { Id: id,problemTopicId:TopicId }).pipe(
      map(
        (response: ApiResponse) => {
          return response.data;
        }
      )
    );
  }

  public DeleteEmail(id: any, TopicId:any) {

    return this.http.post(`/CustomerProblem/DeleteCustomerProblem`, { Id: id,problemTopicId:TopicId }).pipe(
      map(
        (response: ApiResponse) => {
          return response;
        }
      )
    );
  }

  public getAllFAQ(search: string, pageNumber: number, pageSize: number, column: string, active: number){
 
    return this.http.post(`/FAQ/BackOffice/GetAllFAQ`, {
      search: search,
      pageNumber: pageNumber,
      pageSize: pageSize,
      column:column,
      active:active
    }).pipe(
      map(
        (response: ApiPagedResponse) => {
          return response;
        }
      )
    );
  }

  public getAllTermsConditions(search: string, pageNumber: number, pageSize: number, column: string, active: number,module: string){
 
    return this.http.post(`/TermAndCondition/Backoffice/GetAllTermAndCondition`,{
      search: search,
      pageNumber: pageNumber,
      pageSize: pageSize,
      column:column,
      active:active,
      module:module
    }).pipe(
      map(
        (response: ApiPagedResponse) => {
          return response;
        }
      )
    );
  }



  public getTermAndConditionById(id: string) {

    return this.http.post(`/TermAndCondition/Backoffice/GetTermAndConditionById`, { Id: id }).pipe(
      map(
        (response: ApiResponse) => {
          return response.data;
        }
      )
    );
  }

  public createTermAndCondition(params: any) {

    return this.http.post(`/TermAndCondition/Backoffice/CreateTermAndCondition`, params).pipe(
      map(
        (response: ApiResponse) => {
          return response.data;
        }
      )
    );

  }

  public updateTermAndCondition(params: any) {

    return this.http.put(`/TermAndCondition/Backoffice/UpdateTermAndConditionById`, params).pipe(
      map(
        (response: ApiResponse) => {
          return response.data;
        }
      )
    );

  }
  public setisPublic(id: any) {

    return this.http.put(`/TermAndCondition/Backoffice/UpdatePubilcTermAndCondition`, {Id:id}).pipe(
      map(
        (response: ApiResponse) => {
          return response.data;
        }
      )
    );

  }
  public getChatLog(search: string, today:Date, pageNumber: number, pageSize: number) {
    return this.http.post('api/messagelog/list', { 
      role: search,
      startdate: "2001-01-01",
      enddate:  today,
      page: pageNumber,
      pageSize: pageSize }, true);
 
  }




}