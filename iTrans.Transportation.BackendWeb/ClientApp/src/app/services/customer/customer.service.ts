

import { Country, CountryList} from '../../models/country.model';
import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiResponse, ApiPagedResponse } from '../../models/api-response';
import { map } from 'rxjs/operators';
import { HttpService } from '../http/http.service';

@Injectable({
  providedIn: 'root'
})
export class CustomerService {
  constructor(
    private http: HttpClient,
    private httpx: HttpService
  ) { }
  
  public getAllCustomerProfile(search: string, customertype:string, pageNumber: number, pageSize: number, column: string, active: number){
    return this.httpx.post('/Customer/Backoffice/GetAllCustomer', {
      search: search,
      customertype:customertype,
      pageNumber: pageNumber,
      pageSize: pageSize,
      column: column,
      active: active,
    });
  
  }

  public getCustomerProfileById(id: string) {
    return this.httpx.post('/Customer/Backoffice/GetCustomerById', { Id: id });
 
  }

  public DeleteCustomer(id: any) {
    return this.httpx.post('/Customer/DeleteCustomerById', { Id: id });
  
  }
  public getAllContactPersonTitle() {

    return this.http.post<ApiResponse>(`${environment.baseWebApiUrl}/Title/GetAllTitle`, {}).pipe(
      map(
        (response: ApiResponse) => {
          return response.data;
        }
      )
    );
  }
  public getAllUserLevel(id: string) {

    return this.httpx.post(`/UserLevel/GetAllCustomerLevel`, { Id: id }).pipe(
      map(
        (response: ApiResponse) => {
          return response.data;
        }
      )
    );
  }

  public getAllCountry() {

    return this.http.post<ApiResponse>(`${environment.baseWebApiUrl}/Country/GetAllCountry`, { }).pipe(
      map(
        (response: ApiResponse) => {
          return response.data;
        }
      )
    );
  }
  public getRegionByCountry(id: string) {

    return this.http.post<ApiResponse>(`${environment.baseWebApiUrl}/Region/GetRegionByCountry`, { countryId: id  }).pipe(
      map(
        (response: ApiResponse) => {
          return response.data;
        }
      )
    );
  }

  public getAllProvince(id: string) {

    return this.http.post<ApiResponse>(`${environment.baseWebApiUrl}/Province/GetProvinceByCountry`, { countryId: id }).pipe(
      map(
        (response: ApiResponse) => {
          return response.data;
        }
      )
    );
  }
  public getProvinceByRegion(id: string) {

    return this.http.post<ApiResponse>(`${environment.baseWebApiUrl}/Province/GetProvinceByRegion`, { regionId: id }).pipe(
      map(
        (response: ApiResponse) => {
          return response.data;
        }
      )
    );
  }

  public getAllDistrict(id: string) {

    return this.http.post<ApiResponse>(`${environment.baseWebApiUrl}/District/GetDistrictByProvince`, { provinceId: id }).pipe(
      map(
        (response: ApiResponse) => {
          return response.data;
        }
      )
    );
  }

  public getAllSubdistrict(id: string) {

    return this.http.post<ApiResponse>(`${environment.baseWebApiUrl}/Subdistrict/GetSubdistrictByDistrict`, { districtId: id }).pipe(
      map(
        (response: ApiResponse) => {
          return response.data;
        }
      )
    );
  }

  public updateCustomer(params: any) {
    return this.httpx.put('/Customer/Backoffice/UpdateCustomerById', params);
  }


  public updateCustomerAddress(params: any) {
    return this.http.put<ApiResponse>(`${environment.baseWebApiUrl}/CustomerAddress/UpdateCustomerAddressById`, params).pipe(
      map(
        (response: ApiResponse) => {
          return response.data;
        }
      )
    );
  }

  public getTermCondition(id: string){
    return this.httpx.post('/Customer/Backoffice/CheckTermAndCondition', { Id: id });
  }
  
  public getCustomerAddressByCutomerId(id: string) {
    return this.http.post<ApiResponse>(`${environment.baseWebApiUrl}/CustomerAddress/GetCustomerAddressByCustomer`, { CustomerId: id }).pipe(
      map(
        (response: ApiResponse) => {
          return response.data;
        }
      )
    );
  }

  public getCustomerTaxById(id){
    return this.httpx.post('/Customer/Backoffice/GetCustomerTaxById', { customerId: id });
  }
  
  public UpdateCustomerTaxById(location: any) {
    return this.httpx.put('/Customer/Backoffice/UpdateCustomerTaxById',location);
  }
  
  public GetCustomerByVerifyStatus(){
    return this.httpx.post('/Customer/Backoffice/GetCustomerByVerifyStatus', {});
  }
  
 
}
