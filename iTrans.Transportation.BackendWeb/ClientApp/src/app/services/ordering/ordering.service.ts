import { environment } from './../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiResponse, ApiPagedResponse } from '../../models/api-response';
import { map } from 'rxjs/operators';
import { ProductPackagingList, ProductTypeList, ProductType, ProductPackaging } from 'src/app/models/product.model';
import { OrderingCancelStatus, OrderingCancelStatusList } from 'src/app/models/ordering.model';
import { HttpService } from '../http/http.service';

@Injectable({
  providedIn: 'root'
})
export class OrderingService {
  public orderingcancelstatuslist: OrderingCancelStatusList | any;
  constructor(private http: HttpService) { }
  public getAllOrderingCancelStatus(search: string, pageNumber: number, pageSize: number, column: string, active: number): Observable<OrderingCancelStatusList> {

    return this.http.post(`/OrderingCancelStatus/Backoffice/GetAllOrderingCancelStatus`, {
      search: search,
      pageNumber: pageNumber,
      pageSize: pageSize,
      column: column,
      active: active
    }).pipe(
      map(
        (response: ApiPagedResponse) => {
          return response;
        }
      )
    );
  }
  public getOrderingCancelStatuses(): Observable<OrderingCancelStatus> {

    return this.http.post(`/OrderingCancelStatus/Backoffice/GetOrderingCancelStatuses`, {}).pipe(
      map(
        (response: ApiResponse) => {
          return response.data;
        }
      )
    );
  }
  public getOrderingCancelStatusById(id: string): Observable<OrderingCancelStatus> {

    return this.http.post(`/OrderingCancelStatus/GetOrderingCancelStatusById`, { Id: id }).pipe(
      map(
        (response: ApiResponse) => {
          return response.data;
        }
      )
    );
  }
  public deleteOrderingCancelStatus(id: string) {

    return this.http.post(`/OrderingCancelStatus/Backoffice/DeleteOrderingCancelStatus`, { Id: id }).pipe(
      map(
        (response: ApiResponse) => {
          return response;
        }
      )
    );
  }
  public createOrderingCancelStatus(params: any): Observable<OrderingCancelStatus> {

    return this.http.post(`/OrderingCancelStatus/CreateOrderingCancelStatus`, params).pipe(
      map(
        (response: ApiResponse) => {
          return response.data;
        }
      )
    );

  }
  public updateOrderingCancelStatus(params: any): Observable<OrderingCancelStatus> {

    return this.http.put(`/OrderingCancelStatus/UpdateOrderingCancelStatusById`, params).pipe(
      map(
        (response: ApiResponse) => {
          return response.data;
        }
      )
    );

  }


  public getAllOrderingPayment(search: string, pageNumber: number, pageSize: number, column: string, active: number) {
    return this.http.post('/Ordering/Backoffice/GetAllOrderingPayment', {
      search: search,
      pageNumber: pageNumber,
      pageSize: pageSize,
      column: column,
      active: active
    });
  }

  public getOrderingPaymentById(id: any) {
    return this.http.post('/Ordering/Backoffice/GetOrderingPaymentById', {
      Id: id,
    });
  }
  public DeleteOrderingPayment(id: any) {
    return this.http.post('/Ordering/Backoffice/DeleteOrderingPayment', {
      Id: id,
    });
  }
  public GetOrderingProvincesPopular() {
    return this.http.post('/Ordering/Backoffice/GetOrderingProvincesPopular', {

    });
  }
  public GetOrderingRegionsPopular() {
    return this.http.post('/Ordering/Backoffice/GetOrderingRegionsPopular', {

    });
  }
  public GetCountOrderingByType() {
    return this.http.post('/Ordering/Backoffice/GetCountOrderingByType', {

    });
  }
  public GetOrderingChart(type: string) {
    return this.http.post('/Ordering/Backoffice/GetOrderingChart', {
      selecttype: type
    });
  }


}
