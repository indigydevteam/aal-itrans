import { environment } from './../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiResponse, ApiPagedResponse } from '../../models/api-response';
import { map } from 'rxjs/operators';
import { ContainerSpecification, ContainerType, ContainerTypeList,ContainerSpecificationList } from 'src/app/models/container.model';
import { HttpService } from '../http/http.service';

@Injectable({
  providedIn: 'root'
})
export class ContainerService {
  public containertypes: ContainerTypeList | any;
  public containerspecifications: ContainerSpecification | any;

  


  constructor(private http: HttpService) { }

  public getAllContainerType(search: string, pageNumber: number, pageSize:number,column :string ,active:number) {
 
    return this.http.post(`/ContainerType/Backoffice/GetAllContainerType`, {
      search: search,
      pageNumber: pageNumber,
      pageSize: pageSize,
      column:column,
      active:active
    }).pipe(
      map(
        (response: ApiPagedResponse) => {
          return response;
        }
      )
    );
  }
  public getContainerTypeById(id: string): Observable<ContainerType> {

    return this.http.post(`/ContainerType/GetContainerTypeById`, { Id: id }).pipe(
      map(
        (response: ApiResponse) => {
          return response.data;
        }
      )
    );
  }
  public deleteContainerTypeById(id: string): Observable<ContainerType> {

    return this.http.post(`/ContainerType/Backoffice/DeleteContainerTypeById`, { Id: id }).pipe(
      map(
        (response: ApiResponse) => {
          return response.data;
        }
      )
    );
  }
  public createContainerType(params: any): Observable<ContainerType> {

    return this.http.post(`/ContainerType/CreateContainerType`, params).pipe(
      map(
        (response: ApiResponse) => {
          return response.data;
        }
      )
    );

  }
  public updateContainerType(params: any): Observable<ContainerType> {

    return this.http.put(`/ContainerType/UpdateContainerTypeById`, params).pipe(
      map(
        (response: ApiResponse) => {
          return response.data;
        }
      )
    );

  }

  public getAllContainerSpecification(search: string, pageNumber: number, pageSize:number,column :string ,active:number){
 
    return this.http.post(`/ContainerSpecification/Backoffice/GetAllContainerSpecification`, {
      search: search,
      pageNumber: pageNumber,
      pageSize: pageSize,
      column:column,
      active:active
    }).pipe(
      map(
        (response: ApiPagedResponse) => {
          return response;
        }
      )
    );
  }
  public getContainerSpecificationById(id: string): Observable<ContainerSpecification> {

    return this.http.post(`/ContainerSpecification/GetContainerSpecificationById`, { Id: id }).pipe(
      map(
        (response: ApiResponse) => {
          return response.data;
        }
      )
    );
  }
  public deleteContainerSpecificationId(id: string): Observable<ContainerSpecification> {

    return this.http.post(`/ContainerSpecification/Backoffice/DeleteContainerSpecificationId`, { Id: id }).pipe(
      map(
        (response: ApiResponse) => {
          return response.data;
        }
      )
    );
  }
  public createContainerSpecification(params: any): Observable<ContainerSpecification> {

    return this.http.post(`/ContainerSpecification/CreateContainerSpecification`, params).pipe(
      map(
        (response: ApiResponse) => {
          return response.data;
        }
      )
    );

  }
  public updateContainerSpecification(params: any): Observable<ContainerSpecification> {

    return this.http.put(`/ContainerSpecification/UpdateContainerSpecificationById`, params).pipe(
      map(
        (response: ApiResponse) => {
          return response.data;
        }
      )
    );

  }
}