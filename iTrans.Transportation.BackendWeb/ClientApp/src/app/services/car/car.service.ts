import { CarType, CarTypeList,CarLists,CarDescription, CarDescriptionList, CarSpecification, CarSpecificationList, CarFeatureList, CarFeature } from '../../models/car.model';
import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiResponse, ApiPagedResponse } from '../../models/api-response';
import { map } from 'rxjs/operators';
import { HttpService } from '../http/http.service';

@Injectable({
  providedIn: 'root'
})
export class CarService {
  

  constructor(
    private http: HttpService,
  ) { }

  public getAllCarType(search: string, pageNumber: number, pageSize: number, column: string, active: number): Observable<CarTypeList> {
    return this.http.post(`/CarType/Backoffice/GetAllCarType`, {
      search: search,
      pageNumber: pageNumber,
      pageSize: pageSize,
      column:column,
      active:active
    }).pipe(
      map(
        (response: ApiPagedResponse) => {
          return response;
        }
      )
    );
  }
   public getCarTypeById(id: string): Observable<CarType> {

    return this.http.post(`/CarType/Backoffice/GetCarTypeById`, { Id: id }).pipe(
      map(
        (response: ApiResponse) => {
          return response.data;
        }
      )
    );
  }
  public GetAllCarTypeQuery(): Observable<CarType> {

    return this.http.post(`/CarType/Backoffice/GetCarTypes`,{}).pipe(
      map(
        (response: ApiResponse) => {
          return response.data;
        }
      )
    );
  }
  public deleteTypeById(id: string) {
    return this.http.post(`/CarType/Backoffice/DeleteCarTypeById`, { Id: id }).pipe(
      map(
        (response: ApiResponse) => {
          return response;
        }
      )
    );
  }
  public createCarType(params: any): Observable<CarType> {
    return this.http.post(`/CarType/Backoffice/CreateCarType`, params).pipe(
      map(
        (response: ApiResponse) => {
          return response.data;
        }
      )
    );

  }
  public updateCarType(params: any): Observable<CarType> {
    return this.http.put(`/CarType/Backoffice/UpdateCarTypeById`, params).pipe(
      map(
        (response: ApiResponse) => {
          return response.data;
        }
      )
    );

  }

  public getAllCarList(search: string, pageNumber: number, pageSize: number, column: string, active: number) {
    return this.http.post(`/CarList/Backoffice/GetAllCarList`, {
      search: search,
      pageNumber: pageNumber,
      pageSize: pageSize,
      column:column,
      active:active
    }).pipe(
      map(
        (response: ApiPagedResponse) => {
          return response;
        }
      )
    );
  }

  public getCarListByIdInfomation(id: string): Observable<CarLists> {
    return this.http.post(`/CarList/GetCarListByIdInfomation`, { Id: id }).pipe(
      map(
        (response: ApiResponse) => {
          return response.data;
        }
      )
    );
  }
  public deleteCarListById(id: string) {
    return this.http.post(`/CarList/Backoffice/DeleteCarListById`, { Id: id }).pipe(
      map(
        (response: ApiResponse) => {
          return response;
        }
      )
    );
  }
  public getCarListById(id: string): Observable<CarLists> {

    return this.http.post(`/CarList/GetCarListById`, { Id: id }).pipe(
      map(
        (response: ApiResponse) => {
          return response.data;
        }
      )
    );
  }
  public GetCarListByType(id: string): Observable<CarLists> {
    return this.http.post(`/CarList/GetCarListByType`, { TypeId: id }).pipe(
      map(
        (response: ApiResponse) => {
          return response.data;
        }
      )
    );
  }
  public createCarList(params: any): Observable<CarLists> {
    return this.http.post(`/CarList/CreateCarList`, params).pipe(
      map(
        (response: ApiResponse) => {
          return response.data;
        }
      )
    );

  }
  public updateCarList(params: any): Observable<CarLists> {

    return this.http.put(`/CarList/UpdateCarListById`, params).pipe(
      map(
        (response: ApiResponse) => {
          return response.data;
        }
      )
    );

  }

  public updateDriverCarById(params: any) {
    return this.http.put('/DriverCar/Backoffice/UpdateDriverCarById', params);

  }
 
  public getAllCarDescription(search: string, pageNumber: number, pageSize: number, column: string, active: number): Observable<CarDescriptionList> {
    return this.http.post(`/CarDescription/BackOffice/GetAllCarDescription`, {
      search: search,
      pageNumber: pageNumber,
      pageSize: pageSize,
      column:column,
      active:active
    }).pipe(
      map(
        (response: ApiPagedResponse) => {
          return response;
        }
      )
    );
  }
  public DeleteCarDescription(id: string) {
    return this.http.post(`/CarDescription/BackOffice/DeleteCarDescriptionById`, {
      id: id
    }).pipe(
      map(
        (response: ApiPagedResponse) => {
          return response;
        }
      )
    );
  }

  public getCarDescriptionById(id: string): Observable<CarDescription> {

    return this.http.post(`/CarDescription/GetCarDescriptionById`, { Id: id }).pipe(
      map(
        (response: ApiResponse) => {
          return response.data;
        }
      )
    );
  }
  public GetCarDescriptionByCarList(id: string): Observable<CarDescription> {
    return this.http.post(`/CarDescription/GetCarDescriptionByCarList`, { CarListId: id }).pipe(
      map(
        (response: ApiResponse) => {
          return response.data;
        }
      )
    );
  }
  public createCarDescription(params: any): Observable<CarDescription> {
    return this.http.post(`/CarDescription/CreateCarDescription`, params).pipe(
      map(
        (response: ApiResponse) => {
          return response.data;
        }
      )
    );
  }
  public updateCarDescription(params: any): Observable<CarDescription> {
    return this.http.put(`/CarDescription/UpdateCarDescriptionById`, params).pipe(
      map(
        (response: ApiResponse) => {
          return response.data;
        }
      )
    );
  }

  public getAllCarSpecification(search: string, pageNumber: number, pageSize: number, column: string, active: number): Observable<CarSpecificationList> {
    return this.http.post(`/CarSpecification/Backoffice/GetAllCarSpecification`, {
      search: search,
      pageNumber: pageNumber,
      pageSize: pageSize,
      column:column,
      active:active
    }).pipe(
      map(
        (response: ApiPagedResponse) => {
          return response;
        }
      )
    );
  }

  public getCarSpecificationById(id: string): Observable<CarSpecification> {

    return this.http.post(`/CarSpecification/Backoffice/GetCarSpecificationById`, { Id: id }).pipe(
      map(
        (response: ApiResponse) => {
          return response.data;
        }
      )
    );
  }
  public DeleteCarSpecificationById(id: string) {

    return this.http.post(`/CarSpecification/Backoffice/DeleteCarSpecificationById`, { Id: id }).pipe(
      map(
        (response: ApiResponse) => {
          return response;
        }
      )
    );
  }
  public GetCarSpecificationByCarList(id: string): Observable<CarSpecification> {

    return this.http.post(`/CarSpecification/Backoffice/GetCarSpecificationByCarList`, { CarListId: id }).pipe(
      map(
        (response: ApiResponse) => {
          return response.data;
        }
      )
    );
  }

  public createCarSpecification(body): Promise<any> {
    const endpoint = `/CarSpecification/Backoffice/CreateCarSpecification`;
    return this.http.post(endpoint, body).toPromise();
    //return this.http.post(`/CarSpecification/Backoffice/CreateCarSpecification`, params).pipe(
    //  map(
    //    (response: ApiResponse) => {
    //      return response.data;
    //    }
    //  )
    //);

  }
  public updateCarSpecification(body): Promise<any> {
    const endpoint = `/CarSpecification/Backoffice/UpdateCarSpecificationById`;
    return this.http.post(endpoint, body).toPromise();
    //return this.http.put(`/CarSpecification/Backoffice/UpdateCarSpecificationById`, params).pipe(
    //  map(
    //    (response: ApiResponse) => {
    //      return response.data;
    //    }
    //  )
    //);

  }
  public getAllCarFeature(search: string, pageNumber: number, pageSize: number, column: string, active: number): Observable<CarFeatureList> {

    return this.http.post(`/CarFeature/Backoffice/GetAllCarFeature`, {
      search: search,
      pageNumber: pageNumber,
      pageSize: pageSize,
      column:column,
      active:active
    }).pipe(
      map(
        (response: ApiPagedResponse) => {
          return response;
        }
      )
    );
  }

  public getCarFeatureById(id: string): Observable<CarFeature> {

    return this.http.post(`/CarFeature/Backoffice/GetCarFeatureById`, { Id: id }).pipe(
      map(
        (response: ApiResponse) => {
          return response.data;
        }
      )
    );
  }
  public deleteCarFeatureById(id: string) {

    return this.http.post(`/CarFeature/Backoffice/DeleteCarFeatureById`, { Id: id }).pipe(
      map(
        (response: ApiResponse) => {
          return response;
        }
      )
    );
  }
  public createCarFeature(params: any): Observable<CarFeature> {

    return this.http.post(`/CarFeature/Backoffice/CreateCarFeature`, params).pipe(
      map(
        (response: ApiResponse) => {
          return response.data;
        }
      )
    );

  }
  public updateCarFeature(params: any): Observable<CarFeature> {

    return this.http.put(`/CarFeature/Backoffice/UpdateCarFeatureById`, params).pipe(
      map(
        (response: ApiResponse) => {
          return response.data;
        }
      )
    );

  }

}
