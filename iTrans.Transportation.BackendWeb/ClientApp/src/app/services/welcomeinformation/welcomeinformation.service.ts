import { environment } from '../../../environments/environment';
import { HttpService } from '../http/http.service';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiResponse, ApiPagedResponse } from '../../models/api-response';
import { map } from 'rxjs/operators';
import { Welcomeinformation, WelcomeinformationList } from '../../models/welcomeinformation';

@Injectable({
  providedIn: 'root'
})
export class WelcomeInformationService {
  public welcomeinformations: WelcomeinformationList | any;
  public welcomeinformation: Welcomeinformation| any;

  constructor(private http: HttpService) { }

  public getAllWelcomeInformation(search: string, pageNumber: number, pageSize: number, column: string, active: number): Observable<WelcomeinformationList> {
    return this.http.post(`/WelcomeInformation/Backoffice/GetAllWelcomeInformation`, {
      search: search,
      pageNumber: pageNumber,
      pageSize: pageSize,
      column: column,
      active: active
    }).pipe(
      map(
        (response: ApiPagedResponse) => {
          return response;
        }
      )
    );
  }

  public getWelcomeinformationById(id: string): Observable<Welcomeinformation> {

    return this.http.post(`/WelcomeInformation/Backoffice/GetWelcomeInformationById`, { Id: id }).pipe(
      map(
        (response: ApiResponse) => {
          return response.data;
        }
      )
    );
  }

  public createWelcomeinformation(body): Promise<any>{
    const endpoint = `/WelcomeInformation/Backoffice/CreateWelcomeInformation`;
    return this.http.post(endpoint, body).toPromise();
    //return this.http.post(`/WelcomeInformation/Backoffice/CreateWelcomeInformation`, params).pipe(
    //  map(
    //    (response: ApiResponse) => {
    //      return response.data;
    //    }
    //  )
    //);
  }

  public updateWelcomeinformation(body): Promise<any> {
    const endpoint = `/WelcomeInformation/Backoffice/UpdateWelcomeInformationById`;
    return this.http.put(endpoint, body).toPromise();
    //(params: any): Observable<Welcomeinformation> {
    //return this.http.put(`/WelcomeInformation/Backoffice/UpdateWelcomeInformationById`, params).pipe(
    //  map(
    //    (response: ApiResponse) => {
    //      return response.data;
    //    }
    //  )
    //);
  }
  public DeleteWelcomeinformationById(id: string) {

    return this.http.post(`/Welcomeinformation/Backoffice/DeleteWelcomeinformationById`, { Id: id }).pipe(
      map(
        (response: ApiResponse) => {
          return response;
        }
      )
    );
  }
}
