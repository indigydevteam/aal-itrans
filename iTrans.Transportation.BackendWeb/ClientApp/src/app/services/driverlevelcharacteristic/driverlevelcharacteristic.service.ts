
import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiResponse, ApiPagedResponse } from '../../models/api-response';
import { map } from 'rxjs/operators';
import { HttpService } from '../http/http.service';
import { DriverLevelCharacteristicList, DriverLevelCharacteristic } from 'src/app/models/DriverLevelCharacteristic.model';

@Injectable({
  providedIn: 'root'
})
export class  DriverLevelCharacteristicService {
  public driverlevelcharacteristiclist:  DriverLevelCharacteristicList | any;
  public driverlevelcharacteristic:  DriverLevelCharacteristic | any;
  constructor(
    private http: HttpService,
  ) { }
  
  public getAll(search: string, pageNumber: number, pageSize: number, column: string, active: number): Observable< DriverLevelCharacteristicList> {
    return this.http.post(`/DriverLevelCharacteristic/Backoffice/GetAll`, {
      search: search,
      pageNumber: pageNumber,
      pageSize: pageSize,
      column:column,
      active:active

    }).pipe(
      map(
        (response: ApiPagedResponse) => {
          this.driverlevelcharacteristiclist = response;
          return this.driverlevelcharacteristiclist;
        }
      )
    );
  }

  public getById(id: string): Observable< DriverLevelCharacteristic> {
    return this.http.post(`/DriverLevelCharacteristic/Backoffice/GetById`, { Id: id }).pipe(
      map(
        (response: ApiResponse) => {
          return response.data;
        }
      )
    );
  }
  public delete(id: string) {

    return this.http.post(`/ DriverLevelCharacteristic/Backoffice/Delete`, { Id: id }).pipe(
      map(
        (response: ApiResponse) => {
          return response;
        }
      )
    );
  }
  public create(params: any): Observable< DriverLevelCharacteristic> {

    return this.http.post(`/DriverLevelCharacteristic/Backoffice/Create`, params).pipe(
      map(
        (response: ApiResponse) => {
          return response.data;
        }
      )
    );

  }
  public update(params: any): Observable<DriverLevelCharacteristic> {

    return this.http.put(`/DriverLevelCharacteristic/Backoffice/Update`, params).pipe(
      map(
        (response: ApiResponse) => {
          return response.data;
        }
      )
    );

  }
}
