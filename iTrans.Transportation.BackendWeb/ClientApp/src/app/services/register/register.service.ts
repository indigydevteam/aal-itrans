import { environment } from './../../../environments/environment';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiResponse, ApiPagedResponse } from '../../models/api-response';
import { map } from 'rxjs/operators';
import { RegisterDocument, RegisterDocumentList, RegisterInformation, RegisterInformationList } from '../../models/register.model';
import { HttpService } from '../http/http.service';

@Injectable({
  providedIn: 'root'
})
export class RegisterService {
  public registerinformations: RegisterInformationList | any;
  public registerdocument: RegisterDocument | any;
  public registerinformation: RegisterInformation| any;

  constructor(private http: HttpService) { }

  public getAllRegisterInformation(search: string, pageNumber: number, pageSize: number, column: string, active: number): Observable<RegisterInformationList> {
 
    return this.http.post(`/RegisterInformation/Backoffice/GetAllRegisterInformation`, {
      search: search,
      pageNumber: pageNumber,
      pageSize: pageSize,
      column: column,
      active: active
    }).pipe(
      map(
        (response: ApiPagedResponse) => {
          return response;
        }
      )
    );
  }

  public getRegisterInformationById(id: string): Observable<RegisterInformation>{

    return this.http.post(`/RegisterInformation/Backoffice/GetRegisterInformationById`, { Id: id }).pipe(
      map(
        (response: ApiResponse) => {
          return response.data;
        }
      )
    );
  }
  public createRegisterInformation(body): Promise<any> {

    const endpoint = `/RegisterInformation/Backoffice/CreateRegisterInformation`;
    return this.http.post(endpoint, body).toPromise();

  }
  public updateRegisterInformation(body): Promise<any>{

    const endpoint = `/RegisterInformation/Backoffice/UpdateRegisterInformationById`;
    return this.http.put(endpoint, body).toPromise();

  }

  public getAllRegisterDocument(search: string, pageNumber: number, pageSize:number): Observable<RegisterDocumentList> {
 
    return this.http.post(`${environment.baseWebApiUrl}/RegisterDocuments/GetAllRegisterDocumentByConditionWithPage`, {
      search: search,
      pageNumber: pageNumber,
      pageSize: pageSize
    }).pipe(
      map(
        (response: ApiPagedResponse) => {
          this.registerdocument = response;
          return this.registerdocument;
        }
      )
    );
  }

  public getRegisterDocumentById(id: string): Observable<RegisterInformation> {

    return this.http.post(`${environment.baseWebApiUrl}/RegisterDocuments/GetRegisterDocumentById`, { Id: id }).pipe(
      map(
        (response: ApiResponse) => {
          console.log(response.data);
          return response.data;
        }
      )
    );
  }
  public creatRegisterDocument(params: any): Observable<RegisterInformation> {

    return this.http.post(`${environment.baseWebApiUrl}/RegisterDocuments/CreateRegisterDocument`, params).pipe(
      map(
        (response: ApiResponse) => {
          return response.data;
        }
      )
    );

  }
  public updateRegisterDocument(params: any): Observable<RegisterInformation> {

    return this.http.put(`${environment.baseWebApiUrl}/RegisterDocuments/UpdateRegisterDocumentById`, params).pipe(
      map(
        (response: ApiResponse) => {
          return response.data;
        }
      )
    );

  }
}
