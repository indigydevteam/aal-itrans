import { Country, CountryList} from '../../models/country.model';
import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiResponse, ApiPagedResponse } from '../../models/api-response';
import { map } from 'rxjs/operators';
import { HttpService } from '../http/http.service';

@Injectable({
  providedIn: 'root'
})
export class SecurityService {
  constructor(
    private http: HttpClient,
    private httpx: HttpService
    ) { }
  
  public getAllRole(search: string, pageNumber: number, pageSize: number){
 
    return this.http.post<ApiPagedResponse>(`${environment.baseWebApiUrl}/Role/GetAllRoleWithPage`, {
      search: search,
      pageNumber: pageNumber,
      pageSize: pageSize,
    }).pipe(
      map(
        (response: ApiPagedResponse) => {
          return response;
        }
      )
    );
  }
  public DeleteRole(id: number){
 
    return this.http.post<ApiPagedResponse>(`${environment.baseWebApiUrl}/Role/DeleteRole`, {Id:id}).pipe(
      map(
        (response: ApiPagedResponse) => {
          return response;
        }
      )
    );
  }
  public GetAllMenuPermission(id: any){
 
    return this.http.post<ApiPagedResponse>(`${environment.baseWebApiUrl}/Menu/BackOffice/GetAllMenuPermission`, {id:id}).pipe(
      map(
        (response: ApiPagedResponse) => {
          return response.data;
        }
      )
    );
  }

  public GetAllMenu(){
 
    return this.http.post<ApiPagedResponse>(`${environment.baseWebApiUrl}/Menu/GetAllMenu`, {}).pipe(
      map(
        (response: ApiPagedResponse) => {
          return response.data;
        }
      )
    );
  }
  public createRoleManu(params: any) {
    // return this.httpx.put('/Role/EditRole', params);

      return this.http.post<ApiResponse>(`${environment.baseWebApiUrl}/Role/CreateRole`, params).pipe(
        map(
          (response: ApiResponse) => {
            return response.data;
          }
        )
      );
  
    }
  public updateRoleManu(params: any) {
    // return this.httpx.put('/Role/EditRole', params);

      return this.http.post<ApiResponse>(`${environment.baseWebApiUrl}/Role/EditRole`, params).pipe(
        map(
          (response: ApiResponse) => {
            return response.data;
          }
        )
      );
  
    }

    public getAllRoles() {
      // return this.httpx.put('/Role/EditRole', params);
  
      return this.http.post<ApiPagedResponse>(`${environment.baseWebApiUrl}/Role/GetAllRole`, {}).pipe(
        map(
          (response: ApiPagedResponse) => {
            return response;
          }
        )
      );
    }
    public CheckPassword(password:string) {
      return this.httpx.post('/ForgotPassword/BackOffice/CheckPassword', {Password:password});
    }
    public SetPassword(params:any) {
      return this.httpx.post('/ForgotPassword/SetPassword', params);
    }

  }


