import { Injectable } from '@angular/core';
import { HttpService } from '../http/http.service';
import { environment } from '../../../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class ChatService {
  companyCode = 'itran'
  constructor(private http: HttpService) { }
  // File Note
  getFileNote(page, pageSize, roomId, fileType) {
    const endpoint = environment.serverSocket.baseURL + environment.serverSocket.apiURL + environment.serverSocket.fileNote.fileGet
      + `?RoomId=${roomId}&NoteType=${fileType}&CompanyCode=${this.companyCode}&page=${page}&pageSize=${pageSize}`;
    return this.http.get(endpoint).toPromise();
  }

  // File Note
  deleteFileNote(id, userid) {
    const body = {
      NoteId: id,
      UserId: userid,
    };
    const endpoint = environment.serverSocket.baseURL + environment.serverSocket.apiURL + environment.serverSocket.fileNote.fileDelete;
    return this.http.post(endpoint, JSON.stringify(body)).toPromise();
  }
  createFileNote(body) {
    const fd = new FormData();
    fd.append('RoomId', body.roomId);
    fd.append('Content', body.content);
    fd.append('NoteType', body.noteType);
    fd.append('File', body.file);
    fd.append('UserId', body.userId);
    fd.append('CompanyCode', this.companyCode);
    const endpoint = environment.serverSocket.baseURL + environment.serverSocket.apiURL + environment.serverSocket.fileNote.fileCreate;
    return this.http.post(endpoint, fd).toPromise();
  }
  editFileNote(body) {
    const fd = new FormData();
    fd.append('RoomId', body.roomId);
    fd.append('Content', body.content);
    fd.append('NoteType', body.noteType);
    fd.append('File', body.file);
    fd.append('UserId', body.userId);
    fd.append('CompanyCode', this.companyCode);
    fd.append('NoteId', body.noteId);
    const endpoint = environment.serverSocket.baseURL + environment.serverSocket.apiURL + environment.serverSocket.fileNote.fileEdit;
    return this.http.post(endpoint, fd).toPromise();
  }

  async sendChatWithFile(body: any) {
    const endpoint = `${environment.serverSocket.fileUpload}`;

    const fd = new FormData();
    let blob: any;
    if (body.file) {
      blob = this.dataURItoBlob(body.file);
      fd.append('file', blob, body.fileName);
    }

    fd.append('roomId', body.roomId);
    fd.append('userId', body.userId);
    fd.append('isPrivate', body.isPrivate);
    fd.append('companyCode', this.companyCode);
    fd.append('serviceCode', body.serviceCode);
    fd.append('type', body.type);
  
    return this.http.post(endpoint, fd, true).toPromise();
  }
  downloadImageByUrl(url: any) {
    const endpoint = environment.baseURL + this.companyCode + `/api/v1/file/urltostring?imageurl=${encodeURI(url)}&comcode=${this.companyCode}`;
    return this.http.get(endpoint);
  }
  public dataURItoBlob(dataURI: string) {
    // convert base64 to raw binary data held in a string
    // doesn't handle URLEncoded DataURIs
    const byteString = atob(dataURI.split(',')[1]);

    // write the bytes of the string to an ArrayBuffer
    const ab = new ArrayBuffer(byteString.length);
    const ia = new Uint8Array(ab);
    for (let i = 0; i < byteString.length; i++) {
      ia[i] = byteString.charCodeAt(i);
    }

    // write the ArrayBuffer to a blob, and you're done
    const bb = new Blob([ab]);
    return bb;
  }
}
