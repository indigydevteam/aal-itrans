import { environment } from './../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiResponse, ApiPagedResponse } from '../../models/api-response';
import { map } from 'rxjs/operators';
import { UserLevelList, UserLevel } from '../../models/user.model';
import { HttpService } from '../http/http.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  public userlevellist: UserLevelList | any;
  public userlevel: UserLevel| any;

  constructor(private http: HttpService, private httpx: HttpService) { }

  public getAllUserLevel(search: string, pageNumber: number, pageSize:number, column: string, active:number): Observable<UserLevelList> {
    return this.http.post(`/UserLevel/BackOffice/GetAllUserLevel`, {
      search: search,
      pageNumber: pageNumber,
      pageSize: pageSize,
      column:column,
      active:active
    }).pipe(
      map(
        (response: ApiPagedResponse) => {
          this.userlevellist = response;
          return this.userlevellist;
        }
      )
    );
  }
  public getUserLevels(search: string, pageNumber: number, pageSize: number, column: string, active: number): Observable<UserLevelList> {
      return this.http.post(`/UserLevel/Backoffice/GetAllUserLevel`, {
        search: search,
        pageNumber: pageNumber,
        pageSize: pageSize,
        column: column,
        active: active
      }).pipe(
        map(
          (response: ApiPagedResponse) => {
            this.userlevellist = response;
            return this.userlevellist;
          }
        )
      );
    }

  public getUserlevelById(id: string): Observable<UserLevel> {

    return this.http.post(`/UserLevel/Backoffice/GetUserLevelById`, { Id: id }).pipe(
      map(
        (response: ApiResponse) => {
          return response.data;
        }
      )
    );
  }
  public deleteUserLevelById(id: string) {

    return this.http.post(`/UserLevel/Backoffice/DeleteUserLevelById`, { Id: id }).pipe(
      map(
        (response: ApiResponse) => {
          return response;
        }
      )
    );
  }
  public createUserlevel(params: any) {
    console.log(params);
    return this.httpx.post('/UserLevel/Backoffice/CreateUserLevel', params);
    //return this.httpx.post(`/UserLevel/CreateUserLevel`, params).pipe(
    //  map(
    //    (response: ApiResponse) => {
    //      return response.data;
    //    }
    //  )
    //);

  }
  public updateUserlevel(params: any){
    return this.httpx.put('/UserLevel/UpdateUserLevelById', params);
    //return this.http.put(`/UserLevel/UpdateUserLevelById`, params).pipe(
    //  map(
    //    (response: ApiResponse) => {
    //      return response.data;
    //    }
    //  )
    //);

  }

  public GetUserProfile() {
    return this.http.post('/User/BackOffice/GetUserProfile', { });
  }
  
  public GetAllUser(search: string, pageNumber: number, pageSize:number, column: string, active:number) {
    return this.http.post('/User/BackOffice/GetAllUser', {
      search: search,
      pageNumber: pageNumber,
      pageSize: pageSize,
      column:column,
      active:active
     });
  }

  public GetUserById(id:string) {
    return this.http.post('/User/BackOffice/GetUserById', { Id:id});
  }

  public DeleteUserById(id:string) {
    return this.http.post('/User/BackOffice/DeleteUserById', { Id:id});
  }
  public CreateUser(params:any) {
    return this.http.put('/User/BackOffice/CreateUser', params);
  }

  public UpdateUserById(params:any) {
    return this.http.put('/User/BackOffice/UpdateUserById', params);
  }

}
