import { CarType, CarTypeList,CarLists,CarDescription, CarDescriptionList, CarSpecification, CarSpecificationList, CarFeatureList, CarFeature } from '../../models/car.model';
import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiResponse, ApiPagedResponse } from '../../models/api-response';
import { map } from 'rxjs/operators';
import { HttpService } from '../http/http.service';

@Injectable({
  providedIn: 'root'
})
export class AudittailService {



  constructor(private http: HttpService) { }

  public getAllApplicationLog(module:string, start:Date,end:Date,modifiedBy: string, pageNumber: number, pageSize: number, column: string, active: number) {

    return this.http.post(`/ApplicationLog/BackOffice/GetAllApplicationLog`, {
      modifiedBy: modifiedBy,
      pageNumber: pageNumber,
      pageSize: pageSize,
      startdate :start,
      enddate: end,
      module:module,
      column:column,
      active:active
    }).pipe(
      map(
        (response: ApiPagedResponse) => {
         
          return response;
        }
      )
    );
  }
  public getAllApplicationLogSelectModifiedby() {
    return this.http.post('/ApplicationLog/BackOffice/GetAllApplicationLogSelectModifiedby', {});
  }
  public getAllApplicationLogSelectModule() {
    return this.http.post('/ApplicationLog/BackOffice/GetAllApplicationLogSelectModule', {});
  }
  
  
}