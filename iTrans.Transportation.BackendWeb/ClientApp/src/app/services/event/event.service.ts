import { EventEmitter, Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class EventService {
  public onNotiChatclickEvent: EventEmitter<any> = new EventEmitter();
  public onCountChatBageEvent: EventEmitter<any> = new EventEmitter();
  public onChatEditGrroupModalEvent: EventEmitter<any> = new EventEmitter();
  public onChatSelectImageModalEvent: EventEmitter<any> = new EventEmitter();
  public onSelectFriendModalEvent: EventEmitter<any> = new EventEmitter();
  public onCreateGroupModalEvent: EventEmitter<any> = new EventEmitter();
  public onPersonalInfomationpModalEvent: EventEmitter<any> = new EventEmitter();
  public onImgCroperModalEvent: EventEmitter<any> = new EventEmitter();
  public onChatBadge: EventEmitter<any> = new EventEmitter();
  public onProfileGet: EventEmitter<any> = new EventEmitter();
  constructor() { }
}
