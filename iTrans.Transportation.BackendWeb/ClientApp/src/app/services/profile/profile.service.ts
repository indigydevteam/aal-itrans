import { Injectable } from '@angular/core';
import { EventService } from '../event/event.service';
import { HttpService } from '../http/http.service';
import { StorageService } from '../storage/storage.service';

@Injectable({
  providedIn: 'root'
})
export class ProfileService {

  constructor(
    private http: HttpService,
    private storage: StorageService,
    private event : EventService
  ) { }

  async GetProfile(): Promise<any> {
    const endpoint = `/User/BackOffice/GetUserProfile`;
    const profile = (await this.http.post(endpoint, {}).toPromise()).data;
    this.storage.setItem('profile', profile);
    this.event.onProfileGet.emit(profile);
    
  }



}
