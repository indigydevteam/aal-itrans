import { Subdistrict, SubdistrictList } from './../../models/subdistrict.model';
import { environment } from './../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiResponse, ApiPagedResponse } from '../../models/api-response';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SubdistrictService {
  public subdistrictlist: SubdistrictList | any;
  constructor(private http: HttpClient) { }

  public getAllSubdistrict(search: string, pageNumber: number, pageSize: number): Observable<SubdistrictList> {
 
    return this.http.post<ApiPagedResponse>(`${environment.baseWebApiUrl}/Subdistrict/Backoffice/GetAll`, {
      search: search,
      pageNumber: pageNumber,
      pageSize: pageSize
    }).pipe(
      map(
        (response: ApiPagedResponse) => {
          this.subdistrictlist = response;
          return this.subdistrictlist;
        }
      )
    );
  }

  public getSubdistrictById(id: string): Observable<Subdistrict> {

    return this.http.post<ApiResponse>(`${environment.baseWebApiUrl}/Subdistrict/Backoffice/GetById`, { Id: id }).pipe(
      map(
        (response: ApiResponse) => {
          console.log(response.data);
          return response.data;
        }
      )
    );
  }
  public createSubdistrict(params: any): Observable<Subdistrict> {

    return this.http.post<ApiResponse>(`${environment.baseWebApiUrl}/Subdistrict/Backoffice/Create`, params).pipe(
      map(
        (response: ApiResponse) => {
          return response.data;
        }
      )
    );

  }
  public updateSubdistrict(params: any): Observable<Subdistrict> {

    return this.http.put<ApiResponse>(`${environment.baseWebApiUrl}/Subdistrict/Backoffice/UpdateById`, params).pipe(
      map(
        (response: ApiResponse) => {
          return response.data;
        }
      )
    );

  }
}
