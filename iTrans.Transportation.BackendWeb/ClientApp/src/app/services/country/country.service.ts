import { Country, CountryList} from './../../models/country.model';
import { environment } from './../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiResponse, ApiPagedResponse } from '../../models/api-response';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CountryService {
  public countrylist: CountryList | any;
  constructor(private http: HttpClient) { }
  
  public getAllCountry(search: string, pageNumber: number, pageSize:number): Observable<CountryList> {
 
    return this.http.post<ApiPagedResponse>(`${environment.baseWebApiUrl}/Country/Backoffice/GetAll`, {
      search: search,
      pageNumber: pageNumber,
      pageSize: pageSize
    }).pipe(
      map(
        (response: ApiPagedResponse) => {
          this.countrylist = response;
          return this.countrylist;
        }
      )
    );
  }

  public getCountryById(id: string): Observable<Country> {

    return this.http.post<ApiResponse>(`${environment.baseWebApiUrl}/Country/Backoffice/GetById`, { Id: id }).pipe(
      map(
        (response: ApiResponse) => {
          console.log(response.data);
          return response.data;
        }
      )
    );
  }
  public createCountry(params: any): Observable<Country> {

    return this.http.post<ApiResponse>(`${environment.baseWebApiUrl}/Country/Backoffice/Create`, params).pipe(
      map(
        (response: ApiResponse) => {
          return response.data;
        }
      )
    );

  }
  public updateCountry(params: any): Observable<Country> {

    return this.http.put<ApiResponse>(`${environment.baseWebApiUrl}/Country/Backoffice/UpdateById`, params).pipe(
      map(
        (response: ApiResponse) => {
          return response.data;
        }
      )
    );

  }

  
}
