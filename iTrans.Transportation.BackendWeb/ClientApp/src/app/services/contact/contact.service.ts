import { CarType, CarTypeList,CarLists,CarDescription, CarDescriptionList, CarSpecification, CarSpecificationList, CarFeatureList, CarFeature } from '../../models/car.model';
import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiResponse, ApiPagedResponse } from '../../models/api-response';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ContactService {
  
  constructor(private http: HttpClient) { }

  public getAllContact() {

    return this.http.post<ApiPagedResponse>(`${environment.baseWebApiUrl}/Contact/GetContact`, {
    }).pipe(
      map(
        (response: ApiPagedResponse) => {
          return response;
        }
      )
    );
  }
}