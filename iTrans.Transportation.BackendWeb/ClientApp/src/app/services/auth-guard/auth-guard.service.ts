import { Injectable } from '@angular/core';
// import ส่วนที่จะใช้งาน guard เช่น CanActivate, CanActivateChild เป็นต้นมาใช้งาน
import {
  CanActivate, Router,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  CanActivateChild
} from '@angular/router';
import { AuthenService } from '../authen/authen.service';
// import service ที่เช็คสถานะการล็อกอินมาใช้งาน


@Injectable()
export class AuthGuardService {

  constructor(
    private router: Router,
    private auth: AuthenService
  ) { }


  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    // console.log('canActivate run');
    let url: string = state.url; // เก็บ url 

    return this.checkLogin(url);
  }


  canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    // console.log('canActivateChild run');
    return this.canActivate(route, state);
  }


  checkLogin(url: string): boolean {
    if (url !== '/auth/signin') {
      if (this.auth.isLoggedIn()) {
        return true;
      } else {
        this.router.navigateByUrl('auth/signin');
        return false;
      }
    } else {
      if (this.auth.isLoggedIn()) {
        this.router.navigateByUrl('news');
        return false;
      } else {
        return true;
      }
    }
  }
}