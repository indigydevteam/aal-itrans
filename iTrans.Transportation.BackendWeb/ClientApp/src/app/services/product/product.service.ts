import { environment } from './../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiResponse, ApiPagedResponse } from '../../models/api-response';
import { map } from 'rxjs/operators';
import { ProductPackagingList, ProductTypeList, ProductType, ProductPackaging } from '../../models/product.model';
import { HttpService } from '../http/http.service';

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  public producttypelist: ProductTypeList | any;
  public producttype: ProductType | any;
  public productpackaging: ProductPackagingList | any;

  constructor(private http: HttpService) { }

  public getAllProductType(search: string, pageNumber: number, pageSize:number, column: string, active:number): Observable<ProductTypeList> {
    return this.http.post(`/ProductType/Backoffice/GetAllProductType`, {
      search: search,
      pageNumber: pageNumber,
      pageSize: pageSize,
      column:column,
      active:active
    }).pipe(
      map(
        (response: ApiPagedResponse) => {
          this.producttypelist = response;
          return this.producttypelist;
        }
      )
    );
  }

  public getProductTypeById(id: string): Observable<ProductType> {

    return this.http.post(`/ProductType/Backoffice/GetProductTypeById`, { Id: id }).pipe(
      map(
        (response: ApiResponse) => {
          return response.data;
        }
      )
    );
  }
  
  public deleteProductTypeById(id: string){

    return this.http.post(`/ProductType/Backoffice/DeleteProductTypeById`, { Id: id }).pipe(
      map(
        (response: ApiResponse) => {
          return response;
        }
      )
    );
  }
  public createProductType(body): Promise<any> {
    const endpoint = `/ProductType/Backoffice/CreateProductType`;
    return this.http.post(endpoint, body).toPromise();
    //return this.http.post(`/ProductType/CreateProductType`, params).pipe(
    //  map(
    //    (response: ApiResponse) => {
    //      return response.data;
    //    }
    //  )
    //);

  }
  public updateProductType(body): Promise<any>  {
    const endpoint = `/ProductType/Backoffice/UpdateProductTypeById`;
    return this.http.post(endpoint, body).toPromise();
    //return this.http.put(`/ProductType/Backoffice/UpdateProductTypeById`, params).pipe(
    //  map(
    //    (response: ApiResponse) => {
    //      return response.data;
    //    }
    //  )
    //);
  }



  public getAllProductPackaging(search: string, pageNumber: number, pageSize:number, column: string, active:number): Observable<ProductPackagingList> {

    return this.http.post(`/ProductPackaging/Backoffice/GetAllProductPackaging`, {
      search: search,
      pageNumber: pageNumber,
      pageSize: pageSize,
      column: column,
      active: active
    }).pipe(
      map(
        (response: ApiPagedResponse) => {
          this.producttypelist = response;
          return this.producttypelist;
        }
      )
    );
  }
  
  public getProductPackagingById(id: string): Observable<ProductPackaging> {

    return this.http.post(`/ProductPackaging/Backoffice/GetProductPackagingById`, { Id: id }).pipe(
      map(
        (response: ApiResponse) => {
          return response.data;
        }
      )
    );
  }
  public deleteProductPackagingById(id: string) {

    return this.http.post(`/ProductPackaging/Backofiice/DeleteProductPackagingById`, { Id: id }).pipe(
      map(
        (response: ApiResponse) => {
          return response;
        }
      )
    );
  }
  public createProductPackaging(body): Promise<any>  {
    const endpoint = `/ProductPackaging/Backoffice/CreateProductPackaging`;
    return this.http.post(endpoint, body).toPromise();

    //return this.http.post(`/ProductPackaging/Backoffice/CreateProductPackaging`, params).pipe(
    //  map(
    //    (response: ApiResponse) => {
    //      return response.data;
    //    }
    //  )
    //);

  }
  public updateProductPackaging(body): Promise<any> {
    const endpoint = `/ProductPackaging/Backoffice/UpdateProductPackagingById`;
    return this.http.post(endpoint, body).toPromise();
    //return this.http.put(`/ProductPackaging/Backoffice/UpdateProductPackagingById`, params).pipe(
    //  map(
    //    (response: ApiResponse) => {
    //      return response.data;
    //    }
    //  )
    //);

  }

}
