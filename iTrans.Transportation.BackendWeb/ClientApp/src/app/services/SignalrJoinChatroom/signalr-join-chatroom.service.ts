import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { SignalRService } from '../SignalR/signal-r.service';
import { StorageService } from '../storage/storage.service';


@Injectable({
  providedIn: 'root'
})
export class SignalrJoinChatroomService {

  private profile: any;
  private companyCode: any;
  private myName: any;
  private data: any;

  constructor(
    private signalRServ: SignalRService,
    private storage: StorageService,
  ) {
  }

  public async initData(data: any) {
    await this.signalRServ.connectChatGroupAsync();
    this.data = data;
    this.companyCode = await this.storage.getItem(environment.baseURL+ '/companyCode');
    this.profile = await this.storage.getItem(environment.baseURL+ '/profile');
    this.profile = JSON.parse(this.profile);
    this.myName = this.data.groupId || this.profile.Id;
  }

  public async onJoinChatRoom(data: any): Promise<any> {
    return new Promise(async (resovle) => {
      this.data = data;
      this.companyCode = await this.storage.getItem(environment.baseURL+ '/companyCode');
      this.profile = await this.storage.getItem(environment.baseURL+ '/profile');
      this.profile = JSON.parse(this.profile);
      this.myName = this.data.groupId || this.profile.Id;

      const youCanJoin = await this.checkYouCanJoin();   
     // console.log('youCanJoin', youCanJoin);
      if(youCanJoin) {
       // console.log('youCanJoin Srat', this.myName, this.profile.Id, this.companyCode);
        try {
          this.signalRServ.hubConnectionChat
            .invoke('AcceptInvite', this.myName, this.profile.Id, this.companyCode)
            .then(async (res) => {
             // console.log('##AcceptInvite', res);
              resovle(true);
            })
            .catch(err => console.error(err));
        } catch (error) {
          resovle(false);
         // console.log('##Error', error);
        }
      } else {
        resovle(false);
      }
    });
  }

  public checkYouCanJoin(): Promise<any> {
    return new Promise((resovle) => {
      if (this.data.isPrivateGroup) {
        setTimeout(async () => {
          const isNotJoinG = await this.checkInvalidGroupName();
          if (isNotJoinG) {
            resovle(false);
          }

          let isJoin = await this.checkPermissions();

          if (!isJoin) {
            resovle(false);
          }

          resovle(true);
        }, 9);
      } else {
        resovle(true);
      }
    });
  }

  private checkPermissions(): Promise<any> {
    return new Promise((resolve, reject) => {
      try {
        this.signalRServ.hubConnectionChatGroup
          .invoke('GetGroupMember', this.myName, this.companyCode)
          .then((res) => {
            const profile = res.find((element: any) => {
              return element.userId === this.profile.Id;
            });

            resolve(profile ? true : false);
          })
          .catch(err => console.error(err));
      } catch (error) {
       // console.log('##JoinGroup', error);
        resolve(false);
      }

    });
  }

  private checkInvalidGroupName() {
    return this.signalRServ.hubConnectionChatGroup
      .invoke('IsValidGroupName', this.data.groupName);
  }

}
