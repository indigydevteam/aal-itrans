import { TestBed } from '@angular/core/testing';

import { SignalrJoinChatroomService } from './signalr-join-chatroom.service';

describe('SignalrJoinChatroomService', () => {
  let service: SignalrJoinChatroomService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SignalrJoinChatroomService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
