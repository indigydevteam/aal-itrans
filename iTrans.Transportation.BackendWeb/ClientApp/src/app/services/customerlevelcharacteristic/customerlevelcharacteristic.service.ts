
import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiResponse, ApiPagedResponse } from '../../models/api-response';
import { map } from 'rxjs/operators';
import { HttpService } from '../http/http.service';
import { CustomerLevelCharacteristicList, CustomerLevelCharacteristic } from 'src/app/models/CustomerLevelCharacteristic.model';

@Injectable({
  providedIn: 'root'
})
export class CustomerLevelCharacteristicService {
  public customerlevelcharacteristiclist: CustomerLevelCharacteristicList | any;
  public customerlevelcharacteristic: CustomerLevelCharacteristic | any;
  constructor(
    private http: HttpService,
  ) { }
  
  public getAll(search: string, pageNumber: number, pageSize: number, column: string, active: number): Observable<CustomerLevelCharacteristicList> {
    return this.http.post(`/CustomerLevelCharacteristic/Backoffice/GetAll`, {
      search: search,
      pageNumber: pageNumber,
      pageSize: pageSize,
      column:column,
      active:active
    }).pipe(
      map(
        (response: ApiPagedResponse) => {
          this.customerlevelcharacteristiclist = response;
          return this.customerlevelcharacteristiclist;
        }
      )
    );
  }

  public getById(id: string): Observable<CustomerLevelCharacteristic> {
    return this.http.post(`/CustomerLevelCharacteristic/Backoffice/GetById`, { Id: id }).pipe(
      map(
        (response: ApiResponse) => {
          return response.data;
        }
      )
    );
  }
  public delete(id: string) {

    return this.http.post(`/CustomerLevelCharacteristic/Backoffice/Delete`, { Id: id }).pipe(
      map(
        (response: ApiResponse) => {
          return response;
        }
      )
    );
  }
  public create(params: any): Observable<CustomerLevelCharacteristic> {

    return this.http.post(`/CustomerLevelCharacteristic/Backoffice/Create`, params).pipe(
      map(
        (response: ApiResponse) => {
          return response.data;
        }
      )
    );

  }
  public update(params: any): Observable<CustomerLevelCharacteristic> {

    return this.http.put(`/CustomerLevelCharacteristic/Backoffice/Update`, params).pipe(
      map(
        (response: ApiResponse) => {
          return response.data;
        }
      )
    );

  }
}
