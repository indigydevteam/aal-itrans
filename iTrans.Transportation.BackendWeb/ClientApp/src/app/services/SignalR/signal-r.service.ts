import { Injectable } from '@angular/core';
import { HubConnection, HubConnectionBuilder, HttpTransportType, HubConnectionState } from '@microsoft/signalr'
// import { config } from 'src/app/app.config';
// import { StorageService } from '../utility/utility.service';
import { environment } from '../../../environments/environment';
import * as CryptoJS from 'crypto-js';
import { StorageService } from '../storage/storage.service';

@Injectable({
  providedIn: 'root'
})
export class SignalRService {

  public hubConnectionChat: HubConnection;
  public hubConnectionChatBadge: HubConnection;
  public hubConnectionChatLastMessage: HubConnection;
  public hubConnectionChatGroup: HubConnection;

  profile: any;
  private keys = {
    chat: '$84ue@65%434eu1='
  };
  private keySize = 128 / 8;

  constructor(
    private storage: StorageService
  ) {

  }

  public getConnecttionState() {
    return HubConnectionState;
  }

  public async connectChatAsync() {
    this.profile = await this.storage.getItem('token');
    const accessToken = this.profile.access_token;
    const url = `${environment.serverSocket.baseURL}chathub?userId=${this.profile.Id}&companyCode=${this.profile.CompanyCode}`;

    this.hubConnectionChat = new HubConnectionBuilder()
      .withUrl(url,
        {
          transport: HttpTransportType.WebSockets,
          skipNegotiation: true,
          accessTokenFactory: () => accessToken
        })
      .withAutomaticReconnect([0, 1000, 2000, 3000, 4000, 5000, 6000, 7000, 8000, 10000, 12000])
      .build();
    this.hubConnectionChat.keepAliveIntervalInMilliseconds = 99999999;

    return this.hubConnectionChat.start();
  }

  public async connectChatBadgeAsync() {
    this.profile = await this.storage.getItem('token');
    const accessToken = this.profile.access_token;
    const url = `${environment.serverSocket.baseURL}badgeHub?userId=${this.profile.Id}&companyCode=${this.profile.CompanyCode}`;
    this.hubConnectionChatBadge = new HubConnectionBuilder()
      .withUrl(url,
        {
          transport: HttpTransportType.WebSockets,
          skipNegotiation: true,
          accessTokenFactory: () => accessToken
        })
      .withAutomaticReconnect([0, 1000, 2000, 3000, 4000, 5000, 6000, 7000, 8000, 10000, 12000])
      .build();
    this.hubConnectionChatBadge.keepAliveIntervalInMilliseconds = 99999999;
    return this.hubConnectionChatBadge.start();
  }

  public async connectChatListAsync() {
    this.profile = await this.storage.getItem('token');
    const accessToken = this.profile.access_token;
    const url = `${environment.serverSocket.baseURL}lastMessageHub?userId=${this.profile.Id}&companyCode=${this.profile.CompanyCode}`;
    this.hubConnectionChatLastMessage = new HubConnectionBuilder()
      .withUrl(url,
        {
          transport: HttpTransportType.WebSockets,
          skipNegotiation: true,
          accessTokenFactory: () => accessToken
        })
      .withAutomaticReconnect([0, 1000, 2000, 3000, 4000, 5000, 6000, 7000, 8000, 10000, 12000])
      .build();
    this.hubConnectionChatLastMessage.keepAliveIntervalInMilliseconds = 99999999;

    return this.hubConnectionChatLastMessage.start();
  }

  public async connectChatGroupAsync() {
    this.profile = await this.storage.getItem('token');
    const accessToken = this.profile.access_token;
    const url = `${environment.serverSocket.baseURL}groupHub?userId=${this.profile.Id}&companyCode=${this.profile.CompanyCode}`;
    this.hubConnectionChatGroup = new HubConnectionBuilder()
      .withUrl(url,
        {
          transport: HttpTransportType.WebSockets,
          skipNegotiation: true,
          accessTokenFactory: () => accessToken
        })
      .withAutomaticReconnect([0, 1000, 2000, 3000, 4000, 5000, 6000, 7000, 8000, 10000, 12000])
      .build();

    this.hubConnectionChatGroup.keepAliveIntervalInMilliseconds = 99999999;

    return this.hubConnectionChatGroup.start();
  }

  public disconnectChat() {
    if (this.hubConnectionChat) {
      // console.log('##Disconnect chat##');
      this.hubConnectionChat.stop();
      this.hubConnectionChat = null;
    }
  }

  public disconnectChatBadge() {
    if (this.hubConnectionChatBadge) {
      // console.log('##Disconnect chat badge##');
      this.hubConnectionChatBadge.stop();
      this.hubConnectionChatBadge = null;
    }
  }

  public disconnectChatList() {
    if (this.hubConnectionChatLastMessage) {
      // console.log('##Disconnect chat list##');
      this.hubConnectionChatLastMessage.stop();
      this.hubConnectionChatLastMessage = null;
    }
  }

  public disconnectChatGroup() {
    if (this.hubConnectionChatGroup) {
      // console.log('##Disconnect chat group##');
      this.hubConnectionChatGroup.stop();
      this.hubConnectionChatGroup = null;
    }
  }

  /**
  * @param {any} data
  */
  public encrypt(data: any, objectKey: string) {
    try {

      const salt = this.keys[objectKey];
      const key = CryptoJS.enc.Utf8.parse(salt);
      const iv = CryptoJS.enc.Utf8.parse(salt);

      const encryptData = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse(data), key,
        { keySize: this.keySize, iv: iv, mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Pkcs7 }
      );

      return encryptData ? encryptData.toString() : data;

    } catch (error) {
      return data;
    }
  }

  
  /**
  * @param {any} data
  */
  public decrypt(data: any, objectKey: string) {
    try {
      const salt = this.keys[objectKey];
      const key = CryptoJS.enc.Utf8.parse(salt);
      const iv = CryptoJS.enc.Utf8.parse(salt);
      const decryptData = (CryptoJS.AES.decrypt(data, key,
        { keySize: this.keySize, iv: iv, mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Pkcs7 })).toString(CryptoJS.enc.Utf8);
      return decryptData || data;
    } catch (error) {
      return data;
    }
  }
}
