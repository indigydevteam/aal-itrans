import { Injectable } from '@angular/core';

import Swal from 'sweetalert2'

@Injectable({
  providedIn: 'root'
})
export class DialogService {

  constructor() { }

  async confirm(title: string, message: string, status = true): Promise<boolean> {
    return new Promise((resolve) => {
      Swal.fire({
        title: title,
        text: message,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#203b4b',
        cancelButtonColor: '#999999',
      }).then(function (result) {
        if (result.value) {
          resolve(true);
        } else {
          resolve(false);
        }
      });
    });
  }

  async error(title: string ): Promise<boolean> {
    return new Promise((resolve) => {
      Swal.fire({
        icon: 'error',
        title: title,
        confirmButtonColor: '#203b4b',
      })
    });
  }
}
