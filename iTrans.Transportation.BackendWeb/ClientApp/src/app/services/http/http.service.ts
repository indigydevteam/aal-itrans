import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
import { StorageService } from '../storage/storage.service';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  constructor(
    private http: HttpClient,
    private storage: StorageService
  ) { }

  post(url: string, body: any, ischat = false): Observable<any> {
    return Observable.create((observer: any) => {
      // Promise.all([
      //   this.verifyToken(),
      // ]);
      let endpoint = '';
      if(ischat){
        endpoint = environment.serverSocket.baseURL + url;
      }else{
        endpoint = environment.baseWebApiUrl + url;
      }
      return this.http.post(endpoint, body, { headers: this.authorizationHeader() }).subscribe((data: any) => {

        if (data.Code === 403) {

        }
        observer.next(data);
        observer.complete();
      }, (error) => {
        observer.error(error);
      });
    });

  }

  put(url: string, body: any, ischat = false): Observable<any> {
    return Observable.create((observer: any) => {
      // Promise.all([
      //   this.verifyToken(),
      // ]);
      let endpoint = '';
      if(ischat){
        endpoint = environment.serverSocket.baseURL + url;
      }else{
        endpoint = environment.baseWebApiUrl + url;
      }
      return this.http.put(endpoint, body, { headers: this.authorizationHeader() }).subscribe((data: any) => {

        if (data.Code === 403) {

        }
        observer.next(data);
        observer.complete();
      }, (error) => {
        observer.error(error);
      });
    });

  }

  get(url: string): Observable<any> {
    return Observable.create((observer: any) => {
      Promise.all([
        // this.verifyToken(),
      ]);
      return this.http.get(environment.baseWebApiUrl + url, { headers: this.authorizationHeader() }).subscribe((data: any) => {
        if (data.Code === 403) {

        }
        observer.next(data);
        observer.complete();
      }, (error) => {

        observer.error(error);
      });

    });
  }

  public authorizationHeader() {
    var token = this.storage.getItem('token');
    const tokenType = token.token_type;
    const accessToken = token.access_token;
    return new HttpHeaders({ 'Authorization': `${tokenType} ${accessToken}` });
  }
}
