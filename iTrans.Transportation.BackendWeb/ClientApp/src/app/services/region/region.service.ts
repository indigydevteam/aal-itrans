import { Region, RegionList } from './../../models/region.model';
import { environment } from './../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiResponse, ApiPagedResponse } from '../../models/api-response';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class RegionService {
  
  public regionlist: RegionList | any;
  constructor(private http: HttpClient) { }

  public getAllRegion(search: string, pageNumber: number, pageSize: number): Observable<RegionList> {
    return this.http.post<ApiPagedResponse>(`${environment.baseWebApiUrl}/Region/Backoffice/GetAll`, {
      search: search,
      pageNumber: pageNumber,
      pageSize: pageSize
    }).pipe(
      map(
        (response: ApiPagedResponse) => {
          this.regionlist = response;
          return this.regionlist;
        }
      )
    );
  }

  public getRegionById(id: string): Observable<Region> {
    return this.http.post<ApiResponse>(`${environment.baseWebApiUrl}/Region/Backoffice/GetById`, { Id: id }).pipe(
      map(
        (response: ApiResponse) => {
          return response.data;
        }
      )
    );
  }

  public GetRegionByCountry(id: string): Observable<Region> {
    return this.http.post<ApiResponse>(`${environment.baseWebApiUrl}/Region/GetRegionByCountry`, { CountryId: id }).pipe(
      map(
        (response: ApiResponse) => {
          return response.data;
        }
      )
    );
  }

  public createRegion(params: any): Observable<Region> {
    return this.http.post<ApiResponse>(`${environment.baseWebApiUrl}/Region/Backoffice/Create`, params).pipe(
      map(
        (response: ApiResponse) => {
          return response.data;
        }
      )
    );

  }
  public updateRegion(params: any): Observable<Region> {
    return this.http.put<ApiResponse>(`${environment.baseWebApiUrl}/Region/Backoffice/UpdateById`, params).pipe(
      map(
        (response: ApiResponse) => {
          return response.data;
        }
      )
    );

  }
}
