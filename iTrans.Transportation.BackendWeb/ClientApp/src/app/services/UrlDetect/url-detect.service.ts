import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UrlDetectService {

  constructor() { }
  detectToDownload(text: string) {
    let urlRegex = /(https?:\/\/[^\s]+)/g;
    return text.replace(urlRegex, (url) => {
      if (this.extractHostname(url) === 'itunes.apple.com' || this.extractHostname(url) === 'apps.apple.com') {
        return ` <a name="${url}" href="javascript:void(0)"><img id="link" name="${url}" style="transform: translateY(10px);"  width="120" src="./assets/imgs/icon-ios-download.png"></a>`;
      } else if (this.extractHostname(url) === 'play.google.com') {
        return ` <a name="${url}" href="javascript:void(0)"><img id="link" name="${url}" style="transform: translateY(10px);"  width="120" src="./assets/imgs/icon-android-download.png"></a>`;
      } else {
        return this.detectToURL(url);
      }
    });
  }
  detectToURL(text: string) {
    return text.replace(/(<a href=")?((https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)))(">(.*)<\/a>)?/gi, (link: string) => {
      return `<a id="link" name="${link}" href="javascript:void(0)">${link}</a>`;
    });
    
  }
  
  detectToUrlNewTab(text: string) {
    return text.replace(/(<a href=")?((https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)))(">(.*)<\/a>)?/gi, (link: string) => {
      return `<a id="link" name="${link}" href="${link}" target="_bank" class="custom-url">${link}</a>`;
    });
  }

  extractHostname(url: string) {
    let hostname: string;
    //find & remove protocol (http, ftp, etc.) and get hostname
    if (url.indexOf("//") > -1) {
      hostname = url.split('/')[2];
    } else {
      hostname = url.split('/')[0];
    }
    //find & remove port number
    hostname = hostname.split(':')[0];
    //find & remove "?"
    hostname = hostname.split('?')[0];

    return hostname;
  }

  detectURLYoutube(text: string) {
    let links = [];

    if(!text) return;

    text.replace(/(<a href=")?((https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)))(">(.*)<\/a>)?/gi, (link: string) => {  
      var match = link.match(/^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/);
      
      //For Yotobe only
      if(match&&match[7].length==11){
        links.push('https://www.youtube.com/embed/'+match[7]);
      }

      return '';
    });
    return links;
  }
  getFileNameFormURL(fullPath: any) {
    // console.log(fullPath);
    var filename = fullPath.replace(/^.*[\\\/]/, '');
    // console.log(filename);
    return filename;
  }
  
  getBase64(file: any): Promise<any> {
    return new Promise((resovle) => {
      let me = this;
      let reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = function () {
        //me.modelvalue = reader.result;
        resovle(reader.result);
        // console.log(reader.result);
      };
      reader.onerror = function (error) {
        resovle(null);
        // console.log('Error: ', error);
      };
    });
  }
}
