import { TestBed } from '@angular/core/testing';

import { UrlDetectService } from './url-detect.service';

describe('UrlDetectService', () => {
  let service: UrlDetectService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(UrlDetectService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
