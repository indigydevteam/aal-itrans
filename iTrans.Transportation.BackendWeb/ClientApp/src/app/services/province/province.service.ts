import { Province, ProvinceList } from './../../models/province.model';
import { environment } from './../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiResponse, ApiPagedResponse } from '../../models/api-response';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ProvinceService {
  public provincelist: ProvinceList | any;
  constructor(private http: HttpClient) { }

  public getAllProvince(search: string, pageNumber: number, pageSize: number): Observable<ProvinceList> {
 
    return this.http.post<ApiPagedResponse>(`${environment.baseWebApiUrl}/Province/Backoffice/GetAll`, {
      search: search,
      pageNumber: pageNumber,
      pageSize: pageSize
    }).pipe(
      map(
        (response: ApiPagedResponse) => {
          this.provincelist = response;
          return this.provincelist;
        }
      )
    );
  }

  public getProvinceById(id: string): Observable<Province> {

    return this.http.post<ApiResponse>(`${environment.baseWebApiUrl}/Province/Backoffice/GetById`, { Id: id }).pipe(
      map(
        (response: ApiResponse) => {
          console.log(response.data);
          return response.data;
        }
      )
    );
  }
  public createProvince(params: any): Observable<Province> {

    return this.http.post<ApiResponse>(`${environment.baseWebApiUrl}/Province/Backoffice/Create`, params).pipe(
      map(
        (response: ApiResponse) => {
          return response.data;
        }
      )
    );

  }
  public updateProvince(params: any): Observable<Province> {

    return this.http.put<ApiResponse>(`${environment.baseWebApiUrl}/Province/Backoffice/UpdateById`, params).pipe(
      map(
        (response: ApiResponse) => {
          return response.data;
        }
      )
    );

  }
  public GetProvinceByCountry(id: string) {

    return this.http.post<ApiResponse>(`${environment.baseWebApiUrl}/Province/GetProvinceByCountry`, { countryId: id }).pipe(
      map(
        (response: ApiResponse) => {
          // console.log(response.data);
          return response.data;
        }
      )
    );
  }
}
