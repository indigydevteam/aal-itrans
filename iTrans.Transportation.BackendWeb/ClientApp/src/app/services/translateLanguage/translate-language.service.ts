import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { defaultLanguage } from 'src/app/app.default.language';

@Injectable({
  providedIn: 'root'
})
export class TranslateLanguageService {

  constructor(public translateService: TranslateService) { }
  // getTranslateText(key: any, config: Config): Promise<any> {
  //   return new Promise(async (resovle) => {
  //     let msg = '';
  //     if (Array.isArray(key)) {
  //       for (let i = 0; i < key.length; i++) {
  //         msg += (await this.getText(key[i])) + ' ';
  //       }
  //     } else {
  //       msg = await this.getText(key);
  //     }
  //     if (msg == key) {
  //       msg = defaultLanguage[msg];
  //     }

  //     config?.replaces.forEach(n => {
  //       msg = msg.replace(n.k, n.v);
  //     });

  //     resovle(msg);
  //   });
  // }
  getText(key?: any, config?: ReplaceValue): Promise<any> {
    return new Promise(async (resovle) => {
      let msg = await this.translateService.get(key).toPromise();
      if (msg == key) {
        msg = defaultLanguage[msg];
      }
      config?.replaces.forEach(n => {
        msg = msg.replace(n.k, n.v);
      });

      resovle(msg);
    });
  }


}
export interface ReplaceValue {
  replaces: Array<{ k, v }>;
}