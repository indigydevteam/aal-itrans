import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import * as CryptoJS from 'crypto-js';

type keyName = 'order';

// Example
// 'line_profile' | 'header' | 'line_token' | 'profile' | 'token' | 'nav_hiden';

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  secureKey = environment.baseWebApiUrl;
  storageName = `${this.secureKey}_`;
  constructor() {
  }

  // tslint:disable-next-line:typedef
  public getItem(key: any) {
    const local = window.localStorage.getItem(this.storageName + key);
    const data = local ? this.decrypt(local) : null;
    if (data) {
      return JSON.parse(data);
    } else {
      return null;
    }
  }

  // tslint:disable-next-line:typedef
  setItem(key: any, value: any) {
    window.localStorage.setItem(this.storageName + key, this.encrypt(JSON.stringify(value)));
  }

  // tslint:disable-next-line:typedef
  removeItem(key: any) {
    window.localStorage.removeItem(this.storageName + key);
  }

  // tslint:disable-next-line:typedef
  clear() {
    window.localStorage.clear();
  }

  // tslint:disable-next-line:typedef
  encrypt(value: any) {
    return CryptoJS.AES.encrypt(value, this.secureKey).toString();
  }

  // tslint:disable-next-line:typedef
  decrypt(value: any) {
    return value ? CryptoJS.AES.decrypt(value, this.secureKey).toString(CryptoJS.enc.Utf8) : null;
  }
}
