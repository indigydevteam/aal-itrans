import { District, DistrictList } from './../../models/district.model';
import { environment } from './../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiResponse, ApiPagedResponse } from '../../models/api-response';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DistrictService {
  public districtlist: DistrictList | any;
  constructor(private http: HttpClient) { }

  public getAllDistrict(search: string, pageNumber: number, pageSize: number): Observable<DistrictList> {
 
    return this.http.post<ApiPagedResponse>(`${environment.baseWebApiUrl}/District/Backoffice/GetAll`, {
      search: search,
      pageNumber: pageNumber,
      pageSize: pageSize
    }).pipe(
      map(
        (response: ApiPagedResponse) => {
          this.districtlist = response;
          return this.districtlist;
        }
      )
    );
  }

  public getDistrictById(id: string): Observable<District> {

    return this.http.post<ApiResponse>(`${environment.baseWebApiUrl}/District/Backoffice/GetById`, { Id: id }).pipe(
      map(
        (response: ApiResponse) => {
          console.log(response.data);
          return response.data;
        }
      )
    );
  }
  public createDistrict(params: any): Observable<District> {

    return this.http.post<ApiResponse>(`${environment.baseWebApiUrl}/District/Backoffice/Create`, params).pipe(
      map(
        (response: ApiResponse) => {
          return response.data;
        }
      )
    );

  }
  public updateDistrict(params: any): Observable<District> {

    return this.http.put<ApiResponse>(`${environment.baseWebApiUrl}/District/Backoffice/UpdateById`, params).pipe(
      map(
        (response: ApiResponse) => {
          return response.data;
        }
      )
    );

  }

  public GetDistrictByProvince(id: string) {

    return this.http.post<ApiResponse>(`${environment.baseWebApiUrl}/District/GetDistrictByProvince`, { provinceId: id }).pipe(
      map(
        (response: ApiResponse) => {
          // console.log(response.data);
          return response.data;
        }
      )
    );
  }
}
