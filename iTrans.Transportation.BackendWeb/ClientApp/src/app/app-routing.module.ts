//import { NgModule } from '@angular/core';
//import { Routes, RouterModule } from '@angular/router';
//import { CountryListComponent } from './components/country/country-list/country-list.component';
//import { ManageCountryComponent } from './components/country/manage-country/manage-country.component';
//import { RegionListComponent } from './components/region/region-list/region-list.component';
//import { ManageRegionComponent } from './components/region/manage-region/manage-region.component';
//import { CartypeListComponent } from './components/car/cartype-list/cartype-list.component';
//import { CarlistListComponent } from './components/car/carlist-list/carlist-list.component';
//import { ManageCartypeComponent } from './components/car/manage-cartype/manage-cartype.component';
//import { ManageCarlistComponent } from './components/car/manage-carlist/manage-carlist.component';
//import { CardescriptionListComponent } from './components/car/cardescription-list/cardescription-list.component';
//import { ManageCardescriptionComponent } from './components/car/manage-cardescription/manage-cardescription.component';
//import { CarspecificationListComponent } from './components/car/carspecification-list/carspecification-list.component';
//import { ManageCarspecificationComponent } from './components/car/manage-carspecification/manage-carspecification.component';
//import { ManageCarfeatureComponent } from './components/car/manage-carfeature/manage-carfeature.component';
//import { CarfeatureListComponent } from './components/car/carfeature-list/carfeature-list.component';
//import { ProducttypeListComponent } from './components/product/producttype-list/producttype-list.component';
//import { ProductpackagingListComponent } from './components/product/productpackaging-list/productpackaging-list.component';
//import { ManageProducttypeComponent } from './components/product/manage-producttype/manage-producttype.component';
//import { ManageProductpackagingComponent } from './components/product/manage-productpackaging/manage-productpackaging.component';
//import { UserlevelListComponent } from './components/user/userlevel-list/userlevel-list.component';
//import { ManageUserlevelComponent } from './components/user/manage-userlevel/manage-userlevel.component';
//import { OrderingcancelstatusListComponent } from './components/ordering/orderingcancelstatus-list/orderingcancelstatus-list.component';
//import { ManageOrderingcancelstatusComponent } from './components/ordering/manage-orderingcancelstatus/manage-orderingcancelstatus.component';
//import { WelcomeinformationListComponent } from './components/welcomeinformation/welcomeinformation-list/welcomeinformation-list.component';
//import { ManageWelcomeinformationComponent } from './components/welcomeinformation/manage-welcomeinformation/manage-welcomeinformation.component';
//import { CorporatetypeListComponent } from './components/corporatetype/corporatetype-list/corporatetype-list.component';
//import { ManageCorporatetypeComponent } from './components/corporatetype/manage-corporatetype/manage-corporatetype.component';
//import { RegisterdocumentListComponent } from './components/register/registerdocument-list/registerdocument-list.component';
//import { RegisterinformationListComponent } from './components/register/registerinformation-list/registerinformation-list.component';
//import { ManageRegisterinformationComponent } from './components/register/manage-registerinformation/manage-registerinformation.component';
//import { ManageRegisterdocumentComponent } from './components/register/manage-registerdocument/manage-registerdocument.component';
//import { TitleListComponent } from './components/title/title-list/title-list.component';
//import { ManageTitleComponent } from './components/title/manage-title/manage-title.component';
//import { ContainertypeListComponent } from './components/container/containertype-list/containertype-list.component';
//import { ContainerspecificationListComponent } from './components/container/containerspecification-list/containerspecification-list.component';
//import { ManageCaontainertypeComponent } from './components/container/manage-containertype/manage-containertype.component';
//import { ManageContainerspecificationComponent } from './components/container/manage-containerspecification/manage-containerspecification.component';
//import { ProvinceListComponent } from './components/province/province-list/province-list.component';
//import { ManageProvinceComponent } from './components/province/manage-province/manage-province.component';
//import { DistrictListComponent } from './components/district/district-list/district-list.component';
//import { ManageDistrictComponent } from './components/district/manage-district/manage-district.component';
//import { SubdistrictListComponent } from './components/subdistrict/subdistrict-list/subdistrict-list.component';
//import { ManageSubdistrictComponent } from './components/subdistrict/manage-subdistrict/manage-subdistrict.component';
//import { DeliveryListComponent } from './components/delivery/delivery-list/delivery-list.component';
//import { ManageDeliveryComponent } from './components/delivery/manage-delivery/manage-delivery.component';
//import { EnergysavingdeviceListComponent } from './components/energysavingdevice/energysavingdevice-list/energysavingdevice-list.component';
//import { ManageEnergysavingdeviceComponent } from './components/energysavingdevice/manage-energysavingdevice/manage-energysavingdevice.component';
//import { CustomerpersonalprofileComponent } from './components/customer/customerpersonalprofile-list/customerprofile-list.component';
//import { ManageCustomerpersonalprofileComponent } from './components/customer/manage-customerpersonalprofile/manage-customerprofile.component';
//import { ManageDriverprofileComponent } from './components/driver/manage-driverprofile/manage-driverprofile.component';
//import { DriverprofileListComponent } from './components/driver/driverprofile-list/driverprofile-list.component';
//import { ManageCarprofileComponent } from './components/car/manage-carprofile/manage-carprofile.component';
//import { CarprofileListComponent } from './components/car/carprofile-list/carprofile-list.component';
//import { SecurityListComponent } from './components/security/security-list/security-list.component';
//import { ManageSecurityComponent } from './components/security/manage-security/manage-security.component';
//import { TaxCustomerComponent } from './components/tax/tax-customer/tax-customer.component';
//import { TaxDriverComponent } from './components/tax/tax-driver/tax-driver.component';
//import { SigninComponent } from './components/signin/signin.component';
//import { AdminLayoutComponent } from './layouts/admin-layout/admin-layout.component';
//import { AuthLayoutComponent } from './layouts/auth-layout/auth-layout.component';
//import { AuthGuardService } from './services/auth-guard/auth-guard.service';
//export const authContent: Routes = [
//  {
//    path: '',
//    loadChildren: () => import('../app/layouts/auth-layout/auth-layout.module').then(m => m.AuthLayoutModule)
//  }
//]
//export const adminContent: Routes = [
//  {
//    path: '',
//    loadChildren: () => import('../app/layouts/admin-layout/admin-layout.module').then(m => m.AdminLayoutModule)
//  }
//]
//const routes: Routes = [
//  {
//    path: '',
//    redirectTo: '/news', // first time 
//    pathMatch: 'full'
//  },
//  {
//    path: '',
//    component: AuthLayoutComponent,
//    children: authContent,
//    canActivate: [AuthGuardService]
//  },
//  {
//    path: '',
//    component: AdminLayoutComponent,
//    children: adminContent,
//    canActivate: [AuthGuardService]
//  },
//  {
//    path: '**',
//    redirectTo: '',

//  },
//  { path: 'country', component: CountryListComponent },
//  { path: 'managecountry', component: ManageCountryComponent },
//  { path: 'managecountry/:id', component: ManageCountryComponent },
//  { path: 'region', component: RegionListComponent },
//  { path: 'manageregion', component: ManageRegionComponent },
//  { path: 'manageregion/:id', component: ManageRegionComponent },
//  { path: 'cartype', component: CartypeListComponent },
//  { path: 'managecartype', component: ManageCartypeComponent },
//  { path: 'managecartype/:id', component: ManageCartypeComponent },
//  { path: 'carlist', component: CarlistListComponent },
//  { path: 'managecarlist', component: ManageCarlistComponent },
//  { path: 'managecarlist/:id', component: ManageCarlistComponent },
//  { path: 'cardescription', component: CardescriptionListComponent },
//  { path: 'managecardescription', component: ManageCardescriptionComponent },
//  { path: 'managecardescription/:id', component: ManageCardescriptionComponent },
//  { path: 'carspecification', component: CarspecificationListComponent },
//  { path: 'managecarspecification', component: ManageCarspecificationComponent },
//  { path: 'managecarspecification/:id', component: ManageCarspecificationComponent },
//  { path: 'carfeature', component: CarfeatureListComponent },
//  { path: 'managecarfeature', component: ManageCarfeatureComponent },
//  { path: 'managecarfeature/:id', component: ManageCarfeatureComponent },
//  { path: 'producttype', component: ProducttypeListComponent },
//  { path: 'manageproducttype', component: ManageProducttypeComponent },
//  { path: 'manageproducttype/:id', component: ManageProducttypeComponent },
//  { path: 'productpackaging', component: ProductpackagingListComponent },
//  { path: 'manageproductpackaging', component: ManageProductpackagingComponent },
//  { path: 'manageproductpackaging/:id', component: ManageProductpackagingComponent },
//  { path: 'userlevel', component: UserlevelListComponent },
//  { path: 'manageuserlevel', component: ManageUserlevelComponent },
//  { path: 'manageuserlevel/:id', component: ManageUserlevelComponent },
//  { path: 'orderingcancelstatus', component: OrderingcancelstatusListComponent },
//  { path: 'manageorderingcancelstatus', component: ManageOrderingcancelstatusComponent },
//  { path: 'manageorderingcancelstatus/:id', component: ManageOrderingcancelstatusComponent },
//  { path: 'welcomeinformation', component: WelcomeinformationListComponent },
//  { path: 'managewelcomeinformation', component: ManageWelcomeinformationComponent },
//  { path: 'managewelcomeinformation/:id', component: ManageWelcomeinformationComponent },
//  { path: 'corporatetype', component: CorporatetypeListComponent },
//  { path: 'managecorporatetype', component: ManageCorporatetypeComponent },
//  { path: 'managecorporatetype/:id', component: ManageCorporatetypeComponent },
//  { path: 'registerinformation', component: RegisterinformationListComponent },
//  { path: 'manageregisterinformation', component: ManageRegisterinformationComponent },
//  { path: 'manageregisterinformation/:id', component: ManageRegisterinformationComponent },
//  { path: 'registerdocument', component: RegisterdocumentListComponent },
//  { path: 'manageregisterdocument', component: ManageRegisterdocumentComponent },
//  { path: 'manageregisterdocument/:id', component: ManageRegisterdocumentComponent },
//  { path: 'title', component: TitleListComponent },
//  { path: 'managetitle', component: ManageTitleComponent },
//  { path: 'managetitle/:id', component: ManageTitleComponent },
//  { path: 'containertype', component: ContainertypeListComponent },
//  { path: 'managecontainertype', component: ManageCaontainertypeComponent },
//  { path: 'managecontainertype/:id', component: ManageCaontainertypeComponent },
//  { path: 'containerspecification', component: ContainerspecificationListComponent },
//  { path: 'managecontainerspecification', component: ManageContainerspecificationComponent },
//  { path: 'managecontainerspecification/:id', component: ManageContainerspecificationComponent },
//  { path: 'delivery', component: DeliveryListComponent },
//  { path: 'managedelivery', component: ManageDeliveryComponent },
//  { path: 'managedelivery/:id', component: ManageDeliveryComponent },
//  { path: 'energysavingdevice', component: EnergysavingdeviceListComponent },
//  { path: 'manageenergysavingdevice', component: ManageEnergysavingdeviceComponent },
//  { path: 'manageenergysavingdevice/:id', component: ManageEnergysavingdeviceComponent },
//  { path: 'customerprofile', component: CustomerpersonalprofileComponent },
//  { path: 'managecustomerprofile', component: ManageCustomerpersonalprofileComponent },
//  { path: 'managecustomerprofile/:id', component: ManageCustomerpersonalprofileComponent },
//  { path: 'province', component: ProvinceListComponent },
//  { path: 'manageprovince', component: ManageProvinceComponent },
//  { path: 'manageprovince/:id', component: ManageProvinceComponent },
//  { path: 'district', component: DistrictListComponent },
//  { path: 'managedistrict', component: ManageDistrictComponent },
//  { path: 'managedistrict/:id', component: ManageDistrictComponent },
//  { path: 'subdistrict', component: SubdistrictListComponent },
//  { path: 'managesubdistrict', component: ManageSubdistrictComponent },
//  { path: 'managesubdistrict/:id', component: ManageSubdistrictComponent },
//  { path: 'driverprofile', component: DriverprofileListComponent },
//  { path: 'managedriverprofile', component: ManageDriverprofileComponent },
//  { path: 'managedriverprofile/:id', component: ManageDriverprofileComponent },
//  { path: 'carprofile', component: CarprofileListComponent },
//  { path: 'managecarprofile', component: ManageCarprofileComponent },
//  { path: 'managecarprofile/:id', component: ManageCarprofileComponent },
//  { path: 'security', component: SecurityListComponent },
//  { path: 'managesecurity', component: ManageSecurityComponent },
//  { path: 'managesecurity/:id', component: ManageSecurityComponent },
//  { path: 'taxcustomer/:id', component: TaxCustomerComponent },
//  { path: 'taxdriver/:id', component: TaxDriverComponent },

  
//];

//@NgModule({
//  imports: [RouterModule.forRoot(routes)],
//  exports: [RouterModule]
//})
//export class AppRoutingModule { }

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ConditionComponent } from './components/condition/condition.component';
import { ContactComponent } from './components/contact/contact/contact.component';
import { TermsConditionsComponent } from './components/help/terms-conditions/terms-conditions.component';
import { SigninComponent } from './components/signin/signin.component';
import { AdminLayoutComponent } from './layouts/admin-layout/admin-layout.component';
import { AuthLayoutComponent } from './layouts/auth-layout/auth-layout.component';
import { AuthGuardService } from './services/auth-guard/auth-guard.service';
export const authContent: Routes = [
  {
    path: '',
    loadChildren: () => import('../app/layouts/auth-layout/auth-layout.module').then(m => m.AuthLayoutModule)
  }
]
export const adminContent: Routes = [
  {
    path: '',
    loadChildren: () => import('../app/layouts/admin-layout/admin-layout.module').then(m => m.AdminLayoutModule)
  }
]
const routes: Routes = [
  {
    path: '',
    redirectTo: '/dashboard', // first time 
    pathMatch: 'full'
  },
  {
    path: '',
    component: AuthLayoutComponent,
    children: authContent,
    canActivate: [AuthGuardService]
  },
  {
    path: '',
    component: AdminLayoutComponent,
    children: adminContent,
    canActivate: [AuthGuardService]
  },
  { path: 'contact', component: ContactComponent },
  { path: 'condition', component: ConditionComponent },


  {
    path: '**',
    redirectTo: '',

  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
