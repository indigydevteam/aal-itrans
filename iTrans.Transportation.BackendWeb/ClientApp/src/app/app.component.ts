//import { Component } from '@angular/core';

//@Component({
//  selector: 'app-root',
//  templateUrl: './app.component.html',
//  styleUrls: ['./app.component.scss']
//})
//export class AppComponent {
//  title = 'ClientApp';
//}

import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AppComponent implements OnInit {
  title = 'ClientApp';
  constructor(private translate: TranslateService) { }
  ngOnInit() {
    this.setLanguage();
  }
  async setLanguage() {
    const browserLang = window.localStorage.getItem('languages') ? window.localStorage.getItem('languages') : 'th';
    // Register translation languages
    this.translate.addLangs(['en', 'th']);
    // Set default language
    this.translate.setDefaultLang(browserLang);
  }
}

