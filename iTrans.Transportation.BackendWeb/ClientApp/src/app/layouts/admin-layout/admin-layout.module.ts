import { NgModule } from '@angular/core';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { Router, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AdminLayoutRoutes } from './admin-layout.routing';
import { CountryListComponent } from 'src/app/components/country/country-list/country-list.component';
import { ManageCountryComponent } from 'src/app/components/country/manage-country/manage-country.component';
import { RegionListComponent } from 'src/app/components/region/region-list/region-list.component';
import { ManageRegionComponent } from 'src/app/components/region/manage-region/manage-region.component';
import { ToastComponent } from 'src/app/components/toast/toast.component';
import { ManageCartypeComponent } from 'src/app/components/car/manage-cartype/manage-cartype.component';
import { CartypeListComponent } from 'src/app/components/car/cartype-list/cartype-list.component';
import { AnnouncementEditComponent } from 'src/app/components/news/announcement-edit/announcement-edit.component';
import { NewsEditComponent } from 'src/app/components/news/news-edit/news-edit.component';
import { TablePaginationComponent } from 'src/app/components/table-pagination/table-pagination.component';
import { AnnouncementCreateComponent } from 'src/app/components/news/announcement-create/announcement-create.component';
import { AnnouncementDetailComponent } from 'src/app/components/news/announcement-detail/announcement-detail.component';
import { PromotionsCreateComponent } from 'src/app/components/news/promotions-create/promotions-create.component';
import { PromotionsDetailComponent } from 'src/app/components/news/promotions-detail/promotions-detail.component';
import { NewsCreateComponent } from 'src/app/components/news/news-create/news-create.component';
import { NewsDetailComponent } from 'src/app/components/news/news-detail/news-detail.component';
import { PromotionsComponent } from 'src/app/components/news/promotions/promotions.component';
import { AnnouncementComponent } from 'src/app/components/news/announcement/announcement.component';
import { NewsComponent } from 'src/app/components/news/news/news.component';
import { NavbarComponent } from 'src/app/components/navbar/navbar.component';
import { ManageRegisterdocumentComponent } from 'src/app/components/register/manage-registerdocument/manage-registerdocument.component';
import { RegisterdocumentListComponent } from 'src/app/components/register/registerdocument-list/registerdocument-list.component';
import { ManageRegisterinformationComponent } from 'src/app/components/register/manage-registerinformation/manage-registerinformation.component';
import { RegisterinformationListComponent } from 'src/app/components/register/registerinformation-list/registerinformation-list.component';
import { ManageCorporatetypeComponent } from 'src/app/components/corporatetype/manage-corporatetype/manage-corporatetype.component';
import { CorporatetypeListComponent } from 'src/app/components/corporatetype/corporatetype-list/corporatetype-list.component';
import { ManageWelcomeinformationComponent } from 'src/app/components/welcomeinformation/manage-welcomeinformation/manage-welcomeinformation.component';
import { ManageOrderingcancelstatusComponent } from 'src/app/components/ordering/manage-orderingcancelstatus/manage-orderingcancelstatus.component';
import { WelcomeinformationListComponent } from 'src/app/components/welcomeinformation/welcomeinformation-list/welcomeinformation-list.component';
import { OrderingcancelstatusListComponent } from 'src/app/components/ordering/orderingcancelstatus-list/orderingcancelstatus-list.component';
import { ManageUserlevelComponent } from 'src/app/components/user/manage-userlevel/manage-userlevel.component';
import { UserlevelListComponent } from 'src/app/components/user/userlevel-list/userlevel-list.component';
import { ManageProductpackagingComponent } from 'src/app/components/product/manage-productpackaging/manage-productpackaging.component';
import { ManageProducttypeComponent } from 'src/app/components/product/manage-producttype/manage-producttype.component';
import { ProductpackagingListComponent } from 'src/app/components/product/productpackaging-list/productpackaging-list.component';
import { ProducttypeListComponent } from 'src/app/components/product/producttype-list/producttype-list.component';
import { ManageCarfeatureComponent } from 'src/app/components/car/manage-carfeature/manage-carfeature.component';
import { CarfeatureListComponent } from 'src/app/components/car/carfeature-list/carfeature-list.component';
import { CarspecificationListComponent } from 'src/app/components/car/carspecification-list/carspecification-list.component';
import { ManageCarspecificationComponent } from 'src/app/components/car/manage-carspecification/manage-carspecification.component';
import { ManageCardescriptionComponent } from 'src/app/components/car/manage-cardescription/manage-cardescription.component';
import { CardescriptionListComponent } from 'src/app/components/car/cardescription-list/cardescription-list.component';
import { ManageCarlistComponent } from 'src/app/components/car/manage-carlist/manage-carlist.component';
import { CarlistListComponent } from 'src/app/components/car/carlist-list/carlist-list.component';
import { AdminComponentModule } from '../admin-component/admin-component.module';
import { BrowserModule } from '@angular/platform-browser';
import { AngularEditorModule } from '@kolkov/angular-editor';
import { DatepickerModule } from 'ng2-datepicker';
import { SignalRService } from 'src/app/services/SignalR/signal-r.service';
import { SignalrJoinChatroomService } from 'src/app/services/SignalrJoinChatroom/signalr-join-chatroom.service';
import { EventService } from 'src/app/services/event/event.service';
import { ChatService } from 'src/app/services/chat/chat.service';
import { ChatSlideBarComponent } from 'src/app/components/chat/chat-slide-bar/chat-slide-bar.component';
import { PipeModule } from 'src/app/pipe/pipe.module';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';

import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { ImageCropperModule } from 'ngx-image-cropper';
import { DeliveryListComponent } from 'src/app/components/delivery/delivery-list/delivery-list.component';
import { ManageDeliveryComponent } from 'src/app/components/delivery/manage-delivery/manage-delivery.component';
import { CustomerprofileComponent } from 'src/app/components/customer/customerprofile-list/customerprofile-list.component';
import { ManageCustomerprofileComponent } from 'src/app/components/customer/manage-customerpersonalprofile/manage-customerprofile.component';
import { DriverprofileListComponent } from 'src/app/components/driver/driverprofile-list/driverprofile-list.component';
import { ManageDriverprofileComponent } from 'src/app/components/driver/manage-driverprofile/manage-driverprofile.component';
import { CarprofileListComponent } from 'src/app/components/car/carprofile-list/carprofile-list.component';
import { ManageCarprofileComponent } from 'src/app/components/car/manage-carprofile/manage-carprofile.component';
import { ContainertypeListComponent } from 'src/app/components/container/containertype-list/containertype-list.component';
import { ContainerspecificationListComponent } from 'src/app/components/container/containerspecification-list/containerspecification-list.component';
import { EnergysavingdeviceListComponent } from 'src/app/components/energysavingdevice/energysavingdevice-list/energysavingdevice-list.component';
import { AdminprofileListComponent } from 'src/app/components/admin/adminprofile-list/adminprofile-list.component';
import { PaymentListComponent } from 'src/app/components/payment/payment-list/payment-list.component';
import { MangePaymentComponent } from 'src/app/components/payment/mange-payment/mange-payment.component';
import { ManageCaontainertypeComponent } from 'src/app/components/container/manage-containertype/manage-containertype.component';
import { ManageContainerspecificationComponent } from 'src/app/components/container/manage-containerspecification/manage-containerspecification.component';
import { ManageEnergysavingdeviceComponent } from 'src/app/components/energysavingdevice/manage-energysavingdevice/manage-energysavingdevice.component';
import { TaxDriverComponent } from 'src/app/components/tax/tax-driver/tax-driver.component';
import { ChatComponent } from 'src/app/components/chat/chat/chat.component';
import { EmailComponent } from 'src/app/components/help/email/email.component';
import { FAQComponent } from 'src/app/components/help/faq/faq.component';
import { ComplainComponent } from 'src/app/components/help/complain/complain.component';
import { ConsentMarketingComponent } from 'src/app/components/help/consent-marketing/consent-marketing.component';
import { TermsConditionsComponent } from 'src/app/components/help/terms-conditions/terms-conditions.component';
import { ManageTermsconditionComponent } from 'src/app/components/help/manage-termscondition/manage-termscondition.component';
import { ManageFAQComponent } from 'src/app/components/help/manage-faq/manage-faq.component';
import { NgxSummernoteModule } from 'ngx-summernote';
import { DetailComplainComponent } from 'src/app/components/help/detail-complain/detail-complain.component';
import { DetailEmailComponent } from 'src/app/components/help/detail-email/detail-email.component';
import { ManageConsentMarketingComponent } from 'src/app/components/help/manage-consentmarketing/manage-consent-marketing.component';
import { AudittailListComponent } from 'src/app/components/audittail/audittail-list/audittail-list.component';
import { ContactComponent } from 'src/app/components/contact/contact/contact.component';
import { TaxCustomerComponent } from 'src/app/components/tax/tax-customer/tax-customer.component';
import { ChatListComponent } from 'src/app/components/help/chat-list/chat-list.component';
import { PermissionListComponent } from 'src/app/components/security/permission-list/permission-list.component';
import { ManagePermissionComponent } from 'src/app/components/security/manage-permission/manage-permission.component';
import { DetailTermsconditionComponent } from 'src/app/components/help/detail-termscondition/detail-termscondition.component';
import { DetailConsentmarketingComponent } from 'src/app/components/help/detail-consentmarketing/detail-consentmarketing.component';
import { DashboardComponent } from 'src/app/components/dashboard/dashboard/dashboard.component';
import { ChartsModule } from 'ng2-charts';
import { ManageAdminprofileComponent } from 'src/app/components/admin/manage-adminprofile/manage-adminprofile.component';
import { ChangepasswordComponent } from 'src/app/components/security/changepassword/changepassword.component';
import { PersonalprivacyListComponent } from 'src/app/components/help/personalprivacy-list/personalprivacy-list.component';
import { PersonalprivacyDetailComponent } from 'src/app/components/help/personalprivacy-detail/personalprivacy-detail.component';
import { ManagePersonalprivacyComponent } from 'src/app/components/help/manage-personalprivacy/manage-personalprivacy.component';
import { CustomerLevelCharacteristicListComponent } from 'src/app/components/customer-level-characteristic/customer-level-characteristic-list/customer-level-characteristic-list.component';
import { ManageCustomerLevelCharacteristicComponent } from 'src/app/components/customer-level-characteristic/manage-customer-level-characteristic/manage-customer-level-characteristic.component';
import { DriverLevelCharacteristicListComponent } from 'src/app/components/driver-level-characteristic/driver-level-characteristic-list/driver-level-characteristic-list.component';
import { ManageDriverLevelCharacteristicComponent } from 'src/app/components/driver-level-characteristic/manage-driver-level-characteristic/manage-driver-level-characteristic.component';
import { ManageTitleComponent } from 'src/app/components/title/manage-title/manage-title.component';
import { TitleListComponent } from 'src/app/components/title/title-list/title-list.component';


import { AgGridModule } from 'ag-grid-angular';
import { CreateFaqComponent } from 'src/app/components/help/create-faq/create-faq.component';
 

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AdminLayoutRoutes),
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgbModule,
    AdminComponentModule,
    AngularEditorModule,
    DatepickerModule,
    PipeModule,
    NgxSummernoteModule,
    ChartsModule,
    AgGridModule.withComponents([]),
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
  ],
  declarations: [

    CountryListComponent,
    ManageCountryComponent,
    RegionListComponent,
    ManageRegionComponent,
    ToastComponent,
    CartypeListComponent,
    ManageCartypeComponent,
    CarlistListComponent,
    ManageCarlistComponent,
    CardescriptionListComponent,
    ManageCardescriptionComponent,
    CarspecificationListComponent,
    ManageCarspecificationComponent,
    CarfeatureListComponent,
    ManageCarfeatureComponent,
    ProducttypeListComponent,
    ProductpackagingListComponent,
    ManageProducttypeComponent,
    ManageProductpackagingComponent,
    UserlevelListComponent,
    ManageUserlevelComponent,
    OrderingcancelstatusListComponent,
    ManageOrderingcancelstatusComponent,
    WelcomeinformationListComponent,
    ManageWelcomeinformationComponent,
    CorporatetypeListComponent,
    ManageCorporatetypeComponent,
    RegisterinformationListComponent,
    ManageRegisterinformationComponent,
    RegisterdocumentListComponent,
    ManageRegisterdocumentComponent,
    NewsComponent,
    NewsCreateComponent,
    NewsEditComponent,
    NewsDetailComponent,
    AnnouncementComponent,
    AnnouncementDetailComponent,
    AnnouncementCreateComponent,
    AnnouncementEditComponent,
    PromotionsComponent,
    PromotionsDetailComponent,
    PromotionsCreateComponent,
    TablePaginationComponent,
    DeliveryListComponent,
    ManageDeliveryComponent,
    CustomerprofileComponent,
    ManageCustomerprofileComponent,
    DriverprofileListComponent,
    ManageDriverprofileComponent,
    CarprofileListComponent,
    ManageCarprofileComponent,
    ContainertypeListComponent,
    ContainerspecificationListComponent,
    EnergysavingdeviceListComponent,
    AdminprofileListComponent,
    PaymentListComponent,
    MangePaymentComponent,
    ManageCaontainertypeComponent,
    ManageContainerspecificationComponent,
    ManageEnergysavingdeviceComponent,
    TaxDriverComponent,
    EmailComponent,
    FAQComponent,
    ComplainComponent,
    ConsentMarketingComponent,
    TermsConditionsComponent,
    ManageTermsconditionComponent,
    ManageFAQComponent,
    ManageConsentMarketingComponent,
    DetailEmailComponent,
    DetailComplainComponent,
    AudittailListComponent,
    TaxCustomerComponent,
    ChatListComponent,
    PermissionListComponent,
    ManagePermissionComponent,
    DetailTermsconditionComponent,
    DetailConsentmarketingComponent,
    DashboardComponent,
    ManageAdminprofileComponent,
    ChangepasswordComponent,
    PersonalprivacyListComponent,
    PersonalprivacyDetailComponent,
    ManagePersonalprivacyComponent,
    CustomerLevelCharacteristicListComponent,
    ManageCustomerLevelCharacteristicComponent,
    DriverLevelCharacteristicListComponent,
    ManageDriverLevelCharacteristicComponent,
    CreateFaqComponent,
    TitleListComponent,
    ManageTitleComponent,
  ],
  providers: [
    SignalRService,
    SignalrJoinChatroomService,


  ],
  exports: [
    NavbarComponent,

  ]
})

export class AdminLayoutModule { }
export function HttpLoaderFactory(http: HttpClient): TranslateHttpLoader {
  return new TranslateHttpLoader(http);
}
