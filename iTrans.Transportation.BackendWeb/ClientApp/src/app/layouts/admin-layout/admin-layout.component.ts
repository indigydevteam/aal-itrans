import { Component, OnInit } from '@angular/core';

import { filter } from 'rxjs/operators';
import { NavigationEnd, ResolveStart, Router } from '@angular/router';
import { SignalRService } from 'src/app/services/SignalR/signal-r.service';
import { StorageService } from 'src/app/services/storage/storage.service';
import { EventService } from 'src/app/services/event/event.service';

// import { OneSignalService } from 'src/app/services/onesignal/one-signal.service';

@Component({
  selector: 'app-admin-layout',
  templateUrl: './admin-layout.component.html',
  styleUrls: ['./admin-layout.component.scss']
})
export class AdminLayoutComponent implements OnInit {
  userid: any;
  showChat = false;
  page: any = null;
  constructor(
    private router: Router,
    private signalRServ: SignalRService,
    private storage: StorageService,
    private event: EventService
  ) {

    this.router.events.subscribe((e: ResolveStart) => {
      const p = e.url.split('/');
      this.page = p[1];
    })

  }

  ngOnInit() {
    // this.onesignal.initOneSignal();
    this.countChatBage();
  }
  async countChatBage() {
    try {
  
      // await this.signalRServ.disconnectChatBadge();
      // await this.signalRServ.connectChatBadgeAsync();
      // this.signalRServ.hubConnectionChatBadge.off('badgeResult');
      // this.signalRServ.hubConnectionChatBadge.on('badgeResult', (res: any) => {


      //   this.event.onChatBadge.emit(res)
       
      // });


    } catch (error) {


      console.log(error);
      // this.coreService.services.logEvent.log('##badgeResult##', error, null);
    }
  }
}

