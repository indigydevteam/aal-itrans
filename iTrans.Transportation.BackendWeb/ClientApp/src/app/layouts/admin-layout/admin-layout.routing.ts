import { Routes } from '@angular/router';
import { CardescriptionListComponent } from 'src/app/components/car/cardescription-list/cardescription-list.component';
import { CarfeatureListComponent } from 'src/app/components/car/carfeature-list/carfeature-list.component';
import { CarlistListComponent } from 'src/app/components/car/carlist-list/carlist-list.component';
import { CarspecificationListComponent } from 'src/app/components/car/carspecification-list/carspecification-list.component';
import { CartypeListComponent } from 'src/app/components/car/cartype-list/cartype-list.component';
import { ManageCardescriptionComponent } from 'src/app/components/car/manage-cardescription/manage-cardescription.component';
import { ManageCarfeatureComponent } from 'src/app/components/car/manage-carfeature/manage-carfeature.component';
import { ManageCarlistComponent } from 'src/app/components/car/manage-carlist/manage-carlist.component';
import { ManageCarspecificationComponent } from 'src/app/components/car/manage-carspecification/manage-carspecification.component';
import { ManageCartypeComponent } from 'src/app/components/car/manage-cartype/manage-cartype.component';
import { ChatComponent } from 'src/app/components/chat/chat/chat.component';
import { CorporatetypeListComponent } from 'src/app/components/corporatetype/corporatetype-list/corporatetype-list.component';
import { ManageCorporatetypeComponent } from 'src/app/components/corporatetype/manage-corporatetype/manage-corporatetype.component';
import { CountryListComponent } from 'src/app/components/country/country-list/country-list.component';
import { ManageCountryComponent } from 'src/app/components/country/manage-country/manage-country.component';
import { AnnouncementCreateComponent } from 'src/app/components/news/announcement-create/announcement-create.component';
import { AnnouncementDetailComponent } from 'src/app/components/news/announcement-detail/announcement-detail.component';
import { AnnouncementEditComponent } from 'src/app/components/news/announcement-edit/announcement-edit.component';
import { AnnouncementComponent } from 'src/app/components/news/announcement/announcement.component';
import { NewsCreateComponent } from 'src/app/components/news/news-create/news-create.component';
import { NewsDetailComponent } from 'src/app/components/news/news-detail/news-detail.component';
import { NewsEditComponent } from 'src/app/components/news/news-edit/news-edit.component';
import { NewsComponent } from 'src/app/components/news/news/news.component';
import { PromotionsCreateComponent } from 'src/app/components/news/promotions-create/promotions-create.component';
import { PromotionsDetailComponent } from 'src/app/components/news/promotions-detail/promotions-detail.component';
import { PromotionsComponent } from 'src/app/components/news/promotions/promotions.component';
import { ManageOrderingcancelstatusComponent } from 'src/app/components/ordering/manage-orderingcancelstatus/manage-orderingcancelstatus.component';
import { OrderingcancelstatusListComponent } from 'src/app/components/ordering/orderingcancelstatus-list/orderingcancelstatus-list.component';
import { ManageProductpackagingComponent } from 'src/app/components/product/manage-productpackaging/manage-productpackaging.component';
import { ManageProducttypeComponent } from 'src/app/components/product/manage-producttype/manage-producttype.component';
import { ProductpackagingListComponent } from 'src/app/components/product/productpackaging-list/productpackaging-list.component';
import { ProducttypeListComponent } from 'src/app/components/product/producttype-list/producttype-list.component';
import { ManageRegionComponent } from 'src/app/components/region/manage-region/manage-region.component';
import { RegionListComponent } from 'src/app/components/region/region-list/region-list.component';
import { ManageRegisterdocumentComponent } from 'src/app/components/register/manage-registerdocument/manage-registerdocument.component';
import { ManageRegisterinformationComponent } from 'src/app/components/register/manage-registerinformation/manage-registerinformation.component';
import { RegisterdocumentListComponent } from 'src/app/components/register/registerdocument-list/registerdocument-list.component';
import { RegisterinformationListComponent } from 'src/app/components/register/registerinformation-list/registerinformation-list.component';
import { ManageUserlevelComponent } from 'src/app/components/user/manage-userlevel/manage-userlevel.component';
import { UserlevelListComponent } from 'src/app/components/user/userlevel-list/userlevel-list.component';
import { ManageWelcomeinformationComponent } from 'src/app/components/welcomeinformation/manage-welcomeinformation/manage-welcomeinformation.component';
import { WelcomeinformationListComponent } from 'src/app/components/welcomeinformation/welcomeinformation-list/welcomeinformation-list.component';
import { ContainertypeListComponent } from 'src/app/components/container/containertype-list/containertype-list.component';
import { ManageCaontainertypeComponent } from 'src/app/components/container/manage-containertype/manage-containertype.component';
import { ContainerspecificationListComponent } from 'src/app/components/container/containerspecification-list/containerspecification-list.component';
import { ManageContainerspecificationComponent } from 'src/app/components/container/manage-containerspecification/manage-containerspecification.component';
import { EnergysavingdeviceListComponent } from 'src/app/components/energysavingdevice/energysavingdevice-list/energysavingdevice-list.component';
import { ManageEnergysavingdeviceComponent } from 'src/app/components/energysavingdevice/manage-energysavingdevice/manage-energysavingdevice.component';
import { DeliveryListComponent } from 'src/app/components/delivery/delivery-list/delivery-list.component';
import { ManageDeliveryComponent } from 'src/app/components/delivery/manage-delivery/manage-delivery.component';
import { CustomerprofileComponent } from 'src/app/components/customer/customerprofile-list/customerprofile-list.component';
import { ManageCustomerprofileComponent } from 'src/app/components/customer/manage-customerpersonalprofile/manage-customerprofile.component';
import { AuthGuardService } from 'src/app/services/auth-guard/auth-guard.service';
import { DriverprofileListComponent } from 'src/app/components/driver/driverprofile-list/driverprofile-list.component';
import { ManageDriverprofileComponent } from 'src/app/components/driver/manage-driverprofile/manage-driverprofile.component';
import { CarprofileListComponent } from 'src/app/components/car/carprofile-list/carprofile-list.component';
import { ManageCarprofileComponent } from 'src/app/components/car/manage-carprofile/manage-carprofile.component';
import { AdminprofileListComponent } from 'src/app/components/admin/adminprofile-list/adminprofile-list.component';
import { ManageAdminprofileComponent } from 'src/app/components/admin/manage-adminprofile/manage-adminprofile.component';
import { PaymentListComponent } from 'src/app/components/payment/payment-list/payment-list.component';
import { MangePaymentComponent } from 'src/app/components/payment/mange-payment/mange-payment.component';
import { TaxDriverComponent } from 'src/app/components/tax/tax-driver/tax-driver.component';
import { EmailComponent } from 'src/app/components/help/email/email.component';
import { FAQComponent } from 'src/app/components/help/faq/faq.component';
// import { PrivacyComponent } from 'src/app/components/help/privacy-usage/privacy.component';
import { ComplainComponent } from 'src/app/components/help/complain/complain.component';
import { ConsentMarketingComponent } from 'src/app/components/help/consent-marketing/consent-marketing.component';
import { TermsConditionsComponent } from 'src/app/components/help/terms-conditions/terms-conditions.component';
import { ManageTermsconditionComponent } from 'src/app/components/help/manage-termscondition/manage-termscondition.component';
import { ManageFAQComponent } from 'src/app/components/help/manage-faq/manage-faq.component';
import { DetailComplainComponent } from 'src/app/components/help/detail-complain/detail-complain.component';
import { DetailEmailComponent } from 'src/app/components/help/detail-email/detail-email.component';
import { ManageConsentMarketingComponent } from 'src/app/components/help/manage-consentmarketing/manage-consent-marketing.component';
import { AudittailListComponent } from 'src/app/components/audittail/audittail-list/audittail-list.component';
import { TaxCustomerComponent } from 'src/app/components/tax/tax-customer/tax-customer.component';
import { ChatListComponent } from 'src/app/components/help/chat-list/chat-list.component';
import { ManagePermissionComponent } from 'src/app/components/security/manage-permission/manage-permission.component';
import { PermissionListComponent } from 'src/app/components/security/permission-list/permission-list.component';
import { DetailTermsconditionComponent } from 'src/app/components/help/detail-termscondition/detail-termscondition.component';
import { DetailConsentmarketingComponent } from 'src/app/components/help/detail-consentmarketing/detail-consentmarketing.component';
import { DashboardComponent } from 'src/app/components/dashboard/dashboard/dashboard.component';
import { ChangepasswordComponent } from 'src/app/components/security/changepassword/changepassword.component';
import { PersonalprivacyListComponent } from 'src/app/components/help/personalprivacy-list/personalprivacy-list.component';
import { PersonalprivacyDetailComponent } from 'src/app/components/help/personalprivacy-detail/personalprivacy-detail.component';
import { ManagePersonalprivacyComponent } from 'src/app/components/help/manage-personalprivacy/manage-personalprivacy.component';
import { CustomerLevelCharacteristicListComponent } from 'src/app/components/customer-level-characteristic/customer-level-characteristic-list/customer-level-characteristic-list.component';
import { ManageCustomerLevelCharacteristicComponent } from 'src/app/components/customer-level-characteristic/manage-customer-level-characteristic/manage-customer-level-characteristic.component';
import { DriverLevelCharacteristicListComponent } from 'src/app/components/driver-level-characteristic/driver-level-characteristic-list/driver-level-characteristic-list.component';
import { ManageDriverLevelCharacteristicComponent } from 'src/app/components/driver-level-characteristic/manage-driver-level-characteristic/manage-driver-level-characteristic.component';
import { CreateFaqComponent } from 'src/app/components/help/create-faq/create-faq.component';
import { ManageTitleComponent } from 'src/app/components/title/manage-title/manage-title.component';
import { TitleListComponent } from 'src/app/components/title/title-list/title-list.component';
export const AdminLayoutRoutes: Routes = [
  {
    path: 'country', component: CountryListComponent,
    // canActivate: [AuthGuardService]
  },
  { path: 'managecountry', component: ManageCountryComponent },
  { path: 'managecountry/:id', component: ManageCountryComponent },
  { path: 'region', component: RegionListComponent },
  { path: 'manageregion', component: ManageRegionComponent },
  { path: 'manageregion/:id', component: ManageRegionComponent },
  { path: 'cartype', component: CartypeListComponent },
  { path: 'managecartype', component: ManageCartypeComponent },
  { path: 'managecartype/:id', component: ManageCartypeComponent },
  { path: 'carlist', component: CarlistListComponent },
  { path: 'managecarlist', component: ManageCarlistComponent },
  { path: 'managecarlist/:id', component: ManageCarlistComponent },
  { path: 'cardescription', component: CardescriptionListComponent },
  { path: 'managecardescription', component: ManageCardescriptionComponent },
  { path: 'managecardescription/:id', component: ManageCardescriptionComponent },
  { path: 'carspecification', component: CarspecificationListComponent },
  { path: 'managecarspecification', component: ManageCarspecificationComponent },
  { path: 'managecarspecification/:id', component: ManageCarspecificationComponent },
  { path: 'carfeature', component: CarfeatureListComponent },
  { path: 'managecarfeature', component: ManageCarfeatureComponent },
  { path: 'managecarfeature/:id', component: ManageCarfeatureComponent },
  { path: 'producttype', component: ProducttypeListComponent },
  { path: 'manageproducttype', component: ManageProducttypeComponent },
  { path: 'manageproducttype/:id', component: ManageProducttypeComponent },
  { path: 'productpackaging', component: ProductpackagingListComponent },
  { path: 'manageproductpackaging', component: ManageProductpackagingComponent },
  { path: 'manageproductpackaging/:id', component: ManageProductpackagingComponent },
  { path: 'userlevel', component: UserlevelListComponent },
  { path: 'manageuserlevel', component: ManageUserlevelComponent },
  { path: 'manageuserlevel/:id', component: ManageUserlevelComponent },
  { path: 'orderingcancelstatus', component: OrderingcancelstatusListComponent },
  { path: 'manageorderingcancelstatus', component: ManageOrderingcancelstatusComponent },
  { path: 'manageorderingcancelstatus/:id', component: ManageOrderingcancelstatusComponent },
  { path: 'welcomeinformation', component: WelcomeinformationListComponent },
  { path: 'managewelcomeinformation', component: ManageWelcomeinformationComponent },
  { path: 'managewelcomeinformation/:id', component: ManageWelcomeinformationComponent },
  { path: 'corporatetype', component: CorporatetypeListComponent },
  {path: 'managecorporatetype', component: ManageCorporatetypeComponent},
  { path: 'managecorporatetype/:id', component: ManageCorporatetypeComponent },
  { path: 'registerinformation', component: RegisterinformationListComponent },
  { path: 'manageregisterinformation', component: ManageRegisterinformationComponent },
  { path: 'manageregisterinformation/:id', component: ManageRegisterinformationComponent },
  { path: 'registerdocument', component: RegisterdocumentListComponent },
  { path: 'manageregisterdocument', component: ManageRegisterdocumentComponent },
  { path: 'manageregisterdocument/:id', component: ManageRegisterdocumentComponent },
  { path: 'title', component: TitleListComponent },
  { path: 'managetitle', component: ManageTitleComponent },
  { path: 'managetitle/:id', component: ManageTitleComponent },
  // News
  {
    path: 'news', component: NewsComponent,
    // canActivate: [AuthGuardService]
  },
  { path: 'news/create', component: NewsCreateComponent },
  { path: 'news/edit/:id', component: NewsEditComponent },
  { path: 'news/detail/:id', component: NewsDetailComponent },

  { path: 'announcement', component: AnnouncementComponent },
  { path: 'announcement/create', component: AnnouncementCreateComponent },
  { path: 'announcement/detail/:id', component: AnnouncementDetailComponent },
  { path: 'announcement/edit/:id', component: AnnouncementEditComponent },


  { path: 'promotion', component: PromotionsComponent },
  { path: 'promotion/:id', component: PromotionsDetailComponent },
  { path: 'promotion/create', component: PromotionsCreateComponent },
  { path: 'chat', component: ChatComponent },

  { path: 'corporatetype', component: CorporatetypeListComponent },
  { path: 'managecorporatetype', component: ManageCorporatetypeComponent },
  { path: 'managecorporatetype/:id', component: ManageCorporatetypeComponent },

  { path: 'cartype', component: CartypeListComponent },
  { path: 'managecartype', component: ManageCartypeComponent },
  { path: 'managecartype/:id', component: ManageCartypeComponent },

  { path: 'carlist', component: CarlistListComponent },
  { path: 'managecarlist', component: ManageCarlistComponent },
  { path: 'managecarlist/:id', component: ManageCarlistComponent },

  { path: 'cardescription', component: CardescriptionListComponent },
  { path: 'managecardescription', component: ManageCardescriptionComponent },
  { path: 'managecardescription/:id', component: ManageCardescriptionComponent },

  { path: 'carspecification', component: CarspecificationListComponent },
  { path: 'managecarspecification', component: ManageCarspecificationComponent },
  { path: 'managecarspecification/:id', component: ManageCarspecificationComponent },

  { path: 'carfeature', component: CarfeatureListComponent },
  { path: 'managecarfeature', component: ManageCarfeatureComponent },
  { path: 'managecarfeature/:id', component: ManageCarfeatureComponent },

  { path: 'producttype', component: ProducttypeListComponent },
  { path: 'manageproducttype', component: ManageProducttypeComponent },
  { path: 'manageproducttype/:id', component: ManageProducttypeComponent },

  { path: 'productpackaging', component: ProductpackagingListComponent },
  { path: 'manageproductpackaging', component: ManageProductpackagingComponent },
  { path: 'manageproductpackaging/:id', component: ManageProductpackagingComponent },

  { path: 'containertype', component: ContainertypeListComponent },
  { path: 'managecontainertype', component: ManageCaontainertypeComponent },
  { path: 'managecontainertype/:id', component: ManageCaontainertypeComponent },

  { path: 'userlevel', component: UserlevelListComponent },
  { path: 'manageuserlevel', component: ManageUserlevelComponent },
  { path: 'manageuserlevel/:id', component: ManageUserlevelComponent },

  { path: 'orderingcancelstatus', component: OrderingcancelstatusListComponent },
  { path: 'manageorderingcancelstatus', component: ManageOrderingcancelstatusComponent },
  { path: 'manageorderingcancelstatus/:id', component: ManageOrderingcancelstatusComponent },

  { path: 'containerspecification', component: ContainerspecificationListComponent },
  { path: 'managecontainerspecification', component: ManageContainerspecificationComponent },
  { path: 'managecontainerspecification/:id', component: ManageContainerspecificationComponent },

  { path: 'energysavingdevice', component: EnergysavingdeviceListComponent },
  { path: 'manageenergysavingdevice', component: ManageEnergysavingdeviceComponent },
  { path: 'manageenergysavingdevice/:id', component: ManageEnergysavingdeviceComponent },

  { path: 'delivery', component: DeliveryListComponent },
  { path: 'managedelivery', component: ManageDeliveryComponent },
  { path: 'managedelivery/:id', component: ManageDeliveryComponent },

  { path: 'customerprofile', component: CustomerprofileComponent },
  { path: 'managecustomerprofile', component: ManageCustomerprofileComponent },
  { path: 'managecustomerprofile/:id', component: ManageCustomerprofileComponent },

  { path: 'driverprofile', component: DriverprofileListComponent },
  { path: 'managedriverprofile', component: ManageDriverprofileComponent },
  { path: 'managedriverprofile/:id', component: ManageDriverprofileComponent },
  
  { path: 'carprofile', component: CarprofileListComponent },
  { path: 'managecarprofile', component: ManageCarprofileComponent },
  { path: 'managecarprofile/:id', component: ManageCarprofileComponent },
  
  { path: 'adminprofile', component: AdminprofileListComponent },
  { path: 'manageadminprofile', component: ManageAdminprofileComponent },
  { path: 'manageadminprofile/:id', component: ManageAdminprofileComponent },
  
  
  { path: 'payment', component: PaymentListComponent },
  { path: 'detailpayment', component: MangePaymentComponent },
  { path: 'detailpayment/:id', component: MangePaymentComponent },

  { path: 'taxdriver', component: TaxDriverComponent },
  { path: 'taxdriver/:id', component: TaxDriverComponent },

  { path: 'taxcustomer', component: TaxCustomerComponent },
  { path: 'taxcustomer/:id', component: TaxCustomerComponent },

  { path: 'email', component: EmailComponent },
  { path: 'detailemail/:id/:problemTopicId', component: DetailEmailComponent },

  { path: 'FAQ', component: FAQComponent },
  { path: 'manageFAQ', component: ManageFAQComponent },
  { path: 'manageFAQ/:id', component: ManageFAQComponent },
  { path: 'createFAQ', component: CreateFaqComponent },

  { path: 'consentmarketing', component: ConsentMarketingComponent },
  { path: 'manageconsentmarketing', component:  ManageConsentMarketingComponent},
  { path: 'manageconsentmarketing/:id', component:  ManageConsentMarketingComponent},
  { path: 'detailconsentmarketing/:id', component:  DetailConsentmarketingComponent},

  // { path: 'consentmarketing/:id', component: ConsentMarketingComponent },
  { path: 'termsconditions', component: TermsConditionsComponent },
  { path: 'managetermsconditions', component: ManageTermsconditionComponent },
  { path: 'managetermsconditions/:id', component: ManageTermsconditionComponent },
  { path: 'detailtermsconditions/:id', component: DetailTermsconditionComponent },


  { path: 'complain', component: ComplainComponent },
  { path: 'detailcomplain/:id/:module', component: DetailComplainComponent },

  { path: 'audittails', component: AudittailListComponent },
  { path: 'chatlog', component: ChatListComponent },

  { path: 'permission', component: PermissionListComponent },
  { path: 'managepermission', component: ManagePermissionComponent },
  { path: 'managepermission/:id', component: ManagePermissionComponent },

  { path: 'dashboard', component: DashboardComponent},
  { path: 'changepassword', component: ChangepasswordComponent},

  { path: 'personalprivacy', component: PersonalprivacyListComponent},
  { path: 'managepersonalprivacy', component: ManagePersonalprivacyComponent },
  { path: 'managepersonalprivacy/:id', component: ManagePersonalprivacyComponent },

  { path: 'detailpersonalprivacy/:id', component: PersonalprivacyDetailComponent},

  { path: 'customerlevelcharacteristic', component: CustomerLevelCharacteristicListComponent },
  { path: 'managecustomerlevelcharacteristic', component: ManageCustomerLevelCharacteristicComponent },
  { path: 'managecustomerlevelcharacteristic/:id', component: ManageCustomerLevelCharacteristicComponent },

  { path: 'driverlevelcharacteristic', component: DriverLevelCharacteristicListComponent },
  { path: 'managedriverlevelcharacteristic', component: ManageDriverLevelCharacteristicComponent },
  { path: 'managedriverlevelcharacteristic/:id', component: ManageDriverLevelCharacteristicComponent },
];
