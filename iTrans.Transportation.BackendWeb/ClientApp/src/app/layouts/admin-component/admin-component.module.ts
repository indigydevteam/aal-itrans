import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavbarComponent } from 'src/app/components/navbar/navbar.component';
import { Router, RouterModule } from '@angular/router';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { ChatSlideBarComponent } from 'src/app/components/chat/chat-slide-bar/chat-slide-bar.component';
import { EventService } from 'src/app/services/event/event.service';
import { SignalRService } from 'src/app/services/SignalR/signal-r.service';
import { StorageService } from 'src/app/services/storage/storage.service';
import { TranslateLanguageService } from 'src/app/services/translateLanguage/translate-language.service';
import { ToastService } from 'src/app/services/toast/toast.service';
import { AlertService } from 'src/app/services/alert/alert.service';
import { SignalrJoinChatroomService } from 'src/app/services/SignalrJoinChatroom/signalr-join-chatroom.service';
import { UrlDetectService } from 'src/app/services/UrlDetect/url-detect.service';
import { NgxSmartModalModule, NgxSmartModalService } from 'ngx-smart-modal';
import { PipeModule } from 'src/app/pipe/pipe.module';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { ToastrModule } from 'ngx-toastr';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
import { ModalModule } from 'ngx-bootstrap/modal';
import { ImageCropperModule } from 'ngx-image-cropper';
import { NgxGalleryModule } from '@kolkov/ngx-gallery';



@NgModule({
  declarations: [
    NavbarComponent,
    ChatSlideBarComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
    PipeModule,
    NgbModule,
    ImageCropperModule,
    NgxSmartModalModule.forRoot(),
    ModalModule.forRoot(),
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    ToastrModule.forRoot(),
    NgxGalleryModule
  ],
  exports: [
    NavbarComponent,
    ChatSlideBarComponent,
    ImageCropperModule,
    NgxGalleryModule
  ]
})
export class AdminComponentModule { }
export function HttpLoaderFactory(http: HttpClient): TranslateHttpLoader {
  return new TranslateHttpLoader(http);
}