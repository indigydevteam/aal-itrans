import { Routes } from '@angular/router';
import { SigninComponent } from 'src/app/components/signin/signin.component';
import { AuthGuardService } from 'src/app/services/auth-guard/auth-guard.service';


export const AuthLayoutRoutes: Routes = [
    {
        path: 'auth/signin',
        component: SigninComponent,
       
    },
    // { path: 'authorization', component: AuthorizationComponent },
];


