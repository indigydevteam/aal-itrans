

import { LoadingService } from './services/loading.service';
import { BrowserModule } from '@angular/platform-browser';
import { CUSTOM_ELEMENTS_SCHEMA, ErrorHandler, LOCALE_ID, NgModule } from '@angular/core';
import { NgxRangeModule } from 'ngx-range';
import { NgbDateParserFormatter, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpClient, HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { GlobalErrorService } from './services/error/global-error.service';
import { CountryService } from './services/country/country.service';
import { CarService } from './services/car/car.service';
import { ProductService } from './services/product/product.service';
import { UserService } from './services/user/user.service';
import { OrderingService } from './services/ordering/ordering.service';
import { NgxSummernoteModule } from 'ngx-summernote';
import { WelcomeInformationService } from './services/welcomeinformation/welcomeinformation.service';
import { CorporateTypeService } from './services/corporatetype/corporatetype';
import { RegisterService } from './services/register/register.service';

import { HttpService } from './services/http/http.service';
import { NewsService } from './services/news/news.service';
import { AdminLayoutComponent } from './layouts/admin-layout/admin-layout.component';
import { AuthLayoutComponent } from './layouts/auth-layout/auth-layout.component';
import { AdminComponentModule } from './layouts/admin-component/admin-component.module';
import { AuthenService } from './services/authen/authen.service';
import { DeviceDetectorService } from 'ngx-device-detector';
import { StorageService } from './services/storage/storage.service';

import { ChatService } from './services/chat/chat.service';


import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { ChatComponent } from './components/chat/chat/chat.component';
import { NgxSmartModalModule } from 'ngx-smart-modal';
import { PipeModule } from './pipe/pipe.module';
import { AlertService } from './services/alert/alert.service';
import { NgxCoolDialogsModule } from 'ngx-cool-dialogs';
import { NgxUiLoaderConfig, NgxUiLoaderModule, PB_DIRECTION, SPINNER } from 'ngx-ui-loader';
import { ImgCroperComponent } from './components/img-croper/img-croper.component';
import { AuthGuardService } from './services/auth-guard/auth-guard.service';
import { DriverService } from './services/driver/driver.service';
import { DistrictService } from './services/district/district.service';
import { ProvinceService } from './services/province/province.service';
import { RegionService } from './services/region/region.service';
import { HelpService } from './services/help/help.service';
import { CustomerLevelCharacteristicService } from './services/customerlevelcharacteristic/customerlevelcharacteristic.service';
import { DetailEmailComponent } from './components/help/detail-email/detail-email.component';
import { DetailComplainComponent } from './components/help/detail-complain/detail-complain.component';
import { ManageConsentMarketingComponent } from './components/help/manage-consentmarketing/manage-consent-marketing.component';
import { AudittailListComponent } from './components/audittail/audittail-list/audittail-list.component';
import { AudittailService } from './services/audittail/audittail.service';
import { ContactComponent } from './components/contact/contact/contact.component';
import { ConditionComponent } from './components/condition/condition.component';
import { ContactService } from './services/contact/contact.service';
import { PermissionListComponent } from './components/security/permission-list/permission-list.component';
import { ManagePermissionComponent } from './components/security/manage-permission/manage-permission.component';
import { SecurityService } from './services/security/security.service';
import { DetailTermsconditionComponent } from './components/help/detail-termscondition/detail-termscondition.component';
import { DetailConsentmarketingComponent } from './components/help/detail-consentmarketing/detail-consentmarketing.component';
import { DashboardComponent } from './components/dashboard/dashboard/dashboard.component';
import { PersonalprivacyListComponent } from './components/help/personalprivacy-list/personalprivacy-list.component';
import { PersonalprivacyDetailComponent } from './components/help/personalprivacy-detail/personalprivacy-detail.component';
import { ManagePersonalprivacyComponent } from './components/help/manage-personalprivacy/manage-personalprivacy.component';
import { DriverLevelCharacteristicService } from './services/driverlevelcharacteristic/driverlevelcharacteristic.service';
import { CreateFaqComponent } from './components/help/create-faq/create-faq.component';

const ngxUiLoaderConfig: NgxUiLoaderConfig = {
  bgsColor: '#00a0fa',
  bgsSize: 40,
  bgsType: SPINNER.rectangleBounce, // background spinner type
  fgsType: SPINNER.rectangleBounce, // foreground spinner type
  fgsColor: '#00a0fa',
  fgsSize: 40,
  pbColor: '#323e81',
  pbDirection: PB_DIRECTION.leftToRight, // progress bar direction
  pbThickness: 3, // progress bar thickness,
};
@NgModule({
  declarations: [
    AppComponent,
    AdminLayoutComponent,
    AuthLayoutComponent,
    ChatComponent,
    ImgCroperComponent,
    ContactComponent,
    ConditionComponent,
  ],
  imports: [
    AdminComponentModule,
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    HttpClientModule,
    FormsModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    NgxCoolDialogsModule.forRoot(),
    ReactiveFormsModule,
    NgxRangeModule,
    NgxSummernoteModule,
    // Chart,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    NgxUiLoaderModule.forRoot(ngxUiLoaderConfig),
    NgxSmartModalModule.forRoot(),
    PipeModule
  ],
  providers: [
    AuthenService,
    LoadingService,
    CountryService,
    CarService,
    ProductService,
    UserService,
    OrderingService,
    WelcomeInformationService,
    CorporateTypeService,
    RegisterService,
    HttpService,
    NewsService,
    DeviceDetectorService,
    StorageService,
    ChatService,
    AlertService,
    DriverService,
    DistrictService,
    ProvinceService,
    RegionService,
    AuthGuardService,
    HelpService,
    AudittailService,
    ContactService,
    SecurityService,
    { provide: ErrorHandler, useClass: GlobalErrorService },
    { provide: NgbDateParserFormatter, useValue: 'DD/MM/YYYY' },
  
  ],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class AppModule { }
export function HttpLoaderFactory(http: HttpClient): TranslateHttpLoader {
  return new TranslateHttpLoader(http);
}
