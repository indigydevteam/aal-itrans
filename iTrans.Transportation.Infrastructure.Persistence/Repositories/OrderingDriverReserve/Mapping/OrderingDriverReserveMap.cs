﻿using iTrans.Transportation.Domain.Entities;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using System;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
   public partial class OrderingDriverReserveMap : ClassMapping<Domain.OrderingDriverReserve>
    {
        public OrderingDriverReserveMap()
        {
            Table("Ordering_DriverReserve");
            Lazy(true);
            Id(x => x.Id, map => map.Generator(Generators.Identity));
            Property(x => x.CustomerDesiredPrice);
            Property(x => x.DriverOfferingPrice);
            Property(x => x.IsOrderingDriverOffer);
            Property(x => x.Status);
            Property(x => x.Created);
            Property(x => x.Modified);
            Property(x => x.CreatedBy);
            Property(x => x.ModifiedBy);
            ManyToOne(x => x.Ordering, map => { map.Column("OrderingId"); map.Fetch(FetchKind.Join); map.NotFound(NotFoundMode.Ignore); });
            ManyToOne(x => x.Driver, map => { map.Column("DriverId"); map.Fetch(FetchKind.Select); map.NotFound(NotFoundMode.Ignore); });
            ManyToOne(x => x.DriverAnnouncement, map => { map.Column("DriverAnnouncementId"); map.Fetch(FetchKind.Select); map.NotFound(NotFoundMode.Ignore); });
        }
    }
}