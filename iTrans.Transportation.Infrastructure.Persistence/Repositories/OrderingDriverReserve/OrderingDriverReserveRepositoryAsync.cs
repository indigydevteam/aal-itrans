﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
   public class OrderingDriverReserveRepositoryAsync : GenericRepositoryAsync<Domain.OrderingDriverReserve>, IOrderingDriverReserveRepositoryAsync
    {
        public OrderingDriverReserveRepositoryAsync(ISession session) : base(session)
        { }
    }
}
