﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
    public class DriverPaymentHistoryRepositoryAsync : GenericRepositoryAsync<Domain.DriverPaymentHistory>, IDriverPaymentHistoryRepositoryAsync
    {
        public DriverPaymentHistoryRepositoryAsync(ISession session) : base(session)
        { }
    }
}

