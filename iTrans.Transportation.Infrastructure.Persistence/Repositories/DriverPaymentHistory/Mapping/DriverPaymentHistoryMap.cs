﻿using iTrans.Transportation.Domain;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using System;
namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
    public partial class DriverPaymentHistoryMap : ClassMapping<Domain.DriverPaymentHistory>
    {
        public DriverPaymentHistoryMap()
        {
            Table("Driver_PaymentHistory");
            Lazy(true);
            Id(x => x.Id, map => map.Generator(Generators.Identity));
            //Property(x => x.DriverId, map => { map.NotNullable(true); });
            Property(x => x.Detail); ;
            Property(x => x.Created);
            Property(x => x.Modified);
            Property(x => x.CreatedBy);
            Property(x => x.ModifiedBy);
            ManyToOne(x => x.Payment, map => { map.Column("DriverPaymentId"); map.Fetch(FetchKind.Select); map.NotFound(NotFoundMode.Ignore); });
        }
    }
}
