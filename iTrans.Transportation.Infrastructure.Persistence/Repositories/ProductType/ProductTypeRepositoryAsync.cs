﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using NHibernate;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
    public class ProductTypeRepositoryAsync : GenericRepositoryAsync<Domain.ProductType>, IProductTypeRepositoryAsync
    {
        public ProductTypeRepositoryAsync(ISession session) : base(session)
        { }
    }
}
