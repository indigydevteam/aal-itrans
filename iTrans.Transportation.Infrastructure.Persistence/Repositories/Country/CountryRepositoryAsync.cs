﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using NHibernate;


namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
    public class CountryRepositoryAsync : GenericRepositoryAsync<Domain.Country>, ICountryRepositoryAsync
    {
        public CountryRepositoryAsync(ISession session) : base(session)
        { }
    }
}
