﻿using iTrans.Transportation.Domain;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using System;
namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
    public partial class OrderingPaymentMap : ClassMapping<Domain.OrderingPayment>
    {
        public OrderingPaymentMap()
        {
            Table("Ordering_Payment");
            Lazy(true);
            Id(x => x.Id, map => map.Generator(Generators.Identity));
            Property(x => x.Module, map => { map.NotNullable(true); });
            Property(x => x.UserId, map => { map.NotNullable(true); });
            Property(x => x.Channel, map => { map.NotNullable(true); });
            Property(x => x.Amount);
            Property(x => x.Description); 
            Property(x => x.Created);
            Property(x => x.Modified);
            Property(x => x.CreatedBy);
            Property(x => x.ModifiedBy);
            ManyToOne(x => x.Ordering, map => { map.Column("OrderingId"); map.Fetch(FetchKind.Select); map.NotFound(NotFoundMode.Ignore); });
        }
    }
}
