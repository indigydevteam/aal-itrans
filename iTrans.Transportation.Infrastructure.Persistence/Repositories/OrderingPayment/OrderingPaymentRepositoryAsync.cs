﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using NHibernate;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
    public class OrderingPaymentRepositoryAsync : GenericRepositoryAsync<Domain.OrderingPayment>, IOrderingPaymentRepositoryAsync
    {
        public OrderingPaymentRepositoryAsync(ISession session) : base(session)
        { }
    }
}
