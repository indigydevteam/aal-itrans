﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using NHibernate;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
   public class DriverComplainAndRecommendFileRepositoryAsync : GenericRepositoryAsync<Domain.DriverComplainAndRecommendFile>, IDriverComplainAndRecommendFileRepositoryAsync
    {
        public DriverComplainAndRecommendFileRepositoryAsync(ISession session) : base(session)
        { }
    }
}
