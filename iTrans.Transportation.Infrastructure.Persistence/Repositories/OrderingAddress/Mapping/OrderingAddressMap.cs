﻿using iTrans.Transportation.Domain.Entities;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using System;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories.OrderingAddress.Mapping
{
   public partial class OrderingAddressMap : ClassMapping<Domain.OrderingAddress>
    {
        public OrderingAddressMap()
        {
            Table("Ordering_Address");
            Lazy(true);
            Id(x => x.Id, map => map.Generator(Generators.Identity));
            //Property(x => x.OrderingId);
            Property(x => x.PersonalName);
            Property(x => x.PhoneCode);
            Property(x => x.PhoneNumber);
            Property(x => x.Email);
            Property(x => x.TrackingCode);
            //Property(x => x.Country, map => {map.NotNullable(true);});
            //Property(x => x.Province, map => {map.NotNullable(true);});
            //Property(x => x.District, map => {map.NotNullable(true);});
            //Property(x => x.Subdistrict, map => {map.NotNullable(true);});
            Property(x => x.PostCode, map => {map.Length(50); });
            Property(x => x.Road, map => {map.Length(500); });
            Property(x => x.Alley, map => {map.Length(500); });
            Property(x => x.Address, map => {map.Length(500); });
            Property(x => x.Branch, map => {map.Length(250); });
            Property(x => x.AddressType, map => {map.Length(250); });
            Property(x => x.AddressName, map => {map.Length(250); });
            Property(x => x.Date);
            Property(x => x.Maps);
            Property(x => x.Sequence);
            Property(x => x.Status);
            //Property(x => x.StatusReason);
            Property(x => x.Note);
            Property(x => x.Compensation);
            Property(x => x.ActualReceive);
            Property(x => x.PeriodTime);
            Property(x => x.DriverWaitingToPickUpDate);
            Property(x => x.DriverWaitingToDeliveryDate);
            Property(x => x.UploadNote);
            Property(x => x.NotiAmount);
            Property(x => x.Created);
            Property(x => x.Modified);
            Property(x => x.CreatedBy);
            Property(x => x.ModifiedBy);
            Bag(x => x.AddressProducts, colmap => { colmap.Key(x => x.Column("OrderingAddressId")); colmap.Inverse(true); colmap.Cascade(Cascade.All); }, map => { map.OneToMany(a => a.Class(typeof(Domain.OrderingAddressProduct))); });
            Bag(x => x.Files, colmap => { colmap.Key(x => x.Column("OrderingAddressId")); colmap.Inverse(true); colmap.Cascade(Cascade.Persist); }, map => { map.OneToMany(a => a.Class(typeof(Domain.OrderingAddressFile))); });
            ManyToOne(x => x.Ordering, map => { map.Column("OrderingId"); map.Fetch(FetchKind.Join); map.NotFound(NotFoundMode.Ignore); });
            ManyToOne(x => x.Country, map => { map.Column("CountryId"); map.Fetch(FetchKind.Select); map.NotFound(NotFoundMode.Ignore); });
            ManyToOne(x => x.Province, map => { map.Column("ProvinceId"); map.Fetch(FetchKind.Select); map.NotFound(NotFoundMode.Ignore); });
            ManyToOne(x => x.District, map => { map.Column("DistrictId"); map.Fetch(FetchKind.Select); map.NotFound(NotFoundMode.Ignore); });
            ManyToOne(x => x.Subdistrict, map => { map.Column("SubdistrictId"); map.Fetch(FetchKind.Select); map.NotFound(NotFoundMode.Ignore); });
        }
    }
}