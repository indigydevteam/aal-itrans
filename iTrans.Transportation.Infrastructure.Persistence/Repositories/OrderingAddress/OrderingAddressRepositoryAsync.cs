﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories.OrderingAddress
{
   public class OrderingAddressRepositoryAsync : GenericRepositoryAsync<Domain.OrderingAddress>, IOrderingAddressRepositoryAsync
    {
        public OrderingAddressRepositoryAsync(ISession session) : base(session)
        { }
    }
}