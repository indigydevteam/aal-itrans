﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using NHibernate;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
    public class DriverLevelHistoryRepositoryAsync : GenericRepositoryAsync<Domain.DriverLevelHistory>, IDriverLevelHistoryRepositoryAsync
    {
        public DriverLevelHistoryRepositoryAsync(ISession session) : base(session)
        { }
    }
}
