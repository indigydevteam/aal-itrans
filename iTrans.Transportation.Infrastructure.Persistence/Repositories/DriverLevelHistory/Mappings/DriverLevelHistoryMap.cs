﻿using iTrans.Transportation.Domain;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using System;


namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
    public partial class DriverLevelHistoryMap : ClassMapping<Domain.DriverLevelHistory>
    {
        public DriverLevelHistoryMap()
        {
            Table("Driver_LevelHistory");
            Lazy(true);
            Id(x => x.Id, map => map.Generator(Generators.Identity));
            Property(x => x.DriverId);
            Property(x => x.Level);
            Property(x => x.Star);
            Property(x => x.Rating);
            Property(x => x.AcceptJob);
            Property(x => x.Complaint);
            Property(x => x.Reject);
            Property(x => x.Cancel);
            Property(x => x.InsuranceValue);
            Property(x => x.Created);
            Property(x => x.Modified);
            Property(x => x.CreatedBy);
            Property(x => x.ModifiedBy);
        }
    }
}
