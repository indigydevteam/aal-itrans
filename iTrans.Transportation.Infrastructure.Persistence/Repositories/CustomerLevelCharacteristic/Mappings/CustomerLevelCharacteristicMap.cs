﻿using iTrans.Transportation.Domain;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using System;


namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
    public partial class CustomerLevelCharacteristicMap : ClassMapping<Domain.CustomerLevelCharacteristic>
    {
        public CustomerLevelCharacteristicMap()
        {
            Table("Customer_LevelCharacteristic");
            Lazy(true);
            Id(x => x.Id, map => map.Generator(Generators.Identity));
            Property(x => x.Level);
            Property(x => x.Star);
            Property(x => x.RequestCarPerWeek);
            Property(x => x.RequestCarPerMonth);
            Property(x => x.RequestCarPerYear);
            Property(x => x.CancelPerWeek);
            Property(x => x.CancelPerMonth);
            Property(x => x.CancelPerYear);
            Property(x => x.OrderingValuePerWeek);
            Property(x => x.OrderingValuePerMonth);
            Property(x => x.OrderingValuePerYear);
            Property(x => x.Discount);
            Property(x => x.Fine);
            Property(x => x.IsDelete);
            Property(x => x.Created);
            Property(x => x.Modified);
            Property(x => x.CreatedBy);
            Property(x => x.ModifiedBy);
        }
    }
}