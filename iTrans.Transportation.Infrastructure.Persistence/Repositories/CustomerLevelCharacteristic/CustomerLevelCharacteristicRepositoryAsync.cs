﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using NHibernate;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
    public class CustomerLevelCharacteristicRepositoryAsync : GenericRepositoryAsync<Domain.CustomerLevelCharacteristic>, ICustomerLevelCharacteristicRepositoryAsync
    {
        public CustomerLevelCharacteristicRepositoryAsync(ISession session) : base(session)
        { }
    }
}
