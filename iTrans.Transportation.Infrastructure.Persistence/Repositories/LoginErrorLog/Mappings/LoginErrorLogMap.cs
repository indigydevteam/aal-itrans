﻿using iTrans.Transportation.Domain;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using System;


namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
    public partial class LoginErrorLogMap : ClassMapping<Domain.LoginErrorLog>
    {
        public LoginErrorLogMap()
        {
            Table("Login_Error_Log");
            Lazy(true);
            Id(x => x.Id, map => map.Generator(Generators.Identity));
            Property(x => x.Username);
            Property(x => x.IncorrectTime);
            Property(x => x.UnlockDate);
            Property(x => x.LastModified);
        }
    }
}
