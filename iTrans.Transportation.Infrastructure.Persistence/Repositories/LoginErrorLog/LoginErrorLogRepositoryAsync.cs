﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using NHibernate;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
    public class LoginErrorLogRepositoryAsync : GenericRepositoryAsync<Domain.LoginErrorLog>, ILoginErrorLogRepositoryAsync
    {
        public LoginErrorLogRepositoryAsync(ISession session) : base(session)
        { }
    }
}
