﻿using iTrans.Transportation.Domain;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using System;


namespace iTrans.Transportation.Infrastructure.Persistence.Repositories.OrderingAddressProduct.Mapping
{
   public partial class OrderingAddressProductMap : ClassMapping<Domain.OrderingAddressProduct>
    {
        public OrderingAddressProductMap()
        {
            Table("Ordering_AddressProduct");
            Lazy(true);
            Id(x => x.Id, map => map.Generator(Generators.Identity));
            Property(x => x.TrackingCode);
            
            Property(x => x.Name);
            Property(x => x.ProductType);
            Property(x => x.ProductTypeDetail);
            Property(x => x.Packaging);
            Property(x => x.PackagingDetail);
            Property(x => x.Width);
            Property(x => x.Length);
            Property(x => x.Height);
            Property(x => x.Weight);
            Property(x => x.Quantity);
            Property(x => x.Sequence);
            Property(x => x.Created);
            Property(x => x.Modified);
            Property(x => x.CreatedBy);
            Property(x => x.ModifiedBy);
            ManyToOne(x => x.OrderingProduct, map => { map.Column("OrderingProductId"); map.Fetch(FetchKind.Join); map.Cascade(Cascade.Persist); });
            ManyToOne(x => x.OrderingContainer, map => { map.Column("OrderingContainerId"); map.Fetch(FetchKind.Join); map.Cascade(Cascade.Persist); });
            Bag(x => x.OrderingAddressProductFiles, colmap => { colmap.Key(x => x.Column("OrderingAddressProductId")); colmap.Inverse(true); colmap.Cascade(Cascade.All); }, map => { map.OneToMany(a => a.Class(typeof(Domain.OrderingAddressProductFile))); });
            ManyToOne(x => x.OrderingAddress, map => { map.Column("OrderingAddressId"); map.Fetch(FetchKind.Join); map.Cascade(Cascade.Persist); });
            //ManyToOne(x => x.ProductType, map => { map.Column("ProductTypeId"); map.Fetch(FetchKind.Select); map.NotFound(NotFoundMode.Ignore);});
            //ManyToOne(x => x.Packaging, map => { map.Column("PackagingId"); map.Fetch(FetchKind.Select); map.NotFound(NotFoundMode.Ignore); });
        }
    }
}