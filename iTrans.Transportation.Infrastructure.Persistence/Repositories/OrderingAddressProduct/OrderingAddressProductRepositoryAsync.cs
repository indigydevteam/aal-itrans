﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories.OrderingProduct
{
    class OrderingAddressProductRepositoryAsync : GenericRepositoryAsync<Domain.OrderingAddressProduct>, IOrderingAddressProductRepositoryAsync
    {
        public OrderingAddressProductRepositoryAsync(ISession session) : base(session)
        { }
    }
}