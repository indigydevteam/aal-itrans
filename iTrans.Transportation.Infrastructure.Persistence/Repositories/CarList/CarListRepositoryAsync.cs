﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using NHibernate;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
    public class CarListRepositoryAsync : GenericRepositoryAsync<Domain.CarList>, ICarListRepositoryAsync
    {
        public CarListRepositoryAsync(ISession session) : base(session)
        { }
    }
}
