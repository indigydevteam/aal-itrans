﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using NHibernate;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
   public class CarSpecificationFileRepositoryAsync : GenericRepositoryAsync<Domain.CarSpecificationFile>, ICarSpecificationFileRepositoryAsync
    {
        public CarSpecificationFileRepositoryAsync(ISession session) : base(session)
        { }
    }
}
