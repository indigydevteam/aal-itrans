﻿using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories.RegisterDocument.Mappings
{
    public partial class RegisterInformationMap : ClassMapping<Domain.Entities.RegisterDocument>
    {
        public RegisterInformationMap()
        {
            Table("Register_Document");
            Lazy(true);
            Id(x => x.Id, map => map.Generator(Generators.Identity));
            Property(x => x.Module, map => { map.NotNullable(true); map.Length(50); });
            Property(x => x.Information_TH, map => { map.NotNullable(true); map.Length(50); });
            Property(x => x.Information_ENG, map => { map.NotNullable(true); map.Length(50); });
            Property(x => x.Sequence);
            Property(x => x.Active);
            Property(x => x.Created);
            Property(x => x.Modified);
            Property(x => x.CreatedBy);
            Property(x => x.ModifiedBy);
        }
    }
}
