﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories.RegisterDocument
{
   public class RegisterDocumentRepositoryAsync : GenericRepositoryAsync<Domain.Entities.RegisterDocument>, IRegisterDocumentRepositoryAsync
    {
        public RegisterDocumentRepositoryAsync(ISession session) : base(session)
        { }
    }
}
