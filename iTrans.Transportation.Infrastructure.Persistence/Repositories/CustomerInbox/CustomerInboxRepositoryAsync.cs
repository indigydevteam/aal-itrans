﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using NHibernate;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
    public class CustomerInboxRepositoryAsync : GenericRepositoryAsync<Domain.CustomerInbox>, ICustomerInboxRepositoryAsync
    {
        public CustomerInboxRepositoryAsync(ISession session) : base(session)
        { }
    }
}
