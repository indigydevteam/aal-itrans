﻿using iTrans.Transportation.Domain;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using System;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
   public partial class OrderingMap : ClassMapping<Domain.Ordering>
    {
        public OrderingMap()
        {
            Table("Ordering");
            Lazy(true);
            Id(x => x.Id, map =>
            {
                map.Generator(Generators.Guid);
                map.Column("Id");
                map.UnsavedValue(Guid.Empty);
            });
            Property(x => x.TrackingCode);
            Property(x => x.IsRequestTax);
            Property(x => x.TransportType);
            Property(x => x.ProductCBM);
            Property(x => x.ProductTotalWeight);
            Property(x => x.OrderingPrice);
            Property(x => x.OrderingDesiredPrice);
            Property(x => x.OrderingDriverOffering);
            Property(x => x.IsOrderingDriverOffer);
            Property(x => x.AdditionalDetail);
            Property(x => x.Note);
            Property(x => x.Status);
            //Property(x => x.CancelStatus);
            Property(x => x.CustomerRanking);
            Property(x => x.DriverRanking);
            Property(x => x.TotalDistance);
            Property(x => x.TotalEstimateTime);
            Property(x => x.Distance);
            Property(x => x.EstimateTime);
            Property(x => x.PinnedDate);
            Property(x => x.IsDriverPay);
            Property(x => x.CurrentLocation);
            Property(x => x.GPValue);
            Property(x => x.DriverPayValue);
            Property(x => x.CustomerCancelValue);
            Property(x => x.DriverCancelValue);
            Property(x => x.CustomerCashBack);
            Property(x => x.DriverCashBack);
            Property(x => x.IsMutipleRoutes);
            Property(x => x.Created);
            Property(x => x.Modified);
            Property(x => x.CreatedBy);
            Property(x => x.ModifiedBy);
            Property(x => x.CarRegistration_Str);
            Property(x => x.DriverName_Str);
            Property(x => x.StatusName_Str);
            Property(x => x.CustomerName_Str);
            Property(x => x.PickupPoint_Str);
            Property(x => x.PickupPointDate_Str);
            Property(x => x.RecipientName_Str);
            Property(x => x.DeliveryPoint_Str);
            Property(x => x.DeliveryPointDate_Str);

            ManyToOne(x => x.Customer, map => { map.Column("CustomerId"); map.Fetch(FetchKind.Select); map.NotFound(NotFoundMode.Ignore); });
            ManyToOne(x => x.Driver, map => { map.Column("DriverId"); map.Fetch(FetchKind.Select); map.NotFound(NotFoundMode.Ignore); });
            ManyToOne(x => x.CancelStatus, map => { map.Column("CancelStatusId"); map.Fetch(FetchKind.Select); map.NotFound(NotFoundMode.Ignore); });
            Bag(x => x.Payments, colmap => { colmap.Key(x => x.Column("OrderingId")); colmap.Inverse(true); colmap.Cascade(Cascade.All); }, map => { map.OneToMany(a => a.Class(typeof(Domain.OrderingPayment))); });
            Bag(x => x.Products, colmap => { colmap.Key(x => x.Column("OrderingId")); colmap.Inverse(true); colmap.Cascade(Cascade.All); }, map => { map.OneToMany(a => a.Class(typeof(Domain.OrderingProduct))); });
            Bag(x => x.Cars, colmap => { colmap.Key(x => x.Column("OrderingId")); colmap.Inverse(true); colmap.Cascade(Cascade.All); }, map => { map.OneToMany(a => a.Class(typeof(Domain.OrderingCar))); });
            Bag(x => x.Drivers, colmap => { colmap.Key(x => x.Column("OrderingId")); colmap.Inverse(true); colmap.Cascade(Cascade.All); }, map => { map.OneToMany(a => a.Class(typeof(Domain.OrderingDriver))); });
            Bag(x => x.Addresses, colmap => { colmap.Key(x => x.Column("OrderingId")); colmap.Inverse(true); colmap.Cascade(Cascade.All); }, map => { map.OneToMany(a => a.Class(typeof(Domain.OrderingAddress))); });
            Bag(x => x.Containers, colmap => { colmap.Key(x => x.Column("OrderingId")); colmap.Inverse(true); colmap.Cascade(Cascade.All); }, map => { map.OneToMany(a => a.Class(typeof(Domain.OrderingContainer))); });
            Bag(x => x.Insurances, colmap => { colmap.Key(x => x.Column("OrderingId")); colmap.Inverse(true); colmap.Cascade(Cascade.All); }, map => { map.OneToMany(a => a.Class(typeof(Domain.OrderingInsurance))); });
            Bag(x => x.DriverReserve, colmap => { colmap.Key(x => x.Column("OrderingId")); colmap.Inverse(true); colmap.Cascade(Cascade.All); }, map => { map.OneToMany(a => a.Class(typeof(Domain.OrderingDriverReserve))); });
        }
    }
}