﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
   public class OrderingRepositoryAsync : GenericRepositoryAsync<Domain.Ordering>, IOrderingRepositoryAsync
    {
        public OrderingRepositoryAsync(ISession session) : base(session)
        { }
    }
}