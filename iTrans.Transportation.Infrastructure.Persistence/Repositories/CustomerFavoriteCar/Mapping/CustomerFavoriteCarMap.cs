﻿using iTrans.Transportation.Domain;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using System;
namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
    public partial class CustomerFavoriteCarMap : ClassMapping<Domain.CustomerFavoriteCar>
    {
        public CustomerFavoriteCarMap()
        {
            Table("Customer_FavoriteCar");
            Lazy(true);
            Id(x => x.Id, map => map.Generator(Generators.Identity));
            //Property(x => x.CustomerId);
            //Property(x => x.DriverId);
            //Property(x => x.DriverCarId);
      
            Property(x => x.Created);
            Property(x => x.Modified);
            Property(x => x.CreatedBy);
            Property(x => x.ModifiedBy);

            ManyToOne(x => x.Customer, map => { map.Column("CustomerId"); map.Fetch(FetchKind.Select); map.NotFound(NotFoundMode.Ignore); });
            ManyToOne(x => x.Driver, map => { map.Column("DriverId"); map.Fetch(FetchKind.Select); map.NotFound(NotFoundMode.Ignore); });
            ManyToOne(x => x.Car, map => { map.Column("DriverCarId"); map.Fetch(FetchKind.Select); map.NotFound(NotFoundMode.Ignore); });
        }
    }
}
