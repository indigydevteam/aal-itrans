﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
  public class CustomerFavoriteCarRepositoryAsync : GenericRepositoryAsync<Domain.CustomerFavoriteCar>, ICustomerFavoriteCarRepositoryAsync
    {
        public CustomerFavoriteCarRepositoryAsync(ISession session) : base(session)
        { }
    }
}
