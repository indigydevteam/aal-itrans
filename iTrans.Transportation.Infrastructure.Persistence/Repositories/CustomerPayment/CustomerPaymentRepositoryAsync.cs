﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using NHibernate;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
    public class CustomerPaymentRepositoryAsync : GenericRepositoryAsync<Domain.CustomerPayment>, ICustomerPaymentRepositoryAsync
    {
        public CustomerPaymentRepositoryAsync(ISession session) : base(session)
        { }
    }
}
