﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using NHibernate;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
    public class ProblemTopicRepositoryAsync : GenericRepositoryAsync<Domain.ProblemTopic>, IProblemTopicRepositoryAsync
    {
        public ProblemTopicRepositoryAsync(ISession session) : base(session)
        { }
    }
}
