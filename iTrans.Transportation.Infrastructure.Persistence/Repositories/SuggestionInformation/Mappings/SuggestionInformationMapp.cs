﻿using iTrans.Transportation.Domain;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using System;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
    public partial class SuggestionInformationMapp : ClassMapping<Domain.SuggestionInformation>
    {
        public SuggestionInformationMapp()
        {
            Table("Suggestion_Information");
            Lazy(true);
            Id(x => x.Id, map => map.Generator(Generators.Identity));
            Property(x => x.Title, map => { map.NotNullable(true); map.Length(250); });
            Property(x => x.Title_EN, map => { map.NotNullable(true); map.Length(250); });
            Property(x => x.Detail, map => { map.NotNullable(true);});
            Property(x => x.Detail_EN, map => { map.NotNullable(true);});
            Property(x => x.Picture);
            Property(x => x.QRCode);
            Property(x => x.Created);
            Property(x => x.Modified);
            Property(x => x.CreatedBy);
            Property(x => x.ModifiedBy);
        }
    }
}
