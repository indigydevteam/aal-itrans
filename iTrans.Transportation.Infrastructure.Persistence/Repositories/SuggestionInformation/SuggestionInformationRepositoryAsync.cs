﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using NHibernate;


namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
    public class SuggestionInformationRepositoryAsync : GenericRepositoryAsync<Domain.SuggestionInformation>, ISuggestionInformationRepositoryAsync
    {
        public SuggestionInformationRepositoryAsync(ISession session) : base(session)
        { }
    }
}
