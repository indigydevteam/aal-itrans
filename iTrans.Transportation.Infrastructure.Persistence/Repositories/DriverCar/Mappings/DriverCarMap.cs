﻿using iTrans.Transportation.Domain;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using System;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
    public partial class DriverCarMap : ClassMapping<Domain.DriverCar>
    {
        public DriverCarMap()
        {
            Table("Driver_Car");
            Lazy(true);
            Id(x => x.Id, map => map.Generator(Generators.Identity));
            //Property(x => x.DriverId, map => { map.NotNullable(true); });
            Property(x => x.CarRegistration, map => { map.NotNullable(true); map.Length(50); });
            Property(x => x.ActExpiry, map => { map.NotNullable(true); });
            //Property(x => x.CarTypeId); 
            //Property(x => x.CarListId);
            //Property(x => x.CarDescriptionId);
            Property(x => x.CarDescriptionDetail);
           // Property(x => x.CarFeatureId);
            Property(x => x.CarFeatureDetail);
            // Property(x => x.CarSpecificationId);
            Property(x => x.CarSpecificationDetail );
            Property(x => x.Width);
            Property(x => x.Length);
            Property(x => x.Height);
            Property(x => x.Temperature);
            Property(x => x.DriverName );
            Property(x => x.DriverPhoneCode, map => {  map.Length(50); });
            Property(x => x.DriverPhoneNumber, map => {   map.Length(50); });
            Property(x => x.DriverIdentityNumber, map => { map.Length(50); });
            Property(x => x.ProductInsurance);
            Property(x => x.ProductInsuranceAmount);
            Property(x => x.AllLocation);
            Property(x => x.Status);
            Property(x => x.Note);
            Property(x => x.Created);
            Property(x => x.Modified);
            Property(x => x.CreatedBy);
            Property(x => x.ModifiedBy);
            Bag(x => x.CarFiles, colmap => { colmap.Key(x => x.Column("DriverCarId")); colmap.Inverse(true); colmap.Cascade(Cascade.Persist); }, map => { map.OneToMany(a => a.Class(typeof(DriverCarFile))); });
            Bag(x => x.Locations, colmap => { colmap.Key(x => x.Column("DriverCarId")); colmap.Inverse(true); colmap.Cascade(Cascade.Persist); }, map => { map.OneToMany(a => a.Class(typeof(DriverCarLocation))); });
            ManyToOne(x => x.Driver, map => { map.Column("DriverId"); map.Fetch(FetchKind.Select); map.NotFound(NotFoundMode.Ignore); });
            ManyToOne(x => x.Owner, map => { map.Column("OwnerId"); map.Fetch(FetchKind.Select); map.NotFound(NotFoundMode.Ignore); });
            ManyToOne(x => x.CarType, map => { map.Column("CarTypeId");   map.Fetch(FetchKind.Select); map.NotFound(NotFoundMode.Ignore); });
            ManyToOne(x => x.CarList, map => { map.Column("CarListId");  map.Fetch(FetchKind.Select); map.NotFound(NotFoundMode.Ignore); });
            ManyToOne(x => x.CarDescription, map => { map.Column("CarDescriptionId");   map.Fetch(FetchKind.Select); map.NotFound(NotFoundMode.Ignore); });
            ManyToOne(x => x.CarFeature, map => { map.Column("CarFeatureId");   map.Fetch(FetchKind.Select); map.NotFound(NotFoundMode.Ignore); });
            ManyToOne(x => x.CarSpecification, map => { map.Column("CarSpecificationId");   map.Fetch(FetchKind.Select); map.NotFound(NotFoundMode.Ignore); });
            ManyToOne(x => x.EnergySavingDevice, map => { map.Column("EnergySavingDeviceId"); map.Fetch(FetchKind.Join); map.NotFound(NotFoundMode.Ignore); });
        }
    }
}
