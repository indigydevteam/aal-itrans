﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using NHibernate;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories 
{
    public class DriverCarRepositoryAsync : GenericRepositoryAsync<Domain.DriverCar>, IDriverCarRepositoryAsync
    {
        public DriverCarRepositoryAsync(ISession session) : base(session)
        { }
    }
}
