﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories.OrderingAddressHistory
{
  public  class OrderingAddressHistoryRepositoryAsync : GenericRepositoryAsync<Domain.Entities.OrderingAddressHistory>, IOrderingAddressHistoryRepositoryAsync
    {
        public OrderingAddressHistoryRepositoryAsync(ISession session) : base(session)
        { }
    }
}
