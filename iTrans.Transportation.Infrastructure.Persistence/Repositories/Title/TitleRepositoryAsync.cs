﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using NHibernate;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
    public class TitleRepositoryAsync : GenericRepositoryAsync<Domain.Title>, ITitleRepositoryAsync
    {
        public TitleRepositoryAsync(ISession session) : base(session)
        { }
    }
}
