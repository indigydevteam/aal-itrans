﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories.RoleMenu
{
    public class RoleMenuRepositoryAsync : GenericRepositoryAsync<Domain.Entities.RoleMenu>, IRoleMenuRepositoryAsync
    {
        public RoleMenuRepositoryAsync(ISession session) : base(session)
        {
        }
    }
}
