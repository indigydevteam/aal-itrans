﻿using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories.RoleMenu.Mappings
{
    public class RoleMenuMap : ClassMapping<Domain.Entities.RoleMenu>
    {
        public RoleMenuMap()
        {
            {
                Table("RoleMenu");
                Lazy(true);
                Id(x => x.Id, map => map.Generator(Generators.Identity));
                Property(x => x.RoleId);
                Property(x => x.MenuId);
                //Property(x => x.MenuPermissionId);
                Property(x => x.Created);
                Property(x => x.Modified);
                Property(x => x.CreatedBy);
                Property(x => x.ModifiedBy);
                ManyToOne(x => x.MenuPermission, map => { map.Column("MenuPermissionId"); map.Fetch(FetchKind.Select); map.NotFound(NotFoundMode.Ignore); });

            }
        }
    }
}
