﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
    public class CustomerProblemFileRepositoryAsync : GenericRepositoryAsync<Domain.CustomerProblemFile>, ICustomerProblemFileRepositoryAsync
    {
        public CustomerProblemFileRepositoryAsync(ISession session) : base(session)
        { }
    }
}
