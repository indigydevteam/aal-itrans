﻿using iTrans.Transportation.Domain.Entities;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using System;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories.OrderingDriver.Mapping
{
   public partial class OrderingDriverMap : ClassMapping<Domain.OrderingDriver>
    {
        public OrderingDriverMap()
        {
            Table("Ordering_Driver");
            Lazy(true);
            Id(x => x.Id, map => map.Generator(Generators.Identity));
            Property(x => x.DriverId);
            Property(x => x.DriverType);
            Property(x => x.FirstName);
            Property(x => x.MiddleName);
            Property(x => x.LastName);
            Property(x => x.Name);
            Property(x => x.IdentityNumber);
            Property(x => x.ContactPersonFirstName);
            Property(x => x.ContactPersonMiddleName);
            Property(x => x.ContactPersonLastName);
            Property(x => x.PhoneCode);
            Property(x => x.PhoneNumber);
            Property(x => x.Email);
            Property(x => x.Facbook, map => { map.Length(250); });
            Property(x => x.Line, map => { map.Length(250); });
            Property(x => x.Twitter, map => { map.Length(250); });
            Property(x => x.Whatapp, map => { map.Length(250); });
            Property(x => x.Level);
            Property(x => x.Corparate);
            Property(x => x.Star);
            Property(x => x.Rating);
            Property(x => x.Grade);
            Property(x => x.Created);
            Property(x => x.Modified);
            Property(x => x.CreatedBy);
            Property(x => x.ModifiedBy);
            Bag(x => x.DriverFiles, colmap => { colmap.Key(x => x.Column("OrderingDriverId")); colmap.Inverse(true); colmap.Cascade(Cascade.All); }, map => { map.OneToMany(a => a.Class(typeof(Domain.OrderingDriverFile))); });
            ManyToOne(x => x.Ordering, map => { map.Column("OrderingId"); map.Fetch(FetchKind.Join); map.NotFound(NotFoundMode.Ignore); });
            ManyToOne(x => x.Title, map => { map.Column("TitleId"); map.Fetch(FetchKind.Select); map.NotFound(NotFoundMode.Ignore); });
            ManyToOne(x => x.ContactPersonTitle, map => { map.Column("ContactPersonTitleId"); map.Fetch(FetchKind.Select); map.NotFound(NotFoundMode.Ignore); });
            ManyToOne(x => x.CorporateType, map => { map.Column("CorporateTypeId"); map.Fetch(FetchKind.Select); map.NotFound(NotFoundMode.Ignore); });
        }
    }
}