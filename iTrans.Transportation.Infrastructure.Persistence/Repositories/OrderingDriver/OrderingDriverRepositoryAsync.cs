﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories.OrderingDriver
{
   public class OrderingDriverRepositoryAsync : GenericRepositoryAsync<Domain.OrderingDriver>, IOrderingDriverRepositoryAsync
    {
        public OrderingDriverRepositoryAsync(ISession session) : base(session)
        { }
    }
}
