﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using NHibernate;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
   public class CarFeatureFileRepositoryAsync : GenericRepositoryAsync<Domain.CarFeatureFile>, ICarFeatureFileRepositoryAsync
    {
        public CarFeatureFileRepositoryAsync(ISession session) : base(session)
        { }
    }
}
