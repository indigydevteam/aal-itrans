﻿using iTrans.Transportation.Domain;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using System;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
   public partial class DriverTermAndConditionMap : ClassMapping<Domain.DriverTermAndCondition>
    {
        public DriverTermAndConditionMap()
        {
            Table("Driver_TermAndCondition");
            Lazy(true);
            Id(x => x.Id, map => map.Generator(Generators.Identity));
            Property(x => x.TermAndConditionId, map => { map.NotNullable(true); });
            Property(x => x.Name_TH);
            Property(x => x.Name_ENG);
            Property(x => x.Section);
            Property(x => x.Version);
            Property(x => x.IsAccept);
            Property(x => x.IsUserAccept);
            Property(x => x.Created);
            Property(x => x.Modified);
            Property(x => x.CreatedBy);
            Property(x => x.ModifiedBy);
            ManyToOne(x => x.Driver, map => { map.Column("DriverId"); map.Fetch(FetchKind.Select); map.NotFound(NotFoundMode.Ignore); });
        }
    }
}
