﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
   public class DriverTermAndConditionRepositoryAsync : GenericRepositoryAsync<Domain.DriverTermAndCondition>, IDriverTermAndConditionRepositoryAsync
    {
        public DriverTermAndConditionRepositoryAsync(ISession session) : base(session)
        { }
    }
}
