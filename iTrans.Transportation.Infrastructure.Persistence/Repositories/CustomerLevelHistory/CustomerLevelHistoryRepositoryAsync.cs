﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using NHibernate;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
    public class CustomerLevelHistoryRepositoryAsync : GenericRepositoryAsync<Domain.CustomerLevelHistory>, ICustomerLevelHistoryRepositoryAsync
    {
        public CustomerLevelHistoryRepositoryAsync(ISession session) : base(session)
        { }
    }
}
