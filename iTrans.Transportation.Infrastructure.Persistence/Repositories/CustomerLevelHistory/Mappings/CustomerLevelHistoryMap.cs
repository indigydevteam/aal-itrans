﻿using iTrans.Transportation.Domain;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using System;


namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
    public partial class CustomerLevelHistoryMap : ClassMapping<Domain.CustomerLevelHistory>
    {
        public CustomerLevelHistoryMap()
        {
            Table("Customer_LevelHistory");
            Lazy(true);
            Id(x => x.Id, map => map.Generator(Generators.Identity));
            Property(x => x.CustomerId);
            Property(x => x.Level);
            Property(x => x.Star);
            Property(x => x.Rating);
            Property(x => x.RequestCar);
            Property(x => x.Cancel);
            Property(x => x.OrderingValue);
            Property(x => x.Created);
            Property(x => x.Modified);
            Property(x => x.CreatedBy);
            Property(x => x.ModifiedBy);
        }
    }
}
