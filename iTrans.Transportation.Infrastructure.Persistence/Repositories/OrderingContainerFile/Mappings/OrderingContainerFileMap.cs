﻿using iTrans.Transportation.Domain;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using System;
namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
    public partial class OrderingContainerFileMap : ClassMapping<Domain.OrderingContainerFile>
    {
        public OrderingContainerFileMap()
        {
            Table("Ordering_ContainerFile");
            Lazy(true);
            Id(x => x.Id, map => map.Generator(Generators.Identity));
            //Property(x => x.OrderingCarId, map => { map.NotNullable(true); });
            Property(x => x.FileName);
            Property(x => x.ContentType);
            Property(x => x.FilePath);
            Property(x => x.DirectoryPath);
            Property(x => x.DocumentType);
            Property(x => x.FileEXT);
            Property(x => x.Sequence);
            Property(x => x.Created);
            Property(x => x.Modified);
            Property(x => x.CreatedBy);
            Property(x => x.ModifiedBy);
            ManyToOne(x => x.OrderingContainer, map => { map.Column("OrderingContainerId"); map.Fetch(FetchKind.Join); map.NotFound(NotFoundMode.Ignore); });
        }
    }
}
