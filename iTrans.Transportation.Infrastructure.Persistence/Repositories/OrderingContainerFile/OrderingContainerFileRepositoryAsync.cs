﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using NHibernate;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
   public class OrderingContainerFileRepositoryAsync : GenericRepositoryAsync<Domain.OrderingContainerFile>, IOrderingContainerFileRepositoryAsync
    {
        public OrderingContainerFileRepositoryAsync(ISession session) : base(session)
        { }
    }
}
