﻿using iTrans.Transportation.Domain;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using System;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
    public partial class DriverAnnouncementCarMap : ClassMapping<Domain.DriverAnnouncementCar>
    {
        public DriverAnnouncementCarMap()
        {
            Table("Driver_AnnouncementCar");
            Lazy(true);
            Id(x => x.Id, map => map.Generator(Generators.Identity));
            Property(x => x.CarRegistration, map => { map.NotNullable(true); map.Length(50); });
            Property(x => x.ActExpiry, map => { map.NotNullable(true); });
            Property(x => x.CarDescriptionDetail, map => { map.NotNullable(true); });
            Property(x => x.CarFeatureDetail, map => { map.NotNullable(true); });
            Property(x => x.CarSpecificationDetail, map => { map.NotNullable(true); });
            Property(x => x.Width);
            Property(x => x.Length);
            Property(x => x.Height);
            Property(x => x.DriverName, map => { map.NotNullable(true); map.Length(250); });
            Property(x => x.DriverPhoneCode, map => { map.NotNullable(true); map.Length(50); });
            Property(x => x.DriverPhoneNumber, map => { map.NotNullable(true); map.Length(50); });
            Property(x => x.DriverIdentityNumber, map => { map.NotNullable(true); map.Length(50); });
            Property(x => x.ProductInsurance);
            Property(x => x.ProductInsuranceAmount);
            Property(x => x.AllLocation);
            Property(x => x.Note);
            Property(x => x.Created);
            Property(x => x.Modified);
            Property(x => x.CreatedBy);
            Property(x => x.ModifiedBy);
            Bag(x => x.CarFiles, colmap => { colmap.Key(x => x.Column("DriverAnnouncementCarId")); colmap.Inverse(true); colmap.Cascade(Cascade.Persist); }, map => { map.OneToMany(a => a.Class(typeof(DriverAnnouncementCarFile))); });
            ManyToOne(x => x.DriverAnnouncement, map => { map.Column("DriverAnnouncementId"); map.Fetch(FetchKind.Select); map.NotFound(NotFoundMode.Ignore); });
            ManyToOne(x => x.CarType, map => { map.Column("CarTypeId");   map.Fetch(FetchKind.Select); map.NotFound(NotFoundMode.Ignore); });
            ManyToOne(x => x.CarList, map => { map.Column("CarListId");  map.Fetch(FetchKind.Select); map.NotFound(NotFoundMode.Ignore); });
            ManyToOne(x => x.CarDescription, map => { map.Column("CarDescriptionId");   map.Fetch(FetchKind.Select); map.NotFound(NotFoundMode.Ignore); });
            ManyToOne(x => x.CarFeature, map => { map.Column("CarFeatureId");   map.Fetch(FetchKind.Select); map.NotFound(NotFoundMode.Ignore); });
            ManyToOne(x => x.CarSpecification, map => { map.Column("CarSpecificationId");   map.Fetch(FetchKind.Select); map.NotFound(NotFoundMode.Ignore); });
        }
    }
}
