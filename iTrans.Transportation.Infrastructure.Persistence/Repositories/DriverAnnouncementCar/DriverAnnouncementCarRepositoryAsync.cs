﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using NHibernate;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories 
{
    public class DriverAnnouncementCarRepositoryAsync : GenericRepositoryAsync<Domain.DriverAnnouncementCar>, IDriverAnnouncementCarRepositoryAsync
    {
        public DriverAnnouncementCarRepositoryAsync(ISession session) : base(session)
        { }
    }
}
