﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using NHibernate;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
   public class DriverCarFileRepositoryAsync : GenericRepositoryAsync<Domain.DriverCarFile>, IDriverCarFileRepositoryAsync
    {
        public DriverCarFileRepositoryAsync(ISession session) : base(session)
        { }
    }
}
