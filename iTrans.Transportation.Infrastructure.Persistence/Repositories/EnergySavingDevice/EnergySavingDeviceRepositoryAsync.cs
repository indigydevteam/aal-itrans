﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
    public class EnergySavingDeviceRepositoryAsync : GenericRepositoryAsync<Domain.EnergySavingDevice>, IEnergySavingDeviceRepositoryAsync
    {
        public EnergySavingDeviceRepositoryAsync(ISession session) : base(session)
        { }
    }
}
