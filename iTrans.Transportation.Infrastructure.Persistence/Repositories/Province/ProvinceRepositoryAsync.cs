﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using NHibernate;


namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
    public class ProvinceRepositoryAsync : GenericRepositoryAsync<Domain.Province>, IProvinceRepositoryAsync
    {
        public ProvinceRepositoryAsync(ISession session) : base(session)
        { }
    }
}
