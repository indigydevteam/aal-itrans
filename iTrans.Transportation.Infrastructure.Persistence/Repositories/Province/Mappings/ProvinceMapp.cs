﻿using iTrans.Transportation.Domain;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using System;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
    public partial class ProvinceMapp : ClassMapping<Domain.Province>
    {
        public ProvinceMapp()
        {
            Table("Province");
            Lazy(true);
            Id(x => x.Id, map => map.Generator(Generators.Identity));
            //Property(x => x.CountryId, map => { map.NotNullable(true); });
            //Property(x => x.RegionId, map => { map.NotNullable(true); });
            Property(x => x.Name_TH, map => { map.NotNullable(true); map.Length(50); });
            Property(x => x.Name_ENG, map => { map.NotNullable(true); map.Length(50); });
            Property(x => x.Active);
            Property(x => x.Created);
            Property(x => x.Modified);
            Property(x => x.CreatedBy);
            Property(x => x.ModifiedBy);
            ManyToOne(x => x.Country, map => { map.Column("CountryId"); map.Fetch(FetchKind.Select); map.NotFound(NotFoundMode.Ignore); });
            ManyToOne(x => x.Region, map => { map.Column("RegionId"); map.Fetch(FetchKind.Select); map.NotFound(NotFoundMode.Ignore); });
        }
    }
}
