﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using NHibernate;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
   public class CountryFileRepositoryAsync : GenericRepositoryAsync<Domain.CountryFile>, ICountryFileRepositoryAsync
    {
        public CountryFileRepositoryAsync(ISession session) : base(session)
        { }
    }
}
