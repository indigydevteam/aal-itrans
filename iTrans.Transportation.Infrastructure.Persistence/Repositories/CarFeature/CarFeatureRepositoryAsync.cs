﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using NHibernate;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
    public class CarFeatureRepositoryAsync : GenericRepositoryAsync<Domain.CarFeature>, ICarFeatureRepositoryAsync
    {
        public CarFeatureRepositoryAsync(ISession session) : base(session)
        { }
    }
}
