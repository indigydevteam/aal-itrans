﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using NHibernate;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
    public class CorporateTypeRepositoryAsync : GenericRepositoryAsync<Domain.CorporateType>, ICorporateTypeRepositoryAsync
    {
        public CorporateTypeRepositoryAsync(ISession session) : base(session)
        { }
    }
}
