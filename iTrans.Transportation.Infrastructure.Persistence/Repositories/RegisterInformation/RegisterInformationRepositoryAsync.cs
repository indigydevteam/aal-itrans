﻿using System;
using System.Collections.Generic;
using System.Text;
using iTrans.Transportation.Application.Interfaces.Repositories;
using NHibernate;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories.RegisterInformation
{
   public class RegisterInformationRepositoryAsync : GenericRepositoryAsync<Domain.Entities.RegisterInformation>, IRegisterInformationRepositoryAsync
    {
        public RegisterInformationRepositoryAsync(ISession session) : base(session)
        { }
    }
}