﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories.CustomerProblem
{
   public class CustomerProblemRepositoryAsync : GenericRepositoryAsync<Domain.CustomerProblem>, ICustomerProblemRepositoryAsync
    {
        public CustomerProblemRepositoryAsync(ISession session) : base(session)
        { }
    }
}
