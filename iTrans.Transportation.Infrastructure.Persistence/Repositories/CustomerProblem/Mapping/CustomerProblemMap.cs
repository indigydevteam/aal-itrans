﻿using iTrans.Transportation.Domain;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using System;
namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
    public partial class CustomerProblemMap : ClassMapping<Domain.CustomerProblem>
    {
        public CustomerProblemMap()
        {
            Table("Customer_Problem");
            Lazy(true);
            Id(x => x.Id, map => map.Generator(Generators.Identity));
            //Property(x => x.ProblemTopicId, map => { map.NotNullable(true); });
            Property(x => x.Message, map => { map.NotNullable(true); });
            Property(x => x.Email, map => { map.NotNullable(true); });
            Property(x => x.Created);
            Property(x => x.Modified);
            Property(x => x.CreatedBy);
            Property(x => x.ModifiedBy);
            ManyToOne(x => x.Customer, map => { map.Column("CustomerId"); map.Fetch(FetchKind.Select); map.NotFound(NotFoundMode.Ignore); });
            ManyToOne(x => x.ProblemTopic, map => { map.Column("ProblemTopicId"); map.Fetch(FetchKind.Select); map.NotFound(NotFoundMode.Ignore); });
            Bag(x => x.Files, colmap => { colmap.Key(x => x.Column("CustomerProblemId")); colmap.Inverse(true); colmap.Cascade(Cascade.Persist); }, map => { map.OneToMany(a => a.Class(typeof(CustomerProblemFile))); });
        }
    }
}
