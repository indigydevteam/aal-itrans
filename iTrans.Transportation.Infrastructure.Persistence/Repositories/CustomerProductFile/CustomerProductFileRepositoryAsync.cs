﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using NHibernate;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
   public class CustomerProductFileRepositoryAsync : GenericRepositoryAsync<Domain.CustomerProductFile>, ICustomerProductFileRepositoryAsync
    {
        public CustomerProductFileRepositoryAsync(ISession session) : base(session)
        { }
    }
}
