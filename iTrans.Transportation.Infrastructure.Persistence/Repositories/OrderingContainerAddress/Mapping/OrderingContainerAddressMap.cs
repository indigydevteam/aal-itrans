﻿using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories.OrderingContainerAddress.Mapping
{
  public partial  class OrderingContainerAddressMap : ClassMapping<Domain.OrderingContainerAddress>
    {
        public OrderingContainerAddressMap()
        {
            Table("Ordering_ContainerAddress");
            Lazy(true);
            Id(x => x.Id, map => map.Generator(Generators.Identity));
            //Property(x => x.OrderingContainerId, map => { map.NotNullable(true); });
            Property(x => x.LoadingPostCode, map => { map.Length(50); });
            Property(x => x.LoadingRoad, map => {map.Length(250); });
            Property(x => x.LoadingAlley, map => {map.Length(250); });
            Property(x => x.LoadingAddress, map => {map.Length(250); });
            Property(x => x.LoadingBranch, map => {map.Length(50); });
            Property(x => x.LoadingAddressType, map => {  map.Length(50); });
            Property(x => x.LoadingAddressName, map => { map.Length(550); });
            Property(x => x.ETD);
            Property(x => x.LoadingMaps, map => { map.Length(250);});
            Property(x => x.DestinationPostCode, map => { map.Length(50); });
            Property(x => x.DestinationRoad, map => { map.Length(250); });
            Property(x => x.DestinationAlley, map => { map.Length(250); });
            Property(x => x.DestinationAddress, map => { map.Length(250); });
            Property(x => x.DestinationBranch, map => { map.Length(50); });
            Property(x => x.DestinationAddressType, map => { map.Length(50); });
            Property(x => x.DestinationAddressName, map => { map.Length(550); });
            Property(x => x.ETA);
            Property(x => x.DestinationMaps, map => { map.Length(250); });
            Property(x => x.FeederVessel);
            Property(x => x.TStime);
            Property(x => x.MotherVessel);
            Property(x => x.CPSDate);
            Property(x => x.CPSDetail);
            Property(x => x.CYDate);
            Property(x => x.CYDetail);
            Property(x => x.ReturnDate);
            Property(x => x.ReturnDetail);
            Property(x => x.ClosingTime);
            Property(x => x.CutOffVGMTime);
            Property(x => x.CutOffSITime);
            Property(x => x.Agent);
            Property(x => x.Created);
            Property(x => x.Modified);
            Property(x => x.CreatedBy);
            Property(x => x.ModifiedBy);
            ManyToOne(x => x.LoadingCountry, map => { map.Column("LoadingCountryId"); map.Fetch(FetchKind.Select); map.NotFound(NotFoundMode.Ignore); });
            ManyToOne(x => x.LoadingProvince, map => { map.Column("LoadingProvinceId"); map.Fetch(FetchKind.Select); map.NotFound(NotFoundMode.Ignore); });
            ManyToOne(x => x.LoadingDistrict, map => { map.Column("LoadingDistrictId"); map.Fetch(FetchKind.Select); map.NotFound(NotFoundMode.Ignore); });
            ManyToOne(x => x.LoadingSubdistrict, map => { map.Column("LoadingSubdistrictId"); map.Fetch(FetchKind.Select); map.NotFound(NotFoundMode.Ignore); });
            ManyToOne(x => x.DestinationCountry, map => { map.Column("DestinationCountryId"); map.Fetch(FetchKind.Select); map.NotFound(NotFoundMode.Ignore); });
            ManyToOne(x => x.DestinationProvince, map => { map.Column("DestinationProvinceId"); map.Fetch(FetchKind.Select); map.NotFound(NotFoundMode.Ignore); });
            ManyToOne(x => x.DestinationDistrict, map => { map.Column("DestinationDistrictId"); map.Fetch(FetchKind.Select); map.NotFound(NotFoundMode.Ignore); });
            ManyToOne(x => x.DestinationSubdistrict, map => { map.Column("DestinationSubdistrictId"); map.Fetch(FetchKind.Select); map.NotFound(NotFoundMode.Ignore); });
            ManyToOne(x => x.OrderingContainer, map => { map.Column("OrderingContainerId"); map.Fetch(FetchKind.Join); map.Cascade(Cascade.Persist); });
        }
    }
}