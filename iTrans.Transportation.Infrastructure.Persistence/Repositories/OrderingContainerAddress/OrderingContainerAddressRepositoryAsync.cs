﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories.OrderingContainerAddress
{
   public class OrderingContainerAddressRepositoryAsync : GenericRepositoryAsync<Domain.OrderingContainerAddress>, IOrderingContainerAddressRepositoryAsync
    {
        public OrderingContainerAddressRepositoryAsync(ISession session) : base(session)
        { }
    }
}
