﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using NHibernate;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
    public class CustomerProductRepositoryAsync : GenericRepositoryAsync<Domain.CustomerProduct>, ICustomerProductRepositoryAsync
    {
        public CustomerProductRepositoryAsync(ISession session) : base(session)
        { }
    }
}
