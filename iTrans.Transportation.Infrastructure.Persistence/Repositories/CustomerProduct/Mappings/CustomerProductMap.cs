﻿using iTrans.Transportation.Domain;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using System;


namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
    public partial class CustomerProductMap : ClassMapping<Domain.CustomerProduct>
    {
        public CustomerProductMap()
        {
            Table("Customer_Product");
            Lazy(true);
            Id(x => x.Id, map => map.Generator(Generators.Identity));
            Property(x => x.CustomerId, map => { map.NotNullable(true); });
            Property(x => x.Name, map => { map.NotNullable(true); map.Length(250); });
            //Property(x => x.ProductTypeId, map => { map.NotNullable(true);  });
            Property(x => x.ProductTypeDetail);
            //Property(x => x.PackagingId, map => { map.NotNullable(true); });
            Property(x => x.PackagingDetail);
            Property(x => x.Width);
            Property(x => x.Length);
            Property(x => x.Height);
            Property(x => x.Weight);
            Property(x => x.Quantity);
            Property(x => x.Created);
            Property(x => x.Modified);
            Property(x => x.CreatedBy);
            Property(x => x.ModifiedBy);
            Bag(x => x.ProductFiles, colmap => { colmap.Key(x => x.Column("CustomerProductId")); colmap.Inverse(true); colmap.Cascade(Cascade.Persist); }, map => { map.OneToMany(a => a.Class(typeof(CustomerProductFile))); });
            ManyToOne(x => x.ProductType, map => { map.Column("ProductTypeId");  map.Fetch(FetchKind.Select); map.NotFound(NotFoundMode.Ignore); });
            ManyToOne(x => x.Packaging, map => { map.Column("PackagingId"); map.Fetch(FetchKind.Select); map.NotFound(NotFoundMode.Ignore); });
        }
    }
}
