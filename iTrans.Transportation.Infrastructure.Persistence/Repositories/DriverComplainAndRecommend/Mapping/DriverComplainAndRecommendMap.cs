﻿using iTrans.Transportation.Domain;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories.DriverComplainAndRecommend.Mapping
{
  public partial  class DriverComplainAndRecommendMap : ClassMapping<Domain.Entities.DriverComplainAndRecommend>
    {
        public DriverComplainAndRecommendMap()
        {
            Table("Driver_ComplainAndRecommend");
            Lazy(true);
            Id(x => x.Id, map => map.Generator(Generators.Identity));
            //Property(x => x.DriverId, map => { map.NotNullable(true);});
            Property(x => x.Title , map => {  map.Length(250); });
            Property(x => x.Detail );   
            Property(x => x.Created);
            Property(x => x.Modified);
            Property(x => x.CreatedBy);
            Property(x => x.ModifiedBy);
            ManyToOne(x => x.Driver, map => { map.Column("DriverId"); map.Fetch(FetchKind.Select); map.NotFound(NotFoundMode.Ignore); });
            Bag(x => x.Files, colmap => { colmap.Key(x => x.Column("DriverComplainAndRecommendId")); colmap.Inverse(true); colmap.Cascade(Cascade.Persist); }, map => { map.OneToMany(a => a.Class(typeof(DriverComplainAndRecommendFile))); });
        }
    }
}
