﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories.DriverComplainAndRecommend
{
    public class DriverComplainAndRecommendRepositoryAsync : GenericRepositoryAsync<Domain.Entities.DriverComplainAndRecommend>, IDriverComplainAndRecommendRepositoryAsync
    {
        public DriverComplainAndRecommendRepositoryAsync(ISession session) : base(session)
        { }
    }
}
