﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using NHibernate;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
   public class CarTypeFileRepositoryAsync : GenericRepositoryAsync<Domain.CarTypeFile>, ICarTypeFileRepositoryAsync
    {
        public CarTypeFileRepositoryAsync(ISession session) : base(session)
        { }
    }
}
