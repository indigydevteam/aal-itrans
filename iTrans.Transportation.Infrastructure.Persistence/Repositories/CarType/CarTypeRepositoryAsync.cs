﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using NHibernate;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
    public class CarTypeRepositoryAsync : GenericRepositoryAsync<Domain.CarType>, ICarTypeRepositoryAsync
    {
        public CarTypeRepositoryAsync(ISession session) : base(session)
        { }
    }
}
