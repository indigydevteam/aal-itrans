﻿using iTrans.Transportation.Domain;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using System;


namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
    public partial class CarTypeMap : ClassMapping<Domain.CarType>
    {
        public CarTypeMap()
        {
            Table("Car_Type");
            Lazy(true);
            Id(x => x.Id, map => map.Generator(Generators.Identity));
            Property(x => x.Name_TH, map => { map.NotNullable(true); map.Length(50); });
            Property(x => x.Name_ENG, map => { map.NotNullable(true); map.Length(50); });
            Property(x => x.IsTemperatureInfo);
            Property(x => x.IsContainer);
            Property(x => x.Sequence);
            Property(x => x.Active);
            Property(x => x.IsDelete);
            Property(x => x.Created);
            Property(x => x.Modified);
            Property(x => x.CreatedBy);
            Property(x => x.ModifiedBy);
            OneToOne(x => x.File, map =>
            {
                map.PropertyReference(typeof(CarTypeFile).GetProperty("CarType"));
                map.Cascade(Cascade.All);
            });
        }
    }
}
