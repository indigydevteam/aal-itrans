﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using NHibernate;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
   public class ProductPackagingFileRepositoryAsync : GenericRepositoryAsync<Domain.ProductPackagingFile>, IProductPackagingFileRepositoryAsync
    {
        public ProductPackagingFileRepositoryAsync(ISession session) : base(session)
        { }
    }
}
