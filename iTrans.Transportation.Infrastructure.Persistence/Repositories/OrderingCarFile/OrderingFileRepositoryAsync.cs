﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using NHibernate;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
   public class OrderingCarFileRepositoryAsync : GenericRepositoryAsync<Domain.OrderingCarFile>, IOrderingCarFileRepositoryAsync
    {
        public OrderingCarFileRepositoryAsync(ISession session) : base(session)
        { }
    }
}
