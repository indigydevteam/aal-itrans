﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories.DriverAnnouncementLocation
{
   public class DriverAnnouncementLocationRepositoryAsync : GenericRepositoryAsync<Domain.DriverAnnouncementLocation>, IDriverAnnouncementLocationRepositoryAsync
    {
        public DriverAnnouncementLocationRepositoryAsync(ISession session) : base(session)
        { }
    }
}
