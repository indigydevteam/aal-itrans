﻿using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories.DriverAnnouncementLocation.Mapping
{
   public partial class DriverAnnouncementLocationMap : ClassMapping<Domain.DriverAnnouncementLocation>
    {
        public DriverAnnouncementLocationMap()
        {
            Table("Driver_AnnouncementLocation");
            Lazy(true);
            Id(x => x.Id, map => map.Generator(Generators.Identity));
            //Property(x => x.DriverAnnouncementId, map => { map.NotNullable(true); });
            Property(x => x.LocationType);
            Property(x => x.Note);
            Property(x => x.Sequence);
            Property(x => x.SendDate);
            Property(x => x.ReceiveDate);
            Property(x => x.Created);
            Property(x => x.Modified);
            Property(x => x.CreatedBy);
            Property(x => x.ModifiedBy);
            ManyToOne(x => x.DriverAnnouncement, map => { map.Column("DriverAnnouncementId"); map.Cascade(Cascade.All); map.Fetch(FetchKind.Select); });
            ManyToOne(x => x.Country, map => { map.Column("CountryId");  map.Fetch(FetchKind.Select); map.NotFound(NotFoundMode.Ignore); });
            ManyToOne(x => x.Province, map => { map.Column("ProvinceId");   map.Fetch(FetchKind.Select); map.NotFound(NotFoundMode.Ignore); });
            ManyToOne(x => x.District, map => { map.Column("DistrictId");  map.Fetch(FetchKind.Select); map.NotFound(NotFoundMode.Ignore); });
            ManyToOne(x => x.Subdistrict, map => { map.Column("SubdistrictId");   map.Fetch(FetchKind.Select); map.NotFound(NotFoundMode.Ignore); });
        }
    }
}
