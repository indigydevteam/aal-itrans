﻿using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories.OrderingCar.Mapping
{
    public partial class OrderingCarMap : ClassMapping<Domain.OrderingCar>
    {
        public OrderingCarMap()
        {
            Table("Ordering_Car");
            Lazy(true);
            Id(x => x.Id, map => map.Generator(Generators.Identity));
            Property(x => x.DriverCarId);
            Property(x => x.CarRegistration);
            Property(x => x.ActExpiry, map => { map.NotNullable(true); });
            //Property(x => x.CarTypeId, map => { map.NotNullable(true); });
            //Property(x => x.CarListId, map => { map.NotNullable(true); });
            //Property(x => x.CarDescriptionId, map => { map.NotNullable(true); });
            Property(x => x.CarDescriptionDetail);
            //Property(x => x.CarFeatureId, map => { map.NotNullable(true); });
            Property(x => x.CarFeatureDetail);
            //Property(x => x.CarSpecificationId, map => { map.NotNullable(true); });
            Property(x => x.CarSpecificationDetail);
            Property(x => x.Width);
            Property(x => x.Length);
            Property(x => x.Height);
            Property(x => x.Temperature);
            Property(x => x.DriverName);
            Property(x => x.DriverPhoneCode);
            Property(x => x.DriverPhoneNumber);
            Property(x => x.ProductInsurance);
            Property(x => x.ProductInsuranceAmount);
            Property(x => x.Note);
            Property(x => x.IsCharter);
            Property(x => x.IsRequestTax);
            Property(x => x.TransportType);
            Property(x => x.Created);
            Property(x => x.Modified);
            Property(x => x.CreatedBy);
            Property(x => x.ModifiedBy);
            Bag(x => x.CarFiles, colmap => { colmap.Key(x => x.Column("OrderingCarId")); colmap.Inverse(true); colmap.Cascade(Cascade.Persist); }, map => { map.OneToMany(a => a.Class(typeof(Domain.OrderingCarFile))); });
            //Bag(x => x.Drivers, colmap => { colmap.Key(x => x.Column("OrderingCarId")); colmap.Inverse(true); colmap.Cascade(Cascade.All); }, map => { map.OneToMany(a => a.Class(typeof(Domain.OrderingCarDriver))); });
            ManyToOne(x => x.Ordering, map => { map.Column("OrderingId"); map.Fetch(FetchKind.Join); map.NotFound(NotFoundMode.Ignore); });
            ManyToOne(x => x.OrderingDriver, map => { map.Column("OrderingDriverId"); map.Fetch(FetchKind.Join); map.NotFound(NotFoundMode.Ignore); });
            ManyToOne(x => x.CarType, map => { map.Column("CarTypeId"); map.Fetch(FetchKind.Select); map.NotFound(NotFoundMode.Ignore); });
            ManyToOne(x => x.CarList, map => { map.Column("CarListId"); map.Fetch(FetchKind.Select); map.NotFound(NotFoundMode.Ignore); });
            ManyToOne(x => x.CarDescription, map => { map.Column("CarDescriptionId"); map.Fetch(FetchKind.Select); map.NotFound(NotFoundMode.Ignore); });
            ManyToOne(x => x.CarFeature, map => { map.Column("CarFeatureId"); map.Fetch(FetchKind.Select); map.NotFound(NotFoundMode.Ignore); });
            ManyToOne(x => x.CarSpecification, map => { map.Column("CarSpecificationId"); map.Fetch(FetchKind.Select); map.NotFound(NotFoundMode.Ignore); });
            ManyToOne(x => x.EnergySavingDevice, map => { map.Column("EnergySavingDeviceId"); map.Fetch(FetchKind.Join); map.NotFound(NotFoundMode.Ignore); });
        }
    }
}