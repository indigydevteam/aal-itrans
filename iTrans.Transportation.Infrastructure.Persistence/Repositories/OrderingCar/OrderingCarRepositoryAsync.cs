﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories.OrderingCar
{
   public class OrderingCarRepositoryAsync : GenericRepositoryAsync<Domain.OrderingCar>, IOrderingCarRepositoryAsync
    {
        public OrderingCarRepositoryAsync(ISession session) : base(session)
        { }
    }
}