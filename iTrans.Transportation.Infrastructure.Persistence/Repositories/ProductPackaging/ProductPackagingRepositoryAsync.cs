﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using NHibernate;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
    public class ProductPackagingRepositoryAsync : GenericRepositoryAsync<Domain.ProductPackaging>, IProductPackagingRepositoryAsync
    {
        public ProductPackagingRepositoryAsync(ISession session) : base(session)
        { }
    }
}
