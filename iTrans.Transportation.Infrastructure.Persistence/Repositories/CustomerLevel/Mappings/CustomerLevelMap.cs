﻿using iTrans.Transportation.Domain;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using System;


namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
    public partial class CustomerLevelMap : ClassMapping<Domain.CustomerLevel>
    {
        public CustomerLevelMap()
        {
            Table("Customer_Level");
            Lazy(true);
            Id(x => x.CustomerId, map =>
            {
                map.Column("CustomerId");
                map.UnsavedValue(Guid.Empty);
            });
            //Property(x => x.CarListId);
            Property(x => x.CustomerId, map => { map.NotNullable(true);});
            Property(x => x.Level);
            Property(x => x.RequestCar);
            Property(x => x.Star);
            Property(x => x.CancelAmount);
            Property(x => x.OrderValue);
            Property(x => x.Created);
            Property(x => x.Modified);
            Property(x => x.CreatedBy);
            Property(x => x.ModifiedBy);
        }
    }
}
