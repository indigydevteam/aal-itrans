﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using NHibernate;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
    public class CustomerLevelRepositoryAsync : GenericRepositoryAsync<Domain.CustomerLevel>, ICustomerLevelRepositoryAsync
    {
        public CustomerLevelRepositoryAsync(ISession session) : base(session)
        { }
    }
}
