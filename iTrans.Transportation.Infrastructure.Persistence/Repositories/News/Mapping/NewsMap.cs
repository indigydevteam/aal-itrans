﻿using iTrans.Transportation.Domain;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using System;


namespace iTrans.Transportation.Infrastructure.Persistence.Repositories.News.Mapping
{
    public partial class NewsMap : ClassMapping<Domain.News>
    {
        public NewsMap()
        {
            Table("News");
            Lazy(true);
            Id(x => x.Id, map => map.Generator(Generators.Identity));
            Property(x => x.Title, map => { map.NotNullable(true); map.Length(250); });
            Property(x => x.Detail);
            Property(x => x.StartDate);
            Property(x => x.EndDate);
            Property(x => x.HLStartDate);
            Property(x => x.HLEndDate);
            Property(x => x.ImageFileId);
            Property(x => x.IsActive);
            Property(x => x.IsHighlight);
            Property(x => x.IsNotification);
            Property(x => x.IsDelete);
            Property(x => x.IsActive);
            Property(x => x.IsGuest);
            Property(x => x.Url, map => { map.Length(250); });
            Property(x => x.VideoUrl, map => { map.Length(1000); });
            Property(x => x.FileId);
            Property(x => x.VideoId);
            Property(x => x.NewsType);
            Property(x => x.TotalView);
            Property(x => x.IsAll);
            Property(x => x.TargetCustomerType);
            Property(x => x.TargetDriverType);
            Property(x => x.IsSelection);
            Property(x => x.Created);
            Property(x => x.Modified);
            Property(x => x.CreatedBy);
            Property(x => x.ModifiedBy);
        }
    }
}
