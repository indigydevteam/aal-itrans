﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using NHibernate;


namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
    public class NewsRepositoryAsync : GenericRepositoryAsync<Domain.News>, INewsRepositoryAsync
    {
        public NewsRepositoryAsync(ISession session) : base(session)
        { }

        
    }
}
