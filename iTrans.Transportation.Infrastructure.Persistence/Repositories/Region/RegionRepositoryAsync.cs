﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using NHibernate;


namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
    public class RegionRepositoryAsync : GenericRepositoryAsync<Domain.Region>, IRegionRepositoryAsync
    {
        public RegionRepositoryAsync(ISession session) : base(session)
        { }
    }
}
