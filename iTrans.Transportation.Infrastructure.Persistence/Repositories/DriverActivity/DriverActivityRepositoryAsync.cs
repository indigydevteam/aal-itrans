﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using NHibernate;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
    public class DriverActivityRepositoryAsync : GenericRepositoryAsync<Domain.DriverActivity>, IDriverActivityRepositoryAsync
    {
        public DriverActivityRepositoryAsync(ISession session) : base(session)
        { }
    }
}
