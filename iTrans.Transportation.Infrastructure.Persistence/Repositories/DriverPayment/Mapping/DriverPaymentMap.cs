﻿using iTrans.Transportation.Domain;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using System;
namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
    public partial class DriverPaymentMap : ClassMapping<Domain.DriverPayment>
    {
        public DriverPaymentMap()
        {
            Table("Driver_Payment");
            Lazy(true);
            Id(x => x.Id, map => map.Generator(Generators.Identity));
            Property(x => x.Bank);
            Property(x => x.AccountType);
            Property(x => x.Amount);
            Property(x => x.Value);
            Property(x => x.Description);
            Property(x => x.Created);
            Property(x => x.Modified);
            Property(x => x.CreatedBy);
            Property(x => x.ModifiedBy);
            ManyToOne(x => x.Driver, map => { map.Column("DriverId"); map.Fetch(FetchKind.Select); map.NotFound(NotFoundMode.Ignore); });
            ManyToOne(x => x.Type, map => { map.Column("PaymentTypeId"); map.Fetch(FetchKind.Select); map.NotFound(NotFoundMode.Ignore); });
            Bag(x => x.Histories, colmap => { colmap.Key(x => x.Column("DriverPaymentId")); colmap.Inverse(true); colmap.Cascade(Cascade.Persist); }, map => { map.OneToMany(a => a.Class(typeof(DriverPaymentHistory))); });
        }
    }
}
