﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
   public class DriverPaymentRepositoryAsync : GenericRepositoryAsync<Domain.DriverPayment>, IDriverPaymentRepositoryAsync
    {
        public DriverPaymentRepositoryAsync(ISession session) : base(session)
        { }
    }
}
