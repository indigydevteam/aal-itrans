﻿using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
    public partial class TermAndConditionMap : ClassMapping<Domain.Entities.TermAndCondition>
    {
        public TermAndConditionMap()
        {
            Table("TermAndCondition");
            Lazy(true);
            Id(x => x.Id, map => map.Generator(Generators.Identity));
            Property(x => x.Module, map => { map.NotNullable(true); map.Length(250); });
            Property(x => x.Name_TH, map => { map.NotNullable(true);});
            Property(x => x.Name_ENG, map => { map.NotNullable(true);});
            Property(x => x.Section, map => { map.NotNullable(true); map.Length(250); });
            Property(x => x.Version);
            Property(x => x.Sequence);
            Property(x => x.Active);
            Property(x => x.IsAccept);
            Property(x => x.IsPublic);
            Property(x => x.Created);
            Property(x => x.Modified);
            Property(x => x.CreatedBy);
            Property(x => x.ModifiedBy);
        }
    }
}
