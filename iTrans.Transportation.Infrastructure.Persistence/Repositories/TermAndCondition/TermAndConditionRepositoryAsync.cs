﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories.TermAndCondition
{
   public class TermAndConditionRepositoryAsync : GenericRepositoryAsync<Domain.Entities.TermAndCondition>, ITermAndConditionRepositoryAsync
    {
        public TermAndConditionRepositoryAsync(ISession session) : base(session)
        { }
    }
}