﻿using iTrans.Transportation.Domain;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using System;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
    public partial class DriverMap : ClassMapping<Domain.Driver>
    {
        public DriverMap()
        {
            Table("Driver");
            Lazy(true);
            Id(x => x.Id, map =>
            {
                map.Generator(Generators.Guid);
                map.Column("Id");
                map.UnsavedValue(Guid.Empty);
            });
            Property(x => x.Password, map => { map.NotNullable(true); map.Length(250); });
            Property(x => x.DriverType, map => { map.NotNullable(true); map.Length(250); });
            //Property(x => x.CorporateTypeId);
            //Property(x => x.Title, map => { map.NotNullable(true); map.Length(50); });
            Property(x => x.FirstName, map => { map.Length(250); });
            Property(x => x.MiddleName, map => { map.Length(250); });
            Property(x => x.LastName, map => { map.Length(250); });
            Property(x => x.Name, map => { map.NotNullable(true); map.Length(250); });
            Property(x => x.IdentityType);
            Property(x => x.IdentityNumber, map => { map.Length(250); });
            //Property(x => x.ContactPersonTitle, map => { map.NotNullable(true); map.Length(50); });
            Property(x => x.ContactPersonFirstName, map => { map.Length(250); });
            Property(x => x.ContactPersonMiddleName, map => { map.Length(250); });
            Property(x => x.ContactPersonLastName, map => { map.Length(250); });
            Property(x => x.Birthday);
            Property(x => x.PhoneCode, map => { map.NotNullable(true); map.Length(50); });
            Property(x => x.PhoneNumber, map => { map.NotNullable(true); map.Length(50); });
            Property(x => x.Email, map => { map.Length(250); });
            Property(x => x.Facbook, map => { map.Length(250); });
            Property(x => x.Line, map => { map.Length(250); });
            Property(x => x.Twitter, map => { map.Length(250); });
            Property(x => x.Whatapp, map => { map.Length(250); });
            Property(x => x.Wechat, map => { map.Length(250); });
            Property(x => x.Level);
            Property(x => x.Corparate);
            Property(x => x.Star);
            Property(x => x.VerifyStatus);
            Property(x => x.IsRegister);
            Property(x => x.Rating);
            Property(x => x.Status);
            Property(x => x.Grade);
            Property(x => x.Created);
            Property(x => x.Modified);
            Property(x => x.CreatedBy);
            Property(x => x.ModifiedBy);
            Property(x => x.IsDelete);
            Bag(x => x.TermAndConditions, colmap => { colmap.Key(x => x.Column("DriverId")); colmap.Inverse(true); colmap.Cascade(Cascade.Persist); }, map => { map.OneToMany(a => a.Class(typeof(DriverTermAndCondition))); });
            Bag(x => x.Payments, colmap => { colmap.Key(x => x.Column("DriverId")); colmap.Inverse(true); colmap.Cascade(Cascade.Persist); }, map => { map.OneToMany(a => a.Class(typeof(DriverPayment))); });
            Bag(x => x.Addresses, colmap => { colmap.Key(x => x.Column("DriverId")); colmap.Inverse(true); colmap.Cascade(Cascade.Persist); }, map => { map.OneToMany(a => a.Class(typeof(DriverAddress))); });
            Bag(x => x.Files, colmap => { colmap.Key(x => x.Column("DriverId")); colmap.Inverse(true); colmap.Cascade(Cascade.Persist); }, map => { map.OneToMany(a => a.Class(typeof(DriverFile))); });
            Bag(x => x.Cars, colmap => { colmap.Key(x => x.Column("DriverId")); colmap.Inverse(true); colmap.Cascade(Cascade.Persist); }, map => { map.OneToMany(a => a.Class(typeof(DriverCar))); });
            ManyToOne(x => x.CorporateType, map => { map.Column("CorporateTypeId");  map.Fetch(FetchKind.Select); map.NotFound(NotFoundMode.Ignore); });
            ManyToOne(x => x.Title, map => { map.Column("TitleId");  map.Fetch(FetchKind.Select); map.NotFound(NotFoundMode.Ignore); });
            ManyToOne(x => x.ContactPersonTitle, map => { map.Column("ContactPersonTitleId"); map.Fetch(FetchKind.Select); map.NotFound(NotFoundMode.Ignore); });
        }
    }
}