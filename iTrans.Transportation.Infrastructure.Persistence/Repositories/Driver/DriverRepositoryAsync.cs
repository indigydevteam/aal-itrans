﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using NHibernate;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
    public class DriverRepositoryAsync : GenericRepositoryAsync<Domain.Driver>, IDriverRepositoryAsync
    {
        public DriverRepositoryAsync(ISession session) : base(session)
        { }
    }
}
