﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories.CustomerPaymentTransaction
{
   public class CustomerPaymentTransactionRepositoryAsync : GenericRepositoryAsync<Domain.CustomerPaymentTransaction>, ICustomerPaymentTransactionRepositoryAsync
    {
        public CustomerPaymentTransactionRepositoryAsync(ISession session) : base(session)
        { }
    }
}
