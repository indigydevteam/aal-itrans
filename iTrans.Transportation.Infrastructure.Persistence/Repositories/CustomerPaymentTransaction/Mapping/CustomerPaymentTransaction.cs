﻿using iTrans.Transportation.Domain;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using System;
namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
    public partial class CustomerPaymentTransactionMap : ClassMapping<Domain.CustomerPaymentTransaction>
    {
        public CustomerPaymentTransactionMap()
        {
            Table("Customer_PaymentTransaction");
            Lazy(true);
            Id(x => x.Id, map => map.Generator(Generators.Identity));
            Property(x => x.Customer_PaymentHistoryId, map => { map.NotNullable(true); });
            Property(x => x.Detail);
            Property(x => x.Created);
            Property(x => x.Modified);
            Property(x => x.CreatedBy);
            Property(x => x.ModifiedBy);
        }
    }
}
