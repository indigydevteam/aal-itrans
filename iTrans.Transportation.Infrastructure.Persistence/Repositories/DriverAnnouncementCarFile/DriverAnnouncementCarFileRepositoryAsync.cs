﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using NHibernate;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
   public class DriverAnnouncementCarFileRepositoryAsync : GenericRepositoryAsync<Domain.DriverAnnouncementCarFile>, IDriverAnnouncementCarFileRepositoryAsync
    {
        public DriverAnnouncementCarFileRepositoryAsync(ISession session) : base(session)
        { }
    }
}
