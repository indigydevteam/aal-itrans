﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using NHibernate;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories 
{
    public class CustomerAddressRepositoryAsync : GenericRepositoryAsync<Domain.CustomerAddress>, ICustomerAddressRepositoryAsync
    {
        public CustomerAddressRepositoryAsync(ISession session) : base(session)
        { }
    }
}
