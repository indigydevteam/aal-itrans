﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories.WelcomeInformation
{
   public class WelcomeInformationRepositoryAsync : GenericRepositoryAsync<Domain.Entities.WelcomeInformation>, IWelcomeInformationRepositoryAsync
    {
        public WelcomeInformationRepositoryAsync(ISession session) : base(session)
        { }
    }
}