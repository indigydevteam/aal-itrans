﻿using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories.WelcomeInformation.Mapping
{
   public class WelcomeInformationMap : ClassMapping<Domain.Entities.WelcomeInformation>
    {
        public WelcomeInformationMap()
        {
            Table("Welcome_Information");
            Lazy(true);
            Id(x => x.Id, map => map.Generator(Generators.Identity));
            Property(x => x.Module, map => { map.NotNullable(true); map.Length(50); });
            Property(x => x.Title_TH, map => { map.NotNullable(true); map.Length(250); });
            Property(x => x.Information_TH, map => { map.NotNullable(true);});
            Property(x => x.Title_ENG, map => { map.NotNullable(true); map.Length(250); });
            Property(x => x.Information_ENG, map => { map.NotNullable(true); });
            Property(x => x.Picture);
            Property(x => x.Sequence);
            Property(x => x.Active);
            Property(x => x.IsDelete);
            Property(x => x.Created);
            Property(x => x.Modified);
            Property(x => x.CreatedBy);
            Property(x => x.ModifiedBy);
        }
    }
}
