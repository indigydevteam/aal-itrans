﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories.Menu
{
    public class MenuRepositoryAsync : GenericRepositoryAsync<Domain.Menu>, IMenuRepositoryAsync
    {
        public MenuRepositoryAsync(ISession session) : base(session)
        {
        }
    }
}
