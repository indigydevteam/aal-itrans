﻿using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories.Menu.Mapping
{
    public class MenuMap : ClassMapping<Domain.Menu>
    {
        public MenuMap()
        {
            Table("Menu");
            Lazy(true);
            Id(x => x.Id, map => map.Generator(Generators.Identity));
            Property(x => x.NameTH);
            Property(x => x.NameEN);
            Property(x => x.Icon);
            Property(x => x.Path);
            Property(x => x.Created);
            Property(x => x.Modified);
            Property(x => x.CreatedBy);
            Property(x => x.ModifiedBy);
        }
    }
}
