﻿using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
    public partial class OrderingCancelStatusMap : ClassMapping<Domain.OrderingCancelStatus>
    {
        public OrderingCancelStatusMap()
        {
            Table("Ordering_CancelStatus");
            Lazy(true);
            Id(x => x.Id, map => map.Generator(Generators.Identity));
            Property(x => x.Module, map => { map.NotNullable(true); });
            Property(x => x.Name_TH, map => { map.NotNullable(true); });
            Property(x => x.Name_ENG, map => { map.NotNullable(true); });
            Property(x => x.Active, map => { map.NotNullable(true); });
            Property(x => x.Active, map => { map.NotNullable(true); });
            Property(x => x.Sequence, map => { map.NotNullable(true); });
            Property(x => x.Created);
            Property(x => x.Modified);
            Property(x => x.CreatedBy);
            Property(x => x.ModifiedBy);
        }
    }
}