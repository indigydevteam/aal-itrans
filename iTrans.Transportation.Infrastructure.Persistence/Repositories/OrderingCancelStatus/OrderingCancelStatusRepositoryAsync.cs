﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
    public class OrderingCancelStatusRepositoryAsync : GenericRepositoryAsync<Domain.OrderingCancelStatus>, IOrderingCancelStatusRepositoryAsync
    {
        public OrderingCancelStatusRepositoryAsync(ISession session) : base(session)
        { }
    }
}