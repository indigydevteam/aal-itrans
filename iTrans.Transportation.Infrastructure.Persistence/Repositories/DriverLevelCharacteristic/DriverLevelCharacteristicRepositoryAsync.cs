﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using NHibernate;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
    public class DriverLevelCharacteristicRepositoryAsync : GenericRepositoryAsync<Domain.DriverLevelCharacteristic>, IDriverLevelCharacteristicRepositoryAsync
    {
        public DriverLevelCharacteristicRepositoryAsync(ISession session) : base(session)
        { }
    }
}
