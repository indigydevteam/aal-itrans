﻿using iTrans.Transportation.Domain;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using System;


namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
    public partial class DriverLevelCharacteristicMap : ClassMapping<Domain.DriverLevelCharacteristic>
    {
        public DriverLevelCharacteristicMap()
        {
            Table("Driver_LevelCharacteristic");
            Lazy(true);
            Id(x => x.Id, map => map.Generator(Generators.Identity));
            Property(x => x.Level);
            Property(x => x.Star);
            Property(x => x.AcceptJobPerWeek);
            Property(x => x.AcceptJobPerMonth);
            Property(x => x.AcceptJobPerYear);
            Property(x => x.CancelPerWeek);
            Property(x => x.CancelPerMonth);
            Property(x => x.CancelPerYear);
            Property(x => x.ComplaintPerWeek);
            Property(x => x.ComplaintPerMonth);
            Property(x => x.ComplaintPerYear);
            Property(x => x.RejectPerWeek);
            Property(x => x.RejectPerMonth);
            Property(x => x.RejectPerYear);
            Property(x => x.InsuranceValue);
            Property(x => x.Commission);
            Property(x => x.Fine);
            Property(x => x.Discount);
            Property(x => x.IsDelete);
            Property(x => x.Created);
            Property(x => x.Modified);
            Property(x => x.CreatedBy);
            Property(x => x.ModifiedBy);
        }
    }
}