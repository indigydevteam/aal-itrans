﻿using iTrans.Transportation.Domain;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using System;
namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
    public partial class DriverProblemFileMap : ClassMapping<Domain.DriverProblemFile>
    {
        public DriverProblemFileMap()
        {
            Table("Driver_ProblemFile");
            Lazy(true);
            Id(x => x.Id, map => map.Generator(Generators.Identity));
            //Property(x => x.DriverProblemId, map => { map.NotNullable(true); });
            Property(x => x.FileName, map => { map.NotNullable(true); });
            Property(x => x.ContentType, map => { map.NotNullable(true); });
            Property(x => x.FilePath, map => { map.NotNullable(true); });
            Property(x => x.DirectoryPath, map => { map.NotNullable(true); });
            Property(x => x.FileEXT, map => { map.NotNullable(true); });
            Property(x => x.Sequence);
            Property(x => x.Created);
            Property(x => x.Modified);
            Property(x => x.CreatedBy);
            Property(x => x.ModifiedBy);
            ManyToOne(x => x.DriverProblem, map => { map.Column("DriverProblemId"); map.Fetch(FetchKind.Select); map.NotFound(NotFoundMode.Ignore); });
        }
    }
}
