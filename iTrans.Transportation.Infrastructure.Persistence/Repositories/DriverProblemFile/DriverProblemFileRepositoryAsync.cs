﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using NHibernate;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
   public class DriverProblemFileRepositoryAsync : GenericRepositoryAsync<Domain.DriverProblemFile>, IDriverProblemFileRepositoryAsync
    {
        public DriverProblemFileRepositoryAsync(ISession session) : base(session)
        { }
    }
}
