﻿using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using System;
namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
    public partial class CustomerComplainAndRecommendFileMap : ClassMapping<Domain.CustomerComplainAndRecommendFile>
    {
        public CustomerComplainAndRecommendFileMap()
        {
            Table("Customer_ComplainAndRecommendFile");
            Lazy(true);
            Id(x => x.Id, map => map.Generator(Generators.Identity));
            Property(x => x.FileName, map => { map.NotNullable(true); });
            Property(x => x.ContentType, map => { map.NotNullable(true); });
            Property(x => x.FilePath, map => { map.NotNullable(true); });
            Property(x => x.DirectoryPath, map => { map.NotNullable(true); });
            Property(x => x.FileEXT, map => { map.NotNullable(true); });
            Property(x => x.DocumentType);
            Property(x => x.Sequence);
            Property(x => x.Created);
            Property(x => x.Modified);
            Property(x => x.CreatedBy);
            Property(x => x.ModifiedBy);
            ManyToOne(x => x.CustomerComplainAndRecommend, map => { map.Column("CustomerComplainAndRecommendId"); map.Fetch(FetchKind.Select); map.NotFound(NotFoundMode.Ignore); });
        }
    }
}
