﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using NHibernate;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
   public class CustomerComplainAndRecommendFileRepositoryAsync : GenericRepositoryAsync<Domain.CustomerComplainAndRecommendFile>, ICustomerComplainAndRecommendFileRepositoryAsync
    {
        public CustomerComplainAndRecommendFileRepositoryAsync(ISession session) : base(session)
        { }
    }
}
