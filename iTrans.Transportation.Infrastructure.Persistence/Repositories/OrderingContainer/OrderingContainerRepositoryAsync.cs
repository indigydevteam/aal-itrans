﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories.OrderingContainer
{
   public class OrderingContainerRepositoryAsync : GenericRepositoryAsync<Domain.OrderingContainer>, IOrderingContainerRepositoryAsync
    {
        public OrderingContainerRepositoryAsync(ISession session) : base(session)
        { }
    }
}
