﻿using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using System;
using System.Collections.Generic;
using System.Text;
namespace iTrans.Transportation.Infrastructure.Persistence.Repositories.OrderingContainer.Mapping
{
    public class OrderingContainerMap : ClassMapping<Domain.OrderingContainer>
    {
        public OrderingContainerMap()
        {
            Table("Ordering_Container");
            Lazy(true);
            Id(x => x.Id, map => map.Generator(Generators.Identity));
            Property(x => x.Section, map => { map.NotNullable(true); map.Length(250); });
            Property(x => x.ContainerBooking);
            Property(x => x.Temperature);
            Property(x => x.Moisture);
            Property(x => x.Ventilation);
            //Property(x => x.EnergySavingDeviceId); //
            Property(x => x.SpecialSpecify);
            Property(x => x.Weight);
            Property(x => x.Commodity);
            Property(x => x.DateOfBooking);
            Property(x => x.PickupPointPostCode);
            Property(x => x.PickupPointRoad);
            Property(x => x.PickupPointAlley);
            Property(x => x.PickupPointAddress);
            Property(x => x.PickupPointMaps);
            Property(x => x.PickupPointDate);
            Property(x => x.ReturnPointPostCode);
            Property(x => x.ReturnPointRoad);
            Property(x => x.ReturnPointAlley);
            Property(x => x.ReturnPointAddress);
            Property(x => x.ReturnPointMaps);
            Property(x => x.ReturnPointDate);
            Property(x => x.CutOffDate);
            Property(x => x.ShippingContact);
            Property(x => x.ShippingContactPhoneCode);
            Property(x => x.ShippingContactPhoneNumber);
            Property(x => x.YardContact);
            Property(x => x.YardContactPhoneCode);
            Property(x => x.YardContactPhoneNumber);
            Property(x => x.LinerContact);
            Property(x => x.LinerContactPhoneCode);
            Property(x => x.LinerContactPhoneNumber);
            Property(x => x.DeliveryOrderNumber);
            Property(x => x.BookingNumber);
            Property(x => x.ContainerAmount);
            Property(x => x.SpecialOrder);

            Property(x => x.Created);
            Property(x => x.Modified);
            Property(x => x.CreatedBy);
            Property(x => x.ModifiedBy);
            Bag(x => x.Files, colmap => { colmap.Key(x => x.Column("OrderingContainerId")); colmap.Inverse(true); colmap.Cascade(Cascade.Persist); }, map => { map.OneToMany(a => a.Class(typeof(Domain.OrderingContainerFile))); });
            ManyToOne(x => x.Ordering, map => { map.Column("OrderingId"); map.Fetch(FetchKind.Join); map.NotFound(NotFoundMode.Ignore); });
            ManyToOne(x => x.ContainerType, map => { map.Column("ContainerTypeId"); map.Fetch(FetchKind.Join); map.NotFound(NotFoundMode.Ignore); });
            ManyToOne(x => x.ContainerSpecification, map => { map.Column("ContainerSpecificationId"); map.Fetch(FetchKind.Join); map.NotFound(NotFoundMode.Ignore); });
            ManyToOne(x => x.EnergySavingDevice, map => { map.Column("EnergySavingDeviceId"); map.Fetch(FetchKind.Join); map.NotFound(NotFoundMode.Ignore); });

            ManyToOne(x => x.PickupPointCountry, map => { map.Column("PickupPointCountryId"); map.Fetch(FetchKind.Join); map.NotFound(NotFoundMode.Ignore); });
            ManyToOne(x => x.PickupPointProvince, map => { map.Column("PickupPointProvinceId"); map.Fetch(FetchKind.Join); map.NotFound(NotFoundMode.Ignore); });
            ManyToOne(x => x.PickupPointDistrict, map => { map.Column("PickupPointDistrictId"); map.Fetch(FetchKind.Join); map.NotFound(NotFoundMode.Ignore); });
            ManyToOne(x => x.PickupPointSubdistrict, map => { map.Column("PickupPointSubdistrictId"); map.Fetch(FetchKind.Join); map.NotFound(NotFoundMode.Ignore); });

            ManyToOne(x => x.ReturnPointCountry, map => { map.Column("ReturnPointCountryId"); map.Fetch(FetchKind.Join); map.NotFound(NotFoundMode.Ignore); });
            ManyToOne(x => x.ReturnPointProvince, map => { map.Column("ReturnPointProvinceId"); map.Fetch(FetchKind.Join); map.NotFound(NotFoundMode.Ignore); });
            ManyToOne(x => x.ReturnPointDistrict, map => { map.Column("ReturnPointDistrictId"); map.Fetch(FetchKind.Join); map.NotFound(NotFoundMode.Ignore); });
            ManyToOne(x => x.ReturnPointSubdistrict, map => { map.Column("ReturnPointSubdistrictId"); map.Fetch(FetchKind.Join); map.NotFound(NotFoundMode.Ignore); });
        }
    }
}