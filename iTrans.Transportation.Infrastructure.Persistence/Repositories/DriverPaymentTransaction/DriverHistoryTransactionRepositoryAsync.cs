﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories.DriverPaymentTransaction
{
    public class DriverPaymentTransactionRepositoryAsync : GenericRepositoryAsync<Domain.DriverPaymentTransaction>, IDriverPaymentTransactionRepositoryAsync
    {
        public DriverPaymentTransactionRepositoryAsync(ISession session) : base(session)
        { }
    }
}
