﻿using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories.NotificationUser.Mapping
{
    public partial class NotificationUserMap : ClassMapping<Domain.NotificationUser>
    {
        public NotificationUserMap()
        {
            Table("NotificationUser");
            Lazy(true);
            Id(x => x.Id, map => map.Generator(Generators.Identity));
            Property(x => x.OneSignalId, map => { map.Length(50); });
            Property(x => x.Device, map => { map.Length(50); });
            Property(x => x.UserId);
            Property(x => x.IsActive);
            Property(x => x.Created);
            Property(x => x.Modified);
            Property(x => x.CreatedBy);
            Property(x => x.ModifiedBy);
        }
    }
}
