﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using NHibernate;


namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
    public class NotificationUserRepositoryAsync : GenericRepositoryAsync<Domain.NotificationUser>, INotificationUserRepositoryAsync
    {
        public NotificationUserRepositoryAsync(ISession session) : base(session)
        { } 
    }
}
