﻿using iTrans.Transportation.Domain;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using System;
namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
    public partial class PaymentTypeMap : ClassMapping<Domain.PaymentType>
    {
        public PaymentTypeMap()
        {
            Table("Payment_Type");
            Lazy(true);
            Id(x => x.Id, map => map.Generator(Generators.Identity));
            Property(x => x.Name, map => { map.NotNullable(true); });
            Property(x => x.Name_EN, map => { map.NotNullable(true); });
            Property(x => x.Active);
            Property(x => x.Sequence); 
            Property(x => x.Created);
            Property(x => x.Modified);
            Property(x => x.CreatedBy);
            Property(x => x.ModifiedBy);
        }
    }
}
