﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using NHibernate;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
    public class PaymentTypeRepositoryAsync : GenericRepositoryAsync<Domain.PaymentType>, IPaymentTypeRepositoryAsync
    {
        public PaymentTypeRepositoryAsync(ISession session) : base(session)
        { }
    }
}
