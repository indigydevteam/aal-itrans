﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using NHibernate;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
   public class DriverProblemRepositoryAsync : GenericRepositoryAsync<Domain.DriverProblem>, IDriverProblemRepositoryAsync
    {
        public DriverProblemRepositoryAsync(ISession session) : base(session)
        { }
    }
}
