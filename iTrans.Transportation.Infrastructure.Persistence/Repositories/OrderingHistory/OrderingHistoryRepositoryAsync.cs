﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories.OrderingHistory
{
   public class OrderingHistoryRepositoryAsync : GenericRepositoryAsync<Domain.OrderingHistory>, IOrderingHistoryRepositoryAsync
    {
        public OrderingHistoryRepositoryAsync(ISession session) : base(session)
        { }
    }
}