﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using NHibernate;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
   public class ProductTypeFileRepositoryAsync : GenericRepositoryAsync<Domain.ProductTypeFile>, IProductTypeFileRepositoryAsync
    {
        public ProductTypeFileRepositoryAsync(ISession session) : base(session)
        { }
    }
}
