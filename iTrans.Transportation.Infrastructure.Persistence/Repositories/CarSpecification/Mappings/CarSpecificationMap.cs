﻿using iTrans.Transportation.Domain;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using System;


namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
    public partial class CarSpecificationMap : ClassMapping<Domain.CarSpecification>
    {
        public CarSpecificationMap()
        {
            Table("Car_Specification");
            Lazy(true);
            Id(x => x.Id, map => map.Generator(Generators.Identity));
            //Property(x => x.CarListId);
            Property(x => x.Name_TH, map => { map.NotNullable(true); map.Length(50); });
            Property(x => x.Name_ENG, map => { map.NotNullable(true); map.Length(50); });
            Property(x => x.Sequence);
            Property(x => x.Specified);
            Property(x => x.SpecifiedTemperature, map => { map.NotNullable(true); });
            Property(x => x.SpecifiedEnergySavingDevice, map => { map.NotNullable(true); });
            Property(x => x.Active);
            Property(x => x.IsDelete);
            Property(x => x.Created);
            Property(x => x.Modified);
            Property(x => x.CreatedBy);
            Property(x => x.ModifiedBy);

            OneToOne(x => x.File, map =>
            {
                map.PropertyReference(typeof(CarSpecificationFile).GetProperty("CarSpecification"));
                map.Cascade(Cascade.All);
            });
            ManyToOne(x => x.CarType, map => { map.Column("CarTypeId"); map.Fetch(FetchKind.Select); map.NotFound(NotFoundMode.Ignore); });
        }
    }
}
