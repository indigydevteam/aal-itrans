﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using NHibernate;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
    public class CarSpecificationRepositoryAsync : GenericRepositoryAsync<Domain.CarSpecification>, ICarSpecificationRepositoryAsync
    {
        public CarSpecificationRepositoryAsync(ISession session) : base(session)
        { }
    }
}
