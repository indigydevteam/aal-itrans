﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
    public class CustomerPaymentHistoryRepositoryAsync : GenericRepositoryAsync<Domain.CustomerPaymentHistory>, ICustomerPaymentHistoryRepositoryAsync
    {
        public CustomerPaymentHistoryRepositoryAsync(ISession session) : base(session)
        { }
    }
}
