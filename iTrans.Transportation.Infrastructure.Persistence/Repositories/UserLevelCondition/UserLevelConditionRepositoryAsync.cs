﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories.UserLevelCondition
{
 public class UserLevelConditionRepositoryAsync : GenericRepositoryAsync<Domain.UserLevelCondition>, IUserLevelConditionRepositoryAsync
    {
        public UserLevelConditionRepositoryAsync(ISession session) : base(session)
        { }
    }
}