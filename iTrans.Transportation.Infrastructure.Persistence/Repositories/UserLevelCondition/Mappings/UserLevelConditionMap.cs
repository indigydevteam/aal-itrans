﻿using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories.UserLevelCondition.Mappings
{
   public partial class UserLevelConditionMap : ClassMapping<Domain.UserLevelCondition>
    {
        public UserLevelConditionMap()
        {
            Table("UserLevelCondition");
            Lazy(true);
            Id(x => x.Id, map => map.Generator(Generators.Identity));
            //Property(x => x.UserLevelId);
            Property(x => x.Characteristics_TH, map => { map.NotNullable(true); });
            Property(x => x.Characteristics_ENG, map => { map.NotNullable(true);  });
            Property(x => x.Sequence);
            Property(x => x.Active);
            Property(x => x.Created);
            Property(x => x.Modified);
            Property(x => x.CreatedBy);
            Property(x => x.ModifiedBy);
            ManyToOne(x => x.UserLevel, map => { map.Column("UserLevelId"); map.Fetch(FetchKind.Select); });
        }
    }
}