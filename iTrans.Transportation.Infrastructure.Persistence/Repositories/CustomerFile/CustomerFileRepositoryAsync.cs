﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using NHibernate;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
   public class CustomerFileRepositoryAsync : GenericRepositoryAsync<Domain.CustomerFile>, ICustomerFileRepositoryAsync
    {
        public CustomerFileRepositoryAsync(ISession session) : base(session)
        { }
    }
}
