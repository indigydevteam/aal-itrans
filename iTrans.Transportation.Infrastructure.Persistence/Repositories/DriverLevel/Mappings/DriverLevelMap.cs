﻿using iTrans.Transportation.Domain;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using System;


namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
    public partial class DriverLevelMap : ClassMapping<Domain.DriverLevel>
    {
        public DriverLevelMap()
        {
            Table("Driver_Level");
            Lazy(true);
            Id(x => x.DriverId, map =>
            {
                map.Column("DriverId");
                map.UnsavedValue(Guid.Empty);
            });
            //Property(x => x.CarListId);
            Property(x => x.DriverId, map => { map.NotNullable(true);});
            Property(x => x.Level);
            Property(x => x.Star);
            Property(x => x.AcceptJob);
            Property(x => x.ComplaintPerMonth);
            Property(x => x.RejectPerMonth);
            Property(x => x.CancelPerMonth);
            Property(x => x.ComplaintPerYear);
            Property(x => x.RejectPerYear);
            Property(x => x.CancelPerYear);
            Property(x => x.InsuranceValue);
            Property(x => x.Created);
            Property(x => x.Modified);
            Property(x => x.CreatedBy);
            Property(x => x.ModifiedBy);
        }
    }
}
