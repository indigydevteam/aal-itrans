﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using NHibernate;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
    public class DriverLevelRepositoryAsync : GenericRepositoryAsync<Domain.DriverLevel>, IDriverLevelRepositoryAsync
    {
        public DriverLevelRepositoryAsync(ISession session) : base(session)
        { }
    }
}
