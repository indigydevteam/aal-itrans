﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories.OrderingInsurance
{
   public  class OrderingInsuranceRepositoryAsync : GenericRepositoryAsync<Domain.OrderingInsurance>, IOrderingInsuranceRepositoryAsync
    {
        public OrderingInsuranceRepositoryAsync(ISession session) : base(session)
        { }
    }
}