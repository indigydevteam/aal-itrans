﻿using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories.OrderingInsurance.Mapping
{
    public partial class OrderingInsuranceMap : ClassMapping<Domain.OrderingInsurance>
    {
        public OrderingInsuranceMap()
        {
            Table("Ordering_Insurance");
            Lazy(true);
            Id(x => x.Id, map => map.Generator(Generators.Identity));
            //Property(x => x.OrderingId, map => { map.NotNullable(true); });
            Property(x => x.ProductName);
            Property(x => x.InsurancePremium, map => { map.NotNullable(true); });
            Property(x => x.WarrantyPeriod, map => { map.NotNullable(true); map.Length(250); });
            Property(x => x.InsuranceLimit, map => { map.NotNullable(true); });
            Property(x => x.Created);
            Property(x => x.Modified);
            Property(x => x.CreatedBy);
            Property(x => x.ModifiedBy);
            ManyToOne(x => x.Ordering, map => { map.Column("OrderingId"); map.Fetch(FetchKind.Join); map.NotFound(NotFoundMode.Ignore); });
            ManyToOne(x => x.ProductType, map => { map.Column("ProductTypeId"); map.Fetch(FetchKind.Select); map.NotFound(NotFoundMode.Ignore); });
        }
    }
}