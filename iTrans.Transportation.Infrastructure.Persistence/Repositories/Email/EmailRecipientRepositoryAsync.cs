using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Domain.Entities;
using NHibernate;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
    public class EmailRecipientRepositoryAsync : GenericRepositoryAsync<EmailRecipient>, IEmailRecipientRepositoryAsync
    {
        public EmailRecipientRepositoryAsync(ISession session) : base(session)
        {
        }
    }
}