using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Domain.Entities;
using NHibernate;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
    public class EmailTemplateRepositoryAsync : GenericRepositoryAsync<EmailTemplate>, IEmailTemplateRepositoryAsync
    {
        public EmailTemplateRepositoryAsync(ISession session) : base(session)
        {
        }
    }
}