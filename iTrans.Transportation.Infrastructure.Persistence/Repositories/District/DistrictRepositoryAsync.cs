﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using NHibernate;


namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
    public class DistrictRepositoryAsync : GenericRepositoryAsync<Domain.District>, IDistrictRepositoryAsync
    {
        public DistrictRepositoryAsync(ISession session) : base(session)
        { }
    }
}
