﻿using iTrans.Transportation.Domain;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using System;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
    public partial class DistrictMapp : ClassMapping<Domain.District>
    {
        public DistrictMapp()
        {
            Table("District");
            Lazy(true);
            Id(x => x.Id, map => map.Generator(Generators.Identity));
            //Property(x => x.ProvinceId, map => { map.NotNullable(true); });
            Property(x => x.Name_TH, map => { map.NotNullable(true); map.Length(50); });
            Property(x => x.Name_ENG, map => { map.NotNullable(true); map.Length(50); });
            Property(x => x.Active);
            Property(x => x.Created);
            Property(x => x.Modified);
            Property(x => x.CreatedBy);
            Property(x => x.ModifiedBy);
            ManyToOne(x => x.Province, map => { map.Column("ProvinceId"); map.Fetch(FetchKind.Select); map.NotFound(NotFoundMode.Ignore); });
        }
    }
}
