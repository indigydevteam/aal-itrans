﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using NHibernate;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
    public class CustomerActivityRepositoryAsync : GenericRepositoryAsync<Domain.CustomerActivity>, ICustomerActivityRepositoryAsync
    {
        public CustomerActivityRepositoryAsync(ISession session) : base(session)
        { }
    }
}
