﻿using iTrans.Transportation.Domain;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using System;


namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
    public partial class FAQMap : ClassMapping<Domain.FAQ>
    {
        public FAQMap()
        {
            Table("FAQ");
            Lazy(true);
            Id(x => x.Id, map => map.Generator(Generators.Identity));
            Property(x => x.Title_TH, map => { map.NotNullable(true);  });
            Property(x => x.Title_ENG, map => { map.NotNullable(true); });
            Property(x => x.Detail_TH, map => { map.NotNullable(true); });
            Property(x => x.Detail_ENG, map => { map.NotNullable(true); });
            Property(x => x.Module, map => { map.NotNullable(true);  });
            Property(x => x.Active);
            Property(x => x.Created);
            Property(x => x.Modified);
            Property(x => x.CreatedBy);
            Property(x => x.ModifiedBy);
        }
    }
}
