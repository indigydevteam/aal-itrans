﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using NHibernate;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
    public class FAQRepositoryAsync : GenericRepositoryAsync<Domain.FAQ>, IFAQRepositoryAsync
    {
        public FAQRepositoryAsync(ISession session) : base(session)
        { }
    }
}
