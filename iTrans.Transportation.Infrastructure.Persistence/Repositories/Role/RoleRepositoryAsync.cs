﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories.Role
{
    public class RoleRepositoryAsync : GenericRepositoryAsync<Domain.Entities.Role>, IRoleRepositoryAsync
    {
        public RoleRepositoryAsync(ISession session) : base(session)
        {
        }
    }
}
