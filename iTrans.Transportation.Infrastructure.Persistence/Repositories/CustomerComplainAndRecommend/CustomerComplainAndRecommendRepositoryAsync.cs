﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
   public class CustomerComplainAndRecommendRepositoryAsync : GenericRepositoryAsync<Domain.Entities.CustomerComplainAndRecommend>, ICustomerComplainAndRecommendRepositoryAsync
    {
        public CustomerComplainAndRecommendRepositoryAsync(ISession session) : base(session)
        { }
    }
}
