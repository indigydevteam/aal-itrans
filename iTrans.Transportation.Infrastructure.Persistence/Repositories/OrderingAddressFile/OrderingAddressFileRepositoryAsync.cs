﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using NHibernate;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
   public class OrderingAddressFileRepositoryAsync : GenericRepositoryAsync<Domain.OrderingAddressFile>, IOrderingAddressFileRepositoryAsync
    {
        public OrderingAddressFileRepositoryAsync(ISession session) : base(session)
        { }
    }
}
