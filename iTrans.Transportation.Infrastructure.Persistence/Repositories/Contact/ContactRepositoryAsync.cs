﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories.Contact
{
  public  class ContactRepositoryAsync : GenericRepositoryAsync<Domain.Entities.Contact>, IContactRepositoryAsync
    {
        public ContactRepositoryAsync(ISession session) : base(session)
        { }
    }
}
