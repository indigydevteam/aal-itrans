﻿using iTrans.Transportation.Domain;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using System;


namespace iTrans.Transportation.Infrastructure.Persistence.Repositories.Contact.Mapping
{
    public partial class ContactMap : ClassMapping<Domain.Entities.Contact>
    {
        public ContactMap()
        {
            Table("Contact");
            Lazy(true);
            Id(x => x.Id, map => map.Generator(Generators.Identity));
            Property(x => x.address );
            Property(x => x.email );
            Property(x => x.fax);
            Property(x => x.faecbook );
            Property(x => x.twitter);
            Property(x => x.instagrame);
            Property(x => x.line);
            Property(x => x.phone);
            Property(x => x.youtube);
           ;
        }
    }
}
