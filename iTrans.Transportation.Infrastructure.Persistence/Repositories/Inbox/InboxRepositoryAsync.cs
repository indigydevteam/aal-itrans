﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using NHibernate;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
    public class  InboxRepositoryAsync : GenericRepositoryAsync<Domain.UserInbox>, IInboxRepositoryAsync
    {
        public InboxRepositoryAsync(ISession session) : base(session)
        { }
    }
}
