﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using NHibernate;


namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
    public class OrderingTrackingCodeRepositoryAsync : GenericRepositoryAsync<Domain.OrderingTrackingCode>, IOrderingTrackingCodeRepositoryAsync
    {
        public OrderingTrackingCodeRepositoryAsync(ISession session) : base(session)
        { }
    }
}
