﻿using iTrans.Transportation.Domain;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using System;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
    public partial class OrderingTrackingCodeMap : ClassMapping<Domain.OrderingTrackingCode>
    {
        public OrderingTrackingCodeMap()
        {
            Table("Ordering_TrackingCode");
            Lazy(true);
            Id(x => x.Id, map => map.Generator(Generators.Identity));
            Property(x => x.TrackingCode, map => { map.NotNullable(true); });
            Property(x => x.Section, map => { map.NotNullable(true); });

        }
    }
}
