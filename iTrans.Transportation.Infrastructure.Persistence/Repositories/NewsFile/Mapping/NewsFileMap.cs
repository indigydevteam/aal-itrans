﻿using iTrans.Transportation.Domain;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using System;


namespace iTrans.Transportation.Infrastructure.Persistence.Repositories.NewsFile.Mapping
{
    public partial class NewsFileMap : ClassMapping<Domain.NewsFile>
    {
        public NewsFileMap()
        {
            Table("NewsFile");
            Lazy(true);
            Id(x => x.Id); 
            Property(x => x.FileName, map => { map.NotNullable(true); map.Length(250); });
            Property(x => x.ContentType, map => { map.NotNullable(true); map.Length(50); });
            Property(x => x.FilePath, map => { map.NotNullable(true); map.Length(250); });
            Property(x => x.FileEXT, map => { map.NotNullable(true); map.Length(10); });
            Property(x => x.DocumentType, map => { map.NotNullable(true); map.Length(50); });
            Property(x => x.Created);
            Property(x => x.Modified);
            Property(x => x.CreatedBy);
            Property(x => x.ModifiedBy);
        }
    }
}
 