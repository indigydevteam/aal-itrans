﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using NHibernate;


namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
    public class NewsFileRepositoryAsync : GenericRepositoryAsync<Domain.NewsFile>, INewsFileRepositoryAsync
    {
        public NewsFileRepositoryAsync(ISession session) : base(session)
        { }
    }
}
