﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using NHibernate;
using NHibernate.Linq;
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
    public class GenericRepositoryAsync<T> : IGenericRepositoryAsync<T> where T : class
    {
        protected readonly ISession Session;

        public GenericRepositoryAsync(ISession session)
        {
            Session = session;
        }

        public virtual async Task<T> AddAsync(T entity)
        {

            await Session.SaveAsync(entity).ConfigureAwait(false);
            await Session.FlushAsync().ConfigureAwait(false);
            return entity;
        }

        public virtual async Task<T> DeleteAsync(T entity)
        {
            await Session.DeleteAsync(entity).ConfigureAwait(false);
            await Session.FlushAsync().ConfigureAwait(false);
            return entity;
        }

        public virtual async Task<IQueryable<T>> FindByCondition(Expression<Func<T, bool>> expression)
        {
            return (await (Session.Query<T>()
                .Where(expression).ToListAsync().ConfigureAwait(false))).AsQueryable();
        }
        public virtual async Task<IQueryable<T>> FindByCondition(Expression<Func<T, bool>> expression, Expression<Func<T, decimal>> order, bool isDescending)
        {
            if (expression == null)
            {
                if (order == null)
                {
                    return (await Session.Query<T>().ToListAsync().ConfigureAwait(false)).AsQueryable();
                }
                else
                {
                    if (isDescending)
                    {

                        return (await Session.Query<T>().OrderByDescending(order).ToListAsync().ConfigureAwait(false)).AsQueryable();
                    }
                    else
                    {

                        return (await Session.Query<T>().OrderBy(order).ToListAsync().ConfigureAwait(false)).AsQueryable();
                    }
                }
            }
            else
            {
                if (order == null)
                {
                    return (await Session.Query<T>().Where(expression).ToListAsync().ConfigureAwait(false)).AsQueryable();
                }
                else
                {
                    if (isDescending)
                    {
                        return (await Session.Query<T>().Where(expression).OrderByDescending(order).ToListAsync().ConfigureAwait(false)).AsQueryable();
                    }
                    else
                    {

                        return (await Session.Query<T>().Where(expression).OrderBy(order).ToListAsync().ConfigureAwait(false)).AsQueryable();
                    }
                }
            }
        }
        public virtual async Task<IQueryable<T>> FindByCondition(Expression<Func<T, bool>> expression, Expression<Func<T, DateTime>> order, bool isDescending)
        {
            if (expression == null)
            {
                if (order == null)
                {
                    return (await Session.Query<T>().ToListAsync().ConfigureAwait(false)).AsQueryable();
                }
                else
                {
                    if (isDescending)
                    {

                        return (await Session.Query<T>().OrderByDescending(order).ToListAsync().ConfigureAwait(false)).AsQueryable();
                    }
                    else
                    {

                        return (await Session.Query<T>().OrderBy(order).ToListAsync().ConfigureAwait(false)).AsQueryable();
                    }
                }
            }
            else
            {
                if (order == null)
                {
                    return (await Session.Query<T>().Where(expression).ToListAsync().ConfigureAwait(false)).AsQueryable();
                }
                else
                {
                    if (isDescending)
                    {
                        return (await Session.Query<T>().Where(expression).OrderByDescending(order).ToListAsync().ConfigureAwait(false)).AsQueryable();
                    }
                    else
                    {

                        return (await Session.Query<T>().Where(expression).OrderBy(order).ToListAsync().ConfigureAwait(false)).AsQueryable();
                    }
                }
            }
        }
        public virtual async Task<IQueryable<T>> GetAllAsync()
        {
            return (await Session.Query<T>().ToListAsync().ConfigureAwait(false)).AsQueryable();
        }

        public virtual async Task<IQueryable<T>> GetPagedResponseAsync(int pageNumber, int pageSize)
        {
            if (pageNumber == 0) pageNumber = 1;
            if (pageSize == 0) pageSize = 5;
            return (await Session.Query<T>().Skip((pageNumber - 1) * pageSize).Take(pageSize).ToListAsync().ConfigureAwait(false)).AsQueryable();
        }

        public virtual async Task<IQueryable<T>> FindByConditionWithPage(Expression<Func<T, bool>> expression, int pageNumber, int pageSize)
        {
            if (expression == null)
            {
                if (pageNumber != 0 && pageSize != 0)
                    return (await Session.Query<T>().Skip((pageNumber - 1) * pageSize).Take(pageSize).ToListAsync().ConfigureAwait(false)).AsQueryable();
                else
                    return (await Session.Query<T>().ToListAsync().ConfigureAwait(false)).AsQueryable();
            }
            else
            {
                if (pageNumber != 0 && pageSize != 0)
                    return (await Session.Query<T>()
                            .Where(expression).Skip((pageNumber - 1) * pageSize).Take(pageSize).ToListAsync().ConfigureAwait(false)).AsQueryable();
                else
                    return (await Session.Query<T>()
                   .Where(expression).ToListAsync().ConfigureAwait(false)).AsQueryable();
            }
        }

        public virtual async Task<IQueryable<T>> FindByConditionWithPage(Expression<Func<T, bool>> expression, Expression<Func<T, DateTime>> order, bool isDescending, int pageNumber, int pageSize)
        {
            if (expression == null)
            {
                if (order == null)
                {
                    if (pageNumber != 0 && pageSize != 0)
                        return (await Session.Query<T>().Skip((pageNumber - 1) * pageSize).Take(pageSize).ToListAsync().ConfigureAwait(false)).AsQueryable();
                    else
                        return (await Session.Query<T>().ToListAsync().ConfigureAwait(false)).AsQueryable();
                }
                else
                {
                    if (isDescending)
                    {
                        if (pageNumber != 0 && pageSize != 0)
                            return (await Session.Query<T>().OrderByDescending(order).Skip((pageNumber - 1) * pageSize).Take(pageSize).ToListAsync().ConfigureAwait(false)).AsQueryable();
                        else
                            return (await Session.Query<T>().OrderByDescending(order).ToListAsync().ConfigureAwait(false)).AsQueryable();
                    }
                    else
                    {
                        if (pageNumber != 0 && pageSize != 0)
                            return (await Session.Query<T>().OrderBy(order).Skip((pageNumber - 1) * pageSize).Take(pageSize).ToListAsync().ConfigureAwait(false)).AsQueryable();
                        else
                            return (await Session.Query<T>().OrderBy(order).ToListAsync().ConfigureAwait(false)).AsQueryable();
                    }
                }
            }
            else
            {
                if (order == null)
                {
                    if (pageNumber != 0 && pageSize != 0)
                        return (await Session.Query<T>()
                                .Where(expression).Skip((pageNumber - 1) * pageSize).Take(pageSize).ToListAsync().ConfigureAwait(false)).AsQueryable();
                    else
                        return (await Session.Query<T>()
                       .Where(expression).ToListAsync().ConfigureAwait(false)).AsQueryable();
                }
                else
                {
                    if (isDescending)
                    {
                        if (pageNumber != 0 && pageSize != 0)
                            return (await Session.Query<T>()
                                    .Where(expression).OrderByDescending(order).Skip((pageNumber - 1) * pageSize).Take(pageSize).ToListAsync().ConfigureAwait(false)).AsQueryable();
                        else
                            return (await Session.Query<T>()
                           .Where(expression).OrderByDescending(order).ToListAsync().ConfigureAwait(false)).AsQueryable();
                    }
                    else
                    {
                        if (pageNumber != 0 && pageSize != 0)
                            return (await Session.Query<T>()
                                    .Where(expression).OrderBy(order).Skip((pageNumber - 1) * pageSize).Take(pageSize).ToListAsync().ConfigureAwait(false)).AsQueryable();
                        else
                            return (await Session.Query<T>()
                           .Where(expression).OrderBy(order).ToListAsync().ConfigureAwait(false)).AsQueryable();
                    }
                }
            }
        }

        public virtual async Task<IQueryable<T>> FindByConditionWithPage(Expression<Func<T, bool>> expression, Expression<Func<T, decimal>> order, bool isDescending, int pageNumber, int pageSize)
        {
            if (expression == null)
            {
                if (order == null)
                {
                    if (pageNumber != 0 && pageSize != 0)
                        return (await Session.Query<T>().Skip((pageNumber - 1) * pageSize).Take(pageSize).ToListAsync().ConfigureAwait(false)).AsQueryable();
                    else
                        return (await Session.Query<T>().ToListAsync().ConfigureAwait(false)).AsQueryable();
                }
                else
                {
                    if (isDescending)
                    {
                        if (pageNumber != 0 && pageSize != 0)
                            return (await Session.Query<T>().OrderByDescending(order).Skip((pageNumber - 1) * pageSize).Take(pageSize).ToListAsync().ConfigureAwait(false)).AsQueryable();
                        else
                            return (await Session.Query<T>().OrderByDescending(order).ToListAsync().ConfigureAwait(false)).AsQueryable();
                    }
                    else
                    {
                        if (pageNumber != 0 && pageSize != 0)
                            return (await Session.Query<T>().OrderBy(order).Skip((pageNumber - 1) * pageSize).Take(pageSize).ToListAsync().ConfigureAwait(false)).AsQueryable();
                        else
                            return (await Session.Query<T>().OrderBy(order).ToListAsync().ConfigureAwait(false)).AsQueryable();
                    }
                }
            }
            else
            {
                if (order == null)
                {
                    if (pageNumber != 0 && pageSize != 0)
                        return (await Session.Query<T>()
                                .Where(expression).Skip((pageNumber - 1) * pageSize).Take(pageSize).ToListAsync().ConfigureAwait(false)).AsQueryable();
                    else
                        return (await Session.Query<T>()
                       .Where(expression).ToListAsync().ConfigureAwait(false)).AsQueryable();
                }
                else
                {
                    if (isDescending)
                    {
                        if (pageNumber != 0 && pageSize != 0)
                            return (await Session.Query<T>()
                                    .Where(expression).OrderByDescending(order).Skip((pageNumber - 1) * pageSize).Take(pageSize).ToListAsync().ConfigureAwait(false)).AsQueryable();
                        else
                            return (await Session.Query<T>()
                           .Where(expression).OrderByDescending(order).ToListAsync().ConfigureAwait(false)).AsQueryable();
                    }
                    else
                    {
                        if (pageNumber != 0 && pageSize != 0)
                            return (await Session.Query<T>()
                                    .Where(expression).OrderBy(order).Skip((pageNumber - 1) * pageSize).Take(pageSize).ToListAsync().ConfigureAwait(false)).AsQueryable();
                        else
                            return (await Session.Query<T>()
                           .Where(expression).OrderBy(order).ToListAsync().ConfigureAwait(false)).AsQueryable();
                    }
                }
            }
        }

        public virtual async Task<IQueryable<T>> FindByConditionWithPage(Expression<Func<T, bool>> expression, Expression<Func<T, string>> order, bool isDescending, int pageNumber, int pageSize)
        {
            if (expression == null)
            {
                if (order == null)
                {
                    if (pageNumber != 0 && pageSize != 0)
                        return (await Session.Query<T>().Skip((pageNumber - 1) * pageSize).Take(pageSize).ToListAsync().ConfigureAwait(false)).AsQueryable();
                    else
                        return (await Session.Query<T>().ToListAsync().ConfigureAwait(false)).AsQueryable();
                }
                else
                {
                    if (isDescending)
                    {
                        if (pageNumber != 0 && pageSize != 0)
                            return (await Session.Query<T>().OrderByDescending(order).Skip((pageNumber - 1) * pageSize).Take(pageSize).ToListAsync().ConfigureAwait(false)).AsQueryable();
                        else
                            return (await Session.Query<T>().OrderByDescending(order).ToListAsync().ConfigureAwait(false)).AsQueryable();
                    }
                    else
                    {
                        if (pageNumber != 0 && pageSize != 0)
                            return (await Session.Query<T>().OrderBy(order).Skip((pageNumber - 1) * pageSize).Take(pageSize).ToListAsync().ConfigureAwait(false)).AsQueryable();
                        else
                            return (await Session.Query<T>().OrderBy(order).ToListAsync().ConfigureAwait(false)).AsQueryable();
                    }
                }
            }
            else
            {
                if (order == null)
                {
                    if (pageNumber != 0 && pageSize != 0)
                        return (await Session.Query<T>()
                                .Where(expression).Skip((pageNumber - 1) * pageSize).Take(pageSize).ToListAsync().ConfigureAwait(false)).AsQueryable();
                    else
                        return (await Session.Query<T>()
                       .Where(expression).ToListAsync().ConfigureAwait(false)).AsQueryable();
                }
                else
                {
                    if (isDescending)
                    {
                        if (pageNumber != 0 && pageSize != 0)
                            return (await Session.Query<T>()
                                    .Where(expression).OrderByDescending(order).Skip((pageNumber - 1) * pageSize).Take(pageSize).ToListAsync().ConfigureAwait(false)).AsQueryable();
                        else
                            return (await Session.Query<T>()
                           .Where(expression).OrderByDescending(order).ToListAsync().ConfigureAwait(false)).AsQueryable();
                    }
                    else
                    {
                        if (pageNumber != 0 && pageSize != 0)
                            return (await Session.Query<T>()
                                    .Where(expression).OrderBy(order).Skip((pageNumber - 1) * pageSize).Take(pageSize).ToListAsync().ConfigureAwait(false)).AsQueryable();
                        else
                            return (await Session.Query<T>()
                           .Where(expression).OrderBy(order).ToListAsync().ConfigureAwait(false)).AsQueryable();
                    }
                }
            }
        }

        public virtual async Task<IQueryable<T>> FindByConditionWithItemNumber(Expression<Func<T, bool>> expression, Expression<Func<T, decimal>> order, bool isDescending, int itemStart, int itemCount)
        {
            if (expression == null)
            {
                if (order == null)
                {
                    if (itemStart >= 0 && itemCount >= 0)
                        return (await Session.Query<T>().Skip((itemStart - 1)).Take(itemCount).ToListAsync().ConfigureAwait(false)).AsQueryable();
                    else
                        return (await Session.Query<T>().ToListAsync().ConfigureAwait(false)).AsQueryable();
                }
                else
                {
                    if (isDescending)
                    {
                        if (itemStart >= 0 && itemCount >= 0)
                            return (await Session.Query<T>().OrderByDescending(order).Skip((itemStart - 1)).Take(itemCount).ToListAsync().ConfigureAwait(false)).AsQueryable();
                        else
                            return (await Session.Query<T>().OrderByDescending(order).ToListAsync().ConfigureAwait(false)).AsQueryable();
                    }
                    else
                    {
                        if (itemStart >= 0 && itemCount >= 0)
                            return (await Session.Query<T>().OrderBy(order).Skip((itemStart - 1)).Take(itemCount).ToListAsync().ConfigureAwait(false)).AsQueryable();
                        else
                            return (await Session.Query<T>().OrderBy(order).ToListAsync().ConfigureAwait(false)).AsQueryable();
                    }
                }
            }
            else
            {
                if (order == null)
                {
                    if (itemStart >= 0 && itemCount >= 0)
                        return (await Session.Query<T>()
                                .Where(expression).Skip((itemStart - 1)).Take(itemCount).ToListAsync().ConfigureAwait(false)).AsQueryable();
                    else
                        return (await Session.Query<T>()
                       .Where(expression).ToListAsync().ConfigureAwait(false)).AsQueryable();
                }
                else
                {
                    if (isDescending)
                    {
                        if (itemStart >= 0 && itemCount >= 0)
                            return (await Session.Query<T>()
                                    .Where(expression).OrderByDescending(order).Skip((itemStart - 1)).Take(itemCount).ToListAsync().ConfigureAwait(false)).AsQueryable();
                        else
                            return (await Session.Query<T>()
                           .Where(expression).OrderByDescending(order).ToListAsync().ConfigureAwait(false)).AsQueryable();
                    }
                    else
                    {
                        if (itemStart >= 0 && itemCount >= 0)
                            return (await Session.Query<T>()
                                    .Where(expression).OrderBy(order).Skip((itemStart - 1) ).Take(itemCount).ToListAsync().ConfigureAwait(false)).AsQueryable();
                        else
                            return (await Session.Query<T>()
                           .Where(expression).OrderBy(order).ToListAsync().ConfigureAwait(false)).AsQueryable();
                    }
                }
            }
        }
        public virtual async Task<T> UpdateAsync(T entity)
        {
            await Session.SaveOrUpdateAsync(entity).ConfigureAwait(false);
            await Session.FlushAsync().ConfigureAwait(false);
            return entity;
        }


        public virtual async Task<IQueryable<T>> GetSession()
        {
            return Session.Query<T>();
        }

        public virtual async Task<IQuery> CreateSQLQuery(string sql)
        {
            return Session.CreateSQLQuery(sql);
        }
        public virtual async Task<IQuery> CreateStoreProc(string spName)
        {

            return Session.GetNamedQuery(spName);
        }

        public virtual int GetItemCount(Expression<Func<T, bool>> expression)
        {

            if (expression == null)
                return Session.Query<T>().ToFutureValue(x => x.Count()).Value;
            else
                return Session.Query<T>().Where(expression).ToFutureValue(x => x.Count()).Value;

        }
    }
}
