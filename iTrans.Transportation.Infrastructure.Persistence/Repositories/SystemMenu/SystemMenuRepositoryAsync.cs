﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using NHibernate;


namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
    public class SystemMenuRepositoryAsync : GenericRepositoryAsync<Domain.SystemMenu>, ISystemMenuRepositoryAsync
    {
        public SystemMenuRepositoryAsync(ISession session) : base(session)
        { }
    }
}
