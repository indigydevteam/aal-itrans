﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using NHibernate;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
   public class OrderingProductFileRepositoryAsync : GenericRepositoryAsync<Domain.OrderingProductFile>, IOrderingProductFileRepositoryAsync
    {
        public OrderingProductFileRepositoryAsync(ISession session) : base(session)
        { }
    }
}
