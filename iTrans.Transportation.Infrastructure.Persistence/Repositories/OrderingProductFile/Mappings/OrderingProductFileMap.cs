﻿using iTrans.Transportation.Domain;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using System;
namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
    public partial class OrderingProductFileMap : ClassMapping<Domain.OrderingProductFile>
    {
        public OrderingProductFileMap()
        {
            Table("Ordering_ProductFile");
            Lazy(true);
            Id(x => x.Id, map => map.Generator(Generators.Identity));
            //Property(x => x.OrderingProductId, map => { map.NotNullable(true); });
            Property(x => x.FileName, map => { map.NotNullable(true); });
            Property(x => x.ContentType, map => { map.NotNullable(true); });
            Property(x => x.FilePath, map => { map.NotNullable(true); });
            Property(x => x.DirectoryPath, map => { map.NotNullable(true); });
            Property(x => x.FileEXT, map => { map.NotNullable(true); });
            Property(x => x.Created);
            Property(x => x.Modified);
            Property(x => x.CreatedBy);
            Property(x => x.ModifiedBy);
            ManyToOne(x => x.OrderingProduct, map => { map.Column("OrderingProductId"); map.Fetch(FetchKind.Join); map.NotFound(NotFoundMode.Ignore); });
        }
    }
}
