﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using NHibernate;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
   public class QRCodeRepositoryAsync : GenericRepositoryAsync<Domain.QRCode>, IQRCodeRepositoryAsync
    {
        public QRCodeRepositoryAsync(ISession session) : base(session)
        { }
    }
}
