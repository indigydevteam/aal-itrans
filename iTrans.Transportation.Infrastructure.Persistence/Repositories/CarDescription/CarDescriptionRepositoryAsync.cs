﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using NHibernate;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
    public class CarDescriptionRepositoryAsync : GenericRepositoryAsync<Domain.CarDescription>, ICarDescriptionRepositoryAsync
    {
        public CarDescriptionRepositoryAsync(ISession session) : base(session)
        { }
    }
}
