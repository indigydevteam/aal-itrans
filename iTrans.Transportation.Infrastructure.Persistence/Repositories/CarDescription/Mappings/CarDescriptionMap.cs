﻿using iTrans.Transportation.Domain;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using System;


namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
    public partial class CarDescriptionMap : ClassMapping<Domain.CarDescription>
    {
        public CarDescriptionMap()
        {
            Table("Car_Description");
            Lazy(true);
            Id(x => x.Id, map => map.Generator(Generators.Identity));
            //Property(x => x.CarListId);
            Property(x => x.Name_TH, map => { map.NotNullable(true); map.Length(50); });
            Property(x => x.Name_ENG, map => { map.NotNullable(true); map.Length(50); });
            Property(x => x.Sequence);
            Property(x => x.Specified);
            Property(x => x.Active);
            Property(x => x.Created);
            Property(x => x.Modified);
            Property(x => x.CreatedBy);
            Property(x => x.ModifiedBy);
            OneToOne(x => x.File, map =>
            {
                map.PropertyReference(typeof(CarDescriptionFile).GetProperty("CarDescription"));
                map.Cascade(Cascade.All);
            });
            ManyToOne(x => x.CarList, map => { map.Column("CarListId"); map.Fetch(FetchKind.Select); map.NotFound(NotFoundMode.Ignore); });
        }
    }
}
