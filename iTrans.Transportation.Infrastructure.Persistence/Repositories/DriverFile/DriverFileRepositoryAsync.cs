﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using NHibernate;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
   public class DriverFileRepositoryAsync : GenericRepositoryAsync<Domain.DriverFile>, IDriverFileRepositoryAsync
    {
        public DriverFileRepositoryAsync(ISession session) : base(session)
        { }
    }
}
