﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using NHibernate;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
   public class CarListFileRepositoryAsync : GenericRepositoryAsync<Domain.CarListFile>, ICarListFileRepositoryAsync
    {
        public CarListFileRepositoryAsync(ISession session) : base(session)
        { }
    }
}
