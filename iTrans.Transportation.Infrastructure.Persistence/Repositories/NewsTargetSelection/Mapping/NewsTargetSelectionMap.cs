﻿using iTrans.Transportation.Domain;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using System;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories.NewsTargetSelection.Mapping
{
    public partial class NewsTargetSelectionMap : ClassMapping<Domain.NewsTargetSelection>
    {
        public NewsTargetSelectionMap()
        {
            Table("NewsTargetSelection");
            Lazy(true);
            Id(x => x.Id, map => map.Generator(Generators.Identity));
            Property(x => x.NewsId);
            Property(x => x.UserId);
            Property(x => x.Created);
            Property(x => x.Modified);
            Property(x => x.CreatedBy);
            Property(x => x.ModifiedBy);
        } 
    }
}
