﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using NHibernate;


namespace iTrans.Transportation.Infrastructure.Persistence.Repositories.NewsTargetSelection
{
    class NewsTargetSelectionRepositoryAsync : GenericRepositoryAsync<Domain.NewsTargetSelection>, INewsTargetSelectionRepositoryAsync
    {
        public NewsTargetSelectionRepositoryAsync(ISession session) : base(session)
        { }
    }
}
