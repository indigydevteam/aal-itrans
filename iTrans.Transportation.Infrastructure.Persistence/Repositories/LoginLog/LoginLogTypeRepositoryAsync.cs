using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Domain;
using NHibernate;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
    public class LoginLogTypeRepositoryAsync : GenericRepositoryAsync<LoginLogType>, ILoginLogTypeRepositoryAsync
    {
        public LoginLogTypeRepositoryAsync(ISession session) : base(session)
        { }
    }
}