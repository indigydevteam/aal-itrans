using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Domain;
using NHibernate;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
    public class LoginLogRepositoryAsync: GenericRepositoryAsync<LoginLog>, ILoginLogRepositoryAsync
    {
        public LoginLogRepositoryAsync(ISession session) : base(session)
        { }
    }
}