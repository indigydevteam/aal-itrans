﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories.ContainerSpecification
{
    public class ContainerSpecificationRepositoryAsync : GenericRepositoryAsync<Domain.ContainerSpecification>, IContainerSpecificationRepositoryAsync
    {
        public ContainerSpecificationRepositoryAsync(ISession session) : base(session)
        { }
    }
}
