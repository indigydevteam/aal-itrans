﻿using iTrans.Transportation.Domain;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using System;


namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
    public partial class DriverInboxMap : ClassMapping<Domain.DriverInbox>
    {
        public DriverInboxMap()
        {
            Table("Driver_Inbox");
            Lazy(true);
            Id(x => x.Id, map => map.Generator(Generators.Identity));
            //Property(x => x.CarListId);
            Property(x => x.DriverId, map => { map.NotNullable(true);});
            Property(x => x.Title);
            Property(x => x.Title_ENG);
            Property(x => x.Module);
            Property(x => x.Module_ENG);
            Property(x => x.Content);
            Property(x => x.FromUser);
            Property(x => x.IsDelete);
            Property(x => x.Created);
            Property(x => x.Modified);
            Property(x => x.CreatedBy);
            Property(x => x.ModifiedBy);
        }
    }
}
