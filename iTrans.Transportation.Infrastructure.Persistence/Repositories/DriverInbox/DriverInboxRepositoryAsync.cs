﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using NHibernate;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
    public class DriverInboxRepositoryAsync : GenericRepositoryAsync<Domain.DriverInbox>, IDriverInboxRepositoryAsync
    {
        public DriverInboxRepositoryAsync(ISession session) : base(session)
        { }
    }
}
