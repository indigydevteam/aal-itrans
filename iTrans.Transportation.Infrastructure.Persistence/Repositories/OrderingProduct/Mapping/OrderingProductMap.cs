﻿using iTrans.Transportation.Domain;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using System;


namespace iTrans.Transportation.Infrastructure.Persistence.Repositories.OrderingProduct.Mapping
{
   public partial class OrderingProductMap : ClassMapping<Domain.OrderingProduct>
    {
        public OrderingProductMap()
        {
            Table("Ordering_Product");
            Lazy(true);
            Id(x => x.Id, map => map.Generator(Generators.Identity));
            //Property(x => x.OrderingId, map => { map.NotNullable(true); });
            Property(x => x.CustomerProductId);
            Property(x => x.Name);
            Property(x => x.ProductType);
            Property(x => x.ProductTypeDetail);
            Property(x => x.Packaging);
            Property(x => x.PackagingDetail);
            Property(x => x.Width);
            Property(x => x.Length);
            Property(x => x.Height);
            Property(x => x.Weight);
            Property(x => x.Quantity);
            Property(x => x.Sequence);
            Property(x => x.Created);
            Property(x => x.Modified);
            Property(x => x.CreatedBy);
            Property(x => x.ModifiedBy);
            ManyToOne(o => o.Container,
                 o =>
                 {
                     o.Column("OrderingContainerId");
                 });
            Bag(x => x.ProductFiles, colmap => { colmap.Key(x => x.Column("OrderingProductId")); colmap.Inverse(true); colmap.Cascade(Cascade.Persist); }, map => { map.OneToMany(a => a.Class(typeof(Domain.OrderingProductFile))); });
            ManyToOne(x => x.Ordering, map => { map.Column("OrderingId"); map.Fetch(FetchKind.Join); map.NotFound(NotFoundMode.Ignore); });
            //ManyToOne(x => x.ProductType, map => { map.Column("ProductTypeId"); map.Fetch(FetchKind.Select); map.NotFound(NotFoundMode.Ignore); });
            //ManyToOne(x => x.Packaging, map => { map.Column("PackagingId"); map.Fetch(FetchKind.Select); map.NotFound(NotFoundMode.Ignore); });
        }
    }
}