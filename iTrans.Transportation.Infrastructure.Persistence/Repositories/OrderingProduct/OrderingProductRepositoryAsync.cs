﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories.OrderingProduct
{
    class OrderingProductRepositoryAsync : GenericRepositoryAsync<Domain.OrderingProduct>, IOrderingProductRepositoryAsync
    {
        public OrderingProductRepositoryAsync(ISession session) : base(session)
        { }
    }
}