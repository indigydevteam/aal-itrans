﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using NHibernate;


namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
    public class SubdistrictRepositoryAsync : GenericRepositoryAsync<Domain.Subdistrict>, ISubdistrictRepositoryAsync
    {
        public SubdistrictRepositoryAsync(ISession session) : base(session)
        { }
    }
}
