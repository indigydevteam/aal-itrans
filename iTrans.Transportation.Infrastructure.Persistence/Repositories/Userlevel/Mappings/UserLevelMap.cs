﻿using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories.Userlevel.Mappings
{
    public partial class UserLevelMap : ClassMapping<Domain.UserLevel>
    {
        public UserLevelMap()
        {
            Table("UserLevel");
            Lazy(true);
            Id(x => x.Id, map => map.Generator(Generators.Identity));
            Property(x => x.Level);
            Property(x => x.Module, map => { map.NotNullable(true); map.Length(50); });
            Property(x => x.Name_TH, map => { map.NotNullable(true); map.Length(50); });
            Property(x => x.Name_ENG, map => { map.NotNullable(true); map.Length(50); });
            Property(x => x.IsDelete);
            Property(x => x.Star);
            Property(x => x.Active);
            Property(x => x.IsCalculateLevel);
            Property(x => x.Created);
            Property(x => x.Modified);
            Property(x => x.CreatedBy);
            Property(x => x.ModifiedBy);
            Bag(x => x.Conditions, colmap => { colmap.Key(x => x.Column("UserLevelId")); colmap.Inverse(true); colmap.Cascade(Cascade.Persist); }, map => { map.OneToMany(a => a.Class(typeof(Domain.UserLevelCondition))); });
        }
    }
}
