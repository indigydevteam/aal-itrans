﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories.Userlevel
{
   public class UserLevelRepositoryAsync : GenericRepositoryAsync<Domain.UserLevel>, IUserLevelRepositoryAsync
    {
        public UserLevelRepositoryAsync(ISession session) : base(session)
        { }
    }
}
