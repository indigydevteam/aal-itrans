﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using NHibernate;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories 
{
    public class DriverAddressRepositoryAsync : GenericRepositoryAsync<Domain.DriverAddress>, IDriverAddressRepositoryAsync
    {
        public DriverAddressRepositoryAsync(ISession session) : base(session)
        { }
    }
}
