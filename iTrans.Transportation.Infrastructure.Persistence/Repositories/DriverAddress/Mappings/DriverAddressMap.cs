﻿using iTrans.Transportation.Domain;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using System;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
    public partial class DriverAddressMap : ClassMapping<Domain.DriverAddress>
    {
        public DriverAddressMap()
        {
            Table("Driver_Address");
            Lazy(true);
            Id(x => x.Id, map => map.Generator(Generators.Identity));
            Property(x => x.DriverId, map => { map.NotNullable(true); });
            //Property(x => x.Country);
            //Property(x => x.Province);
            //Property(x => x.District);
            //Property(x => x.Subdistrict);
            Property(x => x.PostCode, map => {  map.Length(50); });
            Property(x => x.Road, map => {  map.Length(250); });
            Property(x => x.Alley, map => {  map.Length(250); });
            Property(x => x.Address, map => {  map.Length(250); });
            Property(x => x.Branch, map => {  map.Length(50); });
            Property(x => x.AddressType, map => {  map.Length(50); });
            Property(x => x.AddressName, map => {  map.Length(250); });
            Property(x => x.ContactPerson, map => {  map.Length(250); });
            Property(x => x.ContactPhoneCode, map => { map.Length(50); });
            Property(x => x.ContactPhoneNumber, map => {  map.Length(250); });
            Property(x => x.ContactEmail, map => {  map.Length(250); });
            Property(x => x.Maps, map => {  map.Length(250); });
            Property(x => x.IsMainData);
            Property(x => x.Sequence);
            Property(x => x.Created);
            Property(x => x.Modified);
            Property(x => x.CreatedBy);
            Property(x => x.ModifiedBy);
            ManyToOne(x => x.Country, map => { map.Column("CountryId");map.Fetch(FetchKind.Select); map.NotFound(NotFoundMode.Ignore); });
            ManyToOne(x => x.Province, map => { map.Column("ProvinceId"); map.Fetch(FetchKind.Select); map.NotFound(NotFoundMode.Ignore); });
            ManyToOne(x => x.District, map => { map.Column("DistrictId");  map.Fetch(FetchKind.Select); map.NotFound(NotFoundMode.Ignore); });
            ManyToOne(x => x.Subdistrict, map => { map.Column("SubdistrictId"); map.Fetch(FetchKind.Select); map.NotFound(NotFoundMode.Ignore);  });
        }
    }
}
