﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories.MenuPermission
{
    public class MenuPermissionRepositoryAsync : GenericRepositoryAsync<Domain.Entities.MenuPermission>, IMenuPermissionRepositoryAsync
    {
        public MenuPermissionRepositoryAsync(ISession session) : base(session)
        {
        }
    }
}
