﻿using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories.MenuPermission.Mapping
{
    public class MenuPermissionMap : ClassMapping<Domain.Entities.MenuPermission>
    {
        public MenuPermissionMap()
        {
            Table("MenuPermission");
            Lazy(true);
            Id(x => x.Id, map => map.Generator(Generators.Identity));
            Property(x => x.MenuId);
            Property(x => x.Permission);
            Property(x => x.Created);
            Property(x => x.Modified);
            Property(x => x.CreatedBy);
            Property(x => x.ModifiedBy);
        }
    }
}
