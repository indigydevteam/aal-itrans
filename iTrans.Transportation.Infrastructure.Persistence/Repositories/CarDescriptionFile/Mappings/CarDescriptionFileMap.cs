﻿using iTrans.Transportation.Domain;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using System;
namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
    public partial class CarDescriptionFileMap : ClassMapping<Domain.CarDescriptionFile>
    {
        public CarDescriptionFileMap()
        {
            Table("CarDescription_File");
            Lazy(true);
            Id(x => x.Id, map => map.Generator(Generators.Identity));
            //Property(x => x.CarTypeId, map => { map.NotNullable(true); });
            Property(x => x.FileName, map => { map.NotNullable(true); });
            Property(x => x.ContentType, map => { map.NotNullable(true); });
            Property(x => x.FilePath, map => { map.NotNullable(true); });
            Property(x => x.DirectoryPath, map => { map.NotNullable(true); });
            Property(x => x.FileEXT, map => { map.NotNullable(true); });
            Property(x => x.Created);
            Property(x => x.Modified);
            Property(x => x.CreatedBy);
            Property(x => x.ModifiedBy);
            ManyToOne(o => o.CarDescription,
                 o =>
                 {
                     o.Column("CarDescriptionId");
                     //o.Unique(true);
                     //o.ForeignKey("CarTypes_CarTypeFiles_FK1");
                 });
        }
    }
}
