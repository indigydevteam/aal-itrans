﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using NHibernate;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
   public class CarDescriptionFileRepositoryAsync : GenericRepositoryAsync<Domain.CarDescriptionFile>, ICarDescriptionFileRepositoryAsync
    {
        public CarDescriptionFileRepositoryAsync(ISession session) : base(session)
        { }
    }
}
