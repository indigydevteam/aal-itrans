﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using NHibernate;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
    public class ChatGroupMessageRepositoryAsync : GenericRepositoryAsync<Domain.ChatGroupMessage>, IChatGroupMessageRepositoryAsync
    {
        public ChatGroupMessageRepositoryAsync(ISession session) : base(session)
        { }
    }
}
