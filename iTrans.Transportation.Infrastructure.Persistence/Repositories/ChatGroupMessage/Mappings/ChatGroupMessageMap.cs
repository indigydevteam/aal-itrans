﻿using iTrans.Transportation.Domain;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using System;


namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
    public partial class ChatGroupMessageMap : ClassMapping<Domain.ChatGroupMessage>
    {
        public ChatGroupMessageMap()
        {
            Table("Chat_GroupMessage");
            Lazy(true);
            Id(x => x.Id, map => map.Generator(Generators.Identity));
            //Property(x => x.CarListId);
            Property(x => x.Content, map => { map.NotNullable(true);});
            Property(x => x.Timestamp);
            Property(x => x.FromUserId);
            Property(x => x.ToRoomId);
            Property(x => x.ContentType);
            Property(x => x.Content);
            Property(x => x.IsRead);
            Property(x => x.IsDelete);
        }
    }
}
