﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using NHibernate;


namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
    public class NotificationRepositoryAsync : GenericRepositoryAsync<Domain.Notification>, INotificationRepositoryAsync
    {
        public NotificationRepositoryAsync(ISession session) : base(session)
        { } 
    }
}
