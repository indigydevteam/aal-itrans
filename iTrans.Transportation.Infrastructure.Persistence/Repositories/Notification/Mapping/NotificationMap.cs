﻿using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories.Notification.Mapping
{
    public partial class NotificationMap : ClassMapping<Domain.Notification>
    {
        public NotificationMap()
        {
            Table("Notification");
            Lazy(true);
            Id(x => x.Id, map => map.Generator(Generators.Identity));
            Property(x => x.Id);
            Property(x => x.OneSignalId);
            Property(x => x.UserId);
            Property(x => x.OwnerId);
            Property(x => x.Title, map => { map.NotNullable(true); map.Length(250); });
            Property(x => x.Detail, map => { map.NotNullable(true); map.Length(1000); }); 
            Property(x => x.Payload, map => { map.Length(1000); }); 
            Property(x => x.ServiceCode, map => { map.NotNullable(true); map.Length(25); });
            Property(x => x.ReferenceContentKey);
            Property(x => x.IsDelete);
            Property(x => x.IsRead);
            Property(x => x.Created);
            Property(x => x.Modified);
            Property(x => x.CreatedBy);
            Property(x => x.ModifiedBy);
        }
    }
}
