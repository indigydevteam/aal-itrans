﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using NHibernate;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
   public class OrderingDriverFileRepositoryAsync : GenericRepositoryAsync<Domain.OrderingDriverFile>, IOrderingDriverFileRepositoryAsync
    {
        public OrderingDriverFileRepositoryAsync(ISession session) : base(session)
        { }
    }
}
