﻿using iTrans.Transportation.Domain;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using System;
namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
    public partial class OrderingDriverFileMap : ClassMapping<Domain.OrderingDriverFile>
    {
        public OrderingDriverFileMap()
        {
            Table("Ordering_DriverFile");
            Lazy(true);
            Id(x => x.Id, map => map.Generator(Generators.Identity));
            //Property(x => x.OrderingDriverId, map => { map.NotNullable(true); });
            Property(x => x.FileName, map => { map.NotNullable(true); });
            Property(x => x.ContentType, map => { map.NotNullable(true); });
            Property(x => x.FilePath, map => { map.NotNullable(true); });
            Property(x => x.DirectoryPath, map => { map.NotNullable(true); });
            Property(x => x.FileEXT, map => { map.NotNullable(true); });
            Property(x => x.DocumentType);
            Property(x => x.Sequence);
            Property(x => x.Created);
            Property(x => x.Modified);
            Property(x => x.CreatedBy);
            Property(x => x.ModifiedBy);
            ManyToOne(x => x.OrderingDriver, map => { map.Column("OrderingDriverId"); map.Fetch(FetchKind.Join); map.Cascade(Cascade.Persist); });
        }
    }
}
