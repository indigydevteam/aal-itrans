﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using NHibernate;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
    public class CustomerRepositoryAsync : GenericRepositoryAsync<Domain.Customer>, ICustomerRepositoryAsync
    {
        public CustomerRepositoryAsync(ISession session) : base(session)
        { }
    }
}
