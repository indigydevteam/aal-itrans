﻿using iTrans.Transportation.Domain;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using System;


namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
    public partial class ContainerTypeMap : ClassMapping<Domain.ContainerType>
    {
        public ContainerTypeMap()
        {
            Table("Container_Type");
            Lazy(true);
            Id(x => x.Id, map => map.Generator(Generators.Identity));
            Property(x => x.Name_TH, map => { map.NotNullable(true);  });
            Property(x => x.Name_ENG, map => { map.NotNullable(true);  });
            Property(x => x.Sequence);
            Property(x => x.Specified, map => { map.NotNullable(true); });
            Property(x => x.Active);
            Property(x => x.Created);
            Property(x => x.Modified);
            Property(x => x.CreatedBy);
            Property(x => x.ModifiedBy);
        }
    }
}
