﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories.ContainerType
{
    public class ContainerTypeRepositoryAsync : GenericRepositoryAsync<Domain.ContainerType>, IContainerTypeRepositoryAsync
    {
        public ContainerTypeRepositoryAsync(ISession session) : base(session)
        { }
    }
}
