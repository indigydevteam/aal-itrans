﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories.ApplicationLog
{
    public class ApplicationLogRepositoryAsync : GenericRepositoryAsync<Domain.Entities.ApplicationLog>, IApplicationLogRepositoryAsync
    {
        public ApplicationLogRepositoryAsync(ISession session) : base(session)
        { }
    }
}
