﻿using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories.ApplicationLog.Mapping
{
   public partial class ApplicationLogMap : ClassMapping<Domain.Entities.ApplicationLog>
    {
        public ApplicationLogMap()
        {
            Table("Application_Log");
            Lazy(true);
            Id(x => x.Id, map => map.Generator(Generators.Identity));
            Property(x => x.Module);
            Property(x => x.Function, map => { map.NotNullable(true); map.Length(250); });
            Property(x => x.Section, map => { map.NotNullable(true); map.Length(250); });
            Property(x => x.Function);
            Property(x => x.Content);
            Property(x => x.Created);
            Property(x => x.Modified);
            Property(x => x.CreatedBy);
            Property(x => x.ModifiedBy);
        }
    }
}
