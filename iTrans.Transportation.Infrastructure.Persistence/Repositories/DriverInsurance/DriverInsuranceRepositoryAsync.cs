﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories.DriverInsurance
{
   public class DriverInsuranceRepositoryAsync : GenericRepositoryAsync<Domain.Entities.DriverInsurance>, IDriverInsuranceRepositoryAsync
    {
        public DriverInsuranceRepositoryAsync(ISession session) : base(session)
        { }
    }
}
