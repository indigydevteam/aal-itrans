﻿using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories.DriverInsurance.Mapping
{
  public partial  class DriverInsuranceMap : ClassMapping<Domain.Entities.DriverInsurance>
    {
        public DriverInsuranceMap()
        {
            Table("Driver_Insurance");
            Lazy(true);
            Id(x => x.Id, map => map.Generator(Generators.Identity));
            Property(x => x.DriverId);
            Property(x => x.Product, map => { map.NotNullable(true); });
            Property(x => x.ProductType, map => { map.NotNullable(true); });
            Property(x => x.ProductTypeDetail, map => { map.NotNullable(true); });
            Property(x => x.WarrantyLimit, map => { map.NotNullable(true); });
            Property(x => x.WarrantyPeriod, map => { map.NotNullable(true); });
            Property(x => x.TotalAmount, map => { map.NotNullable(true); });
            Property(x => x.Created);
            Property(x => x.Modified);
            Property(x => x.CreatedBy);
            Property(x => x.ModifiedBy);
        }
    }
}
