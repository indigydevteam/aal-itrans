﻿using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories.DriverAnnouncement.Mapping
{
   public partial class DriverAnnouncementMap : ClassMapping<Domain.DriverAnnouncement>
    {
        public DriverAnnouncementMap()
        {
            Table("Driver_Announcement");
            Lazy(true);
            Id(x => x.Id, map => map.Generator(Generators.Identity));
            //Property(x => x.DriverId, map => { map.NotNullable(true); });
            Property(x => x.AcceptProduct);
            Property(x => x.AcceptProductDetail);
            Property(x => x.AcceptPackaging);
            Property(x => x.AcceptPackagingDetail);
            Property(x => x.RejectProduct);
            Property(x => x.RejectProductDetail);
            Property(x => x.RejectPackaging);
            Property(x => x.RejectPackagingDetail);
            Property(x => x.SendDate);
            Property(x => x.ReceiveDate);
            Property(x => x.CarRegistration, map => {  map.Length(50); });       
            Property(x => x.AllRent);
            Property(x => x.Additional);
            Property(x => x.AdditionalDetail);
            Property(x => x.Note);
            Property(x => x.DesiredPrice);
            Property(x => x.Status);
            Property(x => x.StatusReason);
            //Property(x => x.OrderId);
            Property(x => x.CustomerId);
            Property(x => x.Created);
            Property(x => x.Modified);
            Property(x => x.CreatedBy);
            Property(x => x.ModifiedBy);
            Bag(x => x.Locations, colmap => { colmap.Key(x => x.Column("DriverAnnouncementId")); colmap.Inverse(true); colmap.Cascade(Cascade.Persist); }, map => { map.OneToMany(a => a.Class(typeof(Domain.DriverAnnouncementLocation))); });
            ManyToOne(x => x.Driver, map => { map.Column("DriverId"); map.Fetch(FetchKind.Select); map.NotFound(NotFoundMode.Ignore); });
            ManyToOne(x => x.Order, map => { map.Column("OrderId");   map.Fetch(FetchKind.Select); map.NotFound(NotFoundMode.Ignore); });
            //ManyToOne(x => x.AcceptPackaging, map => { map.Column("AcceptProductPackagingId");   map.Fetch(FetchKind.Select); map.NotFound(NotFoundMode.Ignore); });
           // ManyToOne(x => x.RejectProduct, map => { map.Column("RejectProductTypeId");   map.Fetch(FetchKind.Select); map.NotFound(NotFoundMode.Ignore); });
           // ManyToOne(x => x.RejectPackaging, map => { map.Column("RejectProductPackagingId"); map.Fetch(FetchKind.Select); map.NotFound(NotFoundMode.Ignore); });
        }
    }
}
