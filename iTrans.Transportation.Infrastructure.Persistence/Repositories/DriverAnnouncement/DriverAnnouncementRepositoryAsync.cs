﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories.DriverAnnouncement
{
   public class DriverAnnouncementRepositoryAsync : GenericRepositoryAsync<Domain.DriverAnnouncement>, IDriverAnnouncementRepositoryAsync
    {
        public DriverAnnouncementRepositoryAsync(ISession session) : base(session)
        { }
    }
}
