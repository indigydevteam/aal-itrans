﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using NHibernate;


namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
    public class ConfigRepositoryAsync : GenericRepositoryAsync<Domain.Config>, IConfigRepositoryAsync
    {
        public ConfigRepositoryAsync(ISession session) : base(session)
        { }
    }
}
