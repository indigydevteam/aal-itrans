﻿using iTrans.Transportation.Domain;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using System;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
    public partial class ConfigMapp : ClassMapping<Domain.Config>
    {
        public ConfigMapp()
        {
            Table("Config");
            Lazy(true);
            Id(x => x.Id, map => map.Generator(Generators.Identity));
            Property(x => x.Key, map => { map.NotNullable(true); map.Length(50); });
            Property(x => x.Value, map => { map.NotNullable(true); map.Length(250); });
            Property(x => x.Created);
            Property(x => x.Modified);
            Property(x => x.CreatedBy);
            Property(x => x.ModifiedBy);
        }
    }
}
