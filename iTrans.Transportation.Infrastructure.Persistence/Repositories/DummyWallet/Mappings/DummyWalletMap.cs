﻿using iTrans.Transportation.Domain;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using System;


namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
    public partial class DummyWalletMap : ClassMapping<Domain.DummyWallet>
    {
        public DummyWalletMap()
        {
            Table("Dummy_Wallet");
            Lazy(true);
            Id(x => x.Id, map => map.Generator(Generators.Identity));
            Property(x => x.UserId, map => { map.NotNullable(true);  });
            Property(x => x.Module, map => { map.NotNullable(true); });
            Property(x => x.Amount);
            Property(x => x.Created);
            Property(x => x.Modified);
            Property(x => x.CreatedBy);
            Property(x => x.ModifiedBy);
        }
    }
}
