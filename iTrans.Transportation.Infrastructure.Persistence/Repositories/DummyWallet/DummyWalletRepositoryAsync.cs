﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using NHibernate;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
    public class DummyWalletRepositoryAsync : GenericRepositoryAsync<Domain.DummyWallet>, IDummyWalletRepositoryAsync
    {
        public DummyWalletRepositoryAsync(ISession session) : base(session)
        { }
    }
}
