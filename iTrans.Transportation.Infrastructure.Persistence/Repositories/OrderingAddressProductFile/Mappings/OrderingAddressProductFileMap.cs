﻿using iTrans.Transportation.Domain;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using System;
namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
    public partial class OrderingAddressProductFileMap : ClassMapping<Domain.OrderingAddressProductFile>
    {
        public OrderingAddressProductFileMap()
        {
            Table("Ordering_AddressProductFile");
            Lazy(true);
            Id(x => x.Id, map => map.Generator(Generators.Identity));
            //Property(x => x.OrderingAddressProductId, map => { map.NotNullable(true); });
            Property(x => x.FileName, map => { map.NotNullable(true); });
            Property(x => x.ContentType, map => { map.NotNullable(true); });
            Property(x => x.FilePath, map => { map.NotNullable(true); });
            Property(x => x.DirectoryPath);
            Property(x => x.FileEXT, map => { map.NotNullable(true); });
            Property(x => x.Type);
            Property(x => x.Sequence);
            Property(x => x.Created);
            Property(x => x.Modified);
            Property(x => x.CreatedBy);
            Property(x => x.ModifiedBy);
            ManyToOne(x => x.OrderingAddressProduct, map => { map.Column("OrderingAddressProductId"); map.Fetch(FetchKind.Join); map.NotFound(NotFoundMode.Ignore); });
        }
    }
}
