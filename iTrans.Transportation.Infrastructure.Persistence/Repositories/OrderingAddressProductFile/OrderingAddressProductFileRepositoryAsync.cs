﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using NHibernate;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
   public class OrderingAddressProductFileRepositoryAsync : GenericRepositoryAsync<Domain.OrderingAddressProductFile>, IOrderingAddressProductFileRepositoryAsync
    {
        public OrderingAddressProductFileRepositoryAsync(ISession session) : base(session)
        { }
    }
}
