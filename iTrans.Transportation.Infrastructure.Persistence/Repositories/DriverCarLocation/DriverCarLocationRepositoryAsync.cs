﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using NHibernate;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories 
{
    public class DriverCarLocationRepositoryAsync : GenericRepositoryAsync<Domain.DriverCarLocation>, IDriverCarLocationRepositoryAsync
    {
        public DriverCarLocationRepositoryAsync(ISession session) : base(session)
        { }
    }
}
