﻿using iTrans.Transportation.Domain;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using System;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
    public partial class DriverCarLocationMap : ClassMapping<Domain.DriverCarLocation>
    {
        public DriverCarLocationMap()
        {
            Table("Driver_CarLocation");
            Lazy(true);
            Id(x => x.Id, map => map.Generator(Generators.Identity));
            //Property(x => x.DriverCarId);
            //Property(x => x.Country);
            //Property(x => x.Region);
            //Property(x => x.Province);
            //Property(x => x.District);
            Property(x => x.LocationType, map => { map.NotNullable(true); map.Length(250); });
            Property(x => x.Sequence);
            Property(x => x.Created);
            Property(x => x.Modified);
            Property(x => x.CreatedBy);
            Property(x => x.ModifiedBy);
            ManyToOne(x => x.DriverCar, map => { map.Column("DriverCarId"); map.Fetch(FetchKind.Select); map.NotFound(NotFoundMode.Ignore); });
            ManyToOne(x => x.Country, map => { map.Column("CountryId");map.Fetch(FetchKind.Select); map.NotFound(NotFoundMode.Ignore); });
            ManyToOne(x => x.Region, map => { map.Column("RegionId");map.Fetch(FetchKind.Select); map.NotFound(NotFoundMode.Ignore); });
            ManyToOne(x => x.Province, map => { map.Column("ProvinceId");map.Fetch(FetchKind.Select); map.NotFound(NotFoundMode.Ignore); });
            ManyToOne(x => x.District, map => { map.Column("DistrictId");map.Fetch(FetchKind.Select); map.NotFound(NotFoundMode.Ignore); });
        }
    }
}
