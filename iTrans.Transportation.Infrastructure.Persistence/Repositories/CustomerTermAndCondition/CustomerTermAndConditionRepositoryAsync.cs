﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
   public class CustomerTermAndConditionRepositoryAsync : GenericRepositoryAsync<Domain.CustomerTermAndCondition>, ICustomerTermAndConditionRepositoryAsync
    {
        public CustomerTermAndConditionRepositoryAsync(ISession session) : base(session)
        { }
    }
}
