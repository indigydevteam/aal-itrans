﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using NHibernate;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
    public class UserRepositoryAsync : GenericRepositoryAsync<Domain.User>, IUserRepositoryAsync
    {
        public UserRepositoryAsync(ISession session) : base(session)
        { }
    }
}