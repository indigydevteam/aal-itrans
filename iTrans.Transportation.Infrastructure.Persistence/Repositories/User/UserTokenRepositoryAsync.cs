using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Domain;
using NHibernate;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
    public class UserTokenRepositoryAsync : GenericRepositoryAsync<UserToken>, IUserTokenRepositoryAsync
    {
        public UserTokenRepositoryAsync(ISession session) : base(session)
        {
        }
    }
}