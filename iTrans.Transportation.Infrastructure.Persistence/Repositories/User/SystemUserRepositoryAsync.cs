﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories.User
{
  public  class SystemUserRepositoryAsync : GenericRepositoryAsync<Domain.Entities.SystemUsers>, ISystemUserRepositoryAsync
    {
        public SystemUserRepositoryAsync(ISession session) : base(session)
        { }
    }
}