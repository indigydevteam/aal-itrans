﻿using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using System;

namespace iTrans.Transportation.Infrastructure.Persistence.Repositories
{
    public partial class SystemUserMap : ClassMapping<Domain.Entities.SystemUsers>
    {
        public SystemUserMap()
        {
            Table("Users");
            Lazy(true);
            Id(x => x.EmplID);
            Property(x => x.Email);
            Property(x => x.Password);
            Property(x => x.UserToken, map => map.Length(50));
            Property(x => x.IsNaverExpire, map => { map.NotNullable(true);  });
            Property(x => x.IsExternalUser);
            Property(x => x.EffectiveDate);
            Property(x => x.ExpiredDate);
            Property(x => x.IsActive);
            Property(x => x.Activated, map => { map.NotNullable(true); });
            Property(x => x.Reactivated);
            Property(x => x.IncorrectLoginCount);
            Property(x => x.FirstNameTH);
            Property(x => x.Reactivated);
            Property(x => x.FirstNameEN);
            Property(x => x.LastNameTH);
            Property(x => x.LastNameEN);
            Property(x => x.MiddleNameTH);
            Property(x => x.MiddleNameEN);
            Property(x => x.PhoneCode);
            Property(x => x.PhoneNumber);
            Property(x => x.IdentityNumber);
            Property(x => x.Birthday);
            Property(x => x.CreatedByEN);
            Property(x => x.CreatedByTH);
            Property(x => x.CreatedByID);
            Property(x => x.ModifiedByEN);
            Property(x => x.ModifiedByTH);
            Property(x => x.ModifiedByID);
            Property(x => x.Imageprofile);
            Property(x => x.Created);
            Property(x => x.Modified);
            ManyToOne(x => x.Role, map => { map.Column("RoleId"); map.Fetch(FetchKind.Select); map.NotFound(NotFoundMode.Ignore); });
            ManyToOne(x => x.Title, map => { map.Column("TitleId"); map.Fetch(FetchKind.Select); map.NotFound(NotFoundMode.Ignore); });




        }
    }
}