﻿using System.Threading.Tasks;
using Amazon.SimpleSystemsManagement;
using Amazon.SimpleSystemsManagement.Model;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Infrastructure.Persistence.Repositories;
using iTrans.Transportation.Infrastructure.Persistence.Repositories.ApplicationLog;
using iTrans.Transportation.Infrastructure.Persistence.Repositories.Contact;
using iTrans.Transportation.Infrastructure.Persistence.Repositories.ContainerSpecification;
using iTrans.Transportation.Infrastructure.Persistence.Repositories.ContainerType;
//using iTrans.Transportation.Infrastructure.Persistence.Repositories.CustomerFavoriteCar;
//using iTrans.Transportation.Infrastructure.Persistence.Repositories.CustomerPaymentHistory;
using iTrans.Transportation.Infrastructure.Persistence.Repositories.CustomerPaymentTransaction;
using iTrans.Transportation.Infrastructure.Persistence.Repositories.CustomerProblem;
//using iTrans.Transportation.Infrastructure.Persistence.Repositories.CustomerTermAndCondition;
using iTrans.Transportation.Infrastructure.Persistence.Repositories.DriverAnnouncement;
using iTrans.Transportation.Infrastructure.Persistence.Repositories.DriverAnnouncementLocation;
using iTrans.Transportation.Infrastructure.Persistence.Repositories.DriverComplainAndRecommend;
using iTrans.Transportation.Infrastructure.Persistence.Repositories.DriverInsurance;
//using iTrans.Transportation.Infrastructure.Persistence.Repositories.DriverPayment;
//using iTrans.Transportation.Infrastructure.Persistence.Repositories.DriverPaymentHistory;
using iTrans.Transportation.Infrastructure.Persistence.Repositories.DriverPaymentTransaction;
using iTrans.Transportation.Infrastructure.Persistence.Repositories.Menu;
using iTrans.Transportation.Infrastructure.Persistence.Repositories.MenuPermission;
//using iTrans.Transportation.Infrastructure.Persistence.Repositories.DriverTermAndCondition;
using iTrans.Transportation.Infrastructure.Persistence.Repositories.NewsTargetSelection;
using iTrans.Transportation.Infrastructure.Persistence.Repositories.OrderingAddress;
using iTrans.Transportation.Infrastructure.Persistence.Repositories.OrderingAddressHistory;
//using iTrans.Transportation.Infrastructure.Persistence.Repositories.OrderingCancelStatus;
using iTrans.Transportation.Infrastructure.Persistence.Repositories.OrderingCar;
//using iTrans.Transportation.Infrastructure.Persistence.Repositories.OrderingCarDriver;
//using iTrans.Transportation.Infrastructure.Persistence.Repositories.OrderingCarDriverFlie;
using iTrans.Transportation.Infrastructure.Persistence.Repositories.OrderingContainer;
//using iTrans.Transportation.Infrastructure.Persistence.Repositories.OrderingContainerAddress;
using iTrans.Transportation.Infrastructure.Persistence.Repositories.OrderingDriver;
using iTrans.Transportation.Infrastructure.Persistence.Repositories.OrderingHistory;
using iTrans.Transportation.Infrastructure.Persistence.Repositories.OrderingInsurance;
//using iTrans.Transportation.Infrastructure.Persistence.Repositories.OrderingPerson;
using iTrans.Transportation.Infrastructure.Persistence.Repositories.OrderingProduct;
//using iTrans.Transportation.Infrastructure.Persistence.Repositories.OrderingRepositoryAsync;
using iTrans.Transportation.Infrastructure.Persistence.Repositories.RegisterDocument;
using iTrans.Transportation.Infrastructure.Persistence.Repositories.RegisterInformation;
using iTrans.Transportation.Infrastructure.Persistence.Repositories.Role;
using iTrans.Transportation.Infrastructure.Persistence.Repositories.RoleMenu;
using iTrans.Transportation.Infrastructure.Persistence.Repositories.TermAndCondition;
using iTrans.Transportation.Infrastructure.Persistence.Repositories.User;
using iTrans.Transportation.Infrastructure.Persistence.Repositories.Userlevel;
using iTrans.Transportation.Infrastructure.Persistence.Repositories.UserLevelCondition;
using iTrans.Transportation.Infrastructure.Persistence.Repositories.WelcomeInformation;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using NHibernate.Cfg;
using NHibernate.Cfg.MappingSchema;
using NHibernate.Dialect;
using NHibernate.Mapping.ByCode;


namespace iTrans.Transportation.Infrastructure.Persistence
{
    public static class ServiceRegistration
    {
        public static void AddPersistenceInfrastructure(this IServiceCollection services, IConfiguration configuration)
        {
            var mapper = new ModelMapper();
            mapper.AddMappings(typeof(UserMap).Assembly.ExportedTypes);

            HbmMapping domainMapping = mapper.CompileMappingForAllExplicitlyAddedEntities();
            var connectionString = string.Empty;
            var NHConfiguration = new Configuration();

            if (configuration.GetValue<string>("DBProvider").ToLower().Equals("mssql"))
            {
                connectionString = configuration.GetConnectionString("MSSQLConnection");

                if (configuration.GetValue<bool>("IsCloudDeployment"))
                {
                    var request = new GetParameterRequest()
                    {
                        Name = configuration.GetConnectionString("CloudSSMConnectionString")
                    };

                    using (var client = new AmazonSimpleSystemsManagementClient(Amazon.RegionEndpoint.GetBySystemName(configuration.GetValue<string>("Region"))))
                    {
                        var response = client.GetParameterAsync(request).GetAwaiter().GetResult(); ;
                        connectionString = response.Parameter.Value;
                    }

                }

                NHConfiguration.DataBaseIntegration(c =>
                {
                    c.Dialect<MsSql2008Dialect>();
                    c.ConnectionString = connectionString;
                    c.KeywordsAutoImport = Hbm2DDLKeyWords.AutoQuote;
                    c.LogFormattedSql = true;
                    c.LogSqlInConsole = true;
                });
            }
            else if (configuration.GetValue<string>("DBProvider").ToLower().Equals("postgres"))
            {
                connectionString = configuration.GetConnectionString("PostgresConnection");

                if (configuration.GetValue<bool>("IsCloudDeployment"))
                {
                    var request = new GetParameterRequest()
                    {
                        Name = configuration.GetConnectionString("CloudSSMConnectionString")
                    };

                    using (var client = new AmazonSimpleSystemsManagementClient(Amazon.RegionEndpoint.GetBySystemName(configuration.GetValue<string>("Region"))))
                    {
                        var response = client.GetParameterAsync(request).GetAwaiter().GetResult();
                        connectionString = response.Parameter.Value;
                    }
                }

                NHConfiguration.DataBaseIntegration(c =>
                {
                    c.Dialect<PostgreSQL82Dialect>();
                    c.ConnectionString = connectionString;
                    c.KeywordsAutoImport = Hbm2DDLKeyWords.AutoQuote;
                    c.LogFormattedSql = true;
                    c.LogSqlInConsole = true;
                });
            }
            NHConfiguration.AddMapping(domainMapping);

            var sessionFactory = NHConfiguration.BuildSessionFactory();

            services.AddSingleton(sessionFactory);
            services.AddScoped(factory => sessionFactory.OpenSession());

            #region Repositories

            services.AddTransient(typeof(IGenericRepositoryAsync<>), typeof(GenericRepositoryAsync<>));
            //services.AddTransient<IUserRepositoryAsync, UserRepositoryAsync>();
            //services.AddTransient<IUserStatusRepositoryAsync, UserStatusRepositoryAsync>();
            //services.AddTransient<ILoginLogRepositoryAsync, LoginLogRepositoryAsync>();
            //services.AddTransient<ILoginLogTypeRepositoryAsync, LoginLogTypeRepositoryAsync>();
            //services.AddTransient<IUserTokenRepositoryAsync, UserTokenRepositoryAsync>();
            //services.AddTransient<IEmailTemplateRepositoryAsync, EmailTemplateRepositoryAsync>();
            //services.AddTransient<IEmailRecipientRepositoryAsync, EmailRecipientRepositoryAsync>();
            services.AddTransient<ICustomerRepositoryAsync, CustomerRepositoryAsync>();
            //services.AddTransient<ICustomerLevelRepositoryAsync, CustomerLevelRepositoryAsync>();
            services.AddTransient<ICustomerLevelCharacteristicRepositoryAsync, CustomerLevelCharacteristicRepositoryAsync>();
            services.AddTransient<ICustomerAddressRepositoryAsync, CustomerAddressRepositoryAsync>();
            services.AddTransient<ICustomerFileRepositoryAsync, CustomerFileRepositoryAsync>();
            services.AddTransient<ICustomerProductRepositoryAsync, CustomerProductRepositoryAsync>();
            services.AddTransient<ICustomerProductFileRepositoryAsync, CustomerProductFileRepositoryAsync>();
            services.AddTransient<ITitleRepositoryAsync, TitleRepositoryAsync>();
            services.AddTransient<ICorporateTypeRepositoryAsync, CorporateTypeRepositoryAsync>();
            services.AddTransient<ICountryRepositoryAsync, CountryRepositoryAsync>();
            services.AddTransient<ICarTypeRepositoryAsync, CarTypeRepositoryAsync>();
            services.AddTransient<ICarListRepositoryAsync, CarListRepositoryAsync>();
            services.AddTransient<IFAQRepositoryAsync, FAQRepositoryAsync>();
            services.AddTransient<IProblemTopicRepositoryAsync, ProblemTopicRepositoryAsync>();
            services.AddTransient<ICarDescriptionRepositoryAsync, CarDescriptionRepositoryAsync>();
            services.AddTransient<ICarSpecificationRepositoryAsync, CarSpecificationRepositoryAsync>();
            services.AddTransient<ICarFeatureRepositoryAsync, CarFeatureRepositoryAsync>();
            services.AddTransient<IProductTypeRepositoryAsync, ProductTypeRepositoryAsync>();
            services.AddTransient<IProductPackagingRepositoryAsync, ProductPackagingRepositoryAsync>();
            services.AddTransient<IProvinceRepositoryAsync, ProvinceRepositoryAsync>();
            services.AddTransient<IDistrictRepositoryAsync, DistrictRepositoryAsync>();
            services.AddTransient<ISubdistrictRepositoryAsync, SubdistrictRepositoryAsync>();
            services.AddTransient<IRegionRepositoryAsync, RegionRepositoryAsync>();
            services.AddTransient<IDriverRepositoryAsync, DriverRepositoryAsync>();
            //services.AddTransient<IDriverLevelRepositoryAsync, DriverLevelRepositoryAsync>();
            services.AddTransient<IDriverLevelCharacteristicRepositoryAsync, DriverLevelCharacteristicRepositoryAsync>();
            services.AddTransient<IDriverAddressRepositoryAsync, DriverAddressRepositoryAsync>();
            services.AddTransient<IDriverCarRepositoryAsync, DriverCarRepositoryAsync>();
            services.AddTransient<INewsRepositoryAsync, NewsRepositoryAsync>();
            services.AddTransient<INewsFileRepositoryAsync, NewsFileRepositoryAsync>();
            services.AddTransient<IDriverFileRepositoryAsync, DriverFileRepositoryAsync>();
            services.AddTransient<IDriverCarFileRepositoryAsync, DriverCarFileRepositoryAsync>();
            services.AddTransient<IDriverCarLocationRepositoryAsync, DriverCarLocationRepositoryAsync>();
            services.AddTransient<IDriverCarFileRepositoryAsync, DriverCarFileRepositoryAsync>();
            services.AddTransient<IDriverCarLocationRepositoryAsync, DriverCarLocationRepositoryAsync>();
            services.AddTransient<IDriverCarFileRepositoryAsync, DriverCarFileRepositoryAsync>();
            services.AddTransient<IDriverCarLocationRepositoryAsync, DriverCarLocationRepositoryAsync>();
            services.AddTransient<IUserLevelRepositoryAsync, UserLevelRepositoryAsync>();
            services.AddTransient<IUserLevelConditionRepositoryAsync, UserLevelConditionRepositoryAsync>();
            services.AddTransient<IRegisterDocumentRepositoryAsync, RegisterDocumentRepositoryAsync>();
            services.AddTransient<IRegisterInformationRepositoryAsync, RegisterInformationRepositoryAsync>();
            services.AddTransient<ITermAndConditionRepositoryAsync, TermAndConditionRepositoryAsync>();
            services.AddTransient<IWelcomeInformationRepositoryAsync, WelcomeInformationRepositoryAsync>();
            services.AddTransient<IOrderingRepositoryAsync, OrderingRepositoryAsync>();
            services.AddTransient<IOrderingAddressRepositoryAsync, OrderingAddressRepositoryAsync>();
            services.AddTransient<IOrderingAddressFileRepositoryAsync, OrderingAddressFileRepositoryAsync>();
            services.AddTransient<IOrderingAddressProductRepositoryAsync, OrderingAddressProductRepositoryAsync>();
            services.AddTransient<IOrderingAddressProductFileRepositoryAsync, OrderingAddressProductFileRepositoryAsync>();
            services.AddTransient<IOrderingCarRepositoryAsync, OrderingCarRepositoryAsync>();
            //services.AddTransient<IOrderingCarDriverRepositoryAsync, OrderingCarDriverRepositoryAsync>();
            services.AddTransient<IOrderingContainerRepositoryAsync, OrderingContainerRepositoryAsync>();
            services.AddTransient<IOrderingCarRepositoryAsync, OrderingCarRepositoryAsync>();
            services.AddTransient<IOrderingCarFileRepositoryAsync, OrderingCarFileRepositoryAsync>();
            services.AddTransient<IOrderingContainerRepositoryAsync, OrderingContainerRepositoryAsync>();
            //services.AddTransient<IOrderingContainerAddressRepositoryAsync, OrderingContainerAddressRepositoryAsync>();
            //services.AddTransient<IOrderingContainerFileRepositoryAsync, OrderingContainerFileRepositoryAsync>();
            services.AddTransient<IOrderingDriverRepositoryAsync, OrderingDriverRepositoryAsync>();
            services.AddTransient<IOrderingDriverFileRepositoryAsync, OrderingDriverFileRepositoryAsync>();
            services.AddTransient<IOrderingHistoryRepositoryAsync, OrderingHistoryRepositoryAsync>();
            services.AddTransient<IOrderingInsuranceRepositoryAsync, OrderingInsuranceRepositoryAsync>();
            //services.AddTransient<IOrderingPersonRepositoryAsync, OrderingPersonRepositoryAsync>();
            services.AddTransient<IOrderingProductRepositoryAsync, OrderingProductRepositoryAsync>();
            services.AddTransient<IOrderingProductFileRepositoryAsync, OrderingProductFileRepositoryAsync>();
            services.AddTransient<IDriverAnnouncementRepositoryAsync, DriverAnnouncementRepositoryAsync>();
            services.AddTransient<IDriverAnnouncementCarRepositoryAsync, DriverAnnouncementCarRepositoryAsync>();
            services.AddTransient<IDriverAnnouncementCarFileRepositoryAsync, DriverAnnouncementCarFileRepositoryAsync>();
            services.AddTransient<IDriverAnnouncementLocationRepositoryAsync, DriverAnnouncementLocationRepositoryAsync>();
            services.AddTransient<IDriverTermAndConditionRepositoryAsync, DriverTermAndConditionRepositoryAsync>();
            services.AddTransient<ICustomerTermAndConditionRepositoryAsync, CustomerTermAndConditionRepositoryAsync>();
            services.AddTransient<IDriverInsuranceRepositoryAsync, DriverInsuranceRepositoryAsync>();
            services.AddTransient<IDriverComplainAndRecommendRepositoryAsync, DriverComplainAndRecommendRepositoryAsync>();
            services.AddTransient<ICustomerComplainAndRecommendRepositoryAsync, CustomerComplainAndRecommendRepositoryAsync>();
            services.AddTransient<ICustomerComplainAndRecommendFileRepositoryAsync, CustomerComplainAndRecommendFileRepositoryAsync>();
            services.AddTransient<IApplicationLogRepositoryAsync, ApplicationLogRepositoryAsync>();
            services.AddTransient<ICustomerFavoriteCarRepositoryAsync, CustomerFavoriteCarRepositoryAsync>();
            services.AddTransient<ICustomerPaymentRepositoryAsync, CustomerPaymentRepositoryAsync>();
            services.AddTransient<ICustomerPaymentHistoryRepositoryAsync, CustomerPaymentHistoryRepositoryAsync>();
            services.AddTransient<ICustomerPaymentTransactionRepositoryAsync, CustomerPaymentTransactionRepositoryAsync>();
            services.AddTransient<IDriverPaymentRepositoryAsync, DriverPaymentRepositoryAsync>();
            services.AddTransient<IDriverPaymentHistoryRepositoryAsync, DriverPaymentHistoryRepositoryAsync>();
            services.AddTransient<IDriverPaymentTransactionRepositoryAsync, DriverPaymentTransactionRepositoryAsync>();
            services.AddTransient<IOrderingCancelStatusRepositoryAsync, OrderingCancelStatusRepositoryAsync>();
            //services.AddTransient<IOrderingCarDriverFileRepositoryAsync, OrderingCarDriverFileRepositoryAsync>();
            services.AddTransient<IOrderingTrackingCodeRepositoryAsync, OrderingTrackingCodeRepositoryAsync>();
            services.AddTransient<INotificationRepositoryAsync, NotificationRepositoryAsync>();
            services.AddTransient<INotificationUserRepositoryAsync, NotificationUserRepositoryAsync>();
            services.AddTransient<INewsTargetSelectionRepositoryAsync, NewsTargetSelectionRepositoryAsync>();
            services.AddTransient<IContainerTypeRepositoryAsync, ContainerTypeRepositoryAsync>();
            services.AddTransient<IContainerSpecificationRepositoryAsync, ContainerSpecificationRepositoryAsync>();
            services.AddTransient<IEnergySavingDeviceRepositoryAsync, EnergySavingDeviceRepositoryAsync>();
            services.AddTransient<IOrderingPaymentRepositoryAsync, OrderingPaymentRepositoryAsync>();
            services.AddTransient<IOrderingDriverReserveRepositoryAsync, OrderingDriverReserveRepositoryAsync>();
            services.AddTransient<ISystemMenuRepositoryAsync, SystemMenuRepositoryAsync>();
            services.AddTransient<IDriverProblemRepositoryAsync, DriverProblemRepositoryAsync>();
            services.AddTransient<IDriverProblemFileRepositoryAsync, DriverProblemFileRepositoryAsync>();
            services.AddTransient<ISystemUserRepositoryAsync, SystemUserRepositoryAsync>();
            services.AddTransient<ICustomerProblemRepositoryAsync, CustomerProblemRepositoryAsync>();
            services.AddTransient<ICustomerProblemFileRepositoryAsync, CustomerProblemFileRepositoryAsync>();
            services.AddTransient<IContactRepositoryAsync, ContactRepositoryAsync>();
            services.AddTransient<IMenuRepositoryAsync, MenuRepositoryAsync>();
            services.AddTransient<IRoleRepositoryAsync, RoleRepositoryAsync>();
            services.AddTransient<IRoleMenuRepositoryAsync, RoleMenuRepositoryAsync>();
            services.AddTransient<IMenuPermissionRepositoryAsync, MenuPermissionRepositoryAsync>();
            services.AddTransient<IDummyWalletRepositoryAsync, DummyWalletRepositoryAsync>();
            services.AddTransient<IOrderingAddressHistoryRepositoryAsync, OrderingAddressHistoryRepositoryAsync>();
            services.AddTransient<IQRCodeRepositoryAsync, QRCodeRepositoryAsync>();
            services.AddTransient<ICustomerInboxRepositoryAsync, CustomerInboxRepositoryAsync>();
            services.AddTransient<IDriverInboxRepositoryAsync, DriverInboxRepositoryAsync>();
            services.AddTransient<IInboxRepositoryAsync, InboxRepositoryAsync>();
            services.AddTransient<IChatGroupMessageRepositoryAsync, ChatGroupMessageRepositoryAsync>();
            services.AddTransient<IConfigRepositoryAsync, ConfigRepositoryAsync>();
            services.AddTransient<ISuggestionInformationRepositoryAsync, SuggestionInformationRepositoryAsync>();
            services.AddTransient<IPaymentTypeRepositoryAsync, PaymentTypeRepositoryAsync>();
            services.AddTransient<ILoginErrorLogRepositoryAsync, LoginErrorLogRepositoryAsync>();
            services.AddTransient<ICarTypeFileRepositoryAsync, CarTypeFileRepositoryAsync>();
            services.AddTransient<ICarListFileRepositoryAsync, CarListFileRepositoryAsync>();
            services.AddTransient<ICarDescriptionFileRepositoryAsync, CarDescriptionFileRepositoryAsync>();
            services.AddTransient<ICarFeatureFileRepositoryAsync, CarFeatureFileRepositoryAsync>();
            services.AddTransient<ICarSpecificationFileRepositoryAsync, CarSpecificationFileRepositoryAsync>();
            services.AddTransient<IProductTypeFileRepositoryAsync, ProductTypeFileRepositoryAsync>();
            services.AddTransient<IProductPackagingFileRepositoryAsync, ProductPackagingFileRepositoryAsync>();
            services.AddTransient<ICustomerActivityRepositoryAsync, CustomerActivityRepositoryAsync>();
            services.AddTransient<IDriverActivityRepositoryAsync, DriverActivityRepositoryAsync>();
            services.AddTransient<ICustomerLevelHistoryRepositoryAsync, CustomerLevelHistoryRepositoryAsync>();
            services.AddTransient<IDriverLevelHistoryRepositoryAsync, DriverLevelHistoryRepositoryAsync>();
            services.AddTransient<IOrderingContainerFileRepositoryAsync, OrderingContainerFileRepositoryAsync>();
            #endregion Repositories
        }
    }
}
