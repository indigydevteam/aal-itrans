﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Infrastructure.Shared
{
    public class AppUser
    {
        string userType { get; set; }
        Guid Id { get; set; }
        string Name { get;set; }
    }
}
