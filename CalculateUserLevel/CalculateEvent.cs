﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using Microsoft.Extensions.Configuration;

namespace iTrans.CalculateUserLevel
{
    public class CalculateEvent
    {
        public async System.Threading.Tasks.Task CalculateAsync() //Get All Events Records  
        {
            try
            {
                //IConfiguration Config = new ConfigurationBuilder().AddJsonFile("appSettings.json").Build();
                //var dusername = EncryptionClass.Decrypt(Config.GetSection("credentials")["username"]);
                //var dpassword = EncryptionClass.Decrypt(Config.GetSection("credentials")["password"]);
                //var URL = Config.GetSection("URL").Value;
                var URL = "https://itrans-test-api.humantix.cloud/api/Public/CalculateUserLevel";
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(URL);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    var dataContent = new StringContent("{}");
                    HttpResponseMessage response = await client.PostAsync(URL, dataContent);
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
    }
}
