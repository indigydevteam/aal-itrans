﻿using AutoMapper;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Domain;
using iTrans.Transportation.ImportLocation.InsertToTable;
using Microsoft.Extensions.Configuration;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;

namespace iTrans.Transportation.ImportLocation
{
    public static class Program
    {

        public static void Main(string[] args)
        {
            string filename = @"D:/indigy/iTrans/ThepExcel-Thailand-Tambon - Copy.xlsx";
            string newPath = Path.GetFullPath(filename);


            using (FileStream s = File.Open(newPath, FileMode.Open))
            {
                ImportLocation(s);

            }
        }


        public static void ImportLocation(Stream ms)
        {
            using (ExcelPackage package = new ExcelPackage(ms))
            {

                var dis = new List<District>();
                var pro = new List<Province>();
                var sub = new List<Subdistrict>();
                var reg = new List<Region>();
                var currentSheet = package.Workbook.Worksheets;
                var worksheets = currentSheet.ToList();
                #region
                //foreach (var s in worksheets)
                //{
                //    var noOfCol = s.Dimension.End.Column;
                //    var noOfRow = s.Dimension.End.Row;


                //    for (int rowIterator = 1; rowIterator <= noOfRow; rowIterator++)
                //    {
                //        try
                //        {
                //            if (rowIterator > 1)
                //            {
                //                var RegionThai = $"{ s.Cells[rowIterator, 1].Value}".Trim();
                //                var RegionEng = $"{ s.Cells[rowIterator, 1].Value}".Trim();


                //                var provinceId = $"{ s.Cells[rowIterator, 14].Value}".Trim();

                //                var DistrictId = $"{ s.Cells[rowIterator, 9].Value}".Trim();
                //                var DistrictThai = $"{ s.Cells[rowIterator, 10].Value}".Trim();
                //                var DistrictEng = $"{ s.Cells[rowIterator,11].Value}".Trim();

                //                var provinceThai = $"{ s.Cells[rowIterator, 15].Value}".Trim();
                //                var provinceEng = $"{ s.Cells[rowIterator, 16].Value}".Trim();

                //                var SubdistrictThai = $"{ s.Cells[rowIterator, 2].Value}".Trim();
                //                var SubdistrictEng = $"{ s.Cells[rowIterator, 3].Value}".Trim();
                //                var SubdistrictPostCode = $"{ s.Cells[rowIterator, 6].Value}".Trim();



                //                using (var context = new InsertToTables())
                //                {
                //                        //var ObjDis = new District()
                //                        //{
                //                        //    //Province.Id = Convert.ToInt32(provinceId),
                //                        //    Name_TH = DistrictThai,
                //                        //    Name_ENG = DistrictEng,
                //                        //    Active = true
                //                        //};

                //                        //context.District.Add(ObjDis);


                //                        //var objSub = new Subdistrict()
                //                        //{
                //                        //    //District.Id = Convert.ToInt32(DistrictId),
                //                        //    Name_TH = SubdistrictThai,
                //                        //    Name_ENG = SubdistrictEng,
                //                        //    PostCode = SubdistrictPostCode,
                //                        //    Active = true

                //                        //};

                //                        //context.Subdistrict.Add(objSub);

                //                        var objPro = new Province()
                //                        {
                //                            //Country.Id = 10,
                //                            //Region.Id = 10,
                //                            Name_TH = provinceThai,
                //                            Name_ENG = provinceEng,
                //                            Active = true

                //                        };

                //                        context.Province.Add(objPro);

                //                        //var objreg = new Region()
                //                        //{
                //                        //    //Country.Id = 10,
                //                        //    Name_TH = SubdistrictThai,
                //                        //    Name_ENG = SubdistrictEng,
                //                        //    Active = true,

                //                        //};

                //                        //context.Region.Add(objreg);


                //                    context.SaveChanges();

                //                }


                //            }

                //        }
                //        catch (Exception ex)
                //        {
                //            Console.WriteLine(ex.Message);
                //        }

                //    }
                //}
                #endregion
            }
        }
    }



}