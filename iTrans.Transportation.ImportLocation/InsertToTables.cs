﻿using AutoMapper;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.ImportLocation.InsertToTable
{
  
        public class InsertToTables : DbContext
        {
            

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Data Source=.;Initial Catalog=iTrans;User ID=sa;Password=p@ssw0rd;Integrated Security=True;");
        }
        public DbSet<Country> Country { get; set; }
        public DbSet<Province> Province { get; set; }
        public DbSet<District> District { get; set; }
        public DbSet<Subdistrict> Subdistrict { get; set; }
        public DbSet<Region> Region { get; set; }
    }

    
}