﻿using iTrans.Transportation.Application.Features.UserLevels.Queries;
using iTrans.Transportation.Application.Features.Users.Backoffice;
using iTrans.Transportation.Application.Features.Users.Commands;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace iTrans.Transportation.WebApi.Controllers.V1
{
    [ApiVersion("1.0")]

    public class UserController : BaseApiController
    {
        [HttpPost("CreateUser")]
        public async Task<IActionResult> Post(CreateUserCommand command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPut("UpdateUser")]
        public async Task<IActionResult> UpdateUser(UpdateUserCommand command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }
        [HttpPost("GetAllUserQuery")]
        public async Task<IActionResult> GetAllUserQuery(GetAllUserQuery command)
        {
            try
            {

                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [HttpPost("GetUserByIdQuery")]
        public async Task<IActionResult> GetUserByIdQuery(GetUserByIdQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }
        
        [HttpPost("BackOffice/GetUserProfile")]
        public async Task<IActionResult> GetUserProfile(GetUserProfile command)
        {
            command.UserId = UserIdentity;
            command.Role = UserRole;

            return this.Ok(await this.Mediator.Send(command));
        }
        
        [HttpPost("BackOffice/GetAllUser")]
        public async Task<IActionResult> GetAllUser(GetAllUser command)
        {

            return this.Ok(await this.Mediator.Send(command));
        }  
       
        [HttpPost("BackOffice/GetUserById")]
        public async Task<IActionResult> GetUserById(GetUserById command)
        {
            command.Role = UserRole;

            return this.Ok(await this.Mediator.Send(command));
        }
        
        [HttpPost("BackOffice/DeleteUserById")]
        public async Task<IActionResult> DeleteUserById(DeleteUserById command)
        {

            return this.Ok(await this.Mediator.Send(command));
        }
        [HttpPut("BackOffice/CreateUser")]
        public async Task<IActionResult> CreateUser([FromForm] CreateUser command)
        {
            command.UserId = UserIdentity;
            command.ContentDirectory = Configuration["ContentPath"];

            return this.Ok(await this.Mediator.Send(command));
        }
        [HttpPut("BackOffice/UpdateUserById")]
        public async Task<IActionResult> UpdateUserById([FromForm] UpdateUserById command)
        {
            command.UserId = UserIdentity;
            command.ContentDirectory = Configuration["ContentPath"];

            return this.Ok(await this.Mediator.Send(command));
        }

    }
}
