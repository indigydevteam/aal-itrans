﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using iTrans.Transportation.Application.Features.DriverCars.Backoffice;
using iTrans.Transportation.Application.Features.DriverCars.Backoffice.Queries;
using iTrans.Transportation.Application.Features.DriverCars.Commands;
using iTrans.Transportation.Application.Features.DriverCars.Queries;
using iTrans.Transportation.Domain;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace iTrans.Transportation.WebApi.Controllers.V1
{
    [ApiVersion("1.0")]
    [Authorize]
    public class DriverCarController : BaseApiController
    {
        [HttpPost("CreateDriverCar")]
        public async Task<IActionResult> Post(CreateDriverCarCommand command)
        {
            if (UserIdentity != null)
            {
                command.UserId = UserIdentity.GetValueOrDefault();
            }
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("GetAllDriverCar")]
        public async Task<IActionResult> GetAllDriverCar(GetAllDriverCarQuery command)
        {
          
                return this.Ok(await this.Mediator.Send(command));   
        }

        [HttpPost("GetDriverCarById")]
        public async Task<IActionResult> GetDriverCarById(GetDriverCarByIdQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPut("UpdateDriverCarById")]
        public async Task<IActionResult> UpdateDriverCarById(UpdateDriverCarCommand command)
        {
            if (UserIdentity != null)
            {
                command.UserId = UserIdentity.GetValueOrDefault();
            }
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("CreateDriverCarWithLocation")]
        public async Task<IActionResult> Post([FromForm] CreateDriverCarWithLocationCommand command)
        {
            if (UserIdentity != null)
            {
                command.UserId = UserIdentity.GetValueOrDefault();
            }

            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("GetCarsByDriver")]
        public async Task<IActionResult> GetCarByDriver(GetCarsByDriverQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }
        [HttpPost("GetCarsByOwner")]
        public async Task<IActionResult> GetCarByOwner(GetCarsByOwnerQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }
        [HttpPost("GetCarInformation")]
        public async Task<IActionResult> GetCarInformation(GetCarInformationQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }
        [HttpPost("CreateCarInformation")]
        public async Task<IActionResult> Post([FromForm] CreateCarInformationCommand command)
        {
            //command.UserId = UserIdentity;
            if (UserIdentity != null)
            {
                command.UserId = UserIdentity.GetValueOrDefault();
            }
            return this.Ok(await this.Mediator.Send(command));
        }
        [HttpPut("UpdateCarInformation")]
        public async Task<IActionResult> UpdateCarInformationById([FromForm] UpdateCarInformationCommand command)
        {
            if (UserIdentity != null)
            {
                command.UserId = UserIdentity.GetValueOrDefault();
            }
            return this.Ok(await this.Mediator.Send(command));
        }
        [HttpPost("DeleteCarById")]
        public async Task<IActionResult> DeleteCarById(DeleteCarByIdCommand command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }
          [HttpPost("Backoffice/DeleteDriverCarById")]
        public async Task<IActionResult> DeleteDriverCarById(DeleteDriverCarByIdCommand command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }
        [HttpPost("Backoffice/GetAllDriverCar")]
        public async Task<IActionResult> GetAllDriverCar(GetAllDriverCar command)
        {

            return this.Ok(await this.Mediator.Send(command));
        }
        [HttpPost("Backoffice/GetDriverCarById")]
        public async Task<IActionResult> GetDriveCarByIdBackOffice(GetDriveCarById command)
        {
            command.ContentDirectory = Configuration["ContentEnvironment"];

            return this.Ok(await this.Mediator.Send(command));
        }
        [HttpPost("Backoffice/GetDriverCarByOwnerId")]
        public async Task<IActionResult> GetCarByOwnerId(GetCarByOwnerId command)
        {

            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPut("Backoffice/UpdateDriverCarById")]
        public async Task<IActionResult> UpdateDriverCarById([FromForm] UpdateDriverCarById command)
        {
            if (UserIdentity != null)
            {
                command.UserId = UserIdentity.GetValueOrDefault();
            }
            command.ContentDirectory = Configuration["ContentPath"];

            return this.Ok(await this.Mediator.Send(command));
        }
    }
}
