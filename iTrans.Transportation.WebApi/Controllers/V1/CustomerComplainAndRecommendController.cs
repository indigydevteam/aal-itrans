﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using iTrans.Transportation.Application.Features.CustomerComplainAndRecommendes.Commands;
using iTrans.Transportation.Application.Features.CustomerComplainAndRecommends.backoffice;
using iTrans.Transportation.Application.Features.CustomerComplainAndRecommends.Commands;
using iTrans.Transportation.Application.Features.CustomerComplainAndRecommends.Queries;
using iTrans.Transportation.Domain;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace iTrans.Transportation.WebApi.Controllers.V1
{
    [ApiVersion("1.0")]
    [Authorize]
    public class CustomerComplainAndRecommendController : BaseApiController
    {
        [HttpPost("CreateCustomerComplainAndRecommend")]
        public async Task<IActionResult> Post([FromForm] CreateCustomerComplainAndRecommendCommand command)
        {
            command.UserId = UserIdentity;

            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("GetAllCustomerComplainAndRecommend")]
        public async Task<IActionResult> GetAllCustomerComplainAndRecommend(GetAllCustomerComplainAndRecommendQuery command)
        {
            try
            {

                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost("GetCustomerComplainAndRecommendById")]
        public async Task<IActionResult> GetCustomerComplainAndRecommendById(GetCustomerComplainAndRecommendByIdQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("GetCustomerComplainAndRecommendByCustomer")]
        public async Task<IActionResult> GetInsuranceByCustomer(GetCustomerComplainAndRecommendByCustomerQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("Backoffice/GetAllCustomerComplainAndRecommend")]
        public async Task<IActionResult> GetAllCustomerComplainAndRecommend(GetAllComplainAndRecommend command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("Backoffice/GetCustomerComplainAndRecommendById")]
        public async Task<IActionResult> GetCustomerComplainAndRecommendById(GetCustomerComplainAndRecommendById command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("Backoffice/DeleteCustomerComplainAndRecommendeById")]
        public async Task<IActionResult> DeleteCustomerComplainAndRecommendeById(DeleteCustomerComplainAndRecommendeByIdCommand command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }


        [HttpPut("UpdateCustomerComplainAndRecommendById")]
        public async Task<IActionResult> UpdateCustomerComplainAndRecommendById(UpdateCustomerComplainAndRecommendCommand command)
        {
            command.UserId = UserIdentity;

            return this.Ok(await this.Mediator.Send(command));
        }
    }
}
