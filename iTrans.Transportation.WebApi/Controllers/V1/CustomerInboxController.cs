﻿using iTrans.Transportation.Application.Features.CustomerInboxs.Commands;
using iTrans.Transportation.Application.Features.CustomerInboxs.Queries;
using iTrans.Transportation.Application.Features.Inboxs.Commands;
using iTrans.Transportation.Application.Features.Inboxs.Queries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace iTrans.Transportation.WebApi.Controllers.V1
{
    [ApiVersion("1.0")]
    [ApiController]
    [Authorize]
    public class CustomerInboxController : BaseApiController
    {
        [HttpPost("GetByCustomer")]
        public async Task<IActionResult> GetByCustomer(GetInboxQuery command)
        {
            if (UserIdentity != null)
            {
                command.UserId = UserIdentity.GetValueOrDefault();
            }
            command.UserType = "customer";
            return this.Ok(await this.Mediator.Send(command));
        }
        [HttpPost("DeleteById")]
        public async Task<IActionResult> DeleteById(DeleteInboxByIdCommand command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }
        [HttpPost("UpdateRead")]
        public async Task<IActionResult> UpDateRead(UpdateIsReadByIdCommand command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }
    }
}
