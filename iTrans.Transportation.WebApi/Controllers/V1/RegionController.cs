﻿using System;
using System.Threading.Tasks;
using iTrans.Transportation.Application.Features.Regions.Backoffice.Commands;
using iTrans.Transportation.Application.Features.Regions.Backoffice.Queries;
//using iTrans.Transportation.Application.Features.Regions.Commands;
using iTrans.Transportation.Application.Features.Regions.Queries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace iTrans.Transportation.WebApi.Controllers.V1
{
    [ApiVersion("1.0")]
    //[Authorize]
    public class RegionController : BaseApiController
    {
        

        [HttpPost("GetAllRegion")]
        public async Task<IActionResult> GetAllRegion(GetAllRegionQuery command)
        {
            try
            {

                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost("GetRegionById")]
        public async Task<IActionResult> GetRegionById(GetRegionByIdQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }
       
        [HttpPost("GetRegionByCountry")]
        public async Task<IActionResult> GetRegionByCountry(GetRegionByCountryQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }
        //Backoffice
        [HttpPost("Backoffice/GetAll")]
        public async Task<IActionResult> GetAllRegionParameter(GetAllRegionParameter command)
        {
            try
            {

                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [HttpPost("Backoffice/GetById")]
        public async Task<IActionResult> GetById(BackofficeGetRegionByIdQuery command)
        {
            try
            {
                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [HttpPost("Backoffice/Create")]
        public async Task<IActionResult> Post(CreateRegionCommand command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPut("Backoffice/UpdateById")]
        public async Task<IActionResult> UpdateRegionById(UpdateRegionCommand command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("Backoffice/DeleteRegionById")]
        public async Task<IActionResult> DeleteRegionById(DeleteRegionByIdCommand command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }
    }
}

