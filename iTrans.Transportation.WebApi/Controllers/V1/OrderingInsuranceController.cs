﻿

using iTrans.Transportation.Application.Features.OrderingInsurances.Commands;
using iTrans.Transportation.Application.Features.OrderingInsurances.Queries;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace iTrans.Transportation.WebApi.Controllers.V1
{
    [ApiVersion("1.0")]
    //[Authorize]
    public class OrderingInsuranceController : BaseApiController
    {
        [HttpPost("CreateOrderingInsurance")]
        public async Task<IActionResult> Post(CreateOrderingInsuranceCommand command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("GetAllOrderingInsurance")]
        public async Task<IActionResult> GetAllOrderingInsurance(GetAllOrderingInsuranceQuery command)
        {
            try
            {

                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [HttpPost("GetOrderingInsuranceById")]
        public async Task<IActionResult> GetOrderingInsuranceById(GetOrderingInsuranceByIdQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("GetOrderingInsuranceByOrdering")]
        public async Task<IActionResult> GetOrderingInsuranceByOrdering(GetOrderingInsuranceByOrderingQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

      
       
        [HttpPut("UpdateOrderingInsuranceById")]
        public async Task<IActionResult> UpdateOrderingInsuranceById(UpdateOrderingInsuranceCommand command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }
    }
}