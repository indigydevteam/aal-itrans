﻿using System;
using System.Threading.Tasks;
using iTrans.Transportation.Application.Features.DriverAnnouncements.Commands;
using iTrans.Transportation.Application.Features.DriverAnnouncements.Queries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace iTrans.Transportation.WebApi.Controllers.V1
{
    [ApiVersion("1.0")]
    [Authorize]
    public class DriverAnnouncementController : BaseApiController
    {
         
        [HttpPost("GetLookingJobAnnouncement")]
        public async Task<IActionResult> GetAllLookingJobAnnouncementQuery(GetAllLookingJobAnnouncementQuery command)
        {
            try
            {

                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        //[HttpPost("CreateDriverAnnouncement")]
        //public async Task<IActionResult> CreateDriverAnnouncement(CreateDriverAnnouncementCommand command)
        //{
        //    return this.Ok(await this.Mediator.Send(command));
        //}
        [HttpPost("CreateAnnouncementCommand")]
        public async Task<IActionResult> CreateAnnouncement(CreateAnnouncementCommand command)
        {
            try
            {
                 
                if (UserIdentity != null)
                {
                    command.UserId = UserIdentity.GetValueOrDefault();
                }
                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost("GetAnnouncementByDriver")]
        public async Task<IActionResult> GetAnnouncementByDriver(GetAnnouncementByDriverQuery command)
        {
            try
            {
                if (UserIdentity != null)
                {
                    command.driverId = UserIdentity.GetValueOrDefault();
                }
                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [HttpPost("GetAnnouncementByOwner")]
        public async Task<IActionResult> GetAnnouncementByOwner(GetAnnouncementByOwnerQuery command)
        {
            try
            {

                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPut("UpdateAnnouncementById")]
        public async Task<IActionResult> UpdateAnnouncementById(UpdateAnnouncementByIdCommand command)
        {
            if (UserIdentity != null)
            {
                command.UserId = UserIdentity.GetValueOrDefault();
            }
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("GetAnnouncementById")]
        public async Task<IActionResult> GetAnnouncementById(GetAnnouncementByIdQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("DeleteAnnouncementById")]
        public async Task<IActionResult> DeleteAnnouncementById(DeleteAnnouncementByIdCommand command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPut("UpdateStatus")]
        public async Task<IActionResult> UpdateStatusById(UpdateStatusByIdCommand command)
        {
            
            command.APP_ID = Configuration["OneSignal:ApplicationID"];
            command.REST_API_KEY = Configuration["OneSignal:RestApiKey"];
            command.AUTH_ID = Configuration["OneSignal:AuthenticationID"];
            if (UserIdentity != null)
            {
                command.idUser = UserIdentity.GetValueOrDefault();
            }
            if (UserIdentity != null)
            {
                command.UserId = UserIdentity.GetValueOrDefault();
            }
            if (UserRole != null)
            {
                command.UserRole = UserRole;
            }

            return this.Ok(await this.Mediator.Send(command));
        }
        [HttpPost("SearchAnnouncementByOrder")]
        public async Task<IActionResult> SearchAnnouncementByOrder(SearchAnnouncementByOrderQuery command)
        {
           
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("SearchAnnouncement")]
        public async Task<IActionResult> SearchAnnouncement(SearchAnnouncementQuery command)
        {

            return this.Ok(await this.Mediator.Send(command));
        }
    }
}
