﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using iTrans.Transportation.Application.Features.DriverComplainAndRecommends.Commands;
using iTrans.Transportation.Application.Features.DriverComplainAndRecommends.Queries;
using iTrans.Transportation.Domain;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace iTrans.Transportation.WebApi.Controllers.V1
{
    [ApiVersion("1.0")]
    //[Authorize]
    public class DriverComplainAndRecommendController : BaseApiController
    {
        [HttpPost("CreateDriverComplainAndRecommend")]
        public async Task<IActionResult> Post([FromForm] CreateDriverComplainAndRecommendCommand command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("GetAllDriverComplainAndRecommend")]
        public async Task<IActionResult> GetAllDriverComplainAndRecommend(GetAllDriverComplainAndRecommendQuery command)
        {
            try
            {

                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost("GetDriverComplainAndRecommendById")]
        public async Task<IActionResult> GetDriverComplainAndRecommendById(GetDriverComplainAndRecommendByIdQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("GetDriverComplainAndRecommendByDriver")]
        public async Task<IActionResult> GetInsuranceByDriver(GetDriverComplainAndRecommendByDriverQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }


        [HttpPut("UpdateDriverComplainAndRecommendById")]
        public async Task<IActionResult> UpdateDriverComplainAndRecommendById(UpdateDriverComplainAndRecommendCommand command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }
    }
}
