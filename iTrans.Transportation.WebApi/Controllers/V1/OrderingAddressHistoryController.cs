﻿using iTrans.Transportation.Application.Features.OrderingAddressHistories.Commands;
using iTrans.Transportation.Application.Features.OrderingAddressHistoryHistories.Commands;
using iTrans.Transportation.Application.Features.OrderingAddressHistoryHistories.Qureies;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace iTrans.Transportation.WebApi.Controllers.V1
{
    public class OrderingAddressHistoryController : BaseApiController
    {
        [HttpPost("CreateOrderingAddressHistory")]
        public async Task<IActionResult> CreateOrderingAddressHistory(CreateOrderingAddressHistoryCommand command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("GetAllOrderingAddressHistory")]
        public async Task<IActionResult> GetAllOrderingAddressHistory(GetAllOrderingAddressHistoryQuery command)
        {         
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("GetOrderingAddressHistoryById")]
        public async Task<IActionResult> GetOrderingAddressHistoryById(GetOrderingAddressHistoryByIdQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }  
        
        [HttpPost("GetOrderingAddressHistoryByOrdering")]
        public async Task<IActionResult> GetOrderingAddressHistoryByOrdering(GetOrderingAddressHistoryByOrderingQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }
        //[HttpPost("GetAllOrderingAddressHistoryParameter")]
        //public async Task<IActionResult> GetAllOrderingAddressHistoryParameter(GetAllOrderingAddressHistoryParameter command)
        //{
        //    return this.Ok(await this.Mediator.Send(command));
        //}

        [HttpPut("UpdateOrderingAddressHistory")]
        public async Task<IActionResult> UpdateOrderingAddressHistory(UpdateOrderingAddressHistoryCommand command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }  
        [HttpPut("DeleteOrderingAddressHistory")]
        public async Task<IActionResult> DeleteOrderingAddressHistory(DeleteOrderingAddressHistoryCommand command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }
    }
}
