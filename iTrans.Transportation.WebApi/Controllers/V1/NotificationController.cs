﻿using iTrans.Transportation.Application.Features.Notification.Command;
using iTrans.Transportation.Application.Features.Notification.Queries;
using iTrans.Transportation.Application.Features.NotificationUser.Command;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc; 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace iTrans.Transportation.WebApi.Controllers.V1
{
    [Route("api/[controller]")]
    [ApiController] 
    [Authorize]
    public class NotificationController : BaseApiController
    { 
        [HttpPost("Send")]
        public async Task<IActionResult> Send(CreateNotificationCommand command)
        {
            command.IdUser = UserIdentity;

            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("Register")]
        public async Task<IActionResult> Register(CreateNotificationUserCommand command)
        {
            command.UserId = UserIdentity;
            command.Action = "Register";
            return this.Ok(await this.Mediator.Send(command));
        }


        [HttpPost("Unregister")]
        public async Task<IActionResult> Unregister(CreateNotificationUserCommand command)
        {
            command.UserId = UserIdentity;
            command.Action = "Unregister";
            return this.Ok(await this.Mediator.Send(command));
        }


        [HttpPost("List")]
        public async Task<IActionResult> List(GetAllNotificationParameter command)
        {
            command.UserId = UserIdentity;
            return this.Ok(await this.Mediator.Send(command));
        }
         
        [HttpPost("Badge")]
        public async Task<IActionResult> Badge(GetBadgeNotificationQuery command)
        {
            command.UserId = UserIdentity;

            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("Read")]
        public async Task<IActionResult> Read(ReadNotificationCommand command)
        {
            command.UserId = UserIdentity;

            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("Delete")]
        public async Task<IActionResult> Delete(DeleteNotificationCommand command)
        {
            command.UserId = UserIdentity;

            return this.Ok(await this.Mediator.Send(command));
        }
    }
}
