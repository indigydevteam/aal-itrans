﻿using System;
using System.Threading.Tasks;
using iTrans.Transportation.Application.Features.ProductPackagings.Queries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace iTrans.Transportation.WebApi.Controllers.V1
{
    [ApiVersion("1.0")]
    [Authorize]
    public class ProductPackagingController : BaseApiController
    {
        [HttpPost("GetAllProductPackaging")]
        public async Task<IActionResult> GetAllProductPackaging(GetAllProductPackaging command)
        {
            try
            {

                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [HttpPost("GetProductPackagingById")]
        public async Task<IActionResult> GetProductPackagingById(GetProductPackagingById command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        //[HttpPost("CreateProductPackaging")]
        //public async Task<IActionResult> Post([FromForm] CreateProductPackagingCommand command)
        //{
        //    command.UserId = UserIdentity;
        //    command.ContentDirectory = Configuration["ContentPath"];

        //    return this.Ok(await this.Mediator.Send(command));
        //}

        //[HttpPost("GetAllProductPackagingByConditionWithPage")]
        //public async Task<IActionResult> GetAllProductPackagingByConditionWithPage(GetAllProductPackagingParameter command)
        //{
        //    try
        //    {

        //        return this.Ok(await this.Mediator.Send(command));
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}


        //[HttpPut("UpdateProductPackagingById")]
        //public async Task<IActionResult> UpdateProductPackagingById([FromForm] UpdateProductPackagingCommand command)
        //{
        //    command.UserId = UserIdentity;

        //    return this.Ok(await this.Mediator.Send(command));
        //}

        //[HttpPost("Backofiice/GetAllProductPackaging")]
        //public async Task<IActionResult> DeleteProductPackagingById(GetAllProductPackaging command)
        //{
        //    return this.Ok(await this.Mediator.Send(command));
        //}

        //[HttpPost("Backofiice/DeleteProductPackagingById")]
        //public async Task<IActionResult> DeleteProductPackagingById(DeleteProductPackagingByIdCommand command)
        //{
        //    return this.Ok(await this.Mediator.Send(command));
        //}

    }
}
