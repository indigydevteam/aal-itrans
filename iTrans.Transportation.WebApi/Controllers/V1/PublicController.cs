﻿using iTrans.Transportation.Application.Features.DriverAnnouncements.Queries;
using iTrans.Transportation.Application.Features.DriverCars.Commands;
using iTrans.Transportation.Application.Features.Drivers.Queries;
using iTrans.Transportation.Application.Features.Orderings.Backoffice.Commands;
using iTrans.Transportation.Application.Features.Orderings.Commands;
using iTrans.Transportation.Application.Features.Orderings.Queries;
using iTrans.Transportation.Application.Features.UserLevels.Backoffice.Commands;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace iTrans.Transportation.WebApi.Controllers.V1
{
    [ApiVersion("1.0")]
    public class PublicController : BaseApiController
    {
        [HttpPost("GetLookingCarOrdering")]
        public async Task<IActionResult> GetLookingCarOrderingQuery(GetLookingCarOrderingQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("GetLookingJobAnnouncement")]
        public async Task<IActionResult> GetAllLookingJobAnnouncementQuery(GetAllLookingJobAnnouncementQuery command)
        {
            try
            {

                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [HttpPost("GetOrderingByTrackingCode")]
        public async Task<IActionResult> GetOrderingByTrackingCode(GetOrderingByTrackingCodeQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }
        [HttpPost("GetOrderingById")]
        public async Task<IActionResult> GetOrderingById(GetOrderingByIdQuery command)
        {
            if (UserIdentity != null)
            {
                command.UserId = UserIdentity.GetValueOrDefault();
            }
            if (UserRole != null)
            {
                command.UserRole = UserRole;
            }
            return this.Ok(await this.Mediator.Send(command));
        }
        [HttpPost("GetAnnouncementById")]
        public async Task<IActionResult> GetAnnouncementById(GetAnnouncementByIdQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }
        [HttpPost("GetDriverInformationByPhoneNumber")]
        public async Task<IActionResult> GetDriverInformationByPhoneNumber(GetDriverInformationByPhoneNumberQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("CalculateUserLevel")]
        public async Task<IActionResult> CalculateUserLevel(CalculateLevelCommand command)
        {
            try
            {

                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost("ActExpiryNotification")]
        public async Task<IActionResult> ActExpiryNotification(ActExpiryNotificationCommand command)
        {
            try
            {
                command.DRIVER_APP_ID = Configuration["DriverOneSignal:ApplicationID"];
                command.DRIVER_REST_API_KEY = Configuration["DriverOneSignal:RestApiKey"];
                command.DRIVER_AUTH_ID = Configuration["DriverOneSignal:AuthenticationID"];

                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
