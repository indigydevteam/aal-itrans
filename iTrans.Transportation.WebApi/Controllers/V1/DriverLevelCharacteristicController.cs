﻿using iTrans.Transportation.Application.Features.DriverLevelCharacteristics.Commands;
using iTrans.Transportation.Application.Features.DriverLevelCharacteristics.Queries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace iTrans.Transportation.WebApi.Controllers.V1
{
    [ApiVersion("1.0")]
    [Authorize]

    public class DriverLevelCharacteristicController : BaseApiController
    {
        [HttpPost("Backoffice/GetAll")]
        public async Task<IActionResult> Post(GetAllDriverLevelCharacteristicQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }
        [HttpPost("Backoffice/GetById")]
        public async Task<IActionResult> GetDriverLevelCharacteristic(GetDriverLevelCharacteristicByIdQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }
        [HttpPost("Backoffice/Create")]
        public async Task<IActionResult> Post(CreateDriverLevelCharacteristicCommand command)
        {
            command.UserId = UserIdentity;

            return this.Ok(await this.Mediator.Send(command));
        }
        [HttpPut("Backoffice/Update")]
        public async Task<IActionResult> UpdateCarFeatureById(UpdateDriverLevelCharacteristicCommand command)
        {
            command.UserId = UserIdentity;

            return this.Ok(await this.Mediator.Send(command));
        }
        [HttpPost("Backoffice/Delete")]
        public async Task<IActionResult> DeleteCarFeatureById(DeleteDriverLevelCharacteristicCommand command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }
    }
}
