﻿using System;
using System.Linq;
using System.Threading.Tasks;
using iTrans.Transportation.Application.Features.CustomerPassWords.Commands;
using iTrans.Transportation.Application.Features.Customers.Backoffice.Commands;
using iTrans.Transportation.Application.Features.Customers.Backoffice.Queries;
using iTrans.Transportation.Application.Features.Customers.Commands;
using iTrans.Transportation.Application.Features.Customers.Queries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace iTrans.Transportation.WebApi.Controllers
{
    [ApiVersion("1.0")]
    [ApiController]
    [Authorize]

    public class CustomerController : BaseApiController
    {
        [HttpPost("CreateCustomer")]
        public async Task<IActionResult> Post(CreateCustomerCommand command)
        {
            command.UserId = UserIdentity;

            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("GetAllCustomers")]
        public async Task<IActionResult> GetAllCustomer(GetAllCustomerQuery command)
        {
            try
            {
                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost("GetCustomersByConditionWithPage")]
        public async Task<IActionResult> GetCustomersByConditionWithPage(GetAllCustomerParameter command)
        {
            try
            {
                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
       
        [HttpPost("GetCustomerById")]
        public async Task<IActionResult> GetCustomerById(GetCustomerByIdQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("GetCustomerWithAddressById")]
        public async Task<IActionResult> GetCustomerWithAddressById(GetCustomerWithAddressByIdQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPut("UpdateCustomerById")]
        public async Task<IActionResult> UpdateCustomerById(UpdateCustomerCommand command)
        {
            command.UserId = UserIdentity;

            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("GetCustomerInformationById")]
        public async Task<IActionResult> GetCustomerInformationById(GetCustomerInformationByIdQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }


        [HttpPost("UpdateCustomerInformationById")]
        public async Task<IActionResult> UpdateCustomerInformationById([FromForm] UpdateCustomerInformationByIdCommand command)
        {
            command.UserId = UserIdentity;

            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("GetCustomerInformation")]
        public async Task<IActionResult> GetCustomerInformationByAuth(GetCustomerInformationByAuthQuery command)
        {
            if (UserIdentity != null)
            {
                command.Id = UserIdentity.GetValueOrDefault();
            }

            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPut("UpdatePassword")]
        public async Task<IActionResult> UpdateCustomerPasswordById(UpdateCustomerPasswordByIdCommand command)
        {
            if (UserIdentity != null)
            {
                command.Id = UserIdentity.GetValueOrDefault();
            }
             
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPut("UpdateRanking")]
        public async Task<IActionResult> UpdateCustomerStardById(UpdateCustomerStarByIdCommand command)
        {
            command.UserId = UserIdentity;
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("ConfirmPassword")]
        public async Task<IActionResult> ConfirmCustomerPassword(ConfirmCustomerPasswordQuery command)
        {
            if (UserIdentity != null)
            {
                command.Id = UserIdentity.GetValueOrDefault();
            }
            return this.Ok(await this.Mediator.Send(command));
        }

        //[HttpPost("CheckIdentityNumber")]
        //public async Task<IActionResult> CheckIdentityNumber(CheckCustomerIdentityNumberQuery command)
        //{
        //    if (UserIdentity != null)
        //    {
        //        command.Id = UserIdentity.GetValueOrDefault();
        //    }
        //    return this.Ok(await this.Mediator.Send(command));
        //}

        //[HttpPut("CheckPhoneNumber")]
        //public async Task<IActionResult> CheckPhoneNumber(CheckCustomerPhoneNumberQuery command)
        //{
        //    return this.Ok(await this.Mediator.Send(command));
        //}

        [HttpPost("Backoffice/GetCustomerById")]
        public async Task<IActionResult> GetCustomerByIdBackOfficeQuery(GetCustomerById command)
        {
            try
            {
                command.ContentDirectory = Configuration["ContentEnvironment"];

                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        } 
        [HttpPost("Backoffice/GetAllCustomer")]
        public async Task<IActionResult> GetAllCustomer(GetAllCustomer command)
        {
            try
            {

                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [HttpPost("Backoffice/GetCustomerTaxById")]
        public async Task<IActionResult> GetCustomerTaxById(GetCustomerTaxById command)
        {
            try
            {

                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [HttpPut("Backoffice/UpdateCustomerById")]
        public async Task<IActionResult> UpdateCustomerById([FromForm] UpdateCustomerByIdCommand command)
        {
            command.UserId = UserIdentity;
            command.ContentDirectory = Configuration["ContentPath"];
            command.APP_ID = Configuration["OneSignal:ApplicationID"];
            command.REST_API_KEY = Configuration["OneSignal:RestApiKey"];
            command.AUTH_ID = Configuration["OneSignal:AuthenticationID"];

            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPut("Backoffice/UpdateCustomerTaxById")]
        public async Task<IActionResult> UpdateCustomerTaxById(UpdateCustomerTaxById command)
        {
            command.UserId = UserIdentity;

            return this.Ok(await this.Mediator.Send(command));
        }
          
        [HttpPost("Backoffice/GetCustomerByVerifyStatus")]
        public async Task<IActionResult> GetCustomerByVerifyStatus(GetCustomerByVerifyStatus command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }


        [HttpPost("DeleteCustomerById")]
        public async Task<IActionResult> DeleteCustomerById(DeleteCustomerByIdCommand command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }
        [HttpPost("UpdateLevel")]
        public async Task<IActionResult> UpdateLevel([FromForm] UpdateCustomerLevelCommand command)
        {
            command.UserId = UserIdentity;

            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("VerifyIdentityNumber")]
        public async Task<IActionResult> VerifyIdentityNumber(VerifyCustomerIdentityNumberQuery command)
        {
            if (UserIdentity != null)
            {
                command.Id = UserIdentity.GetValueOrDefault();
            }
            return this.Ok(await this.Mediator.Send(command));
        }
        [HttpPost("VerifyPhoneNumber")]
        public async Task<IActionResult> VerifyPhoneNumber(VerifyCustomerPhoneNumberQuery command)
        {
            if (UserIdentity != null)
            {
                command.Id = UserIdentity.GetValueOrDefault();
            }
            return this.Ok(await this.Mediator.Send(command));
        }
        [HttpPost("VerifyPassword")]
        public async Task<IActionResult> VerifyPassword(VerifyCustomerPasswordQuery command)
        {
            if (UserIdentity != null)
            {
                command.Id = UserIdentity.GetValueOrDefault();
            }
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("CheckTermAndCondition")]
        public async Task<IActionResult> CheckTermAndCondition(CheckCustomerTermAndCondition command)
        {
            if (UserIdentity != null)
            {
                command.Id = UserIdentity.GetValueOrDefault();
            }
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("Backoffice/CheckTermAndCondition")]
        public async Task<IActionResult> BackofficeCheckTermAndCondition(CheckCustomerTermAndCondition command)
        {
            //if (UserIdentity != null)
            //{
            //    command.Id = UserIdentity.GetValueOrDefault();
            //}
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("VerifyStatus")]
        public async Task<IActionResult> VerifyStatus(VerifyCustomerStatusCommand command)
        {
            if (UserIdentity != null)
            {
                command.Id = UserIdentity.GetValueOrDefault();
            }
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("Delete")]
        public async Task<IActionResult> Delete(DeleteCustomerCommand command)
        {
            if (UserIdentity != null)
            {
                command.Id = UserIdentity.GetValueOrDefault();
            }
            return this.Ok(await this.Mediator.Send(command));
        }
    }
}
