﻿using System;
using System.Linq;
using System.Threading.Tasks;
using iTrans.Transportation.Application.Features.driverPassWords.Commands;
using iTrans.Transportation.Application.Features.Drivers.Commands;
using iTrans.Transportation.Application.Features.Drivers.Queries;
using iTrans.Transportation.Application.Features.Stars.Commands;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace iTrans.Transportation.WebApi.Controllers
{
    [ApiVersion("1.0")]
   // [Authorize]
    public class DriverNoAuthController : BaseApiController
    {
        [HttpPost("CheckExistIdentityNumber")]
        public async Task<IActionResult> CheckIdentityNumber(CheckExistDriverIdentityNumberQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("CheckExistPhoneNumber")]
        public async Task<IActionResult> CheckPhoneNumber(CheckExistDriverPhoneNumberQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }
    }
}
