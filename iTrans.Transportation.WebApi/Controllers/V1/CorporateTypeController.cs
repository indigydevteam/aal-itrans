﻿using System;
using System.Threading.Tasks;
//using iTrans.Transportation.Application.Features.CorporateTypes.Backoffice;
//using iTrans.Transportation.Application.Features.CorporateTypes.Commands;
using iTrans.Transportation.Application.Features.CorporateTypes.Queries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace iTrans.Transportation.WebApi.Controllers.V1
{
    [ApiVersion("1.0")]
    //[Authorize]
    public class CorporateTypeController : BaseApiController
    {
        [HttpPost("GetAllCorporateType")]
        public async Task<IActionResult> GetAllCorporateType(GetAllCorporateType command)
        {
            try
            {

                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost("GetCorporateTypeById")]
        public async Task<IActionResult> GetCorporateTypeById(GetCorporateTypeById command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        //[HttpPost("GetAllCorporateTypeByConditionWithPage")]
        //public async Task<IActionResult> GetAllCorporateTypeByConditionWithPage(GetAllCorporateTypeParameter command)
        //{
        //    try
        //    {

        //        return this.Ok(await this.Mediator.Send(command));
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}
        //[HttpPost("CreateCorporateType")]
        //public async Task<IActionResult> Post(CreateCorporateTypeCommand command)
        //{
        //    command.UserId = UserIdentity;
        //    return this.Ok(await this.Mediator.Send(command));
        //}

        //[HttpPut("UpdateCorporateTypeById")]
        //public async Task<IActionResult> UpdateCorporateTypeById(UpdateCorporateTypeCommand command)
        //{
        //    command.UserId = UserIdentity;

        //    return this.Ok(await this.Mediator.Send(command));
        //}
        //[HttpPost("Backoffice/GetAllCorporateType")]
        //public async Task<IActionResult> GetAllCorporateType(GetAllCorporateType command)
        //{
        //    return this.Ok(await this.Mediator.Send(command));
        //}  [HttpPost("Backoffice/DeleteCorporateTypeById")]
        //public async Task<IActionResult> DeleteCorporateTypeById(DeleteCorporateTypeByIdCommand command)
        //{
        //    command.UserId = UserIdentity;
        //    return this.Ok(await this.Mediator.Send(command));
        //}
    }
}
