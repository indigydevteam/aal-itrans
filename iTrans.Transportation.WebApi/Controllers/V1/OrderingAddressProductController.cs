﻿using iTrans.Transportation.Application.Features.OrderingAddressProducts.Commands;
using iTrans.Transportation.Application.Features.OrderingAddressProducts.Queries;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace iTrans.Transportation.WebApi.Controllers.V1
{
    [ApiVersion("1.0")]
    //[Authorize]
    public class OrderingAddressProductController : BaseApiController
    {
        [HttpPost("Create")]
        public async Task<IActionResult> Post(CreateOrderingAddressProductCommand command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("GetAll")]
        public async Task<IActionResult> GetAllOrderingProduct(GetAllOrderingAddressProductQuery command)
        {
            try
            {

                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [HttpPost("GetById")]
        public async Task<IActionResult> GetOrderingProductById(GetOrderingAddressProductByIdQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("GetByOrdering")]
        public async Task<IActionResult> GetOrderingProductByOrdering(GetOrderingAddressProductByOrderingAddressQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }



        [HttpPut("UpdateById")]
        public async Task<IActionResult> UpdateOrderingProductById(UpdateOrderingAddressProductCommand command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }
        [HttpPost("DeleteById")]
        public async Task<IActionResult> DeleteeOrderingProductById(DeleteOrderingAddressProductCommand command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }
    }
}