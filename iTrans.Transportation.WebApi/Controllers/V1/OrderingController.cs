﻿using iTrans.Transportation.Application.Features.Orderings.Backoffice.Commands;
using iTrans.Transportation.Application.Features.Orderings.Backoffice.Queries;
using iTrans.Transportation.Application.Features.Orderings.Commands;
using iTrans.Transportation.Application.Features.Orderings.Queries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace iTrans.Transportation.WebApi.Controllers.V1
{
    [ApiVersion("1.0")]
    [Authorize]
    public class OrderingController : BaseApiController
    {
        //[HttpPost("CreateOrdering")]
        //public async Task<IActionResult> Post([FromForm]  CreateOrderingCommand command)
        //{
        //    command.UserId = UserIdentity;

        //    return this.Ok(await this.Mediator.Send(command));
        //}

        [HttpPost("GetAllOrdering")]
        public async Task<IActionResult> GetAllOrdering(GetAllOrderingQuery command)
        {
            try
            {

                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [HttpPost("GetOrderingById")]
        public async Task<IActionResult> GetOrderingById(GetOrderingByIdQuery command)
        {
            if (UserIdentity != null)
            {
                command.UserId = UserIdentity.GetValueOrDefault();
            }
            if (UserRole != null)
            {
                command.UserRole = UserRole;
            }
            return this.Ok(await this.Mediator.Send(command));
        }
        [HttpPost("GetOrderingByTrackingCode")]
        public async Task<IActionResult> GetOrderingByTrackingCode(GetOrderingByTrackingCodeQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }
        [HttpPost("GetOrderingByCustomer")]
        public async Task<IActionResult> GetOrderingByCustomer(GetOrderingByCustomerQuery command)
        {
            if (UserIdentity != null)
            {
                command.UserId = UserIdentity.GetValueOrDefault();
            }
            if (UserRole != null)
            {
                command.UserRole = UserRole;
            }
            return this.Ok(await this.Mediator.Send(command));
        }
        [HttpPost("GetLookingCarOrdering")]
        public async Task<IActionResult> GetLookingCarOrderingQuery(GetLookingCarOrderingQuery command)
        {
            if (UserIdentity != null)
            {
                command.UserId = UserIdentity.GetValueOrDefault();
            }
            if (UserRole != null)
            {
                command.UserRole = UserRole;
            }
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("GetOrderingByDriver")]
        public async Task<IActionResult> GetOrderingByDriver(GetOrderingByDriverQuery command)
        {
            if (UserIdentity != null)
            {
                command.UserId = UserIdentity.GetValueOrDefault();
                command.DriverId = UserIdentity.GetValueOrDefault();
            }
            if (UserRole != null)
            {
                command.UserRole = UserRole;
            }
            return this.Ok(await this.Mediator.Send(command));
        }
        [HttpPost("GetOrderingByCorporate")]
        public async Task<IActionResult> GetOrderingByCorporate(GetOrderingByCorporateQuery command)
        {
            if (UserIdentity != null)
            {
                command.UserId = UserIdentity.GetValueOrDefault();
            }
            if (UserRole != null)
            {
                command.UserRole = UserRole;
            }
            return this.Ok(await this.Mediator.Send(command));
        }

        //[HttpPut("UpdateOrderingById")]
        //public async Task<IActionResult> UpdateOrderingById(UpdateOrderingCommand command)
        //{
        //    command.UserId = UserIdentity;

        //    return this.Ok(await this.Mediator.Send(command));

        //}

        [HttpPost("Create")]
        public async Task<IActionResult> CreateOrder([FromForm] CustomerCreateOrderingCommand command)
        {
            command.DRIVER_APP_ID = Configuration["DriverOneSignal:ApplicationID"];
            command.DRIVER_REST_API_KEY = Configuration["DriverOneSignal:RestApiKey"];
            command.DRIVER_AUTH_ID = Configuration["DriverOneSignal:AuthenticationID"];
            if (UserIdentity != null)
            {
                command.CustomerId = UserIdentity.GetValueOrDefault();
            }

            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("Update")]
        public async Task<IActionResult> UpdateOrder([FromForm] UpdateOrderingCommand command)
        {
            //command.APP_ID = Configuration["DriverOneSignal:ApplicationID"];
            //command.REST_API_KEY = Configuration["DriverOneSignal:RestApiKey"];
            //command.AUTH_ID = Configuration["DriverOneSignal:AuthenticationID"];
            if (UserIdentity != null)
            {
                command.UserId = UserIdentity.GetValueOrDefault();
            }

            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPut("UpdateTrackingCode")]
        public async Task<IActionResult> UpdateTrackingCodeCommand(UpdateTrackingCodeCommand command)
        {
            command.UserId = UserIdentity;

            return this.Ok(await this.Mediator.Send(command));

        }

        [HttpPost("DriverSpecifyPrice")]
        public async Task<IActionResult> UpdateDesiredPrice(DriverSpecifyPriceCommand command)
        {
            command.UserId = UserIdentity;
            command.APP_ID = Configuration["OneSignal:ApplicationID"];
            command.REST_API_KEY = Configuration["OneSignal:RestApiKey"];
            command.AUTH_ID = Configuration["OneSignal:AuthenticationID"];
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("DriverAccept")]
        public async Task<IActionResult> DriverAccept(DriverAcceptOrderingCommand command)
        {
            command.UserId = UserIdentity;
            command.APP_ID = Configuration["OneSignal:ApplicationID"];
            command.REST_API_KEY = Configuration["OneSignal:RestApiKey"];
            command.AUTH_ID = Configuration["OneSignal:AuthenticationID"];

            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("Payment")]
        public async Task<IActionResult> Payment(CustomerPaymentCommand command)
        {
            command.UserId = UserIdentity;

            command.APP_ID = Configuration["OneSignal:ApplicationID"];
            command.REST_API_KEY = Configuration["OneSignal:RestApiKey"];
            command.AUTH_ID = Configuration["OneSignal:AuthenticationID"];

            command.DRIVER_APP_ID = Configuration["DriverOneSignal:ApplicationID"];
            command.DRIVER_REST_API_KEY = Configuration["DriverOneSignal:RestApiKey"];
            command.DRIVER_AUTH_ID = Configuration["DriverOneSignal:AuthenticationID"];

            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("UpdateStatus")]
        public async Task<IActionResult> UpdateStatus(UpdateStatusCommand command)
        {

            command.APP_ID = Configuration["OneSignal:ApplicationID"];
            command.REST_API_KEY = Configuration["OneSignal:RestApiKey"];
            command.AUTH_ID = Configuration["OneSignal:AuthenticationID"];

            command.DRIVER_APP_ID = Configuration["DriverOneSignal:ApplicationID"];
            command.DRIVER_REST_API_KEY = Configuration["DriverOneSignal:RestApiKey"];
            command.DRIVER_AUTH_ID = Configuration["DriverOneSignal:AuthenticationID"];

            if (UserIdentity != null)
            {
                command.UserId = UserIdentity.GetValueOrDefault();
            }
            if (UserRole != null)
            {
                command.UserRole = UserRole;
            }

            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("UploadFile")]
        public async Task<IActionResult> UploadFile([FromForm] UploadFileCommand command)
        {
            command.UserId = UserIdentity;

            command.APP_ID = Configuration["OneSignal:ApplicationID"];
            command.REST_API_KEY = Configuration["OneSignal:RestApiKey"];
            command.AUTH_ID = Configuration["OneSignal:AuthenticationID"];

            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("DeleteOrdering")]
        public async Task<IActionResult> DeleteOrdering(DeleteOrderingCommand command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("SearchOrdering")]
        public async Task<IActionResult> SearchOrderingByFilter([FromForm] SearchOrderingQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("ReserveDriver")]
        public async Task<IActionResult> OrderingReserveDriver(OrderingReserveDriverCommand command)
        {
            command.UserId = UserIdentity;

            //command.APP_ID = Configuration["OneSignal:ApplicationID"];
            //command.REST_API_KEY = Configuration["OneSignal:RestApiKey"];
            //command.AUTH_ID = Configuration["OneSignal:AuthenticationID"];

            command.DRIVER_APP_ID = Configuration["DriverOneSignal:ApplicationID"];
            command.DRIVER_REST_API_KEY = Configuration["DriverOneSignal:RestApiKey"];
            command.DRIVER_AUTH_ID = Configuration["DriverOneSignal:AuthenticationID"];

            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("ComfirmPrice")]
        public async Task<IActionResult> CustomerComfirmPrice(CustomerComfirmPriceCommand command)
        {
            command.UserId = UserIdentity;

            command.APP_ID = Configuration["OneSignal:ApplicationID"];
            command.REST_API_KEY = Configuration["OneSignal:RestApiKey"];
            command.AUTH_ID = Configuration["OneSignal:AuthenticationID"];

            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("DriverCancelReserve")]
        public async Task<IActionResult> DriverCancelReserve(DriverCancelReserveCommand command)
        {
            command.UserId = UserIdentity;

            command.APP_ID = Configuration["OneSignal:ApplicationID"];
            command.REST_API_KEY = Configuration["OneSignal:RestApiKey"];
            command.AUTH_ID = Configuration["OneSignal:AuthenticationID"];

            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("SearchMathingDriver")]
        public async Task<IActionResult> SearchMathingDriver(SearchMathingDriverCommand command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("GetOrderingCompensationById")]
        public async Task<IActionResult> GetOrderingCompensationById(GetOrderingCompensationByIdQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPut("UpdateActualCompensation")]
        public async Task<IActionResult> UpdateActualCompensation(UpdateActualCompensationCommand command)
        {
            command.UserId = UserIdentity;

            return this.Ok(await this.Mediator.Send(command));

        }

        [HttpPost("GetMyAnnouncement")]
        public async Task<IActionResult> GetMyAnnouncement(GetMyAnnouncementQuery command)
        {
            if (UserIdentity != null)
            {
                command.UserId = UserIdentity.GetValueOrDefault();
            }
            if (UserRole != null)
            {
                command.UserRole = UserRole;
            }
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("GetCurrentLocation")]
        public async Task<IActionResult> GetCurrentLocation(GetOrderingCurrentLocationByIdQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("UpdateCurrentLocation")]
        public async Task<IActionResult> UpdateCurrentLocation(UpdateCurrentLocationCommand command)
        {
            command.UserId = UserIdentity;
            command.APP_ID = Configuration["OneSignal:ApplicationID"];
            command.REST_API_KEY = Configuration["OneSignal:RestApiKey"];
            command.AUTH_ID = Configuration["OneSignal:AuthenticationID"];
            return this.Ok(await this.Mediator.Send(command));
        }
        [HttpPost("DriverPayment")]
        public async Task<IActionResult> DriverPayment(DriverPaymentCommand command)
        {
            command.UserId = UserIdentity;

            command.APP_ID = Configuration["OneSignal:ApplicationID"];
            command.REST_API_KEY = Configuration["OneSignal:RestApiKey"];
            command.AUTH_ID = Configuration["OneSignal:AuthenticationID"];

            command.DRIVER_APP_ID = Configuration["DriverOneSignal:ApplicationID"];
            command.DRIVER_REST_API_KEY = Configuration["DriverOneSignal:RestApiKey"];
            command.DRIVER_AUTH_ID = Configuration["DriverOneSignal:AuthenticationID"];

            return this.Ok(await this.Mediator.Send(command));
        }
        //[HttpPost("GetAllOrderingByConditionWithPage")]
        //public async Task<IActionResult> GetAllOrderingByConditionWithPage(GetAllOrderingParameter command)
        //{
        //    try
        //    {

        //        return this.Ok(await this.Mediator.Send(command));
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}
        //[HttpPost("Backoffice/GetAllOrdering")]
        //public async Task<IActionResult> GetAllOrderinge(Application.Features.Orderings.Backoffice.Commands.GetAllOrderingParameter command)
        //{
        //    try
        //    {

        //        return this.Ok(await this.Mediator.Send(command));
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}
        //[HttpPost("Backoffice/GetAllOrderingPayment")]
        //public async Task<IActionResult> GetAllOrderingPayment(GetAllOrderingPayment command)
        //{
        //    try
        //    {

        //        return this.Ok(await this.Mediator.Send(command));
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        //[HttpPost("Backoffice/GetOrderingPaymentById")]
        //public async Task<IActionResult> GetOrderingPaymentById(GetOrderingPaymentById command)
        //{
        //    try
        //    {

        //        return this.Ok(await this.Mediator.Send(command));
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}
        //[HttpPost("Backoffice/DeleteOrderingPayment")]
        //public async Task<IActionResult> DeleteOrderingPayment(DeleteOrderingPayment command)
        //{
        //    return this.Ok(await this.Mediator.Send(command));
        //}
        //[HttpPost("Backoffice/GetOrderingById")]
        //public async Task<IActionResult> GetOrderingsById(GetOrderingsByIdQuery command)
        //{
        //    try
        //    {
        //        return this.Ok(await this.Mediator.Send(command));
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        //[HttpPost("Backoffice/GetOrderingProvincesPopular")]
        //public async Task<IActionResult> GetOrderingProvincesPopular(GetOrderingProvincesPopular command)
        //{

        //    return this.Ok(await this.Mediator.Send(command));

        //}
        //[HttpPost("Backoffice/GetOrderingRegionsPopular")]
        //public async Task<IActionResult> GetOrderingRegionsPopular(GetOrderingRegionsPopular command)
        //{

        //    return this.Ok(await this.Mediator.Send(command));

        //}

        //[HttpPost("Backoffice/GetCountOrderingByType")]
        //public async Task<IActionResult> GetCountOrderingByType(GetCountOrderingByType command)
        //{

        //    return this.Ok(await this.Mediator.Send(command));

        //}

        //[HttpPost("Backoffice/GetOrderingChart")]
        //public async Task<IActionResult> GetOrderingChart(GetOrderingChart command)
        //{

        //    return this.Ok(await this.Mediator.Send(command));

        //}
        //[HttpPost("Backoffice/GetOrderingStatus")]
        //public async Task<IActionResult> GetAllCarType(GetOrderingStatus command)
        //{
        //    try
        //    {
        //        return this.Ok(await this.Mediator.Send(command));
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        //[HttpPost("Backoffice/UpdateOrderingStatus")]
        //public async Task<IActionResult> UpdateOrderingStatus(UpdateOrderingStatusCommand command)
        //{
        //    try
        //    {
        //        command.UserId = UserIdentity;
        //        return this.Ok(await this.Mediator.Send(command));
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}
    }
}