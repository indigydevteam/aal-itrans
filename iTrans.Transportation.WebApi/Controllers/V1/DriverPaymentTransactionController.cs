﻿using System;
using System.Threading.Tasks;
using iTrans.Transportation.Application.Features.DriverPaymentTransactiones.Queries;
using iTrans.Transportation.Application.Features.DriverPaymentTransactions.Commands;
using iTrans.Transportation.Application.Features.DriverPaymentTransactions.Queries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace iTrans.Transportation.WebApi.Controllers.V1
{
    [ApiVersion("1.0")]
    //[Authorize]
    public class DriverPaymentTransactionController : BaseApiController
    {
        [HttpPost("CreateDriverPaymentTransaction")]
        public async Task<IActionResult> Post(CreateDriverPaymentTransactionCommand command)
        {
            try
            {
                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost("GetAllDriverPaymentTransaction")]
        public async Task<IActionResult> GetAllDriverPaymentTransaction(GetAllDriverPaymentTransactionQuery command)
        {
            try
            {

                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost("GetDriverPaymentTransactionById")]
        public async Task<IActionResult> GetDriverPaymentTransactionById(GetDriverPaymentTransactionByIdQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPut("UpdateDriverPaymentTransactionById")]
        public async Task<IActionResult> UpdateDriverPaymentTransactionById(UpdateDriverPaymentTransactionCommand command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }



        [HttpPost("GetDriverPaymentTransactionByDriver")]
        public async Task<IActionResult> GetDriverPaymentTransactionByDriver(GetDriverPaymentTransactionByDriverPaymentHistoryQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }
    }
}
