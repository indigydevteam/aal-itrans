﻿using System;
using System.Threading.Tasks;
using iTrans.Transportation.Application.Features.CarLists.Backoffice;
using iTrans.Transportation.Application.Features.CarLists.Commands;
using iTrans.Transportation.Application.Features.CarLists.Queries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace iTrans.Transportation.WebApi.Controllers.V1
{
    [ApiVersion("1.0")]
    [Authorize]
    public class CarListController : BaseApiController
    {
        [HttpPost("GetCarListByType")]
        public async Task<IActionResult> GetCarListByType(GetCarListByTypeQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }
        [HttpPost("GetAllCarList")]
        public async Task<IActionResult> GetAllCarList(GetAllCarListQuery command)
        {
            try
            {

                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        //Backoffice
        [HttpPost("CreateCarList")]
        public async Task<IActionResult> Post(CreateCarListCommand command)
        {
            command.UserId = UserIdentity;
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("GetCarListByConditionWithPage")]
        public async Task<IActionResult> GetCarListByConditionWithPage(GetAllCarListParameter command)
        {
            try
            {

                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost("GetCarListById")]
        public async Task<IActionResult> GetCarListById(GetCarListByIdQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }
        [HttpPost("GetCarListByIdInfomation")]
        public async Task<IActionResult> GetCarListByIdInfomation(GetCarListByIdInfomationQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPut("UpdateCarListById")]
        public async Task<IActionResult> UpdateCarListById(UpdateCarListCommand command)
        {
            command.UserId = UserIdentity;
            return this.Ok(await this.Mediator.Send(command));
        }

        
        [HttpPost("Backoffice/GetAllCarList")]
        public async Task<IActionResult> GetAllCarList(GetAllCarList command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }        
        [HttpPost("Backoffice/DeleteCarListById")]
        public async Task<IActionResult> DeleteCarListById(DeleteCarListByIdCommand command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }
    }
}
