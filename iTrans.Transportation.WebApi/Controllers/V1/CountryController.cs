﻿using System;
using System.Linq;
using System.Threading.Tasks;
using iTrans.Transportation.Application.Features.Countries.Backoffice.Commands;
using iTrans.Transportation.Application.Features.Countries.Backoffice.Queries;
//using iTrans.Transportation.Application.Features.Countries.Commands;
using iTrans.Transportation.Application.Features.Countries.Queries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace iTrans.Transportation.WebApi.Controllers.V1
{
    [ApiVersion("1.0")]
    //[Authorize]
    public class CountryController : BaseApiController
    {
        
        [HttpPost("GetAllCountry")]
        public async Task<IActionResult> GetAllCountry(GetAllCountryQuery command)
        {
            try
            {
               
                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost("GetCountryById")]
        public async Task<IActionResult> GetCountryById(GetCountryByIdQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

       
        [HttpPost("GetAllCountryCode")]
        public async Task<IActionResult> GetAllCountryCode(GetAllCountryCodeQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }
        [HttpPost("GetCountryCodeById")]
        public async Task<IActionResult> GetCountryCodeById(GetCountryCodeByIdQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        //Backoffice
        [HttpPost("Backoffice/GetAll")]
        public async Task<IActionResult> GetAllCountryParameter(GetAllCountryParameter command)
        {
            try
            {

                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [HttpPost("Backoffice/GetById")]
        public async Task<IActionResult> GetById(BackofficeGetCountryByIdQuery command)
        {
            try
            {
                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [HttpPost("Backoffice/Create")]
        public async Task<IActionResult> Post(CreateCountryCommand command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }
        [HttpPut("Backoffice/UpdateById")]
        public async Task<IActionResult> UpdateCountryById(UpdateCountryCommand command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }
        [HttpPost("Backoffice/DeleteCountryById")]
        public async Task<IActionResult> DeleteCountryById(DeleteCountryByIdCommand command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }
    }
}
