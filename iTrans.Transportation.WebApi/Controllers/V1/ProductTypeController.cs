﻿using System;
using System.Threading.Tasks;
using iTrans.Transportation.Application.Features.ProductTypes.Queries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace iTrans.Transportation.WebApi.Controllers.V1
{
    [ApiVersion("1.0")]
    [Authorize]
    public class ProductTypeController : BaseApiController
    {
        [HttpPost("GetAllProductType")]
        public async Task<IActionResult> GetAllProductType(GetAllProductType command)
        {
            try
            {

                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost("GetProductTypeById")]
        public async Task<IActionResult> GetProductTypeById(GetProductTypeById command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        //[HttpPost("GetAllProductTypeByConditionWithPage")]
        //public async Task<IActionResult> GetAllProductTypeByConditionWithPage(GetAllProductTypeParameter command)
        //{
        //    try
        //    {

        //        return this.Ok(await this.Mediator.Send(command));
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        //[HttpPost("CreateProductType")]
        //public async Task<IActionResult> Post([FromForm] CreateProductTypeCommand command)
        //{
        //    command.UserId = UserIdentity;
        //    command.ContentDirectory = Configuration["ContentPath"];

        //    return this.Ok(await this.Mediator.Send(command));
        //}
        //[HttpPut("UpdateProductTypeById")]
        //public async Task<IActionResult> UpdateProductTypeById([FromForm] UpdateProductTypeCommand command)
        //{
        //    command.UserId = UserIdentity;
        //    command.ContentDirectory = Configuration["ContentPath"];

        //    return this.Ok(await this.Mediator.Send(command));
        //}

        //[HttpPost("Backofiice/GetAllProductType")]
        //public async Task<IActionResult> GetAllProductType(GetAllProductType command)
        //{
        //    return this.Ok(await this.Mediator.Send(command));
        //}

        //[HttpPost("Backofiice/DeleteProductTypeById")]
        //public async Task<IActionResult> DeleteProductTypeById(DeleteProductTypeByIdCommand command)
        //{
        //    return this.Ok(await this.Mediator.Send(command));
        //}
    }
}
