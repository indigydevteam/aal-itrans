﻿using System;
using System.Threading.Tasks;
using iTrans.Transportation.Application.Features.Subdistricts.Backoffice.Commands;
using iTrans.Transportation.Application.Features.Subdistricts.Backoffice.Queries;
using iTrans.Transportation.Application.Features.Subdistricts.Queries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace iTrans.Transportation.WebApi.Controllers.V1
{
    [ApiVersion("1.0")]
    //[Authorize]
    public class SubdistrictController : BaseApiController
    {
        
        [HttpPost("GetAllSubdistrict")]
        public async Task<IActionResult> GetAllSubdistrict(GetAllSubdistrictQuery command)
        {
            try
            {

                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost("GetSubdistrictById")]
        public async Task<IActionResult> GetSubdistrictById(GetSubdistrictByIdQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("GetSubdistrictByDistrict")]
        public async Task<IActionResult> GetSubdistrictByDistrict(GetSubdistrictByDistrictQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("GetPostCodeById")]
        public async Task<IActionResult> GetPostCodeById(GetPostCodeByIdQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        //Backoffice
        [HttpPost("Backoffice/GetAll")]
        public async Task<IActionResult> GetAllSubdistrictParameter(GetAllSubdistrictParameter command)
        {
            try
            {

                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost("Backoffice/Create")]
        public async Task<IActionResult> Post(CreateSubdistrictCommand command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPut("Backoffice/UpdateById")]
        public async Task<IActionResult> UpdateSubdistrictById(UpdateSubdistrictCommand command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("Backoffice/GetById")]
        public async Task<IActionResult> GetById(BackofficeGetSubdistrictByIdQuery command)
        {
            try
            {
                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}

