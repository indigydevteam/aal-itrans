﻿using System;
using System.Threading.Tasks;
using iTrans.Transportation.Application.Features.DriverAnnouncementLocations.Commands;
using iTrans.Transportation.Application.Features.DriverAnnouncementLocations.Queries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace iTrans.Transportation.WebApi.Controllers.V1
{
    [ApiVersion("1.0")]
    //[Authorize]
    public class DriverAnnouncementLocationController : BaseApiController
    {
      
        [HttpPost("CreateAnnouncementLocationCommand")]
        public async Task<IActionResult> Post(CreateDriverAnnouncementLocationCommand command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }


        [HttpPut("UpdateAnnouncementLocationById")]
        public async Task<IActionResult> UpdateAnnouncementLocationById(UpdateDriverAnnouncementLocationCommand command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("GetAnnouncementLocationById")]
        public async Task<IActionResult> GetAnnouncementLocationById(GetDriverAnnouncementLocationByIdQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

       
    }
}
