﻿using System;
using System.Threading.Tasks;
using iTrans.Transportation.Application.Features.Provinces.Backoffice.Commands;
using iTrans.Transportation.Application.Features.Provinces.Backoffice.Queries;
using iTrans.Transportation.Application.Features.Provinces.Queries;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace iTrans.Transportation.WebApi.Controllers.V1
{
    [ApiVersion("1.0")]
    //[Authorize]
    public class ProvinceController : BaseApiController
    {

        [HttpPost("GetAllProvince")]
        public async Task<IActionResult> GetAllProvince(GetAllProvinceQuery command)
        {
            try
            {

                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost("GetProvinceById")]
        public async Task<IActionResult> GetProvinceById(GetProvinceByIdQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("GetProvinceByCountry")]
        public async Task<IActionResult> GetProvinceByCountry(GetProvinceByCountryQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("GetProvinceByRegion")]
        public async Task<IActionResult> GetProvinceByRegion(GetProvinceByRegionQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        //Backoffice
        [HttpPost("Backoffice/GetAll")]
        public async Task<IActionResult> GetAllProvinceParameter(GetAllProvinceParameter command)
        {
            try
            {

                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost("Backoffice/Create")]
        public async Task<IActionResult> Post(CreateProvinceCommand command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPut("Backoffice/UpdateById")]
        public async Task<IActionResult> UpdateProvinceById(UpdateProvinceCommand command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("Backoffice/GetById")]
        public async Task<IActionResult> GetById(BackofficeGetProvinceByIdQuery command)
        {
            try
            {
                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}

