﻿using System;
using System.Threading.Tasks;
using iTrans.Transportation.Application.Features.ProblemTopics.Commands;
using iTrans.Transportation.Application.Features.ProblemTopics.Queries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace iTrans.Transportation.WebApi.Controllers.V1
{
    [ApiVersion("1.0")]
    //[Authorize]
    public class ProblemTopicController : BaseApiController
    {
        [HttpPost("CreateProblemTopic")]
        public async Task<IActionResult> Post(CreateProblemTopicCommand command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("GetAllProblemTopic")]
        public async Task<IActionResult> GetAllProblemTopic(GetAllProblemTopicQuery command)
        {
            try
            {

                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [HttpPost("GetAllProblemTopicByConditionWithPage")]
        public async Task<IActionResult> GetAllProblemTopicByConditionWithPage(GetAllProblemTopicParameter command)
        {
            try
            {

                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost("GetProblemTopicById")]
        public async Task<IActionResult> GetProblemTopicById(GetProblemTopicByIdQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPut("UpdateProblemTopicById")]
        public async Task<IActionResult> UpdateProblemTopicById(UpdateProblemTopicCommand command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("GetCustomerProblemTopic")]
        public async Task<IActionResult> GetCustomerProblemTopic(GetCustomerProblemTopicQuery command)
        {
            try
            {

                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [HttpPost("GetDriverProblemTopic")]
        public async Task<IActionResult> GetDriverProblemTopic(GetDriverProblemTopicQuery command)
        {
            try
            {

                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
