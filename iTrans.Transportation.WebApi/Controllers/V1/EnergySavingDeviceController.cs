﻿using System;
using System.Threading.Tasks;
using iTrans.Transportation.Application.Features.EnergySavingDevices.Queries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace iTrans.Transportation.WebApi.Controllers.V1
{
    [ApiVersion("1.0")]
    [Authorize]
    public class EnergySavingDeviceController : BaseApiController
    {
        [HttpPost("GetAllEnergySavingDevice")]
        public async Task<IActionResult> GetAllEnergySavingDevice(GetAllEnergySavingDevice command)
        {
            try
            {

                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost("GetEnergySavingDeviceList")]
        public async Task<IActionResult> GetList(GetEnergySavingDeviceListQuery command)
        {
            try
            {

                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost("GetEnergySavingDeviceById")]
        public async Task<IActionResult> GetEnergySavingDeviceById(GetEnergySavingDeviceById command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        //[HttpPost("CreateEnergySavingDevice")]
        //public async Task<IActionResult> Post(CreateEnergySavingDeviceCommand command)
        //{
        //    command.UserId = UserIdentity;

        //    return this.Ok(await this.Mediator.Send(command));
        //}

        //[HttpPost("GetAllEnergySavingDeviceByConditionWithPage")]
        //public async Task<IActionResult> GetAllEnergySavingDeviceByConditionWithPage(GetAllEnergySavingDeviceParameter command)
        //{
        //    try
        //    {

        //        return this.Ok(await this.Mediator.Send(command));
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        //[HttpPut("UpdateEnergySavingDeviceById")]
        //public async Task<IActionResult> UpdateEnergySavingDeviceById(UpdateEnergySavingDeviceCommand command)
        //{
        //    command.UserId = UserIdentity;

        //    return this.Ok(await this.Mediator.Send(command));
        //}
        //[HttpPost("Backoffice/GetAllEnergySavingDevice")]
        //public async Task<IActionResult> GetAllEnergySavingDevice(GetAllEnergySavingDevice command)
        //{
        //    return this.Ok(await this.Mediator.Send(command));
        //}
        //[HttpPost("Backoffice/DeleteEnergySavingDevice")]
        //public async Task<IActionResult> DeleteEnergySavingDevice(DeleteEnergySavingDeviceComand command)
        //{
        //    return this.Ok(await this.Mediator.Send(command));
        //}
    }
}
