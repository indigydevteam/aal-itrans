﻿

using iTrans.Transportation.Application.Features.OrderingProducts.Commands;
using iTrans.Transportation.Application.Features.OrderingProducts.Queries;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace iTrans.Transportation.WebApi.Controllers.V1
{
    [ApiVersion("1.0")]
    //[Authorize]
    public class OrderingProductController : BaseApiController
    {
        [HttpPost("GetAllOrderingProduct")]
        public async Task<IActionResult> GetAllOrderingProduct(GetAllOrderingProductQuery command)
        {
            try
            {

                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [HttpPost("GetOrderingProductById")]
        public async Task<IActionResult> GetOrderingProductById(GetOrderingProductByIdQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("GetOrderingProductByOrdering")]
        public async Task<IActionResult> GetOrderingProductByOrdering(GetOrderingProductByOrderingQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("Create")]
        public async Task<IActionResult> Post([FromForm] CreateOrderingProductCommand command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPut("UpdateById")]
        public async Task<IActionResult> UpdateOrderingProductById([FromForm] UpdateOrderingProductCommand command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }
        [HttpPost("DeleteById")]
        public async Task<IActionResult> DeleteeOrderingProductById(DeleteOrderingProductCommand command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }
    }
}