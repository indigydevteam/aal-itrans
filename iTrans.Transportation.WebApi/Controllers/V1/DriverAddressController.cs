﻿using System;
using System.Threading.Tasks;
using iTrans.Transportation.Application.Features.DriverAddresses.Commands;
using iTrans.Transportation.Application.Features.DriverAddresses.Queries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace iTrans.Transportation.WebApi.Controllers.V1
{
    [ApiVersion("1.0")]
    [Authorize]
    public class DriverAddressController : BaseApiController
    {
        [HttpPost("CreateDriverAddress")]
        public async Task<IActionResult> Post(CreateDriverAddressCommand command)
        {
            command.UserId = UserIdentity;
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("GetAllDriverAddress")]
        public async Task<IActionResult> GetAllDriverAddress(GetAllDriverAddressQuery command)
        {
            try
            {

                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost("GetDriverAddressById")]
        public async Task<IActionResult> GetDriverAddressById(GetDriverAddressByIdQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPut("UpdateDriverAddressById")]
        public async Task<IActionResult> UpdateDriverAddressById(UpdateDriverAddressCommand command)
        {
            command.UserId = UserIdentity;
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("DeleteDriverAddressById")]
        public async Task<IActionResult> DeleteDriverAddressById(DeleteDriverAddressByIdCommand command)
        {
            command.UserId = UserIdentity;
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("GetDriverAddressByDriver")]
        public async Task<IActionResult> GetDriverAddressByDriver(GetDriverAddressByDriverQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }
        [HttpPost("GetDriverTaxAddress")]
        public async Task<IActionResult> GetDriverTaxAddress(GetDriverTaxAddressQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }
        [HttpPost("GetDriverRegisterAddress")]
        public async Task<IActionResult> GetCustomerRegisterAddress(GetDriverRegisterAddressQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }
    }
}
