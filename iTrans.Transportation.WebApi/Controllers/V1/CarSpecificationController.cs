﻿using System;
using System.Threading.Tasks;
using iTrans.Transportation.Application.Features.CarSpecifications.Queries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace iTrans.Transportation.WebApi.Controllers.V1
{
    [ApiVersion("1.0")]
    //[Authorize]
    public class CarSpecificationController : BaseApiController
    {
        [HttpPost("GetAllCarSpecification")]
        public async Task<IActionResult> GetAllCarSpecification(GetAllCarSpecification command)
        {
            try
            {

                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost("GetCarSpecificationByCarType")]
        public async Task<IActionResult> GetCarSpecificationByCarList(GetCarSpecificationByCarType command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("GetCarSpecificationById")]
        public async Task<IActionResult> GetCarSpecificationById(GetCarSpecificationById command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }
        //[HttpPost("GetAllCarSpecificationConditionWithPage")]
        //public async Task<IActionResult> GetAllCarSpecificationConditionWithPage(GetAllCarSpecificationParameter command)
        //{
        //    try
        //    {

        //        return this.Ok(await this.Mediator.Send(command));
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        //[HttpPost("CreateCarSpecification")]
        //public async Task<IActionResult> Post(CreateCarSpecificationCommand command)
        //{
        //    command.UserId = UserIdentity;
        //    return this.Ok(await this.Mediator.Send(command));
        //}

        //[HttpPut("UpdateCarSpecificationById")]
        //public async Task<IActionResult> UpdateCarSpecificationById(UpdateCarSpecificationCommand command)
        //{
        //    command.UserId = UserIdentity;

        //    return this.Ok(await this.Mediator.Send(command));
        //}

        //[HttpPost("Backoffice/GetAllCarSpecification")]
        //public async Task<IActionResult> GetAllCarSpecification(GetAllCarSpecification command)
        //{
        //    return this.Ok(await this.Mediator.Send(command));
        //} 
        //[HttpPost("Backoffice/DeleteCarSpecificationById")]
        //public async Task<IActionResult> DeleteCarSpecificationById(DeleteCarSpecificationByIdCommand command)
        //{
        //    return this.Ok(await this.Mediator.Send(command));
        //}
    }
}