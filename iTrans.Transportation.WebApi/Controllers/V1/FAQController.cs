﻿using System;
using System.Threading.Tasks;
using iTrans.Transportation.Application.Features.FAQs.backoffice;
using iTrans.Transportation.Application.Features.FAQs.Commands;
using iTrans.Transportation.Application.Features.FAQs.Queries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace iTrans.Transportation.WebApi.Controllers.V1
{
    [ApiVersion("1.0")]
    [Authorize]
    public class FAQController : BaseApiController
    {
        [HttpPost("CreateFAQ")]
        public async Task<IActionResult> Post(CreateFAQCommand command)
        {
            command.UserId = UserIdentity;

            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("GetAllFAQ")]
        public async Task<IActionResult> GetAllFAQ(GetAllFAQQuery command)
        {
            try
            {

                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [HttpPost("GetAllFAQByConditionWithPage")]
        public async Task<IActionResult> GetAllFAQByConditionWithPage(GetAllFAQParameter command)
        {
            try
            {

                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost("GetFAQById")]
        public async Task<IActionResult> GetFAQById(GetFAQByIdQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPut("UpdateFAQById")]
        public async Task<IActionResult> UpdateFAQById(UpdateFAQCommand command)
        {
            command.UserId = UserIdentity;

            return this.Ok(await this.Mediator.Send(command));
        }
        [HttpPost("GetCustomerFAQ")]
        public async Task<IActionResult> GetCustomerFAQ(GetCustomerFAQQuery command)
        {
            try
            {

                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [HttpPost("GetDriverFAQ")]
        public async Task<IActionResult> GetDriverFAQ(GetDriverFAQQuery command)
        {
            try
            {

                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost("BackOffice/GetAllFAQ")]
        public async Task<IActionResult> GetAllFAQ(GetAllFAQ command)
        {
         
             return this.Ok(await this.Mediator.Send(command));
         
        }
        [HttpPost("BackOffice/CreateFAQ")]
        public async Task<IActionResult> CreateFAQ(CreateFAQ command)
        {
            command.UserId = UserIdentity;

            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("BackOffice/DeleteFAQ")]
        public async Task<IActionResult> DeleteFAQ(DeleteFAQ command)
        {
  
            return this.Ok(await this.Mediator.Send(command));
     
        }
    }
}
