﻿using iTrans.Transportation.Application.Features.Orderings.Commands;
using iTrans.Transportation.Application.Features.Test;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace iTrans.Transportation.WebApi.Controllers.V1
{
    [ApiVersion("1.0")]
    public class TestController : BaseApiController
    {
        [HttpPost("Test")]
        public async Task<IActionResult> GetAllOrdering(TestCommand command)
        {
            try
            {

                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [HttpPost("UpdateCurrentLocation")]
        public async Task<IActionResult> UpdateCurrentLocation(UpdateCurrentLocationCommand command)
        {
            command.UserId = UserIdentity;
            command.APP_ID = Configuration["OneSignal:ApplicationID"];
            command.REST_API_KEY = Configuration["OneSignal:RestApiKey"];
            command.AUTH_ID = Configuration["OneSignal:AuthenticationID"];

            return this.Ok(await this.Mediator.Send(command));
        }
    }
}
