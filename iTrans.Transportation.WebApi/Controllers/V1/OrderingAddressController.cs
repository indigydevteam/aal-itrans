﻿using iTrans.Transportation.Application.Features.OrderingAddressAddresses.Queries;
using iTrans.Transportation.Application.Features.OrderingAddresses.backoffice;
using iTrans.Transportation.Application.Features.OrderingAddresses.Commands;
using iTrans.Transportation.Application.Features.OrderingAddresses.Queries;

using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace iTrans.Transportation.WebApi.Controllers.V1
{
    [ApiVersion("1.0")]
    //[Authorize]
    public class OrderingAddressController : BaseApiController
    {
        [HttpPost("Create")]
        public async Task<IActionResult> Post(CreateOrderingAddressCommand command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("GetAll")]
        public async Task<IActionResult> GetAllOrderingAddress(GetAllOrderingAddressQuery command)
        {
            try
            {

                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [HttpPost("GetById")]
        public async Task<IActionResult> GetOrderingAddressById(GetOrderingAddressByIdQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        } 
        [HttpPost("GetByOrdering")]
        public async Task<IActionResult> GetOrderingAddressByOrdering(GetOrderingAddressByOrderingQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }
        [HttpPost("Backoffice/GetAllOrderingAddress")]
        public async Task<IActionResult> GetAllOrderingAddress(GetAllOrderingAddress command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("Backoffice/GetAllOrderingAddressByOrderingId")]
        public async Task<IActionResult> GetAllOrderingAddressByOrderingId(GetAllOrderingAddressByOrderingId command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPut("UpdateById")]
        public async Task<IActionResult> UpdateOrderingAddressById(UpdateOrderingAddressCommand command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }
        [HttpPost("Delete")]
        public async Task<IActionResult> DeleteOrderingAddress(DeleteOrderingAddressCommand command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }
        [HttpPost("UpdateOrderingAddressStatus")]
        public async Task<IActionResult> UpdateOrderingAddressStatus(UpdateOrderingAddressStatusCommand command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }
    }
}