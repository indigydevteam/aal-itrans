﻿using iTrans.Transportation.Application.Features.Role.Commands;
using iTrans.Transportation.Application.Features.Role.Qureies;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace iTrans.Transportation.WebApi.Controllers.V1
{
    public class RoleController : BaseApiController
    {
        [HttpPost("CreateRole")]
        public async Task<IActionResult> Create([FromForm] CreateRoleCommand command)
        {
            command.UserId = UserIdentity;

            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("EditRole")]
        public async Task<IActionResult> EditNews([FromForm] UpdateRoleCommand command)
        {
            command.UserId = UserIdentity;
            return this.Ok(await this.Mediator.Send(command));
        }


        [HttpPost("DeleteRole")]
        public async Task<IActionResult> DeleteRole( DeleteRoleCommand command)
        {
            command.UserId = UserIdentity;
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("GetAllRoleWithPage")]
        public async Task<IActionResult> GetAllRoleWithPage(GetAllRoleParameter command)
        {
            try
            {
                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost("GetAllRole")]
        public async Task<IActionResult> GetAllRole(GetAllRoleQuery command)
        {
            try
            {
                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost("GetRoleById")]
        public async Task<IActionResult> GetRoleById(GetRoleByIdQuery command)
        {
            try
            {
                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
