﻿using System;
using System.Linq;
using System.Threading.Tasks;
using iTrans.Transportation.Application.Features.CustomerProducts.Commands;
using iTrans.Transportation.Application.Features.CustomerProducts.Queries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace iTrans.Transportation.WebApi.Controllers.V1
{
    [ApiVersion("1.0")]
    [Authorize]
    public class CustomerProductController : BaseApiController
    {
        [HttpPost("CreateCustomerProduct")]
        public async Task<IActionResult> Post(CreateCustomerProductCommand command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("GetAllCustomerProduct")]
        public async Task<IActionResult> GetAllCustomerProduct(GetAllCustomerProductQuery command)
        {
            try
            {

                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost("GetCustomerProductById")]
        public async Task<IActionResult> GetCustomerProductById(GetCustomerProductByIdQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPut("UpdateCustomerProductById")]
        public async Task<IActionResult> UpdateCustomerProductById(UpdateCustomerProductCommand command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("CreateProduct")]
        public async Task<IActionResult> Post([FromForm] CreateProductCommand command)
        {
            if (UserIdentity != null)
            {
                command.UserId = UserIdentity.GetValueOrDefault();
            }
            return this.Ok(await this.Mediator.Send(command));
        }
        [HttpPost("GetProductByCustomer")]
        public async Task<IActionResult> GetProductByCustomer(GetProductByCustomerQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }
        [HttpPost("GetProductById")]
        public async Task<IActionResult> GetProductById(GetProductByIdQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }
        [HttpPut("UpdateProduct")]
        public async Task<IActionResult> UpdateProduct([FromForm] UpdateProductCommand command)
        {
            if (UserIdentity != null)
            {
                command.UserId = UserIdentity.GetValueOrDefault();
            }
            return this.Ok(await this.Mediator.Send(command));
        }
        [HttpPost("DeleteProduct")]
        public async Task<IActionResult> DeleteProducd(DeleteProductByIdCommand command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }
    }
}
