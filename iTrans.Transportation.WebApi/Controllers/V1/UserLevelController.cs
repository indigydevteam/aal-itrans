﻿using iTrans.Transportation.Application.Features.UserLevels.Backoffice;
using iTrans.Transportation.Application.Features.UserLevels.Commands;
using iTrans.Transportation.Application.Features.UserLevels.Queries;
using iTrans.Transportation.Application.Features.UserLevels.Backoffice.Queries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace iTrans.Transportation.WebApi.Controllers.V1
{
    [ApiVersion("1.0")]
    [Authorize]

    public class UserLevelController : BaseApiController
    {
        [HttpPost("CreateUserLevel")]
        public async Task<IActionResult> Post([FromForm] CreateUserLevelCommand command)
        {
            command.UserId = UserIdentity;

            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("GetAllUserLevel")]
        public async Task<IActionResult> GetAllUserLevel(GetAllUserLevelQuery command)
        {
            try
            {

                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [HttpPost("GetAllUserLevelByConditionWithPage")]
        public async Task<IActionResult> GetAllUserLevelByConditionWithPage(GetAllUserLevelParameter command)
        {
            try
            {

                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [HttpPost("GetUserLevelById")]
        public async Task<IActionResult> GetUserLevelById(GetUserLevelByIdQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPut("UpdateUserLevelById")]
        public async Task<IActionResult> UpdateUserLevelById([FromForm] UpdateUserLevelCommand command)
        {
            command.UserId = UserIdentity;

            return this.Ok(await this.Mediator.Send(command));
        }
        [HttpPost("GetCustomerLevel")]
        public async Task<IActionResult> GetCustomerLevel(GetCustomerLevelQuery command)
        {
            try
            {

                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost("GetDriverLevel")]
        public async Task<IActionResult> GetDriverLevel(GetDriverLevelQuery command)
        {
            try
            {

                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [HttpPost("GetAllCustomerLevel")]
        public async Task<IActionResult> GetAllCustomerLevel(GetAllCustomerLevelQuery command)
        {
            try
            {

                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost("GetAllDriverLevel")]
        public async Task<IActionResult> GetAllDriverLevel(GetAllDriverLevelQuery command)
        {
            try
            {

                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        //[HttpPost("Backoffice/GetAllUserLevel")]
        //public async Task<IActionResult> GetAllUserLevel(GetAllUserLevel command)
        //{
        //    return this.Ok(await this.Mediator.Send(command));
        //}
        //[HttpPost("Backoffice/DeleteUserLevelById")]
        //public async Task<IActionResult> DeleteUserLevelById(DeleteUserLevelByIdCommand command)
        //{
        //    return this.Ok(await this.Mediator.Send(command));
        //}
        //[HttpPost("Backoffice/GetUserLevelById")]
        //public async Task<IActionResult> GetBackofficeUserLevelById(BackofficeGetUserLevelByIdQuery command)
        //{
        //    return this.Ok(await this.Mediator.Send(command));
        //}
    
    }
}