﻿using System;
using System.Threading.Tasks;
using iTrans.Transportation.Application.Features.DriverCarFiles.Commands;
using iTrans.Transportation.Application.Features.DriverCarFiles.Queries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace iTrans.Transportation.WebApi.Controllers.V1
{
    [ApiVersion("1.0")]
    //[Authorize]
    public class DriverCarFileController : BaseApiController
    {
        [HttpPost("CreateDriverCarFile")]
        public async Task<IActionResult> Post(CreateDriverCarFileCommand command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("GetAllDriverCarFile")]
        public async Task<IActionResult> GetAllDriverCarFile(GetAllDriverCarFileQuery command)
        {
            try
            {

                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost("GetDriverCarFileById")]
        public async Task<IActionResult> GetDriverCarFileById(GetDriverCarFileByIdQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPut("UpdateDriverCarFileById")]
        public async Task<IActionResult> UpdateDriverCarFileById(UpdateDriverCarFileCommand command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }
    }
}
