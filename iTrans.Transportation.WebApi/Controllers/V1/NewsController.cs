﻿using System;
using System.Threading.Tasks;
using iTrans.Transportation.Application.Features.News.Commands;
using iTrans.Transportation.Application.Features.News.Queries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace iTrans.Transportation.WebApi.Controllers.V1
{
    [ApiVersion("1.0")]
    [ApiController]
    [Authorize]

    public class NewsController : BaseApiController
    {
       
        [HttpPost("CreateNews")]
        public async Task<IActionResult> CreateNews([FromForm] CreateNewsCommand command)
        {
            command.ContentDirectory = Configuration["ContentPath"];
            command.UserId = UserIdentity;

            command.Token =  Request.Headers["Authorization"].ToString();
            command.NotificationEndpoint = Configuration["NotificationEndpoint"];

            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("EditNews")]
        public async Task<IActionResult> EditNews([FromForm] UpdateNewsCommand command)
        {
            command.ContentDirectory = Configuration["ContentPath"];
            command.UserId = UserIdentity;

            command.Token = Request.Headers["Authorization"].ToString();
            command.NotificationEndpoint = Configuration["NotificationEndpoint"];

            command.UserId = UserIdentity;
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("DeleteNews")]
        public async Task<IActionResult> DeleteNews(DeleteNewsByIdCommand command)
        {
            command.UserId = UserIdentity;
            return this.Ok(await this.Mediator.Send(command));
        }
         

        [HttpPost("GetNews")]
        public async Task<IActionResult> GetNews(GetAllNewsParameter command)
        {
            command.Domain = Configuration["ContentEnvironment"];
            command.UserId = UserIdentity;
            command.UserRole = UserRole;
            command.UserType = UserType;
            command.IsGuest = false;

            return this.Ok(await this.Mediator.Send(command));
        }


        [HttpPost("GetNewsByID")]
        public async Task<IActionResult> GetNewsByID(GetNewsByIdQuery command)
        {
            command.Domain = Configuration["ContentEnvironment"];
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("GetNewsGuest")]
        [AllowAnonymous]
        public async Task<IActionResult> GetNewsGuest(GetAllNewsParameter command)
        {
            command.Domain = Configuration["ContentEnvironment"];
            command.IsGuest = true;
            return this.Ok(await this.Mediator.Send(command));
        } 
    }
}
