﻿using iTrans.Transportation.Application.Features.CustomerProblemes.Queries;
using iTrans.Transportation.Application.Features.CustomerProblems.Backoffice;
using iTrans.Transportation.Application.Features.CustomerProblems.Commands;
using iTrans.Transportation.Application.Features.CustomerProblems.Queries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace iTrans.Transportation.WebApi.Controllers.V1
{
    [ApiVersion("1.0")]
    [Authorize]

    public class CustomerProblemController : BaseApiController
    {
        [HttpPost("CreateCustomerProblem")]
        public async Task<IActionResult> Post([FromForm] CreateCustomerProblemCommand command)
        {
            command.UserId = UserIdentity;

            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("GetAllCustomerProblem")]
        public async Task<IActionResult> GetAllCustomerProblem(GetAllCustomerProblemQuery command)
        {
            try
            {

                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost("GetCustomerProblemById")]
        public async Task<IActionResult> GetCustomerProblemById(GetCustomerProblemByIdQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPut("UpdateCustomerProblemById")]
        public async Task<IActionResult> UpdateCustomerProblem(UpdateCustomerProblemCommand command)
        {
            command.UserId = UserIdentity;

            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("Backoffice/GetAllCustomerProblem")]
        public async Task<IActionResult> GetAllCustomerProblem(GetAllCustomerProblem command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("DeleteCustomerProblem")]
        public async Task<IActionResult> DeleteCustomerProblem(DeleteCustomerProblemCommand command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }
    }
}
