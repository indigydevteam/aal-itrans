﻿using System;
using System.Threading.Tasks;
using iTrans.Transportation.Application.Features.CarFeatures.Backoffice;
using iTrans.Transportation.Application.Features.CarFeatures.Commands;
using iTrans.Transportation.Application.Features.CarFeatures.Queries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace iTrans.Transportation.WebApi.Controllers.V1
{
    [ApiVersion("1.0")]
    [Authorize]
    public class CarFeatureController : BaseApiController
    {
        [HttpPost("CreateCarFeature")]
        public async Task<IActionResult> Post(CreateCarFeatureCommand command)
        {
            command.UserId = UserIdentity;
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("GetAllCarFeature")]
        public async Task<IActionResult> GetAllCarFeature(GetAllCarFeatureQuery command)
        {
            try
            {

                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [HttpPost("GetAllCarFeatureConditionWithPage")]
        public async Task<IActionResult> GetAllCarFeatureConditionWithPage(GetAllCarFeatureParameter command)
        {
            try
            {

                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost("GetCarFeatureById")]
        public async Task<IActionResult> GetCarFeatureById(GetCarFeatureByIdQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPut("UpdateCarFeatureById")]
        public async Task<IActionResult> UpdateCarFeatureById(UpdateCarFeatureCommand command)
        {
            command.UserId = UserIdentity;
            return this.Ok(await this.Mediator.Send(command));
        }
        [HttpPost("GetCarFeatureByCarList")]
        public async Task<IActionResult> GetCarFeatureByCarList(GetCarFeatureByCarListQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("Backoffice/GetAllCarFeature")]
        public async Task<IActionResult> GetAllCarFeature(GetAllCarFeature command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }  
        [HttpPost("Backoffice/DeleteCarFeatureById")]
        public async Task<IActionResult> DeleteCarFeatureById(DeleteCarFeatureByIdCommand command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }
    }
}
