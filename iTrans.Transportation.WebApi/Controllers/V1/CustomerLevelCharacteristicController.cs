﻿using iTrans.Transportation.Application.Features.CustomerLevelCharacteristics.Commands;
using iTrans.Transportation.Application.Features.CustomerLevelCharacteristics.Queries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace iTrans.Transportation.WebApi.Controllers.V1
{
    [ApiVersion("1.0")]
    [Authorize]
    public class CustomerLevelCharacteristicController : BaseApiController
    {
        [HttpPost("Backoffice/GetAll")]
        public async Task<IActionResult> Post(GetAllCustomerLevelCharacteristicQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }
        [HttpPost("Backoffice/GetById")]
        public async Task<IActionResult> GetCustomerLevelCharacteristic(GetCustomerLevelCharacteristicByIdQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }
        [HttpPost("Backoffice/Create")]
        public async Task<IActionResult> Post(CreateCustomerLevelCharacteristicCommand command)
        {
            command.UserId = UserIdentity;

            return this.Ok(await this.Mediator.Send(command));
        }
        [HttpPut("Backoffice/Update")]
        public async Task<IActionResult> UpdateCarFeatureById(UpdateCustomerLevelCharacteristicCommand command)
        {
            command.UserId = UserIdentity;

            return this.Ok(await this.Mediator.Send(command));
        }
        [HttpPost("Backoffice/Delete")]
        public async Task<IActionResult> DeleteCarFeatureById(DeleteCustomerLevelCharacteristicCommand command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }
    }
}
