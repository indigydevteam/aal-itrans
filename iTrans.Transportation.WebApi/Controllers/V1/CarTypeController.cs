﻿using System;
using System.Threading.Tasks;
using iTrans.Transportation.Application.Features.CarTypes.Queries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace iTrans.Transportation.WebApi.Controllers.V1
{
    [ApiVersion("1.0")]
    [Authorize]
    public class CarTypeController : BaseApiController
    {

        [HttpPost("GetAllCarType")]
        public async Task<IActionResult> GetAllCarType(GetAllCarType command)
        {
            try
            {
                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [HttpPost("GetCarTypeById")]
        public async Task<IActionResult> GetCarTypeById(GetCarTypeById command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        //[HttpPost("CreateCarType")]
        //public async Task<IActionResult> Post([FromForm] CreateCarTypeCommand command)

        //{
        //    command.UserId = UserIdentity;
        //    command.ContentDirectory = Configuration["ContentPath"];
        //    return this.Ok(await this.Mediator.Send(command));
        //}

        //[HttpPost("GetCarTypeByConditionWithPage")]
        //public async Task<IActionResult> GetAllCarTypeConditionWithPage(GetAllCarTypeParameter command)
        //{
        //    try
        //    {
        //        return this.Ok(await this.Mediator.Send(command));
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        //[HttpPut("UpdateCarTypeById")]
        //public async Task<IActionResult> UpdateCarTypeById([FromForm] UpdateCarTypeCommand command)
        //{
        //    command.UserId = UserIdentity;
        //    command.ContentDirectory = Configuration["ContentPath"];
        //    return this.Ok(await this.Mediator.Send(command));
        //}
        //[HttpPost("Backoffice/GetAllCarType")]
        //public async Task<IActionResult> GetAllCarType(GetAllCarType command)
        //{
        //    return this.Ok(await this.Mediator.Send(command));
        //}
        //[HttpPost("Backoffice/GetCarTypeById")]
        //public async Task<IActionResult> GetCarTypeById(GetCarTypeById command)
        //{
        //    return this.Ok(await this.Mediator.Send(command));
        //}
        //[HttpPost("Backoffice/DeleteCarTypeById")]
        //public async Task<IActionResult> DeleteCarTypeById(DeleteCarTypeByIdCommand command)
        //{
        //    return this.Ok(await this.Mediator.Send(command));
        //}
    }
}
