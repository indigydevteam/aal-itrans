﻿using System;
using System.Threading.Tasks;
using iTrans.Transportation.Application.Features.DriverCarLocations.Commands;
using iTrans.Transportation.Application.Features.DriverCarLocations.Queries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace iTrans.Transportation.WebApi.Controllers.V1
{
    [ApiVersion("1.0")]
    //[Authorize]
    public class DriverCarLocationController : BaseApiController
    {
        [HttpPost("CreateDriverCarLocation")]
        public async Task<IActionResult> Post(CreateDriverCarLocationCommand command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("GetAllDriverCarLocation")]
        public async Task<IActionResult> GetAllDriverCarLocation(GetAllDriverCarLocationQuery command)
        {
            try
            {

                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost("GetDriverCarLocationById")]
        public async Task<IActionResult> GetDriverCarLocationById(GetDriverCarLocationByIdQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPut("UpdateDriverCarLocationById")]
        public async Task<IActionResult> UpdateDriverCarLocationById(UpdateDriverCarLocationCommand command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }
    }
}
