﻿using System;
using System.Threading.Tasks;
using iTrans.Transportation.Application.Features.ContainerTypes.Backoffice;
using iTrans.Transportation.Application.Features.ContainerTypes.Commands;
using iTrans.Transportation.Application.Features.ContainerTypes.Queries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace iTrans.Transportation.WebApi.Controllers.V1
{
    [ApiVersion("1.0")]
    [Authorize]
    public class ContainerTypeController : BaseApiController
    {
        [HttpPost("CreateContainerType")]
        public async Task<IActionResult> Post(CreateContainerTypeCommand command)
        {
            command.UserId = UserIdentity;

            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("GetAllContainerType")]
        public async Task<IActionResult> GetAllContainerType(GetAllContainerTypeQuery command)
        {
            try
            {

                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [HttpPost("GetContainerTypeList")]
        public async Task<IActionResult> GetList(GetContainerTypeListQuery command)
        {
            try
            {

                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [HttpPost("GetAllContainerTypeByConditionWithPage")]
        public async Task<IActionResult> GetAllContainerTypeByConditionWithPage(GetAllContainerTypeParameter command)
        {
            try
            {

                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost("GetContainerTypeById")]
        public async Task<IActionResult> GetContainerTypeById(GetContainerTypeByIdQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPut("UpdateContainerTypeById")]
        public async Task<IActionResult> UpdateContainerTypeById(UpdateContainerTypeCommand command)
        {
            command.UserId = UserIdentity;

            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("Backoffice/GetAllContainerType")]
        public async Task<IActionResult> GetAllContainerType(GetAllContainerType command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("Backoffice/DeleteContainerTypeById")]
        public async Task<IActionResult> DeleteContainerTypeById(DeleteContainerTypeByIdCommand command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

    }
}
