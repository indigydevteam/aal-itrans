﻿using iTrans.Transportation.Application.Features.driverPassWords.Commands;
using iTrans.Transportation.Application.Features.Drivers.Backoffice.Commands;
using iTrans.Transportation.Application.Features.Drivers.Backoffice.Queries;
using iTrans.Transportation.Application.Features.Drivers.Commands;
using iTrans.Transportation.Application.Features.Drivers.Queries;
using iTrans.Transportation.Application.Features.Stars.Commands;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace iTrans.Transportation.WebApi.Controllers
{
    [ApiVersion("1.0")]
    [Authorize]
    public class DriverController : BaseApiController
    {
        public DriverController() : base()
        {

        }
        [HttpPost("CreateDriver")]
        public async Task<IActionResult> Post(CreateDriverCommand command)
        {
            command.UserId = UserIdentity;

            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("GetAllDrivers")]
        public async Task<IActionResult> GetAllDriver(GetAllDriverQuery command)
        {
            try
            {
                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost("GetDriverByConditionWithPage")]
        public async Task<IActionResult> GetDriverByConditionWithPage(GetAllDriverParameter command)
        {
            try
            {
                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost("GetDriverById")]
        public async Task<IActionResult> GetDriverById(GetDriverByIdQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("GetDriverWithAddressById")]
        public async Task<IActionResult> GetDriverWithAddressById(GetDriverWithAddressByIdQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPut("UpdateDriverById")]
        public async Task<IActionResult> UpdateDriverById(UpdateDriverCommand command)
        {
            command.UserId = UserIdentity;

            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("GetDriverInformationById")]
        public async Task<IActionResult> GetDriverInformationById(GetDriverInformationByIdQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("UpdateDriverInformationById")]
        public async Task<IActionResult> UpdateDriverInformationById([FromForm] UpdateDriverInformationByIdCommand command)
        {
            command.UserId = UserIdentity;

            return this.Ok(await this.Mediator.Send(command));
        }
        [HttpPost("GetDriverInformation")]
        public async Task<IActionResult> GetDriverInformationByAuth(GetDriverInformationByAuthQuery command)
        {
            if (UserIdentity != null)
            {
                command.Id = UserIdentity.GetValueOrDefault();
            }
           // command.Id = new Guid("ada70cf4-35d0-4b5c-ac42-9051d5a2c558");
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("GetDriverInformationByPhoneNumber")]
        public async Task<IActionResult> GetDriverInformationByPhoneNumber(GetDriverInformationByPhoneNumberQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }
        [HttpPost("UpdatePassword")]
        public async Task<IActionResult> UpdateDriverPasswordById(UpdatedriverPasswordByIdCommand command)
        {

            if (UserIdentity != null)
            {
                command.Id = UserIdentity.GetValueOrDefault();
            }
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPut("UpdateRanking")]
        public async Task<IActionResult> UpdateDriverStardById(UpdateDriverStarBIdCommand command)
        {
            if (UserIdentity != null)
            {
                command.UserId = UserIdentity.GetValueOrDefault();
            }
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("ConfirmPassword")]
        public async Task<IActionResult> ConfirmDriverPassword(ConfirmDriverPasswordQuery command)
        {
            if (UserIdentity != null)
            {
                command.Id = UserIdentity.GetValueOrDefault();
            }
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("CheckIdentityNumber")]
        public async Task<IActionResult> CheckIdentityNumber(CheckDriverIdentityNumberQuery command)
        {
            if (UserIdentity != null)
            {
                command.Id = UserIdentity.GetValueOrDefault();
            }
            return this.Ok(await this.Mediator.Send(command));
        }
        [HttpPost("Backoffice/GetDriverById")]
        public async Task<IActionResult> GetCustomerByIdBackOfficeQuery(GetDriverByIdBackOfficeQuery command)
        {
            
                command.ContentDirectory = Configuration["ContentEnvironment"];

                return this.Ok(await this.Mediator.Send(command));
            
        }

        [HttpPost("Backoffice/GetDriverByVerifyStatus")]
        public async Task<IActionResult> GetDriverByVerifyStatus(GetDriverByVerifyStatus command)
        {
           
             return this.Ok(await this.Mediator.Send(command));
      
        }

        [HttpPost("Backoffice/DeleteDriverById")]
        public async Task<IActionResult> DeleteDriverById(DeleteDriverByIdCommand command)
        {
            try
            {

                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [HttpPut("Backoffice/UpdateDriverById")]
        public async Task<IActionResult> UpdateCustomerById([FromForm] UpdateDriverById command)
        {
            try
            {
                command.UserId = UserIdentity;
                command.ContentDirectory = Configuration["ContentPath"];
                command.APP_ID = Configuration["DriverOneSignal:ApplicationID"];
                command.REST_API_KEY = Configuration["DriverOneSignal:RestApiKey"];
                command.AUTH_ID = Configuration["DriverOneSignal:AuthenticationID"];

                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [HttpPut("Backoffice/UpdataDriverTaxById")]
        public async Task<IActionResult> UpdataDriverAddressTaxById(UpdataDriverAddressTaxById command)
        {
            try
            {
                command.UserId = UserIdentity;
                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [HttpPost("Backoffice/GetDriverTaxById")]
        public async Task<IActionResult> GetDriverTaxById(GetDriverTaxById command)
        {
            try
            {

                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [HttpPost("Backoffice/GetAllDriver")]
        public async Task<IActionResult> GetAllDriver(GetAllDriver command)
        {
         
                return this.Ok(await this.Mediator.Send(command));
           
        }
        [HttpPost("UpdateLevel")]
        public async Task<IActionResult> UpdateLevel([FromForm] UpdateDriverLevelCommand command)
        {
            command.UserId = UserIdentity;

            return this.Ok(await this.Mediator.Send(command));
        }
        [HttpPost("VerifyIdentityNumber")]
        public async Task<IActionResult> VerifyIdentityNumber(VerifyDriverIdentityNumberQuery command)
        {
            if (UserIdentity != null)
            {
                command.Id = UserIdentity.GetValueOrDefault();
            }
            return this.Ok(await this.Mediator.Send(command));
        }
        [HttpPost("VerifyPhoneNumber")]
        public async Task<IActionResult> VerifyPhoneNumber(VerifyDriverPhoneNumberQuery command)
        {
            if (UserIdentity != null)
            {
                command.Id = UserIdentity.GetValueOrDefault();
            }
            return this.Ok(await this.Mediator.Send(command));
        }
        [HttpPost("VerifyPassword")]
        public async Task<IActionResult> VerifyPassword(VerifyDriverPasswordQuery command)
        {
            if (UserIdentity != null)
            {
                command.Id = UserIdentity.GetValueOrDefault();
            }
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("CheckTermAndCondition")]
        public async Task<IActionResult> CheckTermAndCondition(CheckDriverTermAndCondition command)
        {
            if (UserIdentity != null)
            {
                command.Id = UserIdentity.GetValueOrDefault();
            }
            return this.Ok(await this.Mediator.Send(command));
        }
        [HttpPost("Backoffice/CheckTermAndCondition")]
        public async Task<IActionResult> BackofficeCheckTermAndCondition(CheckDriverTermAndCondition command)
        {
            //if (UserIdentity != null)
            //{
            //    command.Id = UserIdentity.GetValueOrDefault();
            //}
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("VerifyStatus")]
        public async Task<IActionResult> VerifyStatus(VerifyDriverStatusCommand command)
        {
            if (UserIdentity != null)
            {
                command.Id = UserIdentity.GetValueOrDefault();
            }
            return this.Ok(await this.Mediator.Send(command));
        }
        [HttpPost("Delete")]
        public async Task<IActionResult> Delete(DeleteDriverCommand command)
        {
            if (UserIdentity != null)
            {
                command.Id = UserIdentity.GetValueOrDefault();
            }
            return this.Ok(await this.Mediator.Send(command));
        }
    }
}
