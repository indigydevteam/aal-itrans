﻿using iTrans.Transportation.Application.Features.OrderingCars.Commands;
using iTrans.Transportation.Application.Features.OrderingCars.Queries;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace iTrans.Transportation.WebApi.Controllers.V1
{
    [ApiVersion("1.0")]
    //[Authorize]
    public class OrderingCarController : BaseApiController
    {

        [HttpPost("GetAllOrderingCar")]
        public async Task<IActionResult> GetAllOrderingCar(GetAllOrderingCarQuery command)
        {
            try
            {

                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [HttpPost("GetOrderingCarById")]
        public async Task<IActionResult> GetOrderingCarById(GetOrderingCarByIdQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }
        [HttpPost("GetOrderingCarByOrdering")]
        public async Task<IActionResult> GetOrderingCarByOrdering(GetOrderingCarByOrderingQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }
        [HttpPost("Create")]
        public async Task<IActionResult> Post(CreateOrderingCarCommand command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }
        [HttpPut("UpdateById")]
        public async Task<IActionResult> UpdateOrderingCarById(UpdateOrderingCarCommand command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }
        [HttpPost("DeleteById")]
        public async Task<IActionResult> DeleteOrderingCar(DeleteOrderingCarCommand command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }
    }
}