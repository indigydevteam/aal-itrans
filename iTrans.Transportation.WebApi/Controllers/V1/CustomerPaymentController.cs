﻿using System;
using System.Threading.Tasks;
using iTrans.Transportation.Application.Features.CustomerPaymentes.Queries;
using iTrans.Transportation.Application.Features.CustomerPayments.Commands;
using iTrans.Transportation.Application.Features.CustomerPayments.Queries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace iTrans.Transportation.WebApi.Controllers.V1
{
    [ApiVersion("1.0")]
    [Authorize]
    public class CustomerPaymentController : BaseApiController
    {
        [HttpPost("GetAllCustomerPayment")]
        public async Task<IActionResult> GetAllPayment(GetAllCustomerPaymentQuery command)
        {
            try
            {

                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [HttpPost("GetByCustomer")]
        public async Task<IActionResult> GetPaymentByCustomer(GetCustomerPaymentByCustomerQuery command)
        {
            if (UserIdentity != null)
            {
                command.CustomerId = UserIdentity.GetValueOrDefault();
            }
            return this.Ok(await this.Mediator.Send(command));
        }
        [HttpPost("GetPaymentById")]
        public async Task<IActionResult> GetPaymentById(GetCustomerPaymentByIdQuery command)
        {
            if (UserIdentity != null)
            {
                command.CustomerId = UserIdentity.GetValueOrDefault();
            }
            return this.Ok(await this.Mediator.Send(command));
        }
        [HttpPost("CreatePayment")]
        public async Task<IActionResult> Post(CreateCustomerPaymentCommand command)
        {
            try
            {
                if (UserIdentity != null)
                {
                    command.CustomerId = UserIdentity.GetValueOrDefault();
                }
                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [HttpPut("UpdatePaymentById")]
        public async Task<IActionResult> UpdatePaymentById(UpdateCustomerPaymentCommand command)
        {
            if (UserIdentity != null)
            {
                command.CustomerId = UserIdentity.GetValueOrDefault();
            }
            return this.Ok(await this.Mediator.Send(command));
        }

        
        [HttpPost("DeletePaymentById")]
        public async Task<IActionResult> DeletePaymentyId(DeleteCustomerPaymentCommand command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }
    }
}
