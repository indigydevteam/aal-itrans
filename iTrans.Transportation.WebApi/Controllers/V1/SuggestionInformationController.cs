﻿using iTrans.Transportation.Application.Features.SuggestionInformations.Queries;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace iTrans.Transportation.WebApi.Controllers.V1
{
    [ApiVersion("1.0")]
    public class SuggestionInformationController : BaseApiController
    {
        [HttpPost("Get")]
        public async Task<IActionResult> GetAllRoleWithPage(GetSuggestionInformationsQuery command)
        {
            try
            {
                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
