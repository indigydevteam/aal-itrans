﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using iTrans.Transportation.Application.Features.DriverInsurances.Commands;
using iTrans.Transportation.Application.Features.DriverInsurances.Queries;
using iTrans.Transportation.Domain;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace iTrans.Transportation.WebApi.Controllers.V1
{
    [ApiVersion("1.0")]
    //[Authorize]
    public class DriverInsuranceController : BaseApiController
    {
        [HttpPost("CreateDriverInsurance")]
        public async Task<IActionResult> Post(CreateDriverInsuranceCommand command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("GetAllDriverInsurance")]
        public async Task<IActionResult> GetAllDriverInsurance(GetAllDriverInsuranceQuery command)
        {
            try
            {

                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost("GetDriverInsuranceById")]
        public async Task<IActionResult> GetDriverInsuranceById(GetDriverInsuranceByIdQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("GetDriverInsuranceByDriver")]
        public async Task<IActionResult> GetInsuranceByDriver(GetDriverInsuranceByDriverQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }


        [HttpPut("UpdateDriverInsuranceById")]
        public async Task<IActionResult> UpdateDriverInsuranceById(UpdateDriverInsuranceCommand command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }
    }
}
