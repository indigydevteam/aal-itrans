﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using iTrans.Transportation.Application.Features.ForgotPassword;
using iTrans.Transportation.Application.Features.ForgotPassword.Backoffice;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace iTrans.Transportation.WebApi.Controllers.V1
{
    [ApiVersion("1.0")]
    [Authorize]
    public class ForgotPasswordController : BaseApiController
    {
        [HttpPost("VerifyIdentityNumber")]
        public async Task<IActionResult> VerifyIdentityNumber(VerifyIdentityNumberQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("SetPassword")]
        public async Task<IActionResult> UpdateCustomerPasswordById(SetPasswordCommand command)
        {
            if (UserIdentity != null)
            {
                command.Id = UserIdentity.GetValueOrDefault();
            }

            return this.Ok(await this.Mediator.Send(command));
        }
        [HttpPost("BackOffice/CheckPassword")]
        public async Task<IActionResult> CkeckPassword(CheckPassword command)
        {
            if (UserIdentity != null)
            {
                command.UserId = UserIdentity;
            }

            return this.Ok(await this.Mediator.Send(command));
        }
    }
}
