﻿using System;
using System.Threading.Tasks;
using iTrans.Transportation.Application.Features.DriverProblems.Commands;
using iTrans.Transportation.Application.Features.DriverProblems.Queries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace iTrans.Transportation.WebApi.Controllers.V1
{
    [ApiVersion("1.0")]
    //[Authorize]
    public class DriverProblemController : BaseApiController
    {
        [HttpPost("CreateDriverProblem")]
        public async Task<IActionResult> Post([FromForm] CreateDriverProblemCommand command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("GetAllDriverProblem")]
        public async Task<IActionResult> GetAllDriverProblem(GetAllDriverProblemQuery command)
        {
            try
            {

                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost("GetDriverProblemById")]
        public async Task<IActionResult> GetDriverProblemById(GetDriverProblemByIdQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPut("UpdateDriverProblemById")]
        public async Task<IActionResult> UpdateDriverProblemById(UpdateDriverProblemCommand command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }
    }
}
