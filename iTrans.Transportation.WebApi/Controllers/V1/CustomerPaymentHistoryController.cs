﻿using System;
using System.Threading.Tasks;
using iTrans.Transportation.Application.Features.CustomerPaymentHistoryes.Queries;
using iTrans.Transportation.Application.Features.CustomerPaymentHistorys.Commands;
using iTrans.Transportation.Application.Features.CustomerPaymentHistorys.Queries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace iTrans.Transportation.WebApi.Controllers.V1
{
    [ApiVersion("1.0")]
    //[Authorize]
    public class CustomerPaymentHistoryController : BaseApiController
    {
        [HttpPost("CreatePaymentHistory")]
        public async Task<IActionResult> Post(CreateCustomerPaymentHistoryCommand command)
        {
            try
            {
                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPut("UpdatePaymentHistoryById")]
        public async Task<IActionResult> UpdateCustomerPaymentHistoryById(UpdateCustomerPaymentHistoryCommand command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("DeletePaymentHistoryById")]
        public async Task<IActionResult> DeletePaymentHistoryById(DeleteCustomerPaymentHistoryCommand command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("GetAllCustomerPaymentHistory")]
        public async Task<IActionResult> GetAllCustomerPaymentHistory(GetAllCustomerPaymentHistoryQuery command)
        {
            try
            {

                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost("GetPaymentHistoryById")]
        public async Task<IActionResult> GetCustomerPaymentHistoryById(GetCustomerPaymentHistoryByIdQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }


        [HttpPost("GetPaymentHistoryByCustomer")]
        public async Task<IActionResult> GetCustomerPaymentHistoryByCustomer(GetCustomerPaymentHistoryByCustomerQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }
        [HttpPost("GetPaymentHistoryByPayment")]
        public async Task<IActionResult> GetCustomerPaymentHistoryByPayment(GetCustomerPaymentHistoryByPaymentQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }
    }
}
