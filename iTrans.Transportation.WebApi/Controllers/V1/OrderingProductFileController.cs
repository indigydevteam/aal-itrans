﻿using System;
using System.Threading.Tasks;
using iTrans.Transportation.Application.Features.OrderingProductFiles.Commands;
using iTrans.Transportation.Application.Features.OrderingProductFiles.Queries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace iTrans.Transportation.WebApi.Controllers.V1
{
    [ApiVersion("1.0")]
    //[Authorize]
    public class OrderingProductFileController : BaseApiController
    {
        [HttpPost("CreateOrderingProductFile")]
        public async Task<IActionResult> Post(CreateOrderingProductFileCommand command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("GetAllOrderingProductFile")]
        public async Task<IActionResult> GetAllOrderingProductFile(GetAllOrderingProductFileQuery command)
        {
            try
            {

                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost("GetOrderingProductFileById")]
        public async Task<IActionResult> GetOrderingProductFileById(GetOrderingProductFileByIdQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPut("UpdateOrderingProductFileById")]
        public async Task<IActionResult> UpdateOrderingProductFileById(UpdateOrderingProductFileCommand command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }
    }
}
