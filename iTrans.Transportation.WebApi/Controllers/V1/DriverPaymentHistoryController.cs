﻿using System;
using System.Threading.Tasks;
using iTrans.Transportation.Application.Features.DriverPaymentHistoryes.Queries;
using iTrans.Transportation.Application.Features.DriverPaymentHistories.Commands;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using iTrans.Transportation.Application.Features.DriverPaymentHistorys.Queries;

namespace iTrans.Transportation.WebApi.Controllers.V1
{
    [ApiVersion("1.0")]
    //[Authorize]
    public class DriverPaymentHistoryController : BaseApiController
    {
        [HttpPost("CreateDriverPaymentHistory")]
        public async Task<IActionResult> Post(CreateDriverPaymentHistoryCommand command)
        {
            try
            {
                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost("GetAllDriverPaymentHistory")]
        public async Task<IActionResult> GetAllDriverPaymentHistory(GetAllDriverPaymentHistoryQuery command)
        {
            try
            {

                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost("GetDriverPaymentHistoryById")]
        public async Task<IActionResult> GetDriverPaymentHistoryById(GetDriverPaymentHistoryByIdQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPut("UpdateDriverPaymentHistoryById")]
        public async Task<IActionResult> UpdateDriverPaymentHistoryById(UpdateDriverPaymentHistoryCommand command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }



        [HttpPost("GetDriverPaymentHistoryByDriver")]
        public async Task<IActionResult> GetDriverPaymentHistoryByDriver(GetDriverPaymentHistoryByDriverQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }
    }
}
