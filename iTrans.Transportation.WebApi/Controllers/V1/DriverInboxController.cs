﻿using iTrans.Transportation.Application.Features.DriverInboxs.Commands;
using iTrans.Transportation.Application.Features.DriverInboxs.Queries;
using iTrans.Transportation.Application.Features.Inboxs.Commands;
using iTrans.Transportation.Application.Features.Inboxs.Queries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace iTrans.Transportation.WebApi.Controllers
{
    [ApiVersion("1.0")]
    [ApiController]
    [Authorize]
    public class DriverInboxController : BaseApiController
    {
        [HttpPost("GetByDriver")]
        public async Task<IActionResult> GetByDriver(GetInboxQuery command)
        {
            if (UserIdentity != null)
            {
                command.UserId = UserIdentity.GetValueOrDefault();
            }
            command.UserType = "driver";
            return this.Ok(await this.Mediator.Send(command));
        }
        [HttpPost("DeleteById")]
        public async Task<IActionResult> DeleteById(DeleteInboxByIdCommand command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }
        [HttpPost("UpdateRead")]
        public async Task<IActionResult> UpDateRead(UpdateIsReadByIdCommand command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }
    }
}
