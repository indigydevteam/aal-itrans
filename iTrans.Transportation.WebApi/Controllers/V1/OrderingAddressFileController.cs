﻿using iTrans.Transportation.Application.Features.OrderingAddressFiles.Commands;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace iTrans.Transportation.WebApi.Controllers.V1
{
    [ApiVersion("1.0")]
    //[Authorize]
    public class OrderingAddressFileController : BaseApiController
    {
        [HttpPost("Create")]
        public async Task<IActionResult> Post([FromForm] CreateOrderingAddressFileCommand command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }
        [HttpPut("Update")]
        public async Task<IActionResult> Put([FromForm] UpdateOrderingAddressFileCommand command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }
        [HttpPost("DeleteByIds")]
        public async Task<IActionResult> DeleteeOrderingProductById(DeleteOrderingAddressFileByIdCommand command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }
    }
}
