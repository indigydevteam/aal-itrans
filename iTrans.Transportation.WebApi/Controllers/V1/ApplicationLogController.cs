﻿using iTrans.Transportation.Application.Features.ApplicationLogs.BackOffice;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Features.ApplicationLogs.Queries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace iTrans.Transportation.WebApi.Controllers.V1
{
    [ApiVersion("1.0")]
    [Authorize]
    public class ApplicationLogController : BaseApiController
    {
        [HttpPost("CreateApplicationLog")]
        public async Task<IActionResult> Post(CreateApplicationLogCommand command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("GetAllApplicationLog")]
        public async Task<IActionResult> GetAllApplicationLog(GetAllApplicationLogQuery command)
        {
            try
            {

                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost("GetApplicationLogById")]
        public async Task<IActionResult> GetApplicationLogById(GetApplicationLogByIdQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }
        [HttpPost("BackOffice/GetAllApplicationLog")]
        public async Task<IActionResult> GetAllApplicationLog(GetAllApplicationLog command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }  
        [HttpPost("BackOffice/GetAllApplicationLogSelectModifiedby")]
        public async Task<IActionResult> GetAllApplicationLogSelectModifiedby(GetAllApplicationLogSelectModifiedby command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }
        [HttpPost("BackOffice/GetAllApplicationLogSelectModule")]
        public async Task<IActionResult> GetAllApplicationLogSelectModule(GetAllApplicationLogSelectModule command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPut("UpdateApplicationLogById")]
        public async Task<IActionResult> UpdateApplicationLogById(UpdateApplicationLogCommand command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }
    }
}
