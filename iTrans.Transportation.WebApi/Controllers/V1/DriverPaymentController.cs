﻿using System;
using System.Threading.Tasks;
using iTrans.Transportation.Application.Features.DriverPaymentes.Queries;
using iTrans.Transportation.Application.Features.DriverPayments.Commands;
using iTrans.Transportation.Application.Features.DriverPayments.Queries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace iTrans.Transportation.WebApi.Controllers.V1
{
    [ApiVersion("1.0")]
    [Authorize]
    public class DriverPaymentController : BaseApiController
    {
      
        [HttpPost("GetAllDriverPayment")]
        public async Task<IActionResult> GetAllDriverPayment(GetAllDriverPaymentQuery command)
        {
            try
            {

                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost("GetByDriver")]
        public async Task<IActionResult> GetDriverPaymentByDriver(GetDriverPaymentByDriverQuery command)
        {
            if (UserIdentity != null)
            {
                command.DriverId = UserIdentity.GetValueOrDefault();
            }
            return this.Ok(await this.Mediator.Send(command));
        }
        [HttpPost("GetPaymentById")]
        public async Task<IActionResult> GetDriverPaymentById(GetDriverPaymentByIdQuery command)
        {
            if (UserIdentity != null)
            {
                command.DriverId = UserIdentity.GetValueOrDefault();
            }
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("CreatePayment")]
        public async Task<IActionResult> Post(CreateDriverPaymentCommand command)
        {
            try
            {
                if (UserIdentity != null)
                {
                    command.DriverId = UserIdentity.GetValueOrDefault();
                }
                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPut("UpdatePaymentById")]
        public async Task<IActionResult> UpdateDriverPaymentById(UpdateDriverPaymentCommand command)
        {
            if (UserIdentity != null)
            {
                command.DriverId = UserIdentity.GetValueOrDefault();
            }
            return this.Ok(await this.Mediator.Send(command));
        }
        [HttpPost("DeletePaymentById")]
        public async Task<IActionResult> DeletePaymentyId(DeleteDriverPaymentCommand command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }
    }
}
