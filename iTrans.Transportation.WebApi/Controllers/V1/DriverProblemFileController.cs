﻿using System;
using System.Threading.Tasks;
using iTrans.Transportation.Application.Features.DriverProblemFiles.Commands;
using iTrans.Transportation.Application.Features.DriverProblemFiles.Queries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace iTrans.Transportation.WebApi.Controllers.V1
{
    [ApiVersion("1.0")]
    //[Authorize]
    public class DriverProblemFileController : BaseApiController
    {
        [HttpPost("CreateDriverProblemFile")]
        public async Task<IActionResult> Post(CreateDriverProblemFileCommand command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("GetAllDriverProblemFile")]
        public async Task<IActionResult> GetAllDriverProblemFile(GetAllDriverProblemFileQuery command)
        {
            try
            {

                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost("GetDriverProblemFileById")]
        public async Task<IActionResult> GetDriverProblemFileById(GetDriverProblemFileByIdQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPut("UpdateDriverProblemFileById")]
        public async Task<IActionResult> UpdateDriverProblemFileById(UpdateDriverProblemFileCommand command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }
    }
}
