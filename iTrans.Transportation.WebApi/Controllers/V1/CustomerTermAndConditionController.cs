﻿using System;
using System.Threading.Tasks;
using iTrans.Transportation.Application.Features.CustomerTermAndConditions.Commands;
using iTrans.Transportation.Application.Features.CustomerTermAndConditions.Queries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace iTrans.Transportation.WebApi.Controllers.V1
{
    [ApiVersion("1.0")]
    //[Authorize]
    public class CustomerTermAndConditionController : BaseApiController
    {
        [HttpPost("Create")]
        public async Task<IActionResult> Post(CreateCustomerTermAndConditionCommand command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("GetAllCustomerTermAndCondition")]
        public async Task<IActionResult> GetAllCustomerTermAndCondition(GetAllCustomerTermAndConditionQuery command)
        {
            try
            {

                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost("GetById")]
        public async Task<IActionResult> GetCustomerTermAndConditionById(GetCustomerTermAndConditionByIdQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }
        [HttpPost("GetByCustomer")]
        public async Task<IActionResult> GetTermAndConditionByCustomer(GetTermAndConditionByCustomerQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPut("UpdateCustomerTermAndConditionById")]
        public async Task<IActionResult> UpdateCustomerTermAndConditionById(UpdateCustomerTermAndConditionCommand command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }
    }
}
