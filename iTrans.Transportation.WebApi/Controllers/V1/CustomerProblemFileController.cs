﻿using iTrans.Transportation.Application.Features.CustomerPayments.Commands;
using iTrans.Transportation.Application.Features.CustomerProblemFiles.Commands;
using iTrans.Transportation.Application.Features.CustomerProblemFiles.Queries;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace iTrans.Transportation.WebApi.Controllers.V1
{
    [ApiVersion("1.0")]

    public class CustomerProblemFileController : BaseApiController
    {
        [HttpPost("CreateCustomerProblemFile")]
        public async Task<IActionResult> Post(CreateCustomerProblemFileCommand command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("GetAllCustomerProblemFile")]
        public async Task<IActionResult> GetAllCustomerProblemFile(GetAllCustomerProblemFileQuery command)
        {
            try
            {

                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost("GetCustomerProblemFileById")]
        public async Task<IActionResult> GetCustomerProblemFileById(GetCustomerProblemFileByIdQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPut("UpdateCustomerProblemFileById")]
        public async Task<IActionResult> UpdateCustomerProblemFile(UpdateCustomerProblemFileCommand command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("DeleteCustomerProblemFile")]
        public async Task<IActionResult> DeleteCustomerProblemFile(DeleteCustomerProblemFileCommand command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }
    }
}