﻿using iTrans.Transportation.Application.Features.OrderingDrivers.Commands;
using iTrans.Transportation.Application.Features.OrderingDrivers.Queries;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace iTrans.Transportation.WebApi.Controllers.V1
{
    [ApiVersion("1.0")]
    //[Authorize]
    public class OrderingDriverController : BaseApiController
    {
        [HttpPost("CreateOrderingDriver")]
        public async Task<IActionResult> Post(CreateOrderingDriverCommand command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("GetAllOrderingDriver")]
        public async Task<IActionResult> GetAllOrderingDriver(GetAllOrderingDriverQuery command)
        {
            try
            {

                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [HttpPost("GetOrderingDriverById")]
        public async Task<IActionResult> GetOrderingDriverById(GetOrderingDriverByIdQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }
        [HttpPost("GetOrderingDriverByOrderingCar")]
        public async Task<IActionResult> GetOrderingDriverByOrdering(GetOrderingDriverByOrderingCarQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPut("UpdateOrderingDriverById")]
        public async Task<IActionResult> UpdateOrderingDriverById(UpdateOrderingDriverCommand command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }
    }
}