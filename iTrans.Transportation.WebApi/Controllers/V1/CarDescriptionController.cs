﻿using System;
using System.Threading.Tasks;
using iTrans.Transportation.Application.Features.CarDescriptions.Backoffice;
using iTrans.Transportation.Application.Features.CarDescriptions.Commands;
using iTrans.Transportation.Application.Features.CarDescriptions.Queries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace iTrans.Transportation.WebApi.Controllers.V1
{
    [ApiVersion("1.0")]
    //[Authorize]
    public class CarDescriptionController : BaseApiController
    {
        [HttpPost("CreateCarDescription")]
        public async Task<IActionResult> Post(CreateCarDescriptionCommand command)
        {
            command.UserId = UserIdentity;

            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("GetAllCarDescription")]
        public async Task<IActionResult> GetAllCarDescription(GetAllCarDescriptionQuery command)
        {
            try
            {

                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [HttpPost("GetAllCarDescriptionConditionWithPage")]
        public async Task<IActionResult> GetAllCarDescriptionConditionWithPage(GetAllCarDescriptionParameter command)
        {
            try
            {

                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost("GetCarDescriptionById")]
        public async Task<IActionResult> GetCarDescriptionById(GetCarDescriptionByIdQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPut("UpdateCarDescriptionById")]
        public async Task<IActionResult> UpdateCarDescriptionById(UpdateCarDescriptionCommand command)
        {
            command.UserId = UserIdentity;

            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("GetCarDescriptionByCarList")]
        public async Task<IActionResult> GetCarDescriptionByCarList(GetCarDescriptionByCarListQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("BackOffice/GetAllCarDescription")]
        public async Task<IActionResult> GetAllCarDescription(GetAllCarDescription command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("BackOffice/DeleteCarDescriptionById")]
        public async Task<IActionResult> DeleteCarDescriptionById(DeleteCarDescriptionByIdCommand command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }
    }
}
