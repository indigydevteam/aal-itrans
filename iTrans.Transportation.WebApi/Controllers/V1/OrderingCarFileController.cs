﻿using System;
using System.Threading.Tasks;
using iTrans.Transportation.Application.Features.OrderingCarFiles.Commands;
using iTrans.Transportation.Application.Features.OrderingCarFiles.Queries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace iTrans.Transportation.WebApi.Controllers.V1
{
    [ApiVersion("1.0")]
    //[Authorize]
    public class OrderingCarFileController : BaseApiController
    {
        [HttpPost("CreateOrderingCarFile")]
        public async Task<IActionResult> Post(CreateOrderingCarFileCommand command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("GetAllOrderingCarFile")]
        public async Task<IActionResult> GetAllOrderingCarFile(GetAllOrderingCarFileQuery command)
        {
            try
            {

                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost("GetOrderingCarFileById")]
        public async Task<IActionResult> GetOrderingCarFileById(GetOrderingCarFileByIdQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPut("UpdateOrderingCarFileById")]
        public async Task<IActionResult> UpdateOrderingCarFileById(UpdateOrderingCarFileCommand command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }
    }
}
