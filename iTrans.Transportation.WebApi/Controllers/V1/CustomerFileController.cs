﻿using System;
using System.Threading.Tasks;
using iTrans.Transportation.Application.Features.CustomerFiles.Commands;
using iTrans.Transportation.Application.Features.CustomerFiles.Queries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace iTrans.Transportation.WebApi.Controllers.V1
{
    [ApiVersion("1.0")]
    //[Authorize]
    public class CustomerFileController : BaseApiController
    {
        [HttpPost("CreateCustomerFile")]
        public async Task<IActionResult> Post(CreateCustomerFileCommand command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("GetAllCustomerFile")]
        public async Task<IActionResult> GetAllCustomerFile(GetAllCustomerFileQuery command)
        {
            try
            {

                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost("GetCustomerFileById")]
        public async Task<IActionResult> GetCustomerFileById(GetCustomerFileByIdQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPut("UpdateCustomerFileById")]
        public async Task<IActionResult> UpdateCustomerFileById(UpdateCustomerFileCommand command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }
    }
}
