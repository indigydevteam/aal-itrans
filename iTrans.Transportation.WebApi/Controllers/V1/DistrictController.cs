﻿using System;
using System.Threading.Tasks;
using iTrans.Transportation.Application.Features.Districts.Backoffice.Commands;
using iTrans.Transportation.Application.Features.Districts.Backoffice.Queries;
using iTrans.Transportation.Application.Features.Districts.Queries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace iTrans.Transportation.WebApi.Controllers.V1
{
    [ApiVersion("1.0")]
    //[Authorize]
    public class DistrictController : BaseApiController
    {

        [HttpPost("GetAllDistrict")]
        public async Task<IActionResult> GetAllDistrict(GetAllDistrictQuery command)
        {
            try
            {

                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost("GetDistrictById")]
        public async Task<IActionResult> GetDistrictById(GetDistrictByIdQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("GetDistrictByProvince")]
        public async Task<IActionResult> GetDistrictByProvince(GetDistrictByProvinceQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }


        //Backoffice
        [HttpPost("Backoffice/GetAll")]
        public async Task<IActionResult> GetAllDistrictParameter(GetAllDistrictParameter command)
        {
            try
            {

                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost("Backoffice/Create")]
        public async Task<IActionResult> Post(CreateDistrictCommand command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPut("Backoffice/UpdateById")]
        public async Task<IActionResult> UpdateDistrictById(UpdateDistrictCommand command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("Backoffice/GetById")]
        public async Task<IActionResult> GetById(BackofficeGetDistrictByIdQuery command)
        {
            try
            {
                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}

