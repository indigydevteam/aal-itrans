﻿using System;
using System.Threading.Tasks;
//using iTrans.Transportation.Application.Features.Countries.Commands;
using iTrans.Transportation.Application.Features.Countries.Queries;
using iTrans.Transportation.Application.Features.CustomerFavoriteCares.Queries;
using iTrans.Transportation.Application.Features.CustomerFavoriteCars.Commands;
using iTrans.Transportation.Application.Features.CustomerFavoriteCars.Queries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace iTrans.Transportation.WebApi.Controllers.V1
{
    [ApiVersion("1.0")]
    [Authorize]
    public class CustomerFavoriteCarController : BaseApiController
    {
        [HttpPost("CreateFavoriteCar")]
        public async Task<IActionResult> Post(CreateCustomerFavoriteCarCommand command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }
        [HttpPut("UpdateFavoriteCarById")]
        public async Task<IActionResult> UpdateFavoriteCarById(UpdateCustomerFavoriteCarCommand command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("DeleteFavoriteCarById")]
        public async Task<IActionResult> DeleteFavoriteCarById(DeleteCustomerFavoriteCarCommand command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("GetAllFavoriteCar")]
        public async Task<IActionResult> GetAllCustomerFavoriteCar(GetAllCustomerFavoriteCarQuery command)
        {
            try
            {

                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost("GetFavoriteCarById")]
        public async Task<IActionResult> GetCustomerFavoriteCarById(GetCustomerFavoriteCarByIdQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }
        
        [HttpPost("GetFavoriteCarByCustomer")]
        public async Task<IActionResult> GetCustomerFavoriteCarBycustomer(GetCustomerFavoriteCarByCustomerQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

    }
}
