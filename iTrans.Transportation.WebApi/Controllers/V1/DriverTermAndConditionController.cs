﻿using System;
using System.Threading.Tasks;
using iTrans.Transportation.Application.Features.DriverTermAndConditions.Commands;
using iTrans.Transportation.Application.Features.DriverTermAndConditions.Queries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace iTrans.Transportation.WebApi.Controllers.V1
{
    [ApiVersion("1.0")]
    //[Authorize]
    public class DriverTermAndConditionController : BaseApiController
    {
        [HttpPost("Create")]
        public async Task<IActionResult> Post(CreateDriverTermAndConditionCommand command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("GetAllDriverTermAndCondition")]
        public async Task<IActionResult> GetAllDriverTermAndCondition(GetAllDriverTermAndConditionQuery command)
        {
            try
            {

                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost("GetById")]
        public async Task<IActionResult> GetDriverTermAndConditionById(GetDriverTermAndConditionByIdQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }
        [HttpPost("GetByDriver")]
        public async Task<IActionResult> GetTermAndConditionByDriver(GetTermAndConditionByDriverQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPut("UpdateDriverTermAndConditionById")]
        public async Task<IActionResult> UpdateDriverTermAndConditionById(UpdateDriverTermAndConditionCommand command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }
    }
}
