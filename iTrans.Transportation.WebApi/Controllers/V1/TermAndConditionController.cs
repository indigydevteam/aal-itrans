﻿using iTrans.Transportation.Application.Features.TermAndConditions.Backoffice;
using iTrans.Transportation.Application.Features.TermAndConditions.Backoffice.Commands;
using iTrans.Transportation.Application.Features.TermAndConditions.Commands;
using iTrans.Transportation.Application.Features.TermAndConditions.Queries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace iTrans.Transportation.WebApi.Controllers.V1
{
   
    [ApiVersion("1.0")]
    [Authorize]
    public class TermAndConditionController : BaseApiController
    {
        [HttpPost("CreateTermAndCondition")]
        public async Task<IActionResult> Post(CreateTermAndConditionCommand command)
        {
            command.UserId = UserIdentity;

            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("GetAllTermAndCondition")]
        public async Task<IActionResult> GetAllTermAndCondition(GetAllTermAndConditionQuery command)
        {
            try
            {
                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [HttpPost("GetTermAndConditionById")]
        public async Task<IActionResult> GetTermAndConditionById(GetTermAndConditionByIdQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPut("UpdateTermAndConditionById")]
        public async Task<IActionResult> UpdateTermAndConditionById(UpdateTermAndConditionCommand command)
        {
            command.UserId = UserIdentity;

            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("GetTermAndCondition")]
        public async Task<IActionResult> GetTermAndCondition(GetTermAndConditionQuery command)
        {
            try
            {
                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [HttpPost("GetCustomerTermAndCondition")]
        public async Task<IActionResult> GetCustomerTermAndCondition(GetCustomerTermAndConditionQuery command)
        {
            try
            {
                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [HttpPost("Backoffice/CreateTermAndCondition")]
        public async Task<IActionResult> CreateTermAndCondition(CreateTermAndCondition command)
        {
            try
            {
                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [HttpPost("Backoffice/GetAllTermAndCondition")]
        public async Task<IActionResult> GetAllTermAndCondition(GetAllTermAndCondition command)
        {
            try
            {
                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [HttpPost("Backoffice/GetTermAndConditionById")]
        public async Task<IActionResult> GetTermAndConditionById(GetTermAndConditionById command)
        {
            try
            {
                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
     
        [HttpPut("Backoffice/UpdatePubilcTermAndCondition")]
        public async Task<IActionResult> UpdatePubilcTermAndCondition(UpdatePubilcTermAndCondition command)
        {
            
                command.UserId = UserIdentity;

                return this.Ok(await this.Mediator.Send(command));
          
        }
        [HttpPut("Backoffice/UpdateTermAndConditionById")]
        public async Task<IActionResult> UpdateTermAndCondition(UpdateTermAndConditionById command)
        {
            
                command.UserId = UserIdentity;

                return this.Ok(await this.Mediator.Send(command));
          
        }
        [HttpPost("GetDriverTermAndCondition")]
        public async Task<IActionResult> GetDriverTermAndCondition(GetDriverTermAndConditionQuery command)
        {
            try
            {
                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}