﻿using System;
using System.Threading.Tasks;
using iTrans.Transportation.Application.Features.QRCodes.Commands;
using iTrans.Transportation.Application.Features.QRCodes.Queries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace iTrans.Transportation.WebApi.Controllers.V1
{
    [ApiVersion("1.0")]
    //[Authorize]
    public class QRCodeController : BaseApiController
    {
        [HttpPost("CreateQRCode")]
        public async Task<IActionResult> Post(CreateQRCodeCommand command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("GetAllQRCode")]
        public async Task<IActionResult> GetAllQRCode(GetAllQRCodeQuery command)
        {
            try
            {

                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost("GetQRCodeById")]
        public async Task<IActionResult> GetQRCodeById(GetQRCodeByIdQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPut("UpdateQRCodeById")]
        public async Task<IActionResult> UpdateQRCodeById(UpdateQRCodeCommand command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("GetQRCode")]
        public async Task<IActionResult> GetQRCode(GetQRCodeQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }
    }
}
