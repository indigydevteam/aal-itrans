﻿using iTrans.Transportation.Application.Features.UserLevelConditions.Commands;
using iTrans.Transportation.Application.Features.UserLevelConditions.Queries;
using iTrans.Transportation.Application.Features.UserLevels.Commands;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace iTrans.Transportation.WebApi.Controllers.V1
{
    [ApiVersion("1.0")]
    public class UserLevelConditionController : BaseApiController
    {
        [HttpPost("CreateUserLevelCondition")]
        public async Task<IActionResult> Post(CreateUserLevelConditionCommand command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("GetAllUserLevelCondition")]
        public async Task<IActionResult> GetAllUserLevelCondition(GetAllUserLevelConditionQuery command)
        {
            try
            {

                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [HttpPost("GetUserLevelConditionById")]
        public async Task<IActionResult> GetUserLevelConditionById(GetUserLevelConditionByIdQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPut("UpdateUserLevelConditionById")]
        public async Task<IActionResult> UpdateUserLevelConditionById(UpdateUserLevelConditionCommand command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }
    }
}