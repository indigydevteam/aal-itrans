﻿using System;
using System.Threading.Tasks;
using iTrans.Transportation.Application.Features.PaymentTypes.Queries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace iTrans.Transportation.WebApi.Controllers.V1
{
    [ApiVersion("1.0")]
    //[Authorize]
    public class PaymentTypeController : BaseApiController
    {
        [HttpPost("GetPaymentTypes")]
        public async Task<IActionResult> GetPaymentTypes(GetAllPaymentTypeQuery command)
        {
            try
            {

                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
