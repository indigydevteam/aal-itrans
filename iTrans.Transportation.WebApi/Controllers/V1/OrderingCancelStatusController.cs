﻿

using iTrans.Transportation.Application.Features.OrderingCancelStatusCancelStatuses.Queries;
using iTrans.Transportation.Application.Features.OrderingCancelStatuses.Backoffice;
using iTrans.Transportation.Application.Features.OrderingCancelStatuses.Commands;
using iTrans.Transportation.Application.Features.OrderingCancelStatuses.Queries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace iTrans.Transportation.WebApi.Controllers.V1
{
    [ApiVersion("1.0")]
    [Authorize]
    public class OrderingCancelStatusController : BaseApiController
    {
        [HttpPost("CreateOrderingCancelStatus")]
        public async Task<IActionResult> Post(CreateOrderingCancelStatusCommand command)
        {
            command.UserId = UserIdentity;

            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("GetAllOrderingCancelStatus")]
        public async Task<IActionResult> GetAllOrderingCancelStatus(GetAllOrderingCancelStatusQuery command)
        {
            try
            {

                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //[HttpPost("GetAllOrderingCancelStatusByConditionWithPage")]
        //public async Task<IActionResult> GetAllOrderingCancelStatusByConditionWithPage(Application.Features.OrderingCancelStatuses.Queries.GetAllOrderingCancelStatusParameter command)
        //{
        //    try
        //    {
        //        return this.Ok(await this.Mediator.Send(command));
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}
        [HttpPost("GetOrderingCancelStatusById")]
        public async Task<IActionResult> GetOrderingCancelStatusById(GetOrderingCancelStatusByIdQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPut("UpdateOrderingCancelStatusById")]
        public async Task<IActionResult> UpdateOrderingCancelStatusById(UpdateOrderingCancelStatusCommand command)
        {
            command.UserId = UserIdentity;

            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("GetCustomerCancelStatus")]
        public async Task<IActionResult> GetCustomerCancelStatus(GetAllCustomerOrderingCancelStatusQuery command)
        {
            try
            {
                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [HttpPost("GetDriverCancelStatus")]
        public async Task<IActionResult> GetDriverCancelStatus(GetAllDriverOrderingCancelStatusQuery command)
        {
            try
            {
                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        [HttpPost("Backoffice/GetAllOrderingCancelStatus")]
        public async Task<IActionResult> GetAllOrderingCancelStatus(Application.Features.OrderingCancelStatuses.Backoffice.GetAllOrderingCancelStatusParameter command)
        {

            return this.Ok(await this.Mediator.Send(command));

        }
        [HttpPost("Backoffice/DeleteOrderingCancelStatus")]
        public async Task<IActionResult> DeleteOrderingCancelStatus(DeleteOrderingCancelStatusCommand command)
        {

            return this.Ok(await this.Mediator.Send(command));

        }
        [HttpPost("Backoffice/GetOrderingCancelStatuses")]
        public async Task<IActionResult> GetOrderingCancelStatuses(GetAllOrderingCancelStatus command)
        {

            return this.Ok(await this.Mediator.Send(command));

        }
    }
}