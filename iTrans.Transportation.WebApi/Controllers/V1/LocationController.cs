﻿using iTrans.Transportation.Application.Features.Locations;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace iTrans.Transportation.WebApi.Controllers 
{
    [ApiVersion("1.0")]
    public class LocationController : BaseApiController
    {
        [HttpPost("GetLocation")]
        public async Task<IActionResult> Post(GetLocationQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }
    }
}
