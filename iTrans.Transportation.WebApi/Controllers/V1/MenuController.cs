﻿using iTrans.Transportation.Application.Features.Menu.backoffice;
using iTrans.Transportation.Application.Features.Menu.Commands;
using iTrans.Transportation.Application.Features.Menu.Queries;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace iTrans.Transportation.WebApi.Controllers.V1
{
    public class MenuController : BaseApiController
    {

        [HttpPost("CreateMenu")]
        public async Task<IActionResult> Create([FromForm] CreateMenuCommand command)
        {
            command.UserId = UserIdentity;
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("EditMenu")]
        public async Task<IActionResult> EditNews([FromForm] UpdateMenuCommand command)
        {
            command.UserId = UserIdentity;
            return this.Ok(await this.Mediator.Send(command));
        }


        [HttpPost("DeleteMenu")]
        public async Task<IActionResult> DeleteRole([FromForm] DeleteMenuCommand command)
        {
            command.UserId = UserIdentity;
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("GetAllMenuWithPage")]
        public async Task<IActionResult> GetAllRoleWithPage(GetAllMenuQueryParameter command)
        {
            try
            {
                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost("GetAllPermission")]
        public async Task<IActionResult> GetAllPermission(GetAllPermissionQuery command)
        {
            try
            {
                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [HttpPost("BackOffice/GetAllMenuPermission")]
        public async Task<IActionResult> GetAllMenuPermission(GetAllMenuPermission command)
        {
            try
            {
                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost("GetAllMenu")]
        public async Task<IActionResult> GetAllMenu(GetAllMenuQuery command)
        {
            try
            {
                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
