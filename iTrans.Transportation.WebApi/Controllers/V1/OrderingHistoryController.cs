﻿

using iTrans.Transportation.Application.Features.OrderingHistorys.Commands;
using iTrans.Transportation.Application.Features.OrderingHistorys.Queries;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace iTrans.Transportation.WebApi.Controllers.V1
{
    [ApiVersion("1.0")]
    //[Authorize]
    public class OrderingHistoryController : BaseApiController
    {
        [HttpPost("CreateOrderingHistory")]
        public async Task<IActionResult> Post(CreateOrderingHistoryCommand command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("GetAllOrderingHistory")]
        public async Task<IActionResult> GetAllOrderingHistory(GetAllOrderingHistoryQuery command)
        {
            try
            {

                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [HttpPost("GetOrderingHistoryById")]
        public async Task<IActionResult> GetOrderingHistoryById(GetOrderingHistoryByIdQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }
      
        [HttpPost("GetOrderingHistoryByOrdering")]
        public async Task<IActionResult> GetOrderingHistoryByOrdering(GetOrderingHistoryByOrderingQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }
       
        [HttpPost("GetOrderingHistoryByDriver")]
        public async Task<IActionResult> GetOrderingHistoryByDriver(GetOrderingHistoryByDriverQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }
      
        
        [HttpPost("GetOrderingHistoryByCustomer")]
        public async Task<IActionResult> GetOrderingHistoryByCustomer(GetOrderingHistoryByCustomerQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }
      
        [HttpPut("UpdateOrderingHistoryById")]
        public async Task<IActionResult> UpdateOrderingHistoryById(UpdateOrderingHistoryCommand command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }
    }
}