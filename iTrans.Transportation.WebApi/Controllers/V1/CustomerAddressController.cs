﻿using System;
using System.Threading.Tasks;
using iTrans.Transportation.Application.Features.CustomerAddresses.Commands;
using iTrans.Transportation.Application.Features.CustomerAddresses.Queries;
using iTrans.Transportation.Application.Features.Customers.Backoffice.Commands;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace iTrans.Transportation.WebApi.Controllers.V1
{
    [ApiVersion("1.0")]
    //[Authorize]
    public class CustomerAddressController : BaseApiController
    {
        [HttpPost("CreateCustomerAddress")]
        public async Task<IActionResult> Post(CreateCustomerAddressCommand command)
        {
            try
            {
                command.UserId = UserIdentity;
                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost("GetAllCustomerAddress")]
        public async Task<IActionResult> GetAllCustomerAddress(GetAllCustomerAddressQuery command)
        {
            try
            {

                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost("GetCustomerAddressById")]
        public async Task<IActionResult> GetCustomerAddressById(GetCustomerAddressByIdQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }
        [HttpPost("GetCustomerRegisterAddress")]
        public async Task<IActionResult> GetCustomerRegisterAddress(GetCustomerRegisterAddressQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }


        [HttpPut("UpdateCustomerAddressById")]
        public async Task<IActionResult> UpdateCustomerAddressById(UpdateCustomerAddressCommand command)
        {
            command.UserId = UserIdentity;
            return this.Ok(await this.Mediator.Send(command));
        }
     

        [HttpPost("DeleteCustomerAddressById")]
        public async Task<IActionResult> DeleteCustomerAddressById(DeleteCustomerAddressByIdCommand command)
        {
            command.UserId = UserIdentity;
            return this.Ok(await this.Mediator.Send(command));
        }
        
        [HttpPost("GetCustomerAddressByCustomer")]
        public async Task<IActionResult> GetCustomerAddressByCustomer(GetCustomerAddressByCustomerQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        
        [HttpPost("GetCustomerFavoriteAddress")]
        public async Task<IActionResult> GetCustomerFavoriteAddress(GetCustomerFavoriteAddressQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }
        [HttpPost("GetCustomerTaxAddress")]
        public async Task<IActionResult> GetCustomerTaxAddress(GetCustomerTaxAddressQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }
    }
}
