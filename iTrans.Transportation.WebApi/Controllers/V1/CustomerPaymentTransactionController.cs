﻿using System;
using System.Threading.Tasks;
using iTrans.Transportation.Application.Features.CustomerPaymentTransactiones.Queries;
using iTrans.Transportation.Application.Features.CustomerPaymentTransactions.Commands;
using iTrans.Transportation.Application.Features.CustomerPaymentTransactions.Queries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace iTrans.Transportation.WebApi.Controllers.V1
{
    [ApiVersion("1.0")]
    //[Authorize]
    public class CustomerPaymentTransactionController : BaseApiController
    {
        [HttpPost("CreateCustomerPaymentTransaction")]
        public async Task<IActionResult> Post(CreateCustomerPaymentTransactionCommand command)
        {
            try
            {
                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost("GetAllCustomerPaymentTransaction")]
        public async Task<IActionResult> GetAllCustomerPaymentTransaction(GetAllCustomerPaymentTransactionQuery command)
        {
            try
            {

                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost("GetCustomerPaymentTransactionById")]
        public async Task<IActionResult> GetCustomerPaymentTransactionById(GetCustomerPaymentTransactionByIdQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPut("UpdateCustomerPaymentTransactionById")]
        public async Task<IActionResult> UpdateCustomerPaymentTransactionById(UpdateCustomerPaymentTransactionCommand command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }


        [HttpPost("GetCustomerPaymentTransactionByCustomer")]
        public async Task<IActionResult> GetCustomerPaymentTransactionByCustomer(GetCustomerPaymentTransactionByCustomerPaymentHistoryQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }
    }
}
