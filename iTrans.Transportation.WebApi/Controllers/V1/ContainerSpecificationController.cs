﻿using System;
using System.Threading.Tasks;
using iTrans.Transportation.Application.Features.CarSpecifications.Queries;
using iTrans.Transportation.Application.Features.ContainerSpecifications.Backoffice;
using iTrans.Transportation.Application.Features.ContainerSpecifications.Commands;
using iTrans.Transportation.Application.Features.ContainerSpecifications.Queries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace iTrans.Transportation.WebApi.Controllers.V1
{
    [ApiVersion("1.0")]
    [Authorize]
    public class ContainerSpecificationController : BaseApiController
    {
        [HttpPost("CreateContainerSpecification")]
        public async Task<IActionResult> Post(CreateContainerSpecificationCommand command)
        {
            command.UserId = UserIdentity;

            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("GetAllContainerSpecification")]
        public async Task<IActionResult> GetAllContainerSpecification(GetAllContainerSpecificationQuery command)
        {
            try
            {

                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [HttpPost("GetContainerSpecificationList")]
        public async Task<IActionResult> GetList(GetContainerSpecificationListQuery command)
        {
            try
            {

                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [HttpPost("GetAllContainerSpecificationByConditionWithPage")]
        public async Task<IActionResult> GetAllContainerSpecificationByConditionWithPage(GetAllContainerSpecificationParameter command)
        {
            try
            {

                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost("GetContainerSpecificationById")]
        public async Task<IActionResult> GetContainerSpecificationById(GetContainerSpecificationByIdQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("GetCarSpecificationByCarType")]
        public async Task<IActionResult> GetCarSpecificationByCarList(GetCarSpecificationByCarTypeQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPut("UpdateContainerSpecificationById")]
        public async Task<IActionResult> UpdateContainerSpecificationById(UpdateContainerSpecificationCommand command)
        {
            command.UserId = UserIdentity;

            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("Backoffice/GetAllContainerSpecification")]
        public async Task<IActionResult> GetAllContainerSpecification(GetAllContainerSpecification command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("Backoffice/DeleteContainerSpecificationId")]
        public async Task<IActionResult> DeleteContainerSpecificationId(DeleteContainerSpecificationIdCommand command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }
    }
}
