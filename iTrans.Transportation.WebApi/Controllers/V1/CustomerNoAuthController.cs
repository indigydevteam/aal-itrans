﻿using System;
using System.Linq;
using System.Threading.Tasks;
using iTrans.Transportation.Application.Features.CustomerPassWords.Commands;
using iTrans.Transportation.Application.Features.Customers.Commands;
using iTrans.Transportation.Application.Features.Customers.Queries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace iTrans.Transportation.WebApi.Controllers
{
    [ApiVersion("1.0")]
    //[Authorize]
    public class CustomerNoAuthController : BaseApiController
    {
        [HttpPost("CheckExistIdentityNumber")]
        public async Task<IActionResult> CheckIdentityNumber(CheckExistCustomerIdentityNumberQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("CheckExistPhoneNumber")]
        public async Task<IActionResult> CheckPhoneNumber(CheckExistCustomerPhoneNumberQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }
    }
}
