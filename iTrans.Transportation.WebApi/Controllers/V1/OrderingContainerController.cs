﻿using iTrans.Transportation.Application.Features.OrderingContainers.Commands;
using iTrans.Transportation.Application.Features.OrderingContainers.Queries;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace iTrans.Transportation.WebApi.Controllers.V1
{
  
        [ApiVersion("1.0")]
        //[Authorize]
        public class OrderingContainerController : BaseApiController
        {
            [HttpPost("Create")]
            public async Task<IActionResult> Post(CreateOrderingContainerCommand command)
            {
                return this.Ok(await this.Mediator.Send(command));
            }
        [HttpPut("UpdateById")]
        public async Task<IActionResult> UpdateOrderingContainerById(UpdateOrderingContainerByIdCommand command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }
        [HttpPut("DeleteById")]
        public async Task<IActionResult> DeleteOrderingContainerById(DeleteOrderingContainerByIdCommand command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }
        [HttpPost("GetAllOrderingContainer")]
            public async Task<IActionResult> GetAllOrderingContainer(GetAllOrderingContainerQuery command)
            {
                try
                {

                    return this.Ok(await this.Mediator.Send(command));
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            [HttpPost("GetOrderingContainerById")]
            public async Task<IActionResult> GetOrderingContainerById(GetOrderingContainerByIdQuery command)
            {
                return this.Ok(await this.Mediator.Send(command));
            }
            [HttpPost("GetOrderingContainerByOrdering")]
            public async Task<IActionResult> GetOrderingContainerByOrdering(GetOrderingContainerByOrderingQuery command)
            {
                return this.Ok(await this.Mediator.Send(command));
            }

           
        }
    }