﻿using System;
using System.Threading.Tasks;
using iTrans.Transportation.Application.Features.DriverFiles.Commands;
using iTrans.Transportation.Application.Features.DriverFiles.Queries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace iTrans.Transportation.WebApi.Controllers.V1
{
    [ApiVersion("1.0")]
    //[Authorize]
    public class DriverFileController : BaseApiController
    {
        [HttpPost("CreateDriverFile")]
        public async Task<IActionResult> Post(CreateDriverFileCommand command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("GetAllDriverFile")]
        public async Task<IActionResult> GetAllDriverFile(GetAllDriverFileQuery command)
        {
            try
            {

                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost("GetDriverFileById")]
        public async Task<IActionResult> GetDriverFileById(GetDriverFileByIdQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPut("UpdateDriverFileById")]
        public async Task<IActionResult> UpdateDriverFileById(UpdateDriverFileCommand command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }
    }
}
