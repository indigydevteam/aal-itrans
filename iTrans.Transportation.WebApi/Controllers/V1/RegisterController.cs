﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using iTrans.Transportation.Application.Features.Register.Commands;
using iTrans.Transportation.Domain;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.IO;
using iTrans.Transportation.Application.Features.Register;

namespace iTrans.Transportation.WebApi.Controllers.V1
{
    [ApiVersion("1.0")]
    //[Authorize]
    public class RegisterController : BaseApiController
    {
        [HttpPost("Register")]
        public async Task<IActionResult> Post([FromForm] RegisterCommand command)
        {

            return this.Ok(await this.Mediator.Send(command));
        }
        [HttpPost("VerifyIdentityNumber")]
        public async Task<IActionResult> VerifyIdentityNumber(RegisterVerifyIdentityNumberQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("VerifyPhoneNumber")]
        public async Task<IActionResult> VerifyPhoneNumber(RegisterVerifyPhoneNumberQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }
    }
}
