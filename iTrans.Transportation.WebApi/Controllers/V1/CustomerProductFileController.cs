﻿using System;
using System.Threading.Tasks;
using iTrans.Transportation.Application.Features.CustomerProductFiles.Commands;
using iTrans.Transportation.Application.Features.CustomerProductFiles.Queries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace iTrans.Transportation.WebApi.Controllers.V1
{
    [ApiVersion("1.0")]
    //[Authorize]
    public class CustomerProductFileController : BaseApiController
    {
        [HttpPost("CreateCustomerProductFile")]
        public async Task<IActionResult> Post(CreateCustomerProductFileCommand command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("GetAllCustomerProductFile")]
        public async Task<IActionResult> GetAllCustomerProductFile(GetAllCustomerProductFileQuery command)
        {
            try
            {

                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost("GetCustomerProductFileById")]
        public async Task<IActionResult> GetCustomerProductFileById(GetCustomerProductFileByIdQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPut("UpdateCustomerProductFileById")]
        public async Task<IActionResult> UpdateCustomerProductFileById(UpdateCustomerProductFileCommand command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }
    }
}
