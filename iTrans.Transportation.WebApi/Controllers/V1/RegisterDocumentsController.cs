﻿using iTrans.Transportation.Application.Features.RegisterDocuments.Commands;
using iTrans.Transportation.Application.Features.RegisterDocuments.Queries;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace iTrans.Transportation.WebApi.Controllers.V1
{
   
        [ApiVersion("1.0")]
        public class RegisterDocumentsController : BaseApiController
        {
            [HttpPost("CreateRegisterDocument")]
            public async Task<IActionResult> Post(CreateRegisterDocumentCommand command)
            {
                return this.Ok(await this.Mediator.Send(command));
            }

            [HttpPost("GetAllRegisterDocument")]
            public async Task<IActionResult> GetAllRegisterDocument(GetAllRegisterDocumentQuery command)
            {
                try
                {

                    return this.Ok(await this.Mediator.Send(command));
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        [HttpPost("GetAllRegisterDocumentByConditionWithPage")]
        public async Task<IActionResult> GetAllRegisterDocumentByConditionWithPage(GetAllRegisterDocumentParameter command)
        {
            try
            {

                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost("GetRegisterDocumentById")]
            public async Task<IActionResult> GetRegisterDocumentById(GetRegisterDocumentByIdQuery command)
            {
                return this.Ok(await this.Mediator.Send(command));
            }

            [HttpPut("UpdateRegisterDocumentById")]
            public async Task<IActionResult> UpdateRegisterDocumentById(UpdateRegisterDocumentCommand command)
            {
                return this.Ok(await this.Mediator.Send(command));
            }
        }
    }