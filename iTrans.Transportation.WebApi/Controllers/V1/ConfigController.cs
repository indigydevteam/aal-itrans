﻿using iTrans.Transportation.Application.Features.Configs.Queries;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace iTrans.Transportation.WebApi.Controllers.V1
{
    [ApiVersion("1.0")]
    public class ConfigController : BaseApiController
    {
        [HttpPost("GetTimespanUpdate")]
        public async Task<IActionResult> GetAllDistrict(GetTimespanUpdateQuery command)
        {
            try
            {

                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
