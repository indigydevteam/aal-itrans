﻿using iTrans.Transportation.Application.Features.RegisterInformations.Queries;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace iTrans.Transportation.WebApi.Controllers.V1
{
    [ApiVersion("1.0")]
    public class RegisterInformationController : BaseApiController
    {
        //[HttpPost("CreateRegisterInformation")]
        //public async Task<IActionResult> Post(CreateRegisterInformationCommand command)
        //{
        //    return this.Ok(await this.Mediator.Send(command));
        //}

        [HttpPost("GetAllRegisterInformation")]
        public async Task<IActionResult> GetAllRegisterInformation(GetAllRegisterInformation command)
        {
            try
            {

                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        //[HttpPost("GetAllRegisterInformationByConditionWithPage")]
        //public async Task<IActionResult> GetAllRegisterInformationByConditionWithPage(GetAllRegisterInformationParameter command)
        //{
        //    try
        //    {

        //        return this.Ok(await this.Mediator.Send(command));
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}
        [HttpPost("GetRegisterInformationById")]
        public async Task<IActionResult> GetRegisterInformationById(GetRegisterInformationById command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        //[HttpPut("UpdateRegisterInformationById")]
        //public async Task<IActionResult> UpdateRegisterInformationById(UpdateRegisterinformationCommand command)
        //{
        //    return this.Ok(await this.Mediator.Send(command));
        //}

        [HttpPost("GetCustomerRegisterInformation")]
        public async Task<IActionResult> GetCustomerRegisterInformation(GetCustomerRegisterInformation command)
        {
            try
            {

                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [HttpPost("GetDriverRegisterInformation")]
        public async Task<IActionResult> GetDriverRegisterInformation(GetDriverRegisterInformation command)
        {
            try
            {

                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}