﻿using iTrans.Transportation.Application.Features.WelcomeInformations.Backoffice.Commands;
using iTrans.Transportation.Application.Features.WelcomeInformations.Queries;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace iTrans.Transportation.WebApi.Controllers.V1
{
      [ApiVersion("1.0")]
    public class WelcomeInformationController : BaseApiController
    {
        [HttpPost("GetAllWelcomeInformation")]
        public async Task<IActionResult> GetAllWelcomeInformation( GetAllWelcomeInformation command)
        {
            try
            {

                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [HttpPost("GetWelcomeInformationById")]
        public async Task<IActionResult> GetWelcomeInformationById(GetWelcomeInformationById command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("GetWelcomeInformation")]
        public async Task<IActionResult> GetWelcomeInformationByModule(GetWelcomeInformationByModule command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }
        [HttpPost("GetCustomerWelcomeInformation")]
        public async Task<IActionResult> GetCustomerWelcomeInformation(GetCustomerWelcomeInformation command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }
        [HttpPost("GetDriverWelcomeInformation")]
        public async Task<IActionResult> GetDriverWelcomeInformation(GetDriverWelcomeInformation command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        //[HttpPost("GetAllWelcomeInformationStatusByConditionWithPage")]
        //public async Task<IActionResult> GetAllWelcomeInformationStatusByConditionWithPage(GetAllWelcomeInformationParameter command)
        //{
        //    try
        //    {

        //        return this.Ok(await this.Mediator.Send(command));
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}
        //[HttpPost("CreateWelcomeInformation")]
        //public async Task<IActionResult> Post([FromForm] CreateWelcomeInformationCommand command)
        //{
        //    return this.Ok(await this.Mediator.Send(command));
        //}
        //[HttpPut("UpdateWelcomeInformationById")]
        //public async Task<IActionResult> UpdateWelcomeInformationById(UpdateWelcomeInformationCommand command)
        //{
        //    return this.Ok(await this.Mediator.Send(command));
        //}
    }
}