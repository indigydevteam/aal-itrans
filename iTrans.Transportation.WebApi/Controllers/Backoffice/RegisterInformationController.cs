﻿using iTrans.Transportation.Application.Features.RegisterInformations.Backoffice.Commands;
using iTrans.Transportation.Application.Features.RegisterInformations.Backoffice.Queries;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace iTrans.Transportation.WebApi.Controllers.Backoffice
{
    [ApiVersion("1.0")]
    public class RegisterInformationController : BaseApiController
    {
        [HttpPost("Backoffice/GetAllRegisterInformation")]
        public async Task<IActionResult> GetAllRegisterInformation(GetAllRegisterInformationParameter command)
        {
            try
            {

                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [HttpPost("Backoffice/GetRegisterInformationById")]
        public async Task<IActionResult> GetRegisterInformationById(GetRegisterInformationByIdQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        
        [HttpPost("Backoffice/CreateRegisterInformation")]
        public async Task<IActionResult> Post([FromForm] CreateRegisterInformationCommand command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }
        [HttpPut("Backoffice/UpdateRegisterInformationById")]
        public async Task<IActionResult> UpdateRegisterInformationById([FromForm] UpdateRegisterinformationCommand command)
        {
            command.UserId = UserIdentity;
            return this.Ok(await this.Mediator.Send(command));
        }
        //[HttpPost("Backoffice/GetAllRegisterInformationByConditionWithPage")]
        //public async Task<IActionResult> GetAllRegisterInformationByConditionWithPage(GetAllRegisterImformationParameter command)
        //{
        //    try
        //    {

        //        return this.Ok(await this.Mediator.Send(command));
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

    }
}