﻿using System;
using System.Threading.Tasks;
using iTrans.Transportation.Application.Features.CarSpecifications.Backoffice.Commands;
using iTrans.Transportation.Application.Features.CarSpecifications.Backoffice.Queries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace iTrans.Transportation.WebApi.Controllers.Backoffice
{
    [ApiVersion("1.0")]
    //[ApiController]
    //[Authorize]
    public class CarSpecificationController : BaseApiController
    {
        [HttpPost("Backoffice/GetAllCarSpecification")]
        public async Task<IActionResult> GetAllCarSpecification(GetAllCarSpecificationParameter command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("Backoffice/GetCarSpecificationById")]
        public async Task<IActionResult> GetCarSpecificationById(GetCarSpecificationByIdQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("Backoffice/CreateCarSpecification")]
        public async Task<IActionResult> Post([FromForm] CreateCarSpecificationCommand command)
        {
            command.UserId = UserIdentity;
            command.ContentDirectory = Configuration["ContentPath"];
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("Backoffice/UpdateCarSpecificationById")]
        public async Task<IActionResult> UpdateCarSpecificationById([FromForm] UpdateCarSpecificationCommand command)
        {
            command.UserId = UserIdentity;
            command.ContentDirectory = Configuration["ContentPath"];
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("Backoffice/DeleteCarSpecificationById")]
        public async Task<IActionResult> DeleteCarSpecificationById(DeleteCarSpecificationByIdCommand command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }
    }
}