﻿using System;
using System.Threading.Tasks;
using iTrans.Transportation.Application.Features.CarTypes.Backoffice.Commands;
using iTrans.Transportation.Application.Features.CarTypes.Backoffice.Queries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace iTrans.Transportation.WebApi.Controllers.Backoffice
{
    [ApiVersion("1.0")]
    [Authorize]
    public class CarTypeController : BaseApiController
    {


        //[HttpPost("GetCarTypeById")]
        //public async Task<IActionResult> GetCarTypeById(GetCarTypeByIdQuery command)
        //{
        //    return this.Ok(await this.Mediator.Send(command));
        //}

        [HttpPost("Backoffice/GetCarTypes")]
        public async Task<IActionResult> GetCarTypes(GetCarTypesQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("Backoffice/GetAllCarType")]
        public async Task<IActionResult> GetAllCarType(GetAllCarTypeParameter command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }
        [HttpPost("Backoffice/GetCarTypeById")]
        public async Task<IActionResult> GetCarTypeById(GetCarTypeByIdQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }
        [HttpPost("Backoffice/CreateCarType")]
        public async Task<IActionResult> Post(CreateCarTypeCommand command)

        {
            command.UserId = UserIdentity;
            command.ContentDirectory = Configuration["ContentPath"];
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPut("Backoffice/UpdateCarTypeById")]
        public async Task<IActionResult> UpdateCarTypeById(UpdateCarTypeCommand command)
        {
            command.UserId = UserIdentity;
            command.ContentDirectory = Configuration["ContentPath"];
            return this.Ok(await this.Mediator.Send(command));
        }
        [HttpPost("Backoffice/DeleteCarTypeById")]
        public async Task<IActionResult> DeleteCarTypeById(DeleteCarTypeByIdCommand command)
        {
            command.UserId = UserIdentity;
            return this.Ok(await this.Mediator.Send(command));
        }
    }
}
