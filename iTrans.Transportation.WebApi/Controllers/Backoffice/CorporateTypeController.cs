﻿using System;
using System.Threading.Tasks;
using iTrans.Transportation.Application.Features.CorporateTypes.Backoffice.Queries;
using iTrans.Transportation.Application.Features.CorporateTypes.Backoffice.Commands;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace iTrans.Transportation.WebApi.Controllers.Backoffice
{
    [ApiVersion("1.0")]
    [Authorize]
    public class CorporateTypeController : BaseApiController
    {
        [HttpPost("Backoffice/GetAllCorporateType")]
        public async Task<IActionResult> GetAllCorporateType(GetAllCorporateTypeParameter command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("Backoffice/GetAll")]
        public async Task<IActionResult> GetAll(GetAllCorporateTypeParameter command)
        {
            try
            {

                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost("Backoffice/GetCorporateTypeById")]
        public async Task<IActionResult> GetCorporateTypeById(GetCorporateTypeByIdQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("Backoffice/CreateCorporateType")]
        public async Task<IActionResult> Post(CreateCorporateTypeCommand command)
        {
            command.UserId = UserIdentity;
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPut("Backoffice/UpdateCorporateTypeById")]
        public async Task<IActionResult> UpdateCorporateTypeById(UpdateCorporateTypeCommand command)
        {
            command.UserId = UserIdentity;

            return this.Ok(await this.Mediator.Send(command));
        }

        
        [HttpPost("Backoffice/DeleteCorporateTypeById")]
        public async Task<IActionResult> DeleteCorporateTypeById(DeleteCorporateTypeByIdCommand command)
        {
            command.UserId = UserIdentity;
            return this.Ok(await this.Mediator.Send(command));
        }
    }
}
