﻿using System;
using System.Threading.Tasks;
using iTrans.Transportation.Application.Features.Titles.Backoffice.Commands;
using iTrans.Transportation.Application.Features.Titles.Backoffice.Queries;
using iTrans.Transportation.Application.Features.Titles.Queries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace iTrans.Transportation.WebApi.Controllers.Backoffice
{
    [ApiVersion("1.0")]
    [Authorize]
    public class TitleController : BaseApiController
    {
        //Backoffice
        [HttpPost("Backoffice/GetAll")]
        public async Task<IActionResult> GetAllTitleParameter(GetAllTitleParameter command)
        {
            try
            {

                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [HttpPost("Backoffice/GetById")]
        public async Task<IActionResult> GetById(BackofficeGetTitleByIdQuery command)
        {
            try
            {
                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost("Backoffice/Create")]
        public async Task<IActionResult> Post(CreateTitleCommand command)
        {
            if (UserIdentity != null)
            {
                command.UserId = UserIdentity.GetValueOrDefault();
            }
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPut("Backoffice/UpdateById")]
        public async Task<IActionResult> UpdateTitleById(UpdateTitleCommand command)
        {
            if (UserIdentity != null)
            {
                command.UserId = UserIdentity.GetValueOrDefault();
            }
            return this.Ok(await this.Mediator.Send(command));
        }
        [HttpPost("Backoffice/DeleteTitleById")]
        public async Task<IActionResult> DeleteTitleById(DeleteTitleByIdCommand command)
        {
            if (UserIdentity != null)
            {
                command.UserId = UserIdentity.GetValueOrDefault();
            }
            return this.Ok(await this.Mediator.Send(command));
        }
    }
}
