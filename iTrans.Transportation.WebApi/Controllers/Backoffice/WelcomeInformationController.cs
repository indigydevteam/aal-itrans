﻿using iTrans.Transportation.Application.Features.WelcomeInformations.Backoffice.Commands;
using iTrans.Transportation.Application.Features.WelcomeInformations.Backoffice.Queries;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace iTrans.Transportation.WebApi.Controllers.Backoffice
{
    [ApiVersion("1.0")]
    public class WelcomeInformationController : BaseApiController
    {
        [HttpPost("Backoffice/GetAllWelcomeInformation")]
        public async Task<IActionResult> GetAllWelcomeInformation(GetAllWelcomeInformationParameter command)
        {
            try
            {

                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        //[HttpPost("GetAllWelcomeInformationStatusByConditionWithPage")]
        //public async Task<IActionResult> GetAllWelcomeInformationStatusByConditionWithPage(GetAllWelcomeInformationParameter command)
        //{
        //    try
        //    {

        //        return this.Ok(await this.Mediator.Send(command));
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}
        [HttpPost("Backoffice/GetWelcomeInformationById")]
        public async Task<IActionResult> GetWelcomeInformationById(GetWelcomeInformationByIdQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }
        [HttpPost("Backoffice/CreateWelcomeInformation")]
        public async Task<IActionResult> Post([FromForm] CreateWelcomeInformationCommand command)
        {
            command.UserId = UserIdentity;
            return this.Ok(await this.Mediator.Send(command));
        }
        [HttpPut("Backoffice/UpdateWelcomeInformationById")]
        public async Task<IActionResult> UpdateWelcomeInformationById([FromForm] UpdateWelcomeInformationCommand command)
        {
            command.UserId = UserIdentity;
            return this.Ok(await this.Mediator.Send(command));
        }
        [HttpPost("Backoffice/DeleteWelcomeInformationById")]
        public async Task<IActionResult> DeleteTitleById(DeleteWelcomeInformationCommand command)
        {
            if (UserIdentity != null)
            {
                command.UserId = UserIdentity.GetValueOrDefault();
            }
            return this.Ok(await this.Mediator.Send(command));
        }
        //[HttpPost("Backoffice/GetWelcomeInformation")]
        //public async Task<IActionResult> GetWelcomeInformationByModule(GetWelcomeInformationByModuleQuery command)
        //{
        //    return this.Ok(await this.Mediator.Send(command));
        //}

        //[HttpPost("Backoffice/GetCustomerWelcomeInformation")]
        //public async Task<IActionResult> GetCustomerWelcomeInformation(GetCustomerWelcomeInformationQuery command)
        //{
        //    return this.Ok(await this.Mediator.Send(command));
        //}
        //[HttpPost("Backoffice/GetDriverWelcomeInformation")]
        //public async Task<IActionResult> GetDriverWelcomeInformation(GetDriverWelcomeInformationQuery command)
        //{
        //    return this.Ok(await this.Mediator.Send(command));
        //}
    }
}