﻿using System;
using System.Threading.Tasks;
using iTrans.Transportation.Application.Features.ProductTypes.Backoffice.Commands;
using iTrans.Transportation.Application.Features.ProductTypes.Backoffice.Queries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace iTrans.Transportation.WebApi.Controllers.Backoffice
{
    [ApiVersion("1.0")]
    [Authorize]
    public class ProductTypeController : BaseApiController
    {
        [HttpPost("Backoffice/GetAllProductType")]
        public async Task<IActionResult> GetAllProductType(GetAllProductTypeParameter command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        //[HttpPost("GetAllProductTypeByConditionWithPage")]
        //public async Task<IActionResult> GetAllProductTypeByConditionWithPage(GetAllProductTypeParameter command)
        //{
        //    try
        //    {

        //        return this.Ok(await this.Mediator.Send(command));
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        [HttpPost("Backoffice/GetProductTypeById")]
        public async Task<IActionResult> GetProductTypeById(GetProductTypeByIdQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }
        [HttpPost("Backoffice/CreateProductType")]
        public async Task<IActionResult> Post([FromForm] CreateProductTypeCommand command)
        {
            command.UserId = UserIdentity;
            command.ContentDirectory = Configuration["ContentPath"];

            return this.Ok(await this.Mediator.Send(command));
        }
        [HttpPost("Backoffice/UpdateProductTypeById")]
        public async Task<IActionResult> UpdateProductTypeById([FromForm] UpdateProductTypeCommand command)
        {
            command.UserId = UserIdentity;
            command.ContentDirectory = Configuration["ContentPath"];

            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("Backoffice/DeleteProductTypeById")]
        public async Task<IActionResult> DeleteProductTypeById(DeleteProductTypeByIdCommand command)
        {
            command.UserId = UserIdentity;
            return this.Ok(await this.Mediator.Send(command));
        }
    }
}
