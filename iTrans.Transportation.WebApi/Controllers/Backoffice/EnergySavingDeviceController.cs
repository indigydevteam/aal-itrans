﻿using System;
using System.Threading.Tasks;
using iTrans.Transportation.Application.Features.EnergySavingDevices.Backoffice.Commands;
using iTrans.Transportation.Application.Features.EnergySavingDevices.Backoffice.Queries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace iTrans.Transportation.WebApi.Controllers.Backoffice
{
    [ApiVersion("1.0")]
    [Authorize]
    public class EnergySavingDeviceController : BaseApiController
    {
        [HttpPost("Backoffice/GetAllEnergySavingDevice")]
        public async Task<IActionResult> GetAllEnergySavingDeviceByConditionWithPage(GetAllEnergySavingDeviceParameter command)
        {
            try
            {

                return this.Ok(await this.Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost("Backoffice/GetEnergySavingDeviceById")]
        public async Task<IActionResult> GetEnergySavingDeviceById(GetEnergySavingDeviceByIdQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }


        [HttpPost("Backoffice/CreateEnergySavingDevice")]
        public async Task<IActionResult> Post(CreateEnergySavingDeviceCommand command)
        {
            command.UserId = UserIdentity;

            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPut("Backoffice/UpdateEnergySavingDeviceById")]
        public async Task<IActionResult> UpdateEnergySavingDeviceById(UpdateEnergySavingDeviceCommand command)
        {
            command.UserId = UserIdentity;

            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("Backoffice/DeleteEnergySavingDeviceById")]
        public async Task<IActionResult> DeleteEnergySavingDevice(DeleteEnergySavingDeviceComand command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }
        //[HttpPost("GetAllEnergySavingDevice")]
        //public async Task<IActionResult> GetAllEnergySavingDevice(GetAllEnergySavingDevice command)
        //{
        //    try
        //    {

        //        return this.Ok(await this.Mediator.Send(command));
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        //[HttpPost("GetEnergySavingDeviceList")]
        //public async Task<IActionResult> GetList(GetEnergySavingDeviceListQuery command)
        //{
        //    try
        //    {

        //        return this.Ok(await this.Mediator.Send(command));
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}



        //[HttpPost("CreateEnergySavingDevice")]
        //public async Task<IActionResult> Post(CreateEnergySavingDeviceCommand command)
        //{
        //    command.UserId = UserIdentity;

        //    return this.Ok(await this.Mediator.Send(command));
        //}

        //[HttpPut("UpdateEnergySavingDeviceById")]
        //public async Task<IActionResult> UpdateEnergySavingDeviceById(UpdateEnergySavingDeviceCommand command)
        //{
        //    command.UserId = UserIdentity;

        //    return this.Ok(await this.Mediator.Send(command));
        //}
        //[HttpPost("Backoffice/GetAllEnergySavingDevice")]
        //public async Task<IActionResult> GetAllEnergySavingDevice(GetAllEnergySavingDevice command)
        //{
        //    return this.Ok(await this.Mediator.Send(command));
        //}
        //[HttpPost("Backoffice/DeleteEnergySavingDevice")]
        //public async Task<IActionResult> DeleteEnergySavingDevice(DeleteEnergySavingDeviceComand command)
        //{
        //    return this.Ok(await this.Mediator.Send(command));
        //}
    }
}
