﻿using System;
using System.Threading.Tasks;
using iTrans.Transportation.Application.Features.ProductPackagings.Backoffice.Commands;
using iTrans.Transportation.Application.Features.ProductPackagings.Backoffice.Queries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace iTrans.Transportation.WebApi.Controllers.Backoffice
{
    [ApiVersion("1.0")]
    [Authorize]
    public class ProductPackagingController : BaseApiController
    {
        [HttpPost("Backoffice/GetAllProductPackaging")]
        public async Task<IActionResult> DeleteProductPackagingById(GetAllProductPackagingParameter command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("Backoffice/GetProductPackagingById")]
        public async Task<IActionResult> GetProductPackagingById(GetProductPackagingByIdQuery command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("Backoffice/CreateProductPackaging")]
        public async Task<IActionResult> Post([FromForm] CreateProductPackagingCommand command)
        {
            command.UserId = UserIdentity;
            command.ContentDirectory = Configuration["ContentPath"];

            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("Backoffice/UpdateProductPackagingById")]
        public async Task<IActionResult> UpdateProductPackagingById([FromForm] UpdateProductPackagingCommand command)
        {
            command.UserId = UserIdentity;

            return this.Ok(await this.Mediator.Send(command));
        }

        [HttpPost("Backofiice/DeleteProductPackagingById")]
        public async Task<IActionResult> DeleteProductPackagingById(DeleteProductPackagingByIdCommand command)
        {
            return this.Ok(await this.Mediator.Send(command));
        }

    }
}
