﻿using iTrans.Transportation.Infrastructure.Shared;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Linq;
using System.Security.Claims;

namespace iTrans.Transportation.WebApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public abstract class BaseApiController : ControllerBase
    {
        private IMediator _mediator; 
        protected IMediator Mediator => _mediator ??= HttpContext.RequestServices.GetService<IMediator>();
         
        private IConfiguration _configuration; 
        protected IConfiguration Configuration => _configuration ??= HttpContext.RequestServices.GetService<IConfiguration>();
         
        public Guid? UserIdentity
        {
           
            get
            {
                try
                {
                    ClaimsIdentity idnt = HttpContext.User.Identity as ClaimsIdentity;

                    if (idnt.Claims.Any(x => x.Type == "sub"))
                    {
                        return new Guid(idnt.Claims.First(m => m.Type == "sub").Value);
                    }
                    else
                    {
                        return null;
                    }
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
        }

        public string UserRole
        {
            get
            {
                ClaimsIdentity idnt = HttpContext.User.Identity as ClaimsIdentity;

                if (idnt.Claims.Any(x => x.Type == "idp"))
                {
                    return idnt.Claims.First(m => m.Type == "idp").Value;
                }
                else
                {
                    return null;
                }
            }
        }

        public string UserType
        {
            get
            {
                ClaimsIdentity idnt = HttpContext.User.Identity as ClaimsIdentity;

                if (idnt.Claims.Any(x => x.Type == "role"))
                {
                    return idnt.Claims.First(m => m.Type == "role").Value;
                }
                else
                {
                    return null;
                }
            }
        }
    }
}
