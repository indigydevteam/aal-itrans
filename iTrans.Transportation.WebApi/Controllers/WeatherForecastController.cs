﻿using iTrans.Transportation.Application.Constant.Permission;
using iTrans.Transportation.WebApi.Attribute;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace iTrans.Transportation.WebApi.Controllers
{
    //[ApiController]
    //[Route("[controller]")]
    public class WeatherForecastController : BaseApiController
    {

        private readonly ILogger<WeatherForecastController> _logger;

        public WeatherForecastController(ILogger<WeatherForecastController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        [AuthorizePolicy(Policy = MenuPermission.dashboardAndReportCreate)]
        public IActionResult Get()
        {
            return Ok();

        }
    }
}
