﻿namespace iTrans.Transportation.WebApi.Extensions
{
    using iTrans.Transportation.Application.Wrappers;
    using iTrans.Transportation.Domain;
    using Microsoft.AspNetCore.Authentication.JwtBearer;
    using Microsoft.AspNetCore.Http;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.IdentityModel.Tokens;
    using Newtonsoft.Json;
    using System;
    using System.IdentityModel.Tokens.Jwt;
    using System.Text;

    public static class JwtTokenConfig
    {
        public static IServiceCollection AddJwtTokenAuthentication(this IServiceCollection services, IConfiguration configuration)
        {
            JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear();

            services.Configure<JwtConfig>(configuration.GetSection("JWTSettings"));
            services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer("Bearer",
               options =>
               {
                   options.Authority = "https://itrans-test-auth.humantix.cloud";
                   options.Audience = "profileapi";
                   options.RequireHttpsMetadata = false;
                   options.TokenValidationParameters = new TokenValidationParameters()
                   {
                       NameClaimType = "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier"//To set Identity.Name 
                   };
                   options.Events = new JwtBearerEvents()
                   {
                       OnAuthenticationFailed = c =>
                       {
                           c.NoResult();
                           c.Response.StatusCode = 500;
                           c.Response.ContentType = "text/plain";
                           return c.Response.WriteAsync(c.Exception.ToString());
                       },
                       OnChallenge = context =>
                       {
                           if (!context.Response.HasStarted)
                           {
                               context.Response.StatusCode = 401;
                               context.Response.ContentType = "application/json";
                               context.HandleResponse();
                               var result = JsonConvert.SerializeObject(new Response<string>("You are not Authorized"));
                               return context.Response.WriteAsync(result);
                           }
                           else
                           {
                               var result = JsonConvert.SerializeObject(new Response<string>("Token Expired"));
                               return context.Response.WriteAsync(result);
                           }
                       },
                       OnForbidden = context =>
                       {
                           context.Response.StatusCode = 403;
                           context.Response.ContentType = "application/json";
                           var result = JsonConvert.SerializeObject(new Response<string>("You are not authorized to access this resource"));
                           return context.Response.WriteAsync(result);
                       },
                   };
               });
            return services;
        }
    }
}