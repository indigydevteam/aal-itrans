﻿using iTrans.Transportation.Application.DTOs.Authentication;
using iTrans.Transportation.Application.Interfaces.Repositories;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;

namespace iTrans.Transportation.WebApi.Attribute
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class AuthorizePolicyAttribute : ActionFilterAttribute
    {
        public string Policy { get; set; }

        private IMenuRepositoryAsync _menuRepository;
        private IRoleMenuRepositoryAsync _roleMenuRepository;
        private IRoleRepositoryAsync _roleRepository;
        private IMenuPermissionRepositoryAsync _menuPermissionRepository;

        public override void OnActionExecuting(ActionExecutingContext context)
        {

            base.OnActionExecuting(context);

            requestService(context);

            string roleName = string.Empty;

            ClaimsIdentity idnt = context.HttpContext.User.Identity as ClaimsIdentity;

            if (idnt.Claims.Any(x => x.Type == "idp"))
            {
                roleName = idnt.Claims.First(m => m.Type == "idp").Value;
            }
            else
            {
                context.HttpContext.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
                context.Result = new JsonResult(HttpStatusCode.Unauthorized.ToString());
            }

            // Condition
            var exitPolicy = (from r  in _roleRepository.GetSession().GetAwaiter().GetResult()
                              join rm in _roleMenuRepository.GetSession().GetAwaiter().GetResult() on r.Id equals rm.RoleId
                              join m  in _menuRepository.GetSession().GetAwaiter().GetResult() on rm.Id equals m.Id
                              join mp in _menuPermissionRepository.GetSession().GetAwaiter().GetResult() on m.Id equals mp.MenuId
                              where r.NameEN.ToLower().Equals(roleName.ToLower()) && mp.Permission.Equals(Policy)
                              select mp).Any();

            if (!exitPolicy)
            {
                context.HttpContext.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
                context.Result = new JsonResult(HttpStatusCode.Unauthorized.ToString());
            }

        }

        private void requestService(ActionExecutingContext context)
        {
            _menuRepository = context.HttpContext.RequestServices.GetService<IMenuRepositoryAsync>();
            _roleMenuRepository = context.HttpContext.RequestServices.GetService<IRoleMenuRepositoryAsync>();
            _roleRepository = context.HttpContext.RequestServices.GetService<IRoleRepositoryAsync>();
            _menuPermissionRepository = context.HttpContext.RequestServices.GetService<IMenuPermissionRepositoryAsync>();
        }

    }
}
