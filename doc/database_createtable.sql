USE [iTrans]
GO
/****** Object:  Table [dbo].[Announcement]    Script Date: 9/29/2021 10:12:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Announcement](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Title_TH] [nvarchar](max) NOT NULL,
	[Detail_TH] [nvarchar](max) NOT NULL,
	[Title_ENG] [nvarchar](max) NOT NULL,
	[Detail_ENG] [nvarchar](max) NOT NULL,
	[Active] [bit] NOT NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[Created] [datetime2](7) NOT NULL,
	[ModifiedBy] [nvarchar](50) NOT NULL,
	[Modified] [datetime2](7) NOT NULL,
 CONSTRAINT [PK_Announcement] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Car_Description]    Script Date: 9/29/2021 10:12:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Car_Description](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CarListId] [int] NOT NULL,
	[Name_TH] [nvarchar](max) NOT NULL,
	[Name_ENG] [nvarchar](max) NOT NULL,
	[Active] [bit] NOT NULL,
	[Sequence] [int] NOT NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[Created] [datetime2](7) NULL,
	[ModifiedBy] [nvarchar](50) NULL,
	[Modified] [datetime2](7) NULL,
 CONSTRAINT [PK_Car_Description] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Car_Feature]    Script Date: 9/29/2021 10:12:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Car_Feature](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CarListId] [int] NOT NULL,
	[Name_TH] [nvarchar](max) NOT NULL,
	[Name_ENG] [nvarchar](max) NOT NULL,
	[Active] [bit] NOT NULL,
	[Sequence] [int] NOT NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[Created] [datetime2](7) NULL,
	[ModifiedBy] [nvarchar](50) NULL,
	[Modified] [datetime2](7) NULL,
 CONSTRAINT [PK_Car_Feature] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Car_List]    Script Date: 9/29/2021 10:12:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Car_List](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CarTypeId] [int] NOT NULL,
	[Name_TH] [nvarchar](max) NOT NULL,
	[Name_ENG] [nvarchar](max) NOT NULL,
	[Sequence] [int] NOT NULL,
	[Active] [bit] NOT NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[Created] [datetime2](7) NULL,
	[ModifiedBy] [nvarchar](50) NULL,
	[Modified] [datetime2](7) NULL,
 CONSTRAINT [PK_Car_SubType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Car_Specification]    Script Date: 9/29/2021 10:12:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Car_Specification](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CarListId] [int] NOT NULL,
	[Name_TH] [nvarchar](max) NOT NULL,
	[Name_ENG] [nvarchar](max) NOT NULL,
	[Active] [bit] NOT NULL,
	[Sequence] [int] NOT NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[Created] [datetime2](7) NULL,
	[ModifiedBy] [nvarchar](50) NULL,
	[Modified] [datetime2](7) NULL,
 CONSTRAINT [PK_Car_Specification] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Car_Type]    Script Date: 9/29/2021 10:12:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Car_Type](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name_TH] [nvarchar](max) NOT NULL,
	[Name_ENG] [nvarchar](max) NOT NULL,
	[Sequence] [int] NOT NULL,
	[Active] [bit] NOT NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[Created] [datetime2](7) NULL,
	[ModifiedBy] [nvarchar](50) NULL,
	[Modified] [datetime2](7) NULL,
 CONSTRAINT [PK_Car_Type] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Corporate_Type]    Script Date: 9/29/2021 10:12:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Corporate_Type](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name_TH] [nvarchar](max) NOT NULL,
	[Name_ENG] [nvarchar](max) NOT NULL,
	[Sequence] [int] NOT NULL,
	[Active] [bit] NOT NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[Created] [datetime2](7) NULL,
	[ModifiedBy] [nvarchar](50) NULL,
	[Modified] [datetime2](7) NULL,
 CONSTRAINT [PK_Corporate_Type] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Country]    Script Date: 9/29/2021 10:12:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Country](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name_TH] [nvarchar](max) NOT NULL,
	[Name_ENG] [nvarchar](max) NOT NULL,
	[PhoneCode] [nvarchar](25) NULL,
	[Active] [bit] NOT NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[Created] [datetime2](7) NULL,
	[ModifiedBy] [nvarchar](50) NULL,
	[Modified] [datetime2](7) NULL,
 CONSTRAINT [PK_Country] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Customer]    Script Date: 9/29/2021 10:12:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Customer](
	[Id] [uniqueidentifier] NOT NULL,
	[Password] [nvarchar](50) NOT NULL,
	[CustomerType] [nvarchar](50) NOT NULL,
	[RoleId] [int] NOT NULL,
	[CorporateTypeId] [int] NULL,
	[Title] [int] NULL,
	[FirstName] [nvarchar](50) NULL,
	[MiddleName] [nvarchar](50) NULL,
	[LastName] [nvarchar](50) NULL,
	[Name] [nvarchar](250) NOT NULL,
	[IdentityNumber] [nvarchar](50) NOT NULL,
	[ContactPersonTitle] [int] NULL,
	[ContactPersonFirstName] [nvarchar](50) NULL,
	[ContactPersonMiddleName] [nvarchar](50) NULL,
	[ContactPersonLastName] [nvarchar](50) NULL,
	[Birthday] [datetime2](7) NULL,
	[PhoneCode] [nvarchar](50) NULL,
	[PhoneNumber] [nvarchar](50) NOT NULL,
	[Email] [nvarchar](50) NOT NULL,
	[Facbook] [nvarchar](250) NULL,
	[Line] [nvarchar](250) NULL,
	[Twitter] [nvarchar](250) NULL,
	[Whatapp] [nvarchar](250) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[Created] [datetime2](7) NULL,
	[ModifiedBy] [nvarchar](50) NULL,
	[Modified] [datetime2](7) NULL,
 CONSTRAINT [PK_Customer_1] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Customer_Address]    Script Date: 9/29/2021 10:12:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Customer_Address](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CustomerId] [uniqueidentifier] NOT NULL,
	[Country] [int] NOT NULL,
	[Province] [int] NOT NULL,
	[District] [int] NOT NULL,
	[Subdistrict] [int] NOT NULL,
	[PostCode] [nvarchar](50) NOT NULL,
	[Road] [nvarchar](250) NULL,
	[Alley] [nvarchar](250) NULL,
	[Address] [nvarchar](250) NOT NULL,
	[Branch] [nvarchar](50) NULL,
	[AddressType] [nvarchar](50) NOT NULL,
	[AddressName] [nvarchar](250) NULL,
	[ContactPerson] [nvarchar](250) NULL,
	[ContactPhoneNumber] [nvarchar](50) NULL,
	[ContactEmail] [nvarchar](250) NULL,
	[Maps] [nvarchar](250) NULL,
	[Sequence] [int] NOT NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[Created] [datetime2](7) NULL,
	[ModifiedBy] [nvarchar](50) NULL,
	[Modified] [datetime2](7) NULL,
 CONSTRAINT [PK_Customer_Address] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Customer_ComplainAndRecommendat]    Script Date: 9/29/2021 10:12:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Customer_ComplainAndRecommendat](
	[Id] [int] NOT NULL,
	[CustomerId] [int] NULL,
	[Title] [nvarchar](50) NULL,
	[Detail] [nvarchar](max) NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[Created] [datetime2](7) NOT NULL,
	[ModifiedBy] [nvarchar](50) NOT NULL,
	[Modified] [datetime2](7) NOT NULL,
 CONSTRAINT [PK_Customer_ComplainAndRecommendat] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Customer_FavoriteCar]    Script Date: 9/29/2021 10:12:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Customer_FavoriteCar](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CustomerId] [int] NOT NULL,
	[DriverCarId] [int] NOT NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[Created] [datetime2](7) NOT NULL,
	[ModifiedBy] [nvarchar](50) NOT NULL,
	[Modified] [datetime2](7) NOT NULL,
 CONSTRAINT [PK_Customer_FavoriteCar] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Customer_File]    Script Date: 9/29/2021 10:12:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Customer_File](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CustomerId] [uniqueidentifier] NOT NULL,
	[FileName] [nvarchar](max) NOT NULL,
	[ContentType] [nvarchar](max) NOT NULL,
	[FilePath] [nvarchar](max) NOT NULL,
	[FileEXT] [nvarchar](max) NOT NULL,
	[DocumentType] [nvarchar](50) NULL,
	[Sequence] [int] NOT NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[Created] [datetime2](7) NULL,
	[ModifiedBy] [nvarchar](50) NULL,
	[Modified] [datetime2](7) NULL,
 CONSTRAINT [PK_Customer_File] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Customer_Notification]    Script Date: 9/29/2021 10:12:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Customer_Notification](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CustomerId] [int] NULL,
	[Message] [nvarchar](max) NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[Created] [datetime2](7) NOT NULL,
	[ModifiedBy] [nvarchar](50) NOT NULL,
	[Modified] [datetime2](7) NOT NULL,
 CONSTRAINT [PK_Customer_Notification] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Customer_Payment]    Script Date: 9/29/2021 10:12:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Customer_Payment](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CustomerId] [int] NOT NULL,
	[Channel] [nvarchar](50) NOT NULL,
	[Description] [nvarchar](max) NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[Created] [datetime2](7) NOT NULL,
	[ModifiedBy] [nvarchar](50) NOT NULL,
	[Modified] [datetime2](7) NOT NULL,
 CONSTRAINT [PK_Customer_Payment] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Customer_PaymentHistory]    Script Date: 9/29/2021 10:12:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Customer_PaymentHistory](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CustomerId] [int] NOT NULL,
	[Detail] [nvarchar](50) NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[Created] [datetime2](7) NOT NULL,
	[ModifiedBy] [nvarchar](50) NOT NULL,
	[Modified] [datetime2](7) NOT NULL,
 CONSTRAINT [PK_Customer_PaymentHistory] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Customer_PaymentTransaction]    Script Date: 9/29/2021 10:12:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Customer_PaymentTransaction](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Customer_PaymentHistoryId] [int] NOT NULL,
	[Detail] [nvarchar](max) NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[Created] [datetime2](7) NOT NULL,
	[ModifiedBy] [nvarchar](50) NOT NULL,
	[Modified] [datetime2](7) NOT NULL,
 CONSTRAINT [PK_Customer_PaymentTransaction] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Customer_Product]    Script Date: 9/29/2021 10:12:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Customer_Product](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CustomerId] [uniqueidentifier] NOT NULL,
	[Name] [nvarchar](250) NOT NULL,
	[ProductType] [nvarchar](250) NOT NULL,
	[ProductTypeDetail] [nvarchar](500) NOT NULL,
	[Packaging] [nvarchar](250) NOT NULL,
	[PackagingDetail] [nvarchar](500) NOT NULL,
	[Width] [int] NOT NULL,
	[Length] [int] NOT NULL,
	[Height] [int] NOT NULL,
	[Weight] [int] NOT NULL,
	[Quantity] [int] NOT NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[Created] [datetime2](7) NULL,
	[ModifiedBy] [nvarchar](50) NULL,
	[Modified] [datetime2](7) NULL,
 CONSTRAINT [PK_Customer_Product] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Customer_Product_File]    Script Date: 9/29/2021 10:12:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Customer_Product_File](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CustomerProductId] [int] NOT NULL,
	[FileName] [nchar](10) NOT NULL,
	[ContentType] [nchar](10) NOT NULL,
	[FilePath] [nchar](10) NOT NULL,
	[FileEXT] [nchar](10) NOT NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[Created] [datetime2](7) NOT NULL,
	[ModifiedBy] [nvarchar](50) NOT NULL,
	[Modified] [datetime2](7) NOT NULL,
 CONSTRAINT [PK_Customer_Product_File] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Customer_TermAndCondition]    Script Date: 9/29/2021 10:12:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Customer_TermAndCondition](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CustomerId] [int] NOT NULL,
	[TermAndCondition] [nvarchar](max) NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[Created] [datetime2](7) NOT NULL,
	[ModifiedBy] [nvarchar](50) NOT NULL,
	[Modified] [datetime2](7) NOT NULL,
 CONSTRAINT [PK_Customer_TermAndCondition] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[District]    Script Date: 9/29/2021 10:12:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[District](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ProvinceId] [int] NOT NULL,
	[Name_TH] [nvarchar](max) NOT NULL,
	[Name_ENG] [nvarchar](max) NOT NULL,
	[Active] [bit] NOT NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[Created] [datetime2](7) NULL,
	[ModifiedBy] [nvarchar](50) NULL,
	[Modified] [datetime2](7) NULL,
 CONSTRAINT [PK_District] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Driver]    Script Date: 9/29/2021 10:12:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Driver](
	[Id] [uniqueidentifier] NOT NULL,
	[Password] [nvarchar](max) NOT NULL,
	[DriverType] [nvarchar](50) NOT NULL,
	[RoleId] [int] NULL,
	[CorporateTypeId] [int] NULL,
	[Title] [int] NULL,
	[FirstName] [nvarchar](50) NULL,
	[MiddleName] [nvarchar](50) NULL,
	[LastName] [nvarchar](50) NULL,
	[Name] [nvarchar](250) NOT NULL,
	[IdentityNumber] [nvarchar](50) NOT NULL,
	[ContactPersonTitle] [nvarchar](50) NULL,
	[ContactPersonFirstName] [nvarchar](50) NULL,
	[ContactPersonMiddleName] [nvarchar](50) NULL,
	[ContactPersonLastName] [nvarchar](50) NULL,
	[Birthday] [datetime2](7) NULL,
	[PhoneCode] [nvarchar](50) NULL,
	[PhoneNumber] [nvarchar](50) NOT NULL,
	[Email] [nvarchar](50) NOT NULL,
	[Facbook] [nvarchar](250) NULL,
	[Line] [nvarchar](250) NULL,
	[Twitter] [nvarchar](250) NULL,
	[Whatapp] [nvarchar](250) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[Created] [datetime2](7) NULL,
	[ModifiedBy] [nvarchar](50) NULL,
	[Modified] [datetime2](7) NULL,
 CONSTRAINT [PK_Driver_1] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Driver_Address]    Script Date: 9/29/2021 10:12:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Driver_Address](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[DriverId] [uniqueidentifier] NOT NULL,
	[Country] [int] NOT NULL,
	[Province] [int] NOT NULL,
	[District] [int] NOT NULL,
	[Subdistrict] [int] NOT NULL,
	[PostCode] [nvarchar](50) NOT NULL,
	[Road] [nvarchar](250) NULL,
	[Alley] [nvarchar](250) NULL,
	[Address] [nvarchar](250) NOT NULL,
	[Branch] [nvarchar](50) NULL,
	[AddressType] [nvarchar](50) NOT NULL,
	[AddressName] [nvarchar](250) NULL,
	[ContactPerson] [nvarchar](250) NULL,
	[ContactPhoneNumber] [nvarchar](50) NULL,
	[ContactEmail] [nvarchar](250) NULL,
	[Maps] [nvarchar](250) NULL,
	[Sequence] [int] NOT NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[Created] [datetime2](7) NULL,
	[ModifiedBy] [nvarchar](50) NULL,
	[Modified] [datetime2](7) NULL,
 CONSTRAINT [PK_Driver_Address] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Driver_Announcement]    Script Date: 9/29/2021 10:12:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Driver_Announcement](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[DriverId] [int] NOT NULL,
	[AcceptProduct] [nvarchar](250) NULL,
	[AcceptProductDetail] [nvarchar](max) NULL,
	[AcceptPackaging] [nvarchar](250) NULL,
	[AcceptPackagingDetail] [nvarchar](max) NULL,
	[RejectProduct] [nvarchar](250) NULL,
	[RejectProductDetail] [nvarchar](max) NULL,
	[RejectPackaging] [nvarchar](250) NULL,
	[RejectPackagingDetail] [nvarchar](max) NULL,
	[SendDate] [datetime2](7) NULL,
	[ReceiveDate] [datetime2](7) NULL,
	[CarRegistration] [nvarchar](50) NULL,
	[AllRent] [bit] NULL,
	[Additional] [bit] NULL,
	[AdditionalDetail] [nvarchar](max) NULL,
	[Note] [nvarchar](max) NULL,
	[OrderId] [nchar](10) NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[Created] [datetime2](7) NOT NULL,
	[ModifiedBy] [nvarchar](50) NOT NULL,
	[Modified] [datetime2](7) NOT NULL,
 CONSTRAINT [PK_Driver_Announcement] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Driver_AnnouncementLocation]    Script Date: 9/29/2021 10:12:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Driver_AnnouncementLocation](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[DriverAnnouncementId] [int] NOT NULL,
	[Country] [nchar](10) NOT NULL,
	[Province] [nchar](10) NOT NULL,
	[District] [nchar](10) NOT NULL,
	[LocationType] [nchar](10) NOT NULL,
	[Note] [nchar](10) NULL,
	[Order] [nchar](10) NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[Created] [datetime2](7) NOT NULL,
	[ModifiedBy] [nvarchar](50) NOT NULL,
	[Modified] [datetime2](7) NOT NULL,
 CONSTRAINT [PK_Driver_AnnouncementLocation] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Driver_Car]    Script Date: 9/29/2021 10:12:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Driver_Car](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[DriverId] [uniqueidentifier] NOT NULL,
	[CarRegistration] [nvarchar](50) NOT NULL,
	[ActExpiry] [datetime2](7) NOT NULL,
	[CarTypeId] [int] NULL,
	[CarListId] [int] NULL,
	[CarDescriptionId] [int] NULL,
	[CarDescriptionDetail] [nvarchar](max) NULL,
	[CarFeatureId] [int] NULL,
	[CarFeatureDetail] [nvarchar](max) NULL,
	[Width] [int] NULL,
	[Length] [int] NULL,
	[Height] [int] NULL,
	[DriverName] [nvarchar](max) NULL,
	[DriverPhoneCode] [nvarchar](50) NULL,
	[DriverPhoneNumber] [nvarchar](50) NULL,
	[ProductInsurance] [bit] NULL,
	[ProductInsuranceAmount] [decimal](18, 2) NULL,
	[AllLocation] [bit] NULL,
	[Note] [nvarchar](max) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[Created] [datetime2](7) NULL,
	[ModifiedBy] [nvarchar](50) NULL,
	[Modified] [datetime2](7) NULL,
 CONSTRAINT [PK_Driver_Car] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Driver_CarFile]    Script Date: 9/29/2021 10:12:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Driver_CarFile](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[DriverCarId] [int] NOT NULL,
	[FileName] [nvarchar](max) NOT NULL,
	[ContentType] [nvarchar](max) NOT NULL,
	[FilePath] [nvarchar](max) NOT NULL,
	[FileEXT] [nvarchar](max) NOT NULL,
	[DocumentType] [nvarchar](50) NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[Created] [datetime2](7) NOT NULL,
	[ModifiedBy] [nvarchar](50) NOT NULL,
	[Modified] [datetime2](7) NOT NULL,
 CONSTRAINT [PK_Driver_CarFile] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Driver_CarLocation]    Script Date: 9/29/2021 10:12:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Driver_CarLocation](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[DriverCarId] [int] NOT NULL,
	[Country] [int] NOT NULL,
	[Region] [int] NOT NULL,
	[Province] [int] NOT NULL,
	[District] [int] NOT NULL,
	[LocationType] [nvarchar](250) NOT NULL,
	[Sequence] [int] NOT NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[Created] [datetime2](7) NULL,
	[ModifiedBy] [nvarchar](50) NULL,
	[Modified] [datetime2](7) NULL,
 CONSTRAINT [PK_Driver_CarLocation] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Driver_ComplainAndRecommendatio]    Script Date: 9/29/2021 10:12:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Driver_ComplainAndRecommendatio](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[DriverId] [int] NULL,
	[Title] [nvarchar](250) NULL,
	[Detail] [nvarchar](max) NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[Created] [datetime2](7) NOT NULL,
	[ModifiedBy] [nvarchar](50) NOT NULL,
	[Modified] [datetime2](7) NOT NULL,
 CONSTRAINT [PK_Driver_ComplainAndRecommendatio] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Driver_File]    Script Date: 9/29/2021 10:12:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Driver_File](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CustomerId] [int] NOT NULL,
	[FileName] [nvarchar](max) NOT NULL,
	[ContentType] [nvarchar](max) NOT NULL,
	[FilePath] [nvarchar](max) NOT NULL,
	[FileEXT] [nvarchar](max) NOT NULL,
	[DocumentType] [nvarchar](50) NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[Created] [datetime2](7) NOT NULL,
	[ModifiedBy] [nvarchar](50) NOT NULL,
	[Modified] [datetime2](7) NOT NULL,
 CONSTRAINT [PK_Driver_File] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Driver_Insurance]    Script Date: 9/29/2021 10:12:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Driver_Insurance](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[DriverId] [int] NOT NULL,
	[Product] [nvarchar](max) NOT NULL,
	[ProductType] [nvarchar](max) NOT NULL,
	[ProductTypeDetail] [nvarchar](max) NOT NULL,
	[WarrantyLimit] [nvarchar](max) NOT NULL,
	[WarrantyPeriod] [nvarchar](max) NOT NULL,
	[TotalAmount] [decimal](18, 2) NOT NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[Created] [datetime2](7) NOT NULL,
	[ModifiedBy] [nvarchar](50) NOT NULL,
	[Modified] [datetime2](7) NOT NULL,
 CONSTRAINT [PK_Driver_Insurance] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Driver_Notification]    Script Date: 9/29/2021 10:12:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Driver_Notification](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[DriverId] [int] NOT NULL,
	[Message] [nvarchar](max) NOT NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[Created] [datetime2](7) NOT NULL,
	[ModifiedBy] [nvarchar](50) NOT NULL,
	[Modified] [datetime2](7) NOT NULL,
 CONSTRAINT [PK_Driver_Notification] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Driver_Payment]    Script Date: 9/29/2021 10:12:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Driver_Payment](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[DriverId] [int] NOT NULL,
	[Channel] [nvarchar](50) NOT NULL,
	[Description] [nvarchar](max) NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[Created] [datetime2](7) NOT NULL,
	[ModifiedBy] [nvarchar](50) NOT NULL,
	[Modified] [datetime2](7) NOT NULL,
 CONSTRAINT [PK_Driver_Payment] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Driver_PaymentHistory]    Script Date: 9/29/2021 10:12:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Driver_PaymentHistory](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[DriverId] [int] NOT NULL,
	[Detail] [nvarchar](50) NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[Created] [datetime2](7) NOT NULL,
	[ModifiedBy] [nvarchar](50) NOT NULL,
	[Modified] [datetime2](7) NOT NULL,
 CONSTRAINT [PK_Driver_PaymentHistory] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Driver_PaymentTransaction]    Script Date: 9/29/2021 10:12:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Driver_PaymentTransaction](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Driver_PaymentHistoryId] [int] NOT NULL,
	[Detail] [nvarchar](max) NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[Created] [datetime2](7) NOT NULL,
	[ModifiedBy] [nvarchar](50) NOT NULL,
	[Modified] [datetime2](7) NOT NULL,
 CONSTRAINT [PK_Driver_PaymentTransaction] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[FAQ]    Script Date: 9/29/2021 10:12:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FAQ](
	[Id] [int] NOT NULL,
	[Title_TH] [nvarchar](max) NOT NULL,
	[Detail_TH] [nvarchar](max) NOT NULL,
	[Title_ENG] [nvarchar](max) NOT NULL,
	[Detail_ENG] [nvarchar](max) NOT NULL,
	[Active] [bit] NOT NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[Created] [datetime2](7) NOT NULL,
	[ModifiedBy] [nvarchar](50) NOT NULL,
	[Modified] [datetime2](7) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Ordering]    Script Date: 9/29/2021 10:12:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Ordering](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[DriverId] [int] NOT NULL,
	[CustomerId] [int] NOT NULL,
	[ProductCBM] [int] NOT NULL,
	[ProductTotalWeight] [float] NOT NULL,
	[OrderingPrice] [decimal](18, 2) NOT NULL,
	[OrderingStatus] [nvarchar](50) NOT NULL,
	[AdditionalDetail] [nvarchar](max) NULL,
	[Note] [nvarchar](max) NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[Created] [datetime2](7) NOT NULL,
	[ModifiedBy] [nvarchar](50) NOT NULL,
	[Modified] [datetime2](7) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Ordering_Address]    Script Date: 9/29/2021 10:12:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Ordering_Address](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[OrderingId] [int] NOT NULL,
	[Country] [nvarchar](250) NOT NULL,
	[Province] [nvarchar](250) NOT NULL,
	[District] [nvarchar](250) NOT NULL,
	[Subdistrict] [nvarchar](250) NOT NULL,
	[PostCode] [nvarchar](250) NOT NULL,
	[Road] [nvarchar](500) NOT NULL,
	[Alley] [nvarchar](500) NOT NULL,
	[Address] [nvarchar](500) NOT NULL,
	[Branch] [nvarchar](250) NOT NULL,
	[AddressType] [nvarchar](250) NOT NULL,
	[AddressName] [nvarchar](250) NOT NULL,
	[Date] [datetime2](7) NOT NULL,
	[Maps] [nvarchar](50) NOT NULL,
	[Order] [int] NOT NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[Created] [datetime2](7) NOT NULL,
	[ModifiedBy] [nvarchar](50) NOT NULL,
	[Modified] [datetime2](7) NOT NULL,
 CONSTRAINT [PK_Ordering_Address] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Ordering_Car]    Script Date: 9/29/2021 10:12:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Ordering_Car](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[OrderingId] [int] NOT NULL,
	[CarRegistration] [nvarchar](250) NOT NULL,
	[ActExpiry] [nvarchar](250) NOT NULL,
	[CarType] [nvarchar](250) NOT NULL,
	[CarSubType] [nvarchar](250) NULL,
	[CarDescription] [nvarchar](250) NULL,
	[CarDescriptionDetail] [nvarchar](max) NULL,
	[CarFeature] [nvarchar](250) NULL,
	[CarFeatureDetail] [nvarchar](max) NULL,
	[Width] [int] NULL,
	[Length] [int] NULL,
	[Height] [int] NULL,
	[DriverName] [nvarchar](max) NULL,
	[DriverPhoneCode] [nvarchar](50) NULL,
	[DriverPhoneNumber] [nvarchar](50) NULL,
	[ProductInsurance] [bit] NULL,
	[ProductInsuranceAmount] [decimal](18, 2) NULL,
	[Note] [nvarchar](max) NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[Created] [datetime2](7) NOT NULL,
	[ModifiedBy] [nvarchar](50) NOT NULL,
	[Modified] [datetime2](7) NOT NULL,
 CONSTRAINT [PK_Ordering_Car] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Ordering_Container]    Script Date: 9/29/2021 10:12:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Ordering_Container](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[OrderingId] [int] NOT NULL,
	[Shipper] [nvarchar](250) NOT NULL,
	[Booking] [nvarchar](250) NOT NULL,
	[Contact] [nvarchar](250) NOT NULL,
	[Valume] [nvarchar](250) NOT NULL,
	[PhoneCode] [nvarchar](50) NOT NULL,
	[PhoneNumber] [nvarchar](50) NOT NULL,
	[Fax] [nvarchar](50) NOT NULL,
	[Email] [nvarchar](250) NOT NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[Created] [datetime2](7) NOT NULL,
	[ModifiedBy] [nvarchar](50) NOT NULL,
	[Modified] [datetime2](7) NOT NULL,
 CONSTRAINT [PK_Ordering_Container] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Ordering_ContainerAddress]    Script Date: 9/29/2021 10:12:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Ordering_ContainerAddress](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[OrderingContainerId] [int] NOT NULL,
	[Country] [nvarchar](250) NOT NULL,
	[Province] [nvarchar](250) NOT NULL,
	[District] [nvarchar](250) NOT NULL,
	[Subdistrict] [nvarchar](250) NOT NULL,
	[PostCode] [nvarchar](50) NOT NULL,
	[Road] [nvarchar](250) NULL,
	[Alley] [nvarchar](250) NULL,
	[Address] [nvarchar](250) NOT NULL,
	[Branch] [nvarchar](50) NULL,
	[AddressType] [nvarchar](50) NOT NULL,
	[AddressName] [nvarchar](250) NULL,
	[Date] [datetime2](7) NULL,
	[Maps] [nvarchar](250) NULL,
	[Note] [nvarchar](max) NULL,
	[FeederVessel] [nvarchar](max) NULL,
	[TStime] [datetime2](7) NULL,
	[MotherVessel] [nvarchar](max) NULL,
	[CPSDate] [datetime2](7) NULL,
	[CPSDetail] [nvarchar](max) NULL,
	[CYDate] [datetime2](7) NULL,
	[ReturnDate] [datetime2](7) NULL,
	[ReturnDetail] [nvarchar](max) NULL,
	[ClosingTime] [datetime2](7) NULL,
	[CutOffVGMTime] [datetime2](7) NULL,
	[CutOffSITime] [datetime2](7) NULL,
	[Agent] [nvarchar](max) NULL,
	[Commodity] [nvarchar](max) NULL,
	[Term] [nvarchar](max) NULL,
	[Remark] [nvarchar](max) NULL,
	[Import] [bit] NULL,
	[Export] [bit] NULL,
	[Order] [int] NOT NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[Created] [datetime2](7) NOT NULL,
	[ModifiedBy] [nvarchar](50) NOT NULL,
	[Modified] [datetime2](7) NOT NULL,
 CONSTRAINT [PK_Ordering_ContainerAddress] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Ordering_Driver]    Script Date: 9/29/2021 10:12:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Ordering_Driver](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[OrderingCarId] [int] NOT NULL,
	[DriverType] [nvarchar](250) NOT NULL,
	[CorporateType] [nvarchar](250) NOT NULL,
	[Title] [nvarchar](250) NOT NULL,
	[FirstName] [nvarchar](250) NOT NULL,
	[MiddleName] [nvarchar](250) NOT NULL,
	[LastName] [nvarchar](250) NOT NULL,
	[Name] [nvarchar](500) NOT NULL,
	[IdentityNumber] [nvarchar](250) NOT NULL,
	[ContactPersonTitle] [nvarchar](250) NOT NULL,
	[ContactPersonFirstName] [nvarchar](250) NOT NULL,
	[ContactPersonMiddleName] [nvarchar](250) NOT NULL,
	[ContactPersonLastName] [nvarchar](250) NOT NULL,
	[PhoneCode] [nvarchar](50) NOT NULL,
	[PhoneNumber] [nvarchar](50) NOT NULL,
	[Email] [nvarchar](250) NOT NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[Created] [datetime2](7) NOT NULL,
	[ModifiedBy] [nvarchar](50) NOT NULL,
	[Modified] [datetime2](7) NOT NULL,
 CONSTRAINT [PK_Ordering_Driver] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Ordering_File]    Script Date: 9/29/2021 10:12:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Ordering_File](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[OrderingId] [int] NOT NULL,
	[FileName] [nvarchar](max) NOT NULL,
	[ContentType] [nvarchar](max) NOT NULL,
	[FilePath] [nvarchar](max) NOT NULL,
	[FileEXT] [nvarchar](max) NOT NULL,
	[DocumentType] [nvarchar](50) NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[Created] [datetime2](7) NOT NULL,
	[ModifiedBy] [nvarchar](50) NOT NULL,
	[Modified] [datetime2](7) NOT NULL,
 CONSTRAINT [PK_Ordering_File] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Ordering_History]    Script Date: 9/29/2021 10:12:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Ordering_History](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[DriverId] [int] NOT NULL,
	[CustomerId] [int] NOT NULL,
	[ProductCBM] [int] NOT NULL,
	[ProductTotalWeight] [float] NOT NULL,
	[OrderingPrice] [decimal](18, 2) NOT NULL,
	[OrderingStatus] [nvarchar](50) NOT NULL,
	[AdditionalDetail] [nvarchar](max) NULL,
	[Note] [nvarchar](max) NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[Created] [datetime2](7) NOT NULL,
	[ModifiedBy] [nvarchar](50) NOT NULL,
	[Modified] [datetime2](7) NOT NULL,
 CONSTRAINT [PK_Ordering_History] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Ordering_Insurance]    Script Date: 9/29/2021 10:12:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Ordering_Insurance](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[OrderingId] [int] NULL,
	[ProductType] [nvarchar](250) NULL,
	[WarrantyPeriod] [nvarchar](250) NULL,
	[InsurancePremium] [decimal](18, 2) NULL,
	[InsuranceLimit] [decimal](18, 2) NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[Created] [datetime2](7) NOT NULL,
	[ModifiedBy] [nvarchar](50) NOT NULL,
	[Modified] [datetime2](7) NOT NULL,
 CONSTRAINT [PK_Ordering_Insurance] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Ordering_Person]    Script Date: 9/29/2021 10:12:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Ordering_Person](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[OrderingAddressId] [int] NULL,
	[Name] [nvarchar](250) NULL,
	[PhoneCode] [nvarchar](50) NULL,
	[PhoneNumber] [nvarchar](50) NULL,
	[Email] [nvarchar](250) NULL,
	[Type] [nvarchar](250) NULL,
	[OrderingProductId] [int] NULL,
	[Order] [int] NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[Created] [datetime2](7) NOT NULL,
	[ModifiedBy] [nvarchar](50) NOT NULL,
	[Modified] [datetime2](7) NOT NULL,
 CONSTRAINT [PK_Ordering_Person] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Ordering_Product]    Script Date: 9/29/2021 10:12:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Ordering_Product](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[OrderingId] [int] NOT NULL,
	[Name] [nvarchar](250) NOT NULL,
	[ProductType] [nvarchar](250) NOT NULL,
	[ProductTypeDetail] [nvarchar](max) NOT NULL,
	[Packaging] [nvarchar](250) NOT NULL,
	[PackagingDetail] [nvarchar](max) NOT NULL,
	[Width] [int] NOT NULL,
	[Length] [int] NOT NULL,
	[Height] [int] NOT NULL,
	[Weight] [int] NOT NULL,
	[Quantity] [int] NOT NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[Created] [datetime2](7) NOT NULL,
	[ModifiedBy] [nvarchar](50) NOT NULL,
	[Modified] [datetime2](7) NOT NULL,
 CONSTRAINT [PK_Ordering_Product] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Product_Packaging]    Script Date: 9/29/2021 10:12:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Product_Packaging](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name_TH] [nvarchar](max) NOT NULL,
	[Name_ENG] [nvarchar](max) NOT NULL,
	[Active] [bit] NOT NULL,
	[Sequence] [int] NOT NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[Created] [datetime2](7) NULL,
	[ModifiedBy] [nvarchar](50) NULL,
	[Modified] [datetime2](7) NULL,
 CONSTRAINT [PK_Product_Packaging] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Product_Type]    Script Date: 9/29/2021 10:12:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Product_Type](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name_TH] [nvarchar](max) NOT NULL,
	[Name_ENG] [nvarchar](max) NOT NULL,
	[Active] [bit] NOT NULL,
	[Sequence] [int] NOT NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[Created] [datetime2](7) NULL,
	[ModifiedBy] [nvarchar](50) NULL,
	[Modified] [datetime2](7) NULL,
 CONSTRAINT [PK_Product_Type] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Province]    Script Date: 9/29/2021 10:12:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Province](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CountryId] [int] NOT NULL,
	[Name_TH] [nvarchar](max) NOT NULL,
	[Name_ENG] [nvarchar](max) NOT NULL,
	[Active] [bit] NOT NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[Created] [datetime2](7) NULL,
	[ModifiedBy] [nvarchar](50) NULL,
	[Modified] [datetime2](7) NULL,
 CONSTRAINT [PK_Province] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Region]    Script Date: 9/29/2021 10:12:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Region](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CountryId] [int] NOT NULL,
	[Name_TH] [nvarchar](max) NOT NULL,
	[Name_ENG] [nvarchar](max) NOT NULL,
	[Active] [bit] NOT NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[Created] [datetime2](7) NOT NULL,
	[ModifiedBy] [nvarchar](50) NOT NULL,
	[Modified] [datetime2](7) NOT NULL,
 CONSTRAINT [PK_Region] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Register_Document]    Script Date: 9/29/2021 10:12:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Register_Document](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Module] [nvarchar](50) NOT NULL,
	[Information_TH] [nvarchar](max) NOT NULL,
	[Information_ENG] [nvarchar](max) NOT NULL,
	[Active] [bit] NOT NULL,
	[Order] [int] NOT NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[Created] [datetime2](7) NOT NULL,
	[ModifiedBy] [nvarchar](50) NOT NULL,
	[Modified] [datetime2](7) NOT NULL,
 CONSTRAINT [PK_Register_Document] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Register_Information]    Script Date: 9/29/2021 10:12:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Register_Information](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Module] [nvarchar](50) NOT NULL,
	[Information_TH] [nvarchar](max) NOT NULL,
	[Information_ENG] [nvarchar](max) NOT NULL,
	[Active] [bit] NULL,
	[Order] [int] NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[Created] [datetime2](7) NOT NULL,
	[ModifiedBy] [nvarchar](50) NOT NULL,
	[Modified] [datetime2](7) NOT NULL,
 CONSTRAINT [PK_Register_Information] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Role]    Script Date: 9/29/2021 10:12:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Role](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Module] [nvarchar](50) NULL,
	[Level] [int] NOT NULL,
	[Name_TH] [nvarchar](50) NOT NULL,
	[Name_ENG] [nvarchar](50) NOT NULL,
	[Active] [bit] NOT NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[Created] [datetime2](7) NOT NULL,
	[ModifiedBy] [nvarchar](50) NOT NULL,
	[Modified] [datetime2](7) NOT NULL,
 CONSTRAINT [PK_Customer_Role] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[RoleCondition]    Script Date: 9/29/2021 10:12:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RoleCondition](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CustomerRoleId] [int] NOT NULL,
	[Characteristics_TH] [nvarchar](max) NULL,
	[Characteristics_ENG] [nvarchar](max) NOT NULL,
	[Order] [int] NOT NULL,
	[Active] [bit] NOT NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[Created] [datetime2](7) NOT NULL,
	[ModifiedBy] [nvarchar](50) NOT NULL,
	[Modified] [datetime2](7) NOT NULL,
 CONSTRAINT [PK_Customer_RoleCondition] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Subdistrict]    Script Date: 9/29/2021 10:12:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Subdistrict](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[DistrictId] [int] NOT NULL,
	[Name_TH] [nvarchar](max) NOT NULL,
	[Name_ENG] [nvarchar](max) NOT NULL,
	[PostCode] [nvarchar](25) NULL,
	[Active] [bit] NOT NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[Created] [datetime2](7) NULL,
	[ModifiedBy] [nvarchar](50) NULL,
	[Modified] [datetime2](7) NULL,
 CONSTRAINT [PK_Subdistrict] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TermAndCondition]    Script Date: 9/29/2021 10:12:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TermAndCondition](
	[Id] [int] NOT NULL,
	[Module] [nvarchar](50) NULL,
	[Name_TH] [nvarchar](50) NOT NULL,
	[Description_TH] [nvarchar](max) NULL,
	[Name_ENG] [nvarchar](50) NOT NULL,
	[Description_ENG] [nvarchar](max) NULL,
	[Order] [int] NOT NULL,
	[Active] [bit] NOT NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[Created] [datetime2](7) NOT NULL,
	[ModifiedBy] [nvarchar](50) NOT NULL,
	[Modified] [datetime2](7) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Title]    Script Date: 9/29/2021 10:12:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Title](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name_TH] [nvarchar](50) NOT NULL,
	[Name_ENG] [nvarchar](50) NOT NULL,
	[Sequence] [int] NOT NULL,
	[Active] [bit] NOT NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[Created] [datetime2](7) NULL,
	[ModifiedBy] [nvarchar](50) NULL,
	[Modified] [datetime2](7) NULL,
 CONSTRAINT [PK_Title] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserPermission]    Script Date: 9/29/2021 10:12:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserPermission](
	[Code] [nvarchar](250) NOT NULL,
	[NameTH] [nvarchar](250) NOT NULL,
	[NameEN] [nvarchar](250) NOT NULL,
 CONSTRAINT [PK_UserPermission] PRIMARY KEY CLUSTERED 
(
	[Code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserRole]    Script Date: 9/29/2021 10:12:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserRole](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[NameTH] [nvarchar](250) NOT NULL,
	[NameEN] [nvarchar](250) NOT NULL,
	[Menu] [nvarchar](1000) NULL,
	[Created] [datetime] NOT NULL,
	[CreatedByEN] [nvarchar](250) NULL,
	[CreatedByTH] [nvarchar](250) NULL,
	[CreatedByID] [nvarchar](50) NULL,
	[Modified] [datetime] NOT NULL,
	[ModifiedByEN] [nvarchar](250) NULL,
	[ModifiedByTH] [nvarchar](250) NULL,
	[ModifiedByID] [nvarchar](50) NULL,
 CONSTRAINT [PK_UserRole] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserRoleUserPermission]    Script Date: 9/29/2021 10:12:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserRoleUserPermission](
	[RoleID] [int] NOT NULL,
	[PermissionCode] [nvarchar](250) NOT NULL,
 CONSTRAINT [PK_UserRoleUserPermission] PRIMARY KEY CLUSTERED 
(
	[RoleID] ASC,
	[PermissionCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Users]    Script Date: 9/29/2021 10:12:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Users](
	[EmplID] [nvarchar](50) NOT NULL,
	[Email] [nvarchar](250) NULL,
	[Password] [nvarchar](250) NULL,
	[UserToken] [nvarchar](max) NULL,
	[IsNaverExpire] [bit] NOT NULL,
	[IsExternalUser] [bit] NULL,
	[EffectiveDate] [datetime] NULL,
	[ExpiredDate] [datetime] NULL,
	[IsActive] [bit] NOT NULL,
	[Activated] [datetime] NULL,
	[Reactivated] [datetime] NULL,
	[IncorrectLoginCount] [int] NOT NULL,
	[FirstNameTH] [nvarchar](250) NULL,
	[FirstNameEN] [nvarchar](250) NULL,
	[LastNameTH] [nvarchar](250) NULL,
	[LastNameEN] [nvarchar](250) NULL,
	[Created] [datetime] NOT NULL,
	[CreatedByEN] [nvarchar](250) NULL,
	[CreatedByTH] [nvarchar](250) NULL,
	[CreatedByID] [nvarchar](50) NULL,
	[Modified] [datetime] NOT NULL,
	[ModifiedByEN] [nvarchar](250) NULL,
	[ModifiedByTH] [nvarchar](250) NULL,
	[ModifiedByID] [nvarchar](50) NULL,
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[EmplID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserUserRole]    Script Date: 9/29/2021 10:12:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserUserRole](
	[EmplID] [nvarchar](50) NOT NULL,
	[RoleID] [int] NOT NULL,
 CONSTRAINT [PK_UserUserRole] PRIMARY KEY CLUSTERED 
(
	[EmplID] ASC,
	[RoleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Welcome_Information]    Script Date: 9/29/2021 10:12:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Welcome_Information](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Module] [nvarchar](50) NOT NULL,
	[Information_TH] [nvarchar](max) NOT NULL,
	[Information_ENG] [nvarchar](max) NOT NULL,
	[Active] [bit] NOT NULL,
	[Order] [int] NOT NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[Created] [datetime2](7) NOT NULL,
	[ModifiedBy] [nvarchar](50) NOT NULL,
	[Modified] [datetime2](7) NOT NULL,
 CONSTRAINT [PK_Welcome_Information] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
USE [master]
GO
ALTER DATABASE [iTrans] SET  READ_WRITE 
GO
