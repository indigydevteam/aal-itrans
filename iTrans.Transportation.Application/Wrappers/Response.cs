﻿using System.Collections.Generic;
using iTrans.Transportation.Application.Exceptions;

namespace iTrans.Transportation.Application.Wrappers
{
    public class Response<T>
    {
        public Response()
        {
        }
        public Response(T data, string message = null)
        {
            Succeeded = true;
            Message = message;
            Data = data;
        }

        public Response(string message)
        {
            Succeeded = false;
            Message = message;
        }
        public Response(T data, string message, string message_en)
        {
            Succeeded = true;
            Message = message;
            Message_EN = message_en;
            Data = data;
        }

        public Response(string message, string message_en)
        {
            Succeeded = false;
            Message_EN = message_en;
            Message = message;
        }

        public bool Succeeded { get; set; }
        public string Message { get; set; }
        public string Message_EN { get; set; }
        public List<ErrorModel> Errors { get; set; }
        public T Data { get; set; }
    }
}
