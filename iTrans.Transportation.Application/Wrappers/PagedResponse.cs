﻿namespace iTrans.Transportation.Application.Wrappers
{
    public class PagedResponse<T> : Response<T>
    {
        public int itemCount { get; set; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public PagedResponse()
        {
            this.PageNumber = 1;
            this.PageSize = 10;
        }

        public PagedResponse(T data, int pageNumber, int pageSize)
        {
            this.PageNumber = pageNumber < 1 ? 1 : pageNumber;
            this.PageSize = pageSize > 10 ? 10 : pageSize;
            this.Data = data;
            this.Message = null;
            this.Succeeded = true;
            this.Errors = null;
        }
        public PagedResponse(T data, int pageNumber, int pageSize,int itemCount)
        {
            this.itemCount = itemCount;
            this.PageNumber = pageNumber < 1 ? 1 : pageNumber;
            this.PageSize = pageSize < 1 ? itemCount : pageSize;
            this.Data = data;
            this.Message = null;
            this.Succeeded = true;
            this.Errors = null;
        }
    }
}
