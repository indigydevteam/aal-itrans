﻿using iTrans.Transportation.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Application.Interfaces.Repositories
{
    public interface IOrderingPaymentRepositoryAsync : IGenericRepositoryAsync<OrderingPayment>
    {

    }
}
