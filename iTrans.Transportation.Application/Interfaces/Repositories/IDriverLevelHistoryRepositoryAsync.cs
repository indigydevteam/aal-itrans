﻿using iTrans.Transportation.Domain;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Interfaces.Repositories
{
    public interface IDriverLevelHistoryRepositoryAsync : IGenericRepositoryAsync<DriverLevelHistory>
    {

    }
}
