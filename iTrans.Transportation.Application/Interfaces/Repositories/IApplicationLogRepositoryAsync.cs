﻿using iTrans.Transportation.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Application.Interfaces.Repositories
{
    public interface IApplicationLogRepositoryAsync : IGenericRepositoryAsync<ApplicationLog>
    {

    }
}
