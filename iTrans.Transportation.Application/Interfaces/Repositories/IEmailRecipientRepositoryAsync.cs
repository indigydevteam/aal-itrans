﻿using iTrans.Transportation.Domain.Entities;

namespace iTrans.Transportation.Application.Interfaces.Repositories
{
    public interface IEmailRecipientRepositoryAsync : IGenericRepositoryAsync<EmailRecipient>
    {

    }
}
