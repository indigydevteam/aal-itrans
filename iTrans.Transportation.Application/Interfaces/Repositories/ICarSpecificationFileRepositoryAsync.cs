﻿using iTrans.Transportation.Domain;

namespace iTrans.Transportation.Application.Interfaces.Repositories
{
    public interface ICarSpecificationFileRepositoryAsync : IGenericRepositoryAsync<CarSpecificationFile>
    {
    }
}
