﻿using iTrans.Transportation.Domain;

namespace iTrans.Transportation.Application.Interfaces.Repositories
{
    public interface IDriverProblemFileRepositoryAsync : IGenericRepositoryAsync<DriverProblemFile>
    {
    }
}
