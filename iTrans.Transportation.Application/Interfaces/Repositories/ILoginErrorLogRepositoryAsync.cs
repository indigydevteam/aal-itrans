﻿using iTrans.Transportation.Domain;
using System.Threading.Tasks;


namespace iTrans.Transportation.Application.Interfaces.Repositories
{
    public interface ILoginErrorLogRepositoryAsync : IGenericRepositoryAsync<LoginErrorLog>
    {

    }
}
