﻿using iTrans.Transportation.Domain;

namespace iTrans.Transportation.Application.Interfaces.Repositories
{
    public interface ICarTypeFileRepositoryAsync : IGenericRepositoryAsync<CarTypeFile>
    {
    }
}
