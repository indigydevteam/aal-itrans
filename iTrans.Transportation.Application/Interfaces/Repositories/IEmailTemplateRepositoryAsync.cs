﻿using iTrans.Transportation.Domain.Entities;

namespace iTrans.Transportation.Application.Interfaces.Repositories
{
    public interface IEmailTemplateRepositoryAsync : IGenericRepositoryAsync<EmailTemplate>
    {

    }
}
