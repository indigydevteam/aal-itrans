﻿using NHibernate;
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;


namespace iTrans.Transportation.Application.Interfaces.Repositories
{
    public interface IGenericRepositoryAsync<T> where T : class
    {
        Task<IQueryable<T>> GetAllAsync();

        Task<IQueryable<T>> FindByCondition(Expression<Func<T, bool>> expression);
        Task<IQueryable<T>> FindByCondition(Expression<Func<T, bool>> expression, Expression<Func<T, DateTime>> order, bool isDescending);
        Task<IQueryable<T>> FindByCondition(Expression<Func<T, bool>> expression, Expression<Func<T, decimal>> order, bool isDescending);

        Task<IQueryable<T>> GetPagedResponseAsync(int pageNumber, int pageSize);

        Task<IQueryable<T>> FindByConditionWithPage(Expression<Func<T, bool>> expression, int pageNumber, int pageSize);
        Task<IQueryable<T>> FindByConditionWithPage(Expression<Func<T, bool>> expression, Expression<Func<T, DateTime>> order, bool isDescending, int pageNumber, int pageSize);
        Task<IQueryable<T>> FindByConditionWithPage(Expression<Func<T, bool>> expression, Expression<Func<T, decimal>> order, bool isDescending, int pageNumber, int pageSize);
        Task<IQueryable<T>> FindByConditionWithPage(Expression<Func<T, bool>> expression, Expression<Func<T, string>> order, bool isDescending, int pageNumber, int pageSize);
        Task<IQueryable<T>> FindByConditionWithItemNumber(Expression<Func<T, bool>> expression, Expression<Func<T, decimal>> order, bool isDescending, int itemStart, int itemCount);
        Task<T> AddAsync(T entity);

        Task<T> UpdateAsync(T entity);

        Task<T> DeleteAsync(T entity);

        Task <IQueryable<T>> GetSession(); 

        Task<IQuery> CreateSQLQuery(string sql);
        Task<IQuery> CreateStoreProc(string spName);
        int GetItemCount(Expression<Func<T, bool>> expression);
    }
}
