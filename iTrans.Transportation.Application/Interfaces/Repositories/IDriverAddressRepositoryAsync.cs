﻿using iTrans.Transportation.Domain;

namespace iTrans.Transportation.Application.Interfaces.Repositories
{
    public interface IDriverAddressRepositoryAsync : IGenericRepositoryAsync<DriverAddress>
    {
    }
}
