﻿using iTrans.Transportation.Domain;
using System;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Interfaces.Repositories
{
    public interface ICustomerActivityRepositoryAsync : IGenericRepositoryAsync<CustomerActivity>
    {
    }
}
