﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.Config;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.Configs.Queries
{
    public class GetTimespanUpdateQuery : IRequest<Response<int>>
    {
         
    }
    public class GetTimespanUpdateQueryHandler : IRequestHandler<GetTimespanUpdateQuery, Response<int>>
    {
        private readonly IConfigRepositoryAsync _configRepository;
        private readonly IMapper _mapper;
        public GetTimespanUpdateQueryHandler(IConfigRepositoryAsync configRepository, IMapper mapper)
        {
            _configRepository = configRepository;
            _mapper = mapper;
        }
        public async Task<Response<int>> Handle(GetTimespanUpdateQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var config = (await _configRepository.FindByCondition(x => x.Key.Equals("timespan-update")).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (config == null)
                {
                    return new Response<int>(_mapper.Map<int>(10));
                }
                int result = 0;
                if (!int.TryParse(config.Value, out result))
                {
                    result = 10;
                }
                return new Response<int>(_mapper.Map<int>(result));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
