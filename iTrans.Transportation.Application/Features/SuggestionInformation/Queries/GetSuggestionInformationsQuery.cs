﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.SuggestionInformation;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.SuggestionInformations.Queries
{
    public class GetSuggestionInformationsQuery : IRequest<Response<IEnumerable<SuggestionInformationViewModel>>>
    {

    }
    public class GetSuggestionInformationsQueryHandler : IRequestHandler<GetSuggestionInformationsQuery, Response<IEnumerable<SuggestionInformationViewModel>>>
    {
        private readonly ISuggestionInformationRepositoryAsync _suggestionInformationRepository;
        private readonly IMapper _mapper;
        public GetSuggestionInformationsQueryHandler(ISuggestionInformationRepositoryAsync suggestionInformationRepository, IMapper mapper)
        {
            _suggestionInformationRepository = suggestionInformationRepository;
            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<SuggestionInformationViewModel>>> Handle(GetSuggestionInformationsQuery request, CancellationToken cancellationToken)
        {
            try
            {

                var suggestionInformation = await _suggestionInformationRepository.GetAllAsync();
                var suggestionInformationViewModel = _mapper.Map<IEnumerable<SuggestionInformationViewModel>>(suggestionInformation);
                return new Response<IEnumerable<SuggestionInformationViewModel>>(suggestionInformationViewModel);
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
    }
}
