﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.ContainerType;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.ContainerTypes.Queries
{
    public class GetContainerTypeByIdQuery : IRequest<Response<ContainerTypeViewModel>>
    {
        public int Id { get; set; }
    }
    public class GetContainerTypeByIdQueryHandler : IRequestHandler<GetContainerTypeByIdQuery, Response<ContainerTypeViewModel>>
    {
        private readonly IContainerTypeRepositoryAsync _containerTypeRepository;
        private readonly IMapper _mapper;
        public GetContainerTypeByIdQueryHandler(IContainerTypeRepositoryAsync containerTypeRepository, IMapper mapper)
        {
            _containerTypeRepository = containerTypeRepository;
            _mapper = mapper;
        }
        public async Task<Response<ContainerTypeViewModel>> Handle(GetContainerTypeByIdQuery request, CancellationToken cancellationToken)
        {
            var ContainerTypeObject = (await _containerTypeRepository.FindByCondition(x => x.Id.Equals(request.Id)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            return new Response<ContainerTypeViewModel>(_mapper.Map<ContainerTypeViewModel>(ContainerTypeObject));
        }
    }
}
