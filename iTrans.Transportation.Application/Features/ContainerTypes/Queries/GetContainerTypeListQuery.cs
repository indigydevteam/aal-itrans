﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.ContainerType;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.ContainerTypes.Queries
{
    public class GetContainerTypeListQuery : IRequest<Response<IEnumerable<ContainerTypeViewModel>>>
    {

    }
    public class GetContainerTypeListQueryHandler : IRequestHandler<GetContainerTypeListQuery, Response<IEnumerable<ContainerTypeViewModel>>>
    {
        private readonly IContainerTypeRepositoryAsync _containerTypeRepository;
        private readonly IMapper _mapper;
        public GetContainerTypeListQueryHandler(IContainerTypeRepositoryAsync containerTypeRepository, IMapper mapper)
        {
            _containerTypeRepository = containerTypeRepository;
            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<ContainerTypeViewModel>>> Handle(GetContainerTypeListQuery request, CancellationToken cancellationToken)
        {
            var ContainerType = (await _containerTypeRepository.FindByCondition(x => x.Active == true).ConfigureAwait(false)).AsQueryable().ToList();
            var ContainerTypeViewModel = _mapper.Map<IEnumerable<ContainerTypeViewModel>>(ContainerType);
            return new Response<IEnumerable<ContainerTypeViewModel>>(ContainerTypeViewModel);
        }
    }
}
