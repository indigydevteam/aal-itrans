﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.ContainerType;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;

namespace iTrans.Transportation.Application.Features.ContainerTypes.Queries
{
    public class GetAllContainerTypeParameter : IRequest<PagedResponse<IEnumerable<ContainerTypeViewModel>>>
    {
        public string Search { set; get; }

        public int PageNumber { get; set; }
        public int PageSize { get; set; }
    }
    public class GetAllContainerTypeParameterHandler : IRequestHandler<GetAllContainerTypeParameter, PagedResponse<IEnumerable<ContainerTypeViewModel>>>
    {
        private readonly IContainerTypeRepositoryAsync _containerTypeRepository;
        private readonly IMapper _mapper;
        private Expression<Func<ContainerType, bool>> expression;

        public GetAllContainerTypeParameterHandler(IContainerTypeRepositoryAsync containerTypeRepository, IMapper mapper)
        {
            _containerTypeRepository = containerTypeRepository;
            _mapper = mapper;
        }

        public async Task<PagedResponse<IEnumerable<ContainerTypeViewModel>>> Handle(GetAllContainerTypeParameter request, CancellationToken cancellationToken)
        {
            var validFilter = _mapper.Map<GetAllContainerTypeParameter>(request);
            if (validFilter.Search != null && validFilter.Search.Trim() != "")
            {
                expression = x => (x.Name_ENG.Contains(validFilter.Search.Trim()) || x.Name_TH.Contains(validFilter.Search.Trim()) || x.Sequence.ToString().Contains(validFilter.Search.Trim()));
            }
            int itemCount = _containerTypeRepository.GetItemCount(expression);
            //Expression<Func<ContainerType, bool>> expression = x => x.Name_TH != "";
            var ContainerType = await _containerTypeRepository.FindByConditionWithPage(expression, validFilter.PageNumber, validFilter.PageSize);
            var ContainerTypeViewModel = _mapper.Map<IEnumerable<ContainerTypeViewModel>>(ContainerType);
            return new PagedResponse<IEnumerable<ContainerTypeViewModel>>(ContainerTypeViewModel, validFilter.PageNumber, validFilter.PageSize, itemCount);
        }
    }
}
