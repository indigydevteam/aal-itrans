﻿using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.ContainerTypes.Commands
{
    public class DeleteContainerTypeByIdCommand : IRequest<Response<int>>
    {
        public int Id { get; set; }
        public class DeleteContainerTypeByIdCommandHandler : IRequestHandler<DeleteContainerTypeByIdCommand, Response<int>>
        {
            private readonly IContainerTypeRepositoryAsync _containerTypeRepository;
            public DeleteContainerTypeByIdCommandHandler(IContainerTypeRepositoryAsync ContainerTypeRepository)
            {
                _containerTypeRepository = ContainerTypeRepository;
            }
            public async Task<Response<int>> Handle(DeleteContainerTypeByIdCommand command, CancellationToken cancellationToken)
            {
                var ContainerType = (await _containerTypeRepository.FindByCondition(x => x.Id == command.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (ContainerType == null)
                {
                    throw new ApiException($"ContainerType Not Found.");
                }
                else
                {
                    await _containerTypeRepository.DeleteAsync(ContainerType);
                    return new Response<int>(ContainerType.Id);
                }
            }
        }
    }
}
