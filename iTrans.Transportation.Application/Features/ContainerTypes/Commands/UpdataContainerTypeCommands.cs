﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;

namespace iTrans.Transportation.Application.Features.ContainerTypes.Commands
{
    public class UpdateContainerTypeCommand : IRequest<Response<int>>
    {
        public Guid? UserId { get; set; }
        public int Id { get; set; }
        public string Name_TH { get; set; }
        public string Name_ENG { get; set; }
        public int Sequence { get; set; }
        public bool Active { get; set; }
        public bool Specified { get; set; }

    }

    public class UpdateContainerTypeCommandHandler : IRequestHandler<UpdateContainerTypeCommand, Response<int>>
    {
        private readonly IContainerTypeRepositoryAsync _containerTypeRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;

        public UpdateContainerTypeCommandHandler(IContainerTypeRepositoryAsync containerTypeRepository, IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _containerTypeRepository = containerTypeRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(UpdateContainerTypeCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var ContainerType = (await _containerTypeRepository.FindByCondition(x => x.Id == request.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (ContainerType == null)
                {
                    throw new ApiException($"ContainerType Not Found.");
                }
                else
                {
                    ContainerType.Name_TH = request.Name_TH;
                    ContainerType.Name_ENG = request.Name_ENG;
                    ContainerType.Sequence = request.Sequence;
                    ContainerType.Active = request.Active;
                    ContainerType.Specified = request.Specified;
                    ContainerType.Modified = DateTime.UtcNow;

                    await _containerTypeRepository.UpdateAsync(ContainerType);
                    var log = new CreateAppLog(_applicationLogRepository);
                    log.CreateLog("ContainerType", "ContainerType", "Edit", Newtonsoft.Json.JsonConvert.SerializeObject(ContainerType),request.UserId.ToString());
                    return new Response<int>(ContainerType.Id);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
