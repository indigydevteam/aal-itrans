﻿using AutoMapper;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Domain;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;

namespace iTrans.Transportation.Application.Features.ContainerTypes.Commands
{
    public partial class CreateContainerTypeCommand : IRequest<Response<int>>
    {
        public Guid? UserId { get; set; }
        public string Name_TH { get; set; }
        public string Name_ENG { get; set; }
        public int Sequence { get; set; }
        public bool Active { get; set; }
        public bool Specified { get; set; }

    }
    public class CreateContainerTypeCommandHandler : IRequestHandler<CreateContainerTypeCommand, Response<int>>
    {
        private readonly IContainerTypeRepositoryAsync _containerTypeRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;
        public CreateContainerTypeCommandHandler(IContainerTypeRepositoryAsync containerTypeRepository, IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _containerTypeRepository = containerTypeRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(CreateContainerTypeCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var ContainerType = _mapper.Map<ContainerType>(request);
                ContainerType.Created = DateTime.UtcNow;
                ContainerType.Modified = DateTime.UtcNow;
                ContainerType.CreatedBy = request.UserId.ToString();
                //title.ModifiedBy = "xxxxxxx";
                var ContainerTypeObject = await _containerTypeRepository.AddAsync(ContainerType);
                var log = new CreateAppLog(_applicationLogRepository);
                log.CreateLog("ContainerType", "ContainerType", "Create", Newtonsoft.Json.JsonConvert.SerializeObject(ContainerType),request.UserId.ToString());
                return new Response<int>(ContainerTypeObject.Id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
