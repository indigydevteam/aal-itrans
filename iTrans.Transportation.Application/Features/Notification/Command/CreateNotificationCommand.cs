﻿using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Interfaces.Services;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using iTrans.Transportation.Domain.Entities;
using MediatR;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;


namespace iTrans.Transportation.Application.Features.Notification.Command
{
    public partial class CreateNotificationCommand : IRequest<Response<int>>
    {
        public Guid? IdUser { get; set; }
        public string Title { get; set; }
        public string Detail { get; set; }
        public Guid? UserId { get; set; }
        public Guid? OwnerId { get; set; }
        public string ServiceCode { get; set; }
        public string Payload { get; set; }
        public string ReferenceContentKey { get; set; }
        public bool IsListing { get; set; }
         
        public string TargetCustomers { get; set; }
        public string TargetDrivers { get; set; }
        public bool IsAll { get; set; }
    }
    public class CreateNotificationCommandHandler : IRequestHandler<CreateNotificationCommand, Response<int>>
    {
        private readonly INotificationRepositoryAsync _notificationRepository;
        private readonly INotificationUserRepositoryAsync _notificationUserRepository;
        private readonly ICustomerRepositoryAsync _customerRepository;
        private readonly IDriverRepositoryAsync _driverRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;
        private readonly IConfiguration _configuration;
        public CreateNotificationCommandHandler(INotificationRepositoryAsync notificationRepository,
                INotificationUserRepositoryAsync notificationUserRepository,
                ICustomerRepositoryAsync customerRepository,
                IDriverRepositoryAsync driverRepository,
                IApplicationLogRepositoryAsync applicationLogRepository,
                IMapper mapper,
                IConfiguration configuration)
        {
            _notificationRepository = notificationRepository;
            _notificationUserRepository = notificationUserRepository;
            _customerRepository = customerRepository;
            _driverRepository = driverRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
            _configuration = configuration;
        }

        public async Task<Response<int>> Handle(CreateNotificationCommand request, CancellationToken cancellationToken)
        { 
            try
            {
                var notification = _mapper.Map<Domain.Notification>(request);

                //NotificationManager notiMgrCustomers = new NotificationManager(_configuration["OneSignal:ApplicationID"], _configuration["OneSignal:RestApiKey"], _configuration["OneSignal:AuthenticationID"]);
                //NotificationManager notiMgrDrivers = new NotificationManager(_configuration["DriverOneSignal:ApplicationID"], _configuration["DriverOneSignal:RestApiKey"], _configuration["DriverOneSignal:AuthenticationID"]);

                if (request.IsAll)
                {
                    //Sending to all users

                    var customers = (await _customerRepository.GetAllAsync().ConfigureAwait(false)).ToList();
                    var drivers = (await _driverRepository.GetAllAsync().ConfigureAwait(false)).ToList();

                    foreach (var item in customers)
                    {
                        NotificationManager notiMgrCustomers = new NotificationManager(_configuration["OneSignal:ApplicationID"], _configuration["OneSignal:RestApiKey"], _configuration["OneSignal:AuthenticationID"]);
                        var oneSignalIds = (await _notificationUserRepository.FindByCondition(x => x.UserId == item.Id && x.IsActive).ConfigureAwait(false)).AsQueryable().Select(x => x.OneSignalId).ToList();

                        if (oneSignalIds.Count > 0)
                        {
                            if (request.IsListing)
                            {
                                notification.OneSignalId = oneSignalIds.FirstOrDefault();
                                notification.UserId = item.Id;
                                notification.OwnerId = request.OwnerId;
                                notification.ServiceCode = request.ServiceCode;
                                notification.ReferenceContentKey = request.ReferenceContentKey;
                                notification.Payload = request.Payload;
                                notification.Created = DateTime.UtcNow;
                                notification.Modified = DateTime.UtcNow;
                                notification.CreatedBy = request.OwnerId.ToString();
                                notification.ModifiedBy = request.OwnerId.ToString();
                                await _notificationRepository.AddAsync(notification);
                            }

                            notiMgrCustomers.SendNotificationAsync(request.Title, request.Detail, oneSignalIds, request.ServiceCode, request.ReferenceContentKey.ToString(), request.Payload);
                        }
                    }

                    foreach (var item in drivers)
                    {
                        NotificationManager notiMgrDrivers = new NotificationManager(_configuration["DriverOneSignal:ApplicationID"], _configuration["DriverOneSignal:RestApiKey"], _configuration["DriverOneSignal:AuthenticationID"]);
                        var oneSignalIds = (await _notificationUserRepository.FindByCondition(x => x.UserId == item.Id && x.IsActive).ConfigureAwait(false)).AsQueryable().Select(x => x.OneSignalId).ToList();

                        if (oneSignalIds.Count > 0)
                        {
                            if (request.IsListing)
                            {
                                notification.OneSignalId = oneSignalIds.FirstOrDefault();
                                notification.UserId = item.Id;
                                notification.OwnerId = request.OwnerId;
                                notification.ServiceCode = request.ServiceCode;
                                notification.ReferenceContentKey = request.ReferenceContentKey;
                                notification.Payload = request.Payload;
                                notification.Created = DateTime.UtcNow;
                                notification.Modified = DateTime.UtcNow;
                                notification.CreatedBy = request.OwnerId.ToString();
                                notification.ModifiedBy = request.OwnerId.ToString();
                                await _notificationRepository.AddAsync(notification);
                            }


                            notiMgrDrivers.SendNotificationAsync(request.Title, request.Detail, oneSignalIds, request.ServiceCode, request.ReferenceContentKey.ToString(), request.Payload);
                        }
                    }
                }
                else if (string.IsNullOrEmpty(request.TargetCustomers) && string.IsNullOrEmpty(request.TargetDrivers))
                {
                    //Sending to specific user

                    var oneSignalIds = (await _notificationUserRepository.FindByCondition(x => x.UserId == request.UserId && x.IsActive).ConfigureAwait(false)).AsQueryable().Select(x => x.OneSignalId).ToList();

                    if (oneSignalIds.Count > 0)
                    {
                        if (request.IsListing)
                        {
                            notification.OneSignalId = oneSignalIds.FirstOrDefault();
                            notification.UserId = request.UserId;
                            notification.OwnerId = request.OwnerId;
                            notification.ServiceCode = request.ServiceCode;
                            notification.ReferenceContentKey = request.ReferenceContentKey;
                            notification.Payload = request.Payload;
                            notification.Created = DateTime.UtcNow;
                            notification.Modified = DateTime.UtcNow;
                            notification.CreatedBy = request.OwnerId.ToString();
                            notification.ModifiedBy = request.OwnerId.ToString();
                            await _notificationRepository.AddAsync(notification);
                        }

                        var customer = (await _customerRepository.FindByCondition(x => x.Id == request.UserId).ConfigureAwait(false)).FirstOrDefault();

                        if (customer != null)
                        {
                            NotificationManager notiMgrCustomers = new NotificationManager(_configuration["OneSignal:ApplicationID"], _configuration["OneSignal:RestApiKey"], _configuration["OneSignal:AuthenticationID"]);
                            notiMgrCustomers.SendNotificationAsync(request.Title, request.Detail, oneSignalIds, request.ServiceCode, request.ReferenceContentKey.ToString(), request.Payload);
                        }
                        else
                        {
                            var driver = (await _driverRepository.FindByCondition(x => x.Id == request.UserId).ConfigureAwait(false)).FirstOrDefault();

                            if (driver != null)
                            {
                                NotificationManager notiMgrDrivers = new NotificationManager(_configuration["DriverOneSignal:ApplicationID"], _configuration["DriverOneSignal:RestApiKey"], _configuration["DriverOneSignal:AuthenticationID"]);
                                notiMgrDrivers.SendNotificationAsync(request.Title, request.Detail, oneSignalIds, request.ServiceCode, request.ReferenceContentKey.ToString(), request.Payload);
                            }
                        }
                    }
                }

                else
                {
                    //Sending to group user

                    if (!string.IsNullOrEmpty(request.TargetCustomers))
                    {
                        string[] allowType = request.TargetCustomers.Split(',');

                        var customers = (await _customerRepository.FindByCondition(x => allowType.Contains(x.CustomerType)).ConfigureAwait(false)).ToList();

                        foreach (var item in customers)
                        {
                            NotificationManager notiMgrCustomers = new NotificationManager(_configuration["OneSignal:ApplicationID"], _configuration["OneSignal:RestApiKey"], _configuration["OneSignal:AuthenticationID"]);
                            var oneSignalIds = (await _notificationUserRepository.FindByCondition(x => x.UserId == item.Id && x.IsActive).ConfigureAwait(false)).AsQueryable().Select(x => x.OneSignalId).ToList();

                            if (oneSignalIds.Count > 0)
                            {
                                if (request.IsListing)
                                {
                                    notification.OneSignalId = oneSignalIds.FirstOrDefault();
                                    notification.UserId = item.Id;
                                    notification.OwnerId = request.OwnerId;
                                    notification.ServiceCode = request.ServiceCode;
                                    notification.ReferenceContentKey = request.ReferenceContentKey;
                                    notification.Payload = request.Payload;
                                    notification.Created = DateTime.UtcNow;
                                    notification.Modified = DateTime.UtcNow;
                                    notification.CreatedBy = request.OwnerId.ToString();
                                    notification.ModifiedBy = request.OwnerId.ToString();
                                    await _notificationRepository.AddAsync(notification);
                                } 

                                notiMgrCustomers.SendNotificationAsync(request.Title, request.Detail, oneSignalIds, request.ServiceCode, request.ReferenceContentKey.ToString(), request.Payload);
                            }
                        }
                    }

                    if (!string.IsNullOrEmpty(request.TargetDrivers))
                    {
                        string[] allowType = request.TargetDrivers.Split(',');

                        var drivers = (await _driverRepository.FindByCondition(x => allowType.Contains(x.DriverType)).ConfigureAwait(false)).ToList();

                        foreach (var item in drivers)
                        {
                            NotificationManager notiMgrDrivers = new NotificationManager(_configuration["DriverOneSignal:ApplicationID"], _configuration["DriverOneSignal:RestApiKey"], _configuration["DriverOneSignal:AuthenticationID"]);
                            var oneSignalIds = (await _notificationUserRepository.FindByCondition(x => x.UserId == item.Id && x.IsActive).ConfigureAwait(false)).AsQueryable().Select(x => x.OneSignalId).ToList();

                            if (oneSignalIds.Count > 0)
                            {
                                if (request.IsListing)
                                {
                                    notification.OneSignalId = oneSignalIds.FirstOrDefault();
                                    notification.UserId = item.Id;
                                    notification.OwnerId = request.OwnerId;
                                    notification.ServiceCode = request.ServiceCode;
                                    notification.ReferenceContentKey = request.ReferenceContentKey;
                                    notification.Payload = request.Payload;
                                    notification.Created = DateTime.UtcNow;
                                    notification.Modified = DateTime.UtcNow;
                                    notification.CreatedBy = request.OwnerId.ToString();
                                    notification.ModifiedBy = request.OwnerId.ToString();
                                    await _notificationRepository.AddAsync(notification);
                                }
                                 
                                notiMgrDrivers.SendNotificationAsync(request.Title, request.Detail, oneSignalIds, request.ServiceCode, request.ReferenceContentKey.ToString(), request.Payload);
                            }
                        }
                    }
                }

                var log = new CreateAppLog(_applicationLogRepository);
                log.CreateLog("Notification", "Notification", "Create", Newtonsoft.Json.JsonConvert.SerializeObject(notification),request.IdUser.ToString());
                return new Response<int>(1);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
