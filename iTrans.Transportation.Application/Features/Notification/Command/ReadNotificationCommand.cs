﻿using AutoMapper;
using iTrans.Transportation.Application.DTOs.OrderingAddressProduct;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Interfaces.Services;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using iTrans.Transportation.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;


namespace iTrans.Transportation.Application.Features.Notification.Command
{
    public class ReadNotificationCommand : IRequest<Response<int>>
    {
        public Guid? UserId { get; set; }
        public int MessageID { get; set; }

        public class ReadNotificationCommandIRequest : IRequestHandler<ReadNotificationCommand, Response<int>>
        {
            private readonly INotificationRepositoryAsync _notificationRepository;
            private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
            private readonly IMapper _mapper;
            public ReadNotificationCommandIRequest(INotificationRepositoryAsync notificationRepository, IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
            {
                _notificationRepository = notificationRepository;
                _applicationLogRepository = applicationLogRepository;
                _mapper = mapper;
            }

            public async Task<Response<int>> Handle(ReadNotificationCommand request, CancellationToken cancellationToken)
            {
                try
                {
                    var noti = (await _notificationRepository.FindByCondition(x => x.Id == request.MessageID).ConfigureAwait(false)).FirstOrDefault();
                    noti.IsRead = true;
                    noti.Modified = DateTime.Now;
                    noti.ModifiedBy = request.UserId.ToString();

                    await _notificationRepository.UpdateAsync(noti);

                    var log = new CreateAppLog(_applicationLogRepository);
                    log.CreateLog("Notification", "Notification Read", "Edit", Newtonsoft.Json.JsonConvert.SerializeObject(request.MessageID),request.UserId.ToString());
                    return new Response<int>(request.MessageID);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
    }
}