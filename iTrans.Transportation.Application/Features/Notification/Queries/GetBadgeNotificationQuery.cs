﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.News;
using iTrans.Transportation.Application.DTOs.Notification;
using iTrans.Transportation.Application.Enums;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;

namespace iTrans.Transportation.Application.Features.Notification.Queries
{
    public class GetBadgeNotificationQuery : IRequest<Response<int>>
    {
        public Guid? UserId { get; set; }
    }

    public class GetBadgeNotificationQueryHandler : IRequestHandler<GetBadgeNotificationQuery, Response<int>>
    {
        private readonly INotificationRepositoryAsync _notificationRepository;
        private readonly IMapper _mapper;
        public GetBadgeNotificationQueryHandler(INotificationRepositoryAsync notificationRepository, IMapper mapper)
        {
            _notificationRepository = notificationRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(GetBadgeNotificationQuery request, CancellationToken cancellationToken)
        {
            var validFilter = _mapper.Map<GetBadgeNotificationQuery>(request);

            var notiViewModel = (from n in await _notificationRepository.GetSession()
                                 where !n.IsRead && n.UserId == request.UserId.Value
                                 select n);
            return new PagedResponse<int>(notiViewModel.Count(), 1, 1, notiViewModel.Count());
        }
    }
}
