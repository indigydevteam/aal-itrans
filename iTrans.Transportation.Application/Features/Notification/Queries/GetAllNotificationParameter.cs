﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.News;
using iTrans.Transportation.Application.DTOs.Notification;
using iTrans.Transportation.Application.Enums;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;

namespace iTrans.Transportation.Application.Features.Notification.Queries
{
    public class GetAllNotificationParameter : IRequest<Response<IEnumerable<NotificationViewModel>>>
    {
        public Guid? UserId { get; set; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
    }
    public class GetAllNotificationParameterHandler : IRequestHandler<GetAllNotificationParameter, Response<IEnumerable<NotificationViewModel>>>
    {
        private readonly INotificationRepositoryAsync _notificationRepository;
        private readonly IMapper _mapper;
        public GetAllNotificationParameterHandler(INotificationRepositoryAsync notificationRepository, IMapper mapper)
        {
            _notificationRepository = notificationRepository;
            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<NotificationViewModel>>> Handle(GetAllNotificationParameter request, CancellationToken cancellationToken)
        {
            var validFilter = _mapper.Map<GetAllNotificationParameter>(request);

            var notiViewModel = (from n in await _notificationRepository.GetSession()
                                 where !n.IsDelete && n.UserId == request.UserId.Value
                                 select new NotificationViewModel
                                 {
                                     MessageId = n.Id,
                                     Title = n.Title,
                                     Detail = n.Detail,
                                     ServiceCode = n.ServiceCode,
                                     Payload = n.Payload,
                                     ReferentContentKey = n.ReferenceContentKey.ToString(),
                                     Created = n.Created,
                                     CreatedBy = n.CreatedBy
                                 });
            
            notiViewModel = notiViewModel.OrderByDescending(x => x.Created).Skip((request.PageNumber - 1) * request.PageSize).Take(request.PageSize);

            return new PagedResponse<IEnumerable<NotificationViewModel>>(notiViewModel, validFilter.PageNumber, validFilter.PageSize);
        }
    }

}
