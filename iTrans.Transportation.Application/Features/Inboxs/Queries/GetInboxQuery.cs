﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.Inbox;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using LinqKit;
using MediatR;

namespace iTrans.Transportation.Application.Features.Inboxs.Queries
{
    public class GetInboxQuery : IRequest<Response<IEnumerable<InboxViewModel>>>
    {
        public string UserType { get; set; }
        public Guid UserId { get; set; }
        public int Status { get; set; }
        public string Search { get; set; }
    }
    public class GetInboxQueryHandler : IRequestHandler<GetInboxQuery, Response<IEnumerable<InboxViewModel>>>
    {
        private readonly IInboxRepositoryAsync _inboxRepository;
        private readonly IMapper _mapper;
        public GetInboxQueryHandler(IInboxRepositoryAsync inboxRepository, IMapper mapper)
        {
            _inboxRepository = inboxRepository;
            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<InboxViewModel>>> Handle(GetInboxQuery request, CancellationToken cancellationToken)
        {
            try
            {

                string Search = request.Search == null ? "" : request.Search.Trim();
                var inboxs = (await _inboxRepository.CreateSQLQuery(@"EXEC [dbo].[SP_SELECT_INBOX] @UserType = '" + request.UserType + "',@UserId ='" + request.UserId + "',@Status = "+ request.Status.ToString()  + ",@Search ='"+ Search + "'")
                             .ConfigureAwait(false))
                             .SetResultTransformer(NHibernate.Transform.Transformers.AliasToBean(typeof(UserInbox)))
                             .List<UserInbox>();

                var inboxViewModel = _mapper.Map<IEnumerable<InboxViewModel>>(inboxs);
                return new Response<IEnumerable<InboxViewModel>>(inboxViewModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
