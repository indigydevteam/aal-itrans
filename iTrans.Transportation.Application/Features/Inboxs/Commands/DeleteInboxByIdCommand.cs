﻿using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
namespace iTrans.Transportation.Application.Features.Inboxs.Commands
{
    public class DeleteInboxByIdCommand : IRequest<Response<int>>
    {
        public string Type { get; set; }
        public int Id { get; set; }
        public class DeleteInboxByIdCommandHandler : IRequestHandler<DeleteInboxByIdCommand, Response<int>>
        {
            private readonly INotificationRepositoryAsync _notificationRepository;
            private readonly IChatGroupMessageRepositoryAsync _chatGroupMessageRepository;
            public DeleteInboxByIdCommandHandler(INotificationRepositoryAsync notificationRepository, IChatGroupMessageRepositoryAsync chatGroupMessageRepository)
            {
                _notificationRepository = notificationRepository;
                _chatGroupMessageRepository = chatGroupMessageRepository;
            }
            public async Task<Response<int>> Handle(DeleteInboxByIdCommand command, CancellationToken cancellationToken)
            {
                if (command.Type == "notification")
                {
                    var notification = (await _notificationRepository.FindByCondition(x => x.Id == command.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                    if (notification == null)
                    {
                        return new Response<int>(0);
                    }
                    else
                    {
                        notification.IsDelete = true;
                        await _notificationRepository.UpdateAsync(notification);
                        return new Response<int>(notification.Id);
                    }
                }
                else if(command.Type == "chat")
                {
                    var chat = (await _chatGroupMessageRepository.FindByCondition(x => x.Id == command.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                    if (chat == null)
                    {
                        return new Response<int>(0);
                    }
                    else
                    {
                        chat.IsDelete = true;
                        await _chatGroupMessageRepository.UpdateAsync(chat);
                        return new Response<int>(chat.Id);
                    }
                }
                return new Response<int>(0);
            }
        }
    }
}
