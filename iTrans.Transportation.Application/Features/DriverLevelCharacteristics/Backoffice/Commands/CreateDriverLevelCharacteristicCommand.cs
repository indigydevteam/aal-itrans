﻿using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Interfaces.Services;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using iTrans.Transportation.Domain.Entities;
using MediatR;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.DriverLevelCharacteristics.Commands
{
    public partial class CreateDriverLevelCharacteristicCommand : IRequest<Response<int>>
    {
        public Guid? UserId { get; set; }
        public int Level { get; set; }
        public int Star { get; set; }
        public int AcceptJobPerMonth { get; set; }
        public int CancelPerMonth { get; set; }
        public int AnnouncementPerMonth { get; set; }
        public int AnnouncementPerYear { get; set; }
        public int ComplaintPerMonth { get; set; }
        public int RejectPerMonth { get; set; }
        public int ComplaintPerYear { get; set; }
        public int RejectPerYear { get; set; }
        public  virtual int CancelPerYear { get; set; }
        public decimal InsuranceValue { get; set; }

    }
    public class CreateDriverLevelCharacteristicCommandHandler : IRequestHandler<CreateDriverLevelCharacteristicCommand, Response<int>>
    {
        private readonly IDriverLevelCharacteristicRepositoryAsync _driverLevelCharacteristicRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;
        public CreateDriverLevelCharacteristicCommandHandler(IDriverLevelCharacteristicRepositoryAsync driverLevelCharacteristicRepository, IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _driverLevelCharacteristicRepository = driverLevelCharacteristicRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(CreateDriverLevelCharacteristicCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var driverLevelCharacteristic = _mapper.Map<DriverLevelCharacteristic>(request);
                driverLevelCharacteristic.Created = DateTime.UtcNow;
                driverLevelCharacteristic.Modified = DateTime.UtcNow;
                var driverLevelCharacteristicObject = await _driverLevelCharacteristicRepository.AddAsync(driverLevelCharacteristic);
                var log = new CreateAppLog(_applicationLogRepository);
                log.CreateLog("DriverLevelCharacteristic", "Driver LevelCharacteristic", "Create", Newtonsoft.Json.JsonConvert.SerializeObject(request),request.UserId.ToString());
                return new Response<int>(driverLevelCharacteristicObject.Id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
