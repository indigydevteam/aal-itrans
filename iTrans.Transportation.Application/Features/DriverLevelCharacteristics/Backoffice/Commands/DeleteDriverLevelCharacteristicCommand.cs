﻿using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
namespace iTrans.Transportation.Application.Features.DriverLevelCharacteristics.Commands
{
    public class DeleteDriverLevelCharacteristicCommand : IRequest<Response<int>>
    {
        public int Id { get; set; }
        public class DeleteDriverLevelCharacteristicCommandHandler : IRequestHandler<DeleteDriverLevelCharacteristicCommand, Response<int>>
        {
            private readonly IDriverLevelCharacteristicRepositoryAsync _driverLevelCharacteristicRepository;
            public DeleteDriverLevelCharacteristicCommandHandler(IDriverLevelCharacteristicRepositoryAsync driverLevelCharacteristicRepository)
            {
                _driverLevelCharacteristicRepository = driverLevelCharacteristicRepository;
            }
            public async Task<Response<int>> Handle(DeleteDriverLevelCharacteristicCommand command, CancellationToken cancellationToken)
            {
                var driverLevelCharacteristic = (await _driverLevelCharacteristicRepository.FindByCondition(x => x.Id == command.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (driverLevelCharacteristic == null)
                {
                    throw new ApiException($"Driver LevelCharacteristic Not Found.");
                }
                else
                {
                    await _driverLevelCharacteristicRepository.DeleteAsync(driverLevelCharacteristic);
                    return new Response<int>(driverLevelCharacteristic.Id);
                }
            }
        }
    }
}
