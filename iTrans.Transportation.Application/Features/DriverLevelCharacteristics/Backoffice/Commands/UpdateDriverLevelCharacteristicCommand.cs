﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;

namespace iTrans.Transportation.Application.Features.DriverLevelCharacteristics.Commands
{
    public class UpdateDriverLevelCharacteristicCommand : IRequest<Response<int>>
    {
        public int Id { get; set; }
        public Guid? UserId { get; set; }
        public int Level { get; set; }
        public int Star { get; set; }
        public int AcceptJobPerWeek { get; set; }
        public int AcceptJobPerMonth { get; set; }
        public int AcceptJobPerYear { get; set; }
        public int CancelPerWeek { get; set; }
        public int CancelPerMonth { get; set; }
        public int CancelPerYear { get; set; }
        public int ComplaintPerWeek { get; set; }
        public int ComplaintPerMonth { get; set; }
        public int ComplaintPerYear { get; set; }
        public int RejectPerWeek { get; set; }
        public int RejectPerMonth { get; set; }
        public int RejectPerYear { get; set; }
        public decimal InsuranceValue { get; set; }
        public decimal Commission { get; set; }
        public decimal Discount { get; set; }
        public decimal Fine { get; set; }
    }
    public class UpdateDriverLevelCharacteristicCommandHandler : IRequestHandler<UpdateDriverLevelCharacteristicCommand, Response<int>>
    {
        private readonly IDriverLevelCharacteristicRepositoryAsync _driverLevelCharacteristicRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;

        public UpdateDriverLevelCharacteristicCommandHandler(IDriverLevelCharacteristicRepositoryAsync driverLevelCharacteristicRepository, IApplicationLogRepositoryAsync applicationLogRepository,IMapper mapper)
        {
            _driverLevelCharacteristicRepository = driverLevelCharacteristicRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(UpdateDriverLevelCharacteristicCommand request, CancellationToken cancellationToken)
        {
            var driverLevelCharacteristic = (await _driverLevelCharacteristicRepository.FindByCondition(x => x.Id == request.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            if (driverLevelCharacteristic == null)
            {
                throw new ApiException($"Driver LevelCharacteristic Not Found.");
            }
            else
            {
                driverLevelCharacteristic.Level = request.Level;
                driverLevelCharacteristic.Star = request.Star;
                driverLevelCharacteristic.AcceptJobPerWeek = request.AcceptJobPerWeek;
                driverLevelCharacteristic.AcceptJobPerMonth = request.AcceptJobPerMonth;
                driverLevelCharacteristic.AcceptJobPerYear = request.AcceptJobPerYear;
                driverLevelCharacteristic.CancelPerMonth = request.CancelPerMonth;
                driverLevelCharacteristic.ComplaintPerMonth = request.ComplaintPerMonth;
                driverLevelCharacteristic.ComplaintPerYear = request.ComplaintPerYear;
                driverLevelCharacteristic.RejectPerMonth = request.RejectPerMonth;
                driverLevelCharacteristic.RejectPerYear = request.RejectPerYear;
                driverLevelCharacteristic.CancelPerYear = request.CancelPerYear;
                driverLevelCharacteristic.InsuranceValue = request.InsuranceValue;
                driverLevelCharacteristic.Commission = request.Commission;
                driverLevelCharacteristic.Discount = request.Discount;
                driverLevelCharacteristic.Discount = request.Fine;
                driverLevelCharacteristic.ModifiedBy = request.UserId != null ? request.UserId.Value.ToString():"-";
                driverLevelCharacteristic.Modified = DateTime.UtcNow;
                await _driverLevelCharacteristicRepository.UpdateAsync(driverLevelCharacteristic);
                var log = new CreateAppLog(_applicationLogRepository);
                log.CreateLog("DriverLevelCharacteristic", "Driver LevelCharacteristic", "Edit", Newtonsoft.Json.JsonConvert.SerializeObject(request), request.UserId != null ? request.UserId.Value.ToString() : "-");
                return new Response<int>(driverLevelCharacteristic.Id);
            }
        }
    }
}
