﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.DriverLevelCharacteristic;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.DriverLevelCharacteristics.Queries
{
    public class GetDriverLevelCharacteristicByIdQuery : IRequest<Response<DriverLevelCharacteristicViewModel>>
    {
        public int Id { get; set; }

    }
    public class GetDriverLevelCharacteristicQueryHandler : IRequestHandler<GetDriverLevelCharacteristicByIdQuery, Response<DriverLevelCharacteristicViewModel>>
    {
        private readonly IDriverLevelCharacteristicRepositoryAsync _driverLevelCharacteristicRepository;
        private readonly IMapper _mapper;
        public GetDriverLevelCharacteristicQueryHandler(IDriverLevelCharacteristicRepositoryAsync driverLevelCharacteristicRepository, IMapper mapper)
        {
            _driverLevelCharacteristicRepository = driverLevelCharacteristicRepository;
            _mapper = mapper;
        }
        public async Task<Response<DriverLevelCharacteristicViewModel>> Handle(GetDriverLevelCharacteristicByIdQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var driverObject = (await _driverLevelCharacteristicRepository.FindByCondition(x => x.Id.Equals(request.Id)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                return new Response<DriverLevelCharacteristicViewModel>(_mapper.Map<DriverLevelCharacteristicViewModel>(driverObject));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
