﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.DriverLevelCharacteristic;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using LinqKit;
using MediatR;

namespace iTrans.Transportation.Application.Features.DriverLevelCharacteristics.Queries
{
    public class GetAllDriverLevelCharacteristicQuery : IRequest<PagedResponse<IEnumerable<DriverLevelCharacteristicViewModel>>>
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public string Search { get; set; }
        public int Active { set; get; }
        public string Column { set; get; }
    }
    public class GetAllDriverLevelCharacteristicQueryHandler : IRequestHandler<GetAllDriverLevelCharacteristicQuery, PagedResponse<IEnumerable<DriverLevelCharacteristicViewModel>>>
    {
        private readonly IDriverLevelCharacteristicRepositoryAsync _driverLevelCharacteristicRepository;
        private readonly IMapper _mapper;
        private Expression<Func<DriverLevelCharacteristic, bool>> expression;
        public GetAllDriverLevelCharacteristicQueryHandler(IDriverLevelCharacteristicRepositoryAsync driverLevelCharacteristicRepository, IMapper mapper)
        {
            _driverLevelCharacteristicRepository = driverLevelCharacteristicRepository;
            _mapper = mapper;
            expression = null;
        }

        public async Task<PagedResponse<IEnumerable<DriverLevelCharacteristicViewModel>>> Handle(GetAllDriverLevelCharacteristicQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var validFilter = _mapper.Map<GetAllDriverLevelCharacteristicQuery>(request);

                var result = from x in _driverLevelCharacteristicRepository.GetAllAsync().Result.ToList().OrderByDescending(x => x.Modified)
                             select new DriverLevelCharacteristicViewModel
                             {
                                 Id = x.Id,
                                 Level = x.Level.ToString() != null ? x.Level : 0,
                                 Star = x.Star.ToString() != null ? x.Level : 0,
                                 CancelPerMonth = x.CancelPerMonth.ToString() != null ? x.CancelPerMonth : 0,
                                 RejectPerMonth = x.RejectPerMonth.ToString() != null ? x.RejectPerMonth : 0,
                                 CancelPerYear = x.CancelPerYear.ToString() != null ? x.CancelPerYear : 0,
                                 ComplaintPerMonth = x.ComplaintPerMonth.ToString() != null ? x.ComplaintPerMonth : 0,
                                 RejectPerYear = x.RejectPerYear.ToString() != null ? x.RejectPerYear : 0,
                                 ComplaintPerYear = x.ComplaintPerYear.ToString() != null ? x.ComplaintPerYear : 0,
                                 InsuranceValue = x.InsuranceValue.ToString() != null ? x.InsuranceValue : 0,

                             };

                if (validFilter.Search != null && validFilter.Search.Trim() != "")
                {
                    result = result.Where(x => x.Level.Equals(validFilter.Search.Trim()) || x.Star.Equals(validFilter.Search.Trim())
                             || x.CancelPerMonth.Equals(validFilter.Search.Trim()) || x.RejectPerMonth.Equals(validFilter.Search.Trim())
                             || x.ComplaintPerMonth.Equals(validFilter.Search.Trim()) || x.RejectPerYear.Equals(validFilter.Search.Trim())
                             || x.CancelPerYear.Equals(validFilter.Search.Trim()) || x.InsuranceValue.Equals(validFilter.Search.Trim())
                             || x.ComplaintPerYear.Equals(validFilter.Search.Trim())).ToList();
                }
                if (request.Column == "level" && request.Active == 3)
                {
                    result = result.OrderBy(x => x.Level);
                }
                if (request.Column == "level" && request.Active == 2)
                {
                    result = result.OrderByDescending(x => x.Level);
                }
                if (request.Column == "star" && request.Active == 3)
                {
                    result = result.OrderBy(x => x.Star);
                }
                if (request.Column == "star" && request.Active == 2)
                {
                    result = result.OrderByDescending(x => x.Star);
                }
                if (request.Column == "cancelPerMonth" && request.Active == 3)
                {
                    result = result.OrderBy(x => x.CancelPerMonth);
                }
                if (request.Column == "cancelPerMonth" && request.Active == 2)
                {
                    result = result.OrderByDescending(x => x.CancelPerMonth);
                }
                if (request.Column == "rejectPerMonth" && request.Active == 3)
                {
                    result = result.OrderBy(x => x.RejectPerMonth);
                }
                if (request.Column == "rejectPerMonth" && request.Active == 2)
                {
                    result = result.OrderByDescending(x => x.RejectPerMonth);
                }
                if (request.Column == "complaintPerMonth" && request.Active == 3)
                {
                    result = result.OrderBy(x => x.ComplaintPerMonth);
                }
                if (request.Column == "complaintPerMonth" && request.Active == 2)
                {
                    result = result.OrderByDescending(x => x.ComplaintPerMonth);
                }
                if (request.Column == "rejectPerYear" && request.Active == 3)
                {
                    result = result.OrderBy(x => x.RejectPerYear);
                }
                if (request.Column == "rejectPerYear" && request.Active == 2)
                {
                    result = result.OrderByDescending(x => x.RejectPerYear);
                }
                if (request.Column == "cancelPerYear" && request.Active == 3)
                {
                    result = result.OrderBy(x => x.CancelPerYear);
                }
                if (request.Column == "cancelPerYear" && request.Active == 2)
                {
                    result = result.OrderByDescending(x => x.CancelPerYear);
                }
                if (request.Column == "insuranceValue" && request.Active == 3)
                {
                    result = result.OrderBy(x => x.InsuranceValue);
                }
                if (request.Column == "insuranceValue" && request.Active == 2)
                {
                    result = result.OrderByDescending(x => x.InsuranceValue);
                }
                if (request.Column == "complaintPerYear" && request.Active == 3)
                {
                    result = result.OrderBy(x => x.ComplaintPerYear);
                }
                if (request.Column == "complaintPerYear" && request.Active == 2)
                {
                    result = result.OrderByDescending(x => x.ComplaintPerYear);
                }

                var itemCount = result.Count();
                result = result.Skip((validFilter.PageNumber - 1) * validFilter.PageSize).Take(validFilter.PageSize);
                return new PagedResponse<IEnumerable<DriverLevelCharacteristicViewModel>>(result, request.PageNumber, request.PageSize);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
