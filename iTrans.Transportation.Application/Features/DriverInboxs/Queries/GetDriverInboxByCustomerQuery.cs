﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.DriverInbox;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.DriverInboxs.Queries
{
   public class GetDriverInboxByDriverQuery : IRequest<Response<IEnumerable<DriverInboxViewModel>>>
    {
        public Guid DriverId { get; set; }
    }
    public class GetDriverInboxByDriverQueryHandler : IRequestHandler<GetDriverInboxByDriverQuery, Response<IEnumerable<DriverInboxViewModel>>>
    {
        private readonly IDriverInboxRepositoryAsync _driverInboxRepository;
        private readonly IMapper _mapper;
        public GetDriverInboxByDriverQueryHandler(IDriverInboxRepositoryAsync driverInboxRepository, IMapper mapper)
        {
            _driverInboxRepository = driverInboxRepository;
            _mapper = mapper;
        }
        public async Task<Response<IEnumerable<DriverInboxViewModel>>> Handle(GetDriverInboxByDriverQuery request, CancellationToken cancellationToken)
        {
            var driverInboxObject = (await _driverInboxRepository.FindByCondition(x => x.DriverId.Equals(request.DriverId) && x.IsDelete == false).ConfigureAwait(false) ).AsQueryable().ToList();
            return new Response<IEnumerable<DriverInboxViewModel>>(_mapper.Map<IEnumerable<DriverInboxViewModel>>(driverInboxObject));
        }
    }
}
