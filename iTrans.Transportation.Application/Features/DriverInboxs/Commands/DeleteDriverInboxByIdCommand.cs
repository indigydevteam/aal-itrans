﻿using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
namespace iTrans.Transportation.Application.Features.DriverInboxs.Commands
{
    public class DeleteDriverInboxByIdCommand : IRequest<Response<int>>
    {
        public Guid DriverId { get; set; }
        public int Id { get; set; }
        public class DeleteDriverInboxByIdCommandHandler : IRequestHandler<DeleteDriverInboxByIdCommand, Response<int>>
        {
            private readonly IDriverInboxRepositoryAsync _driverInboxRepository;
            public DeleteDriverInboxByIdCommandHandler(IDriverInboxRepositoryAsync driverInboxRepository)
            {
                _driverInboxRepository = driverInboxRepository;
            }
            public async Task<Response<int>> Handle(DeleteDriverInboxByIdCommand command, CancellationToken cancellationToken)
            {
                var driverInbox = (await _driverInboxRepository.FindByCondition(x => x.Id == command.Id && x.DriverId == command.DriverId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (driverInbox == null)
                {
                    throw new ApiException($"Driver Inbox Not Found.");
                }
                else
                {
                    driverInbox.IsDelete = true;
                    await _driverInboxRepository.UpdateAsync(driverInbox);
                    return new Response<int>(driverInbox.Id);
                }
            }
        }
    }
}
