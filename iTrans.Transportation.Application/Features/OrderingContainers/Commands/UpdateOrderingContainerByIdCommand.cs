﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
//using iTrans.Transportation.Application.DTOs.OrderingContainerAddress;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;


namespace iTrans.Transportation.Application.Features.OrderingContainers.Commands
{
    public class UpdateOrderingContainerByIdCommand : IRequest<Response<int>>
    {
        public int Id { get; set; }
        public Guid OrderingId { get; set; }
        public string Shipper { get; set; }
        public string Booking { get; set; }
        public string Contact { get; set; }
        public string Valume { get; set; }
        public string PhoneCode { get; set; }
        public string PhoneNumber { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string Commodity { get; set; }
        public string Term { get; set; }
        public string Remark { get; set; }
        public bool Import { get; set; }
        public bool Export { get; set; }
        //public CreateContainerAddressViewModel Address { get; set; }
    }

    public class UpdateOrderingContainerByIdCommandHandler : IRequestHandler<UpdateOrderingContainerByIdCommand, Response<int>>
    {
        private readonly IOrderingContainerRepositoryAsync _orderingContainerRepository;
        private readonly ICountryRepositoryAsync _countryRepository;
        private readonly IProvinceRepositoryAsync _provinceRepository;
        private readonly IDistrictRepositoryAsync _districtRepository;
        private readonly ISubdistrictRepositoryAsync _subdistrictRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;

        public UpdateOrderingContainerByIdCommandHandler(IOrderingContainerRepositoryAsync orderingContainerRepository, ICountryRepositoryAsync countryRepository
            , IProvinceRepositoryAsync provinceRepository, IDistrictRepositoryAsync districtRepository, ISubdistrictRepositoryAsync subdistrictRepository,IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _orderingContainerRepository = orderingContainerRepository;
            _countryRepository = countryRepository;
            _provinceRepository = provinceRepository;
            _districtRepository = districtRepository;
            _subdistrictRepository = subdistrictRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(UpdateOrderingContainerByIdCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var orderingContainer = (await _orderingContainerRepository.FindByCondition(x => x.Id == request.Id && x.Ordering.Id.Equals(request.OrderingId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (orderingContainer == null)
                {
                    throw new ApiException($"OrderingContainer Not Found.");
                }
                else
                {
                    //orderingContainer.Shipper = request.Shipper;
                    //orderingContainer.Booking = request.Booking;
                    //orderingContainer.Contact = request.Contact;
                    //orderingContainer.Valume = request.Valume;
                    //orderingContainer.PhoneCode = request.PhoneCode;
                    //orderingContainer.PhoneNumber = request.PhoneNumber;
                    //orderingContainer.Fax = request.Fax;
                    //orderingContainer.Email = request.Email;
                    //orderingContainer.Commodity = request.Commodity;
                    //orderingContainer.Term = request.Term;
                    //orderingContainer.Remark = request.Remark;
                    //orderingContainer.Import = request.Import;
                    //orderingContainer.Export = request.Export;
                    //if (request.Address != null)
                    //{
                    //    Country loadingCountry = (await _countryRepository.FindByCondition(x => x.Id.Equals(request.Address.LoadingCountryId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                    //    Province loadingProvince = (await _provinceRepository.FindByCondition(x => x.Id.Equals(request.Address.LoadingProvinceId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                    //    District loadingDistrict = (await _districtRepository.FindByCondition(x => x.Id.Equals(request.Address.LoadingDistrictId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                    //    Subdistrict loadingSubdistrict = (await _subdistrictRepository.FindByCondition(x => x.Id.Equals(request.Address.LoadingSubdistrictId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                    //    Country destinationCountry = (await _countryRepository.FindByCondition(x => x.Id.Equals(request.Address.DestinationCountryId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                    //    Province destinationProvince = (await _provinceRepository.FindByCondition(x => x.Id.Equals(request.Address.DestinationProvinceId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                    //    District destinationDistrict = (await _districtRepository.FindByCondition(x => x.Id.Equals(request.Address.DestinationDistrictId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                    //    Subdistrict destinationSubdistrict = (await _subdistrictRepository.FindByCondition(x => x.Id.Equals(request.Address.DestinationSubdistrictId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                    //    if (orderingContainer.Addresses.Count == 0)
                    //    {
                    //        orderingContainer.Addresses = new List<OrderingContainerAddress>();
                    //        var containerAddress = _mapper.Map<OrderingContainerAddress>(request.Address);
                    //        containerAddress.OrderingContainer = orderingContainer;
                    //        containerAddress.LoadingCountry = loadingCountry;
                    //        containerAddress.LoadingProvince = loadingProvince;
                    //        containerAddress.LoadingDistrict = loadingDistrict;
                    //        containerAddress.LoadingSubdistrict = loadingSubdistrict;
                    //        containerAddress.DestinationCountry = destinationCountry;
                    //        containerAddress.DestinationProvince = destinationProvince;
                    //        containerAddress.DestinationDistrict = destinationDistrict;
                    //        containerAddress.DestinationSubdistrict = destinationSubdistrict;
                    //        orderingContainer.Addresses.Add(containerAddress);
                    //    }
                    //    else
                    //    {
                    //        //orderingContainer.Addresses[0].
                    //        orderingContainer.Addresses[0].OrderingContainer = orderingContainer;
                    //        orderingContainer.Addresses[0].LoadingCountry = loadingCountry;
                    //        orderingContainer.Addresses[0].LoadingProvince = loadingProvince;
                    //        orderingContainer.Addresses[0].LoadingDistrict = loadingDistrict;
                    //        orderingContainer.Addresses[0].LoadingSubdistrict = loadingSubdistrict;
                    //        orderingContainer.Addresses[0].LoadingPostCode = request.Address.LoadingPostCode;
                    //        orderingContainer.Addresses[0].LoadingRoad = request.Address.LoadingRoad;
                    //        orderingContainer.Addresses[0].LoadingAlley = request.Address.LoadingAlley;
                    //        orderingContainer.Addresses[0].LoadingAddress = request.Address.LoadingAddress;
                    //        orderingContainer.Addresses[0].ETD = request.Address.ETD;
                    //        orderingContainer.Addresses[0].LoadingMaps = request.Address.LoadingMaps;
                    //        orderingContainer.Addresses[0].DestinationCountry = destinationCountry;
                    //        orderingContainer.Addresses[0].DestinationProvince = destinationProvince;
                    //        orderingContainer.Addresses[0].DestinationDistrict = destinationDistrict;
                    //        orderingContainer.Addresses[0].DestinationSubdistrict = destinationSubdistrict;
                    //        orderingContainer.Addresses[0].DestinationPostCode = request.Address.DestinationPostCode;
                    //        orderingContainer.Addresses[0].DestinationRoad = request.Address.DestinationRoad;
                    //        orderingContainer.Addresses[0].DestinationAlley = request.Address.DestinationAlley;
                    //        orderingContainer.Addresses[0].DestinationAddress = request.Address.DestinationAddress;
                    //        orderingContainer.Addresses[0].ETA = request.Address.ETA;
                    //        orderingContainer.Addresses[0].DestinationMaps = request.Address.DestinationMaps;
                    //        orderingContainer.Addresses[0].FeederVessel = request.Address.FeederVessel;
                    //        orderingContainer.Addresses[0].TStime = request.Address.TStime;
                    //        orderingContainer.Addresses[0].MotherVessel = request.Address.MotherVessel;
                    //        orderingContainer.Addresses[0].CPSDate = request.Address.CPSDate;
                    //        orderingContainer.Addresses[0].CPSDetail = request.Address.CPSDetail;
                    //        orderingContainer.Addresses[0].CYDate = request.Address.CYDate;
                    //        orderingContainer.Addresses[0].ReturnDate = request.Address.ReturnDate;
                    //        orderingContainer.Addresses[0].ReturnDetail = request.Address.ReturnDetail;
                    //        orderingContainer.Addresses[0].ClosingTime = request.Address.ClosingTime;
                    //        orderingContainer.Addresses[0].CutOffVGMTime = request.Address.CutOffVGMTime;
                    //        orderingContainer.Addresses[0].CutOffSITime = request.Address.CutOffSITime;
                    //        orderingContainer.Addresses[0].Agent = request.Address.Agent;
                    //    }
                    //}
                    orderingContainer.Modified = DateTime.UtcNow;
                    await _orderingContainerRepository.UpdateAsync(orderingContainer);
                    var log = new CreateAppLog(_applicationLogRepository);
                    log.Create("Ordering", "Ordering Container ", "Edit", Newtonsoft.Json.JsonConvert.SerializeObject(orderingContainer));
                    return new Response<int>(orderingContainer.Id);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}