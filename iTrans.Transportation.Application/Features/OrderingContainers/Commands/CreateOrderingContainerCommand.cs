﻿using AutoMapper;
//using iTrans.Transportation.Application.DTOs.OrderingContainerAddress;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Interfaces.Services;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using iTrans.Transportation.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.OrderingContainers.Commands
{
   public partial class CreateOrderingContainerCommand : IRequest<Response<int>>
    {
        public Guid OrderingId { get; set; }
        public string Shipper { get; set; }
        public string Booking { get; set; }
        public string Contact { get; set; }
        public string Valume { get; set; }
        public string PhoneCode { get; set; }
        public string PhoneNumber { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string Commodity { get; set; }
        public string Term { get; set; }
        public string Remark { get; set; }
        public bool Import { get; set; }
        public bool Export { get; set; }
        //public CreateContainerAddressViewModel Address { get; set; }
    }
    public class CreateOrderingContainerCommandHandler : IRequestHandler<CreateOrderingContainerCommand, Response<int>>
    {
        private readonly IOrderingRepositoryAsync _orderingRepository;
        private readonly IOrderingContainerRepositoryAsync _orderingContainerRepository;
        private readonly ICountryRepositoryAsync _countryRepository;
        private readonly IProvinceRepositoryAsync _provinceRepository;
        private readonly IDistrictRepositoryAsync _districtRepository;
        private readonly ISubdistrictRepositoryAsync _subdistrictRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;
        public CreateOrderingContainerCommandHandler(IOrderingRepositoryAsync orderingRepository,IOrderingContainerRepositoryAsync orderingContainerRepository, ICountryRepositoryAsync countryRepository
            , IProvinceRepositoryAsync provinceRepository, IDistrictRepositoryAsync districtRepository, ISubdistrictRepositoryAsync subdistrictRepository,IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _orderingRepository = orderingRepository;
            _orderingContainerRepository = orderingContainerRepository;
            _countryRepository = countryRepository;
            _provinceRepository = provinceRepository;
            _districtRepository = districtRepository;
            _subdistrictRepository = subdistrictRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(CreateOrderingContainerCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var data = (await _orderingRepository.FindByCondition(x => x.Id == request.OrderingId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (data == null)
                {
                    throw new ApiException($"Ordering Not Found.");
                }
                var orderingContainer = _mapper.Map<OrderingContainer>(request);
                orderingContainer.Ordering = data;
                //if (request.Address != null)
                //{
                //    orderingContainer.Addresses = new List<OrderingContainerAddress>();
                //    var containerAddress = _mapper.Map<OrderingContainerAddress>(request.Address);
                
                //    Country loadingCountry = (await _countryRepository.FindByCondition(x => x.Id.Equals(request.Address.LoadingCountryId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                //    Province loadingProvince = (await _provinceRepository.FindByCondition(x => x.Id.Equals(request.Address.LoadingProvinceId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                //    District loadingDistrict = (await _districtRepository.FindByCondition(x => x.Id.Equals(request.Address.LoadingDistrictId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                //    Subdistrict loadingSubdistrict = (await _subdistrictRepository.FindByCondition(x => x.Id.Equals(request.Address.LoadingSubdistrictId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                //    Country destinationCountry = (await _countryRepository.FindByCondition(x => x.Id.Equals(request.Address.DestinationCountryId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                //    Province destinationProvince = (await _provinceRepository.FindByCondition(x => x.Id.Equals(request.Address.DestinationProvinceId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                //    District destinationDistrict = (await _districtRepository.FindByCondition(x => x.Id.Equals(request.Address.DestinationDistrictId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                //    Subdistrict destinationSubdistrict = (await _subdistrictRepository.FindByCondition(x => x.Id.Equals(request.Address.DestinationSubdistrictId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();

                //    containerAddress.OrderingContainer = orderingContainer;
                //    containerAddress.LoadingCountry = loadingCountry;
                //    containerAddress.LoadingProvince = loadingProvince;
                //    containerAddress.LoadingDistrict = loadingDistrict;
                //    containerAddress.LoadingSubdistrict = loadingSubdistrict;
                //    containerAddress.DestinationCountry = destinationCountry;
                //    containerAddress.DestinationProvince = destinationProvince;
                //    containerAddress.DestinationDistrict = destinationDistrict;
                //    containerAddress.DestinationSubdistrict = destinationSubdistrict;
                //    orderingContainer.Addresses.Add(containerAddress);
                //}

                orderingContainer.Created = DateTime.UtcNow;
                orderingContainer.Modified = DateTime.UtcNow;
                //title.CreatedBy = "xxxxxxx";
                //title.ModifiedBy = "xxxxxxx";
                var OrderingContainerObject = await _orderingContainerRepository.AddAsync(orderingContainer);
                var log = new CreateAppLog(_applicationLogRepository);
                log.Create("Ordering", "Ordering Container ", "Create", Newtonsoft.Json.JsonConvert.SerializeObject(orderingContainer));
                return new Response<int>(OrderingContainerObject.Id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
