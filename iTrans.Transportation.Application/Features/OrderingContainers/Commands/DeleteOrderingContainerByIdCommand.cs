﻿using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.OrderingContainers.Commands
{
    public class DeleteOrderingContainerByIdCommand : IRequest<Response<int>>
    {
        public int Id { get; set; }
        public Guid OrderId { get; set; }
        public class DeleteOrderingContainerByIdCommandHandler : IRequestHandler<DeleteOrderingContainerByIdCommand, Response<int>>
        {
            private readonly IOrderingContainerRepositoryAsync _orderingContainerRepository;
            //private readonly IOrderingContainerAddressRepositoryAsync _orderingContainerAddressRepository;
            public DeleteOrderingContainerByIdCommandHandler(IOrderingContainerRepositoryAsync orderingContainerRepository)//, IOrderingContainerAddressRepositoryAsync orderingContainerAddressRepository)
            {
                _orderingContainerRepository = orderingContainerRepository;
                //_orderingContainerAddressRepository = orderingContainerAddressRepository;
            }
            public async Task<Response<int>> Handle(DeleteOrderingContainerByIdCommand command, CancellationToken cancellationToken)
            {
                var OrderingContainer = (await _orderingContainerRepository.FindByCondition(x => x.Id == command.Id && x.Ordering.Id.Equals(command.OrderId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (OrderingContainer == null)
                {
                    throw new ApiException($"OrderingContainer Not Found.");
                }
                else
                {
                    //(await _orderingContainerAddressRepository.CreateSQLQuery("DELETE Ordering_ContainerAddress where OrderingContainerId = '" + command.Id.ToString() + "'").ConfigureAwait(false)).ExecuteUpdate();
                    await _orderingContainerRepository.DeleteAsync(OrderingContainer);
                    return new Response<int>(OrderingContainer.Id);
                }
            }
        }
    }
}
