﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.OrderingContainer;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;

namespace iTrans.Transportation.Application.Features.OrderingContainers.Queries
{
   public class GetAllOrderingContainerParameter : IRequest<PagedResponse<IEnumerable<OrderingContainerViewModel>>>
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
    }
    public class GetAllOrderingContainerParameterHandler : IRequestHandler<GetAllOrderingContainerParameter, PagedResponse<IEnumerable<OrderingContainerViewModel>>>
    {
        private readonly IOrderingContainerRepositoryAsync _OrderingContainerRepository;
        private readonly IMapper _mapper;
        public GetAllOrderingContainerParameterHandler(IOrderingContainerRepositoryAsync OrderingContainerRepository, IMapper mapper)
        {
            _OrderingContainerRepository = OrderingContainerRepository;
            _mapper = mapper;
        }

        public async Task<PagedResponse<IEnumerable<OrderingContainerViewModel>>> Handle(GetAllOrderingContainerParameter request, CancellationToken cancellationToken)
        {
            var validFilter = _mapper.Map<GetAllOrderingContainerParameter>(request);
            Expression<Func<OrderingContainer, bool>> expression = x => x.ContainerBooking != "";
            var OrderingContainer = await _OrderingContainerRepository.FindByConditionWithPage(expression, validFilter.PageNumber, validFilter.PageSize);
            var OrderingContainerViewModel = _mapper.Map<IEnumerable<OrderingContainerViewModel>>(OrderingContainer);
            return new PagedResponse<IEnumerable<OrderingContainerViewModel>>(OrderingContainerViewModel, validFilter.PageNumber, validFilter.PageSize);
        }
    }
}
