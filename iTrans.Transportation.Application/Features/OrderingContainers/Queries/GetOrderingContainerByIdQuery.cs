﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.OrderingContainer;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.OrderingContainers.Queries
{
   public class GetOrderingContainerByIdQuery : IRequest<Response<OrderingContainerViewModel>>
    {
        public int Id { get; set; }
    }
    public class GetOrderingContainerByIdQueryHandler : IRequestHandler<GetOrderingContainerByIdQuery, Response<OrderingContainerViewModel>>
    {
        private readonly IOrderingContainerRepositoryAsync _OrderingContainerRepository;
        private readonly IMapper _mapper;
        public GetOrderingContainerByIdQueryHandler(IOrderingContainerRepositoryAsync OrderingContainerRepository, IMapper mapper)
        {
            _OrderingContainerRepository = OrderingContainerRepository;
            _mapper = mapper;
        }
        public async Task<Response<OrderingContainerViewModel>> Handle(GetOrderingContainerByIdQuery request, CancellationToken cancellationToken)
        {
            var OrderingContainerObject = (await _OrderingContainerRepository.FindByCondition(x => x.Id.Equals(request.Id)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            return new Response<OrderingContainerViewModel>(_mapper.Map<OrderingContainerViewModel>(OrderingContainerObject));
        }
    }
}
