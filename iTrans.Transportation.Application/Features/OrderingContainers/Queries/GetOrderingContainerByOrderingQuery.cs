﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.Ordering;
using iTrans.Transportation.Application.DTOs.OrderingContainer;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.OrderingContainers.Queries
{
   public class GetOrderingContainerByOrderingQuery : IRequest<Response<IEnumerable<OrderingContainerViewModel>>>
    {
        public int OrderingId { set; get; }
    }
    public class GetOrderingContainerByOrderingQueryHandler : IRequestHandler<GetOrderingContainerByOrderingQuery, Response<IEnumerable<OrderingContainerViewModel>>>
    {
        private readonly IOrderingContainerRepositoryAsync _orderingContainerRepository;
        private readonly IMapper _mapper;
        public GetOrderingContainerByOrderingQueryHandler(IOrderingContainerRepositoryAsync orderingContainerRepository, IMapper mapper)
        {
            _orderingContainerRepository = orderingContainerRepository;
            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<OrderingContainerViewModel>>> Handle(GetOrderingContainerByOrderingQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var OrderingContainer = (await _orderingContainerRepository.FindByCondition(x => x.Ordering.Id.Equals(request.OrderingId)).ConfigureAwait(false)).AsQueryable().ToList();
                var OrderingContainerViewModel = _mapper.Map<IEnumerable<OrderingContainerViewModel>>(OrderingContainer);
                return new Response<IEnumerable<OrderingContainerViewModel>>(OrderingContainerViewModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}