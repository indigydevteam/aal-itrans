﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.OrderingContainer;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.OrderingContainers.Queries
{
   public class GetAllOrderingContainerQuery : IRequest<Response<IEnumerable<OrderingContainerViewModel>>>
    {

    }
    public class GetAllOrderingContainerQueryHandler : IRequestHandler<GetAllOrderingContainerQuery, Response<IEnumerable<OrderingContainerViewModel>>>
    {
        private readonly IOrderingContainerRepositoryAsync _orderingContainerRepository;
        private readonly IMapper _mapper;
        public GetAllOrderingContainerQueryHandler(IOrderingContainerRepositoryAsync OrderingContainerRepository, IMapper mapper)
        {
            _orderingContainerRepository = OrderingContainerRepository;
            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<OrderingContainerViewModel>>> Handle(GetAllOrderingContainerQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var OrderingContainer = await _orderingContainerRepository.GetAllAsync();
                var OrderingContainerViewModel = _mapper.Map<IEnumerable<OrderingContainerViewModel>>(OrderingContainer);
                return new Response<IEnumerable<OrderingContainerViewModel>>(OrderingContainerViewModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
