﻿using System;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.News;
using iTrans.Transportation.Application.Enums;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.News.Queries
{
    public class GetNewsByIdQuery : IRequest<Response<NewsViewModel>>
    {
        public int Id { get; set; }
        public string Domain { get; set; }
    }
    public class GetCustomerByIdQueryHandler : IRequestHandler<GetNewsByIdQuery, Response<NewsViewModel>>
    {
        private readonly INewsRepositoryAsync _newsRepository;
        private readonly IMapper _mapper;

        private string sqlQuery
        {
            get
            {
                return @" SELECT N.ID,N.TITLE,N.DETAIL,'#DNS' + F.FILEPATH AS IMAGE ,N.URL,'#DNS' + D.FILEPATH AS DOCUMENT,D.FILEEXT,D.FILENAME,
                                 N.VIDEOURL,N.STARTDATE,N.TOTALVIEW, N.NEWSTYPE,'#DNS' + V.FILEPATH AS VIDEO, N.ISHIGHLIGHT,N.ISALL,
								 N.TARGETCUSTOMERTYPE,N.TARGETDRIVERTYPE,N.ISSELECTION,N.STARTDATE,N.ENDDATE,N.HLSTARTDATE,N.HLENDDATE,
								 N.ISGUEST,N.ISACTIVE,N.ISNOTIFICATION
	                        FROM NEWS N
	                        JOIN NEWSFILE F ON N.IMAGEFILEID = F.ID
	                        LEFT JOIN NEWSFILE D ON N.FILEID = D.ID
	                        LEFT JOIN NEWSFILE V ON N.VIDEOID = V.ID
	                        WHERE N.ID = ";
            }
        }


        public GetCustomerByIdQueryHandler(INewsRepositoryAsync newsRepository, IMapper mapper)
        {
            _newsRepository = newsRepository;
            _mapper = mapper;
        }
        public async Task<Response<NewsViewModel>> Handle(GetNewsByIdQuery request, CancellationToken cancellationToken)
        {
            var obj = _newsRepository.CreateSQLQuery(sqlQuery.Replace("#DNS", request.Domain) + request.Id).Result.UniqueResult();
            var convert = new GenericMappingObject<NewsViewModel>();
            NewsViewModel newsResult = convert.Mapping(obj);

            var news = (await _newsRepository.FindByCondition(x => x.Id == request.Id).ConfigureAwait(false)).FirstOrDefault();
            news.TotalView += 1;
            await _newsRepository.UpdateAsync(news);

            return new Response<NewsViewModel>(_mapper.Map<NewsViewModel>(newsResult));
        }
    }
}
