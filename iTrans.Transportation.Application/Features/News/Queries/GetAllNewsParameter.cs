﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.News;
using iTrans.Transportation.Application.Enums;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;


namespace iTrans.Transportation.Application.Features.News.Queries
{
    public class GetAllNewsParameter : IRequest<Response<IEnumerable<NewsListViewModel>>>
    {
        public Guid? UserId { get; set; }
        public string UserRole { get; set; }
        public string UserType { get; set; }

        public int PageNumber { get; set; }
        public int PageSize { get; set; }

        public int? Category { get; set; }
        public string Keyword { get; set; }

        public bool IsHighlight { get; set; }
        public string Domain { get; set; }
        public bool IsGuest { get; set; }
    }
    public class GetAllNewsParameterHandler : IRequestHandler<GetAllNewsParameter, Response<IEnumerable<NewsListViewModel>>>
    {
        private readonly INewsRepositoryAsync _newsRepository;
        private readonly INewsFileRepositoryAsync _newsFileRepository;
        private readonly IMapper _mapper;
        public GetAllNewsParameterHandler(INewsRepositoryAsync newsRepository,
            INewsFileRepositoryAsync newsFileRepository,
            IMapper mapper)
        {
            _newsRepository = newsRepository;
            _newsFileRepository = newsFileRepository;
            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<NewsListViewModel>>> Handle(GetAllNewsParameter request, CancellationToken cancellationToken)
        {
            var validFilter = _mapper.Map<GetAllNewsParameter>(request);

            var newsListViewModel = (from n in await _newsRepository.GetSession()
                                     join f in await _newsFileRepository.GetSession() on n.ImageFileId equals f.Id
                                     where !n.IsDelete
                                     select new NewsListViewModel
                                     {
                                         Id = n.Id,
                                         Title = n.Title,
                                         Detail = n.Detail,
                                         ImageFileUrl = request.Domain + f.FilePath,
                                         NewsTypeInt = n.NewsType,
                                         NewsType = Enum.GetName(typeof(NewsType), n.NewsType),
                                         TotalView = n.TotalView,
                                         Published = n.Created,
                                         TargetCustomerType = n.TargetCustomerType,
                                         TargetDriverType = n.TargetDriverType,
                                         IsAll = n.IsAll,
                                         IsHighlight = n.IsHighlight,
                                         StartDate = n.StartDate,
                                         EndDate = n.EndDate,
                                         HLStartDate = n.HLStartDate,
                                         HLEndDate = n.HLEndDate,
                                         IsGuest = n.IsGuest,
                                         IsNotification = n.IsNotification,
                                         IsActive = n.IsActive,
                                         Modified = n.Modified,
                                         ModifiedBy = n.ModifiedBy
                                     });

            DateTime now = DateTime.UtcNow;

            if (request.Category != 0 && request.Category != null)
            {
                newsListViewModel = newsListViewModel.Where(x => x.NewsTypeInt == request.Category);
            }

            if (!string.IsNullOrEmpty(request.Keyword))
            {
                string p = Microsoft.Security.Application.Encoder.HtmlEncode(request.Keyword, true);

                newsListViewModel = newsListViewModel.Where(x => x.Title.Contains(request.Keyword) || x.Detail.Contains(request.Keyword) || x.Detail.Contains(p));
            }

            if (request.IsHighlight)
            {
                newsListViewModel = newsListViewModel.Where(x => now >= x.HLStartDate.Value && now <= x.HLEndDate.Value);
            }
            else if(request.UserRole.ToUpper() != "ADMIN")
            {
                newsListViewModel = newsListViewModel.Where(x => now >= x.StartDate.Value && now <= x.EndDate.Value);
            }

            if (request.UserRole == null)
            {
                newsListViewModel = newsListViewModel.Where(x => x.IsGuest);

            }
            else if (request.UserRole.ToUpper() != "ADMIN")
            {

                foreach (var item in newsListViewModel)
                {
                    if (item.IsAll)
                        continue;

                    bool IsValidRole = false;

                    if (!string.IsNullOrEmpty(item.TargetCustomerType))
                    {
                        string[] allowType = item.TargetCustomerType.Split(',');

                        IsValidRole = request.UserRole.ToUpper() == "CUSTOMER" && allowType.Contains(request.UserType);
                    }


                    if (!string.IsNullOrEmpty(item.TargetDriverType))
                    {
                        string[] allowType = item.TargetDriverType.Split(',');

                        IsValidRole = request.UserRole.ToUpper() == "DRIVER" && allowType.Contains(request.UserType);
                    }

                    if (!IsValidRole)
                        newsListViewModel = newsListViewModel.Where(x => x.Id != item.Id);

                    newsListViewModel = newsListViewModel.Where(x => x.IsActive);
                }
            }

            int itemCount = newsListViewModel.Count();
            newsListViewModel = newsListViewModel.OrderByDescending(x => x.Published).Skip((validFilter.PageNumber - 1) * validFilter.PageSize).Take(validFilter.PageSize);
             
            return new PagedResponse<IEnumerable<NewsListViewModel>>(newsListViewModel, validFilter.PageNumber, validFilter.PageSize, itemCount);
        }
    }
}
