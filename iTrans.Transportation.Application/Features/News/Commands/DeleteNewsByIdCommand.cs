﻿using AutoMapper;
using iTrans.Transportation.Application.Enums;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Interfaces.Services;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using iTrans.Transportation.Domain.Entities;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.News.Commands
{
    public partial class DeleteNewsByIdCommand : IRequest<Response<int>>
    {
        public int Id { get; set; }
        public Guid? UserId { get; set; }
    }

    public class DeleteNewsCommandHandler : IRequestHandler<DeleteNewsByIdCommand, Response<int>>
    {
        private readonly INewsRepositoryAsync _newsRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;

        public DeleteNewsCommandHandler(INewsRepositoryAsync newsRepository, IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _newsRepository = newsRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(DeleteNewsByIdCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var news = (await _newsRepository.FindByCondition(x => x.Id == request.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();

                if (news == null)
                {
                    throw new ApiException($"News Not Found.");
                }
                else
                {
                    news.IsDelete = true; 
                    news.Modified = DateTime.UtcNow;
                    news.ModifiedBy = request.UserId.ToString(); 
                    await _newsRepository.UpdateAsync(news);

                    var log = new CreateAppLog(_applicationLogRepository);
                    log.Create("news", "delete news", this.ToString(), Newtonsoft.Json.JsonConvert.SerializeObject(news));

                    return new Response<int>(news.Id);
                }
                 
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
