﻿using AutoMapper;
using iTrans.Transportation.Application.Enums;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Interfaces.Services;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using iTrans.Transportation.Domain.Entities;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.News.Commands
{
    public partial class UpdateNewsCommand : IRequest<Response<int>>
    {
        public Guid? UserId { get; set; }
        public int Id { get; set; }
        public string Title { get; set; }
        public string Detail { get; set; }

        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }

        public DateTime? HLStartDate { get; set; }
        public DateTime? HLEndDate { get; set; }

        public bool IsActive { get; set; }
        public bool IsHighlight { get; set; }
        public bool IsNotification { get; set; }

        public IFormFile ImageFile { get; set; }
        public IFormFile VideoFile { get; set; }
        public IFormFile DocumentFile { get; set; }

        public string Url { get; set; }
        public string VideoUrl { get; set; }

        public int NewsType { get; set; }
        public bool IsAll { get; set; }

        public string TargetCustomerType { get; set; }
        public string TargetDriverType { get; set; }

        public string ContentDirectory { get; set; }
        public bool IsGuest { get; set; }

        public string Token { get; set; }
        public string NotificationEndpoint { get; set; }

        public bool IsRemoveDocument { get; set; }
        public bool IsRemoveVideo { get; set; }
        public bool IsSubmitWithNotification { get; set; }
    }

    public class EditNewsCommandHandler : IRequestHandler<UpdateNewsCommand, Response<int>>
    {
        private readonly INewsRepositoryAsync _newsRepository;
        private readonly INewsFileRepositoryAsync _newsFileRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;

        public EditNewsCommandHandler(INewsRepositoryAsync newsRepository,
            INewsFileRepositoryAsync newsFileRepository,
            IApplicationLogRepositoryAsync applicationLogRepository,
            IMapper mapper)
        {
            _newsRepository = newsRepository;
            _newsFileRepository = newsFileRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(UpdateNewsCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var news = (await _newsRepository.FindByCondition(x => x.Id == request.Id).ConfigureAwait(false)).FirstOrDefault();

                if (news != null)
                {
                    if (request.ImageFile != null)
                    {
                        var imgName = DateTime.Now.ToString("yyyymmddMMss") + "_" + Path.GetFileName(request.ImageFile.FileName);
                        var imgFullPath = Path.Combine(request.ContentDirectory, "news", "image", imgName);

                        using (var fileStream = new FileStream(imgFullPath, FileMode.Create))
                        {
                            await request.ImageFile.CopyToAsync(fileStream);
                        }

                        news.ImageFileId = Guid.NewGuid();

                        var imgFile = new NewsFile
                        {
                            Id = news.ImageFileId.Value,
                            ContentType = request.ImageFile.ContentType,
                            DocumentType = "image",
                            FileEXT = Path.GetExtension(imgFullPath),
                            FileName = imgName,
                            FilePath = "/news/image/" + imgName,
                            CreatedBy = request.UserId.ToString(),
                            ModifiedBy = request.UserId.ToString(),
                            Created = DateTime.UtcNow,
                            Modified = DateTime.UtcNow
                        };

                        await _newsFileRepository.AddAsync(imgFile);
                    }

                    if (request.VideoFile != null)
                    {
                        var videoName = DateTime.Now.ToString("yyyymmddMMss") + "_" + Path.GetFileName(request.VideoFile.FileName);
                        var videoFullPath = Path.Combine(request.ContentDirectory, "news", "video", videoName);

                        using (var fileStream = new FileStream(videoFullPath, FileMode.Create))
                        {
                            await request.VideoFile.CopyToAsync(fileStream);
                        }

                        news.VideoId = Guid.NewGuid();

                        var videoFile = new NewsFile
                        {
                            Id = news.VideoId.Value,
                            ContentType = request.VideoFile.ContentType,
                            DocumentType = "video",
                            FileEXT = Path.GetExtension(videoFullPath),
                            FileName = videoName,
                            FilePath = "/news/video/" + videoName,
                            CreatedBy = request.UserId.ToString(),
                            ModifiedBy = request.UserId.ToString(),
                            Created = DateTime.UtcNow,
                            Modified = DateTime.UtcNow
                        };

                        await _newsFileRepository.AddAsync(videoFile);
                    }

                    if (request.DocumentFile != null)
                    {
                        var documentName = DateTime.Now.ToString("yyyymmddMMss") + "_" + Path.GetFileName(request.DocumentFile.FileName);
                        var documentFullPath = Path.Combine(request.ContentDirectory, "news", "document", documentName);

                        using (var fileStream = new FileStream(documentFullPath, FileMode.Create))
                        {
                            await request.DocumentFile.CopyToAsync(fileStream);
                        }

                        news.FileId = Guid.NewGuid();

                        var videoFile = new NewsFile
                        {
                            Id = news.FileId.Value,
                            ContentType = request.DocumentFile.ContentType,
                            DocumentType = "video",
                            FileEXT = Path.GetExtension(documentFullPath),
                            FileName = documentName,
                            FilePath = "/news/document/" + documentName,
                            CreatedBy = request.UserId.ToString(),
                            ModifiedBy = request.UserId.ToString(),
                            Created = DateTime.UtcNow,
                            Modified = DateTime.UtcNow
                        };

                        await _newsFileRepository.AddAsync(videoFile);
                    }


                    if (request.IsRemoveDocument)
                        news.FileId = null;

                    if (request.IsRemoveVideo)
                        news.VideoId = null;


                    news.Title = request.Title;
                    news.Detail = request.Detail; 
                    news.StartDate = request.StartDate;
                    news.EndDate = request.EndDate; 
                    news.HLStartDate = request.HLStartDate;
                    news.HLEndDate = request.HLEndDate; 
                    news.TargetCustomerType = request.TargetCustomerType;
                    news.TargetDriverType = request.TargetDriverType; 
                    news.NewsType = request.NewsType;
                    news.Url = request.Url;
                    news.VideoUrl = request.VideoUrl;
                    news.IsAll = request.IsAll;
                    news.IsHighlight = request.IsHighlight;
                    news.IsNotification = request.IsNotification;
                    news.IsActive = request.IsActive;
                    news.IsGuest = request.IsGuest; 
                    news.ModifiedBy = request.UserId.ToString();
                    news.Modified = DateTime.UtcNow;
                     
                    var dataListObject = await _newsRepository.UpdateAsync(news);

                    var log = new CreateAppLog(_applicationLogRepository);
                    log.CreateLog("News", "News", "Edit", Newtonsoft.Json.JsonConvert.SerializeObject(news),request.UserId.ToString());

                    if (request.IsSubmitWithNotification && request.IsNotification)
                    {
                        var http = new HttpNotificationManager(request.NotificationEndpoint);
                        http.SendNotification(news.Title, news.Detail, Enum.GetName(typeof(ServiceCode), request.NewsType), null,
                                              request.UserId.Value, news.Id.ToString(), request.Token, request.TargetCustomerType, request.TargetDriverType, request.IsAll);

                    }

                    return new Response<int>(news.Id);
                } 

                return new Response<int>(0);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}

