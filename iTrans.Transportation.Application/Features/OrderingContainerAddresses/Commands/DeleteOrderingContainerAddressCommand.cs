﻿using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
namespace iTrans.Transportation.Application.Features.OrderingContainerAddresses.Commands
{
   public class DeleteOrderingContainerAddressCommand : IRequest<Response<int>>
    {
        public int Id { get; set; }
        public class DeleteOrderingContainerAddressCommandHandler : IRequestHandler<DeleteOrderingContainerAddressCommand, Response<int>>
        {
            private readonly IOrderingContainerAddressRepositoryAsync _orderingContainerAddressRepository;
            public DeleteOrderingContainerAddressCommandHandler(IOrderingContainerAddressRepositoryAsync orderingContainerAddressRepository)
            {
                _orderingContainerAddressRepository = orderingContainerAddressRepository;
            }
            public async Task<Response<int>> Handle(DeleteOrderingContainerAddressCommand command, CancellationToken cancellationToken)
            {
                var OrderingContainerAddress = (await _orderingContainerAddressRepository.FindByCondition(x => x.Id == command.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (OrderingContainerAddress == null)
                {
                    throw new ApiException($"OrderingContainerAddress Not Found.");
                }
                else
                {
                    await _orderingContainerAddressRepository.DeleteAsync(OrderingContainerAddress);
                    return new Response<int>(OrderingContainerAddress.Id);
                }
            }
        }
    }
}
