﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;

namespace iTrans.Transportation.Application.Features.OrderingContainerAddresses.Commands
{
   public class UpdateOrderingContainerAddressCommand : IRequest<Response<int>>
    {
        public  int Id { get; set; }
        public  int OrderingContainerId { get; set; }
        public  int LoadingCountryId { get; set; }
        public  int LoadingProvinceId { get; set; }
        public  int LoadingDistrictId { get; set; }
        public  int LoadingSubdistrictId { get; set; }
        public  string LoadingPostCode { get; set; }
        public  string LoadingRoad { get; set; }
        public  string LoadingAlley { get; set; }
        public  string LoadingAddress { get; set; }
        public  DateTime ETD { get; set; }
        public  string LoadingMaps { get; set; }
        public int DestinationCountryId { get; set; }
        public int DestinationProvinceId { get; set; }
        public int DestinationDistrictId { get; set; }
        public int DestinationSubdistrictId { get; set; }
        public string DestinationPostCode { get; set; }
        public string DestinationRoad { get; set; }
        public string DestinationAlley { get; set; }
        public string DestinationAddress { get; set; }
        public DateTime ETA { get; set; }
        public string DestinationMaps { get; set; }
        public  string FeederVessel { get; set; }
        public  DateTime TStime { get; set; }
        public  string MotherVessel { get; set; }
        public  DateTime CPSDate { get; set; }
        public  string CPSDetail { get; set; }
        public  DateTime CYDate { get; set; }
        public  DateTime ReturnDate { get; set; }
        public  string ReturnDetail { get; set; }
        public  DateTime ClosingTime { get; set; }
        public  DateTime CutOffVGMTime { get; set; }
        public  DateTime CutOffSITime { get; set; }
        public  string Agent { get; set; }

    }

    public class UpdateOrderingContainerAddressCommandHandler : IRequestHandler<UpdateOrderingContainerAddressCommand, Response<int>>
    {
        private readonly IOrderingContainerAddressRepositoryAsync _orderingContainerAddressRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;

        public UpdateOrderingContainerAddressCommandHandler(IOrderingContainerAddressRepositoryAsync orderingContainerAddressRepository,IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _orderingContainerAddressRepository = orderingContainerAddressRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(UpdateOrderingContainerAddressCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var OrderingContainerAddress = (await _orderingContainerAddressRepository.FindByCondition(x => x.Id == request.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (OrderingContainerAddress == null)
                {
                    throw new ApiException($"OrderingContainerAddress Not Found.");
                }
                else
                {
                    //OrderingContainerAddress.OrderingContainerId = request.OrderingContainerId;
                    //OrderingContainerAddress.LoadingCountry = request.LoadingCountryId;
                    //OrderingContainerAddress.LoadingProvince = request.LoadingProvinceId;
                    //OrderingContainerAddress.LoadingDistrict = request.LoadingDistrictId;
                    //OrderingContainerAddress.LoadingSubdistrict = request.LoadingSubdistrictId;
                    OrderingContainerAddress.LoadingPostCode = request.LoadingPostCode;
                    OrderingContainerAddress.LoadingRoad = request.LoadingRoad;
                    OrderingContainerAddress.LoadingAlley = request.LoadingAlley;
                    OrderingContainerAddress.LoadingAddress = request.LoadingAddress;
                    OrderingContainerAddress.ETD = request.ETD;
                    OrderingContainerAddress.LoadingMaps = request.LoadingMaps;
                    //OrderingContainerAddress.DestinationCountryId = request.DestinationCountryId;
                    //OrderingContainerAddress.DestinationProvinceId = request.DestinationProvinceId;
                    //OrderingContainerAddress.DestinationDistrictId = request.DestinationDistrictId;
                    //OrderingContainerAddress.DestinationSubdistrictId = request.DestinationSubdistrictId;
                    OrderingContainerAddress.DestinationPostCode = request.DestinationPostCode;
                    OrderingContainerAddress.DestinationRoad = request.DestinationRoad;
                    OrderingContainerAddress.DestinationAlley = request.DestinationAlley;
                    OrderingContainerAddress.DestinationAddress = request.DestinationAddress;
                    OrderingContainerAddress.ETA = request.ETA;
                    OrderingContainerAddress.DestinationMaps = request.DestinationMaps;
                    OrderingContainerAddress.FeederVessel = request.FeederVessel;
                    OrderingContainerAddress.TStime = request.TStime;
                    OrderingContainerAddress.MotherVessel = request.MotherVessel;
                    OrderingContainerAddress.CPSDate = request.CPSDate;
                    OrderingContainerAddress.CPSDetail = request.CPSDetail;
                    OrderingContainerAddress.CYDate = request.CYDate;
                    OrderingContainerAddress.ReturnDate = request.ReturnDate;
                    OrderingContainerAddress.ReturnDetail = request.ReturnDetail;
                    OrderingContainerAddress.ClosingTime = request.ClosingTime;
                    OrderingContainerAddress.CutOffVGMTime = request.CutOffVGMTime;
                    OrderingContainerAddress.CutOffSITime = request.CutOffSITime;
                    OrderingContainerAddress.Agent = request.Agent;
                    OrderingContainerAddress.Modified = DateTime.UtcNow;

                    await _orderingContainerAddressRepository.UpdateAsync(OrderingContainerAddress);
                    var log = new CreateAppLog(_applicationLogRepository);
                    log.Create("ordering", "ordering containeraddress ", this.ToString(), Newtonsoft.Json.JsonConvert.SerializeObject(OrderingContainerAddress));
                    return new Response<int>(OrderingContainerAddress.Id);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}