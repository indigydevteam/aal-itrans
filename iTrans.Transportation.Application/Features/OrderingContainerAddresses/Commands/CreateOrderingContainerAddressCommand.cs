﻿using AutoMapper;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Interfaces.Services;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using iTrans.Transportation.Domain.Entities;
using MediatR;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.OrderingContainerAddresses.Commands
{
 public partial  class CreateOrderingContainerAddressCommand : IRequest<Response<int>>
    {
        
        public virtual int OrderingContainerId { get; set; }
        public virtual int Country { get; set; }
        public virtual int Province { get; set; }
        public virtual int District { get; set; }
        public virtual int Subdistrict { get; set; }
        public virtual string PostCode { get; set; }
        public virtual string Road { get; set; }
        public virtual string Alley { get; set; }
        public virtual string Address { get; set; }
        public virtual string Branch { get; set; }
        public virtual string AddressType { get; set; }
        public virtual DateTime Date { get; set; }
        public virtual string Maps { get; set; }
        public virtual string Note { get; set; }
        public virtual string FeederVessel { get; set; }
        public virtual DateTime TStime { get; set; }
        public virtual string MotherVessel { get; set; }
        public virtual DateTime CPSDate { get; set; }
        public virtual string CPSDetail { get; set; }
        public virtual DateTime CYDate { get; set; }
        public virtual DateTime ReturnDate { get; set; }
        public virtual string ReturnDetail { get; set; }
        public virtual DateTime ClosingTime { get; set; }
        public virtual DateTime CutOffVGMTime { get; set; }
        public virtual DateTime CutOffSITime { get; set; }
        public virtual string Agent { get; set; }
    }
    public class CreateOrderingContainerAddressCommandHandler : IRequestHandler<CreateOrderingContainerAddressCommand, Response<int>>
    {
        private readonly IOrderingContainerAddressRepositoryAsync _orderingContainerAddressRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;
        public CreateOrderingContainerAddressCommandHandler(IOrderingContainerAddressRepositoryAsync orderingContainerAddressRepository,IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _orderingContainerAddressRepository = orderingContainerAddressRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(CreateOrderingContainerAddressCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var OrderingContainerAddress = _mapper.Map<OrderingContainerAddress>(request);
                OrderingContainerAddress.Created = DateTime.UtcNow;
                OrderingContainerAddress.Modified = DateTime.UtcNow;
                //title.CreatedBy = "xxxxxxx";
                //title.ModifiedBy = "xxxxxxx";
                var OrderingContainerAddressObject = await _orderingContainerAddressRepository.AddAsync(OrderingContainerAddress);
                var log = new CreateAppLog(_applicationLogRepository);
                log.Create("ordering", "ordering containeraddress ", this.ToString(), Newtonsoft.Json.JsonConvert.SerializeObject(OrderingContainerAddress));
                return new Response<int>(OrderingContainerAddressObject.Id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
