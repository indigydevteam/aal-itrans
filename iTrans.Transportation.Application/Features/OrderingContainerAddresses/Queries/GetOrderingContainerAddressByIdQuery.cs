﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.OrderingContainerAddress;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.OrderingContainerAddresses.Queries
{
   public class GetOrderingContainerAddressByIdQuery : IRequest<Response<OrderingContainerAddressViewModel>>
    {
        public int Id { get; set; }
    }
    public class GetOrderingContainerAddressByIdQueryHandler : IRequestHandler<GetOrderingContainerAddressByIdQuery, Response<OrderingContainerAddressViewModel>>
    {
        private readonly IOrderingContainerAddressRepositoryAsync _orderingContainerAddressRepository;
        private readonly IMapper _mapper;
        public GetOrderingContainerAddressByIdQueryHandler(IOrderingContainerAddressRepositoryAsync orderingContainerAddressRepository, IMapper mapper)
        {
            _orderingContainerAddressRepository = orderingContainerAddressRepository;
            _mapper = mapper;
        }
        public async Task<Response<OrderingContainerAddressViewModel>> Handle(GetOrderingContainerAddressByIdQuery request, CancellationToken cancellationToken)
        {
            var DataObject = (await _orderingContainerAddressRepository.FindByCondition(x => x.Id.Equals(request.Id)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            return new Response<OrderingContainerAddressViewModel>(_mapper.Map<OrderingContainerAddressViewModel>(DataObject));
        }
    }
}