﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.OrderingContainer;
using iTrans.Transportation.Application.DTOs.OrderingContainerAddress;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.OrderingContainerContainerAddresses.Queries
{
   public class GetOrderingContainerAddressByOrderingContainerQuery : IRequest<Response<IEnumerable<OrderingContainerAddressViewModel>>>
    {
        public int OrderingContainerId { set; get; }
    }
    public class GetOrderingContainerAddressByOrderingContainerQueryHandler : IRequestHandler<GetOrderingContainerAddressByOrderingContainerQuery, Response<IEnumerable<OrderingContainerAddressViewModel>>>
    {
        private readonly IOrderingContainerAddressRepositoryAsync _orderingContainerAddressRepository;
        private readonly IMapper _mapper;
        public GetOrderingContainerAddressByOrderingContainerQueryHandler(IOrderingContainerAddressRepositoryAsync orderingContainerAddressRepository, IMapper mapper)
        {
            _orderingContainerAddressRepository = orderingContainerAddressRepository;
            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<OrderingContainerAddressViewModel>>> Handle(GetOrderingContainerAddressByOrderingContainerQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var OrderingContainerAddress = (await _orderingContainerAddressRepository.FindByCondition(x => x.OrderingContainer.Id.Equals(request.OrderingContainerId)).ConfigureAwait(false)).AsQueryable().ToList();
                var OrderingContainerAddressViewModel = _mapper.Map<IEnumerable<OrderingContainerAddressViewModel>>(OrderingContainerAddress);
                return new Response<IEnumerable<OrderingContainerAddressViewModel>>(OrderingContainerAddressViewModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
