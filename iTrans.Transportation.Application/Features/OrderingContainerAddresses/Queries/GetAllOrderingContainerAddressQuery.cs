﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.OrderingContainerAddress;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.OrderingContainerAddresses.Queries
{
  public class GetAllOrderingContainerAddressQuery : IRequest<Response<IEnumerable<OrderingContainerAddressViewModel>>>
    {

    }
    public class GetAllOrderingContainerAddressQueryHandler : IRequestHandler<GetAllOrderingContainerAddressQuery, Response<IEnumerable<OrderingContainerAddressViewModel>>>
    {
        private readonly IOrderingContainerAddressRepositoryAsync _orderingContainerAddressRepository;
        private readonly IMapper _mapper;
        public GetAllOrderingContainerAddressQueryHandler(IOrderingContainerAddressRepositoryAsync OrderingContainerAddressRepository, IMapper mapper)
        {
            _orderingContainerAddressRepository = OrderingContainerAddressRepository;
            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<OrderingContainerAddressViewModel>>> Handle(GetAllOrderingContainerAddressQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var OrderingContainerAddress = await _orderingContainerAddressRepository.GetAllAsync();
                var OrderingContainerAddressViewModel = _mapper.Map<IEnumerable<OrderingContainerAddressViewModel>>(OrderingContainerAddress);
                return new Response<IEnumerable<OrderingContainerAddressViewModel>>(OrderingContainerAddressViewModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
