﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.OrderingContainerAddress;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;

namespace iTrans.Transportation.Application.Features.OrderingContainerAddresses.Queries
{
   public class GetAllOrderingContainerAddressParameter : IRequest<PagedResponse<IEnumerable<OrderingContainerAddressViewModel>>>
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
    }
    public class GetAllOrderingContainerAddressParameterHandler : IRequestHandler<GetAllOrderingContainerAddressParameter, PagedResponse<IEnumerable<OrderingContainerAddressViewModel>>>
    {
        private readonly IOrderingContainerAddressRepositoryAsync _OrderingContainerAddressRepository;
        private readonly IMapper _mapper;
        public GetAllOrderingContainerAddressParameterHandler(IOrderingContainerAddressRepositoryAsync OrderingContainerAddressRepository, IMapper mapper)
        {
            _OrderingContainerAddressRepository = OrderingContainerAddressRepository;
            _mapper = mapper;
        }

        public async Task<PagedResponse<IEnumerable<OrderingContainerAddressViewModel>>> Handle(GetAllOrderingContainerAddressParameter request, CancellationToken cancellationToken)
        {
            var validFilter = _mapper.Map<GetAllOrderingContainerAddressParameter>(request);
            Expression<Func<OrderingContainerAddress, bool>> expression = x => x.LoadingPostCode != "";
            var OrderingContainerAddress = await _OrderingContainerAddressRepository.FindByConditionWithPage(expression, validFilter.PageNumber, validFilter.PageSize);
            var OrderingContainerAddressViewModel = _mapper.Map<IEnumerable<OrderingContainerAddressViewModel>>(OrderingContainerAddress);
            return new PagedResponse<IEnumerable<OrderingContainerAddressViewModel>>(OrderingContainerAddressViewModel, validFilter.PageNumber, validFilter.PageSize);
        }
    }
}
