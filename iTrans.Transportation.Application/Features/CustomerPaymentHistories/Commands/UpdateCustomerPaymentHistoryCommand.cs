﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;

namespace iTrans.Transportation.Application.Features.CustomerPaymentHistorys.Commands
{
    public class UpdateCustomerPaymentHistoryCommand : IRequest<Response<int>>
    {
        public int Id { get; set; }
        public int PaymentId { get; set; }
        public string Detail { get; set; }
    }
    public class UpdateCustomerPaymentHistoryCommandHandler : IRequestHandler<UpdateCustomerPaymentHistoryCommand, Response<int>>
    {
        private readonly ICustomerPaymentHistoryRepositoryAsync _customerPaymentHistoryRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;

        public UpdateCustomerPaymentHistoryCommandHandler(ICustomerPaymentHistoryRepositoryAsync customerPaymentHistoryRepository, IApplicationLogRepositoryAsync applicationLogRepository,IMapper mapper)
        {
            _customerPaymentHistoryRepository = customerPaymentHistoryRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(UpdateCustomerPaymentHistoryCommand request, CancellationToken cancellationToken)
        {
            var customerPaymentHistory = (await _customerPaymentHistoryRepository.FindByCondition(x => x.Id == request.Id && x.Payment.Id.Equals(request.PaymentId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            if (customerPaymentHistory == null)
            {
                throw new ApiException($"Payment History Not Found.");
            }
            else
            {
                //customerPaymentHistory.CustomerId = request.CustomerId;
                customerPaymentHistory.Detail = request.Detail;
                customerPaymentHistory.Modified = DateTime.UtcNow;

                await _customerPaymentHistoryRepository.UpdateAsync(customerPaymentHistory);
                var log = new CreateAppLog(_applicationLogRepository);
                log.Create("Customer", "Customer PaymentHistory", "Edit", Newtonsoft.Json.JsonConvert.SerializeObject(request));
                return new Response<int>(customerPaymentHistory.Id);
            }
        }
    }
}
