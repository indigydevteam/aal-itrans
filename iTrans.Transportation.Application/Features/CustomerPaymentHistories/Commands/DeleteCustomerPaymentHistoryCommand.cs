﻿using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
namespace iTrans.Transportation.Application.Features.CustomerPaymentHistorys.Commands
{
    public class DeleteCustomerPaymentHistoryCommand : IRequest<Response<int>>
    {
        public int Id { get; set; }
        public int PaymentId { get; set; }
        public class DeleteCustomerPaymentHistoryCommandHandler : IRequestHandler<DeleteCustomerPaymentHistoryCommand, Response<int>>
        {
            private readonly ICustomerPaymentHistoryRepositoryAsync _customerPaymentHistoryRepository;
            public DeleteCustomerPaymentHistoryCommandHandler(ICustomerPaymentHistoryRepositoryAsync customerPaymentHistoryRepository)
            {
                _customerPaymentHistoryRepository = customerPaymentHistoryRepository;
            }
            public async Task<Response<int>> Handle(DeleteCustomerPaymentHistoryCommand command, CancellationToken cancellationToken)
            {
                var customerPaymentHistory = (await _customerPaymentHistoryRepository.FindByCondition(x => x.Id == command.Id && x.Payment.Id.Equals(command.PaymentId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (customerPaymentHistory == null)
                {
                    throw new ApiException($"Payment History Not Found.");
                }
                else
                {
                    await _customerPaymentHistoryRepository.DeleteAsync(customerPaymentHistory);
                    return new Response<int>(customerPaymentHistory.Id);
                }
            }
        }
    }
}
