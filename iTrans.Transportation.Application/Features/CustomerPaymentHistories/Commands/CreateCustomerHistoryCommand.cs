﻿using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Interfaces.Services;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using iTrans.Transportation.Domain.Entities;
using MediatR;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.CustomerPaymentHistorys.Commands
{
    public partial class CreateCustomerPaymentHistoryCommand : IRequest<Response<int>>
    {
        public Guid CustomerId { get; set; }
        public int PaymentId { get; set; }
        public string Detail { get; set; }

    }
    public class CreateCustomerPaymentHistoryCommandHandler : IRequestHandler<CreateCustomerPaymentHistoryCommand, Response<int>>
    {
        private readonly ICustomerPaymentRepositoryAsync _customerPaymentRepository;
        private readonly ICustomerPaymentHistoryRepositoryAsync _customerPaymentHistoryRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;
        public CreateCustomerPaymentHistoryCommandHandler(ICustomerPaymentRepositoryAsync customerPaymentRepository,ICustomerPaymentHistoryRepositoryAsync customerPaymentHistoryRepository, IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _customerPaymentRepository = customerPaymentRepository;
            _customerPaymentHistoryRepository = customerPaymentHistoryRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(CreateCustomerPaymentHistoryCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var payment = (await _customerPaymentRepository.FindByCondition(x => x.Id.Equals(request.PaymentId) && x.Customer.Id.Equals(request.CustomerId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (payment == null)
                {
                    throw new ApiException($"Playment Not Found.");
                }
                var customerPaymentHistory = _mapper.Map<CustomerPaymentHistory>(request);
                customerPaymentHistory.Payment = payment;
                customerPaymentHistory.Created = DateTime.UtcNow;
                customerPaymentHistory.Modified = DateTime.UtcNow;
                var customerPaymentHistoryObject = await _customerPaymentHistoryRepository.AddAsync(customerPaymentHistory);
                var log = new CreateAppLog(_applicationLogRepository);
                log.Create("Customer", "Customer PaymentHistory", "Create", Newtonsoft.Json.JsonConvert.SerializeObject(request));
                return new Response<int>(customerPaymentHistoryObject.Id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
