﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.CustomerPaymentHistory;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.CustomerPaymentHistoryes.Queries
{
    public class GetCustomerPaymentHistoryByIdQuery : IRequest<Response<CustomerPaymentHistoryViewModel>>
    {
        public int PaymentId { get; set; }
        public int Id { get; set; }

    }
    public class GetCustomerPaymentHistoryByIdQueryHandler : IRequestHandler<GetCustomerPaymentHistoryByIdQuery, Response<CustomerPaymentHistoryViewModel>>
    {
        private readonly ICustomerPaymentHistoryRepositoryAsync _customerPaymentHistoryRepository;
        private readonly IMapper _mapper;
        public GetCustomerPaymentHistoryByIdQueryHandler(ICustomerPaymentHistoryRepositoryAsync customerPaymentHistoryRepository, IMapper mapper)
        {
            _customerPaymentHistoryRepository = customerPaymentHistoryRepository;
            _mapper = mapper;
        }
        public async Task<Response<CustomerPaymentHistoryViewModel>> Handle(GetCustomerPaymentHistoryByIdQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var customerObject = (await _customerPaymentHistoryRepository.FindByCondition(x => x.Id.Equals(request.Id) && x.Payment.Id.Equals(request.PaymentId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                return new Response<CustomerPaymentHistoryViewModel>(_mapper.Map<CustomerPaymentHistoryViewModel>(customerObject));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
