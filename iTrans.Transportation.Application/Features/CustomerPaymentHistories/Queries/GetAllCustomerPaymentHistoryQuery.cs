﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.CustomerPaymentHistory;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.CustomerPaymentHistorys.Queries
{
    public class GetAllCustomerPaymentHistoryQuery : IRequest<Response<IEnumerable<CustomerPaymentHistoryViewModel>>>
    {

    }
    public class GetAllCustomerPaymentHistoryQueryHandler : IRequestHandler<GetAllCustomerPaymentHistoryQuery, Response<IEnumerable<CustomerPaymentHistoryViewModel>>>
    {
        private readonly ICustomerPaymentHistoryRepositoryAsync _customerPaymentHistoryRepository;
        private readonly IMapper _mapper;
        public GetAllCustomerPaymentHistoryQueryHandler(ICustomerPaymentHistoryRepositoryAsync customerPaymentHistoryRepository, IMapper mapper)
        {
            _customerPaymentHistoryRepository = customerPaymentHistoryRepository;
            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<CustomerPaymentHistoryViewModel>>> Handle(GetAllCustomerPaymentHistoryQuery request, CancellationToken cancellationToken)
        {
            var customer = await _customerPaymentHistoryRepository.GetAllAsync();
            var customerPaymentHistoryViewModel = _mapper.Map<IEnumerable<CustomerPaymentHistoryViewModel>>(customer);
            return new Response<IEnumerable<CustomerPaymentHistoryViewModel>>(customerPaymentHistoryViewModel);
        }
    }
}
