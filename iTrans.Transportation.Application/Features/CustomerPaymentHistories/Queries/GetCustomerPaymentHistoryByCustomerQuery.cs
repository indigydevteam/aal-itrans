﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.CustomerPaymentHistory;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.CustomerPaymentHistoryes.Queries
{
    public class GetCustomerPaymentHistoryByCustomerQuery : IRequest<Response<IEnumerable<CustomerPaymentHistoryViewModel>>>
    {
        public Guid CustomerId { get; set; }
    }
    public class GetCustomerPaymentHistoryByCustomerQueryHandler : IRequestHandler<GetCustomerPaymentHistoryByCustomerQuery, Response<IEnumerable<CustomerPaymentHistoryViewModel>>>
    {
        private readonly ICustomerPaymentHistoryRepositoryAsync _customerPaymentHistoryRepository;
        private readonly IMapper _mapper;
        public GetCustomerPaymentHistoryByCustomerQueryHandler(ICustomerPaymentHistoryRepositoryAsync customerPaymentHistoryRepository, IMapper mapper)
        {
            _customerPaymentHistoryRepository = customerPaymentHistoryRepository;
            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<CustomerPaymentHistoryViewModel>>> Handle(GetCustomerPaymentHistoryByCustomerQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var customerPaymentHistory = (await _customerPaymentHistoryRepository.FindByCondition(x => x.Payment.Customer.Id.Equals(request.CustomerId)).ConfigureAwait(false)).AsQueryable().ToList();
                var customerPaymentHistoryViewModel = _mapper.Map<IEnumerable<CustomerPaymentHistoryViewModel>>(customerPaymentHistory);
                return new Response<IEnumerable<CustomerPaymentHistoryViewModel>>(customerPaymentHistoryViewModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
