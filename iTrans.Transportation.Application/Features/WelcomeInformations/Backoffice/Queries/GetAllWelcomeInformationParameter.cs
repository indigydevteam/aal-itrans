﻿using AutoMapper;
using iTrans.Transportation.Application.DTOs.Backoffice.WelcomeInformation;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;


namespace iTrans.Transportation.Application.Features.WelcomeInformations.Backoffice.Queries
{
   public class GetAllWelcomeInformationParameter : IRequest<PagedResponse<IEnumerable<WelcomeInformationViewModel>>>
    {
        public string Search { set; get; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public int Active { set; get; }
        public string Column { set; get; }
    }
    public class GetAllWelcomeInformationHandler : IRequestHandler<GetAllWelcomeInformationParameter, PagedResponse<IEnumerable<WelcomeInformationViewModel>>>
    {
        private readonly IWelcomeInformationRepositoryAsync _welcomeInformationRepository;
        private readonly IMapper _mapper;
        private Expression<Func<WelcomeInformation, bool>> expression;

        public GetAllWelcomeInformationHandler(IWelcomeInformationRepositoryAsync welcomeInformationRepository, IMapper mapper)
        {
            _welcomeInformationRepository = welcomeInformationRepository;
            _mapper = mapper;
        }

        public async Task<PagedResponse<IEnumerable<WelcomeInformationViewModel>>> Handle(GetAllWelcomeInformationParameter request, CancellationToken cancellationToken)
        {
            var validFilter = _mapper.Map<GetAllWelcomeInformationParameter>(request);
            var result = from x in _welcomeInformationRepository.GetAllAsync().Result.ToList().Where(x => x.IsDelete == false).ToList().OrderByDescending(x => x.Modified)
                         select new WelcomeInformationViewModel
                         {
                             Id = x.Id,
                             Module = x.Module,
                             Title_TH = x.Title_TH != null ? x.Title_TH : "-",
                             Information_TH = x.Information_TH != null ? x.Information_TH : "-",
                             Title_ENG = x.Title_ENG != null ? x.Title_ENG : "-",
                             Information_ENG = x.Information_ENG != null ? x.Information_ENG : "-",
                             Sequence = x.Sequence,
                             Active = x.Active,
                             Created = x.Created,
                             Modified = x.Modified
                         };


            if (validFilter.Search != null && validFilter.Search.Trim() != "")
            {
                result = result.Where(x => x.Module.ToLower().Contains(validFilter.Search.ToLower().Trim()) || 
                        x.Title_TH.ToLower().Contains(validFilter.Search.ToLower().Trim()) || x.Information_TH.ToLower().Contains(validFilter.Search.ToLower().Trim())
                        || x.Title_ENG.ToLower().Contains(validFilter.Search.ToLower().Trim()) || x.Information_ENG.ToLower().Contains(validFilter.Search.ToLower().Trim())
                        ).ToList();
            }
            if (request.Column == "module" && request.Active == 3)
            {
                result = result.OrderBy(x => x.Module);
            }
            if (request.Column == "module" && request.Active == 2)
            {
                result = result.OrderBy(x => x.Module);
            }
            if (request.Column == "title_TH" && request.Active == 3)
            {
                result = result.OrderBy(x => x.Title_TH);
            }
            if (request.Column == "title_TH" && request.Active == 2)
            {
                result = result.OrderByDescending(x => x.Title_TH);
            }
            if (request.Column == "information_TH" && request.Active == 3)
            {
                result = result.OrderBy(x => x.Information_TH);
            }
            if (request.Column == "information_TH" && request.Active == 2)
            {
                result = result.OrderByDescending(x => x.Information_TH);
            }
            if (request.Column == "title_ENG" && request.Active == 3)
            {
                result = result.OrderBy(x => x.Title_ENG);
            }
            if (request.Column == "title_ENG" && request.Active == 2)
            {
                result = result.OrderByDescending(x => x.Title_ENG);
            }
            if (request.Column == "information_ENG" && request.Active == 3)
            {
                result = result.OrderBy(x => x.Information_ENG);
            }
            if (request.Column == "information_ENG" && request.Active == 2)
            {
                result = result.OrderByDescending(x => x.Information_ENG);
            }
            if (request.Column == "sequence" && request.Active == 3)
            {
                result = result.OrderBy(x => x.Sequence);
            }
            if (request.Column == "sequence" && request.Active == 3)
            {
                result = result.OrderBy(x => x.Sequence);
            }
            if (request.Column == "sequence" && request.Active == 2)
            {
                result = result.OrderByDescending(x => x.Sequence);
            }
            if (request.Column == "created" && request.Active == 3)
            {
                result = result.OrderBy(x => x.Sequence);
            }
            if (request.Column == "created" && request.Active == 2)
            {
                result = result.OrderByDescending(x => x.Sequence);
            }
            if (request.Column == "modified" && request.Active == 3)
            {
                result = result.OrderBy(x => x.Sequence);
            }
            if (request.Column == "modified" && request.Active == 2)
            {
                result = result.OrderByDescending(x => x.Sequence);
            }

            var itemCount = result.Count();
            result = result.Skip((validFilter.PageNumber - 1) * validFilter.PageSize).Take(validFilter.PageSize);
            return new PagedResponse<IEnumerable<WelcomeInformationViewModel>>(result, validFilter.PageNumber, validFilter.PageSize, itemCount);
        }
    }
}
