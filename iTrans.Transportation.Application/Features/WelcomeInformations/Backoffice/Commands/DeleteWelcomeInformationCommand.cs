﻿using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;


namespace iTrans.Transportation.Application.Features.WelcomeInformations.Backoffice.Commands
{
   public class DeleteWelcomeInformationCommand : IRequest<Response<int>>
    {
        public Guid? UserId { get; set; }
        public int Id { get; set; }
        public class DeleteWelcomeInformationCommandHandler : IRequestHandler<DeleteWelcomeInformationCommand, Response<int>>
        {
            private readonly IWelcomeInformationRepositoryAsync _welcomeInformationRepository;
            private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
            public DeleteWelcomeInformationCommandHandler(IWelcomeInformationRepositoryAsync welcomeInformationRepository, IApplicationLogRepositoryAsync applicationLogRepository)
            {
                _welcomeInformationRepository = welcomeInformationRepository;
                _applicationLogRepository = applicationLogRepository;
            }
            public async Task<Response<int>> Handle(DeleteWelcomeInformationCommand command, CancellationToken cancellationToken)
            {
                var data = (await _welcomeInformationRepository.FindByCondition(x => x.Id == command.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (data == null)
                {
                    throw new ApiException($"WelcomeInformation Not Found.");
                }
                else
                {
                    data.Active = false;
                    data.IsDelete = true;
                    data.Modified = DateTime.UtcNow;
                    data.ModifiedBy = command.UserId != null ? command.UserId.GetValueOrDefault().ToString() : "";
                    await _welcomeInformationRepository.UpdateAsync(data);
                    var log = new CreateAppLog(_applicationLogRepository);
                    log.CreateLog("WelcomeInformation", "WelcomeInformation", "Delete", Newtonsoft.Json.JsonConvert.SerializeObject(command), command.UserId == null ? "-" : command.UserId.Value.ToString());
                    return new Response<int>(data.Id);
                }
            }
        }
    }
}
