﻿using AutoMapper;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using iTrans.Transportation.Domain.Entities;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.WelcomeInformations.Backoffice.Commands
{

    public partial class CreateWelcomeInformationCommand : IRequest<Response<int>>
    {
        public Guid? UserId { get; set; }
        public string Module { get; set; }
        public string Title_TH { get; set; }
        public string Information_TH { get; set; }
        public string Title_ENG { get; set; }
        public string Information_ENG { get; set; }
        public IFormFile Picture { get; set; }
        public int Sequence { get; set; }
        public bool Active { get; set; }
    }
    public class CreateWelcomeInformationCommandHandler : IRequestHandler<CreateWelcomeInformationCommand, Response<int>>
    {
        private readonly IWelcomeInformationRepositoryAsync _welcomeInformationRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;
        private readonly IConfiguration _configuration;
        public CreateWelcomeInformationCommandHandler(IWelcomeInformationRepositoryAsync welcomeInformationRepository,IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper, IConfiguration configuration)
        {
            _welcomeInformationRepository = welcomeInformationRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
            _configuration = configuration;
        }

        public async Task<Response<int>> Handle(CreateWelcomeInformationCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var getContentPath = _configuration.GetSection("ContentPath");
                var contentPath = getContentPath.Value;
                string currentTimeStr = DateTime.Now.ToString("yyyyMMddHHmmss");

                var data = _mapper.Map<WelcomeInformation>(request);
                if(request.Picture != null)
                {
                    string folderPath = contentPath + "welcomeInformation/" + currentTimeStr;
                    if (!Directory.Exists(folderPath))
                    {
                        Directory.CreateDirectory(folderPath);
                    }
                    string filePath = Path.Combine(folderPath, request.Picture.FileName);
                    using (FileStream fileStream = new FileStream(filePath, FileMode.Create))
                    {
                        await request.Picture.CopyToAsync(fileStream);
                        data.Picture = "welcomeInformation/" + currentTimeStr + "/" + request.Picture.FileName;

                    }
                }
                data.Created = DateTime.UtcNow;
                data.Modified = DateTime.UtcNow;
                data.CreatedBy = request.UserId == null ? "" : request.UserId.Value.ToString();
                data.ModifiedBy = request.UserId == null ? "" : request.UserId.Value.ToString();

                var dataListObject = await _welcomeInformationRepository.AddAsync(data);
                var log = new CreateAppLog(_applicationLogRepository);
                log.CreateLog("WelcomeInformation", "Welcome Information", "Create", Newtonsoft.Json.JsonConvert.SerializeObject(request), request.UserId.ToString());
                return new Response<int>(dataListObject.Id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}