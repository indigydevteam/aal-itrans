﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
namespace iTrans.Transportation.Application.Features.WelcomeInformations.Backoffice.Commands
{
    public class UpdateWelcomeInformationCommandValidator : AbstractValidator<UpdateWelcomeInformationCommand>
    {
        private readonly IWelcomeInformationRepositoryAsync welcomeInformationRepository;

        public UpdateWelcomeInformationCommandValidator(IWelcomeInformationRepositoryAsync welcomeInformationRepository)
        {
            this.welcomeInformationRepository = welcomeInformationRepository;

            RuleFor(p => p.Title_TH)
                .NotEmpty().WithMessage("{PropertyName} is required.")
                .NotNull()
                .MaximumLength(250).WithMessage("{PropertyName} must not exceed 250 characters.");


            RuleFor(p => p.Title_ENG)
                .NotEmpty().WithMessage("{PropertyName} is required.")
                .NotNull()
                .MaximumLength(250).WithMessage("{PropertyName} must not exceed 250 characters.");

        }


    }
}
