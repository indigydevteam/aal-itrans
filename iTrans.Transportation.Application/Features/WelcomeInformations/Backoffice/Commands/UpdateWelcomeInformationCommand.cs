﻿using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.WelcomeInformations.Backoffice.Commands
{
  public class UpdateWelcomeInformationCommand : IRequest<Response<int>>
    {
        public Guid? UserId { get; set; }
        public int Id { get; set; }
        public string Module { get; set; }
        public string Title_TH { get; set; }
        public string Information_TH { get; set; }
        public string Title_ENG { get; set; }
        public string Information_ENG { get; set; }
        public IFormFile Picture { get; set; }
        public int Sequence { get; set; }
        public bool Active { get; set; }
    }
    public class UpdateWelcomeInformationCommandHandler : IRequestHandler<UpdateWelcomeInformationCommand, Response<int>>
    {
        private readonly IWelcomeInformationRepositoryAsync _welcomeInformationRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;
        private readonly IConfiguration _configuration;

        public UpdateWelcomeInformationCommandHandler(IWelcomeInformationRepositoryAsync welcomeInformationRepository,IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper, IConfiguration configuration)
        {
            _welcomeInformationRepository = welcomeInformationRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
            _configuration = configuration;
        }

        public async Task<Response<int>> Handle(UpdateWelcomeInformationCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var data = (await _welcomeInformationRepository.FindByCondition(x => x.Id == request.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (data == null)
                {
                    throw new ApiException($"WelcomeInformation Not Found.");
                }
                else
                {
                    var getContentPath = _configuration.GetSection("ContentPath");
                    var contentPath = getContentPath.Value;
                    string currentTimeStr = DateTime.Now.ToString("yyyyMMddHHmmss");

                    if (request.Picture != null)
                    {
                        string folderPath = contentPath + "welcomeInformation/" + currentTimeStr;
                        if (!Directory.Exists(folderPath))
                        {
                            Directory.CreateDirectory(folderPath);
                        }
                        string filePath = Path.Combine(folderPath, request.Picture.FileName);
                        using (FileStream fileStream = new FileStream(filePath, FileMode.Create))
                        {
                            await request.Picture.CopyToAsync(fileStream);
                            data.Picture = "welcomeInformation/" + currentTimeStr + "/" + request.Picture.FileName;

                        }
                    }

                    data.Module = request.Module;
                    data.Information_TH = request.Information_TH;
                    data.Information_ENG = request.Information_ENG;
                    data.Title_TH = request.Title_TH;
                    data.Title_ENG = request.Title_ENG;
                    data.Sequence = request.Sequence;
                    data.Active = request.Active;
                    data.Modified = DateTime.UtcNow;
                    data.ModifiedBy = request.UserId == null ? "" : request.UserId.Value.ToString();


                    await _welcomeInformationRepository.UpdateAsync(data);
                    var log = new CreateAppLog(_applicationLogRepository);
                    log.CreateLog("WelcomeInformation", "Welcome Information", "Edit", Newtonsoft.Json.JsonConvert.SerializeObject(request), request.UserId.ToString());
                    return new Response<int>(data.Id);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}

