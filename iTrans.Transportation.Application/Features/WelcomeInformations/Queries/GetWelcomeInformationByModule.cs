﻿using AutoMapper;
using iTrans.Transportation.Application.DTOs;
using iTrans.Transportation.Application.DTOs.UserLevel;
using iTrans.Transportation.Application.DTOs.UserLevelCondition;
using iTrans.Transportation.Application.DTOs.WelcomeInformation;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.WelcomeInformations.Queries
{
   public class GetWelcomeInformationByModule : IRequest<Response<IEnumerable<WelcomeInformationViewModel>>>
    {
      public string module { set; get; }
    }
    public class GetWelcomeInformationByModuleHandler : IRequestHandler<GetWelcomeInformationByModule, Response<IEnumerable<WelcomeInformationViewModel>>>
    {
        private readonly IWelcomeInformationRepositoryAsync _welcomeInformationRepository;
        private readonly IMapper _mapper;
        public GetWelcomeInformationByModuleHandler(IWelcomeInformationRepositoryAsync welcomeInformationRepository, IMapper mapper)
        {
            _welcomeInformationRepository = welcomeInformationRepository;
            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<WelcomeInformationViewModel>>> Handle(GetWelcomeInformationByModule request, CancellationToken cancellationToken)
        {
            var data = (await _welcomeInformationRepository.FindByCondition(x => x.Module.Equals(request.module) && x.Active == true).ConfigureAwait(false)).AsQueryable().ToList().OrderBy(x => x.Sequence);
            var WelcomeInformationViewModel = _mapper.Map<IEnumerable<WelcomeInformationViewModel>>(data);
            return new Response<IEnumerable<WelcomeInformationViewModel>>(WelcomeInformationViewModel);
        }
    }
}
