﻿using AutoMapper;
using iTrans.Transportation.Application.DTOs.RegisterInformation;
using iTrans.Transportation.Application.DTOs.UserLevel;
using iTrans.Transportation.Application.DTOs.WelcomeInformation;
using iTrans.Transportation.Application.Features.CarLists.Queries;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.WelcomeInformations.Queries
{
   public class GetWelcomeInformationById : IRequest<Response<WelcomeInformationViewModel>>
    {
        public int Id { get; set; }
    }
    public class GetWelcomeInformationByIdHandler : IRequestHandler<GetWelcomeInformationById, Response<WelcomeInformationViewModel>>
    {
        private readonly IWelcomeInformationRepositoryAsync _welcomeInformationRepository;
        private readonly IMapper _mapper;
        public GetWelcomeInformationByIdHandler(IWelcomeInformationRepositoryAsync welcomeInformationRepository, IMapper mapper)
        {
            _welcomeInformationRepository = welcomeInformationRepository;
            _mapper = mapper;
        }
        public async Task<Response<WelcomeInformationViewModel>> Handle(GetWelcomeInformationById request, CancellationToken cancellationToken)
        {
            var DataObject = (await _welcomeInformationRepository.FindByCondition(x => x.Id.Equals(request.Id)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            return new Response<WelcomeInformationViewModel>(_mapper.Map<WelcomeInformationViewModel>(DataObject));
        }
    }
}
