﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using iTrans.Transportation.Application.Interfaces.Repositories;
using FluentValidation;

namespace iTrans.Transportation.Application.Features.CarSpecifications.Queries
{
    public class GetCarSpecificationByCarTypeValidator : AbstractValidator<GetCarSpecificationByCarType>
    {
        private readonly ICarSpecificationRepositoryAsync carSpecificationRepository;

        public GetCarSpecificationByCarTypeValidator(ICarSpecificationRepositoryAsync carSpecificationRepository)
        {
            this.carSpecificationRepository = carSpecificationRepository;
            RuleFor(p => p.CarTypeId)
               .NotEmpty().WithMessage("{PropertyName} is required.")
               .NotNull()
               .MustAsync(IsCarTypeExists).WithMessage("{PropertyName} not exists.");
        }
         
        private async Task<bool> IsCarTypeExists(int CarTypeId, CancellationToken cancellationToken)
        {
            var userObject = (await carSpecificationRepository.FindByCondition(x => x.CarType.Id.Equals(CarTypeId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            if (userObject != null)
            {
                return true;
            }
            return false;
        }
    }
}
