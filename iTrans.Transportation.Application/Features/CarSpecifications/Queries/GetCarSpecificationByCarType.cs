﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.CarSpecification;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.CarSpecifications.Queries
{
    public class GetCarSpecificationByCarType : IRequest<Response<IEnumerable<CarSpecificationViewModel>>>
    {
        public int CarTypeId { get; set; }
    }
    public class GetCarSpecificationByCarTypeHandler : IRequestHandler<GetCarSpecificationByCarType, Response<IEnumerable<CarSpecificationViewModel>>>
    {
        private readonly ICarSpecificationRepositoryAsync _carSpecificationRepository;
        private readonly IMapper _mapper;
        public GetCarSpecificationByCarTypeHandler(ICarSpecificationRepositoryAsync carSpecificationRepository, IMapper mapper)
        {
            _carSpecificationRepository = carSpecificationRepository;
            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<CarSpecificationViewModel>>> Handle(GetCarSpecificationByCarType request, CancellationToken cancellationToken)
        {
            var carSpecification = (await _carSpecificationRepository.FindByCondition(x => x.CarType.Id == request.CarTypeId && x.Active == true).ConfigureAwait(false)).OrderBy(x => x.Sequence).AsQueryable().ToList();
            var carSpecificationViewModel = _mapper.Map<IEnumerable<CarSpecificationViewModel>>(carSpecification);
            return new Response<IEnumerable<CarSpecificationViewModel>>(carSpecificationViewModel);
        }
    }
}