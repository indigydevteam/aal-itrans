﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using iTrans.Transportation.Application.Interfaces.Repositories;
using FluentValidation;

namespace iTrans.Transportation.Application.Features.CarSpecifications.Queries
{
    public class GetCarSpecificationByIdValidator : AbstractValidator<GetCarSpecificationById>
    {
        private readonly ICarSpecificationRepositoryAsync carSpecificationRepository;

        public GetCarSpecificationByIdValidator(ICarSpecificationRepositoryAsync carSpecificationRepository)
        {
            this.carSpecificationRepository = carSpecificationRepository;
            RuleFor(p => p.Id)
               .NotEmpty().WithMessage("{PropertyName} is required.")
               .NotNull()
               .MustAsync(IsCarSpecificationExists).WithMessage("{PropertyName} not exists.");
        }

        private async Task<bool> IsCarSpecificationExists(int CarSpecificationId, CancellationToken cancellationToken)
        {
            var userObject = (await carSpecificationRepository.FindByCondition(x => x.Id.Equals(CarSpecificationId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            if (userObject != null)
            {
                return true;
            }
            return false;
        }
    }
}
