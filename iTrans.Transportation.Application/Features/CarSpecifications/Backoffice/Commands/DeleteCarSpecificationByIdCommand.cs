﻿using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.CarSpecifications.Backoffice.Commands
{
    public class DeleteCarSpecificationByIdCommand : IRequest<Response<int>>
    {
        public Guid? UserId { get; set; }
        public int Id { get; set; }
        public class DeleteCarSpecificationByIdCommandHandler : IRequestHandler<DeleteCarSpecificationByIdCommand, Response<int>>
        {
            private readonly ICarSpecificationRepositoryAsync _carSpecificationRepository;
            private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
            public DeleteCarSpecificationByIdCommandHandler(ICarSpecificationRepositoryAsync carSpecificationRepository, IApplicationLogRepositoryAsync applicationLogRepository)
            {
                _carSpecificationRepository = carSpecificationRepository;
                _applicationLogRepository = applicationLogRepository;
            }
            public async Task<Response<int>> Handle(DeleteCarSpecificationByIdCommand command, CancellationToken cancellationToken)
            {
                var carSpecification = (await _carSpecificationRepository.FindByCondition(x => x.Id == command.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (carSpecification == null)
                {
                    throw new ApiException($"CarSpecification Not Found.");
                }
                else
                {
                    carSpecification.Active = false;
                    carSpecification.IsDelete = true;
                    carSpecification.Modified = DateTime.UtcNow;
                    carSpecification.ModifiedBy = command.UserId != null ? command.UserId.GetValueOrDefault().ToString() : "";
                    await _carSpecificationRepository.UpdateAsync(carSpecification);
                    var log = new CreateAppLog(_applicationLogRepository);
                    log.CreateLog("CarSpecification", "CarSpecification", "Delete", Newtonsoft.Json.JsonConvert.SerializeObject(command), command.UserId == null ? "-" : command.UserId.Value.ToString());
                    return new Response<int>(carSpecification.Id);
                }
            }
        }
    }
}
