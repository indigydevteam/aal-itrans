﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.CarSpecifications.Backoffice.Commands
{
    public class CreateCarSpecificationCommandValidator : AbstractValidator<CreateCarSpecificationCommand>
    {
        private readonly ICarTypeRepositoryAsync carTypeRepository;

        public CreateCarSpecificationCommandValidator(ICarTypeRepositoryAsync carTypeRepository)
        {
            this.carTypeRepository = carTypeRepository;

            RuleFor(p => p.Name_TH)
                .NotEmpty().WithMessage("{PropertyName} is required.")
                .NotNull()
                .MaximumLength(50).WithMessage("{PropertyName} must not exceed 50 characters.");


            RuleFor(p => p.Name_ENG)
                .NotEmpty().WithMessage("{PropertyName} is required.")
                .NotNull()
                .MaximumLength(50).WithMessage("{PropertyName} must not exceed 50 characters.");

            RuleFor(p => p.CarTypeId)
               .NotNull().WithMessage("{PropertyName} is required.")
               .MustAsync(IsExistCarType).WithMessage("{PropertyName} not exists.");
        }

        private async Task<bool> IsExistCarType(int carTypeId, CancellationToken cancellationToken)
        {
            var carTypeObject = (await carTypeRepository.FindByCondition(x => x.Id == carTypeId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            if (carTypeObject == null)
            {
                return false;
            }
            return true;
        }
    }
}
