﻿using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Interfaces.Services;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.CarSpecifications.Backoffice.Commands
{
    public partial class CreateCarSpecificationCommand : IRequest<Response<int>>
    {
        public Guid? UserId { get; set; }
        public int CarTypeId { get; set; }
        public string Name_TH { get; set; }
        public string Name_ENG { get; set; }
        public int Sequence { get; set; }
        public bool Active { get; set; }
        public bool Specified { get; set; }
        public bool SpecifiedTemperature { get; set; }
        public bool SpecifiedEnergySavingDevice { get; set; }
        public string ContentDirectory { get; set; }
       public IFormFile ImageFile { get; set; }
        //public string Image { get; set; }

    }
    public class CreateCarSpecificationCommandHandler : IRequestHandler<CreateCarSpecificationCommand, Response<int>>
    {
        private readonly ICarTypeRepositoryAsync _carTypeRepository;
        private readonly ICarSpecificationRepositoryAsync _carSpecificationRepository;
        private readonly ICarSpecificationFileRepositoryAsync _carSpecificationFileRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;
        public CreateCarSpecificationCommandHandler(ICarSpecificationRepositoryAsync carSpecificationRepository, ICarSpecificationFileRepositoryAsync carSpecificationFileRepository, ICarTypeRepositoryAsync carTypeRepository, IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _carSpecificationRepository = carSpecificationRepository;
            _carSpecificationFileRepository = carSpecificationFileRepository;
            _carTypeRepository = carTypeRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(CreateCarSpecificationCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var carType = (await _carTypeRepository.FindByCondition(x => x.Id == request.CarTypeId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (carType == null)
                {
                    throw new ApiException($"CarType Not Found.");
                }
                var carSpecification = _mapper.Map<CarSpecification>(request);
                carSpecification.CarType = carType;
                carSpecification.IsDelete = false;
                carSpecification.Created = DateTime.UtcNow;
                carSpecification.Modified = DateTime.UtcNow;
                carSpecification.CreatedBy = request.UserId == null? "": request.UserId.Value.ToString();
                carSpecification.ModifiedBy = request.UserId == null ? "" : request.UserId.Value.ToString();

                var carSpecificationObject = await _carSpecificationRepository.AddAsync(carSpecification);

                if (request.ImageFile != null)
                {
                    string folderPath = request.ContentDirectory + "CarSpecification/" + request.Name_TH.ToString();
                    if (!Directory.Exists(folderPath))
                    {
                        Directory.CreateDirectory(folderPath);
                    }

                    string filePath = Path.Combine(folderPath, request.ImageFile.FileName);
                    using (FileStream fileStream = new FileStream(filePath, FileMode.Create))
                    {
                        await request.ImageFile.CopyToAsync(fileStream);
                        FileInfo fi = new FileInfo(filePath);

                        CarSpecificationFile carSpecificationFile = new CarSpecificationFile
                        {
                            CarSpecification = carSpecification,
                            FileName = request.ImageFile.FileName,
                            ContentType = request.ImageFile.ContentType,
                            FilePath = "CarSpecification/" + request.Name_TH.ToString() + "/" + request.ImageFile.FileName,
                            DirectoryPath = "CarSpecification/" + request.Name_TH.ToString() + "/",
                            FileEXT = fi.Extension,
                            Created = DateTime.Now,
                            CreatedBy = request.UserId != null ? request.UserId.Value.ToString() : "",
                            Modified = DateTime.Now,
                            ModifiedBy = request.UserId != null ? request.UserId.Value.ToString() : ""
                        };
                        await _carSpecificationFileRepository.AddAsync(carSpecificationFile);
                    }
                }

                var log = new CreateAppLog(_applicationLogRepository);
                log.CreateLog("Car", "Car Specification", "Create", Newtonsoft.Json.JsonConvert.SerializeObject(carSpecificationObject),request.UserId.ToString());
                return new Response<int>(carSpecificationObject.Id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
