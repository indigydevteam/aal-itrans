﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.Backoffice.CarSpecification;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.CarSpecifications.Backoffice.Queries
{
    public class GetCarSpecificationByIdQuery : IRequest<Response<CarSpecificationViewModel>>
    {
        public int Id { get; set; }
    }
    public class GetCarSpecificationByIdQueryHandler : IRequestHandler<GetCarSpecificationByIdQuery, Response<CarSpecificationViewModel>>
    {
        private readonly ICarSpecificationRepositoryAsync _carSpecificationRepository;
        private readonly IMapper _mapper;
        public GetCarSpecificationByIdQueryHandler(ICarSpecificationRepositoryAsync carSpecificationRepository, IMapper mapper)
        {
            _carSpecificationRepository = carSpecificationRepository;
            _mapper = mapper;
        }
        public async Task<Response<CarSpecificationViewModel>> Handle(GetCarSpecificationByIdQuery request, CancellationToken cancellationToken)
        {
            var carSpecificationObject = (await _carSpecificationRepository.FindByCondition(x => x.Id.Equals(request.Id)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            return new Response<CarSpecificationViewModel>(_mapper.Map<CarSpecificationViewModel>(carSpecificationObject));
        }
    }
}
