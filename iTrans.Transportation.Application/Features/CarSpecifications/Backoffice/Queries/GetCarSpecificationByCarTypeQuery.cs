﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.CarSpecification;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.CarSpecifications.Queries
{
    public class GetCarSpecificationByCarTypeQuery : IRequest<Response<IEnumerable<CarSpecificationViewModel>>>
    {
        public int CarTypeId { get; set; }
    }
    public class GetCarSpecificationByCarTypeQueryHandler : IRequestHandler<GetCarSpecificationByCarTypeQuery, Response<IEnumerable<CarSpecificationViewModel>>>
    {
        private readonly ICarSpecificationRepositoryAsync _carSpecificationRepository;
        private readonly IMapper _mapper;
        public GetCarSpecificationByCarTypeQueryHandler(ICarSpecificationRepositoryAsync carSpecificationRepository, IMapper mapper)
        {
            _carSpecificationRepository = carSpecificationRepository;
            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<CarSpecificationViewModel>>> Handle(GetCarSpecificationByCarTypeQuery request, CancellationToken cancellationToken)
        {
            var carSpecification = (await _carSpecificationRepository.FindByCondition(x => x.CarType.Id == request.CarTypeId && x.Active == true).ConfigureAwait(false)).OrderBy(x => x.Sequence).AsQueryable().ToList();
            var carSpecificationViewModel = _mapper.Map<IEnumerable<CarSpecificationViewModel>>(carSpecification);
            return new Response<IEnumerable<CarSpecificationViewModel>>(carSpecificationViewModel);
        }
    }
}