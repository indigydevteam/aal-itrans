﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.OrderingAddressHistory;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
namespace iTrans.Transportation.Application.Features.OrderingAddressHistoryHistories.Qureies
{
   public class GetOrderingAddressHistoryByIdQuery : IRequest<Response<OrderingAddressHistoryViewModel>>
    {
        public int Id { get; set; }
    }
    public class GetOrderingAddressHistoryByIdQueryHandler : IRequestHandler<GetOrderingAddressHistoryByIdQuery, Response<OrderingAddressHistoryViewModel>>
    {
        private readonly IOrderingAddressHistoryRepositoryAsync _orderingAddressHistoryRepository;
        private readonly IMapper _mapper;
        public GetOrderingAddressHistoryByIdQueryHandler(IOrderingAddressHistoryRepositoryAsync orderingAddressHistoryRepository, IMapper mapper)
        {
            _orderingAddressHistoryRepository = orderingAddressHistoryRepository;
            _mapper = mapper;
        }
        public async Task<Response<OrderingAddressHistoryViewModel>> Handle(GetOrderingAddressHistoryByIdQuery request, CancellationToken cancellationToken)
        {
            var DataObject = (await _orderingAddressHistoryRepository.FindByCondition(x => x.Id.Equals(request.Id)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            return new Response<OrderingAddressHistoryViewModel>(_mapper.Map<OrderingAddressHistoryViewModel>(DataObject));
        }
    }
}
