﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.OrderingAddressHistory;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
namespace iTrans.Transportation.Application.Features.OrderingAddressHistoryHistories.Qureies
{
   public class GetOrderingAddressHistoryByOrderingQuery : IRequest<Response<IEnumerable<OrderingAddressHistoryViewModel>>>
    {
        public int OrderingId { set; get; }
    }
    public class GetOrderingAddressHistoryByOrderingQueryHandler : IRequestHandler<GetOrderingAddressHistoryByOrderingQuery, Response<IEnumerable<OrderingAddressHistoryViewModel>>>
    {
        private readonly IOrderingAddressHistoryRepositoryAsync _orderingAddressHistoryRepository;
        private readonly IMapper _mapper;
        public GetOrderingAddressHistoryByOrderingQueryHandler(IOrderingAddressHistoryRepositoryAsync orderingAddressHistoryRepository, IMapper mapper)
        {
            _orderingAddressHistoryRepository = orderingAddressHistoryRepository;
            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<OrderingAddressHistoryViewModel>>> Handle(GetOrderingAddressHistoryByOrderingQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var OrderingAddressHistory = (await _orderingAddressHistoryRepository.FindByCondition(x => x.Ordering.Id.Equals(request.OrderingId)).ConfigureAwait(false)).AsQueryable().ToList();
                var OrderingAddressHistoryViewModel = _mapper.Map<IEnumerable<OrderingAddressHistoryViewModel>>(OrderingAddressHistory);
                return new Response<IEnumerable<OrderingAddressHistoryViewModel>>(OrderingAddressHistoryViewModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}