﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.OrderingAddressHistory;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
namespace iTrans.Transportation.Application.Features.OrderingAddressHistoryHistories.Qureies
{
   public class GetAllOrderingAddressHistoryQuery : IRequest<Response<IEnumerable<OrderingAddressHistoryViewModel>>>
    {

    }
    public class GetAllOrderingAddressHistoryQueryHandler : IRequestHandler<GetAllOrderingAddressHistoryQuery, Response<IEnumerable<OrderingAddressHistoryViewModel>>>
    {
        private readonly IOrderingAddressHistoryRepositoryAsync _orderingAddressHistoryRepository;
        private readonly IMapper _mapper;
        public GetAllOrderingAddressHistoryQueryHandler(IOrderingAddressHistoryRepositoryAsync OrderingAddressHistoryRepository, IMapper mapper)
        {
            _orderingAddressHistoryRepository = OrderingAddressHistoryRepository;
            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<OrderingAddressHistoryViewModel>>> Handle(GetAllOrderingAddressHistoryQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var OrderingAddressHistory = await _orderingAddressHistoryRepository.GetAllAsync();
                var OrderingAddressHistoryViewModel = _mapper.Map<IEnumerable<OrderingAddressHistoryViewModel>>(OrderingAddressHistory);
                return new Response<IEnumerable<OrderingAddressHistoryViewModel>>(OrderingAddressHistoryViewModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
