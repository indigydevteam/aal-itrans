﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.OrderingAddressHistory;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain.Entities;
using MediatR;
namespace iTrans.Transportation.Application.Features.OrderingAddressHistoryHistories.Qureies
{
   public class GetAllOrderingAddressHistoryParameter : IRequest<PagedResponse<IEnumerable<OrderingAddressHistoryViewModel>>>
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
    }
    public class GetAllOrderingAddressHistoryParameterHandler : IRequestHandler<GetAllOrderingAddressHistoryParameter, PagedResponse<IEnumerable<OrderingAddressHistoryViewModel>>>
    {
        private readonly IOrderingAddressHistoryRepositoryAsync _OrderingAddressHistoryRepository;
        private readonly IMapper _mapper;
        public GetAllOrderingAddressHistoryParameterHandler(IOrderingAddressHistoryRepositoryAsync OrderingAddressHistoryRepository, IMapper mapper)
        {
            _OrderingAddressHistoryRepository = OrderingAddressHistoryRepository;
            _mapper = mapper;
        }

        public async Task<PagedResponse<IEnumerable<OrderingAddressHistoryViewModel>>> Handle(GetAllOrderingAddressHistoryParameter request, CancellationToken cancellationToken)
        {
            var validFilter = _mapper.Map<GetAllOrderingAddressHistoryParameter>(request);
            Expression<Func<OrderingAddressHistory, bool>> expression = x => x.PostCode != "";
            var OrderingAddressHistory = await _OrderingAddressHistoryRepository.FindByConditionWithPage(expression, validFilter.PageNumber, validFilter.PageSize);
            var OrderingAddressHistoryViewModel = _mapper.Map<IEnumerable<OrderingAddressHistoryViewModel>>(OrderingAddressHistory);
            return new PagedResponse<IEnumerable<OrderingAddressHistoryViewModel>>(OrderingAddressHistoryViewModel, validFilter.PageNumber, validFilter.PageSize);
        }
    }
}
