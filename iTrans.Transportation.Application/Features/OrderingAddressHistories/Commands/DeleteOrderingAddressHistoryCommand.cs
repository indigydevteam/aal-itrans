﻿using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
namespace iTrans.Transportation.Application.Features.OrderingAddressHistoryHistories.Commands
{
   public class DeleteOrderingAddressHistoryCommand : IRequest<Response<int>>
    {
        public int Id { get; set; }
        public Guid OrderingId { get; set; }
        public class DeleteOrderingAddressHistoryCommandHandler : IRequestHandler<DeleteOrderingAddressHistoryCommand, Response<int>>
        {
            private readonly IOrderingAddressHistoryRepositoryAsync _OrderingAddressHistoryRepository;
            private readonly IOrderingAddressProductRepositoryAsync _orderingAddressProductRepository;
            private readonly IConfiguration _configuration;
            public DeleteOrderingAddressHistoryCommandHandler(IOrderingAddressHistoryRepositoryAsync OrderingAddressHistoryRepository, IOrderingAddressProductRepositoryAsync orderingAddressProductRepository, IConfiguration configuration)//, IOrderingAddressHistoryFileRepositoryAsync orderingAddressHistoryFileRepository)
            {
                _OrderingAddressHistoryRepository = OrderingAddressHistoryRepository;
                _orderingAddressProductRepository = orderingAddressProductRepository;
                _configuration = configuration;
                // _orderingAddressHistoryFileRepository = orderingAddressHistoryFileRepository;
            }
            public async Task<Response<int>> Handle(DeleteOrderingAddressHistoryCommand command, CancellationToken cancellationToken)
            {
                try
                {
                    var getContentPath = _configuration.GetSection("ContentPath");
                    string contentPath = getContentPath.Value;
                    var OrderingAddressHistory = (await _OrderingAddressHistoryRepository.FindByCondition(x => x.Id == command.Id && x.Ordering.Id.Equals(command.OrderingId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                    if (OrderingAddressHistory == null)
                    {
                        throw new ApiException($"OrderingAddressHistory Not Found.");
                    }
                    else
                    {
                        (await _orderingAddressProductRepository.CreateSQLQuery("DELETE Ordering_AddressHistoryProduct where OrderingAddressHistoryId = '" + command.Id.ToString() + "'").ConfigureAwait(false)).ExecuteUpdate();
                        //(await _orderingAddressHistoryFileRepository.CreateSQLQuery("DELETE Ordering_AddressHistoryFile where OrderingAddressHistoryId = '" + command.Id.ToString() + "'").ConfigureAwait(false)).ExecuteUpdate();
                        //var orderingAddressHistoryFiles = (await _orderingAddressHistoryFileRepository.FindByCondition(x => x.OrderingAddressHistory.Id == command.Id && x.OrderingAddressHistory.Ordering.Id.Equals(command.OrderingId)).ConfigureAwait(false)).AsQueryable().ToList();
                        //foreach (OrderingAddressHistoryFile orderingAddressHistoryFile in orderingAddressHistoryFiles)
                        //{
                        //    File.Delete(Path.Combine(contentPath, orderingAddressHistoryFile.FilePath));
                        //    await _orderingAddressHistoryFileRepository.DeleteAsync(orderingAddressHistoryFile);
                        //}
                        await _OrderingAddressHistoryRepository.DeleteAsync(OrderingAddressHistory);
                        return new Response<int>(OrderingAddressHistory.Id);
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
    }
}
