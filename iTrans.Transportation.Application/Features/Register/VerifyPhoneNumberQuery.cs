﻿using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using iTrans.Transportation.Application.Helper;
using System.Net.Http;
using System.Net.Http.Headers;

namespace iTrans.Transportation.Application.Features.Register
{
    public class RegisterVerifyPhoneNumberQuery : IRequest<Response<bool>>
    {
        public string Module { get; set; }
        public string PhoneNumber { get; set; }
    }
    public class RegisterVerifyPhoneNumberQueryHandler : IRequestHandler<RegisterVerifyPhoneNumberQuery, Response<bool>>
    {
        private readonly ICustomerRepositoryAsync _customerRepository;
        private readonly IDriverRepositoryAsync _driverRepository;
        private readonly IMapper _mapper;

        public RegisterVerifyPhoneNumberQueryHandler(ICustomerRepositoryAsync customerdRepository, IDriverRepositoryAsync driverRepository, IMapper mapper)
        {
            _customerRepository = customerdRepository;
            _driverRepository = driverRepository;
            _mapper = mapper;
        }

        public async Task<Response<bool>> Handle(RegisterVerifyPhoneNumberQuery request, CancellationToken cancellationToken)
        {
            try
            {
                bool result = false;
                if ( request.PhoneNumber == null || request.PhoneNumber == "")
                {
                    return new Response<bool>(false);
                }
                if (request.Module != null && request.Module.Trim() != "")
                {
                    if (!result)
                    {
                        if (request.Module.Trim().ToLower() == "customer")
                        {
                            var customer = (await _customerRepository.FindByCondition(x => x.PhoneNumber == request.PhoneNumber && x.IsDelete == false).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                            if (customer == null)
                            {
                                result = true;
                                
                            }
                        }
                        else if (request.Module.Trim().ToLower() == "driver")
                        {
                            var driver = (await _driverRepository.FindByCondition(x => x.PhoneNumber == request.PhoneNumber && x.IsDelete == false).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                            if (driver == null)
                            {
                                result = true;
                            }
                        }
                    }
                }
                return new Response<bool>(result);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
