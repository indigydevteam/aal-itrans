﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using iTrans.Transportation.Application.Helper;
using System.Text.RegularExpressions;
using Microsoft.Extensions.Configuration;

namespace iTrans.Transportation.Application.Features.Register.Commands
{
    public class RegisterCommandValidator : AbstractValidator<RegisterCommand>
    {
        private readonly ICustomerRepositoryAsync customerRepository;
        private readonly IDriverRepositoryAsync driverRepository;
        private readonly IConfiguration configuration;
        public RegisterCommandValidator(ICustomerRepositoryAsync customerRepository, IDriverRepositoryAsync driverRepository, IConfiguration configuration)
        {
            this.customerRepository = customerRepository;
            this.driverRepository = driverRepository;
            this.configuration = configuration;
            RuleFor(p => p.PhoneNumber)
                .NotEmpty().WithMessage("{PropertyName} is required.")
                .NotNull()
                .MaximumLength(50).WithMessage("{PropertyName} must not exceed 50 characters.")
                .MustAsync(async (p, s, cancellation) =>
                {
                    return await IsUniquePhoneNumber(p.UserType, p.PhoneCode, p.PhoneNumber, p.DriverId).ConfigureAwait(false);
                }).WithMessage("{PropertyName} already exists.");

            //RuleFor(p => p.Name)
            //    .NotEmpty().WithMessage("{PropertyName} is required.")
            //    .NotNull()
            //    .MaximumLength(50).WithMessage("{PropertyName} must not exceed 250 characters.");

            RuleFor(p => p.IdentityNumber)
               .NotEmpty().WithMessage("{PropertyName} is required.")
               .NotNull()
               .MaximumLength(50).WithMessage("{PropertyName} must not exceed 50 characters.")
               .MustAsync(async (p, s, cancellation) =>
               {
                   return await IsUniqueIdentityNumber(p.UserType, p.Type, p.IdentityType, p.IdentityNumber, p.DriverId).ConfigureAwait(false);
               }).WithMessage("{PropertyName} already exists.");
        }

        private async Task<bool> IsUniqueIdentityNumber(string userType, string type, int identityType, string identityNumber, Guid driverId)
        {
            var isDevelopment = this.configuration.GetSection("IsDevelopment");

            if (userType == "driver")
            {
                bool result = true;

                if (type == "personal")
                {
                    if (identityType == 1)
                    {
                        int testCount = 0;
                        try
                        {

                            string DeCodeidentityNumber = AESMgr.Decrypt(identityNumber);

                            Regex rexPersonal = new Regex(@"^[0-9]{13}$");
                            if (rexPersonal.IsMatch(DeCodeidentityNumber))
                            {
                                int sum = 0;

                                for (int i = 0; i < 12; i++)
                                {
                                    testCount = testCount + 1;
                                    sum += int.Parse(DeCodeidentityNumber[i].ToString()) * (13 - i);
                                }

                                result = (int.Parse(DeCodeidentityNumber[12].ToString()) == ((11 - (sum % 11)) % 10));
                            }
                        }
                        catch (Exception ex)
                        {
                            throw ex;
                        }
                    }

                }
                if (isDevelopment != null && isDevelopment.Value == "true")
                {
                    result = true;
                }
                if (result)
                {
                    var driverObject = (await driverRepository.FindByCondition(x => x.IdentityNumber == identityNumber && x.IsDelete == false && (x.Corparate == null || x.Corparate == new Guid())).ConfigureAwait(false)).AsQueryable().FirstOrDefault();

                    if (driverObject != null && driverObject.IsRegister == true)
                    {
                        result = false;
                        if (driverId != null && driverId != new Guid())
                        {
                            var existDriver = (await driverRepository.FindByCondition(x => x.Id == driverId && x.IsDelete == false).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                            if (existDriver != null)
                            {
                                if (driverObject.Id == existDriver.Corparate)
                                {
                                    result = true;
                                }
                            }
                        }
                    }
                }
                return result;
            }
            else
            {
                bool result = true;

                if (type == "personal")
                {
                    if (identityType == 1)
                    {
                        string DeCodeidentityNumber = AESMgr.Decrypt(identityNumber);

                        Regex rexPersonal = new Regex(@"^[0-9]{13}$");
                        if (rexPersonal.IsMatch(DeCodeidentityNumber))
                        {
                            int sum = 0;

                            for (int i = 0; i < 12; i++)
                            {
                                sum += int.Parse(DeCodeidentityNumber[i].ToString()) * (13 - i);
                            }

                            result = (int.Parse(DeCodeidentityNumber[12].ToString()) == ((11 - (sum % 11)) % 10));
                        }
                    }

                }
                if (isDevelopment != null && isDevelopment.Value == "true")
                {
                    result = true;
                }
                if (result)
                {
                    var customerObject = (await customerRepository.FindByCondition(x => x.IdentityNumber.Trim().ToLower() == identityNumber.Trim().ToLower() && x.IsDelete == false).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                    if (customerObject != null)
                    {
                        result = false;
                    }
                }
                return result;
            }
        }
        private async Task<bool> IsUniquePhoneNumber(string userType, string phonecode, string phoneNumber, Guid driverId)
        {
            if (phonecode == "+66" && phoneNumber != null && phoneNumber.Trim().Length > 9 && phoneNumber.Trim().First() == '0')
            {
                phoneNumber = phoneNumber.Trim().Remove(0, 1);
            }

            if (userType == "driver")
            {
                var driverObject = (await driverRepository.FindByCondition(x => x.PhoneNumber.Trim().ToLower() == phoneNumber.Trim().ToLower() && x.IsDelete == false).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (driverObject != null && driverObject.IsRegister == true)
                {
                    return false;
                }
                return true;
            }
            else
            {
                var customerObject = (await customerRepository.FindByCondition(x => x.PhoneNumber.Trim().ToLower() == phoneNumber.Trim().ToLower() && x.IsDelete == false).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (customerObject != null)
                {
                    return false;
                }
                return true;
            }
        }
    }
}
