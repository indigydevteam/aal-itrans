﻿using AutoMapper;
using Microsoft.Extensions.Configuration;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Interfaces.Services;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using iTrans.Transportation.Application.DTOs.Registers;

namespace iTrans.Transportation.Application.Features.Register.Commands
{
    public partial class RegisterCommand : IRequest<Response<Guid>>
    {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string UserType { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string Password { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string Type { get; set; }
        public int CorporateTypeId { get; set; }
        public int TitleId { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string FirstName { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string MiddleName { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string LastName { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string Name { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string IdentityNumber { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public int IdentityType { get; set; }
        public int ContactPersonTitleId { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string ContactPersonFirstName { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string ContactPersonMiddleName { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string ContactPersonLastName { get; set; }
        public DateTime Birthday { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string PhoneCode { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string PhoneNumber { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string Email { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string Facbook { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string Line { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string Twitter { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string Whatapp { get; set; }
        public int? CountryId { get; set; }
        public int? ProvinceId { get; set; }
        public int? DistrictId { get; set; }
        public int? SubdistrictId { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string PostCode { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string Road { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string Alley { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string Address { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string idCard { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string drivingLicense { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string driverPicture { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string companyCertificate { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string NP20 { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string ProfilePicture { get; set; }
        public List<CreateTermAndConditionViewModel> TermAndConditions { get; set; }
        public List<IFormFile> files { get; set; }
        public bool IsCorparateDriver { get; set; }
        public Guid DriverId { get; set; }
    }

    public class RegisterCommandHandler : IRequestHandler<RegisterCommand, Response<Guid>>
    {
        private readonly ICustomerRepositoryAsync _customerRepository;
       // private readonly ICustomerLevelRepositoryAsync _customerLevelRepository;
        private readonly ICustomerAddressRepositoryAsync _customerAddressRepository;
        private readonly ICustomerFileRepositoryAsync _customerFileRepository;
        private readonly ICustomerTermAndConditionRepositoryAsync _customerTermAndConditionRepository;
        private readonly ICustomerPaymentRepositoryAsync _customerPaymentRepository;
        private readonly IDriverTermAndConditionRepositoryAsync _driverTermAndConditionRepository;
        private readonly IDriverRepositoryAsync _driverRepository;
        //private readonly IDriverLevelRepositoryAsync _driverLevelRepository;
        private readonly IDriverAddressRepositoryAsync _driverAddressRepository;
        private readonly IDriverFileRepositoryAsync _driverFileRepository;
        private readonly IDriverPaymentRepositoryAsync _driverPaymentRepository;
        private readonly ICorporateTypeRepositoryAsync _corporateTypeRepository;
        private readonly ITitleRepositoryAsync _titleRepository;
        private readonly ICountryRepositoryAsync _countryRepository;
        private readonly IProvinceRepositoryAsync _provinceRepository;
        private readonly IDistrictRepositoryAsync _districtRepository;
        private readonly ISubdistrictRepositoryAsync _subdistrictRepository;
        private readonly IMapper _mapper;
        private readonly IConfiguration _configuration;
        private readonly IPaymentTypeRepositoryAsync _paymentTypeRepository;
        public RegisterCommandHandler(ICustomerRepositoryAsync customerRepository, ICustomerAddressRepositoryAsync customerAddressRepository, ICustomerFileRepositoryAsync customerFileRepository,
            IDriverRepositoryAsync driverRepository, IDriverAddressRepositoryAsync driverAddressRepository, IDriverFileRepositoryAsync driverFileRepository
            , ICorporateTypeRepositoryAsync corporateTypeRepository, ITitleRepositoryAsync titleRepository, ICustomerTermAndConditionRepositoryAsync customerTermAndConditionRepository, IDriverTermAndConditionRepositoryAsync driverTermAndConditionRepository
            , ICountryRepositoryAsync countryRepository, IProvinceRepositoryAsync provinceRepository, IDistrictRepositoryAsync districtRepository, ISubdistrictRepositoryAsync subdistrictRepository, IMapper mapper
            , IConfiguration configuration, IDummyWalletRepositoryAsync dummyWalletRepository
            , ICustomerPaymentRepositoryAsync customerPaymentRepository, IDriverPaymentRepositoryAsync driverPaymentRepository, IPaymentTypeRepositoryAsync paymentTypeRepository
            //, ICustomerLevelRepositoryAsync customerLevelRepository, IDriverLevelRepositoryAsync driverLevelRepository
            )
        {
            _customerRepository = customerRepository;
            _customerAddressRepository = customerAddressRepository;
            _customerFileRepository = customerFileRepository;
            _customerTermAndConditionRepository = customerTermAndConditionRepository;
            _driverRepository = driverRepository;
            _driverAddressRepository = driverAddressRepository;
            _driverFileRepository = driverFileRepository;
            _driverTermAndConditionRepository = driverTermAndConditionRepository;
            _corporateTypeRepository = corporateTypeRepository;
            _titleRepository = titleRepository;
            _countryRepository = countryRepository;
            _provinceRepository = provinceRepository;
            _districtRepository = districtRepository;
            _subdistrictRepository = subdistrictRepository;
            _mapper = mapper;
            _configuration = configuration;
            _paymentTypeRepository = paymentTypeRepository;
            //_customerLevelRepository = customerLevelRepository;
            //_driverLevelRepository = driverLevelRepository;
            _customerPaymentRepository = customerPaymentRepository;
            _driverPaymentRepository = driverPaymentRepository;
        }

        public async Task<Response<Guid>> Handle(RegisterCommand request, CancellationToken cancellationToken)
        {
            var getContentPath = _configuration.GetSection("ContentPath");
            var contentPath = getContentPath.Value;
            try
            {
                Guid returnValue = new Guid();
                CorporateType corporateType = (await _corporateTypeRepository.FindByCondition(x => x.Id.Equals(request.CorporateTypeId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                Title title = (await _titleRepository.FindByCondition(x => x.Id.Equals(request.TitleId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                Title contactPersonTitle = (await _titleRepository.FindByCondition(x => x.Id.Equals(request.ContactPersonTitleId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                Country country = (await _countryRepository.FindByCondition(x => x.Id == request.CountryId ).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                Province province = (await _provinceRepository.FindByCondition(x => x.Id == request.ProvinceId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                District district = (await _districtRepository.FindByCondition(x => x.Id == request.DistrictId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                Subdistrict subdistrict = (await _subdistrictRepository.FindByCondition(x => x.Id == request.SubdistrictId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                var paymentType = await _paymentTypeRepository.GetAllAsync();
                string phoneNumber = request.PhoneNumber != null ? request.PhoneNumber.Trim() : "";
                if (request.PhoneCode == "+66" && request.PhoneNumber != null && request.PhoneNumber.Trim().Length > 9 && request.PhoneNumber.Trim().First() == '0')
                {
                    phoneNumber = request.PhoneNumber.Trim().Remove(0, 1);
                }

                if (request.UserType.Trim().ToLower() == "customer")
                {
                    Customer customer = new Customer
                    {
                        Password = request.Password,
                        CustomerType = request.Type,
                        CorporateType = corporateType,
                        Title = title,
                        FirstName = request.FirstName,
                        MiddleName = request.MiddleName,
                        LastName = request.LastName,
                        Name = request.Name,
                        IdentityNumber = request.IdentityNumber,
                        ContactPersonTitle = contactPersonTitle,
                        ContactPersonFirstName = request.ContactPersonFirstName,
                        ContactPersonMiddleName = request.ContactPersonMiddleName,
                        ContactPersonLastName = request.ContactPersonLastName,
                        Birthday = request.Birthday,
                        PhoneCode = request.PhoneCode,
                        PhoneNumber = phoneNumber,
                        Email = request.Email,
                        Facbook = request.Facbook,
                        Line = request.Line,
                        Twitter = request.Twitter,
                        Whatapp = request.Whatapp,
                        Level = 1,
                        Rating = "0",
                        Grade = "E",
                        Star = 0,
                        RequestCarPerYear = 0,
                        RequestCarPerMonth = 0,
                        CancelAmountPerYear = 0,
                        CancelAmountPerMonth = 0,
                        OrderValuePerYear = 0,
                        OrderValuePerMonth = 0,
                        Status = true,
                        VerifyStatus = 0,
                        IsRegister = true,
                        IsDelete = false,
                        Created = DateTime.Now,
                        Modified = DateTime.Now,
                        CreatedBy = "",
                        ModifiedBy = ""
                    };
                    var customerObject = await _customerRepository.AddAsync(customer);
                    if (customerObject != null && customerObject.Id != null)
                    {
                        //CustomerLevel customerLevel = new CustomerLevel
                        //{
                        //    CustomerId = customerObject.Id,
                        //    Level = 1,
                        //    RequestCar = 0,
                        //    Star = 0,
                        //    CancelAmount = 0,
                        //    OrderValue = 0,
                        //    Created = DateTime.Now,
                        //    CreatedBy = customerObject.Id.ToString(),
                        //    Modified = DateTime.Now,
                        //    ModifiedBy = customerObject.Id.ToString()
                        //};
                        //var customerLevelObject = await _customerLevelRepository.AddAsync(customerLevel);
                        //CustomerAddress customerAddress = new CustomerAddress
                        //{
                        //    CustomerId = customerObject.Id,
                        //    Country = country,
                        //    Province = province,
                        //    District = district,
                        //    Subdistrict = subdistrict,
                        //    PostCode = request.PostCode,
                        //    Road = request.Road,
                        //    Alley = request.Alley,
                        //    Address = request.Address,
                        //    Branch = "",
                        //    AddressType = "register",
                        //    AddressName = "",
                        //    ContactPerson = "",
                        //    ContactPhoneNumber = "",
                        //    ContactEmail = "",
                        //    Maps = "",
                        //    Sequence = 1,
                        //    IsMainData = true,
                        //    Created = DateTime.UtcNow,
                        //    Modified = DateTime.UtcNow,
                        //    CreatedBy = customerObject.Id.ToString(),
                        //    ModifiedBy = customerObject.Id.ToString()
                        //};
                        //var customerAddressObject = await _customerAddressRepository.AddAsync(customerAddress);
                        //CustomerAddress customerTaxAddress = new CustomerAddress
                        //{
                        //    CustomerId = customerObject.Id,
                        //    Country = country,
                        //    Province = province,
                        //    District = district,
                        //    Subdistrict = subdistrict,
                        //    PostCode = request.PostCode,
                        //    Road = request.Road,
                        //    Alley = request.Alley,
                        //    Address = request.Address,
                        //    Branch = "",
                        //    AddressType = "tax",
                        //    AddressName = "",
                        //    ContactPerson = "",
                        //    ContactPhoneNumber = "",
                        //    ContactEmail = "",
                        //    Maps = "",
                        //    Sequence = 1,
                        //    IsMainData = true,
                        //    Created = DateTime.UtcNow,
                        //    Modified = DateTime.UtcNow,
                        //    CreatedBy = customerObject.Id.ToString(),
                        //    ModifiedBy = customerObject.Id.ToString()
                        //};
                        //var customerTaxAddressObject = await _customerAddressRepository.AddAsync(customerTaxAddress);
                        string folderPath = contentPath + "customer/" + customerObject.Id.ToString();
                        if (!Directory.Exists(folderPath))
                        {
                            Directory.CreateDirectory(folderPath);
                        }
                        int fileCount = 0;
                        if (request.files != null)
                        {
                            foreach (IFormFile file in request.files)
                            {
                                fileCount = fileCount + 1;
                                string filePath = Path.Combine(folderPath, file.FileName);
                                using (FileStream fileStream = new FileStream(filePath, FileMode.Create))
                                {
                                    await file.CopyToAsync(fileStream);
                                    FileInfo fi = new FileInfo(filePath);
                                    string documentType = GetDocumentType(request, file.FileName);

                                    CustomerFile customerFile = new CustomerFile
                                    {
                                        CustomerId = customerObject.Id,
                                        FileName = file.FileName,
                                        ContentType = file.ContentType,
                                        FilePath = "customer/" + customerObject.Id.ToString() + "/" + file.FileName,
                                        DirectoryPath = "customer/" + customerObject.Id.ToString() + "/",
                                        FileEXT = fi.Extension,
                                        DocumentType = documentType,
                                        Sequence = fileCount,
                                        IsApprove = documentType == "profilepicture" ? true : false,
                                        IsDelete = false,
                                        Created = DateTime.UtcNow,
                                        Modified = DateTime.UtcNow,
                                        CreatedBy = customerObject.Id.ToString(),
                                        ModifiedBy = customerObject.Id.ToString()
                                    };
                                    var customerFileObject = await _customerFileRepository.AddAsync(customerFile);
                                }
                            }
                        }

                        if (request.TermAndConditions != null)
                        {
                            foreach (CreateTermAndConditionViewModel termAndConditionViewModel in request.TermAndConditions)
                            {
                                CustomerTermAndCondition termAndCondition = new CustomerTermAndCondition
                                {
                                    Customer = customerObject,
                                    TermAndConditionId = termAndConditionViewModel.Id,
                                    Name_TH = termAndConditionViewModel.Name_TH,
                                    Name_ENG = termAndConditionViewModel.Name_ENG,
                                    Section = termAndConditionViewModel.Section,
                                    Version = termAndConditionViewModel.Version,
                                    IsAccept = termAndConditionViewModel.IsAccept,
                                    IsUserAccept = termAndConditionViewModel.IsUserAccept,
                                    Created = DateTime.Now,
                                    Modified = DateTime.Now,
                                    CreatedBy = customerObject.Id.ToString(),
                                    ModifiedBy = customerObject.Id.ToString()
                                };
                                await _customerTermAndConditionRepository.AddAsync(termAndCondition);

                            }

                        }
                        #region Wallet

                        foreach (PaymentType type in paymentType)
                        {
                            CustomerPayment paymentTemp = new CustomerPayment
                            {
                                Customer = customerObject,
                                Type = type,
                                Value = "000000000000",
                                Amount = 50000,
                                Description = "ข้อมูลทดสอบ"
                                //paymentTemp1.Channel = 
                            };
                            await _customerPaymentRepository.AddAsync(paymentTemp);
                        }

                        //DummyWallet dummyWallet = new DummyWallet()
                        //{
                        //    Module = "customer",
                        //    UserId = customerObject.Id,
                        //    Amount = 50000M
                        //};
                        //await _dummyWalletRepository.AddAsync(dummyWallet);
                        #endregion
                        returnValue = customerObject.Id;
                    }
                }
                else if (request.UserType.Trim().ToLower() == "driver")
                {
                    if (request.IsCorparateDriver)
                    {
                        var existDriver = (await _driverRepository.FindByCondition(x => x.Id == request.DriverId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                        if (existDriver != null)
                        {
                            returnValue = existDriver.Id;
                            existDriver.Password = request.Password;
                            //if (request.TitleId != 0 && request.TitleId != null) existDriver.Title = title;
                            //if (request.FirstName != null && request.FirstName.Trim() != "") existDriver.FirstName = request.FirstName;
                            //if (request.MiddleName != null && request.MiddleName.Trim() != "") existDriver.MiddleName = request.MiddleName;
                            //if (request.LastName != null && request.LastName.Trim() != "") existDriver.LastName = request.LastName;
                            //if (request.Name != null && request.Name.Trim() != "") existDriver.Name = request.Name;
                            //if (request.IdentityType != null && request.IdentityType != 0) existDriver.IdentityType = request.IdentityType;
                            //if (request.IdentityNumber != null && request.IdentityNumber.Trim() != "") existDriver.IdentityNumber = request.IdentityNumber;
                            //if (request.ContactPersonTitleId != null && request.ContactPersonTitleId != 0) existDriver.ContactPersonTitle = contactPersonTitle;
                            //if (request.ContactPersonFirstName != null && request.ContactPersonFirstName.Trim() != "") existDriver.ContactPersonFirstName = request.ContactPersonFirstName;
                            //if (request.ContactPersonMiddleName != null && request.ContactPersonMiddleName.Trim() != "") existDriver.ContactPersonMiddleName = request.ContactPersonMiddleName;
                            //if (request.ContactPersonLastName != null && request.ContactPersonLastName.Trim() != "") existDriver.ContactPersonLastName = request.ContactPersonLastName;
                            //if (request.Birthday != null) existDriver.Birthday = request.Birthday;
                            //if (request.PhoneCode != null && request.PhoneCode.Trim() != "") existDriver.PhoneCode = request.PhoneCode;
                            //if (phoneNumber != null && phoneNumber.Trim() != "") existDriver.PhoneNumber = phoneNumber;
                            //if (request.Email != null && request.Email.Trim() != "") existDriver.Email = request.Email;
                            //if (request.Facbook != null && request.Facbook.Trim() != "") existDriver.Facbook = request.Facbook;
                            //if (request.Line != null && request.Line.Trim() != "") existDriver.Line = request.Line;
                            //if (request.Twitter != null && request.Twitter.Trim() != "") existDriver.Twitter = request.Twitter;
                            //if (request.Whatapp != null && request.Whatapp.Trim() != "") existDriver.Whatapp = request.Whatapp;

                            existDriver.IsRegister = true;
                            existDriver.Status = true;
                            existDriver.Created = DateTime.UtcNow;
                            existDriver.Modified = DateTime.UtcNow;
                            //existDriver.CreatedBy = "xxxxxxx";
                            existDriver.ModifiedBy = existDriver.Id.ToString();

                            //DriverLevel driverLevel = new DriverLevel
                            //{
                            //    DriverId = existDriver.Id,
                            //    Level = 1,
                            //    Star = 0,
                            //    AcceptJob = 0,
                            //    ComplaintPerMonth = 0,
                            //    RejectPerMonth = 0,
                            //    CancelPerMonth = 0,
                            //    ComplaintPerYear = 0,
                            //    RejectPerYear = 0,
                            //    CancelPerYear = 0,
                            //    InsuranceValue = 0,
                            //    Created = DateTime.Now,
                            //    Modified = DateTime.Now,
                            //    CreatedBy = existDriver.Id.ToString(),
                            //    ModifiedBy = existDriver.Id.ToString()
                            //};
                            //var driverLevelObject = await _driverLevelRepository.AddAsync(driverLevel);

                            var driverAddress = existDriver.Addresses.Where(x => x.AddressType == "register").FirstOrDefault();
                            if (driverAddress == null)
                            {
                                DriverAddress address = new DriverAddress
                                {
                                    DriverId = existDriver.Id,
                                    Country = country,
                                    Province = province,
                                    District = district,
                                    Subdistrict = subdistrict,
                                    PostCode = request.PostCode != null ? request.PostCode : "",
                                    Road = request.Road != null ? request.Road : "",
                                    Alley = request.Alley != null ? request.Alley : "",
                                    Address = request.Address != null ? request.Address : "",
                                    Branch = "",
                                    AddressType = "register",
                                    AddressName = "",
                                    ContactPerson = "",
                                    ContactPhoneNumber = "",
                                    ContactEmail = "",
                                    IsMainData = true,
                                    Maps = "",
                                    Sequence = 1,
                                    Created = DateTime.UtcNow,
                                    Modified = DateTime.UtcNow,
                                    CreatedBy = existDriver.Id.ToString(),
                                    ModifiedBy = existDriver.Id.ToString()
                                };
                                existDriver.Addresses.Add(address);
                            }
                            else
                            {
                                if (country != null) driverAddress.Country = country;
                                if (province != null) driverAddress.Province = province;
                                if (district != null) driverAddress.District = district;
                                if (subdistrict != null) driverAddress.Subdistrict = subdistrict;
                                if (request.PostCode != null && request.PostCode.Trim() != "") driverAddress.PostCode = request.PostCode;
                                if (request.Road != null && request.Road.Trim() != "") driverAddress.Road = request.Road;
                                if (request.Alley != null && request.Alley.Trim() != "") driverAddress.Alley = request.Alley;
                                if (request.Address != null && request.Address.Trim() != "") driverAddress.Address = request.Address;
                                driverAddress.IsMainData = true;

                            }
                            var taxAddress = existDriver.Addresses.Where(x => x.AddressType == "tax").FirstOrDefault();
                            if (taxAddress == null)
                            {
                                DriverAddress address = new DriverAddress
                                {
                                    DriverId = existDriver.Id,
                                    Country = country,
                                    Province = province,
                                    District = district,
                                    Subdistrict = subdistrict,
                                    PostCode = request.PostCode != null ? request.PostCode : "",
                                    Road = request.Road != null ? request.Road : "",
                                    Alley = request.Alley != null ? request.Alley : "",
                                    Address = request.Address != null ? request.Address : "",
                                    Branch = "",
                                    AddressType = "register-tax",
                                    AddressName = "",
                                    ContactPerson = "",
                                    ContactPhoneNumber = "",
                                    ContactEmail = "",
                                    IsMainData = true,
                                    Maps = "",
                                    Sequence = 1,
                                    Created = DateTime.UtcNow,
                                    Modified = DateTime.UtcNow,
                                    CreatedBy = existDriver.Id.ToString(),
                                    ModifiedBy = existDriver.Id.ToString()
                                };
                                existDriver.Addresses.Add(address);
                            }

                            string folderPath = contentPath + "driver/" + existDriver.Id.ToString();
                            if (!Directory.Exists(folderPath))
                            {
                                Directory.CreateDirectory(folderPath);
                            }
                            int fileCount = 0;
                            if (request.files != null)
                            {
                                foreach (IFormFile file in request.files)
                                {
                                    fileCount = fileCount + 1;
                                    string filePath = Path.Combine(folderPath, file.FileName);
                                    using (FileStream fileStream = new FileStream(filePath, FileMode.Create))
                                    {
                                        await file.CopyToAsync(fileStream);
                                        FileInfo fi = new FileInfo(filePath);
                                        string documentType = GetDocumentType(request, file.FileName);

                                        DriverFile driverFile = new DriverFile
                                        {
                                            DriverId = existDriver.Id,
                                            FileName = file.FileName,
                                            ContentType = file.ContentType,
                                            FilePath = "driver/" + existDriver.Id.ToString() + "/" + file.FileName,
                                            DirectoryPath = "driver/" + existDriver.Id.ToString() + "/",
                                            FileEXT = fi.Extension,
                                            DocumentType = documentType,
                                            Sequence = fileCount,
                                            IsApprove = documentType == "profilepicture" ? true : false,
                                            IsDelete = false,
                                            CreatedBy = existDriver.Id.ToString(),
                                            ModifiedBy = existDriver.Id.ToString()
                                        };
                                        var driverFileObject = await _driverFileRepository.AddAsync(driverFile);
                                    }
                                }
                            }

                            #region Wallet
                            foreach (PaymentType type in paymentType)
                            {
                                DriverPayment paymentTemp = new DriverPayment
                                {
                                    Driver = existDriver,
                                    Type = type,
                                    Value = "000000000000",
                                    Amount = 50000,
                                    Description = "ข้อมูลทดสอบ"
                                    //paymentTemp1.Channel = 
                                };
                                await _driverPaymentRepository.AddAsync(paymentTemp);
                            }
                            #endregion
                            await _driverRepository.UpdateAsync(existDriver);
                            return new Response<Guid>(returnValue);
                        }
                    }

                    Driver driver = new Driver
                    {
                        Password = request.Password,
                        DriverType = request.Type,
                        CorporateType = corporateType,
                        Title = title,
                        FirstName = request.FirstName,
                        MiddleName = request.MiddleName,
                        LastName = request.LastName,
                        Name = request.Name,
                        IdentityNumber = request.IdentityNumber,
                        ContactPersonTitle = contactPersonTitle,
                        ContactPersonFirstName = request.ContactPersonFirstName,
                        ContactPersonMiddleName = request.ContactPersonMiddleName,
                        ContactPersonLastName = request.ContactPersonLastName,
                        Birthday = request.Birthday,
                        PhoneCode = request.PhoneCode,
                        PhoneNumber = phoneNumber,
                        Email = request.Email,
                        Facbook = request.Facbook,
                        Line = request.Line,
                        Twitter = request.Twitter,
                        Whatapp = request.Whatapp,
                        Level = 1,
                        Rating = "0",
                        Grade = "E",
                        Star = 0,
                        AcceptJobPerYear = 0,
                        AcceptJobPerMonth = 0,
                        ComplaintPerYear = 0,
                        ComplaintPerMonth = 0,
                        RejectPerYear = 0,
                        RejectPerMonth = 0,
                        CancelPerYear = 0,
                        CancelPerMonth = 0,
                        InsuranceValue = 0,
                        VerifyStatus = 0,
                        Status = true,
                        IsRegister = true,
                        IsDelete = false,
                        Created = DateTime.Now,
                        Modified = DateTime.Now,
                        CreatedBy = "",
                        ModifiedBy = ""
                    };
                    var driverObject = await _driverRepository.AddAsync(driver);
                    if (driverObject != null && driverObject.Id != null)
                    {
                        try
                        {
                            //DriverLevel driverLevel = new DriverLevel
                            //{
                            //    DriverId = driverObject.Id,
                            //    Level = 1,
                            //    Star = 0,
                            //    AcceptJob = 0,
                            //    ComplaintPerMonth = 0,
                            //    RejectPerMonth = 0,
                            //    CancelPerMonth = 0,
                            //    ComplaintPerYear = 0,
                            //    RejectPerYear = 0,
                            //    CancelPerYear = 0,
                            //    InsuranceValue = 0,
                            //    Created = DateTime.Now,
                            //    Modified = DateTime.Now,
                            //    CreatedBy = driverObject.Id.ToString(),
                            //    ModifiedBy = driverObject.Id.ToString()
                            //};
                            //var driverLevelObject = await _driverLevelRepository.AddAsync(driverLevel);

                            DriverAddress driverAddress = new DriverAddress
                            {
                                DriverId = driverObject.Id,
                                Country = country,
                                Province = province,
                                District = district,
                                Subdistrict = subdistrict,
                                PostCode = request.PostCode,
                                Road = request.Road,
                                Alley = request.Alley,
                                Address = request.Address,
                                Branch = "",
                                AddressType = "register",
                                AddressName = "",
                                ContactPerson = "",
                                ContactPhoneNumber = "",
                                ContactEmail = "",
                                Maps = "",
                                Sequence = 1,
                                IsMainData = true,
                                Created = DateTime.UtcNow,
                                Modified = DateTime.UtcNow,
                                CreatedBy = driverObject.Id.ToString(),
                                ModifiedBy = driverObject.Id.ToString()
                            };
                            var driverAddressObject = await _driverAddressRepository.AddAsync(driverAddress);
                            DriverAddress driverTaxAddress = new DriverAddress
                            {
                                DriverId = driverObject.Id,
                                Country = country,
                                Province = province,
                                District = district,
                                Subdistrict = subdistrict,
                                PostCode = request.PostCode,
                                Road = request.Road,
                                Alley = request.Alley,
                                Address = request.Address,
                                Branch = "",
                                AddressType = "register-tax",
                                AddressName = "",
                                ContactPerson = "",
                                ContactPhoneNumber = "",
                                ContactEmail = "",
                                Maps = "",
                                Sequence = 1,
                                IsMainData = true,
                                Created = DateTime.UtcNow,
                                Modified = DateTime.UtcNow,
                                CreatedBy = driverObject.Id.ToString(),
                                ModifiedBy = driverObject.Id.ToString()
                            };
                            var driverTaxAddressObject = await _driverAddressRepository.AddAsync(driverTaxAddress);
                            string folderPath = contentPath + "driver/" + driverObject.Id.ToString();
                            if (!Directory.Exists(folderPath))
                            {
                                Directory.CreateDirectory(folderPath);
                            }
                            if (request.files != null)
                            {
                                int fileCount = 0;
                                foreach (IFormFile file in request.files)
                                {
                                    fileCount = fileCount + 1;
                                    string filePath = Path.Combine(folderPath, file.FileName);
                                    using (FileStream fileStream = new FileStream(filePath, FileMode.Create))
                                    {
                                        await file.CopyToAsync(fileStream);
                                        FileInfo fi = new FileInfo(filePath);
                                        string documentType = GetDocumentType(request, file.FileName);

                                        DriverFile driverFile = new DriverFile
                                        {
                                            DriverId = driverObject.Id,
                                            FileName = file.FileName,
                                            ContentType = file.ContentType,
                                            FilePath = "driver/" + driverObject.Id.ToString() + "/" + file.FileName,
                                            DirectoryPath = "driver/" + driverObject.Id.ToString() + "/",
                                            FileEXT = fi.Extension,
                                            DocumentType = documentType,
                                            Sequence = fileCount,
                                            IsApprove = documentType == "profilepicture" ? true : false,
                                            IsDelete = false,
                                            Created = DateTime.UtcNow,
                                            Modified = DateTime.UtcNow,
                                            CreatedBy = driverObject.Id.ToString(),
                                            ModifiedBy = driverObject.Id.ToString()
                                        };
                                        var driverFileObject = await _driverFileRepository.AddAsync(driverFile);
                                    }
                                }
                            }

                            if (request.TermAndConditions != null)
                            {
                                foreach (CreateTermAndConditionViewModel termAndConditionViewModel in request.TermAndConditions)
                                {
                                    DriverTermAndCondition termAndCondition = new DriverTermAndCondition
                                    {
                                        Driver = driverObject,
                                        TermAndConditionId = termAndConditionViewModel.Id,
                                        Name_TH = termAndConditionViewModel.Name_TH,
                                        Name_ENG = termAndConditionViewModel.Name_ENG,
                                        Section = termAndConditionViewModel.Section,
                                        Version = termAndConditionViewModel.Version,
                                        IsAccept = termAndConditionViewModel.IsAccept,
                                        IsUserAccept = termAndConditionViewModel.IsUserAccept,
                                        Created = DateTime.Now,
                                        Modified = DateTime.Now,
                                        CreatedBy = driverObject.Id.ToString(),
                                        ModifiedBy = driverObject.Id.ToString()
                                    };
                                    await _driverTermAndConditionRepository.AddAsync(termAndCondition);
                                }

                            }
                            #region Wallet
                            foreach (PaymentType type in paymentType)
                            {
                                DriverPayment paymentTemp = new DriverPayment
                                {
                                    Driver = driverObject,
                                    Type = type,
                                    Value = "000000000000",
                                    Amount = 50000,
                                    Description = "ข้อมูลทดสอบ"
                                    //paymentTemp1.Channel = 
                                };
                                await _driverPaymentRepository.AddAsync(paymentTemp);
                            }
                            //DummyWallet dummyWallet = new DummyWallet()
                            //{
                            //    Module = "driver",
                            //    UserId = driverObject.Id,
                            //    Amount = 50000M
                            //};
                            //await _dummyWalletRepository.AddAsync(dummyWallet);
                            #endregion
                            returnValue = driverObject.Id;
                        }
                        catch (Exception ex)
                        {
                            throw ex;
                        }
                    }

                }
                return new Response<Guid>(returnValue);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GetDocumentType(RegisterCommand request, string fileName)
        {
            string result = "";
            if (request.idCard != null)
            {
                string[] idCard = request.idCard.Split(",");
                var existIdCard = idCard.Where(x => x == fileName).FirstOrDefault();
                if (existIdCard != null)
                {
                    result = "IdCard";
                    return result;
                }
            }
            if (request.companyCertificate != null)
            {
                string[] companyCertificate = request.companyCertificate.Split(",");
                var existCompanyCertificate = companyCertificate.Where(x => x == fileName).FirstOrDefault();
                if (existCompanyCertificate != null)
                {
                    result = "CompanyCertificate";
                    return result;
                }
            }
            if (request.drivingLicense != null)
            {
                string[] drivingLicense = request.drivingLicense.Split(",");
                var existDrivingLicensee = drivingLicense.Where(x => x == fileName).FirstOrDefault();
                if (existDrivingLicensee != null)
                {
                    result = "DrivingLicense";
                    return result;
                }
            }
            if (request.driverPicture != null)
            {
                string[] driverPicture = request.driverPicture.Split(",");
                var existDriverPicture = driverPicture.Where(x => x == fileName).FirstOrDefault();
                if (existDriverPicture != null)
                {
                    result = "DriverPicture";
                    return result;
                }
            }
            if (request.NP20 != null)
            {
                string[] NP20 = request.NP20.Split(",");
                var existNP20 = NP20.Where(x => x == fileName).FirstOrDefault();
                if (existNP20 != null)
                {
                    result = "NP20";
                    return result;
                }
            }
            if (request.ProfilePicture != null)
            {
                string[] ProfilePicture = request.ProfilePicture.Split(",");
                var existProfilePicture = ProfilePicture.Where(x => x == fileName).FirstOrDefault();
                if (existProfilePicture != null)
                {
                    result = "profilepicture";
                    return result;
                }
            }
            return result;
        }
    }
}
