﻿using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using iTrans.Transportation.Application.Helper;
using Microsoft.Extensions.Configuration;

namespace iTrans.Transportation.Application.Features.Register
{
    public class RegisterVerifyIdentityNumberQuery : IRequest<Response<bool>>
    {
        public string Module { get; set; }
        public string Type { get; set; }
        public string IdentityNumber { get; set; }
    }
    public class RegisterVerifyIdentityNumberQueryHandler : IRequestHandler<RegisterVerifyIdentityNumberQuery, Response<bool>>
    {
        private readonly ICustomerRepositoryAsync _customerRepository;
        private readonly IDriverRepositoryAsync _driverRepository;
        private readonly IConfiguration _configuration;
        private readonly IMapper _mapper;

        public RegisterVerifyIdentityNumberQueryHandler(ICustomerRepositoryAsync customerdRepository, IDriverRepositoryAsync driverRepository, IConfiguration configuration, IMapper mapper)
        {
            _customerRepository = customerdRepository;
            _driverRepository = driverRepository;
            _configuration = configuration;
            _mapper = mapper;
        }

        public async Task<Response<bool>> Handle(RegisterVerifyIdentityNumberQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var isDevelopment = _configuration.GetSection("IsDevelopment");
                bool result = false;
                if (request.IdentityNumber == null || request.IdentityNumber == "")
                {
                    return new Response<bool>(false);
                }
                if (request.Module != null && request.Module.Trim() != "")
                {
                    string type = "";
                    if (request.Type != null)
                    {
                        type = request.Type.Trim().ToLower();
                    }
                    if (type == "passport")
                    {
                        result = true;
                    }
                    else if (type == "corporatenumber")
                    {
                        result = true;
                    }
                    else
                    {
                        string identityNumber = AESMgr.Decrypt(request.IdentityNumber);

                        Regex rexPersonal = new Regex(@"^[0-9]{13}$");
                        if (rexPersonal.IsMatch(identityNumber))
                        {
                            int sum = 0;

                            for (int i = 0; i < 12; i++)
                            {
                                sum += int.Parse(identityNumber[i].ToString()) * (13 - i);
                            }

                            result = (int.Parse(identityNumber[12].ToString()) == ((11 - (sum % 11)) % 10));
                        }
                        if (isDevelopment != null && isDevelopment.Value == "true")
                        {
                            result = true;
                        }
                    }
                    Guid newGuid = new Guid();
                    if (result)
                    {
                        if (request.Module.Trim().ToLower() == "customer")
                        {
                            var customer = (await _customerRepository.FindByCondition(x => x.IdentityNumber == request.IdentityNumber && x.IsDelete == false).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                            if (customer == null)
                            {
                                result = true;
                            }
                            else
                            {
                                result = false;
                            }
                        }
                        else if (request.Module.Trim().ToLower() == "driver")
                        {
                            var driver = (await _driverRepository.FindByCondition(x => x.IdentityNumber == request.IdentityNumber && x.IsDelete == false && ( x.Corparate == null || x.Corparate == newGuid)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                            if (driver == null)
                            {
                                result = true;
                            }
                            else
                            {
                                result = false;
                            }
                        }
                    }
                }

              
                return new Response<bool>(result);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
