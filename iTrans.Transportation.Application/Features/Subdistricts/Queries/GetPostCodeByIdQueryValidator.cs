﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using iTrans.Transportation.Application.Interfaces.Repositories;
using FluentValidation;

namespace iTrans.Transportation.Application.Features.Subdistricts.Queries
{
    public class GetPostCodeByIdQueryValidator : AbstractValidator<GetPostCodeByIdQuery>
    {
        private readonly ISubdistrictRepositoryAsync subdistrictRepository;

        public GetPostCodeByIdQueryValidator(ISubdistrictRepositoryAsync subdistrictRepository)
        {
            this.subdistrictRepository = subdistrictRepository;
            RuleFor(p => p.Id)
               .NotEmpty().WithMessage("{PropertyName} is required.")
               .NotNull()
               .MustAsync(IsSubdistrictExists).WithMessage("{PropertyName} not exists.");
        }

        private async Task<bool> IsSubdistrictExists(int subdistrictId, CancellationToken cancellationToken)
        {
            var subdistrictObject = (await subdistrictRepository.FindByCondition(x => x.Id == subdistrictId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            if (subdistrictObject != null)
            {
                return true;
            }
            return false;
        }
    }
}
