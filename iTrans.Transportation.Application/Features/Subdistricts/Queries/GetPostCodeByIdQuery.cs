﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.Subdistrict;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.Subdistricts.Queries
{
    public class GetPostCodeByIdQuery : IRequest<Response<IEnumerable<string>>>
    {
        public int Id { get; set; }
    }
    public class GetPostCodeByIdQueryHandler : IRequestHandler<GetPostCodeByIdQuery, Response<IEnumerable<string>>>
    {
        private readonly ISubdistrictRepositoryAsync _subdistrictRepository;
        private readonly IMapper _mapper;
        public GetPostCodeByIdQueryHandler(ISubdistrictRepositoryAsync subdistrictRepository, IMapper mapper)
        {
            _subdistrictRepository = subdistrictRepository;
            _mapper = mapper;
        }
        public async Task<Response<IEnumerable<string>>> Handle(GetPostCodeByIdQuery request, CancellationToken cancellationToken)
        {
            var subdistrict = (await _subdistrictRepository.FindByCondition(x => x.Id== request.Id).ConfigureAwait(false)).AsQueryable().ToList();
            var subdistrictObject = _mapper.Map< IEnumerable<SubdistrictViewModel>>(subdistrict);
            return new Response<IEnumerable<string>>(subdistrictObject.Select(x => x.postCode));
        }
    }
}
