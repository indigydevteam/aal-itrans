﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using iTrans.Transportation.Application.Interfaces.Repositories;
using FluentValidation;

namespace iTrans.Transportation.Application.Features.Subdistricts.Queries
{
    public class GetSubdistrictByDIstrictQueryValidator : AbstractValidator<GetSubdistrictByDistrictQuery>
    {
        private readonly ISubdistrictRepositoryAsync subdistrictRepository;

        public GetSubdistrictByDIstrictQueryValidator(ISubdistrictRepositoryAsync subdistrictRepository)
        {
            this.subdistrictRepository = subdistrictRepository;
            RuleFor(p => p.DistrictId)
               .NotEmpty().WithMessage("{PropertyName} is required.")
               .NotNull()
               .MustAsync(IsSubdistrictExists).WithMessage("{PropertyName} not exists.");
        }

        private async Task<bool> IsSubdistrictExists(int DistrictId, CancellationToken cancellationToken)
        {
            var subdistrictObject = (await subdistrictRepository.FindByCondition(x => x.District.Id == DistrictId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            if (subdistrictObject != null)
            {
                return true;
            }
            return false;
        }
    }
}
