﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.Subdistrict;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.Subdistricts.Queries
{
    public class GetSubdistrictByDistrictQuery : IRequest<Response<IEnumerable<SubdistrictViewModel>>>
    {
        public int DistrictId { set; get; }
    }
    public class GetSubdistrictByDistrictQueryHandler : IRequestHandler<GetSubdistrictByDistrictQuery, Response<IEnumerable<SubdistrictViewModel>>>
    {
        private readonly ISubdistrictRepositoryAsync _subdistrictRepository;
        private readonly IMapper _mapper;
        public GetSubdistrictByDistrictQueryHandler(ISubdistrictRepositoryAsync subdistrictRepository, IMapper mapper)
        {
            _subdistrictRepository = subdistrictRepository;
            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<SubdistrictViewModel>>> Handle(GetSubdistrictByDistrictQuery request, CancellationToken cancellationToken)
        {
            try
            {
                CultureInfo ci = CultureInfo.GetCultureInfo("th");
                bool ignoreCase = true; //whether comparison should be case-sensitive
                StringComparer comp = StringComparer.Create(ci, ignoreCase);

                var subdistrict = (await _subdistrictRepository.FindByCondition(x => x.District.Id == request.DistrictId && x.Active == true).ConfigureAwait(false)).AsQueryable().ToList();
                var subdistrictViewModel = _mapper.Map<IEnumerable<SubdistrictViewModel>>(subdistrict);
                return new Response<IEnumerable<SubdistrictViewModel>>(subdistrictViewModel.OrderBy(x => x.name_TH, comp));
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
    }
}
