﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.Subdistrict;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.Subdistricts.Queries
{
    public class GetSubdistrictByIdQuery : IRequest<Response<SubdistrictViewModel>>
    {
        public int Id { get; set; }
    }
    public class GetSubdistrictByIdQueryHandler : IRequestHandler<GetSubdistrictByIdQuery, Response<SubdistrictViewModel>>
    {
        private readonly ISubdistrictRepositoryAsync _subdistrictRepository;
        private readonly IMapper _mapper;
        public GetSubdistrictByIdQueryHandler(ISubdistrictRepositoryAsync subdistrictRepository, IMapper mapper)
        {
            _subdistrictRepository = subdistrictRepository;
            _mapper = mapper;
        }
        public async Task<Response<SubdistrictViewModel>> Handle(GetSubdistrictByIdQuery request, CancellationToken cancellationToken)
        {
            var subdistrictObject = (await _subdistrictRepository.FindByCondition(x => x.Id == request.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            return new Response<SubdistrictViewModel>(_mapper.Map<SubdistrictViewModel>(subdistrictObject));
        }
    }
}
