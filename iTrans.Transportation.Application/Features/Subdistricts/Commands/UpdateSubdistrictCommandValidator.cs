﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;


namespace iTrans.Transportation.Application.Features.Subdistricts.Commands
{
    public class UpdateSubdistrictCommandValidator : AbstractValidator<UpdateSubdistrictCommand>
    {
        private readonly ISubdistrictRepositoryAsync subdistrictRepository;
        public UpdateSubdistrictCommandValidator(ISubdistrictRepositoryAsync subdistrictRepository)
        {
            this.subdistrictRepository = subdistrictRepository;

            RuleFor(p => p.Name_TH)
               .NotEmpty().WithMessage("{PropertyName} is required.")
               .NotNull()
               .MaximumLength(50).WithMessage("{PropertyName} must not exceed 50 characters.");


            RuleFor(p => p.Name_ENG)
                .NotEmpty().WithMessage("{PropertyName} is required.")
                .NotNull()
                .MaximumLength(50).WithMessage("{PropertyName} must not exceed 50 characters.");
        }
    }
}
