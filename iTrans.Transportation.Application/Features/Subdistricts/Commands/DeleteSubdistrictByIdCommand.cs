﻿using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.Subdistricts.Commands
{
    public class DeleteSubdistrictByIdCommand : IRequest<Response<int>>
    {
        public int Id { get; set; }
        public class DeleteSubdistrictByIdCommandHandler : IRequestHandler<DeleteSubdistrictByIdCommand, Response<int>>
        {
            private readonly ISubdistrictRepositoryAsync _subdistrictRepository;
            public DeleteSubdistrictByIdCommandHandler(ISubdistrictRepositoryAsync subdistrictRepository)
            {
                _subdistrictRepository = subdistrictRepository;
            }
            public async Task<Response<int>> Handle(DeleteSubdistrictByIdCommand command, CancellationToken cancellationToken)
            {
                var subdistrict = (await _subdistrictRepository.FindByCondition(x => x.Id == command.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (subdistrict == null)
                {
                    throw new ApiException($"Subdistrict Not Found.");
                }
                else
                {
                    await _subdistrictRepository.DeleteAsync(subdistrict);
                    return new Response<int>(subdistrict.Id);
                }
            }
        }
    }
}
