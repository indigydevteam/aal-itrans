﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;

namespace iTrans.Transportation.Application.Features.Subdistricts.Commands
{
    public class UpdateSubdistrictCommand : IRequest<Response<int>>
    {
        public int Id { get; set; }
        public int DistrictId { get; set; }
        public string Name_TH { get; set; }
        public string Name_ENG { get; set; }
        public string PostCode { get; set; }
        public bool Active { get; set; }
    }

    public class UpdateSubdistrictCommandHandler : IRequestHandler<UpdateSubdistrictCommand, Response<int>>
    {
        private readonly ISubdistrictRepositoryAsync _subdistrictRepository;
        private readonly IDistrictRepositoryAsync _districtRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;

        public UpdateSubdistrictCommandHandler(IDistrictRepositoryAsync districtRepository, ISubdistrictRepositoryAsync subdistrictRepository,IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _districtRepository = districtRepository;
            _subdistrictRepository = subdistrictRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(UpdateSubdistrictCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var subdistrict = (await _subdistrictRepository.FindByCondition(x => x.Id == request.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (subdistrict == null)
                {
                    throw new ApiException($"Subdistrict Not Found.");
                }
                else
                {
                    var district = (await _districtRepository.FindByCondition(x => x.Id == request.DistrictId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                    subdistrict.District = district;
                    subdistrict.Name_TH = request.Name_TH;
                    subdistrict.Name_ENG = request.Name_ENG;
                    subdistrict.PostCode = request.PostCode;
                    subdistrict.Active = request.Active;
                    subdistrict.Modified = DateTime.UtcNow;

                    await _subdistrictRepository.UpdateAsync(subdistrict);
                    var log = new CreateAppLog(_applicationLogRepository);
                    log.Create("Subdistrict", "Subdistrict", "Edit", Newtonsoft.Json.JsonConvert.SerializeObject(subdistrict));
                    return new Response<int>(subdistrict.Id);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
