﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.Subdistrict.Backoffice;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.Subdistricts.Backoffice.Queries
{
    public class BackofficeGetSubdistrictByIdQuery : IRequest<Response<SubdistrictViewModel>>
    {
        public int Id { get; set; }
    }
    public class BackofficeGetSubdistrictByIdQueryHandler : IRequestHandler<BackofficeGetSubdistrictByIdQuery, Response<SubdistrictViewModel>>
    {
        private readonly ISubdistrictRepositoryAsync _subdistrictRepository;
        private readonly IMapper _mapper;
        public BackofficeGetSubdistrictByIdQueryHandler(ISubdistrictRepositoryAsync subdistrictRepository, IMapper mapper)
        {
            _subdistrictRepository = subdistrictRepository;
            _mapper = mapper;
        }
        public async Task<Response<SubdistrictViewModel>> Handle(BackofficeGetSubdistrictByIdQuery request, CancellationToken cancellationToken)
        {
            var subdistrictObject = (await _subdistrictRepository.FindByCondition(x => x.Id.Equals(request.Id)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            return new Response<SubdistrictViewModel>(_mapper.Map<SubdistrictViewModel>(subdistrictObject));
        }
    }
}
