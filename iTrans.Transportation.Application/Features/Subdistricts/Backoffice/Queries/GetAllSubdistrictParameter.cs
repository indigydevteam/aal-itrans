﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.Subdistrict.Backoffice;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;

namespace iTrans.Transportation.Application.Features.Subdistricts.Backoffice.Queries
{
    public class GetAllSubdistrictParameter : IRequest<PagedResponse<IEnumerable<SubdistrictViewModel>>>
    {
        public string Search { set; get; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
    }
    public class GetAllSubdistrictParameterHandler : IRequestHandler<GetAllSubdistrictParameter, PagedResponse<IEnumerable<SubdistrictViewModel>>>
    {
        private readonly ISubdistrictRepositoryAsync _subdistrictRepository;
        private readonly IMapper _mapper;
        private Expression<Func<Subdistrict, bool>> expression;
        public GetAllSubdistrictParameterHandler(ISubdistrictRepositoryAsync subdistrictRepository, IMapper mapper)
        {
            _subdistrictRepository = subdistrictRepository;
            _mapper = mapper;
        }

        public async Task<PagedResponse<IEnumerable<SubdistrictViewModel>>> Handle(GetAllSubdistrictParameter request, CancellationToken cancellationToken)
        {
            var validFilter = _mapper.Map<GetAllSubdistrictParameter>(request);
            if (validFilter.Search != null && validFilter.Search.Trim() != "")
            {
                expression = x => (x.Name_TH.Contains(validFilter.Search.Trim()) || x.Name_ENG.Contains(validFilter.Search.Trim()));
            }
            int itemCount = _subdistrictRepository.GetItemCount(expression);

            //Expression<Func<Subdistrict, bool>> expression = x => x.Name_TH != "";
            var subdistrict = await _subdistrictRepository.FindByConditionWithPage(expression, validFilter.PageNumber, validFilter.PageSize);
            var subdistrictViewModel = _mapper.Map<IEnumerable<SubdistrictViewModel>>(subdistrict);
            return new PagedResponse<IEnumerable<SubdistrictViewModel>>(subdistrictViewModel, validFilter.PageNumber, validFilter.PageSize);
        }
    }
}
