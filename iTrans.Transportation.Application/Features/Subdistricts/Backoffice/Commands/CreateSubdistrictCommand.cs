﻿using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Interfaces.Services;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.Subdistricts.Backoffice.Commands
{
    public partial class CreateSubdistrictCommand : IRequest<Response<int>>
    { 
        public int DistrictId { get; set; }
        public string Name_TH { get; set; }
        public string Name_ENG { get; set; }
        public string PostCode { get; set; }
        public bool Active { get; set; }

    }
    public class CreateSubdistrictCommandHandler : IRequestHandler<CreateSubdistrictCommand, Response<int>>
    {
        private readonly IDistrictRepositoryAsync _districtRepository;
        private readonly ISubdistrictRepositoryAsync _subdistrictRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;
        public CreateSubdistrictCommandHandler(IDistrictRepositoryAsync districtRepository, ISubdistrictRepositoryAsync subdistrictRepository,IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _districtRepository = districtRepository;
            _subdistrictRepository = subdistrictRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(CreateSubdistrictCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var district = (await _districtRepository.FindByCondition(x => x.Id == request.DistrictId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();

                if (district == null)
                {
                    throw new ApiException($"District Not Found.");
                }
                else 
                {
                    var subdistrict = _mapper.Map<Subdistrict>(request);
                    subdistrict.District = district;
                    subdistrict.Created = DateTime.UtcNow;
                    subdistrict.Modified = DateTime.UtcNow;
                    //country.CreatedBy = "xxxxxxx";
                    //country.ModifiedBy = "xxxxxxx";
                    var subdistrictObject = await _subdistrictRepository.AddAsync(subdistrict);
                    var log = new CreateAppLog(_applicationLogRepository);
                    log.Create("Subdistrict", "Subdistrict", "Create", Newtonsoft.Json.JsonConvert.SerializeObject(subdistrict));
                    return new Response<int>(subdistrictObject.Id);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
