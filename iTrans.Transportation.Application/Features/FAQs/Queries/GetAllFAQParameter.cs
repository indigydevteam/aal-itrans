﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.FAQ;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;

namespace iTrans.Transportation.Application.Features.FAQs.Queries
{
    public class GetAllFAQParameter : IRequest<PagedResponse<IEnumerable<FAQViewModel>>>
    {
        public string Search { set; get; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
    }
    public class GetAllFAQParameterHandler : IRequestHandler<GetAllFAQParameter, PagedResponse<IEnumerable<FAQViewModel>>>
    {
        private readonly IFAQRepositoryAsync _faqRepository;
        private readonly IMapper _mapper;
        private Expression<Func<FAQ, bool>> expression;

        public GetAllFAQParameterHandler(IFAQRepositoryAsync faqRepository, IMapper mapper)
        {
            _faqRepository = faqRepository;
            _mapper = mapper;
        }

        public async Task<PagedResponse<IEnumerable<FAQViewModel>>> Handle(GetAllFAQParameter request, CancellationToken cancellationToken)
        {
            var validFilter = _mapper.Map<GetAllFAQParameter>(request);
            if (validFilter.Search != null && validFilter.Search.Trim() != "")
            {
                expression = x => (x.Detail_ENG.Contains(validFilter.Search.Trim()) || x.Detail_TH.Contains(validFilter.Search.Trim()) || x.Title_TH.Contains(validFilter.Search.Trim()) || x.Title_ENG.Contains(validFilter.Search.Trim()) || x.Module.Contains(validFilter.Search.Trim()));
            }
            int itemCount = _faqRepository.GetItemCount(expression);
            //Expression<Func<FAQ, bool>> expression = x => x.Name_TH != "";
            var faq = await _faqRepository.FindByConditionWithPage(expression, validFilter.PageNumber, validFilter.PageSize);
            var faqViewModel = _mapper.Map<IEnumerable<FAQViewModel>>(faq);
            return new PagedResponse<IEnumerable<FAQViewModel>>(faqViewModel.OrderByDescending(x=>x.modified), validFilter.PageNumber, validFilter.PageSize,itemCount);
        }
    }
}
