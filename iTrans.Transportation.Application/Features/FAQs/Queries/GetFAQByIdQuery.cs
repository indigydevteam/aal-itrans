﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.FAQ;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.FAQs.Queries
{
    public class GetFAQByIdQuery : IRequest<Response<FAQViewModel>>
    {
        public int Id { get; set; }
    }
    public class GetFAQByIdQueryHandler : IRequestHandler<GetFAQByIdQuery, Response<FAQViewModel>>
    {
        private readonly IFAQRepositoryAsync _faqRepository;
        private readonly IMapper _mapper;
        public GetFAQByIdQueryHandler(IFAQRepositoryAsync faqRepository, IMapper mapper)
        {
            _faqRepository = faqRepository;
            _mapper = mapper;
        }
        public async Task<Response<FAQViewModel>> Handle(GetFAQByIdQuery request, CancellationToken cancellationToken)
        {
            var faqObject = (await _faqRepository.FindByCondition(x => x.Id.Equals(request.Id)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            return new Response<FAQViewModel>(_mapper.Map<FAQViewModel>(faqObject));
        }
    }
}
