﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.FAQ;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using LinqKit;
using MediatR;

namespace iTrans.Transportation.Application.Features.FAQs.Queries
{
    public class GetDriverFAQQuery : IRequest<Response<IEnumerable<FAQViewModel>>>
    {
        public string Search { get; set; }
    }
    public class GetDriverFAQQueryHandler : IRequestHandler<GetDriverFAQQuery, Response<IEnumerable<FAQViewModel>>>
    {
        private readonly IFAQRepositoryAsync _faqRepository;
        private readonly IMapper _mapper;
        public GetDriverFAQQueryHandler(IFAQRepositoryAsync faqRepository, IMapper mapper)
        {
            _faqRepository = faqRepository;
            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<FAQViewModel>>> Handle(GetDriverFAQQuery request, CancellationToken cancellationToken)
        {
            Expression<Func<Domain.FAQ, bool>> expression = PredicateBuilder.New<Domain.FAQ>(false);
            expression = x => x.Active == true && x.Module == "driver";
            if (request.Search != null && request.Search.Trim() != "")
            {
                expression = expression.And(x => x.Title_TH.Contains(request.Search) || x.Title_ENG.Contains(request.Search) || x.Detail_TH.Contains(request.Search) || x.Detail_ENG.Contains(request.Search));
            }
            var faq = (await _faqRepository.FindByCondition(x => x.Active == true && x.Module == "driver").ConfigureAwait(false)).AsQueryable().ToList().OrderBy(x => x.Sequence);
            var faqViewModel = _mapper.Map<IEnumerable<FAQViewModel>>(faq);
            return new Response<IEnumerable<FAQViewModel>>(faqViewModel);
        }
    }
}
