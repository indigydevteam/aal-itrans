﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using iTrans.Transportation.Application.Interfaces.Repositories;
using FluentValidation;

namespace iTrans.Transportation.Application.Features.FAQs.Queries
{
    public class GetFAQByIdQueryValidator : AbstractValidator<GetFAQByIdQuery>
    {
        private readonly IFAQRepositoryAsync faqRepository;

        public GetFAQByIdQueryValidator(IFAQRepositoryAsync faqRepository)
        {
            this.faqRepository = faqRepository;
            RuleFor(p => p.Id)
               .NotEmpty().WithMessage("{PropertyName} is required.")
               .NotNull()
               .MustAsync(IsFAQExists).WithMessage("{PropertyName} not exists.");
        }

        private async Task<bool> IsFAQExists(int FAQId, CancellationToken cancellationToken)
        {
            var userObject = (await faqRepository.FindByCondition(x => x.Id.Equals(FAQId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            if (userObject != null)
            {
                return true;
            }
            return false;
        }
    }
}
