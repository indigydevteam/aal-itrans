﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.FAQ;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.FAQs.Queries
{
    public class GetAllFAQQuery : IRequest<Response<IEnumerable<FAQViewModel>>>
    {

    }
    public class GetAllFAQQueryHandler : IRequestHandler<GetAllFAQQuery, Response<IEnumerable<FAQViewModel>>>
    {
        private readonly IFAQRepositoryAsync _faqRepository;
        private readonly IMapper _mapper;
        public GetAllFAQQueryHandler(IFAQRepositoryAsync faqRepository, IMapper mapper)
        {
            _faqRepository = faqRepository;
            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<FAQViewModel>>> Handle(GetAllFAQQuery request, CancellationToken cancellationToken)
        {
            var faq = (await _faqRepository.FindByCondition(x => x.Active == true).ConfigureAwait(false)).AsQueryable().ToList();
            var faqViewModel = _mapper.Map<IEnumerable<FAQViewModel>>(faq);
            return new Response<IEnumerable<FAQViewModel>>(faqViewModel);
        }
    }
}
