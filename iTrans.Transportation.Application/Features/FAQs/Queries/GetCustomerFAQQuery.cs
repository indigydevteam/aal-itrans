﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.FAQ;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using LinqKit;
using MediatR;

namespace iTrans.Transportation.Application.Features.FAQs.Queries
{
    public class GetCustomerFAQQuery : IRequest<Response<IEnumerable<FAQViewModel>>>
    {
        public string Search { get; set; }
    }
    public class GetCustomerFAQQueryHandler : IRequestHandler<GetCustomerFAQQuery, Response<IEnumerable<FAQViewModel>>>
    {
        private readonly IFAQRepositoryAsync _faqRepository;
        private readonly IMapper _mapper;
        public GetCustomerFAQQueryHandler(IFAQRepositoryAsync faqRepository, IMapper mapper)
        {
            _faqRepository = faqRepository;
            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<FAQViewModel>>> Handle(GetCustomerFAQQuery request, CancellationToken cancellationToken)
        {
            Expression<Func<Domain.FAQ, bool>> expression = PredicateBuilder.New<Domain.FAQ>(false);
            expression = x => x.Active == true && x.Module == "customer";
            if (request.Search != null && request.Search.Trim() != "")
            {
                expression = expression.And(x => x.Title_TH.Contains(request.Search) || x.Title_ENG.Contains(request.Search) || x.Detail_TH.Contains(request.Search) || x.Detail_ENG.Contains(request.Search)) ;
            }
            var faq = (await _faqRepository.FindByCondition(expression).ConfigureAwait(false)).AsQueryable().ToList().OrderBy(x => x.Sequence);
            var faqViewModel = _mapper.Map<IEnumerable<FAQViewModel>>(faq);
            return new Response<IEnumerable<FAQViewModel>>(faqViewModel);
        }
    }
}
