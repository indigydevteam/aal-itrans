﻿using AutoMapper;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Interfaces.Services;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.FAQs.Commands
{
    public partial class CreateFAQCommand : IRequest<Response<int>>
    {
        public Guid? UserId { get; set; }
        public string Title_TH { get; set; }
        public string Detail_TH { get; set; }
        public string Title_ENG { get; set; }
        public string Detail_ENG { get; set; }
        public string Module { get; set; }
        public int Sequence { get; set; }
        public bool Active { get; set; }

    }
    public class CreateFAQCommandHandler : IRequestHandler<CreateFAQCommand, Response<int>>
    {
        private readonly IFAQRepositoryAsync _FAQRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;
        public CreateFAQCommandHandler(IFAQRepositoryAsync FAQRepository, IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _FAQRepository = FAQRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(CreateFAQCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var faq = _mapper.Map<FAQ>(request);
                faq.Created = DateTime.UtcNow;
                faq.Modified = DateTime.UtcNow;
                //title.CreatedBy = "xxxxxxx";
                //title.ModifiedBy = "xxxxxxx";
                var FAQObject = await _FAQRepository.AddAsync(faq);
                var log = new CreateAppLog(_applicationLogRepository);
                log.CreateLog("FAQ", "FAQ", "Create", Newtonsoft.Json.JsonConvert.SerializeObject(request),request.UserId.ToString());
                return new Response<int>(FAQObject.Id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
