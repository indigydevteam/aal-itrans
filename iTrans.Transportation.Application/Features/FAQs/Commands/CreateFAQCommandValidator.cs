﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.FAQs.Commands
{
    public class CreateFAQCommandValidator : AbstractValidator<CreateFAQCommand>
    {
        private readonly IFAQRepositoryAsync faqRepository;

        public CreateFAQCommandValidator(IFAQRepositoryAsync faqRepository)
        {
            this.faqRepository = faqRepository;

            RuleFor(p => p.Detail_TH)
                .NotEmpty().WithMessage("{PropertyName} is required.")
                .NotNull()
                .MaximumLength(50).WithMessage("{PropertyName} must not exceed 50 characters.");


            RuleFor(p => p.Detail_ENG)
                .NotEmpty().WithMessage("{PropertyName} is required.")
                .NotNull()
                .MaximumLength(50).WithMessage("{PropertyName} must not exceed 50 characters.");
        }


    }
}
