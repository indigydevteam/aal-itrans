﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;


namespace iTrans.Transportation.Application.Features.FAQs.Commands
{
    public class UpdateFAQCommandValidator : AbstractValidator<UpdateFAQCommand>
    {
        private readonly IFAQRepositoryAsync faqRepository;
        public UpdateFAQCommandValidator(IFAQRepositoryAsync faqRepository)
        {
            this.faqRepository = faqRepository;

            RuleFor(p => p.Detail_TH)
               .NotEmpty().WithMessage("{PropertyName} is required.")
               .NotNull();




            RuleFor(p => p.Detail_ENG)
                .NotEmpty().WithMessage("{PropertyName} is required.")
                .NotNull();
        }
    }
}
