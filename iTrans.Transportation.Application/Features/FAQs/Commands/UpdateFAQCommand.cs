﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;

namespace iTrans.Transportation.Application.Features.FAQs.Commands
{
    public class UpdateFAQCommand : IRequest<Response<int>>
    {
        public Guid? UserId { get; set; }
        public int Id { get; set; }
        public string Title_TH { get; set; }
        public string Detail_TH { get; set; }
        public string Title_ENG { get; set; }
        public string Detail_ENG { get; set; }
        public string Module { get; set; }
        public  int Sequence { get; set; }
        public bool Active { get; set; }

    }

    public class UpdateFAQCommandHandler : IRequestHandler<UpdateFAQCommand, Response<int>>
    {
        private readonly IFAQRepositoryAsync _faqRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;

        public UpdateFAQCommandHandler(IFAQRepositoryAsync faqRepository, IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _faqRepository = faqRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(UpdateFAQCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var faq = (await _faqRepository.FindByCondition(x => x.Id == request.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (faq == null)
                {
                    throw new ApiException($"FAQ Not Found.");
                }
                else
                {
                    faq.Title_TH = request.Title_TH;
                    faq.Title_ENG = request.Title_ENG;
                    faq.Detail_TH = request.Detail_TH;
                    faq.Detail_ENG = request.Detail_ENG;
                    faq.Module = request.Module;
                    faq.Active = request.Active;
                    faq.Modified = DateTime.UtcNow;

                    await _faqRepository.UpdateAsync(faq);
                    var log = new CreateAppLog(_applicationLogRepository);
                    log.CreateLog("FAQ", "FAQ", "Create", Newtonsoft.Json.JsonConvert.SerializeObject(request),request.UserId.ToString());
                    return new Response<int>(faq.Id);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
