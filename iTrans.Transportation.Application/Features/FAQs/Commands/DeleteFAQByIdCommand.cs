﻿using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.FAQs.Commands
{
    public class DeleteFAQByIdCommand : IRequest<Response<int>>
    {
        public int Id { get; set; }
        public class DeleteFAQByIdCommandHandler : IRequestHandler<DeleteFAQByIdCommand, Response<int>>
        {
            private readonly IFAQRepositoryAsync _faqRepository;
            public DeleteFAQByIdCommandHandler(IFAQRepositoryAsync faqRepository)
            {
                _faqRepository = faqRepository;
            }
            public async Task<Response<int>> Handle(DeleteFAQByIdCommand command, CancellationToken cancellationToken)
            {
                var faq = (await _faqRepository.FindByCondition(x => x.Id == command.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (faq == null)
                {
                    throw new ApiException($"FAQ Not Found.");
                }
                else
                {
                    await _faqRepository.DeleteAsync(faq);
                    return new Response<int>(faq.Id);
                }
            }
        }
    }
}
