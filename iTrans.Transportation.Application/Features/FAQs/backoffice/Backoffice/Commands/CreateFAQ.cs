﻿using AutoMapper;
using iTrans.Transportation.Application.DTOs.FAQ;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Interfaces.Services;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.FAQs.backoffice
{
    public partial class CreateFAQ : IRequest<Response<int>>
    {
        public Guid? UserId { get; set; }
    
        public List<FAQViewModelFormCreate> faqData { get; set; }

    }
    public class CreateFAQHandler : IRequestHandler<CreateFAQ, Response<int>>
    {
        private readonly IFAQRepositoryAsync _FAQRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;
        public CreateFAQHandler(IFAQRepositoryAsync FAQRepository, IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _FAQRepository = FAQRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(CreateFAQ request, CancellationToken cancellationToken)
        {
            try
            {
                var faq = _mapper.Map<List<FAQ>>(request.faqData);

                foreach (var f in faq)
                {
                    f.Title_ENG = f.Title_TH != null ? f.Title_TH : "-";
                    f.Detail_ENG = f.Detail_TH != null ? f.Detail_TH : "-";
                    f.Sequence = 0;
                    f.Created = DateTime.Now;
                    f.Modified = DateTime.Now;


                    await _FAQRepository.AddAsync(f);
                }

                var log = new CreateAppLog(_applicationLogRepository);
                log.CreateLog("FAQ", "FAQ", "Create", Newtonsoft.Json.JsonConvert.SerializeObject(request.faqData),request.UserId.ToString());
                return new Response<int>(1);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
