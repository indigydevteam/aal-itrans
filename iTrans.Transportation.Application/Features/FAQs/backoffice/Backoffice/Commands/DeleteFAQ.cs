﻿using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.FAQs.Commands
{
   public class DeleteFAQ : IRequest<Response<int>>
    {
        public int Id { get; set; }
        public class DeleteFAQQueryHandler : IRequestHandler<DeleteFAQ, Response<int>>
        {
            private readonly IFAQRepositoryAsync _FAQRepository;
            public DeleteFAQQueryHandler(IFAQRepositoryAsync FAQRepository)
            {
                _FAQRepository = FAQRepository;
            }
            public async Task<Response<int>> Handle(DeleteFAQ command, CancellationToken cancellationToken)
            {
                var data = (await _FAQRepository.FindByCondition(x => x.Id == command.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (data == null)
                {
                    throw new ApiException($"FAQ Not Found.");
                }
                else
                {
                    await _FAQRepository.DeleteAsync(data);
                    return new Response<int>(data.Id);
                }
            }
        }
    }
}