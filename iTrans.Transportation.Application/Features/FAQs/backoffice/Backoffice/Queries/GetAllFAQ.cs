﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.FAQ;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;

namespace iTrans.Transportation.Application.Features.FAQs.backoffice
{
    public class GetAllFAQ : IRequest<PagedResponse<IEnumerable<FAQViewModel>>>
    {
        public string Search { set; get; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public int Active { set; get; }
        public string Column { set; get; }
    }
    public class GetAllFAQHandler : IRequestHandler<GetAllFAQ, PagedResponse<IEnumerable<FAQViewModel>>>
    {
        private readonly IFAQRepositoryAsync _faqRepository;
        private readonly IMapper _mapper;

        public GetAllFAQHandler(IFAQRepositoryAsync faqRepository, IMapper mapper)
        {
            _faqRepository = faqRepository;
            _mapper = mapper;
        }

        public async Task<PagedResponse<IEnumerable<FAQViewModel>>> Handle(GetAllFAQ request, CancellationToken cancellationToken)
        {
            var validFilter = _mapper.Map<GetAllFAQ>(request);
        

            var result =  from x in  _faqRepository.GetAllAsync().Result.ToList().OrderByDescending(x => x.Modified)
                          select new FAQViewModel
                          {
                              id = x.Id,
                              module = x.Module !=null ? x.Module:"-",
                              title_TH = x.Title_TH != null ? x.Title_TH : "-",
                              title_ENG = x.Title_ENG != null ? x.Title_ENG : "-",
                              detail_TH = x.Detail_TH != null ? x.Detail_TH : "-",
                              detail_ENG = x.Detail_ENG != null ? x.Detail_ENG : "-",
                              created = x.Created.ToString("yyyy/MM/dd HH:MM") != null ? x.Created.ToString("yyyy/MM/dd HH:MM") : "-",
                              active = x.Active,
                          };
            
            if (validFilter.Search != null && validFilter.Search.Trim() != ""){
                result = result.Where(l => l.module.Contains(validFilter.Search.Trim()) || l.title_TH.Contains(validFilter.Search.Trim())
                        || l.detail_TH.Contains(validFilter.Search.Trim()) || l.created.Contains(validFilter.Search.Trim())).ToList();
            }
            if (request.Column == "module" && request.Active == 3)
            {
                result = result.OrderBy(x => x.module);
            }
            if (request.Column == "module" && request.Active == 2)
            {
                result = result.OrderByDescending(x => x.module);
            }
            if (request.Column == "title_TH" && request.Active == 3)
            {
                result = result.OrderBy(x => x.title_TH);
            }
            if (request.Column == "title_TH" && request.Active == 2)
            {
                result = result.OrderByDescending(x => x.title_TH);
            }
            if (request.Column == "detail_TH" && request.Active == 3)
            {
                result = result.OrderBy(x => x.detail_TH);
            }
            if (request.Column == "detail_TH" && request.Active == 2)
            {
                result = result.OrderByDescending(x => x.detail_TH);
            }
            if (request.Column == "detail_ENG" && request.Active == 3)
            {
                result = result.OrderBy(x => x.detail_ENG);
            }
            if (request.Column == "detail_ENG" && request.Active == 2)
            {
                result = result.OrderByDescending(x => x.detail_ENG);
            }
            if (request.Column == "created" && request.Active == 3)
            {
                result = result.OrderBy(x => x.created);
            }
            if (request.Column == "created" && request.Active == 2)
            {
                result = result.OrderByDescending(x => x.created);
            }
            var itemCount = result.Count();

            return new PagedResponse<IEnumerable<FAQViewModel>>(result, validFilter.PageNumber, validFilter.PageSize,itemCount);
        }
    }
}
