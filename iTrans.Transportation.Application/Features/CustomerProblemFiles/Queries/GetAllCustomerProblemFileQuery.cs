﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.CustomerProblemFile;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.CustomerProblemFiles.Queries
{
    public class GetAllCustomerProblemFileQuery : IRequest<Response<IEnumerable<CustomerProblemFileViewModel>>>
    {

    }
    public class GetAllCustomerProblemFileQueryHandler : IRequestHandler<GetAllCustomerProblemFileQuery, Response<IEnumerable<CustomerProblemFileViewModel>>>
    {
        private readonly ICustomerProblemFileRepositoryAsync _customerProblemFileRepository;
        private readonly IMapper _mapper;
        public GetAllCustomerProblemFileQueryHandler(ICustomerProblemFileRepositoryAsync customerProblemFileRepository, IMapper mapper)
        {
            _customerProblemFileRepository = customerProblemFileRepository;
            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<CustomerProblemFileViewModel>>> Handle(GetAllCustomerProblemFileQuery request, CancellationToken cancellationToken)
        {
            var customer = await _customerProblemFileRepository.GetAllAsync();
            var customerProblemFileViewModel = _mapper.Map<IEnumerable<CustomerProblemFileViewModel>>(customer);
            return new Response<IEnumerable<CustomerProblemFileViewModel>>(customerProblemFileViewModel);
        }
    }
}
