﻿using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Interfaces.Services;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using iTrans.Transportation.Domain.Entities;
using MediatR;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.CustomerProblemFiles.Commands
{
    public partial class CreateCustomerProblemFileCommand : IRequest<Response<int>>
    {

        public virtual int DriverId { get; set; }
        public virtual string FileName { get; set; }
        public virtual string ContentType { get; set; }
        public virtual string FilePath { get; set; }
        public virtual string DirectoryPath { get; set; }
        public virtual string FileEXT { get; set; }
        public virtual int Sequence { get; set; }

    }
    public class CreateCustomerProblemFileCommandHandler : IRequestHandler<CreateCustomerProblemFileCommand, Response<int>>
    {
        private readonly ICustomerProblemFileRepositoryAsync _customerProblemFileRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;
        public CreateCustomerProblemFileCommandHandler(ICustomerProblemFileRepositoryAsync customerProblemFileRepository, IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _customerProblemFileRepository = customerProblemFileRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(CreateCustomerProblemFileCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var customerProblemFile = _mapper.Map<CustomerProblemFile>(request);
                customerProblemFile.Created = DateTime.UtcNow;
                var customerProblemFileObject = await _customerProblemFileRepository.AddAsync(customerProblemFile);
                var log = new CreateAppLog(_applicationLogRepository);
                log.Create("Customer", "Customer ProblemFile", "Create", Newtonsoft.Json.JsonConvert.SerializeObject(request));
                return new Response<int>(customerProblemFileObject.Id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
