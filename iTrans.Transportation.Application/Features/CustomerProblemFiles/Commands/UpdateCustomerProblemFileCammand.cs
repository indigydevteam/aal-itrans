﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;

namespace iTrans.Transportation.Application.Features.CustomerPayments.Commands
{
    public class UpdateCustomerProblemFileCommand : IRequest<Response<int>>
    {
        public virtual int Id { get; set; }
        public virtual int DriverId { get; set; }
        public virtual string FileName { get; set; }
        public virtual string ContentType { get; set; }
        public virtual string FilePath { get; set; }
        public virtual string DirectoryPath { get; set; }
        public virtual string FileEXT { get; set; }
        public virtual int Sequence { get; set; }
    }
    public class UpdateCustomerProblemFileCommandHandler : IRequestHandler<UpdateCustomerProblemFileCommand, Response<int>>
    {
        private readonly ICustomerProblemFileRepositoryAsync _customerProblemFileRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;

        public UpdateCustomerProblemFileCommandHandler(ICustomerProblemFileRepositoryAsync customerProblemFileRepository, IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _customerProblemFileRepository = customerProblemFileRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(UpdateCustomerProblemFileCommand request, CancellationToken cancellationToken)
        {
            var customerProblemFile = (await _customerProblemFileRepository.FindByCondition(x => x.Id == request.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            if (customerProblemFile == null)
            {
                throw new ApiException($"ProblemFile Not Found.");
            }
            else
            {
                //customerProblemFile.CustomerId = request.CustomerId;
                //customerProblemFile.DriverId = request.DriverId;
                customerProblemFile.FileName = request.FileName;
                customerProblemFile.ContentType = request.ContentType;
                customerProblemFile.FilePath = request.FilePath;
                customerProblemFile.DirectoryPath = request.DirectoryPath;
                customerProblemFile.FileEXT = request.FileEXT;
                customerProblemFile.Sequence = request.Sequence;
                customerProblemFile.Modified = DateTime.UtcNow;
                await _customerProblemFileRepository.UpdateAsync(customerProblemFile);
                var log = new CreateAppLog(_applicationLogRepository);
                log.Create("Customer", "Customer ProblemFile", "Edit", Newtonsoft.Json.JsonConvert.SerializeObject(request));
                return new Response<int>(customerProblemFile.Id);
            }
        }
    }
}
