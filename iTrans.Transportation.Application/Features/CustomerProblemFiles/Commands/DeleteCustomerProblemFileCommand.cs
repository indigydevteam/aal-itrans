﻿using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.CustomerProblemFiles.Commands
{
    public class DeleteCustomerProblemFileCommand : IRequest<Response<int>>
    {
        public int ProblemFileTopicId { get; set; }
        public int Id { get; set; }
        public class DeleteCustomerProblemFileCommandHandler : IRequestHandler<DeleteCustomerProblemFileCommand, Response<int>>
        {
            private readonly ICustomerProblemFileRepositoryAsync _customerProblemFileRepository;
            public DeleteCustomerProblemFileCommandHandler(ICustomerProblemFileRepositoryAsync customerProblemFileRepository)
            {
                _customerProblemFileRepository = customerProblemFileRepository;
            }
            public async Task<Response<int>> Handle(DeleteCustomerProblemFileCommand command, CancellationToken cancellationToken)
            {
                var customerProblemFile = (await _customerProblemFileRepository.FindByCondition(x => x.Id == command.Id ).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (customerProblemFile == null)
                {
                    throw new ApiException($"ProblemFile Not Found.");
                }
                else
                {
                    await _customerProblemFileRepository.DeleteAsync(customerProblemFile);
                    return new Response<int>(customerProblemFile.Id);
                }
            }
        }
    }
}
