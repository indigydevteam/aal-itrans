﻿using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Interfaces.Services;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using iTrans.Transportation.Application.DTOs.DriverCarLocation.Backoffice;

namespace iTrans.Transportation.Application.Features.DriverCars.Backoffice
{
    public partial class UpdateDriverCarById : IRequest<Response<int>>
    {
        public Guid? UserId { get; set; }
        public int Id { get; set; }
        public Guid OwnerId { get; set; }
        public string CarRegistration { get; set; }
        public DateTime ActExpiry { get; set; }
        public int CarTypeId { get; set; }
        public int CarListId { get; set; }
        public int CarDescriptionId { get; set; }
        public string CarDescriptionDetail { get; set; }
        public int CarFeatureId { get; set; }
        public string CarFeatureDetail { get; set; }
        public int CarSpecificationId { get; set; }
        public string CarSpecificationDetail { get; set; }
        public int Width { get; set; }
        public int Length { get; set; }
        public int Height { get; set; }
        public string DriverName { get; set; }
        public string DriverPhoneCode { get; set; }
        public string DriverPhoneNumber { get; set; }
        public virtual string DriverIdentityNumber { get; set; }
        public bool ProductInsurance { get; set; }
        public decimal ProductInsuranceAmount { get; set; }
        public bool Status { get; set; }
        public bool AllLocation { get; set; }
        public string Note { get; set; }
        //public List<DriverCarLocationViewModel> Location { get; set; }
        public IFormFile ProfilePicture { get; set; }
        public string ContentDirectory { get; set; }
        public string Location { get; set; }


    }
    public class UpdateDriverCarByIdHandler : IRequestHandler<UpdateDriverCarById, Response<int>>
    {
        private readonly IDriverRepositoryAsync _driverRepository;
        private readonly IDriverCarRepositoryAsync _driverCarRepository;
        private readonly IDriverCarFileRepositoryAsync _driverCarFileRepository;
        private readonly IDriverCarLocationRepositoryAsync _driverCarLocationRepository;
        private readonly ICarTypeRepositoryAsync _carTypeRepository;
        private readonly ICarListRepositoryAsync _carListRepository;
        private readonly ICarDescriptionRepositoryAsync _carDescriptionRepository;
        private readonly ICarFeatureRepositoryAsync _carFeatureRepository;
        private readonly ICarSpecificationRepositoryAsync _carSpecificationRepository;
        private readonly ICountryRepositoryAsync _countryRepository;
        private readonly IRegionRepositoryAsync _regionRepository;
        private readonly IProvinceRepositoryAsync _provinceRepository;
        private readonly IDistrictRepositoryAsync _districtRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;
        private readonly IConfiguration _configuration;
        public UpdateDriverCarByIdHandler(IDriverRepositoryAsync driverRepository, IDriverCarRepositoryAsync driverCarRepository, IDriverCarFileRepositoryAsync driverCarFileRepository, IDriverCarLocationRepositoryAsync driverCarLocationRepository
             , ICarTypeRepositoryAsync carTypeRepository, ICarListRepositoryAsync carListRepository, ICarDescriptionRepositoryAsync carDescriptionRepository, ICarFeatureRepositoryAsync carFeatureRepository, ICarSpecificationRepositoryAsync carSpecificationRepository
             , ICountryRepositoryAsync countryRepository, IRegionRepositoryAsync regionRepository, IProvinceRepositoryAsync provinceRepository, IDistrictRepositoryAsync districtRepository, IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper
            , IConfiguration configuration)
        {
            _driverRepository = driverRepository;
            _driverCarRepository = driverCarRepository;
            _driverCarFileRepository = driverCarFileRepository;
            _driverCarLocationRepository = driverCarLocationRepository;
            _carTypeRepository = carTypeRepository;
            _carListRepository = carListRepository;
            _carDescriptionRepository = carDescriptionRepository;
            _carFeatureRepository = carFeatureRepository;
            _carSpecificationRepository = carSpecificationRepository;
            _countryRepository = countryRepository;
            _regionRepository = regionRepository;
            _provinceRepository = provinceRepository;
            _districtRepository = districtRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
            _configuration = configuration;
        }

        public async Task<Response<int>> Handle(UpdateDriverCarById request, CancellationToken cancellationToken)
        {
            try
            {
             
                string currentTimeStr = DateTime.Now.ToString("yyyyMMddHHmmss");
                var driverCar = (await _driverCarRepository.FindByCondition(x => x.Id == request.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                //var driverCar = (await _driverCarRepository.FindByCondition(x => x.Id == request.Id && x.Owner.Id.Equals(request.OwnerId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();

                if (driverCar == null)
                {
                    throw new ApiException($"DriverCar Not Found.");
                }
                else if(driverCar.Driver == null)
                {
                    throw new ApiException($"Driver Not Found.");
                }
                else
                {
                    List<DriverCarLocationViewModel> locations = JsonConvert.DeserializeObject<List<DriverCarLocationViewModel>>(request.Location);

                    driverCar.CarType = (await _carTypeRepository.FindByCondition(x => x.Id.Equals(request.CarTypeId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                    driverCar.CarList = (await _carListRepository.FindByCondition(x => x.Id.Equals(request.CarListId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                    driverCar.CarDescription = (await _carDescriptionRepository.FindByCondition(x => x.Id.Equals(request.CarDescriptionId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                    driverCar.CarFeature = (await _carFeatureRepository.FindByCondition(x => x.Id.Equals(request.CarFeatureId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                    driverCar.CarSpecification = (await _carSpecificationRepository.FindByCondition(x => x.Id.Equals(request.CarSpecificationId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                    driverCar.CarRegistration = request.CarRegistration;
                    driverCar.ActExpiry = request.ActExpiry;
                    driverCar.CarDescriptionDetail = request.CarDescriptionDetail;
                    driverCar.CarFeatureDetail = request.CarFeatureDetail;
                    driverCar.CarSpecificationDetail = request.CarSpecificationDetail;
                    driverCar.Width = request.Width;
                    driverCar.Length = request.Length;
                    driverCar.Height = request.Height;
                    driverCar.DriverName = request.DriverName;
                    driverCar.DriverPhoneCode = request.DriverPhoneCode;
                    driverCar.DriverPhoneNumber = request.DriverPhoneNumber;
                    driverCar.DriverIdentityNumber = request.DriverIdentityNumber;
                    driverCar.ProductInsurance = request.ProductInsurance;
                    driverCar.ProductInsuranceAmount = request.ProductInsuranceAmount;
                    driverCar.AllLocation = request.AllLocation;
                    driverCar.Note = request.Note;
                    //await _driverCarRepository.UpdateAsync(driverCar);
                    driverCar.Status = request.Status;
                    driverCar.Modified = DateTime.Now;

                    (await _driverCarLocationRepository.CreateSQLQuery("DELETE Driver_CarLocation where DriverCarId = '" + driverCar.Id.ToString() + "'").ConfigureAwait(false)).ExecuteUpdate();
                

                    if (locations != null)
                    {
                        driverCar.Locations = new List<DriverCarLocation>();
                        foreach (DriverCarLocationViewModel locationViewModel in locations)
                        {
                            locationViewModel.driverCarId = driverCar.Id;
                            var location = _mapper.Map<DriverCarLocation>(locationViewModel);
                            location.Country = (await _countryRepository.FindByCondition(x => x.Id.Equals(locationViewModel.countryId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                            location.Province = (await _provinceRepository.FindByCondition(x => x.Id.Equals(locationViewModel.provinceId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                            location.Region = (await _regionRepository.FindByCondition(x => x.Id.Equals(locationViewModel.regionId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                            location.District = (await _districtRepository.FindByCondition(x => x.Id.Equals(locationViewModel.districtId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                            driverCar.Locations.Add(location);

                            //var locationObject = await _driverCarLocationRepository.AddAsync(location);
                        }
                    }
                    if (driverCar.Owner != null) {
                        if (driverCar.Owner.DriverType == "corporate")
                        {
                            if (driverCar.Driver == null)
                            {
                                Driver driver = new Driver
                                {
                                    DriverType = "peronal",
                                    FirstName = "",
                                    MiddleName = "",
                                    LastName = "",
                                    Name = driverCar.DriverName,
                                    PhoneCode = driverCar.DriverPhoneCode,
                                    PhoneNumber = driverCar.DriverPhoneNumber,
                                    IdentityNumber = driverCar.DriverIdentityNumber,
                                    Corparate = driverCar.Driver.Id,
                                    Password = "none"
                                };
                                await _driverRepository.AddAsync(driver);
                                if (driver != null)
                                {
                                    driverCar.Driver = driver;
                                }
                            }
                            else
                            {
                                driverCar.Driver.Name = driverCar.DriverName;
                                driverCar.Driver.PhoneCode = driverCar.DriverPhoneCode;
                                driverCar.Driver.PhoneNumber = driverCar.DriverPhoneNumber;
                                driverCar.Driver.IdentityNumber = driverCar.DriverIdentityNumber;
                            }
                        }
                    }
                  

                    string folderPath = request.ContentDirectory + "driverCar/" + request.Id.ToString();
                    if (!Directory.Exists(folderPath))
                    {
                        Directory.CreateDirectory(folderPath);
                    }
                    if (request.ProfilePicture != null)
                    {
                        var file = _driverCarFileRepository.FindByCondition(f => f.DocumentType.Equals("CarPicture") && f.DriverCar.Id.Equals(request.Id)).Result.FirstOrDefault();
                        var fileCount = _driverCarFileRepository.FindByCondition(f => f.DriverCar.Id.Equals(request.Id)).Result.Count();
                        if (file != null)
                        {

                            string filePath = Path.Combine(folderPath, request.ProfilePicture.FileName);
                            using (FileStream fileStream = new FileStream(filePath, FileMode.Create))
                            {
                                await request.ProfilePicture.CopyToAsync(fileStream);
                                FileInfo fi = new FileInfo(filePath);


                                file.DriverCar = _mapper.Map<DriverCar>(driverCar);
                                file.FileName = request.ProfilePicture.FileName;
                                file.ContentType = request.ProfilePicture.ContentType;
                                file.FilePath = "drivercar/" + request.Id.ToString() + "/" + request.ProfilePicture.FileName;
                                file.DirectoryPath = "drivercar/" + request.Id.ToString() + "/";
                                file.FileEXT = fi.Extension;
                                file.DocumentType = "CarPicture";
                                file.Sequence = file.Sequence;


                                await _driverCarFileRepository.UpdateAsync(file);
                            }
                        }
                        else
                        {
                            string filePath = Path.Combine(folderPath, request.ProfilePicture.FileName);
                            using (FileStream fileStream = new FileStream(filePath, FileMode.Create))
                            {
                                await request.ProfilePicture.CopyToAsync(fileStream);
                                FileInfo fi = new FileInfo(filePath);

                                DriverCarFile customerFile = new DriverCarFile
                                {
                                    DriverCar = _mapper.Map<DriverCar>(driverCar),
                                    FileName = request.ProfilePicture.FileName,
                                    ContentType = request.ProfilePicture.ContentType,
                                    FilePath = "drivercar/" + request.Id.ToString() + "/" + request.ProfilePicture.FileName,
                                    DirectoryPath = "drivercar/" + request.Id.ToString() + "/",
                                    FileEXT = fi.Extension,
                                    DocumentType = "CarPicture",
                                    Sequence = fileCount + 1
                                };
                                await _driverCarFileRepository.AddAsync(customerFile);
                            }
                        }
                    }

                    await _driverCarRepository.UpdateAsync(driverCar);
                    var log = new CreateAppLog(_applicationLogRepository);
                    log.CreateLog("Drivercar", "Drivercar", "Edit", Newtonsoft.Json.JsonConvert.SerializeObject(request),request.UserId.ToString());
                    return new Response<int>(driverCar.Id);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
       
    }
}
