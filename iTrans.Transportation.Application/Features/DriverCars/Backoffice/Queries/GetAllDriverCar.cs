﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.DriverCar.backoffice;
using iTrans.Transportation.Application.DTOs.DriverCarFile;
using iTrans.Transportation.Application.DTOs.DriverFile;
using iTrans.Transportation.Application.Helper;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;

namespace iTrans.Transportation.Application.Features.DriverCars.Backoffice.Queries
{
    public class GetAllDriverCar : IRequest<PagedResponse<IEnumerable<DriverCarViewModel>>>
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public string Search { set; get; }
        public string Drivertype { get; set; }
        public int Active { set; get; }
        public string Column { set; get; }


    }
    public class GetAllDriverCarHandler : IRequestHandler<GetAllDriverCar, PagedResponse<IEnumerable<DriverCarViewModel>>>
    {
        private readonly IDriverCarRepositoryAsync _driverCarRepository;
        private readonly IDriverRepositoryAsync _driverRepository;
        private readonly IDriverCarFileRepositoryAsync _driverCarFileRepository;
        private readonly IDriverFileRepositoryAsync _driverFileRepository;
        private readonly IMapper _mapper;

        public GetAllDriverCarHandler(IDriverCarRepositoryAsync driverCarRepository, IDriverCarFileRepositoryAsync driverCarFileRepository
            , IDriverRepositoryAsync driverRepository, IDriverFileRepositoryAsync driverFileRepository, IMapper mapper)
        {
            _driverCarRepository = driverCarRepository;
            _driverCarFileRepository = driverCarFileRepository;
            _driverRepository = driverRepository;
            _driverFileRepository = driverFileRepository;
            _mapper = mapper;
        }

        public async Task<PagedResponse<IEnumerable<DriverCarViewModel>>> Handle(GetAllDriverCar request, CancellationToken cancellationToken)
        {
            var validFilter = _mapper.Map<GetAllDriverCar>(request);
         
            var driverCar = (await _driverCarRepository.FindByCondition(x => x.Driver.DriverType.Contains(validFilter.Drivertype)|| x.Owner.DriverType.Contains(validFilter.Drivertype)).ConfigureAwait(false)).AsQueryable().ToList().OrderByDescending(x => x.Modified);
            
            var personal = (from x in driverCar
                            select new DriverCarViewModel
                          {
                              id = x.Id,
                              carRegistration = x.CarRegistration != null ? x.CarRegistration : "-",
                              driverName = x.DriverName != null ? x.DriverName.Replace("  ", " ") : "-",
                              driverPhoneNumber = String.Format("{0}{1}", x.DriverPhoneCode, x.DriverPhoneNumber),
                              created = x.Created.ToString("yyyy/MM/dd HH:MM"),
                              carFiles = _mapper.Map<List<DriverCarFileViewModel>>(x.CarFiles),
                              status = x.Status,
                              temperature = x.Temperature

                            });
            var corporate = (from x in _driverRepository.GetAllAsync().Result.ToList()
                             where x.DriverType.Equals("corporate")
                             select new DriverCarViewModel
                            {
                                driverId = x.Id,
                                driverPhoneNumber = String.Format("{0}{1}", x.PhoneCode, x.PhoneNumber),
                                created = x.Created.ToString("yyyy/MM/dd HH:MM"),
                                driverFiles = _mapper.Map<List<DriverFileViewModel>>(x.Files),
                                status = x.Status,
                                corporateName = x.Name != null ? x.Name.Replace("  ", " ") : "-",
                                corporateNumber = x.IdentityNumber != null ? AESMgr.Decrypt(x.IdentityNumber) : "-",
                                countCar = driverCar.Where(c => c.Owner.Id.Equals(x.Id)).Count(),
                                

                             });

            var result = personal;

            if (validFilter.Drivertype.Trim() == "corporate")
            {
                result = corporate;
            }
            
            if (validFilter.Search != null && validFilter.Search.Trim() != "")
            {
                if (validFilter.Drivertype.Trim() == "corporate")
                {
                    result = result.Where(c =>  c.driverPhoneNumber.Trim().ToLower().Contains(validFilter.Search.Trim().ToLower()) || c.created.Trim().ToLower().Contains(validFilter.Search.Trim().ToLower()) || c.countCar.Equals(validFilter.Search.Trim()) || c.corporateNumber.Trim().ToLower().Contains(validFilter.Search.Trim().ToLower())
                                           || c.corporateName.Trim().ToLower().Contains(validFilter.Search.Trim().ToLower())).ToList();
                }
                else
                {
                 result = result.Where(c => c.carRegistration.Trim().ToLower().Contains(validFilter.Search.Trim().ToLower())
                                          || c.driverName.Trim().ToLower().Contains(validFilter.Search.Trim().ToLower()) || c.driverPhoneNumber.Trim().ToLower().Contains(validFilter.Search.Trim().ToLower())
                                          || c.created.Trim().ToLower().Contains(validFilter.Search.Trim().ToLower()) || c.temperature.ToString().Trim().Contains(validFilter.Search.Trim())).ToList();

                }
               
                }
            if (request.Column == "carRegistration" && request.Active == 2)
            {
                result = result.OrderByDescending(x => x.carRegistration);
            }
            if (request.Column == "carRegistration" && request.Active == 3)
            {
                result = result.OrderBy(x => x.carRegistration);
            }
            if (request.Column == "name" && request.Active == 2)
            {
                result = result.OrderByDescending(x => x.driverName);
            }
            if (request.Column == "name" && request.Active == 3)
            {
                result = result.OrderBy(x => x.driverName);
            }
            if (request.Column == "phoneNumber" && request.Active == 3)
            {
                result = result.OrderBy(x => x.driverPhoneNumber);
            }
            if (request.Column == "phoneNumber" && request.Active == 2)
            {
                result = result.OrderByDescending(x => x.driverPhoneNumber);
            }
            if (request.Column == "created" && request.Active == 3)
            {
                result = result.OrderBy(x => x.created);
            }
            if (request.Column == "created" && request.Active == 2)
            {
                result = result.OrderByDescending(x => x.created);
            }
            if (request.Column == "corporateName" && request.Active == 3)
            {
                result = result.OrderBy(x => x.corporateName);
            }
            if (request.Column == "corporateName" && request.Active == 2)
            {
                result = result.OrderByDescending(x => x.corporateName);
            }
            if (request.Column == "corporateNumber" && request.Active == 3)
            {
                result = result.OrderBy(x => x.corporateNumber);
            }
            if (request.Column == "corporateNumber" && request.Active == 2)
            {
                result = result.OrderByDescending(x => x.corporateNumber);
            }
            if (request.Column == "countCar" && request.Active == 3)
            {
                result = result.OrderBy(x => x.countCar);
            }
            if (request.Column == "countCar" && request.Active == 2)
            {
                result = result.OrderByDescending(x => x.countCar);
            }
            if (request.Column == "temperature" && request.Active == 3)
            {
                result = result.OrderBy(x => x.temperature);
            }
            if (request.Column == "temperature" && request.Active == 2)
            {
                result = result.OrderByDescending(x => x.temperature);
            }


            var itemCount = result.Count();
            result = result.Skip((validFilter.PageNumber - 1) * validFilter.PageSize).Take(validFilter.PageSize);
            return new PagedResponse<IEnumerable<DriverCarViewModel>>(result, validFilter.PageNumber, validFilter.PageSize, itemCount);
        }
    }
}