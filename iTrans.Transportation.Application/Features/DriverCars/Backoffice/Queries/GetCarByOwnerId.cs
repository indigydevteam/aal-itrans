﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.DriverCar;
using iTrans.Transportation.Application.DTOs.DriverCarFile;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.DriverCars.Backoffice.Queries
{
   public class GetCarByOwnerId : IRequest<PagedResponse<IEnumerable<DriverCarViewModel>>>
    {
        public Guid OwnerId { get; set; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public string Search { get; set; }
        public int Active { set; get; }
        public string Column { set; get; }
    }
    public class GetCarByOwnerIdHandler : IRequestHandler<GetCarByOwnerId, PagedResponse<IEnumerable<DriverCarViewModel>>>
    {
        private readonly IDriverCarRepositoryAsync _driverCarRepository;
        private readonly IMapper _mapper;
        public GetCarByOwnerIdHandler(IDriverCarRepositoryAsync driverCarRepository, IMapper mapper)
        {
            _driverCarRepository = driverCarRepository;
            _mapper = mapper;
        }

        public async Task<PagedResponse<IEnumerable<DriverCarViewModel>>> Handle(GetCarByOwnerId request, CancellationToken cancellationToken)
        {
            try
            {

                var driverCar = (await _driverCarRepository.FindByCondition(x => x.Owner.Id.Equals(request.OwnerId)).ConfigureAwait(false)).AsQueryable().ToList().OrderByDescending(x => x.Modified);


                var result = (from x in driverCar
                                select new DriverCarViewModel
                                {
                                    id = x.Id,
                                    carRegistration = x.CarRegistration != null ? x.CarRegistration : "-",
                                    driverName = x.DriverName != null ? x.DriverName : "-",
                                    driverPhoneNumber = String.Format("{0}{1}", x.DriverPhoneCode, x.DriverPhoneNumber),
                                    created = x.Created.ToString("yyyy/MM/dd HH:MM"),
                                    carFiles = _mapper.Map<List<DriverCarFileViewModel>>(x.CarFiles),
                                    status = x.Status,
                                    temperature = x.Temperature



                                });
                if (request.Search != null && request.Search.Trim() != "")
                {

                    result = result.Where(c => c.carRegistration.Trim().ToLower().Contains(request.Search.Trim().ToLower()) || c.driverName.Trim().ToLower().Contains(request.Search.Trim().ToLower())
                                           || c.driverPhoneNumber.Trim().ToLower().Contains(request.Search.Trim().ToLower()) || c.created.Trim().ToLower().Contains(request.Search.Trim().ToLower()) 
                                           || c.temperature.ToString().Trim().Contains(request.Search.Trim().ToLower())).ToList();

                }

            if (request.Column == "carRegistration" && request.Active == 2)
            {
                result = result.OrderByDescending(x => x.carRegistration);
            }
            if (request.Column == "carRegistration" && request.Active == 3)
            {
                result = result.OrderBy(x => x.carRegistration);
            }
            if (request.Column == "name" && request.Active == 2)
            {
                result = result.OrderByDescending(x => x.driverName);
            }
            if (request.Column == "name" && request.Active == 3)
            {
                result = result.OrderBy(x => x.driverName);
            }
            if (request.Column == "phoneNumber" && request.Active == 3)
            {
                result = result.OrderBy(x => x.driverPhoneNumber);
            }
            if (request.Column == "phoneNumber" && request.Active == 2)
            {
                result = result.OrderByDescending(x => x.driverPhoneNumber);
            }
            if (request.Column == "created" && request.Active == 3)
            {
                result = result.OrderBy(x => x.created);
            }
            if (request.Column == "created" && request.Active == 2)
            {
                result = result.OrderByDescending(x => x.created);
            }
            if (request.Column == "temperature" && request.Active == 3)
            {
                result = result.OrderBy(x => x.temperature);
            }
            if (request.Column == "temperature" && request.Active == 2)
            {
                result = result.OrderByDescending(x => x.temperature);
            }


            var itemCount = result.Count();
            result = result.Skip((request.PageNumber - 1) * request.PageSize).Take(request.PageSize);
                return new PagedResponse<IEnumerable<DriverCarViewModel>>(result, request.PageNumber, request.PageSize, itemCount);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
