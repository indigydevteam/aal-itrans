﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.DriverCar.backoffice;
using iTrans.Transportation.Application.Helper;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.DriverCars.Backoffice.Queries
{
    public class GetDriveCarById : IRequest<Response<DriverCarViewModel>>
    {
        public int Id { get; set; }
        public string ContentDirectory { get; set; }

    }
    public class GetDriverCarByIdQueryHandler : IRequestHandler<GetDriveCarById, Response<DriverCarViewModel>>
    {
        private readonly IDriverCarRepositoryAsync _driverCarRepository;
        private readonly IDriverRepositoryAsync _driverRepository;
        private readonly IMapper _mapper;
        public GetDriverCarByIdQueryHandler(IDriverCarRepositoryAsync driverCarRepository, IDriverRepositoryAsync driverRepository, IMapper mapper)
        {
            _driverCarRepository = driverCarRepository;
            _driverRepository = driverRepository;
            _mapper = mapper;
        }
        public async Task<Response<DriverCarViewModel>> Handle(GetDriveCarById request, CancellationToken cancellationToken)
        {
            //var driver = (await _driverRepository.GetAllAsync());

            var drivercar = (await _driverCarRepository.FindByCondition(x => x.Id.Equals(request.Id)).ConfigureAwait(false)).AsQueryable().ToList();

            var result = _mapper.Map<List<DriverCarViewModel>>(drivercar).Select(p => new DriverCarViewModel
            {
                id = p.id,
                driverId = p.driverId,
                carRegistration = p.carRegistration,
                actExpiry = p.actExpiry,
                carType = p.carType,
                carList = p.carList,
                carDescription = p.carDescription,
                carDescriptionDetail = p.carDescriptionDetail,
                carFeature = p.carFeature,
                carFeatureDetail = p.carFeatureDetail,
                carSpecification = p.carSpecification,
                carSpecificationDetail = p.carSpecificationDetail,
                width = p.width,
                length = p.length,
                height = p.height,
                driverName = p.driverName,
                driverPhoneCode = p.driverPhoneCode,
                driverPhoneNumber = p.driverPhoneNumber,
                productInsurance = p.productInsurance,
                productInsuranceAmount = p.productInsuranceAmount,
                allLocation = p.allLocation,
                note = p.note,
                carFiles = p.carFiles,
                locations = p.locations,
                worklocations = p.locations.Where(x=>x.locationType== "working-area").FirstOrDefault(),
                nonworklocations = p.locations.Where(x => x.locationType == "non-working-area").FirstOrDefault(),
                //countcar = driver.Where(c=>c.Id.Equals(p.Owner.id)).Count(),
                status = p.status,
                created = p.created,



            }).FirstOrDefault();

                result.driverPhoneNumber = "0" + result.driverPhoneNumber;
            if (result.carFiles.Where(x => x.documentType.Equals("CarPicture")).Select(x => x.filePath).FirstOrDefault() != null)
            {
                result.imageCarPicture = ConvertImage.ConvertImageURLToBase64(request.ContentDirectory + "/" + result.carFiles.Where(x => x.documentType.Equals("CarPicture")).Select(x => x.filePath).FirstOrDefault());
            }
            if (result.carFiles.Where(x => x.documentType.Equals("CarRegistration")).Select(x => x.filePath).FirstOrDefault() != null)
            {
                result.imageCarRegistration = ConvertImage.ConvertImageURLToBase64(request.ContentDirectory + "/" + result.carFiles.Where(x => x.documentType.Equals("CarRegistration")).Select(x => x.filePath).FirstOrDefault());
            }

            return new Response<DriverCarViewModel>(result);
        }
    }
}
