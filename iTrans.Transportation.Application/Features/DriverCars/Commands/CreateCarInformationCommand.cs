﻿using AutoMapper;
using iTrans.Transportation.Application.DTOs.DriverCarLocation;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Interfaces.Services;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;

namespace iTrans.Transportation.Application.Features.DriverCars.Commands
{
    public partial class CreateCarInformationCommand : IRequest<Response<int>>
    {
        public Guid? UserId { get; set; }
        public Guid OwnerId { get; set; }
        public string CarRegistration { get; set; }
        public DateTime ActExpiry { get; set; }
        public int CarTypeId { get; set; }
        //public int CarListId { get; set; }
        //public int CarDescriptionId { get; set; }
        //public string CarDescriptionDetail { get; set; }
        //public int CarFeatureId { get; set; }
        //public string CarFeatureDetail { get; set; }
        public int CarSpecificationId { get; set; }
        //public string CarSpecificationDetail { get; set; }
        public int Width { get; set; }
        public int Length { get; set; }
        public int Height { get; set; }
        public float Temperature { get; set; }
        public int EnergySavingDevice { get; set; }
        public string DriverName { get; set; }
        public string DriverPhoneCode { get; set; }
        public string DriverPhoneNumber { get; set; }
        public virtual string DriverIdentityNumber { get; set; }
        public bool ProductInsurance { get; set; }
        public decimal ProductInsuranceAmount { get; set; }
        public bool AllLocation { get; set; }
        public string Note { get; set; }
        public string CarPictures { get; set; }
        public string CarRegistrationFiles { get; set; }
        public List<IFormFile> files { get; set; }
        public string Location { get; set; }

    }
    public class CreateCarInformationCommandHandler : IRequestHandler<CreateCarInformationCommand, Response<int>>
    {
        private readonly IDriverRepositoryAsync _driverRepository;
        private readonly IDriverCarRepositoryAsync _driverCarRepository;
        private readonly IDriverCarFileRepositoryAsync _driverCarFileRepository;
        private readonly IDriverCarLocationRepositoryAsync _driverCarLocationRepository;
        private readonly ICarTypeRepositoryAsync _carTypeRepository;
        //private readonly ICarListRepositoryAsync _carListRepository;
        //private readonly ICarDescriptionRepositoryAsync _carDescriptionRepository;
        //private readonly ICarFeatureRepositoryAsync _carFeatureRepository;
        private readonly ICarSpecificationRepositoryAsync _carSpecificationRepository;
        private readonly ICountryRepositoryAsync _countryRepository;
        private readonly IRegionRepositoryAsync _regionRepository;
        private readonly IProvinceRepositoryAsync _provinceRepository;
        private readonly IDistrictRepositoryAsync _districtRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IEnergySavingDeviceRepositoryAsync _energySavingDeviceRepository;
        private readonly IMapper _mapper;
        private readonly IConfiguration _configuration;
        public CreateCarInformationCommandHandler(IDriverRepositoryAsync driverRepository, IDriverCarRepositoryAsync driverCarRepository, IDriverCarFileRepositoryAsync driverCarFileRepository, IDriverCarLocationRepositoryAsync driverCarLocationRepository
            , ICarTypeRepositoryAsync carTypeRepository, ICarSpecificationRepositoryAsync carSpecificationRepository //,ICarListRepositoryAsync carListRepository, ICarDescriptionRepositoryAsync carDescriptionRepository, ICarFeatureRepositoryAsync carFeatureRepository
            , ICountryRepositoryAsync countryRepository, IRegionRepositoryAsync regionRepository, IProvinceRepositoryAsync provinceRepository, IDistrictRepositoryAsync districtRepository,IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper
            , IEnergySavingDeviceRepositoryAsync energySavingDeviceRepository, IConfiguration configuration)
        {
            _driverRepository = driverRepository;
            _driverCarRepository = driverCarRepository;
            _driverCarFileRepository = driverCarFileRepository;
            _driverCarLocationRepository = driverCarLocationRepository;
            _carTypeRepository = carTypeRepository;
            //_carListRepository = carListRepository;
            //_carDescriptionRepository = carDescriptionRepository;
            //_carFeatureRepository = carFeatureRepository;
            _carSpecificationRepository = carSpecificationRepository;
            _countryRepository = countryRepository;
            _regionRepository = regionRepository;
            _provinceRepository = provinceRepository;
            _districtRepository = districtRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
            _energySavingDeviceRepository = energySavingDeviceRepository;
            _configuration = configuration;
        }

        public async Task<Response<int>> Handle(CreateCarInformationCommand request, CancellationToken cancellationToken)
        {
            try
            {
                string currentTimeStr = DateTime.Now.ToString("yyyyMMddHHmmss");
                var getContentPath = _configuration.GetSection("ContentPath");
                string contentPath = getContentPath.Value;
                var ownerObj = (await _driverRepository.FindByCondition(x => x.Id == request.OwnerId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (ownerObj == null)
                {
                    throw new ApiException($"Driver Not Found.");
                }
 
                var driverCar = _mapper.Map<DriverCar>(request);
                driverCar.Owner = ownerObj;
                driverCar.CarType = (await _carTypeRepository.FindByCondition(x => x.Id == request.CarTypeId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                //driverCar.CarList = (await _carListRepository.FindByCondition(x => x.Id == request.CarListId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                //driverCar.CarDescription = (await _carDescriptionRepository.FindByCondition(x => x.Id == request.CarDescriptionId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                //driverCar.CarFeature = (await _carFeatureRepository.FindByCondition(x => x.Id ==  request.CarFeatureId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                driverCar.CarSpecification = (await _carSpecificationRepository.FindByCondition(x => x.Id ==  request.CarSpecificationId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                driverCar.EnergySavingDevice = (await _energySavingDeviceRepository.FindByCondition(x => x.Id == request.EnergySavingDevice).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                driverCar.Created = DateTime.UtcNow;

                string phoneNumber = request.DriverPhoneNumber != null ? request.DriverPhoneNumber.Trim() : "";
                if (request.DriverPhoneCode == "+66" && request.DriverPhoneNumber != null && request.DriverPhoneNumber.Trim().Length > 9 && request.DriverPhoneNumber.Trim().First() == '0')
                {
                    phoneNumber = request.DriverPhoneNumber.Trim().Remove(0, 1);
                }
                driverCar.DriverPhoneNumber = phoneNumber;
                int fileCount = 0;
                if (request.files != null)
                {
                    string folderPath = contentPath + "drivercar/" + ownerObj.Id + "/" + currentTimeStr;
                    if (!Directory.Exists(folderPath))
                    {
                        Directory.CreateDirectory(folderPath);
                    }
                    driverCar.CarFiles = new List<DriverCarFile>();
                    foreach (IFormFile file in request.files)
                    {
                        fileCount = fileCount + 1;
                        string filePath = Path.Combine(folderPath, file.FileName);
                        using (FileStream fileStream = new FileStream(filePath, FileMode.Create))
                        {
                            await file.CopyToAsync(fileStream);
                            FileInfo fi = new FileInfo(filePath);
                            string documentType = GetDocumentType(request, file.FileName);

                            DriverCarFile driverCarFile = new DriverCarFile
                            {
                                DriverCar = driverCar,
                                FileName = file.FileName,
                                ContentType = file.ContentType,
                                FilePath = "drivercar/" + ownerObj.Id + "/" + currentTimeStr + "/" + file.FileName,
                                DirectoryPath = "drivercar/" + ownerObj.Id + "/" + currentTimeStr + "/",
                                FileEXT = fi.Extension,
                                DocumentType = documentType,
                                Sequence = fileCount,
                            };
                            driverCar.CarFiles.Add(driverCarFile);
                            //var customerFileObject = await _driverCarFileRepository.AddAsync(driverCarFile);
                        }
                    }
                }
                if (request.Location != null)
                {
                    List<DriverCarLocationViewModel> locations = JsonConvert.DeserializeObject<List<DriverCarLocationViewModel>>(request.Location);
                    driverCar.Locations = new List<DriverCarLocation>();
                    foreach (DriverCarLocationViewModel locationViewModel in locations)
                    {
                        var location = _mapper.Map<DriverCarLocation>(locationViewModel);
                        location.DriverCar = driverCar;
                        location.Country = (await _countryRepository.FindByCondition(x => x.Id == locationViewModel.countryId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                        location.Province = (await _provinceRepository.FindByCondition(x => x.Id == locationViewModel.provinceId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                        location.Region = (await _regionRepository.FindByCondition(x => x.Id == locationViewModel.regionId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                        location.District = (await _districtRepository.FindByCondition(x => x.Id == locationViewModel.districtId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                        //var locationObject = await _driverCarLocationRepository.AddAsync(location);
                        driverCar.Locations.Add(location);
                    }
                }
                if (ownerObj.DriverType == "corporate")
                {
                    Driver driver = new Driver
                    {
                        DriverType = "personal",
                        CorporateType = ownerObj.CorporateType,
                        FirstName = ownerObj.ContactPersonFirstName,
                        MiddleName = ownerObj.MiddleName,
                        LastName = ownerObj.LastName,
                        Name = ownerObj.Name,
                        IdentityType = ownerObj.IdentityType,
                        IdentityNumber = ownerObj.IdentityNumber,
                        ContactPersonTitle = ownerObj.ContactPersonTitle,
                        ContactPersonFirstName = ownerObj.ContactPersonFirstName,
                        ContactPersonMiddleName = ownerObj.ContactPersonMiddleName,
                        ContactPersonLastName = ownerObj.ContactPersonLastName,
                        Birthday = ownerObj.Birthday,
                        PhoneCode = driverCar.DriverPhoneCode,
                        PhoneNumber = driverCar.DriverPhoneNumber,
                        Email = ownerObj.Email,
                        Facbook = ownerObj.Facbook,
                        Line = ownerObj.Line,
                        Twitter = ownerObj.Twitter,
                        Whatapp = ownerObj.Whatapp,
                        Level = 1,//ownerObj.Level,
                        Star = 0,//ownerObj.Star,
                        Rating = "0",//ownerObj.Rating,
                        Grade = "E",//ownerObj.Grade,
                        AcceptJobPerYear = 0,
                        AcceptJobPerMonth = 0,
                        ComplaintPerYear = 0,
                        ComplaintPerMonth = 0,
                        RejectPerYear = 0,
                        RejectPerMonth = 0,
                        CancelPerYear = 0,
                        CancelPerMonth = 0,
                        InsuranceValue = 0,
                        Corparate = ownerObj.Id,
                        VerifyStatus = ownerObj.VerifyStatus,
                        Status = ownerObj.Status,
                        Wechat = ownerObj.Wechat,
                        Password = "none",
                        IsRegister = false,
                        IsDelete = false,
                        Created = DateTime.Now,
                        Modified = DateTime.Now
                    };
                    //driverCar.Driver = driver;
                    driver = await _driverRepository.AddAsync(driver);
                    if (driver != null)
                    {
                        driverCar.Driver = driver;
                    }
                }
                else
                {
                    driverCar.Driver = ownerObj;
                }
                driverCar.Created = DateTime.UtcNow;
                driverCar.CreatedBy = request.UserId != null ? request.UserId.Value.ToString() : "-";
                driverCar.Modified = DateTime.UtcNow;
                driverCar.ModifiedBy = request.UserId != null ? request.UserId.Value.ToString() : "-";
                var driverCarObject = await _driverCarRepository.AddAsync(driverCar);
                var log = new CreateAppLog(_applicationLogRepository);
                log.CreateLog("Drivercar", "Drivercar", "Create", Newtonsoft.Json.JsonConvert.SerializeObject(request),request.UserId.ToString());
                return new Response<int>(driverCarObject.Id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public string GetDocumentType(CreateCarInformationCommand request, string fileName)
        {
            string result = "";
            if (request.CarPictures != null)
            {
                string[] CarPictures = request.CarPictures.Split(",");
                var existCarPicture = CarPictures.Where(x => x == fileName).FirstOrDefault();
                if (existCarPicture != null)
                {
                    result = "CarPicture";
                    return result;
                }
            }
            if (request.CarRegistrationFiles != null)
            {
                string[] CarRegistrationFiles = request.CarRegistrationFiles.Split(",");
                var existCarRegistrationFile = CarRegistrationFiles.Where(x => x == fileName).FirstOrDefault();
                if (existCarRegistrationFile != null)
                {
                    result = "CarRegistration";
                    return result;
                }
            }
            return result;
        }
    }
}
