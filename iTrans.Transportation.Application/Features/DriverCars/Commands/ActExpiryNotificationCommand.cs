﻿using AutoMapper;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.DriverCars.Commands
{
    public class ActExpiryNotificationCommand : IRequest<Response<bool>>
    {
        public int day { get; set; }
        public string DRIVER_APP_ID { get; set; }
        public string DRIVER_REST_API_KEY { get; set; }
        public string DRIVER_AUTH_ID { get; set; }
    }
   public class ActExpiryNotificationCommandHandler : IRequestHandler<ActExpiryNotificationCommand, Response<bool>>
    {
        private readonly IDriverCarRepositoryAsync _driverCarRepository;
        private readonly INotificationRepositoryAsync _notificationRepository;
        private readonly INotificationUserRepositoryAsync _notificationUserRepository;
        private readonly IMapper _mapper;

        public ActExpiryNotificationCommandHandler(IDriverCarRepositoryAsync driverCarRepository, INotificationRepositoryAsync notificationRepository, INotificationUserRepositoryAsync notificationUserRepository, IMapper mapper)
        {
            _driverCarRepository = driverCarRepository;
            _notificationRepository = notificationRepository;
            _notificationUserRepository = notificationUserRepository;
            _mapper = mapper;
        }

        public async Task<Response<bool>> Handle(ActExpiryNotificationCommand request, CancellationToken cancellationToken)
        {
            try
            {
                if (request.day == 0)
                {
                    request.day = 30;
                }
                var driverCars = (await _driverCarRepository.FindByCondition(x => x.ActExpiry <= DateTime.Now.AddDays(request.day)).ConfigureAwait(false)).AsQueryable().ToList();
                #region send noti
                foreach (DriverCar driverCar in driverCars) {
                    var userId = driverCar.Owner.Id;
                    var ownerId = driverCar.Owner.Id;

                    var APP_ID = request.DRIVER_APP_ID;
                    var REST_API_KEY = request.DRIVER_REST_API_KEY;
                    var AUTH_ID = request.DRIVER_AUTH_ID;

                    if (userId != null && ownerId != null)
                    {

                        var oneSignalIds = (await _notificationUserRepository.FindByCondition(x => x.UserId.Equals(userId)).ConfigureAwait(false)).AsQueryable().Select(x => x.OneSignalId).ToList();

                        string title = "พรบ. หมดอายุ " + driverCar.ActExpiry.ToString();
                        string detail = "รถยนตร์ทะเบียน " + driverCar.CarRegistration + " พรบ. หมดอายุ " + driverCar.ActExpiry.ToString();
                        
                        Domain.Notification notification = new Domain.Notification();

                        notification.OneSignalId = oneSignalIds.FirstOrDefault();
                        notification.UserId = userId;
                        notification.OwnerId = ownerId;
                        notification.Title = title;
                        notification.Detail = detail;
                        notification.ServiceCode = "car";
                        //notification.ReferenceContentKey = request.ReferenceContentKey;
                        notification.Payload = "";
                        notification.Created = DateTime.UtcNow;
                        notification.Modified = DateTime.UtcNow;
                        notification.CreatedBy = userId.ToString();
                        notification.ModifiedBy = userId.ToString();
                        await _notificationRepository.AddAsync(notification);

                        if (oneSignalIds.Count > 0)
                        {
                            NotificationManager notiMgr = new NotificationManager(APP_ID, REST_API_KEY, AUTH_ID);
                            notiMgr.SendNotificationAsync(title, detail, oneSignalIds, notification.ServiceCode, "", "");
                        }
                    }
                }
                #endregion
                return new Response<bool>(true);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
