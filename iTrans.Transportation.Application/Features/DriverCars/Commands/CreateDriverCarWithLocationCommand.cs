﻿using AutoMapper;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Interfaces.Services;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;

namespace iTrans.Transportation.Application.Features.DriverCars.Commands
{
    public partial class CreateDriverCarWithLocationCommand : IRequest<Response<int>>
    {
        public Guid? UserId { get; set; }
        public Guid DriverId { get; set; }
        public string CarRegistration { get; set; }
        public DateTime ActExpiry { get; set; }
        public int CarTypeId { get; set; }
        //public int CarListId { get; set; }
        //public int CarDescriptionId { get; set; }
        //public string CarDescriptionDetail { get; set; }
        //public int CarFeatureId { get; set; }
        //public string CarFeatureDetail { get; set; }
        public int CarSpecificationId { get; set; }
        //public string CarSpecificationDetail { get; set; }
        public int Width { get; set; }
        public int Length { get; set; }
        public int Height { get; set; }
        public string DriverName { get; set; }
        public string DriverPhoneCode { get; set; }
        public string DriverPhoneNumber { get; set; }
        public virtual string DriverIdentityNumber { get; set; }
        public bool ProductInsurance { get; set; }
        public decimal ProductInsuranceAmount { get; set; }
        public bool AllLocation { get; set; }
        public string Note { get; set; }
        public string CarPictures { get; set; }
        public string CarRegistrationFiles { get; set; }
        public List<IFormFile> files { get; set; }
        public string Location { get; set; }

    }
    public class CreateDriverCarWithLocationCommandHandler : IRequestHandler<CreateDriverCarWithLocationCommand, Response<int>>
    {
        private readonly IDriverCarRepositoryAsync _driverCarRepository;
        private readonly IDriverCarFileRepositoryAsync _driverCarFileRepository;
        private readonly IDriverCarLocationRepositoryAsync _driverCarLocationRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;
        private readonly IConfiguration _configuration;
        public CreateDriverCarWithLocationCommandHandler(IDriverCarRepositoryAsync driverCarRepository, IDriverCarFileRepositoryAsync driverCarFileRepository, IDriverCarLocationRepositoryAsync driverCarLocationRepository, IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper
            , IConfiguration configuration)
        {
            _driverCarRepository = driverCarRepository;
            _driverCarFileRepository = driverCarFileRepository;
            _driverCarLocationRepository = driverCarLocationRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
            _configuration = configuration;
        }

        public async Task<Response<int>> Handle(CreateDriverCarWithLocationCommand request, CancellationToken cancellationToken)
        {
            try
            {
                List<DriverCarLocation> locations = JsonConvert.DeserializeObject<List<DriverCarLocation>>(request.Location);
                var driverCar = _mapper.Map<DriverCar>(request);
                driverCar.Created = DateTime.UtcNow;
                var driverCarObject = await _driverCarRepository.AddAsync(driverCar);

                if (driverCarObject != null && driverCarObject.Id != 0 && driverCarObject.Driver.Id != null)
                {
                    var getContentPath = _configuration.GetSection("ContentPath");
                    string contentPath = getContentPath.Value;
                    string folderPath = contentPath + "drivercar/" + driverCarObject.Driver.Id.ToString();
                    if (!Directory.Exists(folderPath))
                    {
                        Directory.CreateDirectory(folderPath);
                    }
                    int fileCount = 0;
                    if (request.files != null)
                    {
                        foreach (IFormFile file in request.files)
                        {
                            fileCount = fileCount + 1;
                            string filePath = Path.Combine(folderPath, file.FileName);
                            using (FileStream fileStream = new FileStream(filePath, FileMode.Create))
                            {
                                await file.CopyToAsync(fileStream);
                                FileInfo fi = new FileInfo(filePath);
                                string documentType = GetDocumentType(request, file.FileName);

                                DriverCarFile driverCarFile = new DriverCarFile
                                {
                                    DriverCar = driverCarObject,
                                    FileName = file.FileName,
                                    ContentType = file.ContentType,
                                    FilePath = "drivercar/" + driverCarObject.Driver.Id.ToString() + "/" + file.FileName,
                                    FileEXT = fi.Extension,
                                    DocumentType = documentType,
                                    Sequence = fileCount,
                                };
                                var customerFileObject = await _driverCarFileRepository.AddAsync(driverCarFile);
                            }
                        }
                    }

                    foreach (DriverCarLocation location in locations)
                    {
                        //location.DriverCarId = driverCarObject.Id;
                        location.Created = DateTime.UtcNow;
                        location.Modified = DateTime.UtcNow;
                        var locationObject = await _driverCarLocationRepository.AddAsync(location);
                        var log = new CreateAppLog(_applicationLogRepository);
                        log.CreateLog("Drivercar", "Drivercar", "Create", Newtonsoft.Json.JsonConvert.SerializeObject(location),request.UserId.ToString());
                    }
                }
                return new Response<int>(driverCarObject.Id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public string GetDocumentType(CreateDriverCarWithLocationCommand request, string fileName)
        {
            string result = "";
            if (request.CarPictures != null)
            {
                string[] CarPictures = request.CarPictures.Split(",");
                var existCarPicture = CarPictures.Where(x => x == fileName).FirstOrDefault();
                if (existCarPicture != null)
                {
                    result = "CarPicture";
                    return result;
                }
            }
            if (request.CarRegistrationFiles != null)
            {
                string[] CarRegistrationFiles = request.CarRegistrationFiles.Split(",");
                var existCarRegistrationFile = CarRegistrationFiles.Where(x => x == fileName).FirstOrDefault();
                if (existCarRegistrationFile != null)
                {
                    result = "CarRegistration";
                    return result;
                }
            }
            return result;
        }
    }
}
