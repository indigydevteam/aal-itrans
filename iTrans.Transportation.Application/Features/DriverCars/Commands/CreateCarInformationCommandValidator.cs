﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using iTrans.Transportation.Application.Helper;
using System.Text.RegularExpressions;
using Microsoft.Extensions.Configuration;

namespace iTrans.Transportation.Application.Features.DriverCars.Commands
{
    public class CreateCarInformationCommandValidator : AbstractValidator<CreateCarInformationCommand>
    {
        private readonly IDriverCarRepositoryAsync driverCarRepository;
        private readonly IDriverRepositoryAsync driverRepository;
        private readonly IConfiguration configuration;

        public CreateCarInformationCommandValidator(IDriverCarRepositoryAsync driverCarRepository, IDriverRepositoryAsync driverRepository, IConfiguration configuration)
        {
            this.driverCarRepository = driverCarRepository;
            this.driverRepository = driverRepository;
            this.configuration = configuration;

            RuleFor(p => p.OwnerId)
                .NotNull().WithMessage("{PropertyName} is required.")
                .MustAsync(IsExistDriver).WithMessage("{PropertyName} not exists.");

            RuleFor(p => p.CarRegistration)
                .NotNull().WithMessage("{PropertyName} is required.")
                .MustAsync(IsRegistrationUnique).WithMessage("{PropertyName} already exists.");

            RuleFor(p => p.DriverPhoneNumber)
               .NotNull().WithMessage("{PropertyName} is required.")
               .MustAsync(async (p, s, cancellation) =>
               {
                   return await IsPhoneUnique(p.DriverPhoneNumber, p.OwnerId, p.DriverPhoneCode).ConfigureAwait(false);
               }).WithMessage("{PropertyName} already exists.");
            RuleFor(p => p.DriverIdentityNumber)
               .NotNull().WithMessage("{PropertyName} is required.")
               .MustAsync(ValidateIdentityNumber).WithMessage("{PropertyName} formay is not correct.")
               .MustAsync(IsIdentityNumberUnique).WithMessage("{PropertyName} already exists.");

        }
        private async Task<bool> IsExistDriver(Guid value, CancellationToken cancellationToken)
        {
            var driverObject = (await driverRepository.FindByCondition(x => x.Id.Equals(value)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            if (driverObject == null)
            {
                return false;
            }
            return true;
        }

        private async Task<bool> IsRegistrationUnique(string value, CancellationToken cancellationToken)
        {
            var driverCarObject = (await driverCarRepository.FindByCondition(x => x.CarRegistration.ToLower().Trim().Replace(" ", "") == value.ToLower().Trim().Replace(" ", "") && x.Driver.IsDelete == false).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            if (driverCarObject == null)
            {
                return true;
            }
            return false;
        }
        private async Task<bool> IsPhoneUnique(string phoneNumber, Guid driverId, string code)
        {
            if (code == "+66" && phoneNumber != null && phoneNumber.Trim().Length > 9 && phoneNumber.Trim().First() == '0')
            {
                phoneNumber = phoneNumber.Trim().Remove(0, 1);
            }
            var driverCarObject = (await driverCarRepository.FindByCondition(x => x.DriverPhoneNumber.ToLower().Trim() == phoneNumber.ToLower().Trim() && x.Driver.IsDelete == false).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            if (driverCarObject != null)
            {
                return false;
            }
            var driverObject = (await driverRepository.FindByCondition(x => x.PhoneNumber.ToLower().Trim() == phoneNumber.ToLower().Trim() && x.Id != driverId && x.IsDelete == false).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            if (driverObject != null)
            {
                return false;
            }
            return true;
        }
        private async Task<bool> ValidateIdentityNumber(string value, CancellationToken cancellationToken)
        {
            var isDevelopment = this.configuration.GetSection("IsDevelopment");
            bool result = false;
            string DeCodeidentityNumber = AESMgr.Decrypt(value);

            Regex rexPersonal = new Regex(@"^[0-9]{13}$");
            if (rexPersonal.IsMatch(DeCodeidentityNumber))
            {
                int sum = 0;

                for (int i = 0; i < 12; i++)
                {
                    sum += int.Parse(DeCodeidentityNumber[i].ToString()) * (13 - i);
                }

                result = (int.Parse(DeCodeidentityNumber[12].ToString()) == ((11 - (sum % 11)) % 10));
            }
            if (isDevelopment != null && isDevelopment.Value == "true")
            {
                result = true;
            }

            return result;
        }
        private async Task<bool> IsIdentityNumberUnique(string value, CancellationToken cancellationToken)
        {
            var driverCarObject = (await driverCarRepository.FindByCondition(x => x.DriverIdentityNumber == value && x.Driver.IsDelete == false).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            if (driverCarObject != null)
            {
                return false;
            }
            
            return true;
        }
    }
}
