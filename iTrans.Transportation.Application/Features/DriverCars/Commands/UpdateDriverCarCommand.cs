﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;

namespace iTrans.Transportation.Application.Features.DriverCars.Commands
{
    public class UpdateDriverCarCommand : IRequest<Response<int>>
    {
        public Guid? UserId { get; set; }
        public int Id { get; set; }
        public Guid DriverId { get; set; }
        public string CarRegistration { get; set; }
        public DateTime ActExpiry { get; set; }
        public int CarTypeId { get; set; }
        //public int CarListId { get; set; }
        //public int CarDescriptionId { get; set; }
        //public string CarDescriptionDetail { get; set; }
        //public int CarFeatureId { get; set; }
        //public string CarFeatureDetail { get; set; }
        public int CarSpecificationId { get; set; }
        //public string CarSpecificationDetail { get; set; }
        public int Width { get; set; }
        public int Length { get; set; }
        public int Height { get; set; }
        public string DriverName { get; set; }
        public string DriverPhoneCode { get; set; }
        public string DriverPhoneNumber { get; set; }
        public bool ProductInsurance { get; set; }
        public decimal ProductInsuranceAmount { get; set; }
        public bool AllLocation { get; set; }
        public string Note { get; set; }
    }

    public class UpdateDriverCarCommandHandler : IRequestHandler<UpdateDriverCarCommand, Response<int>>
    {
        private readonly IDriverCarRepositoryAsync _driverCarRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;

        public UpdateDriverCarCommandHandler(IDriverCarRepositoryAsync driverCarRepository, IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _driverCarRepository = driverCarRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(UpdateDriverCarCommand request, CancellationToken cancellationToken)
        {
            var driverCar = (await _driverCarRepository.FindByCondition(x => x.Id == request.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            if (driverCar == null)
            {
                throw new ApiException($"DriverCar Not Found. for method UpdateDriverCarCommand , Id : " + request.Id + "Driver " + request.DriverId);
            }
            else
            {
                //driverCar.Driver.Id = request.DriverId;
                driverCar.CarRegistration = request.CarRegistration;
                driverCar.ActExpiry = request.ActExpiry;
                //driverCar.CarTypeId = request.CarTypeId;
                //driverCar.CarListId = request.CarListId;
                //driverCar.CarDescriptionId = request.CarDescriptionId;
                //driverCar.CarDescriptionDetail = request.CarDescriptionDetail;
                //driverCar.CarFeatureId = request.CarFeatureId;
                //driverCar.CarFeatureDetail = request.CarFeatureDetail;
                //driverCar.CarSpecificationId = request.CarSpecificationId;
                //driverCar.CarSpecificationDetail = request.CarSpecificationDetail;
                driverCar.Width = request.Width;
                driverCar.Length = request.Length;
                driverCar.Height = request.Height;
                driverCar.DriverName = request.DriverName;
                driverCar.DriverPhoneCode = request.DriverPhoneCode;
                driverCar.DriverPhoneNumber = request.DriverPhoneNumber;
                driverCar.ProductInsurance = request.ProductInsurance;
                driverCar.ProductInsuranceAmount = request.ProductInsuranceAmount;
                driverCar.AllLocation = request.AllLocation;
                driverCar.Note = request.Note;
                driverCar.Modified = DateTime.UtcNow;
                await _driverCarRepository.UpdateAsync(driverCar);
                var log = new CreateAppLog(_applicationLogRepository);
                log.CreateLog("Drivercar", "Drivercar", "Edit", Newtonsoft.Json.JsonConvert.SerializeObject(driverCar),request.UserId.ToString());
                return new Response<int>(driverCar.Id);
            }
        }
    }
}
