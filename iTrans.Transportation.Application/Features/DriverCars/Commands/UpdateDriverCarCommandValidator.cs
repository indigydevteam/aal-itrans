﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.DriverCars.Commands
{
    public class UpdateDriverCarCommandValidator : AbstractValidator<UpdateDriverCarCommand>
    {
        private readonly IDriverCarRepositoryAsync driverCarRepository;
        public UpdateDriverCarCommandValidator(IDriverCarRepositoryAsync driverCarRepository)
        {
            this.driverCarRepository = driverCarRepository;

            RuleFor(p => p.DriverId)
               .NotNull().WithMessage("{PropertyName} is required.");

            RuleFor(p => p.CarRegistration)
                .NotNull().WithMessage("{PropertyName} is required.")
                 .MustAsync(async (p, s, cancellation) =>
                 {
                     return await IsUnique(p.Id, p.CarRegistration).ConfigureAwait(false);
                 }).WithMessage("{PropertyName} already exists.");
        }

        private async Task<bool> IsUnique(int id, string carRegistration)
        {
            var customerObject = (await driverCarRepository.FindByCondition(x => x.CarRegistration.ToLower() == carRegistration.ToLower() && x.Id != id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            if (customerObject != null)
            {
                return false;
            }
            return true;
        }
    }
}
