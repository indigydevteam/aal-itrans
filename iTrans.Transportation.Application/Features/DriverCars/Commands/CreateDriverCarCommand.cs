﻿using AutoMapper;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Interfaces.Services;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.DriverCars.Commands
{
    public partial class CreateDriverCarCommand : IRequest<Response<int>>
    {
        public Guid? UserId { get; set; }
        public Guid DriverId { get; set; }
        public string CarRegistration { get; set; }
        public DateTime ActExpiry { get; set; }
        public int CarTypeId { get; set; }
        //public int CarListId { get; set; }
        //public int CarDescriptionId { get; set; }
        //public string CarDescriptionDetail { get; set; }
        //public int CarFeatureId { get; set; }
        //public string CarFeatureDetail { get; set; }
        public int CarSpecificationId { get; set; }
        //public string CarSpecificationDetail { get; set; }
        public int Width { get; set; }
        public int Length { get; set; }
        public int Height { get; set; }
        public string DriverName { get; set; }
        public string DriverPhoneCode { get; set; }
        public string DriverPhoneNumber { get; set; }
        public bool ProductInsurance { get; set; }
        public decimal ProductInsuranceAmount { get; set; }
        public bool AllLocation { get; set; }
        public string Note { get; set; }

    }
    public class CreateDriverCarCommandHandler : IRequestHandler<CreateDriverCarCommand, Response<int>>
    {
        private readonly IDriverCarRepositoryAsync _driverCarRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;
        public CreateDriverCarCommandHandler(IDriverCarRepositoryAsync driverCarRepository, IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _driverCarRepository = driverCarRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(CreateDriverCarCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var driverCar = _mapper.Map<DriverCar>(request);
                driverCar.Created = DateTime.UtcNow;
                var driverCarObject = await _driverCarRepository.AddAsync(driverCar);
                var log = new CreateAppLog(_applicationLogRepository);
                log.CreateLog("Drivercar", "Drivercar", "Create", Newtonsoft.Json.JsonConvert.SerializeObject(driverCar),request.UserId.ToString());
                return new Response<int>(driverCarObject.Id);
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
    }
}
