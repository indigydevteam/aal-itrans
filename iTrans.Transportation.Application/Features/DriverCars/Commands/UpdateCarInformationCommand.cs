﻿using AutoMapper;
using iTrans.Transportation.Application.DTOs.DriverCarLocation;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Interfaces.Services;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;

namespace iTrans.Transportation.Application.Features.DriverCars.Commands
{
    public partial class UpdateCarInformationCommand : IRequest<Response<int>>
    {
        public Guid? UserId { get; set; }
        public int Id { get; set; }
        public Guid OwnerId { get; set; }
        public string CarRegistration { get; set; }
        public DateTime ActExpiry { get; set; }
        public int CarTypeId { get; set; }
        //public int CarListId { get; set; }
        //public int CarDescriptionId { get; set; }
        //public string CarDescriptionDetail { get; set; }
        //public int CarFeatureId { get; set; }
        //public string CarFeatureDetail { get; set; }
        public int CarSpecificationId { get; set; }
        //public string CarSpecificationDetail { get; set; }
        public int Width { get; set; }
        public int Length { get; set; }
        public int Height { get; set; }
        public float Temperature { get; set; }
        public int EnergySavingDevice { get; set; }
        public string DriverName { get; set; }
        public string DriverPhoneCode { get; set; }
        public string DriverPhoneNumber { get; set; }
        public virtual string DriverIdentityNumber { get; set; }
        public bool ProductInsurance { get; set; }
        public decimal ProductInsuranceAmount { get; set; }
        public bool AllLocation { get; set; }
        public string Note { get; set; }
        public string CarPictures { get; set; }
        public string CarRegistrationFiles { get; set; }
        public string DeleteFiles { get; set; }
        public List<IFormFile> files { get; set; }
        public string Location { get; set; }

    }
    public class UpdateCarInformationCommandHandler : IRequestHandler<UpdateCarInformationCommand, Response<int>>
    {
        private readonly IDriverRepositoryAsync _driverRepository;
        private readonly IDriverCarRepositoryAsync _driverCarRepository;
        private readonly IDriverCarFileRepositoryAsync _driverCarFileRepository;
        private readonly IDriverCarLocationRepositoryAsync _driverCarLocationRepository;
        private readonly ICarTypeRepositoryAsync _carTypeRepository;
        private readonly ICarListRepositoryAsync _carListRepository;
        private readonly ICarDescriptionRepositoryAsync _carDescriptionRepository;
        private readonly ICarFeatureRepositoryAsync _carFeatureRepository;
        private readonly ICarSpecificationRepositoryAsync _carSpecificationRepository;
        private readonly ICountryRepositoryAsync _countryRepository;
        private readonly IRegionRepositoryAsync _regionRepository;
        private readonly IProvinceRepositoryAsync _provinceRepository;
        private readonly IDistrictRepositoryAsync _districtRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IEnergySavingDeviceRepositoryAsync _energySavingDeviceRepository;
        private readonly IMapper _mapper;
        private readonly IConfiguration _configuration;
        public UpdateCarInformationCommandHandler(IDriverRepositoryAsync driverRepository, IDriverCarRepositoryAsync driverCarRepository, IDriverCarFileRepositoryAsync driverCarFileRepository, IDriverCarLocationRepositoryAsync driverCarLocationRepository
             , ICarTypeRepositoryAsync carTypeRepository, ICarSpecificationRepositoryAsync carSpecificationRepository //, ICarListRepositoryAsync carListRepository, ICarDescriptionRepositoryAsync carDescriptionRepository, ICarFeatureRepositoryAsync carFeatureRepository
             , ICountryRepositoryAsync countryRepository, IRegionRepositoryAsync regionRepository, IProvinceRepositoryAsync provinceRepository, IDistrictRepositoryAsync districtRepository, IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper
             , IEnergySavingDeviceRepositoryAsync energySavingDeviceRepository, IConfiguration configuration)
        {
            _driverRepository = driverRepository;
            _driverCarRepository = driverCarRepository;
            _driverCarFileRepository = driverCarFileRepository;
            _driverCarLocationRepository = driverCarLocationRepository;
            _carTypeRepository = carTypeRepository;
            //_carListRepository = carListRepository;
            //_carDescriptionRepository = carDescriptionRepository;
            //_carFeatureRepository = carFeatureRepository;
            _carSpecificationRepository = carSpecificationRepository;
            _countryRepository = countryRepository;
            _regionRepository = regionRepository;
            _provinceRepository = provinceRepository;
            _districtRepository = districtRepository;
            _applicationLogRepository = applicationLogRepository;
            _energySavingDeviceRepository = energySavingDeviceRepository;
            _mapper = mapper;
            _configuration = configuration;
        }

        public async Task<Response<int>> Handle(UpdateCarInformationCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var getContentPath = _configuration.GetSection("ContentPath");
                string contentPath = getContentPath.Value;
                string currentTimeStr = DateTime.Now.ToString("yyyyMMddHHmmss");
                var driverCar = (await _driverCarRepository.FindByCondition(x => x.Id == request.Id && x.Owner.Id == request.OwnerId ).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (driverCar == null)
                {
                    throw new ApiException($"DriverCar Not Found. for method UpdateCarInformationCommand");
                }
                else
                {
                    string phoneNumber = request.DriverPhoneNumber != null ? request.DriverPhoneNumber.Trim() : "";
                    if (request.DriverPhoneCode == "+66" && request.DriverPhoneNumber != null && request.DriverPhoneNumber.Trim().Length > 9 && request.DriverPhoneNumber.Trim().First() == '0')
                    {
                        phoneNumber = request.DriverPhoneNumber.Trim().Remove(0, 1);
                    }

                    driverCar.CarType = (await _carTypeRepository.FindByCondition(x => x.Id == request.CarTypeId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                    //driverCar.CarList = (await _carListRepository.FindByCondition(x => x.Id == request.CarListId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                    //driverCar.CarDescription = (await _carDescriptionRepository.FindByCondition(x => x.Id == request.CarDescriptionId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                    //driverCar.CarFeature = (await _carFeatureRepository.FindByCondition(x => x.Id == request.CarFeatureId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                    driverCar.CarSpecification = (await _carSpecificationRepository.FindByCondition(x => x.Id == request.CarSpecificationId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                    driverCar.EnergySavingDevice = (await _energySavingDeviceRepository.FindByCondition(x => x.Id == request.EnergySavingDevice).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                    driverCar.CarRegistration = request.CarRegistration;
                    driverCar.ActExpiry = request.ActExpiry;
                    //driverCar.CarTypeId = request.CarTypeId;
                    //driverCar.CarListId = request.CarListId;
                    //driverCar.CarDescriptionId = request.CarDescriptionId;
                    //driverCar.CarDescriptionDetail = request.CarDescriptionDetail;
                    //driverCar.CarFeatureId = request.CarFeatureId;
                    //driverCar.CarFeatureDetail = request.CarFeatureDetail;
                    //driverCar.CarSpecificationId = request.CarSpecificationId;
                    //driverCar.CarSpecificationDetail = request.CarSpecificationDetail;
                    driverCar.Width = request.Width;
                    driverCar.Length = request.Length;
                    driverCar.Height = request.Height;
                    driverCar.Temperature = request.Temperature;
                    driverCar.DriverName = request.DriverName;
                    driverCar.DriverPhoneCode = request.DriverPhoneCode;
                    driverCar.DriverPhoneNumber = phoneNumber;
                    driverCar.DriverIdentityNumber = request.DriverIdentityNumber;
                    driverCar.ProductInsurance = request.ProductInsurance;
                    driverCar.ProductInsuranceAmount = request.ProductInsuranceAmount;
                    driverCar.AllLocation = request.AllLocation;
                    driverCar.Note = request.Note;
                    //await _driverCarRepository.UpdateAsync(driverCar);
                    string directoryPath = "";
                    if (request.DeleteFiles != null && request.DeleteFiles.Trim() != "")
                    {
                        string[] deleteFiles = request.DeleteFiles.Split(",");
                        foreach (string delFile in deleteFiles)
                        {
                            var driverCarFile = (await _driverCarFileRepository.FindByCondition(x => x.FileName.Trim().ToLower() == delFile.Trim().ToLower() && x.DriverCar.Id.Equals(driverCar.Id)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                            if (driverCarFile != null)
                            {
                                if (File.Exists(Path.Combine(contentPath, driverCarFile.FilePath)))
                                {
                                    File.Delete(Path.Combine(contentPath, driverCarFile.FilePath));
                                    directoryPath = contentPath + driverCarFile.DirectoryPath;
                                    await _driverCarFileRepository.DeleteAsync(driverCarFile);
                                }
                            }
                        }
                    }
                    
                    if (driverCar.CarFiles.Count() != 0 )
                    {
                        directoryPath = contentPath + driverCar.CarFiles[0].DirectoryPath;
                    }
                    if(directoryPath == "")
                    {
                        directoryPath = contentPath + "drivercar/" + driverCar.Owner.Id.ToString() + "/" + currentTimeStr + "/";
                    }
                    int fileCount = 0;
                    if (request.files != null)
                    {
                        //string folderPath = contentPath + "drivercar/" + driverCar.Owner.Id.ToString() + "/" + DateTime.Now.ToString("yyyyMMddHHmmss");
                        if (!Directory.Exists(directoryPath))
                        {
                            Directory.CreateDirectory(directoryPath);
                        }
                        driverCar.CarFiles = new List<DriverCarFile>();
                        foreach (IFormFile file in request.files)
                        {
                            fileCount = fileCount + 1;
                            string filePath = Path.Combine(directoryPath, file.FileName);
                            using (FileStream fileStream = new FileStream(filePath, FileMode.Create))
                            {
                                await file.CopyToAsync(fileStream);
                                FileInfo fi = new FileInfo(filePath);
                                string documentType = GetDocumentType(request, file.FileName);

                                DriverCarFile driverCarFile = new DriverCarFile
                                {
                                    DriverCar = driverCar,
                                    FileName = file.FileName,
                                    ContentType = file.ContentType,
                                    FilePath = directoryPath.Replace(contentPath,"") + file.FileName,//"drivercar/" + driverCar.Owner.Id.ToString() + "/" + currentTimeStr + "/" + file.FileName,
                                    DirectoryPath = directoryPath.Replace(contentPath, ""),//"drivercar/" + driverCar.Owner.Id.ToString() + "/" + currentTimeStr + "/",
                                    FileEXT = fi.Extension,
                                    DocumentType = documentType,
                                    Sequence = fileCount,
                                };
                                driverCar.CarFiles.Add(driverCarFile);
                                //var customerFileObject = await _driverCarFileRepository.AddAsync(driverCarFile);
                            }
                            
                        }
                    }
                    (await _driverCarLocationRepository.CreateSQLQuery("DELETE Driver_CarLocation where DriverCarId = '" + driverCar.Id.ToString() + "'").ConfigureAwait(false)).ExecuteUpdate();
                    if (request.Location != null)
                    {
                        List<DriverCarLocationViewModel> locations = JsonConvert.DeserializeObject<List<DriverCarLocationViewModel>>(request.Location);
                        driverCar.Locations = new List<DriverCarLocation>();
                        foreach (DriverCarLocationViewModel locationViewModel in locations)
                        {
                            locationViewModel.driverCarId = driverCar.Id;
                            var location = _mapper.Map<DriverCarLocation>(locationViewModel);
                            location.Country = (await _countryRepository.FindByCondition(x => x.Id == locationViewModel.countryId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                            location.Province = (await _provinceRepository.FindByCondition(x => x.Id == locationViewModel.provinceId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                            location.Region = (await _regionRepository.FindByCondition(x => x.Id == locationViewModel.regionId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                            location.District = (await _districtRepository.FindByCondition(x => x.Id == locationViewModel.districtId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                            driverCar.Locations.Add(location);

                            //var locationObject = await _driverCarLocationRepository.AddAsync(location);
                        }
                    }
                    if (driverCar.Owner.DriverType == "corporate")
                    {
                        if (driverCar.Driver == null)
                        {
                            Driver driver = new Driver
                            {
                                DriverType = "peronal",
                                FirstName = "",
                                MiddleName = "",
                                LastName = "",
                                Name = driverCar.DriverName,
                                PhoneCode = driverCar.DriverPhoneCode,
                                PhoneNumber = driverCar.DriverPhoneNumber,
                                IdentityNumber = driverCar.DriverIdentityNumber,
                                Corparate = driverCar.Driver.Id,
                                Password = "none"
                            };
                            await _driverRepository.AddAsync(driver);
                            if (driver != null)
                            {
                                driverCar.Driver = driver;
                            }
                        }
                        else
                        {
                            driverCar.Driver.Name = driverCar.DriverName;
                            driverCar.Driver.PhoneCode = driverCar.DriverPhoneCode;
                            driverCar.Driver.PhoneNumber = driverCar.DriverPhoneNumber;
                            driverCar.Driver.IdentityNumber = driverCar.DriverIdentityNumber;
                        }
                    }
                    driverCar.Modified = DateTime.UtcNow;
                    driverCar.ModifiedBy = request.UserId != null ? request.UserId.Value.ToString() : "-";
                    await _driverCarRepository.UpdateAsync(driverCar);
                    var log = new CreateAppLog(_applicationLogRepository);
                    log.CreateLog("Drivercar", "Drivercar", "Edit", Newtonsoft.Json.JsonConvert.SerializeObject(request),request.UserId.ToString());
                    return new Response<int>(driverCar.Id);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public string GetDocumentType(UpdateCarInformationCommand request, string fileName)
        {
            string result = "";
            if (request.CarPictures != null)
            {
                string[] CarPictures = request.CarPictures.Split(",");
                var existCarPicture = CarPictures.Where(x => x == fileName).FirstOrDefault();
                if (existCarPicture != null)
                {
                    result = "CarPicture";
                    return result;
                }
            }
            if (request.CarRegistrationFiles != null)
            {
                string[] CarRegistrationFiles = request.CarRegistrationFiles.Split(",");
                var existCarRegistrationFile = CarRegistrationFiles.Where(x => x == fileName).FirstOrDefault();
                if (existCarRegistrationFile != null)
                {
                    result = "CarRegistration";
                    return result;
                }
            }
            return result;
        }
    }
}
