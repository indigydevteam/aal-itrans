﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.DriverCars.Commands
{
    public class UpdateCarInformationCommandValidator : AbstractValidator<UpdateCarInformationCommand>
    {
        private readonly IDriverCarRepositoryAsync driverCarRepository;
        private readonly IDriverRepositoryAsync driverRepository;

        public UpdateCarInformationCommandValidator(IDriverCarRepositoryAsync driverCarRepository, IDriverRepositoryAsync driverRepository)
        {
            this.driverCarRepository = driverCarRepository;
            this.driverRepository = driverRepository;

            RuleFor(p => p.OwnerId)
                .NotNull().WithMessage("{PropertyName} is required.")
                 .MustAsync(IsExistDriver).WithMessage("{PropertyName} ont exists.");

            RuleFor(p => p.CarRegistration)
                .NotNull().WithMessage("{PropertyName} is required.")
                 .MustAsync(async (p, s, cancellation) =>
                 {
                     return await IsRegistrationUnique(p.Id, p.CarRegistration).ConfigureAwait(false);
                 }).WithMessage("{PropertyName} already exists.");

            RuleFor(p => p.DriverPhoneNumber)
               .NotNull().WithMessage("{PropertyName} is required.")
                .MustAsync(async (p, s, x, cancellation) =>
                {
                    return await IsPhoneUnique(p.Id, p.DriverPhoneNumber,p.DriverPhoneCode).ConfigureAwait(false);
                }).WithMessage("{PropertyName} already exists.");
        }

        private async Task<bool> IsRegistrationUnique(int id, string value)
        {
            var driverCarObject = (await driverCarRepository.FindByCondition(x => x.CarRegistration.ToLower().Trim().Replace(" ","") == value.ToLower().Trim().Replace(" ", "") &&  x.Id != id && x.Driver.IsDelete == false).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            if (driverCarObject == null)
            {
                return true;
            }
            return false;
        }
        private async Task<bool> IsPhoneUnique(int id, string value,string code)
        {
            if (code == "+66" && value != null && value.Trim().Length > 9 && value.Trim().First() == '0')
            {
                value = value.Trim().Remove(0, 1);
            }
            var driverCarObject = (await driverCarRepository.FindByCondition(x => x.DriverPhoneNumber.ToLower().Trim() == value.ToLower().Trim() && x.Id != id && x.Driver.IsDelete == false).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            if (driverCarObject != null)
            {
                return false;
            }
            driverCarObject = (await driverCarRepository.FindByCondition(x => x.Id.Equals(id)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            Guid driverId = new Guid();
            if (driverCarObject != null)
            {
                driverId = driverCarObject.Driver.Id;

                var driverObject = (await driverRepository.FindByCondition(x => x.PhoneNumber.ToLower().Trim() == value.ToLower().Trim() && x.Id != driverId && x.IsDelete == false).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (driverObject != null)
                {
                    return false;
                }
            }
            return true;
        }


        private async Task<bool> IsExistDriver(Guid value, CancellationToken cancellationToken)
        {
            var driverObject = (await driverRepository.FindByCondition(x => x.Id.Equals(value)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            if (driverObject == null)
            {
                return false;
            }
            return true;
        }
    }
}
