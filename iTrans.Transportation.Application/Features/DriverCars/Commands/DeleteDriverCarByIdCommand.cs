﻿using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;

namespace iTrans.Transportation.Application.Features.DriverCars.Commands
{
    public class DeleteDriverCarByIdCommand : IRequest<Response<int>>
    {
        public int Id { get; set; }
        public Guid OwnerId { get; set; }
        public class DeleteDriverCarByIdCommandHandler : IRequestHandler<DeleteDriverCarByIdCommand, Response<int>>
        {
            private readonly IDriverCarRepositoryAsync _driverCarRepository;
            private readonly IDriverCarFileRepositoryAsync _driverCarFileRepository;
            private readonly IDriverCarLocationRepositoryAsync _driverCarLocationRepository;
            private readonly IConfiguration _configuration;
            public DeleteDriverCarByIdCommandHandler(IDriverCarRepositoryAsync driverCarRepository, IDriverCarFileRepositoryAsync driverCarFileRepository, IDriverCarLocationRepositoryAsync driverCarLocationRepository
                , IConfiguration configuration)
            {
                _driverCarRepository = driverCarRepository;
                _driverCarFileRepository = driverCarFileRepository;
                _driverCarLocationRepository = driverCarLocationRepository;
                _configuration = configuration;
            }
            public async Task<Response<int>> Handle(DeleteDriverCarByIdCommand command, CancellationToken cancellationToken)
            {
                var getContentPath = _configuration.GetSection("ContentPath");
                string contentPath = getContentPath.Value;

                var driverCar = (await _driverCarRepository.FindByCondition(x => x.Id == command.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (driverCar == null)
                {
                    throw new ApiException($"DriverCar Not Found.");
                }
                else
                {
                    var driverCarFiles = (await _driverCarFileRepository.FindByCondition(x => x.DriverCar.Id.Equals(driverCar.Id)).ConfigureAwait(false)).AsQueryable().ToList();
                    try
                    {
                        foreach (DriverCarFile driverCarFile in driverCarFiles)
                        {
                            File.Delete(Path.Combine(contentPath, driverCarFile.FilePath));
                            await _driverCarFileRepository.DeleteAsync(driverCarFile);
                        }
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                    (await _driverCarLocationRepository.CreateSQLQuery("DELETE Driver_CarLocation where DriverCarId = '" + driverCar.Id.ToString() + "'").ConfigureAwait(false)).ExecuteUpdate();
                    await _driverCarRepository.DeleteAsync(driverCar);
                    return new Response<int>(driverCar.Id);
                }
            }
        }
    }
}
