﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.DriverCars.Commands
{
    public class CreateDriverCarCommandValidator : AbstractValidator<CreateDriverCarCommand>
    {
        private readonly IDriverCarRepositoryAsync driverCarRepository;

        public CreateDriverCarCommandValidator(IDriverCarRepositoryAsync driverCarRepository)
        {
            this.driverCarRepository = driverCarRepository;

            RuleFor(p => p.DriverId)
                .NotNull().WithMessage("{PropertyName} is required.") ;

            RuleFor(p => p.CarRegistration)
                .NotNull().WithMessage("{PropertyName} is required.")
                .MustAsync(IsRegistrationUnique).WithMessage("{PropertyName} already exists.");

            RuleFor(p => p.DriverPhoneNumber)
               .NotNull().WithMessage("{PropertyName} is required.")
               .MustAsync(IsPhoneUnique).WithMessage("{PropertyName} already exists.");

        }

        private async Task<bool> IsRegistrationUnique(string value, CancellationToken cancellationToken)
        {
            var driverCarObject = (await driverCarRepository.FindByCondition(x => x.CarRegistration.ToLower().Trim() == value.ToLower().Trim()).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            if (driverCarObject != null)
            {
                return false;
            }
            return true;
        }
        private async Task<bool> IsPhoneUnique(string value, CancellationToken cancellationToken)
        {
            var driverCarObject = (await driverCarRepository.FindByCondition(x => x.DriverPhoneNumber.ToLower().Trim() == value.ToLower().Trim()).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            if (driverCarObject != null)
            {
                return false;
            }
            return true;
        }
        private async Task<bool> IsIdentityNumberUnique(string value, CancellationToken cancellationToken)
        {
            var driverCarObject = (await driverCarRepository.FindByCondition(x => x.DriverIdentityNumber == value).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            if (driverCarObject != null)
            {
                return false;
            }
            return true;
        }
    }
}
