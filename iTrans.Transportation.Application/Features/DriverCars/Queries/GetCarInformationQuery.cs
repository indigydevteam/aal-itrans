﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.DriverCar;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.DriverCars.Queries
{
    public class GetCarInformationQuery : IRequest<Response<DriverCarInformationViewModel>>
    {
        public int Id { get; set; }
        public Guid OwnerId { get; set; }
    }
    public class GetCarInformationQueryHandler : IRequestHandler<GetCarInformationQuery, Response<DriverCarInformationViewModel>>
    {
        private readonly IDriverCarRepositoryAsync _driverCarRepository;
        private readonly IMapper _mapper;
        public GetCarInformationQueryHandler(IDriverCarRepositoryAsync driverCarRepository, IMapper mapper)
        {
            _driverCarRepository = driverCarRepository;
            _mapper = mapper;
        }

        public async Task<Response<DriverCarInformationViewModel>> Handle(GetCarInformationQuery request, CancellationToken cancellationToken)
        {
            try
            {

                var driverCar = (await _driverCarRepository.FindByCondition(x => x.Id == request.Id && x.Owner.Id.Equals(request.OwnerId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();

                var driverCarViewModel = _mapper.Map<DriverCarInformationViewModel>(driverCar);
                if (driverCarViewModel.driver != null)
                {
                    driverCarViewModel.driver.files.RemoveAll(f => f.IsApprove == false);
                }
                if (driverCarViewModel.owner != null)
                {
                    driverCarViewModel.owner.files.RemoveAll(f => f.IsApprove == false);
                }
                if (driverCarViewModel.driverPhoneCode == "+66" && driverCarViewModel.driverPhoneNumber != null && driverCarViewModel.driverPhoneNumber.Trim() != "")
                {
                    driverCarViewModel.driverPhoneNumber = "0" + driverCarViewModel.driverPhoneNumber;
                }
                return new Response<DriverCarInformationViewModel>(driverCarViewModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
