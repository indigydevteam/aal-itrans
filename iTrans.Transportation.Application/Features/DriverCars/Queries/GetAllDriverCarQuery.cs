﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.DriverCar;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.DriverCars.Queries
{
    public class GetAllDriverCarQuery : IRequest<Response<IEnumerable<DriverCarViewModel>>>
    {
        
    }
    public class GetAllDriverCarQueryHandler : IRequestHandler<GetAllDriverCarQuery, Response<IEnumerable<DriverCarViewModel>>>
    {
        private readonly IDriverCarRepositoryAsync _driverCarRepository;
        private readonly IMapper _mapper;
        public GetAllDriverCarQueryHandler(IDriverCarRepositoryAsync driverCarRepository, IMapper mapper)
        {
            _driverCarRepository = driverCarRepository;
            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<DriverCarViewModel>>> Handle(GetAllDriverCarQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var validFilter = _mapper.Map<GetAllDriverCarQuery>(request);
                var driver = await _driverCarRepository.GetAllAsync();
                var driverCarViewModel = _mapper.Map<IEnumerable<DriverCarViewModel>>(driver);
                return new Response<IEnumerable<DriverCarViewModel>>(driverCarViewModel);
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
    }
}
