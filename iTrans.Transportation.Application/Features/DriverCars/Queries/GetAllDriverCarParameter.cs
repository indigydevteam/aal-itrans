﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.DriverCar;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;

namespace iTrans.Transportation.Application.Features.DriverCars.Queries
{
    public class GetAllDriverCarParameter : IRequest<PagedResponse<IEnumerable<DriverCarViewModel>>>
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public string Search { set; get; }
        public string Drivertype { get; set; }


    }
    public class GetAllDriverCarParameterHandler : IRequestHandler<GetAllDriverCarParameter, PagedResponse<IEnumerable<DriverCarViewModel>>>
    {
        private readonly IDriverCarRepositoryAsync _driverCarRepository;
        private readonly IMapper _mapper;
        private Expression<Func<DriverCar, bool>> expression;

        public GetAllDriverCarParameterHandler(IDriverCarRepositoryAsync driverCarRepository, IMapper mapper)
        {
            _driverCarRepository = driverCarRepository;
            _mapper = mapper;
        }

        public async Task<PagedResponse<IEnumerable<DriverCarViewModel>>> Handle(GetAllDriverCarParameter request, CancellationToken cancellationToken)
        {
            var validFilter = _mapper.Map<GetAllDriverCarParameter>(request);
            if (validFilter.Search != null && validFilter.Search.Trim() != "")
            {
                expression = x => (x.CarRegistration.Contains(validFilter.Search.Trim()) || x.DriverName.Contains(validFilter.Search.Trim())  && x.Driver.DriverType.Equals(validFilter.Drivertype));
            }
            if (validFilter.Drivertype != null && validFilter.Drivertype.Trim() != "")
            {
                expression = x => (x.Driver.DriverType.Equals(validFilter.Drivertype));

            }
            int itemCount = _driverCarRepository.GetItemCount(expression);
            var driverCar = await _driverCarRepository.FindByConditionWithPage(expression, validFilter.PageNumber, validFilter.PageSize);
            var driverCarViewModel = _mapper.Map<IEnumerable<DriverCarViewModel>>(driverCar);
            return new PagedResponse<IEnumerable<DriverCarViewModel>>(driverCarViewModel, validFilter.PageNumber, validFilter.PageSize, itemCount);
        }
    }
}