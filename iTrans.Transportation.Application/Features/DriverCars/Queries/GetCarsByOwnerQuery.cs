﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.DriverCar;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.DriverCars.Queries
{
    public class GetCarsByOwnerQuery : IRequest<Response<IEnumerable<DriverCarInformationViewModel>>>
    {
        public Guid OwnerId { get; set; }
    }
    public class GetCarsByOwnerQueryHandler : IRequestHandler<GetCarsByOwnerQuery, Response<IEnumerable<DriverCarInformationViewModel>>>
    {
        private readonly IDriverCarRepositoryAsync _driverCarRepository;
        private readonly IMapper _mapper;
        public GetCarsByOwnerQueryHandler(IDriverCarRepositoryAsync driverCarRepository, IMapper mapper)
        {
            _driverCarRepository = driverCarRepository;
            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<DriverCarInformationViewModel>>> Handle(GetCarsByOwnerQuery request, CancellationToken cancellationToken)
        {
            try
            {
               
                var driverCar = (await _driverCarRepository.FindByCondition(x => x.Owner.Id  == request.OwnerId).ConfigureAwait(false)).AsQueryable().ToList();
                
                var driverCarViewModel = _mapper.Map<IEnumerable<DriverCarInformationViewModel>>(driverCar);
                foreach (DriverCarInformationViewModel driverCarInformation in driverCarViewModel)
                {
                    if (driverCarInformation.driver != null)
                    {
                        driverCarInformation.driver.files.RemoveAll(f => f.IsApprove == false);
                    }
                    if (driverCarInformation.owner != null)
                    {
                        driverCarInformation.owner.files.RemoveAll(f => f.IsApprove == false);
                    }
                    if (driverCarInformation.driverPhoneCode == "+66" && driverCarInformation.driverPhoneNumber != null && driverCarInformation.driverPhoneNumber.Trim() != "")
                    {
                        driverCarInformation.driverPhoneNumber = "0" + driverCarInformation.driverPhoneNumber;
                    }
                }
                return new Response<IEnumerable<DriverCarInformationViewModel>>(driverCarViewModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
