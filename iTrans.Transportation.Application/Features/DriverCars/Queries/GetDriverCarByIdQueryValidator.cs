﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using iTrans.Transportation.Application.Interfaces.Repositories;
using FluentValidation;

namespace iTrans.Transportation.Application.Features.DriverCars.Queries
{
    public class GetDriverCarByIdQueryValidator : AbstractValidator<GetDriverCarByIdQuery>
    {
        private readonly IDriverCarRepositoryAsync driverCarRepository;

        public GetDriverCarByIdQueryValidator(IDriverCarRepositoryAsync driverCarRepository)
        {
            this.driverCarRepository = driverCarRepository;
            RuleFor(p => p.Id)
               .NotEmpty().WithMessage("{PropertyName} is required.")
               .NotNull()
               .MustAsync(IsDriverCarExists).WithMessage("{PropertyName} not exists.");
        }

        private async Task<bool> IsDriverCarExists(int DriverId, CancellationToken cancellationToken)
        {
            var userObject = (await driverCarRepository.FindByCondition(x => x.Id.Equals(DriverId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            if (userObject != null)
            {
                return true;
            }
            return false;
        }
    }
}
