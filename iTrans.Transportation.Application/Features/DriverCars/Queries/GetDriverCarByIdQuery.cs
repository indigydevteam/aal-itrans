﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.DriverCar;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.DriverCars.Queries
{
    public class GetDriverCarByIdQuery : IRequest<Response<DriverCarViewModel>>
    {
        public int Id { get; set; }
    }
    public class GetDriverCarByIdQueryHandler : IRequestHandler<GetDriverCarByIdQuery, Response<DriverCarViewModel>>
    {
        private readonly IDriverCarRepositoryAsync _driverCarRepository;
        private readonly IMapper _mapper;
        public GetDriverCarByIdQueryHandler(IDriverCarRepositoryAsync driverCarRepository, IMapper mapper)
        {
            _driverCarRepository = driverCarRepository;
            _mapper = mapper;
        }
        public async Task<Response<DriverCarViewModel>> Handle(GetDriverCarByIdQuery request, CancellationToken cancellationToken)
        {
            var driverObject = (await _driverCarRepository.FindByCondition(x => x.Id.Equals(request.Id)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            if (driverObject.DriverPhoneCode == "+66" && driverObject.DriverPhoneNumber != null && driverObject.DriverPhoneNumber.Trim() != "")
            {
                driverObject.DriverPhoneNumber = "0" + driverObject.DriverPhoneNumber;
            }
            return new Response<DriverCarViewModel>(_mapper.Map<DriverCarViewModel>(driverObject));
        }
    }
}
