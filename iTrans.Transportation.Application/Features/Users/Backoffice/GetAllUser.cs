﻿using AutoMapper;
using iTrans.Transportation.Application.DTOs.User.backoffice;
using iTrans.Transportation.Application.DTOs.UserLevel;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.Users.Backoffice
{
    public class GetAllUser : IRequest<Response<IEnumerable<UserViewModel>>>
    {
        public string Search { set; get; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public int Active { set; get; }
        public string Column { set; get; }
    }
    public class GetAllUserHandler : IRequestHandler<GetAllUser, Response<IEnumerable<UserViewModel>>>
    {
        private readonly ISystemUserRepositoryAsync _userRepository;
        private readonly IMapper _mapper;
        public GetAllUserHandler(ISystemUserRepositoryAsync userRepository, IMapper mapper)
        {
            _userRepository = userRepository;
            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<UserViewModel>>> Handle(GetAllUser request, CancellationToken cancellationToken)
        {
            var validFilter = _mapper.Map<GetAllUser>(request);
            var result = from x in _userRepository.GetAllAsync().Result.ToList().OrderByDescending(x => x.Modified)
                         select new UserViewModel
                         {
                             emplID = x.EmplID,
                             name_TH = x.FirstNameEN+" "+x.LastNameEN != null ? x.FirstNameEN+" "+x.LastNameEN : "-",
                             name_EN = x.FirstNameTH+" "+x.LastNameTH != null ? x.FirstNameTH+" "+x.LastNameTH : "-",
                             phoneNumber = x.PhoneCode+""+x.PhoneNumber != null ? x.PhoneCode+""+ x.PhoneNumber : "-",
                             email = x.Email != null ? x.Email : "-",
                             role_Name = x.Role != null ? x.Role.NameEN :"-",
                             modifiedBy = x.ModifiedBy != null ? x.ModifiedBy : "-",
                             modified = x.Modified.ToString() != null ? x.Modified.ToString() : "-",
                             imageprofile = x.Imageprofile
                         };


            if (validFilter.Search != null && validFilter.Search.Trim() != "")
            {
                result = result.Where(x => x.name_TH.Contains(validFilter.Search.Trim()) || x.name_EN.Contains(validFilter.Search.Trim())
                         || x.phoneNumber.Contains(validFilter.Search.Trim()) || x.role_Name.Contains(validFilter.Search.Trim())
                         || x.email.Contains(validFilter.Search.Trim()) || x.modifiedBy.Contains(validFilter.Search.Trim())
                         || x.modified.Contains(validFilter.Search.Trim())).ToList();
            }

            if (request.Column == "name_TH" && request.Active == 3)
            {
                result = result.OrderBy(x => x.name_TH);
            }
            if (request.Column == "name_TH" && request.Active == 2)
            {
                result = result.OrderByDescending(x => x.name_TH);
            }
            if (request.Column == "name_EN" && request.Active == 3)
            {
                result = result.OrderBy(x => x.name_EN);
            }
            if (request.Column == "name_EN" && request.Active == 2)
            {
                result = result.OrderByDescending(x => x.name_EN);
            }
            if (request.Column == "phoneNumber" && request.Active == 3)
            {
                result = result.OrderBy(x => x.phoneNumber);
            }
            if (request.Column == "phoneNumber" && request.Active == 2)
            {
                result = result.OrderByDescending(x => x.phoneNumber);
            }
            if (request.Column == "role_Name" && request.Active == 3)
            {
                result = result.OrderBy(x => x.role_Name);
            }
            if (request.Column == "role_Name" && request.Active == 2)
            {
                result = result.OrderByDescending(x => x.role_Name);
            }
            if (request.Column == "email" && request.Active == 3)
            {
                result = result.OrderBy(x => x.email);
            }
            if (request.Column == "email" && request.Active == 2)
            {
                result = result.OrderByDescending(x => x.email);
            }
            if (request.Column == "modifiedBy" && request.Active == 3)
            {
                result = result.OrderBy(x => x.modifiedBy);
            }
            if (request.Column == "modifiedBy" && request.Active == 2)
            {
                result = result.OrderByDescending(x => x.modifiedBy);
            }
            if (request.Column == "modified" && request.Active == 3)
            {
                result = result.OrderBy(x => x.modified);
            }
            if (request.Column == "modified" && request.Active == 2)
            {
                result = result.OrderByDescending(x => x.modified);
            }


            var itemCount = result.Count();
            result = result.Skip((validFilter.PageNumber - 1) * validFilter.PageSize).Take(validFilter.PageSize);
            return new PagedResponse<IEnumerable<UserViewModel>>(result, validFilter.PageNumber, validFilter.PageSize, itemCount);
        }
    }
}
