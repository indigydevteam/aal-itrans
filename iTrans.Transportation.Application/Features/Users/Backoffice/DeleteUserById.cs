﻿using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.Users.Backoffice
{
   public class DeleteUserById : IRequest<Response<Guid?>>
    {
        public string Id { get; set; }
        public class DeleteUserByIdHandler : IRequestHandler<DeleteUserById, Response<Guid?>>
        {
            private readonly ISystemUserRepositoryAsync _userRepository;
            public DeleteUserByIdHandler(ISystemUserRepositoryAsync userRepository)
            {
                _userRepository = userRepository;
            }
            public async Task<Response<Guid?>> Handle(DeleteUserById command, CancellationToken cancellationToken)
            {
                var data = (await _userRepository.FindByCondition(x => x.EmplID.ToString().Equals(command.Id)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (data == null)
                {
                    throw new ApiException($"User Not Found.");
                }
                else
                {
                    await _userRepository.DeleteAsync(data);
                    return new Response<Guid?>(data.EmplID);
                }
            }
        }
    }
}
