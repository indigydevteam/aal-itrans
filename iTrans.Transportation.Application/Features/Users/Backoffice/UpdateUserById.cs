﻿using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain.Entities;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
namespace iTrans.Transportation.Application.Features.Users.Backoffice
{
    public class UpdateUserById : IRequest<Response<string>>
    {   
        public Guid? UserId { get; set; }
        public Guid? Id { get; set; }
        public int RoleId { get; set; }
        public string FirstNameTH { get; set; }
        public string FirstNameEN { get; set; }
        public string LastNameTH { get; set; }
        public string LastNameEN { get; set; }
        public string MiddleNameTH { get; set; }
        public string MiddleNameEN { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public int TitleId { get; set; }
        public string IdentityNumber { get; set; }
        public DateTime Birthday { get; set; }
        public DateTime ExpiredDate { get; set; }
        public string ContentDirectory { get; set; }
        public IFormFile Imageprofile { get; set; }




    }
    public class UpdateUserByIdHandler : IRequestHandler<UpdateUserById, Response<string>>
    {
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly ISystemUserRepositoryAsync _userRepository;
        private readonly IRoleRepositoryAsync _roleRepository;
        private readonly ITitleRepositoryAsync _titleRepository;
        private readonly IMapper _mapper;

        public UpdateUserByIdHandler(IApplicationLogRepositoryAsync applicationLogRepository, ISystemUserRepositoryAsync userRepository, IRoleRepositoryAsync roleRepository, ITitleRepositoryAsync titleRepository, IMapper mapper)
        {
            _applicationLogRepository = applicationLogRepository;
            _userRepository = userRepository;
            _roleRepository = roleRepository;
            _titleRepository = titleRepository;
            _mapper = mapper;
        }

        public async Task<Response<string>> Handle(UpdateUserById request, CancellationToken cancellationToken)
        {
            try
            {
                var role = (await _roleRepository.FindByCondition(x => x.Id.Equals(request.RoleId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();

                var user = _mapper.Map<SystemUsers>((await _userRepository.FindByCondition(x => x.EmplID.ToString().Equals(request.Id.ToString())).ConfigureAwait(false)).AsQueryable().FirstOrDefault());
                if (user == null)
                {
                    throw new ApiException($"User Not Found.");
                }
                else
                {
                    string folderPath = request.ContentDirectory + "user/" + request.Id.ToString();
                    if (!Directory.Exists(folderPath))
                    {
                        Directory.CreateDirectory(folderPath);
                    }
                    if (request.Imageprofile != null)
                    {
                        var imgName = DateTime.Now.ToString("yyyymmddMMss") + "_" + Path.GetFileName(request.Imageprofile.FileName);
                        var imgFullPath = Path.Combine(folderPath, imgName);

                        using (var fileStream = new FileStream(imgFullPath, FileMode.Create))
                        {
                            await request.Imageprofile.CopyToAsync(fileStream);
                        }

                        //user.Imageprofile = imgName;
                        user.Imageprofile = "user/" + request.Id.ToString() + "/" + imgName; ;

                       
                    }
                    user.EmplID = request.Id;
                    user.Role = role;
                    user.FirstNameTH = request.FirstNameTH;
                    user.FirstNameEN = request.FirstNameEN;
                    user.LastNameTH = request.LastNameTH;
                    user.LastNameEN = request.LastNameEN;
                    user.MiddleNameTH = request.MiddleNameTH;
                    user.MiddleNameEN = request.MiddleNameEN;
                    user.PhoneNumber = request.PhoneNumber;
                    user.Title = _titleRepository.GetAllAsync().Result.Where(x => x.Id.Equals(request.TitleId)).FirstOrDefault();
                    user.Email = request.Email;
                    user.IdentityNumber = request.IdentityNumber;
                    user.Birthday = request.Birthday;
                    user.ExpiredDate = request.ExpiredDate;
                    user.Modified = DateTime.UtcNow;
                    user.ModifiedBy = request.UserId.ToString();
                    user.ModifiedByTH = request.UserId.ToString();
                    user.ModifiedByEN = request.UserId.ToString();




                    await _userRepository.UpdateAsync(user);
                    var log = new CreateAppLog(_applicationLogRepository);
                    log.CreateLog("User", "User", "Edit", Newtonsoft.Json.JsonConvert.SerializeObject(user),request.UserId.ToString());
                    return new Response<string>(user.EmplID.ToString());
                }
               
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
