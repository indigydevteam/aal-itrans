﻿using AutoMapper;
using iTrans.Transportation.Application.DTOs.MenuPermission;
using iTrans.Transportation.Application.DTOs.Role;
using iTrans.Transportation.Application.DTOs.User;
using iTrans.Transportation.Application.DTOs.UserLevel;
using iTrans.Transportation.Application.DTOs.UserLevelCondition;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Features.CarLists.Queries;
using iTrans.Transportation.Application.Helper;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;


namespace iTrans.Transportation.Application.Features.Users.Backoffice
{
   public class GetUserProfile : IRequest<Response<UserViewModel>>
    {
        public Guid? UserId { get; set; }
        public string Role { get; set; }
        public string ExpiredDate { get; set; }
    }
    public class GetUserProfileHandler : IRequestHandler<GetUserProfile, Response<UserViewModel>>
    {
        private readonly ISystemUserRepositoryAsync _userRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IRoleRepositoryAsync _roleRepository;
        private readonly IRoleMenuRepositoryAsync _roleMenuRepository;
        private readonly IMenuPermissionRepositoryAsync _menuPermissionRepository;
        private readonly IMenuRepositoryAsync _menuRepository;
        private readonly ITitleRepositoryAsync _titleRepository;





        private readonly IMapper _mapper;
        public GetUserProfileHandler(ISystemUserRepositoryAsync userLevelRepository, IApplicationLogRepositoryAsync applicationLogRepository, IRoleRepositoryAsync roleRepository, IRoleMenuRepositoryAsync roleMenuRepository
                   , IMenuPermissionRepositoryAsync menuPermissionRepository, IMenuRepositoryAsync menuRepository, ITitleRepositoryAsync titleRepository,IMapper mapper)
        {
            _userRepository = userLevelRepository;
            _applicationLogRepository = applicationLogRepository;
            _roleRepository = roleRepository;
            _roleMenuRepository = roleMenuRepository;
            _menuPermissionRepository = menuPermissionRepository;
            _menuRepository = menuRepository;
            _titleRepository = titleRepository;
            _mapper = mapper;
        }
        public async Task<Response<UserViewModel>> Handle(GetUserProfile request, CancellationToken cancellationToken)
        {
            try
            {

                var user = _mapper.Map<SystemUsers>((await _userRepository.FindByCondition(x => x.EmplID.ToString().Equals(request.UserId.ToString())).ConfigureAwait(false)).AsQueryable().FirstOrDefault());
                var role = (await _roleRepository.FindByCondition(x => x.NameTH.ToLower().Equals(request.Role.ToLower())).ConfigureAwait(false)).AsQueryable().FirstOrDefault();


                SystemUsers data = new SystemUsers();

                if (user == null)
                {

                    data.EmplID = request.UserId;
                    data.Role = role;
                    data.Password = AESMgr.Encrypt("P@ssw0rd"); ;
                    data.FirstNameTH = "FirstName";
                    data.FirstNameEN = "FirstName";
                    data.LastNameTH = "LastName";
                    data.LastNameEN = "LastName";
                    data.MiddleNameTH = "MiddleNameTH";
                    data.MiddleNameEN = "MiddleNameEN";
                    data.PhoneCode = "+66";
                    data.PhoneNumber = "0912345678";
                    data.Email = "email@gmail.com";
                    data.IdentityNumber = AESMgr.Encrypt("111111111111"); ;
                    data.Title = _titleRepository.GetAllAsync().Result.Where(x => x.Id.Equals(1)).FirstOrDefault();
                    data.Birthday = DateTime.UtcNow;
                    data.EffectiveDate = DateTime.UtcNow;
                    data.ExpiredDate = DateTime.UtcNow;
                    data.Activated = DateTime.UtcNow;
                    data.Reactivated = DateTime.UtcNow;
                    data.Created = DateTime.UtcNow;
                    data.CreatedBy = request.UserId.ToString();
                    data.Modified = DateTime.UtcNow;

                    user = data;

                    await _userRepository.AddAsync(user);
                    var log = new CreateAppLog(_applicationLogRepository);
                    log.CreateLog("User", "User", "create", Newtonsoft.Json.JsonConvert.SerializeObject(user), request.UserId.ToString());

                }
                user.Password = AESMgr.Decrypt(user.Password);
                user.IdentityNumber = AESMgr.Decrypt(user.IdentityNumber);

                var menuPermissions = await _menuPermissionRepository.GetAllAsync();
                var roleMenu = await _roleMenuRepository.GetAllAsync();
                var menu = await _menuRepository.GetAllAsync();

                var result = _mapper.Map<UserViewModel>(user);



                var date = menu.Select(p => new MenuPermissionViewModel
                {
                    Id = p.Id,
                    RoleId = user.Role.Id,
                    Rolename = user.Role != null ? user.Role.NameTH : " ",
                    MenuName = p.NameTH,
                    MenuNameEN = p.NameEN,
                    Path = p.Path,
                    Icon = p.Icon,
                    IsShow = roleMenu.Where(r => r.MenuId.Equals(p.Id) && r.RoleId.Equals(user.Role.Id)).Any(),

                    RoleMenuPermission = menuPermissions.Where(x => x.MenuId.Equals(p.Id)).Select(x => new RoleMenuPermission
                    {
                        Id = x.Id,
                        MenuId = x.MenuId,
                        MenuPermissionId = x.Id,
                        Is_Check = roleMenu.Where(r => r.MenuPermission.Id.Equals(x.Id) && r.MenuId.Equals(x.MenuId) && r.RoleId.Equals(user.Role.Id)).Any(),
                        Permission = x.Permission,

                    }).ToList()

                });

                result.MenuPermission = _mapper.Map<List<MenuPermissionViewModel>>(date);



                return new Response<UserViewModel>(result);
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
    }
}
