﻿using AutoMapper;
using iTrans.Transportation.Application.DTOs.MenuPermission;
using iTrans.Transportation.Application.DTOs.Role;
using iTrans.Transportation.Application.DTOs.User;
using iTrans.Transportation.Application.DTOs.UserLevel;
using iTrans.Transportation.Application.DTOs.UserLevelCondition;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Features.CarLists.Queries;
using iTrans.Transportation.Application.Helper;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;



namespace iTrans.Transportation.Application.Features.Users.Backoffice
{
  public  class CreateUser : IRequest<Response<UserViewModel>>
    {
        public Guid? UserId { get; set; }
        public int RoleId { get; set; }
        public string EmplID { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string UserToken { get; set; }
        public bool IsNaverExpire { get; set; }
        public bool IsExternalUser { get; set; }
        public DateTime ExpiredDate { get; set; }
        public bool IsActive { get; set; }
        public DateTime Activated { get; set; }
        public int IncorrectLoginCount { get; set; }
        public string FirstNameTH { get; set; }
        public string FirstNameEN { get; set; }
        public string LastNameTH { get; set; }
        public string LastNameEN { get; set; }
        public string MiddleNameTH { get; set; }
        public string MiddleNameEN { get; set; }
        public string PhoneCode { get; set; }
        public string PhoneNumber { get; set; }
        public string IdentityNumber { get; set; }
        public string Imageprofile { get; set; }
        public string ContentDirectory { get; set; }
        public int TitleId { get; set; }
        public DateTime Birthday { get; set; }
    }
    public class CreateUserHandler : IRequestHandler<CreateUser, Response<UserViewModel>>
    {
        private readonly ISystemUserRepositoryAsync _userRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IRoleRepositoryAsync _roleRepository;
        private readonly IRoleMenuRepositoryAsync _roleMenuRepository;
        private readonly IMenuPermissionRepositoryAsync _menuPermissionRepository;
        private readonly IMenuRepositoryAsync _menuRepository;
        private readonly ITitleRepositoryAsync _titleRepository;





        private readonly IMapper _mapper;
        public CreateUserHandler(ISystemUserRepositoryAsync userLevelRepository, IApplicationLogRepositoryAsync applicationLogRepository, IRoleRepositoryAsync roleRepository, IRoleMenuRepositoryAsync roleMenuRepository
                   , IMenuPermissionRepositoryAsync menuPermissionRepository, IMenuRepositoryAsync menuRepository, ITitleRepositoryAsync titleRepository, IMapper mapper)
        {
            _userRepository = userLevelRepository;
            _applicationLogRepository = applicationLogRepository;
            _roleRepository = roleRepository;
            _roleMenuRepository = roleMenuRepository;
            _menuPermissionRepository = menuPermissionRepository;
            _menuRepository = menuRepository;
            _titleRepository = titleRepository;
            _mapper = mapper;
        }
        public async Task<Response<UserViewModel>> Handle(CreateUser request, CancellationToken cancellationToken)
        {

            var data = _mapper.Map<SystemUsers>(request);
            var role = (await _roleRepository.FindByCondition(x => x.Id.Equals(request.RoleId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();

            //IdentityNumber || Password =>encrip 

            var emplID = Guid.NewGuid();

            string identityNumber = AESMgr.Encrypt(request.IdentityNumber);
            string password = AESMgr.Encrypt("P@assw0rd");


                data.EmplID = emplID;
                data.Role = role;
                data.Password = password;
                //data.FirstNameTH = request.FirstNameTH;
                //data.FirstNameEN = request.FirstNameEN;
                //data.LastNameTH = request.LastNameTH;
                //data.LastNameEN = request.LastNameEN;
                //data.MiddleNameTH = request.MiddleNameTH;
                //data.MiddleNameEN = request.MiddleNameEN;
                //data.PhoneCode = request.PhoneCode;
                //data.PhoneNumber = request.PhoneNumber;
                //data.Email = request.Email;
                data.IdentityNumber = identityNumber;
                data.Title = _titleRepository.GetAllAsync().Result.Where(x => x.Id.Equals(request.TitleId)).FirstOrDefault();
                data.Birthday = request.Birthday;
                data.Birthday = DateTime.UtcNow;
                data.EffectiveDate = DateTime.UtcNow;
                data.ExpiredDate = request.ExpiredDate;
                data.Activated = request.Activated;
                data.Reactivated = DateTime.UtcNow;
                data.Created = DateTime.UtcNow;
                data.CreatedBy = request.UserId.ToString();
                data.Modified = DateTime.UtcNow;



            await _userRepository.AddAsync(data);
                var log = new CreateAppLog(_applicationLogRepository);
                log.CreateLog("User", "User", "create", Newtonsoft.Json.JsonConvert.SerializeObject(data),request.UserId.ToString());

            
    

            return new Response<UserViewModel>(_mapper.Map<UserViewModel>(data));
        }
    }
}