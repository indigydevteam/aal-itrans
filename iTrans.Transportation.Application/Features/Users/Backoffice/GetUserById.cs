﻿using AutoMapper;
using iTrans.Transportation.Application.DTOs.MenuPermission;
using iTrans.Transportation.Application.DTOs.Role;
using iTrans.Transportation.Application.DTOs.User;
using iTrans.Transportation.Application.DTOs.UserLevel;
using iTrans.Transportation.Application.DTOs.UserLevelCondition;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.CarLists.Queries;
using iTrans.Transportation.Application.Helper;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.Users.Backoffice
{
    public class GetUserById : IRequest<Response<UserViewModel>>
    {
        public string Id { get; set; }
        public string Role { get; set; }


    }
    public class GetUserByIdHandler : IRequestHandler<GetUserById, Response<UserViewModel>>
    {
        private readonly ISystemUserRepositoryAsync _userRepository;
        private readonly IRoleMenuRepositoryAsync _roleMenuRepository;
        private readonly IRoleRepositoryAsync _roleRepository;
        private readonly IMenuPermissionRepositoryAsync _menuPermissionRepository;
        private readonly IMenuRepositoryAsync _menuRepository;
        private readonly IMapper _mapper;
        public GetUserByIdHandler(ISystemUserRepositoryAsync userLevelRepository, IRoleRepositoryAsync roleRepository, IMenuPermissionRepositoryAsync menuPermissionRepository, IMenuRepositoryAsync menuRepository, IRoleMenuRepositoryAsync roleMenuRepository,IMapper mapper)
        {
            _userRepository = userLevelRepository;
            _roleMenuRepository = roleMenuRepository;
            _roleRepository = roleRepository;
            _menuPermissionRepository = menuPermissionRepository;
            _menuRepository = menuRepository;
            _mapper = mapper;
        }
        public async Task<Response<UserViewModel>> Handle(GetUserById request, CancellationToken cancellationToken)
        {

            var user = _mapper.Map<UserViewModel>((await _userRepository.FindByCondition(x => x.EmplID.ToString().Equals(request.Id)).ConfigureAwait(false)).AsQueryable().FirstOrDefault());
            if (user == null)
            {
                throw new ApiException($"User Not Found.");
            }
            else {
                user.Password = AESMgr.Decrypt(user.Password);
                user.IdentityNumber = AESMgr.Decrypt(user.IdentityNumber);

                var role = (await _roleRepository.FindByCondition(x => x.NameTH.ToLower().Equals(request.Role.ToLower()) || x.NameEN.ToLower().Equals(request.Role.ToLower())).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                var menuPermissions = await _menuPermissionRepository.GetAllAsync();
                var roleMenu = await _roleMenuRepository.GetAllAsync();
                var menu = await _menuRepository.GetAllAsync();



                //var date = menu.Select(p => new MenuPermissionViewModel
                //{
                //    Id = p.Id,
                //    RoleId = role.Id,
                //    Rolename = role != null ? role.NameTH : " ",
                //    MenuName = p.NameTH,

                //    RoleMenuPermission = menuPermissions.Where(x => x.MenuId.Equals(p.Id)).Select(x => new RoleMenuPermission
                //    {
                //        Id = x.Id,
                //        MenuId = x.MenuId,
                //        MenuPermissionId = x.Id,
                //        Is_Check = roleMenu.Where(r => r.MenuPermission.Id.Equals(x.Id) && r.MenuId.Equals(x.MenuId) && r.RoleId.Equals(role.Id)).Any(),
                //        Permission = x.Permission,

                //    }).ToList()

                //});

                //user.MenuPermission = _mapper.Map<List<MenuPermissionViewModel>>(date);
            }


            return new Response<UserViewModel>(user);
        }
    }
}
