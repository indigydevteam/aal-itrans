﻿using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.Users.Commands
{
   public class UpdateUserCommand : IRequest<Response<string>>
    {
        public  string Id { get; set; }
        public  string Email { get; set; }
        public  string Password { get; set; }
        public  string UserToken { get; set; }
        public  bool IsNaverExpire { get; set; }
        public  bool IsExternalUser { get; set; }
        public  DateTime EffectiveDate { get; set; }
        public  DateTime ExpiredDate { get; set; }
        public  bool IsActive { get; set; }
        public  DateTime Activated { get; set; }
        public  DateTime Reactivated { get; set; }
        public  int IncorrectLoginCount { get; set; }
        public  string FirstNameTH { get; set; }
        public  string FirstNameEN { get; set; }
        public  string LastNameTH { get; set; }
        public  string LastNameEN { get; set; }
        public  string CreatedByEN { get; set; }
        public  string CreatedByTH { get; set; }
        public  string CreatedByID { get; set; }
        public  string ModifiedByEN { get; set; }
        public  string ModifiedByTH { get; set; }
        public  string ModifiedByID { get; set; }


    }
    public class UpdateUserCommandHandler : IRequestHandler<UpdateUserCommand, Response<string>>
    {
        private readonly ISystemUserRepositoryAsync _userRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;

        public UpdateUserCommandHandler(ISystemUserRepositoryAsync userRepository, IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _userRepository = userRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
        }

        public async Task<Response<string>> Handle(UpdateUserCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var user = (await _userRepository.FindByCondition(x => x.EmplID.ToString().Equals(request.Id)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (user == null)
                {
                    throw new ApiException($"User Not Found.");
                }
                else
                {
                    user.Email = request.Email;
                    user.Password = request.Password;
                    user.UserToken = request.UserToken;
                    user.IsNaverExpire = request.IsNaverExpire;
                    user.IsExternalUser = request.IsExternalUser;
                    user.EffectiveDate = request.EffectiveDate;
                    user.ExpiredDate = request.ExpiredDate;     
                    user.IsActive = request.IsActive;
                    user.Activated = request.Activated;
                    user.Reactivated = request.Reactivated;
                    user.IncorrectLoginCount = request.IncorrectLoginCount;
                    user.FirstNameTH = request.FirstNameTH;
                    user.FirstNameEN = request.FirstNameEN;
                    user.LastNameTH = request.LastNameTH;
                    user.LastNameEN = request.LastNameEN;
                    user.LastNameEN = request.LastNameEN;
                    user.ModifiedByEN = request.ModifiedByEN;
                    user.ModifiedByTH = request.ModifiedByTH;
                    user.ModifiedByID = request.ModifiedByID;
                    user.Modified = DateTime.UtcNow;


                    await _userRepository.UpdateAsync(user);
                    var log = new CreateAppLog(_applicationLogRepository);
                    log.Create("User", "User", "Edit", Newtonsoft.Json.JsonConvert.SerializeObject(user));
                    return new Response<string>(user.EmplID.ToString());

                }
            }    

            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}