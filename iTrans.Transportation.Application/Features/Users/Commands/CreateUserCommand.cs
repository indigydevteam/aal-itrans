﻿using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using iTrans.Transportation.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.Users.Commands
{
  public partial class CreateUserCommand: IRequest<Response<string>>
    {

        public string EmplID { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string UserToken { get; set; }
        public bool IsNaverExpire { get; set; }
        public bool IsExternalUser { get; set; }
        public DateTime EffectiveDate { get; set; }
        public DateTime ExpiredDate { get; set; }
        public bool IsActive { get; set; }
        public DateTime Activated { get; set; }
        public DateTime Reactivated { get; set; }
        public int IncorrectLoginCount { get; set; }
        public string FirstNameTH { get; set; }
        public string FirstNameEN { get; set; }
        public string LastNameTH { get; set; }
        public string LastNameEN { get; set; }
        public string CreatedByEN { get; set; }
        public string CreatedByTH { get; set; }
        public string CreatedByID { get; set; }
        public string ModifiedByEN { get; set; }
        public string ModifiedByTH { get; set; }
        public string ModifiedByID { get; set; }
        public string Imageprofile { get; set; }

    }
    public class CreateUserCommandHandler : IRequestHandler<CreateUserCommand, Response<string>>
{
    private readonly ISystemUserRepositoryAsync _userRepository;
    private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
    private readonly IMapper _mapper;
    public CreateUserCommandHandler(ISystemUserRepositoryAsync userRepository, IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
    {
        _userRepository = userRepository;
        _applicationLogRepository = applicationLogRepository;
        _mapper = mapper;
    }

    public async Task<Response<string>> Handle(CreateUserCommand request, CancellationToken cancellationToken)
    {
        try
        {    
                var user = _mapper.Map<SystemUsers>(request);
                user.Created = DateTime.UtcNow;
                user.Modified = DateTime.UtcNow;
          
                var dataListObject = await _userRepository.AddAsync(user);
                var log = new CreateAppLog(_applicationLogRepository);
                log.Create("User", "User", "Create", Newtonsoft.Json.JsonConvert.SerializeObject(user));
                return new Response<string>(dataListObject.EmplID.ToString());
            
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
}
}