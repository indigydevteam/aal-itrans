﻿using AutoMapper;
using iTrans.Transportation.Application.DTOs.User;
using iTrans.Transportation.Application.DTOs.UserLevel;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.UserLevels.Queries
{
    public class GetAllUserQuery : IRequest<Response<IEnumerable<UserViewModel>>>
    {

    }
    public class GetAllUserQueryHandler : IRequestHandler<GetAllUserQuery, Response<IEnumerable<UserViewModel>>>
    {
        private readonly ISystemUserRepositoryAsync _userRepository;
        private readonly IMapper _mapper;
        public GetAllUserQueryHandler(ISystemUserRepositoryAsync userRepository, IMapper mapper)
        {
            _userRepository = userRepository;
            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<UserViewModel>>> Handle(GetAllUserQuery request, CancellationToken cancellationToken)
        {
            var user = await _userRepository.GetAllAsync();
            var UserViewModel = _mapper.Map<IEnumerable<UserViewModel>>(user);
            return new Response<IEnumerable<UserViewModel>>(UserViewModel);
        }
    }
}
