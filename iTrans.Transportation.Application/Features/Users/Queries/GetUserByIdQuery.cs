﻿using AutoMapper;
using iTrans.Transportation.Application.DTOs.User;
using iTrans.Transportation.Application.DTOs.UserLevel;
using iTrans.Transportation.Application.DTOs.UserLevelCondition;
using iTrans.Transportation.Application.Features.CarLists.Queries;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.UserLevels.Queries
{
   public class GetUserByIdQuery : IRequest<Response<UserViewModel>>
    {
        public string Id { get; set; }
    }
    public class GetUserByIdQueryHandler : IRequestHandler<GetUserByIdQuery, Response<UserViewModel>>
    {
        private readonly ISystemUserRepositoryAsync _userRepository;
        private readonly IMapper _mapper;
        public GetUserByIdQueryHandler(ISystemUserRepositoryAsync userLevelRepository, IMapper mapper)
        {
            _userRepository = userLevelRepository;
            _mapper = mapper;
        }
        public async Task<Response<UserViewModel>> Handle(GetUserByIdQuery request, CancellationToken cancellationToken)
        {
          
            var userLeveltObject = (await _userRepository.FindByCondition(x => x.EmplID.Equals(request.Id)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();

            //var userlevel = (await _userLevelConditionRepository.FindByCondition(x => x.UserLevelId.Equals(userLeveltObject.Level) && x.Active == true).ConfigureAwait(false)).AsQueryable().ToList();
            // var userLevelCondition = _mapper.Map<List<Condition>>(userlevel.OrderBy(x => x.Sequence));



            return new Response<UserViewModel>(_mapper.Map<UserViewModel>(userLeveltObject));
        }
    }
}
