﻿using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Interfaces.Services;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using iTrans.Transportation.Domain.Entities;
using MediatR;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.DriverPayments.Commands
{
    public partial class CreateDriverPaymentCommand : IRequest<Response<int>>
    {

        public Guid DriverId { get; set; }
        public int TypeId { get; set; }
        public string Bank { get; set; }
        public string AccountType { get; set; }
        public  string Value { get; set; }
        public decimal Amount { get; set; }
        public string Description { get; set; }

    }
    public class CreateDriverPaymentCommandHandler : IRequestHandler<CreateDriverPaymentCommand, Response<int>>
    {
        private readonly IDriverRepositoryAsync _driverRepository;
        private readonly IDriverPaymentRepositoryAsync _driverPaymentRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IPaymentTypeRepositoryAsync _paymentTypeRepository;
        private readonly IMapper _mapper;
        public CreateDriverPaymentCommandHandler(IDriverRepositoryAsync driverRepository,IDriverPaymentRepositoryAsync driverPaymentRepository,
             IPaymentTypeRepositoryAsync paymentTypeRepository, IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _driverRepository = driverRepository;
            _driverPaymentRepository = driverPaymentRepository;
            _applicationLogRepository = applicationLogRepository;
            _paymentTypeRepository = paymentTypeRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(CreateDriverPaymentCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var driver = (await _driverRepository.FindByCondition(x => x.Id.Equals(request.DriverId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (driver == null)
                {
                    throw new ApiException($"Driver Not Found.");
                }
                var paymentType = (await _paymentTypeRepository.FindByCondition(x => x.Id.Equals(request.TypeId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                var driverPayment = _mapper.Map<DriverPayment>(request);
                driverPayment.Type = paymentType;
                driverPayment.Driver = driver;
                driverPayment.Created = DateTime.UtcNow;
                driverPayment.Modified = DateTime.UtcNow;
                var DriverPaymentObject = await _driverPaymentRepository.AddAsync(driverPayment);
                var log = new CreateAppLog(_applicationLogRepository);
                log.Create("Driver", "Driver Payment", "Create", Newtonsoft.Json.JsonConvert.SerializeObject(request));
                return new Response<int>(DriverPaymentObject.Id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
