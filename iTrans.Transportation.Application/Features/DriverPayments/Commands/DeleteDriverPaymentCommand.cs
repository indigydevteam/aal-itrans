﻿using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
namespace iTrans.Transportation.Application.Features.DriverPayments.Commands
{
    public class DeleteDriverPaymentCommand : IRequest<Response<int>>
    {
        public Guid DriverId { get; set; }
        public int Id { get; set; }
        public class DeleteDriverPaymentCommandHandler : IRequestHandler<DeleteDriverPaymentCommand, Response<int>>
        {
            private readonly IDriverPaymentRepositoryAsync _driverPaymentRepository;
            public DeleteDriverPaymentCommandHandler(IDriverPaymentRepositoryAsync driverPaymentRepository)
            {
                _driverPaymentRepository = driverPaymentRepository;
            }
            public async Task<Response<int>> Handle(DeleteDriverPaymentCommand command, CancellationToken cancellationToken)
            {
                var driverPayment = (await _driverPaymentRepository.FindByCondition(x => x.Id == command.Id && x.Driver.Id.Equals(command.DriverId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (driverPayment == null)
                {
                    throw new ApiException($"Payment Not Found.");
                }
                else
                {
                    await _driverPaymentRepository.DeleteAsync(driverPayment);
                    return new Response<int>(driverPayment.Id);
                }
            }
        }
    }
}
