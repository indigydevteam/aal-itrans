﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;

namespace iTrans.Transportation.Application.Features.DriverPayments.Commands
{
    public class UpdateDriverPaymentCommand : IRequest<Response<int>>
    {
        public int Id { get; set; }
        public Guid DriverId { get; set; }
        public int TypeId { get; set; }
        public string Bank { get; set; }
        public string AccountType { get; set; }
        public string Value { get; set; }
        public decimal Amount { get; set; }
        public string Channel { get; set; }
        public string Description { get; set; }
    }
    public class UpdateDriverPaymentCommandHandler : IRequestHandler<UpdateDriverPaymentCommand, Response<int>>
    {
        private readonly IDriverPaymentRepositoryAsync _driverPaymentRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IPaymentTypeRepositoryAsync _paymentTypeRepository;
        private readonly IMapper _mapper;

        public UpdateDriverPaymentCommandHandler(IDriverPaymentRepositoryAsync driverPaymentRepository,
             IPaymentTypeRepositoryAsync paymentTypeRepository, IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _driverPaymentRepository = driverPaymentRepository;
            _applicationLogRepository = applicationLogRepository;
            _paymentTypeRepository = paymentTypeRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(UpdateDriverPaymentCommand request, CancellationToken cancellationToken)
        {
            var driverPayment = (await _driverPaymentRepository.FindByCondition(x => x.Id == request.Id && x.Driver.Id.Equals(request.DriverId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            if (driverPayment == null)
            {
                throw new ApiException($"Driver Payment Not Found.");
            }
            else
            {
                var paymentType = (await _paymentTypeRepository.FindByCondition(x => x.Id.Equals(request.TypeId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                driverPayment.Type = paymentType;
                driverPayment.Bank = request.Bank;
                driverPayment.AccountType = request.AccountType;
                driverPayment.Value = request.Value;
                driverPayment.Amount = request.Amount;
                driverPayment.Description = request.Description;
                driverPayment.Modified = DateTime.UtcNow;

                await _driverPaymentRepository.UpdateAsync(driverPayment);
                var log = new CreateAppLog(_applicationLogRepository);
                log.Create("Driver", "Driver Payment", "Edit", Newtonsoft.Json.JsonConvert.SerializeObject(request));
                return new Response<int>(driverPayment.Id);
            }
        }
    }
}
