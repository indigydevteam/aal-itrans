﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.DriverPayment;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.DriverPaymentes.Queries
{
    public class GetDriverPaymentByIdQuery : IRequest<Response<DriverPaymentViewModel>>
    {
        public Guid DriverId { get; set; }
        public int Id { get; set; }

    }
    public class GetDriverPaymentByIdQueryHandler : IRequestHandler<GetDriverPaymentByIdQuery, Response<DriverPaymentViewModel>>
    {
        private readonly IDriverPaymentRepositoryAsync _DriverPaymentRepository;
        private readonly IMapper _mapper;
        public GetDriverPaymentByIdQueryHandler(IDriverPaymentRepositoryAsync DriverPaymentRepository, IMapper mapper)
        {
            _DriverPaymentRepository = DriverPaymentRepository;
            _mapper = mapper;
        }
        public async Task<Response<DriverPaymentViewModel>> Handle(GetDriverPaymentByIdQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var DriverObject = (await _DriverPaymentRepository.FindByCondition(x => x.Id.Equals(request.Id) && x.Driver.Id.Equals(request.DriverId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                return new Response<DriverPaymentViewModel>(_mapper.Map<DriverPaymentViewModel>(DriverObject));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
