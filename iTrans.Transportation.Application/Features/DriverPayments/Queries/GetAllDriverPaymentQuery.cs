﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.DriverPayment;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.DriverPayments.Queries
{
    public class GetAllDriverPaymentQuery : IRequest<Response<IEnumerable<DriverPaymentViewModel>>>
    {

    }
    public class GetAllDriverPaymentQueryHandler : IRequestHandler<GetAllDriverPaymentQuery, Response<IEnumerable<DriverPaymentViewModel>>>
    {
        private readonly IDriverPaymentRepositoryAsync _DriverPaymentRepository;
        private readonly IMapper _mapper;
        public GetAllDriverPaymentQueryHandler(IDriverPaymentRepositoryAsync DriverPaymentRepository, IMapper mapper)
        {
            _DriverPaymentRepository = DriverPaymentRepository;
            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<DriverPaymentViewModel>>> Handle(GetAllDriverPaymentQuery request, CancellationToken cancellationToken)
        {
            var Driver = await _DriverPaymentRepository.GetAllAsync();
            var DriverPaymentViewModel = _mapper.Map<IEnumerable<DriverPaymentViewModel>>(Driver);
            return new Response<IEnumerable<DriverPaymentViewModel>>(DriverPaymentViewModel);
        }
    }
}
