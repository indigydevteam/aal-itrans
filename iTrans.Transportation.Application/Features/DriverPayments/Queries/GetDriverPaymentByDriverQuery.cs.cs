﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.DriverPayment;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.DriverPaymentes.Queries
{
    public class GetDriverPaymentByDriverQuery : IRequest<Response<IEnumerable<DriverPaymentViewModel>>>
    {
        public Guid DriverId { get; set; }
    }
    public class GetDriverPaymentByDriverQueryHandler : IRequestHandler<GetDriverPaymentByDriverQuery, Response<IEnumerable<DriverPaymentViewModel>>>
    {
        private readonly IDriverPaymentRepositoryAsync _DriverPaymentRepository;
        private readonly IMapper _mapper;
        public GetDriverPaymentByDriverQueryHandler(IDriverPaymentRepositoryAsync DriverPaymentRepository, IMapper mapper)
        {
            _DriverPaymentRepository = DriverPaymentRepository;
            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<DriverPaymentViewModel>>> Handle(GetDriverPaymentByDriverQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var DriverPayment = (await _DriverPaymentRepository.FindByCondition(x => x.Driver.Id.Equals(request.DriverId)).ConfigureAwait(false)).AsQueryable().ToList();
                var DriverPaymentViewModel = _mapper.Map<IEnumerable<DriverPaymentViewModel>>(DriverPayment);
                return new Response<IEnumerable<DriverPaymentViewModel>>(DriverPaymentViewModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
