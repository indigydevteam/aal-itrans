﻿using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.UserLevelConditions.Commands
{
   public class UpdateUserLevelConditionCommand : IRequest<Response<int>>
    {
        public virtual int Id { get; set; }
        public virtual int UserLevelId { get; set; }
        public virtual string Characteristics_TH { get; set; }
        public virtual string Characteristics_ENG { get; set; }
        public virtual int Sequence { get; set; }
        public virtual bool Active { get; set; }
    }
    public class UpdateUserLevelConditionCommandHandler : IRequestHandler<UpdateUserLevelConditionCommand, Response<int>>
    {
        private readonly IUserLevelConditionRepositoryAsync _userLevelConditionRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;

        public UpdateUserLevelConditionCommandHandler(IUserLevelConditionRepositoryAsync userLevelConditionRepository,IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _userLevelConditionRepository = userLevelConditionRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(UpdateUserLevelConditionCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var userLevelCondition = (await _userLevelConditionRepository.FindByCondition(x => x.Id == request.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (userLevelCondition == null)
                {
                    throw new ApiException($"UserLevelCondition Not Found.");
                }
                else
                {
                    //userLevelCondition.UserLevelId = request.UserLevelId;
                    userLevelCondition.Characteristics_TH = request.Characteristics_TH;
                    userLevelCondition.Characteristics_ENG = request.Characteristics_ENG;
                    userLevelCondition.Active = request.Active;
                    userLevelCondition.Modified = DateTime.UtcNow;
                    await _userLevelConditionRepository.UpdateAsync(userLevelCondition);
                    var log = new CreateAppLog(_applicationLogRepository);
                    log.Create("User", "User LevelCondition", "Edit", Newtonsoft.Json.JsonConvert.SerializeObject(userLevelCondition));
                    return new Response<int>(userLevelCondition.Id);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}


