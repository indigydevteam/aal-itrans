﻿using AutoMapper;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Features.UserLevels.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.UserLevelConditions.Commands
{
   public partial class CreateUserLevelConditionCommand : IRequest<Response<int>>
    {

        public virtual int UserLevelId { get; set; }
        public virtual string Characteristics_TH { get; set; }
        public virtual string Characteristics_ENG { get; set; }
        public virtual int Sequence { get; set; }
        public virtual bool Active { get; set; }
    }
    public class CreateUserLevelConditinCommandHandler : IRequestHandler<CreateUserLevelConditionCommand, Response<int>>
    {
        private readonly IUserLevelConditionRepositoryAsync _userLevelCondotionRepository;
        private readonly IUserLevelRepositoryAsync _userLevelRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;
        public CreateUserLevelConditinCommandHandler(IUserLevelRepositoryAsync userLevelRepository, IUserLevelConditionRepositoryAsync userLevelConditionRepository,IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _userLevelRepository = userLevelRepository;
            _userLevelCondotionRepository = userLevelConditionRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(CreateUserLevelConditionCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var userLevel = (await _userLevelRepository.FindByCondition(x => x.Id.Equals(request.UserLevelId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                var userLevelCondition = _mapper.Map<UserLevelCondition>(request);
                userLevelCondition.UserLevel = userLevel;
                userLevelCondition.Created = DateTime.UtcNow;
                userLevelCondition.Modified = DateTime.UtcNow;
                //title.CreatedBy = "xxxxxxx";
                //title.ModifiedBy = "xxxxxxx";
                var dataListObjectListObject = await _userLevelCondotionRepository.AddAsync(userLevelCondition);
                var log = new CreateAppLog(_applicationLogRepository);
                log.Create("User", "User LevelCondition", "Create", Newtonsoft.Json.JsonConvert.SerializeObject(userLevelCondition));
                return new Response<int>(dataListObjectListObject.Id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}