﻿using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.UserLevelConditions.Commands
{
    public class DeleteUserLevelConditionByIdCommand : IRequest<Response<int>>
    {
        public int Id { get; set; }
        public class DeleteUserLevelConditionByIdCommandHandler : IRequestHandler<DeleteUserLevelConditionByIdCommand, Response<int>>
        {
            private readonly IUserLevelConditionRepositoryAsync _userLevelConditonRepository;
            public DeleteUserLevelConditionByIdCommandHandler(IUserLevelConditionRepositoryAsync userLevelConditionRepository)
            {
                _userLevelConditonRepository = userLevelConditionRepository;
            }
            public async Task<Response<int>> Handle(DeleteUserLevelConditionByIdCommand command, CancellationToken cancellationToken)
            {
                var userLevelCondition = (await _userLevelConditonRepository.FindByCondition(x => x.Id == command.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (userLevelCondition == null)
                {
                    throw new ApiException($"UserLeveConditon Not Found.");
                }
                else
                {
                    await _userLevelConditonRepository.DeleteAsync(userLevelCondition);
                    return new Response<int>(userLevelCondition.Id);
                }
            }
        }
    }
}
