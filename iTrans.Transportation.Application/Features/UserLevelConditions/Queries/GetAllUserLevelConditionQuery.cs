﻿using AutoMapper;
using iTrans.Transportation.Application.DTOs;
using iTrans.Transportation.Application.DTOs.UserLevel;
using iTrans.Transportation.Application.DTOs.UserLevelCondition;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.UserLevelConditions.Queries
{
    public class GetAllUserLevelConditionQuery : IRequest<Response<IEnumerable<UserLevelConditionViewModel>>>
    {

    }
    public class GetAllUserLevelCondotionQueryHandler : IRequestHandler<GetAllUserLevelConditionQuery, Response<IEnumerable<UserLevelConditionViewModel>>>
    {
        private readonly IUserLevelConditionRepositoryAsync _userLevelConditionRepository;
        private readonly IMapper _mapper;
        public GetAllUserLevelCondotionQueryHandler(IUserLevelConditionRepositoryAsync userLevelRepository, IMapper mapper)
        {
            _userLevelConditionRepository = userLevelRepository;
            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<UserLevelConditionViewModel>>> Handle(GetAllUserLevelConditionQuery request, CancellationToken cancellationToken)
        {
            var userlevel = await _userLevelConditionRepository.GetAllAsync();
            var UserLevelConditionViewModel = _mapper.Map<IEnumerable<UserLevelConditionViewModel>>(userlevel);
            return new Response<IEnumerable<UserLevelConditionViewModel>>(UserLevelConditionViewModel);
        }
    }
}
