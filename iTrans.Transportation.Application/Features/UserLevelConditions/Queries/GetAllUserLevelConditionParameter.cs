﻿using AutoMapper;
using iTrans.Transportation.Application.DTOs;
using iTrans.Transportation.Application.DTOs.CarList;
using iTrans.Transportation.Application.DTOs.UserLevel;
using iTrans.Transportation.Application.DTOs.UserLevelCondition;
using iTrans.Transportation.Application.Features.CarLists.Queries;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using iTrans.Transportation.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.UserLevelConditions.Queries
{
  public  class GetAllUserLevelConditionParameter : IRequest<PagedResponse<IEnumerable<UserLevelConditionViewModel>>>
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
    }
    public class GetAllUserLevelParameterHandler : IRequestHandler<GetAllUserLevelConditionParameter, PagedResponse<IEnumerable<UserLevelConditionViewModel>>>
    {
        private readonly IUserLevelConditionRepositoryAsync _userLevelConditionRepository;
        private readonly IMapper _mapper;
        public GetAllUserLevelParameterHandler(IUserLevelConditionRepositoryAsync userLevelConditionRepository, IMapper mapper)
        {
            _userLevelConditionRepository = userLevelConditionRepository;
            _mapper = mapper;
        }

        public async Task<PagedResponse<IEnumerable<UserLevelConditionViewModel>>> Handle(GetAllUserLevelConditionParameter request, CancellationToken cancellationToken)
        {
            var validFilter = _mapper.Map<GetAllUserLevelConditionParameter>(request);
            Expression<Func<UserLevelCondition, bool>> expression = x => x.Characteristics_TH != "";
            var userLevelConditions = await _userLevelConditionRepository.FindByConditionWithPage(expression, validFilter.PageNumber, validFilter.PageSize);
            var UserLevelConditionViewModel = _mapper.Map<IEnumerable<UserLevelConditionViewModel>>(userLevelConditions);
            return new PagedResponse<IEnumerable<UserLevelConditionViewModel>>(UserLevelConditionViewModel, validFilter.PageNumber, validFilter.PageSize);
        }
    }
}
