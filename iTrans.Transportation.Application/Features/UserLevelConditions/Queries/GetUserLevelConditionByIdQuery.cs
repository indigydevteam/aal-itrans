﻿using AutoMapper;
using iTrans.Transportation.Application.DTOs;
using iTrans.Transportation.Application.DTOs.UserLevel;
using iTrans.Transportation.Application.DTOs.UserLevelCondition;
using iTrans.Transportation.Application.Features.CarLists.Queries;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;


namespace iTrans.Transportation.Application.Features.UserLevelConditions.Queries
{
   public class GetUserLevelConditionByIdQuery : IRequest<Response<UserLevelConditionViewModel>>
    {
        public int Id { get; set; }
    }
    public class GetUserLevelConditionByIdQueryHandler : IRequestHandler<GetUserLevelConditionByIdQuery, Response<UserLevelConditionViewModel>>
    {
        private readonly IUserLevelConditionRepositoryAsync _userlevelConditionRepository;
        private readonly IMapper _mapper;
        public GetUserLevelConditionByIdQueryHandler(IUserLevelConditionRepositoryAsync userLevelConditionRepository, IMapper mapper)
        {
            _userlevelConditionRepository = userLevelConditionRepository;
            _mapper = mapper;
        }
        public async Task<Response<UserLevelConditionViewModel>> Handle(GetUserLevelConditionByIdQuery request, CancellationToken cancellationToken)
        {
            var userLeveltConditionObject = (await _userlevelConditionRepository.FindByCondition(x => x.Id.Equals(request.Id)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            return new Response<UserLevelConditionViewModel>(_mapper.Map<UserLevelConditionViewModel>(userLeveltConditionObject));
        }
    }
}
