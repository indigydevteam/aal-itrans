﻿using AutoMapper;
using iTrans.Transportation.Application.DTOs;
using iTrans.Transportation.Application.DTOs.UserLevel;
using iTrans.Transportation.Application.DTOs.UserLevelCondition;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.UserLevelConditions.Queries
{
    public class GetUserLevelConditionByUserLevelQuery : IRequest<Response<IEnumerable<UserLevelConditionViewModel>>>
    {
        public int userLevelId { set; get; }
    }
    public class GetUserLevelConditionByUserLevelQueryHandler : IRequestHandler<GetUserLevelConditionByUserLevelQuery, Response<IEnumerable<UserLevelConditionViewModel>>>
    {
        private readonly IUserLevelConditionRepositoryAsync _userLevelConditionRepository;
        private readonly IMapper _mapper;
        public GetUserLevelConditionByUserLevelQueryHandler(IUserLevelConditionRepositoryAsync userLevelRepository, IMapper mapper)
        {
            _userLevelConditionRepository = userLevelRepository;
            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<UserLevelConditionViewModel>>> Handle(GetUserLevelConditionByUserLevelQuery request, CancellationToken cancellationToken)
        {
            var userlevel = (await _userLevelConditionRepository.FindByCondition(x => x.UserLevel.Id.Equals(request.userLevelId) && x.Active == true).ConfigureAwait(false)).AsQueryable().ToList();
            var UserLevelConditionViewModel = _mapper.Map<IEnumerable<UserLevelConditionViewModel>>(userlevel.OrderBy(x => x.Sequence));
            return new Response<IEnumerable<UserLevelConditionViewModel>>(UserLevelConditionViewModel);
        }
    }
}
