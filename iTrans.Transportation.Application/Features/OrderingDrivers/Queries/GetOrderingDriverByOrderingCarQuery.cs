﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.Ordering;
using iTrans.Transportation.Application.DTOs.OrderingDriver;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
namespace iTrans.Transportation.Application.Features.OrderingDrivers.Queries
{
   public class GetOrderingDriverByOrderingCarQuery : IRequest<Response<IEnumerable<OrderingDriverViewModel>>>
    {
        public int OrderingCarId { set; get; }
    }
    public class GetOrderingDriverByOrderingCarQueryHandler : IRequestHandler<GetOrderingDriverByOrderingCarQuery, Response<IEnumerable<OrderingDriverViewModel>>>
    {
        private readonly IOrderingDriverRepositoryAsync _orderingDriverRepository;
        private readonly IMapper _mapper;
        public GetOrderingDriverByOrderingCarQueryHandler(IOrderingDriverRepositoryAsync orderingDriverRepository, IMapper mapper)
        {
            _orderingDriverRepository = orderingDriverRepository;
            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<OrderingDriverViewModel>>> Handle(GetOrderingDriverByOrderingCarQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var OrderingDriver = (await _orderingDriverRepository.FindByCondition(x => x.Ordering.Id.Equals(request.OrderingCarId)).ConfigureAwait(false)).AsQueryable().ToList();
                var OrderingDriverViewModel = _mapper.Map<IEnumerable<OrderingDriverViewModel>>(OrderingDriver);
                return new Response<IEnumerable<OrderingDriverViewModel>>(OrderingDriverViewModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}