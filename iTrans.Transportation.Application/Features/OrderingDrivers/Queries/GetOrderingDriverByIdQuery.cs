﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.OrderingDriver;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.OrderingDrivers.Queries
{
   public class GetOrderingDriverByIdQuery : IRequest<Response<OrderingDriverViewModel>>
    {
        public int Id { get; set; }
    }
    public class GetOrderingDriverByIdQueryHandler : IRequestHandler<GetOrderingDriverByIdQuery, Response<OrderingDriverViewModel>>
    {
        private readonly IOrderingDriverRepositoryAsync _OrderingDriverRepository;
        private readonly IMapper _mapper;
        public GetOrderingDriverByIdQueryHandler(IOrderingDriverRepositoryAsync OrderingDriverRepository, IMapper mapper)
        {
            _OrderingDriverRepository = OrderingDriverRepository;
            _mapper = mapper;
        }
        public async Task<Response<OrderingDriverViewModel>> Handle(GetOrderingDriverByIdQuery request, CancellationToken cancellationToken)
        {
            var OrderingDriverObject = (await _OrderingDriverRepository.FindByCondition(x => x.Id.Equals(request.Id)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            return new Response<OrderingDriverViewModel>(_mapper.Map<OrderingDriverViewModel>(OrderingDriverObject));
        }
    }
}
