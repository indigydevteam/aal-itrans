﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.OrderingDriver;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;

namespace iTrans.Transportation.Application.Features.OrderingDrivers.Queries
{
  public  class GetAllOrderingDriverParameter : IRequest<PagedResponse<IEnumerable<OrderingDriverViewModel>>>
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
    }
    public class GetAllOrderingDriverParameterHandler : IRequestHandler<GetAllOrderingDriverParameter, PagedResponse<IEnumerable<OrderingDriverViewModel>>>
    {
        private readonly IOrderingDriverRepositoryAsync _OrderingDriverRepository;
        private readonly IMapper _mapper;
        public GetAllOrderingDriverParameterHandler(IOrderingDriverRepositoryAsync OrderingDriverRepository, IMapper mapper)
        {
            _OrderingDriverRepository = OrderingDriverRepository;
            _mapper = mapper;
        }

        public async Task<PagedResponse<IEnumerable<OrderingDriverViewModel>>> Handle(GetAllOrderingDriverParameter request, CancellationToken cancellationToken)
        {
            var validFilter = _mapper.Map<GetAllOrderingDriverParameter>(request);
            Expression<Func<OrderingDriver, bool>> expression = x => x.DriverType != "";
            var OrderingDriver = await _OrderingDriverRepository.FindByConditionWithPage(expression, validFilter.PageNumber, validFilter.PageSize);
            var OrderingDriverViewModel = _mapper.Map<IEnumerable<OrderingDriverViewModel>>(OrderingDriver);
            return new PagedResponse<IEnumerable<OrderingDriverViewModel>>(OrderingDriverViewModel, validFilter.PageNumber, validFilter.PageSize);
        }
    }
}