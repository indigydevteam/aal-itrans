﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.OrderingDriver;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
namespace iTrans.Transportation.Application.Features.OrderingDrivers.Queries
{
   public class GetAllOrderingDriverQuery : IRequest<Response<IEnumerable<OrderingDriverViewModel>>>
    {

    }
    public class GetAllOrderingDriverQueryHandler : IRequestHandler<GetAllOrderingDriverQuery, Response<IEnumerable<OrderingDriverViewModel>>>
    {
        private readonly IOrderingDriverRepositoryAsync _orderingDriverRepository;
        private readonly IMapper _mapper;
        public GetAllOrderingDriverQueryHandler(IOrderingDriverRepositoryAsync OrderingDriverRepository, IMapper mapper)
        {
            _orderingDriverRepository = OrderingDriverRepository;
            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<OrderingDriverViewModel>>> Handle(GetAllOrderingDriverQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var OrderingDriver = await _orderingDriverRepository.GetAllAsync();
                var OrderingDriverViewModel = _mapper.Map<IEnumerable<OrderingDriverViewModel>>(OrderingDriver);
                return new Response<IEnumerable<OrderingDriverViewModel>>(OrderingDriverViewModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
