﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;



namespace iTrans.Transportation.Application.Features.OrderingDrivers.Commands
{
  public  class UpdateOrderingDriverCommand : IRequest<Response<int>>
    {
        public  int Id { get; set; }
        public Guid OrderingId { get; set; }
        public string DriverType { get; set; }
        public int CorporateTypeId { get; set; }
        public int TitleId { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Name { get; set; }
        public string IdentityNumber { get; set; }
        public int ContactPersonTitleId { get; set; }
        public string ContactPersonFirstName { get; set; }
        public string ContactPersonMiddleName { get; set; }
        public string ContactPersonLastName { get; set; }
        public string PhoneCode { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }

    }

    public class UpdateOrderingDriverCommandHandler : IRequestHandler<UpdateOrderingDriverCommand, Response<int>>
    {
        private readonly IOrderingDriverRepositoryAsync _orderingDriverRepository;
        private readonly ITitleRepositoryAsync _titleRepository;
        private readonly ICorporateTypeRepositoryAsync _corporateTypeRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;

        public UpdateOrderingDriverCommandHandler(IOrderingDriverRepositoryAsync orderingDriverRepository, ITitleRepositoryAsync titleRepository
            , ICorporateTypeRepositoryAsync corporateTypeRepository,IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _orderingDriverRepository = orderingDriverRepository;
            _titleRepository = titleRepository;
            _corporateTypeRepository = corporateTypeRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(UpdateOrderingDriverCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var orderingDriver = (await _orderingDriverRepository.FindByCondition(x => x.Id == request.Id && x.Ordering.Id.Equals(request.OrderingId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (orderingDriver == null)
                {
                    throw new ApiException($"OrderingDriver Not Found.");
                }
                else
                {
                    CorporateType corporateType = (await _corporateTypeRepository.FindByCondition(x => x.Id.Equals(request.CorporateTypeId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                    Title title = (await _titleRepository.FindByCondition(x => x.Id.Equals(request.TitleId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                    Title contactPersonTitle = (await _titleRepository.FindByCondition(x => x.Id.Equals(request.ContactPersonTitleId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                    orderingDriver.DriverType = request.DriverType;
                    orderingDriver.CorporateType = corporateType;
                    orderingDriver.Title = title;
                    orderingDriver.FirstName = request.FirstName;
                    orderingDriver.MiddleName = request.MiddleName;
                    orderingDriver.LastName = request.LastName;
                    orderingDriver.Name = request.Name;
                    orderingDriver.IdentityNumber = request.IdentityNumber;
                    orderingDriver.ContactPersonTitle =  contactPersonTitle;
                    orderingDriver.ContactPersonFirstName = request.ContactPersonFirstName;
                    orderingDriver.ContactPersonMiddleName = request.ContactPersonMiddleName;
                    orderingDriver.ContactPersonLastName = request.ContactPersonLastName;
                    orderingDriver.PhoneCode = request.PhoneCode;
                    orderingDriver.PhoneNumber = request.PhoneNumber;
                    orderingDriver.Email = request.Email;
                    orderingDriver.Modified = DateTime.UtcNow;
          

                    await _orderingDriverRepository.UpdateAsync(orderingDriver);
                    var log = new CreateAppLog(_applicationLogRepository);
                    log.Create("Ordering", "Ordering Driver ", "Edit", Newtonsoft.Json.JsonConvert.SerializeObject(orderingDriver));
                    return new Response<int>(orderingDriver.Id);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}