﻿using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Interfaces.Services;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using iTrans.Transportation.Domain.Entities;
using MediatR;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.OrderingDrivers.Commands
{
  public partial  class CreateOrderingDriverCommand : IRequest<Response<int>>
    {
        public  Guid OrderingId { get; set; }
        public  string DriverType { get; set; }
        public  int CorporateTypeId { get; set; }
        public  int TitleId { get; set; }
        public  string FirstName { get; set; }
        public  string MiddleName { get; set; }
        public  string LastName { get; set; }
        public  string Name { get; set; }
        public  string IdentityNumber { get; set; }
        public  int ContactPersonTitleId { get; set; }
        public  string ContactPersonFirstName { get; set; }
        public  string ContactPersonMiddleName { get; set; }
        public  string ContactPersonLastName { get; set; }
        public  string PhoneCode { get; set; }
        public  string PhoneNumber { get; set; }
        public  string Email { get; set; }
    }
    public class CreateOrderingDriverCommandHandler : IRequestHandler<CreateOrderingDriverCommand, Response<int>>
    {
        private readonly IOrderingRepositoryAsync _orderingRepository;
        private readonly IOrderingDriverRepositoryAsync _orderingDriverRepository;
        private readonly ITitleRepositoryAsync _titleRepository;
        private readonly ICorporateTypeRepositoryAsync _corporateTypeRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;
        public CreateOrderingDriverCommandHandler(IOrderingRepositoryAsync orderingRepository, IOrderingDriverRepositoryAsync orderingDriverRepository, ITitleRepositoryAsync titleRepository
            , ICorporateTypeRepositoryAsync corporateTypeRepository,IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _orderingDriverRepository = orderingDriverRepository;
            _orderingRepository = orderingRepository;
            _titleRepository = titleRepository;
            _corporateTypeRepository = corporateTypeRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(CreateOrderingDriverCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var data = (await _orderingRepository.FindByCondition(x => x.Id == request.OrderingId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (data == null)
                {
                    throw new ApiException($"Ordering Not Found.");
                }
                CorporateType corporateType = (await _corporateTypeRepository.FindByCondition(x => x.Id.Equals(request.CorporateTypeId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                Title title = (await _titleRepository.FindByCondition(x => x.Id.Equals(request.TitleId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                Title contactPersonTitle = (await _titleRepository.FindByCondition(x => x.Id.Equals(request.ContactPersonTitleId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();

                var OrderingDriver = _mapper.Map<OrderingDriver>(request);
                OrderingDriver.Ordering = data;
                OrderingDriver.Title = title;
                OrderingDriver.ContactPersonTitle = contactPersonTitle;
                OrderingDriver.CorporateType = corporateType;
                OrderingDriver.Created = DateTime.UtcNow;
                OrderingDriver.Modified = DateTime.UtcNow;
                //title.CreatedBy = "xxxxxxx";
                //title.ModifiedBy = "xxxxxxx";
                var OrderingDriverObject = await _orderingDriverRepository.AddAsync(OrderingDriver);
                var log = new CreateAppLog(_applicationLogRepository);
                log.Create("Ordering", "Ordering Driver ", "Create", Newtonsoft.Json.JsonConvert.SerializeObject(OrderingDriver));
                return new Response<int>(OrderingDriverObject.Id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}