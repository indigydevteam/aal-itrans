﻿using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;


namespace iTrans.Transportation.Application.Features.OrderingDrivers.Commands
{
    class DeleteOrderingDriverCommand : IRequest<Response<int>>
    {
        public int Id { get; set; }
        public Guid OrderId { get; set; }
        public class DeleteOrderingDriverCommandHandler : IRequestHandler<DeleteOrderingDriverCommand, Response<int>>
        {
            private readonly IOrderingDriverRepositoryAsync _orderingDriverRepository;
            public DeleteOrderingDriverCommandHandler(IOrderingDriverRepositoryAsync orderingDriverRepository)
            {
                _orderingDriverRepository = orderingDriverRepository;
            }
            public async Task<Response<int>> Handle(DeleteOrderingDriverCommand command, CancellationToken cancellationToken)
            {
                var OrderingDriver = (await _orderingDriverRepository.FindByCondition(x => x.Id == command.Id && x.Ordering.Id.Equals(command.OrderId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (OrderingDriver == null)
                {
                    throw new ApiException($"OrderingDriver Not Found.");
                }
                else
                {
                    await _orderingDriverRepository.DeleteAsync(OrderingDriver);
                    return new Response<int>(OrderingDriver.Id);
                }
            }
        }
    }
}