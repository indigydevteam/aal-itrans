﻿using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.CustomerAddresses.Commands
{
    public class DeleteCustomerAddressByIdCommand : IRequest<Response<int>>
    {
        public Guid? UserId { get; set; }
        public int Id { get; set; }
        public Guid CustomerId { get; set; }
        public class DeleteCustomerAddressByIdCommandHandler : IRequestHandler<DeleteCustomerAddressByIdCommand, Response<int>>
        {
            private readonly ICustomerAddressRepositoryAsync _customerAddressRepository;
            private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
            public DeleteCustomerAddressByIdCommandHandler(ICustomerAddressRepositoryAsync customerAddressRepository, IApplicationLogRepositoryAsync applicationLogRepository)
            {
                _customerAddressRepository = customerAddressRepository;
                _applicationLogRepository = applicationLogRepository;
            }
            public async Task<Response<int>> Handle(DeleteCustomerAddressByIdCommand command, CancellationToken cancellationToken)
            {
                var customerAddress = (await _customerAddressRepository.FindByCondition(x => x.Id == command.Id && x.CustomerId == command.CustomerId  && (x.AddressType != "register" || x.AddressType != "register-tax")).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (customerAddress == null)
                {
                    throw new ApiException($"CustomerAddress Not Found.");
                }
                else
                {
                    await _customerAddressRepository.DeleteAsync(customerAddress);
                    if (customerAddress.IsMainData)
                    {
                        var address = (await _customerAddressRepository.FindByCondition(x => x.CustomerId == command.CustomerId && x.AddressType == customerAddress.AddressType).ConfigureAwait(false)).OrderByDescending(x => x.Created).AsQueryable().FirstOrDefault();
                        if (address != null)
                        {
                            address.IsMainData = true;
                            await _customerAddressRepository.UpdateAsync(address);
                        }
                    }
                    var log = new CreateAppLog(_applicationLogRepository);
                    log.CreateLog("Driver", "Driver Addresses", "Edit", Newtonsoft.Json.JsonConvert.SerializeObject(command), command.UserId != null ? command.UserId.ToString() : "");
                    return new Response<int>(customerAddress.Id);
                }
            }
        }
    }
}
