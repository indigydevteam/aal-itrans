﻿using AutoMapper;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Interfaces.Services;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.CustomerAddresses.Commands
{
    public partial class CreateCustomerAddressCommand : IRequest<Response<int>>
    {
        public Guid? UserId { get; set; }
        public Guid CustomerId { get; set; }
        public int CountryId { get; set; }
        public int ProvinceId { get; set; }
        public int DistrictId { get; set; }
        public int SubdistrictId { get; set; }
        public string PostCode { get; set; }
        public string Road { get; set; }
        public string Alley { get; set; }
        public string Address { get; set; }
        public string Branch { get; set; }
        public string AddressType { get; set; }
        public string AddressName { get; set; }
        public string ContactPerson { get; set; }
        public string ContactPhoneNumber { get; set; }
        public string ContactEmail { get; set; }
        public string Maps { get; set; }
        public bool IsMainData { get; set; }
        public int Sequence { get; set; }
        public string CountryCode { get; set; }

    }
    public class CreateCustomerAddressCommandHandler : IRequestHandler<CreateCustomerAddressCommand, Response<int>>
    {
        private readonly ICustomerAddressRepositoryAsync _customerAddressRepository;
        private readonly ICountryRepositoryAsync _countryRepository;
        private readonly IProvinceRepositoryAsync _provinceRepository;
        private readonly IDistrictRepositoryAsync _districtRepository;
        private readonly ISubdistrictRepositoryAsync _subdistrictRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;
        public CreateCustomerAddressCommandHandler(ICustomerAddressRepositoryAsync customerAddressRepository, ICountryRepositoryAsync countryRepository, IProvinceRepositoryAsync provinceRepository, IDistrictRepositoryAsync districtRepository, ISubdistrictRepositoryAsync subdistrictRepository, IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _customerAddressRepository = customerAddressRepository;
            _countryRepository = countryRepository;
            _provinceRepository = provinceRepository;
            _districtRepository = districtRepository;
            _subdistrictRepository = subdistrictRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(CreateCustomerAddressCommand request, CancellationToken cancellationToken)
        {
            var customerAddress = _mapper.Map<CustomerAddress>(request);
            Country country = (await _countryRepository.FindByCondition(x => x.Id.Equals(request.CountryId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            Province province = (await _provinceRepository.FindByCondition(x => x.Id.Equals(request.ProvinceId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            District district = (await _districtRepository.FindByCondition(x => x.Id.Equals(request.DistrictId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            Subdistrict subdistrict = (await _subdistrictRepository.FindByCondition(x => x.Id.Equals(request.SubdistrictId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();

            string phoneNumber = request.ContactPhoneNumber != null ? request.ContactPhoneNumber : "";
            if (request.ContactPhoneNumber != null && request.ContactPhoneNumber.Length == 10 && request.ContactPhoneNumber.First() == '0')
            {
                phoneNumber = phoneNumber.Remove(0, 1);
            }

            customerAddress.Country = country;
            customerAddress.Province = province;
            customerAddress.District = district;
            customerAddress.Subdistrict = subdistrict;
            customerAddress.Country = country;
            customerAddress.Province = province;
            customerAddress.District = district;
            customerAddress.Subdistrict = subdistrict;
            customerAddress.PostCode = request.PostCode;
            customerAddress.Road = request.Road;
            customerAddress.Alley = request.Alley;
            customerAddress.Address = request.Address;
            customerAddress.Branch = request.Branch;
            customerAddress.AddressType = request.AddressType;
            customerAddress.AddressName = request.AddressName;
            customerAddress.ContactPerson = request.ContactPerson;
            customerAddress.ContactPhoneCode = request.CountryCode;
            customerAddress.ContactPhoneNumber = phoneNumber;
            customerAddress.ContactEmail = request.ContactEmail;
            customerAddress.Maps = request.Maps;
            customerAddress.IsMainData = request.IsMainData;
            customerAddress.Sequence = request.Sequence;
            customerAddress.Created = DateTime.UtcNow;
            customerAddress.Modified = DateTime.UtcNow;
            customerAddress.CreatedBy = request.UserId != null ? request.UserId.ToString() : "";
            customerAddress.ModifiedBy = request.UserId != null ? request.UserId.ToString() : "";
            if (customerAddress.IsMainData)
            {
                (await _customerAddressRepository.CreateSQLQuery("UPDATE Customer_Address SET IsMainData  = 0 where CustomerId = '" + customerAddress.CustomerId.ToString() + "' and AddressType like '%" + customerAddress.AddressType.Trim() + "%' ").ConfigureAwait(false)).ExecuteUpdate();
            }
            var customerAddressObject = await _customerAddressRepository.AddAsync(customerAddress);
            var log = new CreateAppLog(_applicationLogRepository);
            log.CreateLog("Customer", "Customer Address", "Create", Newtonsoft.Json.JsonConvert.SerializeObject(request), request.UserId != null ? request.UserId.ToString() : "");
            return new Response<int>(customerAddressObject.Id);
        }
    }
}
