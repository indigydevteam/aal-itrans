﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.CustomerAddresses.Commands
{
    public class CreateCustomerAddressCommandValidator : AbstractValidator<CreateCustomerAddressCommand>
    {
        private readonly ICustomerAddressRepositoryAsync customerAddressRepository;
        private readonly ICustomerRepositoryAsync customerRepository;

        public CreateCustomerAddressCommandValidator(ICustomerAddressRepositoryAsync customerAddressRepository, ICustomerRepositoryAsync customerRepositor)
        {
            this.customerAddressRepository = customerAddressRepository;
            this.customerRepository = customerRepositor;

            RuleFor(p => p.CustomerId)
                .NotNull().WithMessage("{PropertyName} is required.")
                .MustAsync(IsExistCustomer).WithMessage("{PropertyName} no exists.");

            RuleFor(p => p.PostCode)
               .NotEmpty().WithMessage("{PropertyName} is required.")
               .NotNull()
               .MaximumLength(250).WithMessage("{PropertyName} must not exceed 250 characters.");
        }

        private async Task<bool> IsExistCustomer(Guid value, CancellationToken cancellationToken)
        {
            var customerObject = (await customerRepository.FindByCondition(x => x.Id.Equals(value)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            if (customerObject == null)
            {
                return false;
            }
            return true;
        }

        private async Task<bool> IsUnique(string value, CancellationToken cancellationToken)
        {
            var customerAddressObject = (await customerAddressRepository.FindByCondition(x => x.AddressName.ToLower() == value.ToLower()).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            if (customerAddressObject != null)
            {
                return false;
            }
            return true;
        }
    }
}
