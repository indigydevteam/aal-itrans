﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.CustomerAddresses.Commands
{
    public class UpdateCustomerAddressCommandValidator : AbstractValidator<UpdateCustomerAddressCommand>
    {
        private readonly ICustomerAddressRepositoryAsync customerAddressRepository;
        public UpdateCustomerAddressCommandValidator(ICustomerAddressRepositoryAsync customerAddressRepository)
        {
            this.customerAddressRepository = customerAddressRepository;

            RuleFor(p => p.CustomerId)
                .NotNull().WithMessage("{PropertyName} is required.");


            RuleFor(p => p.PostCode)
               .NotEmpty().WithMessage("{PropertyName} is required.")
               .NotNull()
               .MaximumLength(250).WithMessage("{PropertyName} must not exceed 250 characters.");
        }

        private async Task<bool> IsUnique(string value, CancellationToken cancellationToken)
        {
            var customerAddressObject = (await customerAddressRepository.FindByCondition(x => x.AddressName.ToLower() == value.ToLower()).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            if (customerAddressObject != null)
            {
                return false;
            }
            return true;
        }
    }
}
