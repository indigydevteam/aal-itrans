﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;

namespace iTrans.Transportation.Application.Features.CustomerAddresses.Commands
{
    public class UpdateCustomerAddressCommand : IRequest<Response<int>>
    {
        public Guid? UserId { get; set; }
        public int Id { get; set; }
        public Guid CustomerId { get; set; }
        public int CountryId { get; set; }
        public int ProvinceId { get; set; }
        public int DistrictId { get; set; }
        public int SubdistrictId { get; set; }
        public string PostCode { get; set; }
        public string Road { get; set; }
        public string Alley { get; set; }
        public string Address { get; set; }
        public string Branch { get; set; }
        public string AddressType { get; set; }
        public string AddressName { get; set; }
        public string ContactPerson { get; set; }
        public string ContactPhoneNumber { get; set; }
        public string ContactEmail { get; set; }
        public string Maps { get; set; }
        public bool IsMainData { get; set; }
        public int Sequence { get; set; }
    }

    public class UpdateCustomerAddressCommandHandler : IRequestHandler<UpdateCustomerAddressCommand, Response<int>>
    {
        private readonly ICustomerAddressRepositoryAsync _customerAddressRepository;
        private readonly ICountryRepositoryAsync _countryRepository;
        private readonly IProvinceRepositoryAsync _provinceRepository;
        private readonly IDistrictRepositoryAsync _districtRepository;
        private readonly ISubdistrictRepositoryAsync _subdistrictRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;

        private readonly IMapper _mapper;

        public UpdateCustomerAddressCommandHandler(ICustomerAddressRepositoryAsync customerAddressRepository, ICountryRepositoryAsync countryRepository, IProvinceRepositoryAsync provinceRepository, IDistrictRepositoryAsync districtRepository, ISubdistrictRepositoryAsync subdistrictRepository, IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _customerAddressRepository = customerAddressRepository;
            _countryRepository = countryRepository;
            _provinceRepository = provinceRepository;
            _districtRepository = districtRepository;
            _subdistrictRepository = subdistrictRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(UpdateCustomerAddressCommand request, CancellationToken cancellationToken)
        {
            var customerAddress = (await _customerAddressRepository.FindByCondition(x => x.Id == request.Id && x.CustomerId.Equals(request.CustomerId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            if (customerAddress == null)
            {
                throw new ApiException($"CustomerAddress Not Found.");
            }
            else
            {
                if (request.IsMainData)
                {
                    (await _customerAddressRepository.CreateSQLQuery("UPDATE Customer_Address SET IsMainData  = 0 where CustomerId = '" + request.CustomerId.ToString() + "' and AddressType like '%" + request.AddressType.Trim() + "%' ").ConfigureAwait(false)).ExecuteUpdate();
                }
                Country country = (await _countryRepository.FindByCondition(x => x.Id.Equals(request.CountryId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                Province province = (await _provinceRepository.FindByCondition(x => x.Id.Equals(request.ProvinceId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                District district = (await _districtRepository.FindByCondition(x => x.Id.Equals(request.DistrictId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                Subdistrict subdistrict = (await _subdistrictRepository.FindByCondition(x => x.Id.Equals(request.SubdistrictId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();

                customerAddress.CustomerId = request.CustomerId;
                customerAddress.Country = country;
                customerAddress.Province = province;
                customerAddress.District = district;
                customerAddress.Subdistrict = subdistrict;

                string phoneNumber = request.ContactPhoneNumber != null ? request.ContactPhoneNumber : "";
                if (request.ContactPhoneNumber != null && request.ContactPhoneNumber.Length == 10 && request.ContactPhoneNumber.First() == '0')
                {
                    phoneNumber = phoneNumber.Remove(0, 1);
                }

                if (request.PostCode != null)
                {
                    customerAddress.PostCode = request.PostCode;
                }
                if (request.Road != null)
                {
                    customerAddress.Road = request.Road;
                }
                if (request.Alley != null)
                {
                    customerAddress.Alley = request.Alley;
                }
                if (request.Address != null)
                {
                    customerAddress.Address = request.Address;
                }
                if (request.Branch != null)
                {
                    customerAddress.Branch = request.Branch;
                }
                if (request.AddressType != null)
                {
                    if (customerAddress.AddressType != null && customerAddress.AddressType.Contains("register"))
                    {

                    }
                    else
                        customerAddress.AddressType = request.AddressType;
                }
                if (request.AddressName != null)
                {
                    customerAddress.AddressName = request.AddressName;
                }
                if (request.ContactPerson != null)
                {
                    customerAddress.ContactPerson = request.ContactPerson;
                }
                customerAddress.ContactPhoneNumber = phoneNumber;

                if (request.ContactEmail != null)
                {
                    customerAddress.ContactEmail = request.ContactEmail;
                }
                if (request.Maps != null)
                {
                    customerAddress.Maps = request.Maps;
                }

                customerAddress.Sequence = request.Sequence;
                customerAddress.IsMainData = request.IsMainData;
                customerAddress.Modified = DateTime.UtcNow;
                customerAddress.ModifiedBy = request.UserId != null ? request.UserId.ToString() : "";
                await _customerAddressRepository.UpdateAsync(customerAddress);

                var log = new CreateAppLog(_applicationLogRepository);
                log.CreateLog("Customer", "Customer Address", "Edit", Newtonsoft.Json.JsonConvert.SerializeObject(request), request.UserId != null ? request.UserId.ToString() : "");

                return new Response<int>(customerAddress.Id);
            }
        }
    }
}
