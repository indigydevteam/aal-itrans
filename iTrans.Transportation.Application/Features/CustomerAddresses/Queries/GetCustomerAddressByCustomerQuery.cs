﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.CustomerAddress;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using LinqKit;
using MediatR;

namespace iTrans.Transportation.Application.Features.CustomerAddresses.Queries
{
    public class GetCustomerAddressByCustomerQuery : IRequest<Response<IEnumerable<CustomerAddressViewModel>>>
    {
        public Guid CustomerId { get; set; }
        public string Search { set; get; }
        public int PageNumber { set; get; }
        public int PageSize { set; get; }
    }
    public class GetCustomerAddressByCustomerQueryHandler : IRequestHandler<GetCustomerAddressByCustomerQuery, Response<IEnumerable<CustomerAddressViewModel>>>
    {
        private readonly ICustomerAddressRepositoryAsync _customerAddressRepository;
        private readonly IMapper _mapper;
        public GetCustomerAddressByCustomerQueryHandler(ICustomerAddressRepositoryAsync customerAddressRepository, IMapper mapper)
        {
            _customerAddressRepository = customerAddressRepository;
            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<CustomerAddressViewModel>>> Handle(GetCustomerAddressByCustomerQuery request, CancellationToken cancellationToken)
        {
            try
            {
                Expression<Func<Domain.CustomerAddress, bool>> expression = PredicateBuilder.New<Domain.CustomerAddress>(false);
                expression = x => x.CustomerId.Equals(request.CustomerId);
                if (request.Search != null && request.Search.Trim() != "")
                {
                    string search = request.Search.Trim();
                    expression = expression.And(a => (a.Country.Name_TH.Contains(search) || a.Country.Name_ENG.Contains(search)) ||
                                                     (a.Province.Name_TH.Contains(search) || a.Province.Name_ENG.Contains(search)) ||
                                                     (a.District.Name_TH.Contains(search) || a.District.Name_ENG.Contains(search)) ||
                                                     (a.Subdistrict.Name_TH.Contains(search) || a.Subdistrict.Name_ENG.Contains(search)) ||
                                                     a.PostCode.Contains(search) || a.Road.Contains(search) || a.Alley.Contains(search) ||
                                                     a.Address.Contains(search) || a.ContactPerson.Contains(search) || a.ContactPhoneNumber.Contains(search) ||
                                                     a.ContactEmail.Contains(search)
                                    );

                }
                
                var customerAddress = await _customerAddressRepository.FindByConditionWithPage(expression, request.PageNumber, request.PageSize);
                var customerAddressViewModel = _mapper.Map<IEnumerable<CustomerAddressViewModel>>(customerAddress);
                return new Response<IEnumerable<CustomerAddressViewModel>>(customerAddressViewModel);
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
    }
}
