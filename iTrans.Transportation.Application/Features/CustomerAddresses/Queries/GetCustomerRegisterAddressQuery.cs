﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.CustomerAddress;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.CustomerAddresses.Queries
{
    public class GetCustomerRegisterAddressQuery : IRequest<Response<CustomerAddressViewModel>>
    {
        public Guid CustomerId { get; set; }
    }
    public class GetCustomerRegisterAddressQueryHandler : IRequestHandler<GetCustomerRegisterAddressQuery, Response<CustomerAddressViewModel>>
    {
        private readonly ICustomerAddressRepositoryAsync _customerAddressRepository;
        private readonly IMapper _mapper;
        public GetCustomerRegisterAddressQueryHandler(ICustomerAddressRepositoryAsync customerAddressRepository, IMapper mapper)
        {
            _customerAddressRepository = customerAddressRepository;
            _mapper = mapper;
        }
        public async Task<Response<CustomerAddressViewModel>> Handle(GetCustomerRegisterAddressQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var customerObject = (await _customerAddressRepository.FindByCondition(x => x.CustomerId.Equals(request.CustomerId) && x.AddressType == "register").ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                return new Response<CustomerAddressViewModel>(_mapper.Map<CustomerAddressViewModel>(customerObject));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
