﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.CustomerAddress;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.CustomerAddresses.Queries
{
    public class GetCustomerAddressByIdQuery : IRequest<Response<CustomerAddressViewModel>>
    {
        public int Id { get; set; }
        public Guid CustomerId { get; set; }
    }
    public class GetCustomerAddressByIdQueryHandler : IRequestHandler<GetCustomerAddressByIdQuery, Response<CustomerAddressViewModel>>
    {
        private readonly ICustomerAddressRepositoryAsync _customerAddressRepository;
        private readonly IMapper _mapper;
        public GetCustomerAddressByIdQueryHandler(ICustomerAddressRepositoryAsync customerAddressRepository, IMapper mapper)
        {
            _customerAddressRepository = customerAddressRepository;
            _mapper = mapper;
        }
        public async Task<Response<CustomerAddressViewModel>> Handle(GetCustomerAddressByIdQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var customerObject = (await _customerAddressRepository.FindByCondition(x => x.Id.Equals(request.Id) && x.CustomerId.Equals(request.CustomerId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                return new Response<CustomerAddressViewModel>(_mapper.Map<CustomerAddressViewModel>(customerObject));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
