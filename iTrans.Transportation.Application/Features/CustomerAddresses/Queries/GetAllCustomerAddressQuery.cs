﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.CustomerAddress;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.CustomerAddresses.Queries
{
    public class GetAllCustomerAddressQuery : IRequest<Response<IEnumerable<CustomerAddressViewModel>>>
    {
        
    }
    public class GetAllCustomerAddressQueryHandler : IRequestHandler<GetAllCustomerAddressQuery, Response<IEnumerable<CustomerAddressViewModel>>>
    {
        private readonly ICustomerAddressRepositoryAsync _customerAddressRepository;
        private readonly IMapper _mapper;
        public GetAllCustomerAddressQueryHandler(ICustomerAddressRepositoryAsync customerAddressRepository, IMapper mapper)
        {
            _customerAddressRepository = customerAddressRepository;
            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<CustomerAddressViewModel>>> Handle(GetAllCustomerAddressQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var validFilter = _mapper.Map<GetAllCustomerAddressQuery>(request);
                var customer = await _customerAddressRepository.GetAllAsync();
                var customerAddressViewModel = _mapper.Map<IEnumerable<CustomerAddressViewModel>>(customer);
                return new Response<IEnumerable<CustomerAddressViewModel>>(customerAddressViewModel);
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
    }
}
