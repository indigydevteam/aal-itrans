﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using iTrans.Transportation.Application.Interfaces.Repositories;
using FluentValidation;

namespace iTrans.Transportation.Application.Features.CustomerAddresses.Queries
{
    public class GetCustomerAddressByIdQueryValidator : AbstractValidator<GetCustomerAddressByIdQuery>
    {
        private readonly ICustomerAddressRepositoryAsync customerAddressRepository;

        public GetCustomerAddressByIdQueryValidator(ICustomerAddressRepositoryAsync customerAddressRepository)
        {
            this.customerAddressRepository = customerAddressRepository;
            RuleFor(p => p.Id)
               .NotEmpty().WithMessage("{PropertyName} is required.")
               .NotNull()
               .MustAsync(IsCustomerAddressExists).WithMessage("{PropertyName} not exists.");
        }

        private async Task<bool> IsCustomerAddressExists(int CustomerId, CancellationToken cancellationToken)
        {
            var userObject = (await customerAddressRepository.FindByCondition(x => x.Id.Equals(CustomerId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            if (userObject != null)
            {
                return true;
            }
            return false;
        }
    }
}
