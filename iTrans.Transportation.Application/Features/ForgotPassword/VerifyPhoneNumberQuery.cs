﻿using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using iTrans.Transportation.Application.Helper;
using System.Net.Http;
using System.Net.Http.Headers;

namespace iTrans.Transportation.Application.Features.ForgotPassword
{
    public class VerifyPhoneNumberQuery : IRequest<Response<bool>>
    {
        public string Module { get; set; }
        public string IdentityNumber { get; set; }
        public string PhoneNumber { get; set; }
    }
    public class VerifyPhoneNumberQueryHandler : IRequestHandler<VerifyPhoneNumberQuery, Response<bool>>
    {
        private readonly ICustomerRepositoryAsync _customerRepository;
        private readonly IDriverRepositoryAsync _driverRepository;
        private readonly IMapper _mapper;

        public VerifyPhoneNumberQueryHandler(ICustomerRepositoryAsync customerdRepository, IDriverRepositoryAsync driverRepository, IMapper mapper)
        {
            _customerRepository = customerdRepository;
            _driverRepository = driverRepository;
            _mapper = mapper;
        }

        public async Task<Response<bool>> Handle(VerifyPhoneNumberQuery request, CancellationToken cancellationToken)
        {
            try
            {
                bool result = false;
                if (request.IdentityNumber == null || request.IdentityNumber == "" || request.PhoneNumber == null || request.PhoneNumber == "")
                {
                    return new Response<bool>(false);
                }
                if (request.Module != null && request.Module.Trim() != "")
                {
                    string identityNumber = AESMgr.Decrypt(request.IdentityNumber);

                    if (!result)
                    {
                        if (request.Module.Trim().ToLower() == "customer")
                        {
                            var customer = (await _customerRepository.FindByCondition(x => x.IdentityNumber == request.IdentityNumber && x.PhoneNumber == request.PhoneNumber).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                            if (customer != null)
                            {
                                result = true;
                                //using (var client = new HttpClient())
                                //{
                                //    client.BaseAddress = new Uri("https://itrans-test-auth.humantix.cloud");
                                //    client.DefaultRequestHeaders.Accept.Clear();
                                //    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                                //    var response = client.GetAsync("api/otp/requestotp?phone_no="+ request.PhoneNumber).Result;
                                //    if (response.IsSuccessStatusCode)
                                //    {
                                //        string responseString = response.Content.ReadAsStringAsync().Result;
                                         
                                //    }
                                //}
                            }
                        }
                        else if (request.Module.Trim().ToLower() == "driver")
                        {
                            var driver = (await _driverRepository.FindByCondition(x => x.IdentityNumber == request.IdentityNumber && x.PhoneNumber == request.PhoneNumber).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                            if (driver != null)
                            {
                                result = true;
                            }
                        }
                    }
                }
                return new Response<bool>(result);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
