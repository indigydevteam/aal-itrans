﻿using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using iTrans.Transportation.Application.Helper;

namespace iTrans.Transportation.Application.Features.ForgotPassword
{
    public class VerifyIdentityNumberQuery : IRequest<Response<bool>>
    {
        public string Module { get; set; }
        public string IdentityNumber { get; set; }
    }
    public class VerifyIdentityNumberQueryHandler : IRequestHandler<VerifyIdentityNumberQuery, Response<bool>>
    {
        private readonly ICustomerRepositoryAsync _customerRepository;
        private readonly IDriverRepositoryAsync _driverRepository;
        private readonly IMapper _mapper;

        public VerifyIdentityNumberQueryHandler(ICustomerRepositoryAsync customerdRepository, IDriverRepositoryAsync driverRepository, IMapper mapper)
        {
            _customerRepository = customerdRepository;
            _driverRepository = driverRepository;
            _mapper = mapper;
        }

        public async Task<Response<bool>> Handle(VerifyIdentityNumberQuery request, CancellationToken cancellationToken)
        {
            try
            {
                bool result = true;
                if (request.IdentityNumber == null || request.IdentityNumber == "")
                {
                    return new Response<bool>(false);
                }
                if (request.Module != null && request.Module.Trim() != "")
                {
                    string identityNumber = AESMgr.Decrypt(request.IdentityNumber);

                    Regex rexPersonal = new Regex(@"^[0-9]{13}$");
                    if (rexPersonal.IsMatch(identityNumber))
                    {
                        int sum = 0;

                        for (int i = 0; i < 12; i++)
                        {
                            sum += int.Parse(identityNumber[i].ToString()) * (13 - i);
                        }

                        result = (int.Parse(identityNumber[12].ToString()) == ((11 - (sum % 11)) % 10));
                    }

                    if (result)
                    {
                        if (request.Module.Trim().ToLower() == "customer")
                        {
                            var customer = (await _customerRepository.FindByCondition(x => x.IdentityNumber == request.IdentityNumber).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                            if (customer != null)
                            {
                                result = true;
                            }
                        }
                        else if (request.Module.Trim().ToLower() == "driver")
                        {
                            var driver = (await _driverRepository.FindByCondition(x => x.IdentityNumber == request.IdentityNumber).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                            if (driver != null)
                            {
                                result = true;
                            }
                        }
                    }
                }
                return new Response<bool>(result);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
