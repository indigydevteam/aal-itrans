﻿using AutoMapper;
using iTrans.Transportation.Application.DTOs.MenuPermission;
using iTrans.Transportation.Application.DTOs.Role;
using iTrans.Transportation.Application.DTOs.User;
using iTrans.Transportation.Application.DTOs.UserLevel;
using iTrans.Transportation.Application.DTOs.UserLevelCondition;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.CarLists.Queries;
using iTrans.Transportation.Application.Helper;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.ForgotPassword.Backoffice
{
   public class CheckPassword : IRequest<Response<string>>
    {
        public Guid? UserId { get; set; }
        public string Password { get; set; }



    }
    public class CkeckPasswordHandler : IRequestHandler<CheckPassword, Response<string>>
    {
        private readonly ISystemUserRepositoryAsync _userRepository;
        private readonly IRoleMenuRepositoryAsync _roleMenuRepository;
        private readonly IRoleRepositoryAsync _roleRepository;
        private readonly IMenuPermissionRepositoryAsync _menuPermissionRepository;
        private readonly IMenuRepositoryAsync _menuRepository;
        private readonly IMapper _mapper;
        public CkeckPasswordHandler(ISystemUserRepositoryAsync userLevelRepository, IRoleRepositoryAsync roleRepository, IMenuPermissionRepositoryAsync menuPermissionRepository, IMenuRepositoryAsync menuRepository, IRoleMenuRepositoryAsync roleMenuRepository, IMapper mapper)
        {
            _userRepository = userLevelRepository;
            _roleMenuRepository = roleMenuRepository;
            _roleRepository = roleRepository;
            _menuPermissionRepository = menuPermissionRepository;
            _menuRepository = menuRepository;
            _mapper = mapper;
        }
        public async Task<Response<string>> Handle(CheckPassword request, CancellationToken cancellationToken)
        {
            var result = "";
            var user = _mapper.Map<UserViewModel>((await _userRepository.FindByCondition(x => x.EmplID.ToString().Equals(request.UserId.ToString())).ConfigureAwait(false)).AsQueryable().FirstOrDefault());
            if (user == null)
            {
                throw new ApiException($"Password Not Found.");
            }
            else
            {
                user.Password = AESMgr.Decrypt(user.Password);
                if (user.Password == request.Password)
                {
                    result = "sucess";
                }

            }
            return new Response<string>(result);
        }
    }
}
