﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Helper;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;

namespace iTrans.Transportation.Application.Features.ForgotPassword
{
    public class SetPasswordCommand : IRequest<Response<Guid>>
    {
        public Guid? Id { get; set; }
        public string Type { get; set; }
        public string NewPassword { get; set; }
        public string ConfirmNewPassword { get; set; }
    }

    public class SetPasswordCommandHandler : IRequestHandler<SetPasswordCommand, Response<Guid>>
    {
        private readonly ICustomerRepositoryAsync _customerRepository;
        private readonly IDriverRepositoryAsync _driverRepository;
        private readonly ISystemUserRepositoryAsync _userRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;

        public SetPasswordCommandHandler(ICustomerRepositoryAsync CustomerdRepository, IDriverRepositoryAsync driverRepository, IApplicationLogRepositoryAsync applicationLogRepository, ISystemUserRepositoryAsync userRepository, IMapper mapper)
        {
            _customerRepository = CustomerdRepository;
            _driverRepository = driverRepository;
            _userRepository = userRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
        }

        public async Task<Response<Guid>> Handle(SetPasswordCommand request, CancellationToken cancellationToken)
        {
            try
            {
                if (request.NewPassword != request.ConfirmNewPassword)
                {
                    throw new ApiException($"Password Not same.");
                }
                if (request.Type == "customer")
                {
                    var customerPassWord = (await _customerRepository.FindByCondition(x => x.Id == request.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                    if (customerPassWord == null)
                    {
                        throw new ApiException($"Customer Not Found.");
                    }
                    else
                    {
                        customerPassWord.Password = request.NewPassword;
                        await _customerRepository.UpdateAsync(customerPassWord);
                        var log = new CreateAppLog(_applicationLogRepository);
                        log.Create("customer", "customer", "Edit", Newtonsoft.Json.JsonConvert.SerializeObject(request));
                        return new Response<Guid>(customerPassWord.Id);
                    }
                }
                else if (request.Type == "driver")
                {
                    var driverPassWord = (await _driverRepository.FindByCondition(x => x.Id == request.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                    if (driverPassWord == null)
                    {
                        throw new ApiException($"Customer Not Found.");
                    }
                    else
                    {
                        driverPassWord.Password = request.NewPassword;
                        await _driverRepository.UpdateAsync(driverPassWord);
                        var log = new CreateAppLog(_applicationLogRepository);
                        log.Create("customer", "customer", "Edit", Newtonsoft.Json.JsonConvert.SerializeObject(request));
                        return new Response<Guid>(driverPassWord.Id);
                    }
                }
                else if (request.Type == "admin")
                {
                    var userPassWord = (await _userRepository.FindByCondition(x => x.EmplID.ToString().Equals(request.Id.ToString())).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                    if (userPassWord == null)
                    {
                        throw new ApiException($"User Not Found.");
                    }
                    else
                    {
                        userPassWord.Password = AESMgr.Encrypt(request.NewPassword); ;
                        await _userRepository.UpdateAsync(userPassWord);
                        var log = new CreateAppLog(_applicationLogRepository);
                        log.Create("user", "usersetpassword", "Edit", Newtonsoft.Json.JsonConvert.SerializeObject(request));
                        return new Response<Guid>(new Guid(userPassWord.EmplID.ToString()));
                    }
                }
                throw new ApiException($"data Not Found.");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}