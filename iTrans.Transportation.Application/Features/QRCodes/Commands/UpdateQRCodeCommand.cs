﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;

namespace iTrans.Transportation.Application.Features.QRCodes.Commands
{
    public class UpdateQRCodeCommand : IRequest<Response<int>>
    {
        public int Id { get; set; }
        public string FileName { get; set; }
        public string ContentType { get; set; }
        public string FilePath { get; set; }
        public string FileEXT { get; set; }
        public string DocumentType { get; set; }
        public int Sequence { get; set; }
    }

    public class UpdateQRCodeCommandHandler : IRequestHandler<UpdateQRCodeCommand, Response<int>>
    {
        private readonly IQRCodeRepositoryAsync _qrCodeRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;

        public UpdateQRCodeCommandHandler(IQRCodeRepositoryAsync qrCodeRepository, IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _qrCodeRepository = qrCodeRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(UpdateQRCodeCommand request, CancellationToken cancellationToken)
        {
            var qrCode = (await _qrCodeRepository.FindByCondition(x => x.Id == request.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            if (qrCode == null)
            {
                throw new ApiException($"QRCode Not Found.");
            }
            else
            {
                qrCode.FileName = request.FileName;
                qrCode.ContentType = request.ContentType;
                qrCode.FilePath = request.FilePath;
                qrCode.FileEXT = request.FileEXT;
                qrCode.DocumentType = request.DocumentType;
                qrCode.Sequence = request.Sequence;
                qrCode.Modified = DateTime.UtcNow;
                await _qrCodeRepository.UpdateAsync(qrCode);
                var log = new CreateAppLog(_applicationLogRepository);
                log.Create("QRCode", "QRCode", "Edit", Newtonsoft.Json.JsonConvert.SerializeObject(qrCode));
                return new Response<int>(qrCode.Id);
            }
        }
    }
}
