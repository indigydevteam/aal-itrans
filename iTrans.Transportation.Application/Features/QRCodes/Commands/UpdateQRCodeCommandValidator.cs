﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;


namespace iTrans.Transportation.Application.Features.QRCodes.Commands
{
    public class UpdateQRCodeCommandValidator : AbstractValidator<UpdateQRCodeCommand>
    {
        private readonly IQRCodeRepositoryAsync qrCodeRepository;

        public UpdateQRCodeCommandValidator(IQRCodeRepositoryAsync qrCodeRepository)
        {
            this.qrCodeRepository = qrCodeRepository;

            RuleFor(p => p.FileName)
                .NotEmpty().WithMessage("{PropertyName} is required.")
                .NotNull();

            RuleFor(p => p.ContentType)
               .NotEmpty().WithMessage("{PropertyName} is required.")
               .NotNull();

            RuleFor(p => p.FilePath)
               .NotEmpty().WithMessage("{PropertyName} is required.")
               .NotNull();

            RuleFor(p => p.FileEXT)
               .NotEmpty().WithMessage("{PropertyName} is required.")
               .NotNull();

            RuleFor(p => p.DocumentType)
               .NotEmpty().WithMessage("{PropertyName} is required.")
               .NotNull();
        }

        private async Task<bool> IsUnique(string value, CancellationToken cancellationToken)
        {
            var qrCodeObject = (await qrCodeRepository.FindByCondition(x => x.FileName.ToLower() == value.ToLower()).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            if (qrCodeObject != null)
            {
                return false;
            }
            return true;
        }
    }
}
