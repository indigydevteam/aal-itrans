﻿using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
namespace iTrans.Transportation.Application.Features.QRCodes.Commands
{
    public class DeleteQRCodeByIdCommand : IRequest<Response<int>>
    {
        public int Id { get; set; }
        public class DeleteQRCodeByIdCommandHandler : IRequestHandler<DeleteQRCodeByIdCommand, Response<int>>
        {
            private readonly IQRCodeRepositoryAsync _qrCodeRepository;
            public DeleteQRCodeByIdCommandHandler(IQRCodeRepositoryAsync qrCodeRepository)
            {
                _qrCodeRepository = qrCodeRepository;
            }
            public async Task<Response<int>> Handle(DeleteQRCodeByIdCommand command, CancellationToken cancellationToken)
            {
                var qrCode = (await _qrCodeRepository.FindByCondition(x => x.Id == command.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (qrCode == null)
                {
                    throw new ApiException($"QRCode Not Found.");
                }
                else
                {
                    await _qrCodeRepository.DeleteAsync(qrCode);
                    return new Response<int>(qrCode.Id);
                }
            }
        }
    }
}
