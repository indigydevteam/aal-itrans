﻿using AutoMapper;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Interfaces.Services;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.QRCodes.Commands
{
    public partial class CreateQRCodeCommand : IRequest<Response<int>>
    {
        public int Id { get; set; }
        public string FileName { get; set; }
        public  string ContentType { get; set; }
        public  string FilePath { get; set; }
        public  string FileEXT { get; set; }
        public  string DocumentType { get; set; }
        public  int Sequence { get; set; }

    }
    public class CreateQRCodeCommandHandler : IRequestHandler<CreateQRCodeCommand, Response<int>>
    {
        private readonly IQRCodeRepositoryAsync _qrCodeRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;
        public CreateQRCodeCommandHandler(IQRCodeRepositoryAsync qrCodeRepository, IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _qrCodeRepository = qrCodeRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(CreateQRCodeCommand request, CancellationToken cancellationToken)
        {
            var qrCode = _mapper.Map<QRCode>(request);
            qrCode.Created = DateTime.UtcNow;
            qrCode.Modified = DateTime.UtcNow;
            var qrCodeObject = await _qrCodeRepository.AddAsync(qrCode);
            var log = new CreateAppLog(_applicationLogRepository);
            log.Create("QRCode", "QRCode", "Create", Newtonsoft.Json.JsonConvert.SerializeObject(qrCode));
            return new Response<int>(qrCodeObject.Id);
        }
    }
}
