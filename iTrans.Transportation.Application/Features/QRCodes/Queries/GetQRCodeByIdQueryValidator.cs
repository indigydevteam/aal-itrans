﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using iTrans.Transportation.Application.Interfaces.Repositories;
using FluentValidation;

namespace iTrans.Transportation.Application.Features.QRCodes.Queries
{
    public class GetQRCodeByIdQueryValidator : AbstractValidator<GetQRCodeByIdQuery>
    {
        private readonly IQRCodeRepositoryAsync qrCodeRepository;

        public GetQRCodeByIdQueryValidator(IQRCodeRepositoryAsync qrCodeRepository)
        {
            this.qrCodeRepository = qrCodeRepository;
            RuleFor(p => p.Id)
               .NotEmpty().WithMessage("{PropertyName} is required.")
               .NotNull()
               .MustAsync(IsQRCodeExists).WithMessage("{PropertyName} not exists.");
        }

        private async Task<bool> IsQRCodeExists(int Id, CancellationToken cancellationToken)
        {
            var userObject = (await qrCodeRepository.FindByCondition(x => x.Id.Equals(Id)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            if (userObject != null)
            {
                return true;
            }
            return false;
        }
    }
}
