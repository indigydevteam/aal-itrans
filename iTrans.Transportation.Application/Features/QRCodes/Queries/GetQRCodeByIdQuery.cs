﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.QRCode;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.QRCodes.Queries
{
   public class GetQRCodeByIdQuery : IRequest<Response<QRCodeViewModel>>
    {
        public int Id { get; set; }
    }
    public class GetQRCodeByIdQueryHandler : IRequestHandler<GetQRCodeByIdQuery, Response<QRCodeViewModel>>
    {
        private readonly IQRCodeRepositoryAsync _qrCodeRepository;
        private readonly IMapper _mapper;
        public GetQRCodeByIdQueryHandler(IQRCodeRepositoryAsync qrCodeRepository, IMapper mapper)
        {
            _qrCodeRepository = qrCodeRepository;
            _mapper = mapper;
        }
        public async Task<Response<QRCodeViewModel>> Handle(GetQRCodeByIdQuery request, CancellationToken cancellationToken)
        {
            var qrCodeObject = (await _qrCodeRepository.FindByCondition(x => x.Id.Equals(request.Id)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            return new Response<QRCodeViewModel>(_mapper.Map<QRCodeViewModel>(qrCodeObject));
        }
    }
}
