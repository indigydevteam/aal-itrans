﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.QRCode;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.QRCodes.Queries
{
   public class GetQRCodeQuery : IRequest<Response<QRCodeViewModel>>
    {
        public string Module { get; set; }
        public string Type { get; set; }
    }
    public class GetQRCodeQueryHandler : IRequestHandler<GetQRCodeQuery, Response<QRCodeViewModel>>
    {
        private readonly IQRCodeRepositoryAsync _qrCodeRepository;
        private readonly IMapper _mapper;
        public GetQRCodeQueryHandler(IQRCodeRepositoryAsync qrCodeRepository, IMapper mapper)
        {
            _qrCodeRepository = qrCodeRepository;
            _mapper = mapper;
        }
        public async Task<Response<QRCodeViewModel>> Handle(GetQRCodeQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var qrCodeObject = (await _qrCodeRepository.FindByCondition(x => x.Module == request.Module && x.DocumentType == request.Type).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                return new Response<QRCodeViewModel>(_mapper.Map<QRCodeViewModel>(qrCodeObject));
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
    }
}
