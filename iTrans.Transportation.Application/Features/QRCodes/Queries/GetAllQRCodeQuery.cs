﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.QRCode;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.QRCodes.Queries
{
    public class GetAllQRCodeQuery : IRequest<Response<IEnumerable<QRCodeViewModel>>>
    {
         
    }
    public class GetAllQRCodeQueryHandler : IRequestHandler<GetAllQRCodeQuery, Response<IEnumerable<QRCodeViewModel>>>
    {
        private readonly IQRCodeRepositoryAsync _qrCodeRepository;
        private readonly IMapper _mapper;
        public GetAllQRCodeQueryHandler(IQRCodeRepositoryAsync qrCodeRepository, IMapper mapper)
        {
            _qrCodeRepository = qrCodeRepository;
            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<QRCodeViewModel>>> Handle(GetAllQRCodeQuery request, CancellationToken cancellationToken)
        {
            var qrCode = await _qrCodeRepository.GetAllAsync();
            var qrCodeViewModel = _mapper.Map<IEnumerable<QRCodeViewModel>>(qrCode);
            return new Response<IEnumerable<QRCodeViewModel>>(qrCodeViewModel);
        }
    }
}
