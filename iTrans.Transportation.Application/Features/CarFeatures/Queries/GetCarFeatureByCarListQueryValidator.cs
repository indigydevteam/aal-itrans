﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using iTrans.Transportation.Application.Interfaces.Repositories;
using FluentValidation;

namespace iTrans.Transportation.Application.Features.CarFeatures.Queries
{
    public class GetCarFeatureByCarListQueryValidator : AbstractValidator<GetCarFeatureByCarListQuery>
    {
        private readonly ICarFeatureRepositoryAsync carFeatureRepository;

        public GetCarFeatureByCarListQueryValidator(ICarFeatureRepositoryAsync carFeatureRepository)
        {
            this.carFeatureRepository = carFeatureRepository;
            RuleFor(p => p.CarListId)
               .NotEmpty().WithMessage("{PropertyName} is required.")
               .NotNull()
               .MustAsync(IsCarListExists).WithMessage("{PropertyName} not exists.");
        }
         
        private async Task<bool> IsCarListExists(int CarListId, CancellationToken cancellationToken)
        {
            var userObject = (await carFeatureRepository.FindByCondition(x => x.CarList.Id.Equals(CarListId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            if (userObject != null)
            {
                return true;
            }
            return false;
        }
    }
}
