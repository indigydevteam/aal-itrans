﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.CarFeature;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;

namespace iTrans.Transportation.Application.Features.CarFeatures.Queries
{
    public class GetAllCarFeatureParameter : IRequest<PagedResponse<IEnumerable<CarFeatureViewModel>>>
    {
        public string Search { set; get; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
    }
    public class GetAllCarFeatureParameterHandler : IRequestHandler<GetAllCarFeatureParameter, PagedResponse<IEnumerable<CarFeatureViewModel>>>
    {
        private readonly ICarFeatureRepositoryAsync _carFeatureRepository;
        private readonly IMapper _mapper;
        private Expression<Func<CarFeature, bool>> expression;

        public GetAllCarFeatureParameterHandler(ICarFeatureRepositoryAsync carFeatureRepository, IMapper mapper)
        {
            _carFeatureRepository = carFeatureRepository;
            _mapper = mapper;
        }

        public async Task<PagedResponse<IEnumerable<CarFeatureViewModel>>> Handle(GetAllCarFeatureParameter request, CancellationToken cancellationToken)
        {
            var validFilter = _mapper.Map<GetAllCarFeatureParameter>(request);
            if (validFilter.Search != null && validFilter.Search.Trim() != "")
            {
                expression = x => (x.Name_TH.Contains(validFilter.Search.Trim()) || x.Name_ENG.Contains(validFilter.Search.Trim())
                                    || x.CarList.Name_TH.Contains(validFilter.Search.Trim()) || x.CarList.Name_ENG.Contains(validFilter.Search.Trim())
                                    || x.CarList.CarType.Name_TH.Contains(validFilter.Search.Trim()) || x.CarList.CarType.Name_ENG.Contains(validFilter.Search.Trim())
                                    || x.Sequence.ToString().Contains(validFilter.Search.Trim())
                                  );
            }
            int itemCount = _carFeatureRepository.GetItemCount(expression);
            var carFeature = await _carFeatureRepository.FindByConditionWithPage(expression, validFilter.PageNumber, validFilter.PageSize);

            var carFeatureViewModel = _mapper.Map<IEnumerable<CarFeatureViewModel>>(carFeature);
            return new PagedResponse<IEnumerable<CarFeatureViewModel>>(carFeatureViewModel, validFilter.PageNumber, validFilter.PageSize, itemCount);
        }
    }
}
