﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.CarFeature;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.CarFeatures.Queries
{
    public class GetAllCarFeatureQuery : IRequest<Response<IEnumerable<CarFeatureViewModel>>>
    {

    }
    public class GetAllCarFeatureQueryHandler : IRequestHandler<GetAllCarFeatureQuery, Response<IEnumerable<CarFeatureViewModel>>>
    {
        private readonly ICarFeatureRepositoryAsync _carFeatureRepository;
        private readonly IMapper _mapper;
        public GetAllCarFeatureQueryHandler(ICarFeatureRepositoryAsync carFeatureRepository, IMapper mapper)
        {
            _carFeatureRepository = carFeatureRepository;
            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<CarFeatureViewModel>>> Handle(GetAllCarFeatureQuery request, CancellationToken cancellationToken)
        {
            var carFeature = (await _carFeatureRepository.FindByCondition(x => x.Active == true).ConfigureAwait(false)).OrderBy(x => x.Sequence).AsQueryable().ToList();
            var carFeatureViewModel = _mapper.Map<IEnumerable<CarFeatureViewModel>>(carFeature);
            return new Response<IEnumerable<CarFeatureViewModel>>(carFeatureViewModel);
        }
    }
}
