﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using iTrans.Transportation.Application.Interfaces.Repositories;
using FluentValidation;

namespace iTrans.Transportation.Application.Features.CarFeatures.Queries
{
    public class GetCarFeatureByIdQueryValidator : AbstractValidator<GetCarFeatureByIdQuery>
    {
        private readonly ICarFeatureRepositoryAsync carFeatureRepository;

        public GetCarFeatureByIdQueryValidator(ICarFeatureRepositoryAsync carFeatureRepository)
        {
            this.carFeatureRepository = carFeatureRepository;
            RuleFor(p => p.Id)
               .NotEmpty().WithMessage("{PropertyName} is required.")
               .NotNull()
               .MustAsync(IsCarFeatureExists).WithMessage("{PropertyName} not exists.");
        }

        private async Task<bool> IsCarFeatureExists(int CarFeatureId, CancellationToken cancellationToken)
        {
            var userObject = (await carFeatureRepository.FindByCondition(x => x.Id.Equals(CarFeatureId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            if (userObject != null)
            {
                return true;
            }
            return false;
        }
    }
}
