﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.CarFeature.Backoffice;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.CarFeatures.Queries
{
    public class GetCarFeatureByIdQuery : IRequest<Response<CarFeatureViewModel>>
    {
        public int Id { get; set; }
    }
    public class GetCarFeatureByIdQueryHandler : IRequestHandler<GetCarFeatureByIdQuery, Response<CarFeatureViewModel>>
    {
        private readonly ICarFeatureRepositoryAsync _carFeatureRepository;
        private readonly IMapper _mapper;
        public GetCarFeatureByIdQueryHandler(ICarFeatureRepositoryAsync carFeatureRepository, IMapper mapper)
        {
            _carFeatureRepository = carFeatureRepository;
            _mapper = mapper;
        }
        public async Task<Response<CarFeatureViewModel>> Handle(GetCarFeatureByIdQuery request, CancellationToken cancellationToken)
        {
            var carFeatureObject = (await _carFeatureRepository.FindByCondition(x => x.Id.Equals(request.Id)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            return new Response<CarFeatureViewModel>(_mapper.Map<CarFeatureViewModel>(carFeatureObject));
        }
    }
}
