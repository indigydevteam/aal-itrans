﻿using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Interfaces.Services;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.CarFeatures.Commands
{
    public partial class CreateCarFeatureCommand : IRequest<Response<int>>
    {
        
        public Guid? UserId { get; set; }
        public int CarListId { get; set; }
        public string Name_TH { get; set; }
        public string Name_ENG { get; set; }
        public int Sequence { get; set; }
        public bool Active { get; set; }
        public bool Specified { get; set; }

    }
    public class CreateCarFeatureCommandHandler : IRequestHandler<CreateCarFeatureCommand, Response<int>>
    {
        private readonly ICarListRepositoryAsync _carListRepository;
        private readonly ICarFeatureRepositoryAsync _carFeatureRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;
        public CreateCarFeatureCommandHandler(ICarFeatureRepositoryAsync carFeatureRepository, ICarListRepositoryAsync carListRepository, IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _carListRepository = carListRepository;
            _carFeatureRepository = carFeatureRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(CreateCarFeatureCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var carList = (await _carListRepository.FindByCondition(x => x.Id == request.CarListId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (carList == null)
                {
                    throw new ApiException($"CarList Not Found.");
                }
                var carFeature = _mapper.Map<CarFeature>(request);
                carFeature.CarList = carList;
                carFeature.Created = DateTime.UtcNow;
                carFeature.Modified = DateTime.UtcNow;
                //title.CreatedBy = "xxxxxxx";
                //title.ModifiedBy = "xxxxxxx";
                var carFeatureObject = await _carFeatureRepository.AddAsync(carFeature);
                var log = new CreateAppLog(_applicationLogRepository);
                log.CreateLog("Car", "Car Feature", "Create", Newtonsoft.Json.JsonConvert.SerializeObject(carFeature),request.UserId.ToString());
                return new Response<int>(carFeatureObject.Id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
