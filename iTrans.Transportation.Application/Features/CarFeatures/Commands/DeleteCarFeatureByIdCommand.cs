﻿using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.CarFeatures.Commands
{
    public class DeleteCarFeatureByIdCommand : IRequest<Response<int>>
    {
        public int Id { get; set; }
        public class DeleteCarFeatureByIdCommandHandler : IRequestHandler<DeleteCarFeatureByIdCommand, Response<int>>
        {
            private readonly ICarFeatureRepositoryAsync _carFeatureRepository;
            public DeleteCarFeatureByIdCommandHandler(ICarFeatureRepositoryAsync carFeatureRepository)
            {
                _carFeatureRepository = carFeatureRepository;
            }
            public async Task<Response<int>> Handle(DeleteCarFeatureByIdCommand command, CancellationToken cancellationToken)
            {
                var carFeature = (await _carFeatureRepository.FindByCondition(x => x.Id == command.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (carFeature == null)
                {
                    throw new ApiException($"CarFeature Not Found.");
                }
                else
                {
                    await _carFeatureRepository.DeleteAsync(carFeature);
                    return new Response<int>(carFeature.Id);
                }
            }
        }
    }
}
