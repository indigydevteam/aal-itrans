﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;

namespace iTrans.Transportation.Application.Features.CarFeatures.Commands
{
    public class UpdateCarFeatureCommand : IRequest<Response<int>>
    {
        public Guid? UserId { get; set; }
        public int Id { get; set; }
        public int CarListId { get; set; }
        public string Name_TH { get; set; }
        public string Name_ENG { get; set; }
        public int Sequence { get; set; }
        public bool Active { get; set; }
        public bool Specified { get; set; }
    }

    public class UpdateCarFeatureCommandHandler : IRequestHandler<UpdateCarFeatureCommand, Response<int>>
    {
        private readonly ICarListRepositoryAsync _carListRepository;
        private readonly ICarFeatureRepositoryAsync _carFeatureRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;

        public UpdateCarFeatureCommandHandler(ICarFeatureRepositoryAsync carFeatureRepository, ICarListRepositoryAsync carListRepository, IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _carListRepository =  carListRepository;
            _carFeatureRepository = carFeatureRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(UpdateCarFeatureCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var carFeature = (await _carFeatureRepository.FindByCondition(x => x.Id == request.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (carFeature == null)
                {
                    throw new ApiException($"CarFeature Not Found.");
                }
                else
                {
                    var carList = (await _carListRepository.FindByCondition(x => x.Id == request.CarListId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                    if (carFeature == null)
                    {
                        throw new ApiException($"CarList Not Found.");
                    }
                    carFeature.Name_TH = request.Name_TH;
                    carFeature.Name_ENG = request.Name_ENG;
                    carFeature.CarList = carList;
                    carFeature.Sequence = request.Sequence;
                    carFeature.Active = request.Active;
                    carFeature.Specified = request.Specified;

                    await _carFeatureRepository.UpdateAsync(carFeature);
                    var log = new CreateAppLog(_applicationLogRepository);
                    log.CreateLog("Car", "Car Feature", "Edit", Newtonsoft.Json.JsonConvert.SerializeObject(carFeature),request.UserId.ToString());
                    return new Response<int>(carFeature.Id);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
