﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;

namespace iTrans.Transportation.Application.Features.CustomerProblems.Commands
{
   public class UpdateCustomerProblemCommand : IRequest<Response<int>>
    {
        public Guid? UserId { get; set; }
        public int Id { get; set; }
        public int ProblemTopicId { get; set; }
        public string Message { get; set; }
        public string Email { get; set; }

    }
    public class UpdateCustomerProblemCommandsHandler : IRequestHandler<UpdateCustomerProblemCommand, Response<int>>
    {
        private readonly ICustomerProblemRepositoryAsync _customerProblemRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;

        public UpdateCustomerProblemCommandsHandler(ICustomerProblemRepositoryAsync customerProblemRepository, IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _customerProblemRepository = customerProblemRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(UpdateCustomerProblemCommand request, CancellationToken cancellationToken)
        {
            var customerProblem = (await _customerProblemRepository.FindByCondition(x => x.Id == request.Id && x.ProblemTopic.Id.Equals(request.ProblemTopicId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            if (customerProblem == null)
            {
                throw new ApiException($"Problem Not Found.");
            }
            else
            {
                //customerProblem.CustomerId = request.CustomerId;
                //customerProblem.ProblemTopicId = request.ProblemTopicId;
                customerProblem.Message = request.Message;
                customerProblem.Email = request.Email;
                customerProblem.Modified = DateTime.UtcNow;
                await _customerProblemRepository.UpdateAsync(customerProblem);
                var log = new CreateAppLog(_applicationLogRepository);
                log.CreateLog("Customer", "Customer Problem", "Edit", Newtonsoft.Json.JsonConvert.SerializeObject(request),request.UserId.ToString());
                return new Response<int>(customerProblem.Id);
            }
        }
    }
}