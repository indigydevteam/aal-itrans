﻿using iTrans.Transportation.Application.Exceptions;
using System;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using AutoMapper;
using iTrans.Transportation.Domain.Entities;

namespace iTrans.Transportation.Application.Features.CustomerProblems.Commands
{
   public partial class CreateCustomerProblemCommand : IRequest<Response<int>>
    {
        public  Guid? UserId { get; set; }
        public  int ProblemTopicId { get; set; }
        public Guid CustomerId { get; set; }
        public string Message { get; set; }
        public  string Email { get; set; }
        public List<IFormFile> Files { get; set; }

    }
    public class CreateCustomerProblemCommandHandler : IRequestHandler<CreateCustomerProblemCommand, Response<int>>
    {
        private readonly ICustomerRepositoryAsync _customerRepository;
        private readonly IProblemTopicRepositoryAsync _problemTopicRepository;
        private readonly ICustomerProblemRepositoryAsync _customerProblemRepository;
        private readonly ICustomerProblemFileRepositoryAsync _customerProblemFileRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IConfiguration _configuration;

        private readonly IMapper _mapper;
        public CreateCustomerProblemCommandHandler(ICustomerRepositoryAsync customerRepository, IProblemTopicRepositoryAsync problemTopicRepository,
            ICustomerProblemRepositoryAsync customerProblemRepository, ICustomerProblemFileRepositoryAsync customerProblemFileRepository ,IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper, IConfiguration configuration)
        {
            _customerRepository = customerRepository;
            _customerProblemRepository = customerProblemRepository;
            _customerProblemFileRepository = customerProblemFileRepository;
            _problemTopicRepository = problemTopicRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
            _configuration = configuration;

        }

        public async Task<Response<int>> Handle(CreateCustomerProblemCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var customerObject = (await _customerRepository.FindByCondition(x => x.Id.Equals(request.CustomerId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (customerObject == null)
                {
                    throw new ApiException($"Customer Not Found.");
                }
                var problemTopic = (await _problemTopicRepository.FindByCondition(x => x.Id.Equals(request.ProblemTopicId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                CustomerProblem problem = new CustomerProblem
                {
                    Customer = customerObject,
                    ProblemTopic = problemTopic,
                    Message = request.Message,
                    Email = request.Email,
                    Files = new List<CustomerProblemFile>()
                };

                if (request.Files != null)
                {
                    string currentTimeStr = DateTime.Now.ToString("yyyyMMddHHmmss");
                    var getContentPath = _configuration.GetSection("ContentPath");
                    string contentPath = getContentPath.Value;

                    int fileCount = 0;

                    string folderPath = contentPath + "customer/problem/" + customerObject.Id.ToString() + "/" + currentTimeStr;
                    if (!Directory.Exists(folderPath))
                    {
                        Directory.CreateDirectory(folderPath);
                    }
                    foreach (IFormFile file in request.Files)
                    {
                        fileCount = fileCount + 1;
                        string filePath = Path.Combine(folderPath, file.FileName);
                        using (FileStream fileStream = new FileStream(filePath, FileMode.Create))
                        {
                            await file.CopyToAsync(fileStream);
                            FileInfo fi = new FileInfo(filePath);

                            CustomerProblemFile problemFile = new CustomerProblemFile
                            {
                                CustomerProblem = problem,
                                FileName = file.FileName,
                                ContentType = file.ContentType,
                                FilePath = "customer/problem/" + customerObject.Id.ToString() + "/" + currentTimeStr + "/" + file.FileName,
                                DirectoryPath = "customer/problem/" + customerObject.Id.ToString() + "/" + currentTimeStr + "/",
                                FileEXT = fi.Extension,
                                Sequence = fileCount,
                            };
                            problem.Files.Add(problemFile);
                        }
                    }
                }

                problem.Created = DateTime.UtcNow;
                problem.Modified = DateTime.UtcNow;

                var problemObject = await _customerProblemRepository.AddAsync(problem);
                var log = new CreateAppLog(_applicationLogRepository);
                log.CreateLog("Customer", "Customer Problem", "Create", Newtonsoft.Json.JsonConvert.SerializeObject(request),request.UserId.ToString());
                return new Response<int>(problemObject.Id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    
    }
}

    

