﻿using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.CustomerProblems.Commands
{
    public class DeleteCustomerProblemCommand : IRequest<Response<int>>
    {
        public int ProblemTopicId { get; set; }
        public int Id { get; set; }
        public class DeleteCustomerProblemCommandHandler : IRequestHandler<DeleteCustomerProblemCommand, Response<int>>
        {
            private readonly ICustomerProblemRepositoryAsync _customerProblemRepository;
            public DeleteCustomerProblemCommandHandler(ICustomerProblemRepositoryAsync customerProblemRepository)
            {
                _customerProblemRepository = customerProblemRepository;
            }
            public async Task<Response<int>> Handle(DeleteCustomerProblemCommand command, CancellationToken cancellationToken)
            {
                var customerProblem = (await _customerProblemRepository.FindByCondition(x => x.Id == command.Id && x.ProblemTopic.Id.Equals(command.ProblemTopicId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (customerProblem == null)
                {
                    throw new ApiException($"Problem Not Found.");
                }
                else
                {
                    await _customerProblemRepository.DeleteAsync(customerProblem);
                    return new Response<int>(customerProblem.Id);
                }
            }
        }
    }
}
