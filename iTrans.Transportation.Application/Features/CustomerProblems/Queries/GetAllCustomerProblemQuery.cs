﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.CustomerProblem;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.CustomerProblems.Queries
{
    public class GetAllCustomerProblemQuery : IRequest<Response<IEnumerable<CustomerProblemViewModel>>>
    {

    }
    public class GetAllCustomerProblemQueryHandler : IRequestHandler<GetAllCustomerProblemQuery, Response<IEnumerable<CustomerProblemViewModel>>>
    {
        private readonly ICustomerProblemRepositoryAsync _customerProblemRepository;
        private readonly IMapper _mapper;
        public GetAllCustomerProblemQueryHandler(ICustomerProblemRepositoryAsync customerProblemRepository, IMapper mapper)
        {
            _customerProblemRepository = customerProblemRepository;
            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<CustomerProblemViewModel>>> Handle(GetAllCustomerProblemQuery request, CancellationToken cancellationToken)
        {
            var customer = await _customerProblemRepository.GetAllAsync();
            var customerProblemViewModel = _mapper.Map<IEnumerable<CustomerProblemViewModel>>(customer);
            return new Response<IEnumerable<CustomerProblemViewModel>>(customerProblemViewModel);
        }
    }
}
