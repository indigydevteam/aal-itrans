﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.CustomerProblem;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using iTrans.Transportation.Domain.Entities;
using MediatR;

namespace iTrans.Transportation.Application.Features.CustomerProblems.Queries
{
    public class GetAllCustomerProblemParameter : IRequest<PagedResponse<IEnumerable<CustomerProblemViewModel>>>
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public string Search { set; get; }

    }
    public class GetAllCustomerProblemParameterHandler : IRequestHandler<GetAllCustomerProblemParameter, PagedResponse<IEnumerable<CustomerProblemViewModel>>>
    {
        private readonly ICustomerProblemRepositoryAsync _customerProblemRepository;
        private readonly IMapper _mapper;
        private Expression<Func<CustomerProblem, bool>> expression;

        public GetAllCustomerProblemParameterHandler(ICustomerProblemRepositoryAsync customerProblemRepository, IMapper mapper)
        {
            _customerProblemRepository = customerProblemRepository;
            _mapper = mapper;
        }

        public async Task<PagedResponse<IEnumerable<CustomerProblemViewModel>>> Handle(GetAllCustomerProblemParameter request, CancellationToken cancellationToken)
        {
            var validFilter = _mapper.Map<GetAllCustomerProblemParameter>(request);
            if (validFilter.Search != null && validFilter.Search.Trim() != "")
            {
                expression = x => (x.Message.Contains(validFilter.Search.Trim()) || x.Email.Contains(validFilter.Search.Trim()));
            }
            int itemCount = _customerProblemRepository.GetItemCount(expression);

            var customerProblem = await _customerProblemRepository.FindByConditionWithPage(expression, validFilter.PageNumber, validFilter.PageSize);
            var customerProblemViewModel = _mapper.Map<IEnumerable<CustomerProblemViewModel>>(customerProblem);
            return new PagedResponse<IEnumerable<CustomerProblemViewModel>>(customerProblemViewModel, validFilter.PageNumber, validFilter.PageSize, itemCount);
        }
    }
}