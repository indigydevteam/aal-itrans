﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.CustomerProblem;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.CustomerProblemes.Queries
{
    public class GetCustomerProblemByIdQuery : IRequest<Response<CustomerProblemViewModel>>
    {
        public int ProblemTopicId { get; set; }
        public int Id { get; set; }

    }
    public class GetCustomerProblemByIdQueryHandler : IRequestHandler<GetCustomerProblemByIdQuery, Response<CustomerProblemViewModel>>
    {
        private readonly ICustomerProblemRepositoryAsync _customerProblemRepository;
        private readonly IMapper _mapper;
        public GetCustomerProblemByIdQueryHandler(ICustomerProblemRepositoryAsync customerProblemRepository, IMapper mapper)
        {
            _customerProblemRepository = customerProblemRepository;
            _mapper = mapper;
        }
        public async Task<Response<CustomerProblemViewModel>> Handle(GetCustomerProblemByIdQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var customerObject = (await _customerProblemRepository.FindByCondition(x => x.Id.Equals(request.Id) && x.ProblemTopic.Id.Equals(request.ProblemTopicId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                return new Response<CustomerProblemViewModel>(_mapper.Map<CustomerProblemViewModel>(customerObject));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
