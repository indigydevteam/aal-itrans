﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.CustomerProblem;
using iTrans.Transportation.Application.DTOs.DriverProblem;
using iTrans.Transportation.Application.DTOs.ProblemTopic;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using iTrans.Transportation.Domain.Entities;
using MediatR;

namespace iTrans.Transportation.Application.Features.CustomerProblems.Backoffice
{
    public class GetAllCustomerProblem : IRequest<PagedResponse<IEnumerable<CustomerProblemViewModel>>>
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public string Search { set; get; }
        public int Active { set; get; }
        public string Column { set; get; }
    }
    public class GetAllCustomerProblemHandler : IRequestHandler<GetAllCustomerProblem, PagedResponse<IEnumerable<CustomerProblemViewModel>>>
    {
        private readonly ICustomerProblemRepositoryAsync _customerProblemRepository;
        private readonly IDriverProblemRepositoryAsync _driverProblemRepository;
        private readonly IMapper _mapper;
        private Expression<Func<CustomerProblem, bool>> expression;

        public GetAllCustomerProblemHandler(ICustomerProblemRepositoryAsync customerProblemRepository, IDriverProblemRepositoryAsync driverProblemRepository, IMapper mapper)
        {
            _customerProblemRepository = customerProblemRepository;
            _driverProblemRepository = driverProblemRepository;
            _mapper = mapper;
        }

        public async Task<PagedResponse<IEnumerable<CustomerProblemViewModel>>> Handle(GetAllCustomerProblem request, CancellationToken cancellationToken)
        {
            var validFilter = _mapper.Map<GetAllCustomerProblem>(request);
         
            var customer = _mapper.Map<List<CustomerProblem>>((await _customerProblemRepository.GetAllAsync().ConfigureAwait(false)).AsQueryable().ToList());
            var driver = _mapper.Map<List<DriverProblem>>((await _driverProblemRepository.GetAllAsync().ConfigureAwait(false)).AsQueryable().ToList());

            var result = (from x in customer select new CustomerProblemViewModel 
                        {  
                            Id = x.Id,
                            Email = x.Email != null ? x.Email:"-",
                            Module ="customer",
                            Message=x.Message != null ? x.Message : "-",
                            ProblemTopic = _mapper.Map<ProblemTopicViewModel>(x.ProblemTopic),
                            Created = x.Created.ToString("yyyy/MM/dd HH:MM") != null ? x.Created.ToString("yyyy/MM/dd HH:MM") : "-",
                            Modified = x.Modified.ToString() != null ? x.Modified.ToString() : "-",
                        }).Concat(from d in driver select new CustomerProblemViewModel { 
                            Id = d.Id, 
                            Email = d.Email != null ? d.Email : "-",
                            Module = "driver", 
                            Message = d.Message != null ? d.Message : "-",
                            ProblemTopic = _mapper.Map<ProblemTopicViewModel>(d.ProblemTopic), 
                            Created = d.Created.ToString("yyyy/MM/dd HH:MM") != null ? d.Created.ToString("yyyy/MM/dd HH:MM") : "-",
                            Modified = d.Modified.ToString() != null ? d.Modified.ToString() : "-",
                        });

            if (validFilter.Search != null && validFilter.Search.Trim() != "")
            {
                result = result.Where(l => l.Email.Contains(validFilter.Search.Trim()) || l.Module.Contains(validFilter.Search.Trim())
                        || l.Message.Contains(validFilter.Search.Trim()) || l.Created.Contains(validFilter.Search.Trim())
                        || l.Modified.Contains(validFilter.Search.Trim()) || l.Module.Contains(validFilter.Search)
                        || l.Message.Contains(validFilter.Search) || l.Created.Contains(validFilter.Search)
                        || l.Modified.Contains(validFilter.Search) || l.Email.Contains(validFilter.Search)).ToList();
            }
            if (request.Column == "email" && request.Active == 3)
            {
                result = result.OrderBy(x => x.Email);
            }
            if (request.Column == "email" && request.Active == 2)
            {
                result = result.OrderByDescending(x => x.Email);
            }
            if (request.Column == "module" && request.Active == 3)
            {
                result = result.OrderBy(x => x.Module);
            }
            if (request.Column == "module" && request.Active == 2)
            {
                result = result.OrderByDescending(x => x.Module);
            }
            if (request.Column == "message" && request.Active == 3)
            {
                result = result.OrderBy(x => x.Message);
            }
            if (request.Column == "message" && request.Active == 2)
            {
                result = result.OrderByDescending(x => x.Message);
            }
            if (request.Column == "created" && request.Active == 3)
            {
                result = result.OrderBy(x => x.Created);
            }
            if (request.Column == "created" && request.Active == 2)
            {
                result = result.OrderByDescending(x => x.Created);
            }
         
            var itemCount = result.Count();
            result = result.Skip((validFilter.PageNumber - 1) * validFilter.PageSize).Take(validFilter.PageSize);



            return new PagedResponse<IEnumerable<CustomerProblemViewModel>>(result, validFilter.PageNumber, validFilter.PageSize, itemCount);
        }
    }
}