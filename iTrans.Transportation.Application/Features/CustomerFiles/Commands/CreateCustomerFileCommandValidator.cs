﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
namespace iTrans.Transportation.Application.Features.CustomerFiles.Commands
{
    public class CreateCustomerFileCommandValidator : AbstractValidator<CreateCustomerFileCommand>
    {
        private readonly ICustomerFileRepositoryAsync customerFileRepository;

        public CreateCustomerFileCommandValidator(ICustomerFileRepositoryAsync customerFileRepository)
        {
            this.customerFileRepository = customerFileRepository;

            RuleFor(p => p.CustomerId)
                .NotNull().WithMessage("{PropertyName} is required.");

            RuleFor(p => p.FileName)
                .NotEmpty().WithMessage("{PropertyName} is required.")
                .NotNull()
                .MustAsync(IsUnique).WithMessage("{PropertyName} already exists.");

            RuleFor(p => p.ContentType)
               .NotEmpty().WithMessage("{PropertyName} is required.")
               .NotNull();

            RuleFor(p => p.FilePath)
               .NotEmpty().WithMessage("{PropertyName} is required.")
               .NotNull();

            RuleFor(p => p.FileEXT)
               .NotEmpty().WithMessage("{PropertyName} is required.")
               .NotNull();

            RuleFor(p => p.DocumentType)
               .NotEmpty().WithMessage("{PropertyName} is required.")
               .NotNull();
        }

        private async Task<bool> IsUnique(string value, CancellationToken cancellationToken)
        {
            var customerFileObject = (await customerFileRepository.FindByCondition(x => x.FileName.ToLower() == value.ToLower()).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            if (customerFileObject != null)
            {
                return false;
            }
            return true;
        }
    }
}
