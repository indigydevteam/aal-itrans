﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;

namespace iTrans.Transportation.Application.Features.CustomerFiles.Commands
{
    public class UpdateCustomerFileCommand : IRequest<Response<int>>
    {
        public int Id { get; set; }
        public Guid CustomerId { get; set; }
        public string FileName { get; set; }
        public string ContentType { get; set; }
        public string FilePath { get; set; }
        public string FileEXT { get; set; }
        public string DocumentType { get; set; }
        public int Sequence { get; set; }
    }

    public class UpdateCustomerFileCommandHandler : IRequestHandler<UpdateCustomerFileCommand, Response<int>>
    {
        private readonly ICustomerFileRepositoryAsync _customerFileRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;

        public UpdateCustomerFileCommandHandler(ICustomerFileRepositoryAsync customerFileRepository, IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _customerFileRepository = customerFileRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(UpdateCustomerFileCommand request, CancellationToken cancellationToken)
        {
            var customerFile = (await _customerFileRepository.FindByCondition(x => x.Id == request.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            if (customerFile == null)
            {
                throw new ApiException($"CustomerFile Not Found.");
            }
            else
            {
                customerFile.CustomerId = request.CustomerId;
                customerFile.FileName = request.FileName;
                customerFile.ContentType = request.ContentType;
                customerFile.FilePath = request.FilePath;
                customerFile.FileEXT = request.FileEXT;
                customerFile.DocumentType = request.DocumentType;
                customerFile.Sequence = request.Sequence;
                customerFile.Modified = DateTime.UtcNow;
                await _customerFileRepository.UpdateAsync(customerFile);
                var log = new CreateAppLog(_applicationLogRepository);
                log.Create("Customer", "Customer File", "Edit", Newtonsoft.Json.JsonConvert.SerializeObject(customerFile));
                return new Response<int>(customerFile.Id);
            }
        }
    }
}
