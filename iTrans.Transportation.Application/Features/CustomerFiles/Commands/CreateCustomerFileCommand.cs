﻿using AutoMapper;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Interfaces.Services;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.CustomerFiles.Commands
{
    public partial class CreateCustomerFileCommand : IRequest<Response<int>>
    {
        public int Id { get; set; }
        public Guid CustomerId { get; set; }
        public string FileName { get; set; }
        public  string ContentType { get; set; }
        public  string FilePath { get; set; }
        public  string FileEXT { get; set; }
        public  string DocumentType { get; set; }
        public  int Sequence { get; set; }

    }
    public class CreateCustomerFileCommandHandler : IRequestHandler<CreateCustomerFileCommand, Response<int>>
    {
        private readonly ICustomerFileRepositoryAsync _customerFileRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;
        public CreateCustomerFileCommandHandler(ICustomerFileRepositoryAsync customerFileRepository, IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _customerFileRepository = customerFileRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(CreateCustomerFileCommand request, CancellationToken cancellationToken)
        {
            var customerFile = _mapper.Map<CustomerFile>(request);
            customerFile.Created = DateTime.UtcNow;
            customerFile.Modified = DateTime.UtcNow;
            var customerFileObject = await _customerFileRepository.AddAsync(customerFile);
            var log = new CreateAppLog(_applicationLogRepository);
            log.Create("Customer", "Customer File", "Create", Newtonsoft.Json.JsonConvert.SerializeObject(customerFile));
            return new Response<int>(customerFileObject.Id);
        }
    }
}
