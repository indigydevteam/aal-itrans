﻿using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
namespace iTrans.Transportation.Application.Features.CustomerFiles.Commands
{
    public class DeleteCustomerFileByIdCommand : IRequest<Response<int>>
    {
        public int Id { get; set; }
        public class DeleteCustomerFileByIdCommandHandler : IRequestHandler<DeleteCustomerFileByIdCommand, Response<int>>
        {
            private readonly ICustomerFileRepositoryAsync _customerFileRepository;
            public DeleteCustomerFileByIdCommandHandler(ICustomerFileRepositoryAsync customerFileRepository)
            {
                _customerFileRepository = customerFileRepository;
            }
            public async Task<Response<int>> Handle(DeleteCustomerFileByIdCommand command, CancellationToken cancellationToken)
            {
                var customerFile = (await _customerFileRepository.FindByCondition(x => x.Id == command.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (customerFile == null)
                {
                    throw new ApiException($"CustomerFile Not Found.");
                }
                else
                {
                    await _customerFileRepository.DeleteAsync(customerFile);
                    return new Response<int>(customerFile.Id);
                }
            }
        }
    }
}
