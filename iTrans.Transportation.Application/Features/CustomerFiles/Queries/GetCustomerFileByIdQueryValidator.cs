﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using iTrans.Transportation.Application.Interfaces.Repositories;
using FluentValidation;

namespace iTrans.Transportation.Application.Features.CustomerFiles.Queries
{
    public class GetCustomerFileByIdQueryValidator : AbstractValidator<GetCustomerFileByIdQuery>
    {
        private readonly ICustomerFileRepositoryAsync customerFileRepository;

        public GetCustomerFileByIdQueryValidator(ICustomerFileRepositoryAsync customerFileRepository)
        {
            this.customerFileRepository = customerFileRepository;
            RuleFor(p => p.Id)
               .NotEmpty().WithMessage("{PropertyName} is required.")
               .NotNull()
               .MustAsync(IsCustomerFileExists).WithMessage("{PropertyName} not exists.");
        }

        private async Task<bool> IsCustomerFileExists(int Id, CancellationToken cancellationToken)
        {
            var userObject = (await customerFileRepository.FindByCondition(x => x.Id.Equals(Id)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            if (userObject != null)
            {
                return true;
            }
            return false;
        }
    }
}
