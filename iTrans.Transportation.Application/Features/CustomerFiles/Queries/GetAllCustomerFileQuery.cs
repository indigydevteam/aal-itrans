﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.CustomerFile;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.CustomerFiles.Queries
{
    public class GetAllCustomerFileQuery : IRequest<Response<IEnumerable<CustomerFileViewModel>>>
    {
         
    }
    public class GetAllCustomerFileQueryHandler : IRequestHandler<GetAllCustomerFileQuery, Response<IEnumerable<CustomerFileViewModel>>>
    {
        private readonly ICustomerFileRepositoryAsync _customerFileRepository;
        private readonly IMapper _mapper;
        public GetAllCustomerFileQueryHandler(ICustomerFileRepositoryAsync customerFileRepository, IMapper mapper)
        {
            _customerFileRepository = customerFileRepository;
            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<CustomerFileViewModel>>> Handle(GetAllCustomerFileQuery request, CancellationToken cancellationToken)
        {
            var customer = await _customerFileRepository.GetAllAsync();
            var customerFileViewModel = _mapper.Map<IEnumerable<CustomerFileViewModel>>(customer);
            return new Response<IEnumerable<CustomerFileViewModel>>(customerFileViewModel);
        }
    }
}
