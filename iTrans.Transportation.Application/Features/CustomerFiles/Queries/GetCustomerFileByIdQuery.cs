﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.CustomerFile;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.CustomerFiles.Queries
{
   public class GetCustomerFileByIdQuery : IRequest<Response<CustomerFileViewModel>>
    {
        public int Id { get; set; }
    }
    public class GetCustomerFileByIdQueryHandler : IRequestHandler<GetCustomerFileByIdQuery, Response<CustomerFileViewModel>>
    {
        private readonly ICustomerFileRepositoryAsync _customerFileRepository;
        private readonly IMapper _mapper;
        public GetCustomerFileByIdQueryHandler(ICustomerFileRepositoryAsync customerFileRepository, IMapper mapper)
        {
            _customerFileRepository = customerFileRepository;
            _mapper = mapper;
        }
        public async Task<Response<CustomerFileViewModel>> Handle(GetCustomerFileByIdQuery request, CancellationToken cancellationToken)
        {
            var customerFileObject = (await _customerFileRepository.FindByCondition(x => x.Id.Equals(request.Id)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            return new Response<CustomerFileViewModel>(_mapper.Map<CustomerFileViewModel>(customerFileObject));
        }
    }
}
