﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.CustomerFavoriteCar;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.CustomerFavoriteCars.Queries
{
    public class GetAllCustomerFavoriteCarQuery : IRequest<Response<IEnumerable<CustomerFavoriteCarViewModel>>>
    {

    }
    public class GetAllCustomerFavoriteCarQueryHandler : IRequestHandler<GetAllCustomerFavoriteCarQuery, Response<IEnumerable<CustomerFavoriteCarViewModel>>>
    {
        private readonly ICustomerFavoriteCarRepositoryAsync _customerFavoriteCarRepository;
        private readonly IMapper _mapper;
        public GetAllCustomerFavoriteCarQueryHandler(ICustomerFavoriteCarRepositoryAsync customerFavoriteCarRepository, IMapper mapper)
        {
            _customerFavoriteCarRepository = customerFavoriteCarRepository;
            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<CustomerFavoriteCarViewModel>>> Handle(GetAllCustomerFavoriteCarQuery request, CancellationToken cancellationToken)
        {
            var customer = await _customerFavoriteCarRepository.GetAllAsync();
            var customerFavoriteCarViewModel = _mapper.Map<IEnumerable<CustomerFavoriteCarViewModel>>(customer);
            return new Response<IEnumerable<CustomerFavoriteCarViewModel>>(customerFavoriteCarViewModel);
        }
    }
}
