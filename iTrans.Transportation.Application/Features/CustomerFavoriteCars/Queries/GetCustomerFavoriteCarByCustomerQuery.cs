﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.Country;
using iTrans.Transportation.Application.DTOs.CustomerFavoriteCar;
using iTrans.Transportation.Application.DTOs.District;
using iTrans.Transportation.Application.DTOs.Driver;
using iTrans.Transportation.Application.DTOs.DriverCar;
using iTrans.Transportation.Application.DTOs.OrderingStatus;
using iTrans.Transportation.Application.DTOs.Province;
using iTrans.Transportation.Application.DTOs.Subdistrict;
using iTrans.Transportation.Application.DTOs.UserLevel;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using LinqKit;
using MediatR;

namespace iTrans.Transportation.Application.Features.CustomerFavoriteCares.Queries
{
    public class GetCustomerFavoriteCarByCustomerQuery : IRequest<Response<IEnumerable<CustomerFavoriteCarInfomationViewModel>>>
    {
        public Guid customerId { get; set; }
        public string Search { set; get; }
        public int PageNumber { set; get; }
        public int PageSize { set; get; }
        public int HistoryPageNumber { set; get; }
        public int HistoryPageSize { set; get; }
    }
    public class GetCustomerFavoriteCarByCustomerQueryHandler : IRequestHandler<GetCustomerFavoriteCarByCustomerQuery, Response<IEnumerable<CustomerFavoriteCarInfomationViewModel>>>
    {
        private readonly IDriverRepositoryAsync _driverRepository;
        private readonly IUserLevelRepositoryAsync _userLevelRepository;
        private readonly ICustomerFavoriteCarRepositoryAsync _customerFavoriteCarRepository;
        private readonly IDriverCarRepositoryAsync _driverCarRepository;
        private readonly IOrderingRepositoryAsync _orderingRepository;

        private readonly IMapper _mapper;
        public GetCustomerFavoriteCarByCustomerQueryHandler(ICustomerFavoriteCarRepositoryAsync customerFavoriteCarRepository, IDriverRepositoryAsync driverRepository, IDriverCarRepositoryAsync driverCarRepository,
            IUserLevelRepositoryAsync userLevelRepository, IOrderingRepositoryAsync orderingRepository, IMapper mapper)
        {
            _customerFavoriteCarRepository = customerFavoriteCarRepository;
            _driverRepository = driverRepository;
            _driverCarRepository = driverCarRepository;
            _userLevelRepository = userLevelRepository;
            _orderingRepository = orderingRepository;

            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<CustomerFavoriteCarInfomationViewModel>>> Handle(GetCustomerFavoriteCarByCustomerQuery request, CancellationToken cancellationToken)
        {
            try
            {
                string orderStatusPath = $"Shared\\customer_orderingstatus.json";
                var orderingStatusPathFile = Path.Combine(Directory.GetCurrentDirectory(), orderStatusPath);

                var statusJson = System.IO.File.ReadAllText(orderingStatusPathFile);
                List<OrderingStatusViewModel> orderingStatus = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OrderingStatusViewModel>>(statusJson);

                Expression<Func<Domain.CustomerFavoriteCar, bool>> expression = PredicateBuilder.New<Domain.CustomerFavoriteCar>(false);
                expression = x => x.Customer.Id.Equals(request.customerId);
                if (request.Search != null && request.Search.Trim() != "")
                {
                    string search = request.Search.Trim().ToLower().Replace(" ", "");
                    expression = expression.And(x => x.Car.CarRegistration.Trim().ToLower().Replace(" ","").Contains(search)
                                                || x.Car.CarType.Name_TH.Trim().ToLower().Replace(" ", "").Contains(search) || x.Car.CarType.Name_ENG.Trim().ToLower().Replace(" ", "").Contains(search)
                                                || x.Car.CarList.Name_TH.Trim().ToLower().Replace(" ", "").Contains(search) || x.Car.CarList.Name_ENG.Trim().ToLower().Replace(" ", "").Contains(search)
                                                || x.Car.CarDescription.Name_TH.Trim().ToLower().Replace(" ", "").Contains(search) || x.Car.CarDescription.Name_ENG.Trim().ToLower().Replace(" ", "").Contains(search)
                                                || x.Car.CarSpecification.Name_TH.Trim().ToLower().Replace(" ", "").Contains(search) || x.Car.CarSpecification.Name_ENG.Trim().ToLower().Replace(" ", "").Contains(search)
                                                || x.Car.CarFeature.Name_TH.Trim().ToLower().Replace(" ", "").Contains(search) || x.Car.CarFeature.Name_ENG.Trim().ToLower().Replace(" ", "").Contains(search)
                                                || x.Car.CarDescriptionDetail.Trim().ToLower().Replace(" ", "").Contains(search) || x.Car.CarSpecificationDetail.Trim().ToLower().Replace(" ", "").Contains(search)
                                                || x.Car.CarFeatureDetail.Trim().ToLower().Replace(" ", "").Contains(search)
                                                || x.Driver.Name.Trim().ToLower().Replace(" ", "").Contains(search) || x.Driver.PhoneNumber.Trim().ToLower().Replace(" ", "").Contains(search)
                                                || x.Driver.Email.Trim().ToLower().Replace(" ", "").Contains(search)
                                                );
                }
                var dataObject = await _customerFavoriteCarRepository.FindByConditionWithPage(expression, request.PageNumber, request.PageSize);

                var favoriteCarList = _mapper.Map<IEnumerable<CustomerFavoriteCarInfomationViewModel>>(dataObject);
                foreach (CustomerFavoriteCarInfomationViewModel favoriteCar in favoriteCarList)
                {
                    if (favoriteCar.Car != null)
                    {
                        if (request.HistoryPageNumber == null || request.HistoryPageNumber == 0)
                            request.HistoryPageNumber = 1;
                        if (request.HistoryPageSize == null || request.HistoryPageSize == 0)
                            request.HistoryPageSize = 10;
                        #region ordering
                        Expression<Func<Domain.Ordering, bool>> orderingExpression = PredicateBuilder.New<Domain.Ordering>(false);
                        orderingExpression = x => (x.Status == 16 || x.Status == 15) && x.Customer.Id.Equals(request.customerId) && (x.Cars.Where(c => c.CarRegistration == favoriteCar.Car.carRegistration).Count() > 0);
                        Expression<Func<Domain.Ordering, DateTime>> orderExpression = x => x.Created;
                        bool isDescending = true;
                        #endregion
                        var orderings = await _orderingRepository.FindByConditionWithPage(orderingExpression, orderExpression, isDescending, request.HistoryPageNumber, request.HistoryPageSize);
                        //var ordering = (await _orderingRepository.FindByCondition(x => x.Customer.Id.Equals(request.customerId) && (x.Cars.Where(c => c.CarRegistration == favoriteCar.Car.carRegistration).Count() > 0)).ConfigureAwait(false)).OrderByDescending(x => x.Modified).AsQueryable().FirstOrDefault();
                        favoriteCar.History = new List<CustomerFavoriteCarHistoryViewModel>();
                        foreach (Ordering ordering in orderings)
                        {
                            if (ordering != null)
                            {
                                var startLocation = ordering.Addresses.OrderBy(x => x.Sequence).FirstOrDefault();
                                var endLocation = ordering.Addresses.OrderByDescending(x => x.Sequence).FirstOrDefault();
                                CustomerFavoriteCarHistoryViewModel history = new CustomerFavoriteCarHistoryViewModel();
                                history.price = ordering.OrderingPrice;
                                history.startLocation = new LocationViewModel
                                {
                                    country = _mapper.Map<CountryViewModel>(startLocation.Country),
                                    province = _mapper.Map<ProvinceViewModel>(startLocation.Province),
                                    district = _mapper.Map<DistrictViewModel>(startLocation.District),
                                    subDistrict = _mapper.Map<SubdistrictViewModel>(startLocation.Subdistrict),
                                    postCode = startLocation.Subdistrict != null ? startLocation.Subdistrict.PostCode : "",
                                };
                                history.endLocation = new LocationViewModel
                                {
                                    country = _mapper.Map<CountryViewModel>(endLocation.Country),
                                    province = _mapper.Map<ProvinceViewModel>(endLocation.Province),
                                    district = _mapper.Map<DistrictViewModel>(endLocation.District),
                                    subDistrict = _mapper.Map<SubdistrictViewModel>(endLocation.Subdistrict),
                                    postCode = endLocation.Subdistrict != null ? endLocation.Subdistrict.PostCode : "",
                                };
                                history.OrderingStatus = orderingStatus.Where(o => o.id == ordering.Status).FirstOrDefault();
                                history.OrderingId = ordering.Id;
                                history.startDate = startLocation.Modified == null ? ordering.Modified : startLocation.Modified;
                                history.endDate = endLocation.Modified == null ? ordering.Modified : endLocation.Modified;
                                favoriteCar.History.Add(history);
                            }
                        }
                    }
                    // var ordering = (await _orderingRepository.FindByCondition(x => x.Customer.Id.Equals(request.customerId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                }
                return new Response<IEnumerable<CustomerFavoriteCarInfomationViewModel>>(favoriteCarList);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
