﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.Country;
using iTrans.Transportation.Application.DTOs.CustomerFavoriteCar;
using iTrans.Transportation.Application.DTOs.District;
using iTrans.Transportation.Application.DTOs.Driver;
using iTrans.Transportation.Application.DTOs.DriverCar;
using iTrans.Transportation.Application.DTOs.Province;
using iTrans.Transportation.Application.DTOs.Subdistrict;
using iTrans.Transportation.Application.DTOs.UserLevel;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using LinqKit;
using MediatR;

namespace iTrans.Transportation.Application.Features.CustomerFavoriteCares.Queries
{
    public class GetCustomerFavoriteCarByIdQuery : IRequest<Response<CustomerFavoriteCarInfomationViewModel>>
    {
        public Guid CustomerId { get; set; }
        public Guid DriverId { get; set; }
        public int DriverCarId { get; set; }
        public int HistoryPageNumber { set; get; }
        public int HistoryPageSize { set; get; }
    }
    public class GetCustomerFavoriteCarByIdQueryHandler : IRequestHandler<GetCustomerFavoriteCarByIdQuery, Response<CustomerFavoriteCarInfomationViewModel>>
    {
        private readonly IDriverRepositoryAsync _driverRepository;
        private readonly IUserLevelRepositoryAsync _userLevelRepository;
        private readonly ICustomerFavoriteCarRepositoryAsync _customerFavoriteCarRepository;
        private readonly IDriverCarRepositoryAsync _driverCarRepository;
        private readonly IOrderingRepositoryAsync _orderingRepository;

        private readonly IMapper _mapper;
        public GetCustomerFavoriteCarByIdQueryHandler(ICustomerFavoriteCarRepositoryAsync customerFavoriteCarRepository, IDriverRepositoryAsync driverRepository,
            IDriverCarRepositoryAsync driverCarRepository, IUserLevelRepositoryAsync userLevelRepository, IOrderingRepositoryAsync orderingRepository, IMapper mapper)
        {
            _customerFavoriteCarRepository = customerFavoriteCarRepository;
            _driverRepository = driverRepository;
            _driverCarRepository = driverCarRepository;
            _userLevelRepository = userLevelRepository;
            _orderingRepository = orderingRepository;

            _mapper = mapper;
        }

        public async Task<Response<CustomerFavoriteCarInfomationViewModel>> Handle(GetCustomerFavoriteCarByIdQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var dataObject = (await _customerFavoriteCarRepository.FindByCondition(x => x.Driver.Id.Equals(request.DriverId) && x.Customer.Id.Equals(request.CustomerId) && x.Car.Id == request.DriverCarId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (dataObject == null)
                {
                    throw new ApiException($"Favorite Car Not Found.");
                }

                CustomerFavoriteCarInfomationViewModel favoriteCar = _mapper.Map<CustomerFavoriteCarInfomationViewModel>(dataObject);
                if (dataObject.Driver != null)
                {
                    if (dataObject.Driver.Corparate != null)
                    {
                        var corparateInformation = ((await _driverRepository.FindByCondition(x => x.Id.Equals(dataObject.Driver.Corparate)).ConfigureAwait(false)).AsQueryable().FirstOrDefault());
                        favoriteCar.Driver.corparateInformation = _mapper.Map<DriverCorparateInformationViewModel>(corparateInformation);
                        if (favoriteCar.Driver.corparateInformation != null)
                        {
                            favoriteCar.Driver.haveCorparate = true;
                        }
                    }
                }
                if (request.HistoryPageNumber == null || request.HistoryPageNumber == 0)
                    request.HistoryPageNumber = 1;
                if (request.HistoryPageSize == null || request.HistoryPageSize == 0)
                    request.HistoryPageSize = 10;
                #region ordering
                Expression<Func<Domain.Ordering, bool>> expression = PredicateBuilder.New<Domain.Ordering>(false);
                expression = x => (x.Status == 16 || x.Status == 15) && x.Customer.Id.Equals(request.CustomerId) && (x.Cars.Where(c => c.CarRegistration == dataObject.Car.CarRegistration).Count() > 0);
                Expression<Func<Domain.Ordering, DateTime>> orderExpression = x => x.Created;
                bool isDescending = true;
                #endregion
                var orderings = await _orderingRepository.FindByConditionWithPage(expression, orderExpression, isDescending, request.HistoryPageNumber, request.HistoryPageSize);
                //var ordering = (await _orderingRepository.FindByCondition(x => x.Customer.Id.Equals(request.CustomerId) && (x.Cars.Where(c => c.CarRegistration == dataObject.Car.CarRegistration).Count() > 0)).ConfigureAwait(false)).OrderByDescending(x => x.Modified).AsQueryable().FirstOrDefault();
                favoriteCar.History = new List<CustomerFavoriteCarHistoryViewModel>();
                foreach (Ordering ordering in orderings)
                {
                    if (ordering != null)
                    {
                        var startLocation = ordering.Addresses.OrderBy(x => x.Sequence).FirstOrDefault();
                        var endLocation = ordering.Addresses.OrderByDescending(x => x.Sequence).FirstOrDefault();
                        CustomerFavoriteCarHistoryViewModel history = new CustomerFavoriteCarHistoryViewModel();
                        history.price = ordering.OrderingPrice;
                        history.startLocation = new LocationViewModel
                        {
                            country = _mapper.Map<CountryViewModel>(startLocation.Country),
                            province = _mapper.Map<ProvinceViewModel>(startLocation.Province),
                            district = _mapper.Map<DistrictViewModel>(startLocation.District),
                            subDistrict = _mapper.Map<SubdistrictViewModel>(startLocation.Subdistrict),
                            postCode = startLocation.Subdistrict != null ? startLocation.Subdistrict.PostCode: "",
                        };
                        history.endLocation = new LocationViewModel
                        {
                            country = _mapper.Map<CountryViewModel>(endLocation.Country),
                            province = _mapper.Map<ProvinceViewModel>(endLocation.Province),
                            district = _mapper.Map<DistrictViewModel>(endLocation.District),
                            subDistrict = _mapper.Map<SubdistrictViewModel>(endLocation.Subdistrict),
                            postCode = endLocation.Subdistrict != null ? endLocation.Subdistrict.PostCode : "",
                        };
                        
                        history.startDate = startLocation.Modified == null? ordering.Modified: startLocation.Modified;
                        history.endDate = endLocation.Modified == null ? ordering.Modified : endLocation.Modified;
                        favoriteCar.History.Add(history);
                    }
                }
                return new Response<CustomerFavoriteCarInfomationViewModel>(favoriteCar);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
