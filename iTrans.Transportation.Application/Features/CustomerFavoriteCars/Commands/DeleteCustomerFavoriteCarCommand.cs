﻿using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
namespace iTrans.Transportation.Application.Features.CustomerFavoriteCars.Commands
{
    public class DeleteCustomerFavoriteCarCommand : IRequest<Response<int>>
    {
        public Guid UserId { get; set; }
        public Guid CustomerId { get; set; }
        public Guid DriverId { get; set; }
        public int CarId { get; set; }
        public class DeleteCustomerFavoriteCarCommandHandler : IRequestHandler<DeleteCustomerFavoriteCarCommand, Response<int>>
        {
            private readonly ICustomerFavoriteCarRepositoryAsync _customerFavoriteCarRepository;
            private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
            public DeleteCustomerFavoriteCarCommandHandler(ICustomerFavoriteCarRepositoryAsync customerFavoriteCarRepository, IApplicationLogRepositoryAsync applicationLogRepository)
            {
                _customerFavoriteCarRepository = customerFavoriteCarRepository;
                _applicationLogRepository = applicationLogRepository;
            }
            public async Task<Response<int>> Handle(DeleteCustomerFavoriteCarCommand command, CancellationToken cancellationToken)
            {
                var customerFavoriteCar = (await _customerFavoriteCarRepository.FindByCondition(x => x.Customer.Id == command.CustomerId && x.Driver.Id == command.DriverId && x.Car.Id == command.CarId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (customerFavoriteCar == null)
                {
                    throw new ApiException($"CustomerFavoriteCar Not Found.");
                }
                else
                {
                    await _customerFavoriteCarRepository.DeleteAsync(customerFavoriteCar);
                    var log = new CreateAppLog(_applicationLogRepository);
                    log.CreateLog("Customer", "CustomerFavoriteCar", "Delete", Newtonsoft.Json.JsonConvert.SerializeObject(command), command.UserId != null ? command.UserId.ToString(): "-");
                    return new Response<int>(customerFavoriteCar.Id);
                }
            }
        }
    }
}
