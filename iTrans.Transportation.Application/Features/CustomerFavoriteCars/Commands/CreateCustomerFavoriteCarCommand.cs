﻿using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Interfaces.Services;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.CustomerFavoriteCars.Commands
{
    public partial class CreateCustomerFavoriteCarCommand : IRequest<Response<int>>
    {
        public Guid CustomerId { get; set; }
        public Guid DriverId { get; set; }
        public int DriverCarId { get; set; }

    }
    public class CreateCustomerFavoriteCarCommandHandler : IRequestHandler<CreateCustomerFavoriteCarCommand, Response<int>>
    {
        private readonly ICustomerFavoriteCarRepositoryAsync _customerFavoriteCarRepository;
        private readonly ICustomerRepositoryAsync _customerRepository;
        private readonly IDriverRepositoryAsync _driverRepository;
        private readonly IDriverCarRepositoryAsync _driverCarRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;
        public CreateCustomerFavoriteCarCommandHandler(ICustomerRepositoryAsync customerRepository, IDriverRepositoryAsync driverRepository, IDriverCarRepositoryAsync driverCarRepository,
            ICustomerFavoriteCarRepositoryAsync customerFavoriteCarRepository, IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _customerFavoriteCarRepository = customerFavoriteCarRepository;
            _customerRepository = customerRepository;
            _driverRepository = driverRepository;
            _driverCarRepository = driverCarRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(CreateCustomerFavoriteCarCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var customer = (await _customerRepository.FindByCondition(x => x.Id.Equals(request.CustomerId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (customer == null)
                {
                    throw new ApiException($"Customer Not Found.");
                }
                var driver = (await _driverRepository.FindByCondition(x => x.Id.Equals(request.DriverId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (driver == null)
                {
                    throw new ApiException($"Driver Not Found.");
                }
                var car = (await _driverCarRepository.FindByCondition(x => x.Id.Equals(request.DriverCarId) && x.Driver.Id.Equals(request.DriverId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (car == null)
                {
                    throw new ApiException($"Car Not Found.");
                }
                var carFavorite = (await _customerFavoriteCarRepository.FindByCondition(x => x.Customer.Id.Equals(request.CustomerId) && x.Driver.Id.Equals(request.DriverId) && x.Car.Id.Equals(request.DriverCarId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (carFavorite != null)
                {
                    throw new ApiException($"Favorite Car is exist.");
                }
                CustomerFavoriteCar customerFavoriteCar = new CustomerFavoriteCar
                {
                    Customer = customer,
                    Driver = driver,
                    Car = car,
                    Created = DateTime.UtcNow,
                    Modified = DateTime.UtcNow
                };

                var customerFavoriteCarObject = await _customerFavoriteCarRepository.AddAsync(customerFavoriteCar);
                var log = new CreateAppLog(_applicationLogRepository);
                log.Create("Customer", "Customer FavoriteCar", "Create", Newtonsoft.Json.JsonConvert.SerializeObject(request));
                return new Response<int>(customerFavoriteCarObject.Id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
