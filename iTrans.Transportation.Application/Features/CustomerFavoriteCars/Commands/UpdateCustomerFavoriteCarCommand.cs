﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;

namespace iTrans.Transportation.Application.Features.CustomerFavoriteCars.Commands
{
    public class UpdateCustomerFavoriteCarCommand : IRequest<Response<int>>
    {
        public int Id { get; set; }
        public Guid CustomerId { get; set; }
        public Guid DriverId { get; set; }
        public int DriverCarId { get; set; }


    }

    public class UpdateCustomerFavoriteCarCommandHandler : IRequestHandler<UpdateCustomerFavoriteCarCommand, Response<int>>
    {
        private readonly ICustomerFavoriteCarRepositoryAsync _customerFavoriteCarRepository;
        private readonly IDriverRepositoryAsync _driverRepository;
        private readonly IDriverCarRepositoryAsync _driverCarRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;

        public UpdateCustomerFavoriteCarCommandHandler(ICustomerRepositoryAsync customerRepository, IDriverRepositoryAsync driverRepository, IDriverCarRepositoryAsync driverCarRepository,
            ICustomerFavoriteCarRepositoryAsync customerFavoriteCarRepository, IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _customerFavoriteCarRepository = customerFavoriteCarRepository;
            _driverRepository = driverRepository;
            _driverCarRepository = driverCarRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(UpdateCustomerFavoriteCarCommand request, CancellationToken cancellationToken)
        {
            var customerFavoriteCar = (await _customerFavoriteCarRepository.FindByCondition(x => x.Id == request.Id && x.Customer.Id.Equals(request.CustomerId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            if (customerFavoriteCar == null)
            {
                throw new ApiException($"CustomerFavoriteCar Not Found.");
            }
            var driver = (await _driverRepository.FindByCondition(x => x.Id.Equals(request.DriverId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            if (driver == null)
            {
                throw new ApiException($"Driver Not Found.");
            }
            var car = (await _driverCarRepository.FindByCondition(x => x.Id.Equals(request.DriverCarId) && x.Driver.Id.Equals(request.DriverId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            if (car == null)
            {
                throw new ApiException($"Car Not Found.");
            }

            customerFavoriteCar.Driver = driver;
            customerFavoriteCar.Car = car;
            customerFavoriteCar.Modified = DateTime.UtcNow;
            await _customerFavoriteCarRepository.UpdateAsync(customerFavoriteCar);
            var log = new CreateAppLog(_applicationLogRepository);
            log.Create("Customer", "Customer FavoriteCar", "Edit", Newtonsoft.Json.JsonConvert.SerializeObject(request));

            return new Response<int>(customerFavoriteCar.Id);

        }
    }
}
