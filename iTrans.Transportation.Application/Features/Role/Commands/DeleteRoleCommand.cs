﻿using AutoMapper;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.Role.Commands
{
    public class DeleteRoleCommand : IRequest<Response<int>>
    {
        public Guid? UserId { get; set; }
        public int Id { get; set; }
    }


    public class DeleteRoleCommandHandler : IRequestHandler<DeleteRoleCommand, Response<int>>
    {
        private readonly IMapper _mapper;
        private readonly IRoleRepositoryAsync _roleRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        public DeleteRoleCommandHandler(IMapper mapper, IRoleRepositoryAsync roleRepository, IApplicationLogRepositoryAsync applicationLogRepository)
        {
            _mapper = mapper;
            _roleRepository = roleRepository;
            _applicationLogRepository = applicationLogRepository;
        }

        public async Task<Response<int>> Handle(DeleteRoleCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var role = (await _roleRepository.FindByCondition(x => x.Id == request.Id).ConfigureAwait(false)).FirstOrDefault();

                await _roleRepository.DeleteAsync(role);

                var log = new CreateAppLog(_applicationLogRepository);

                log.Create("role", "role", this.ToString(), Newtonsoft.Json.JsonConvert.SerializeObject(role));

                return new Response<int>(role.Id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
