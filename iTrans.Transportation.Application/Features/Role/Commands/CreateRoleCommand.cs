﻿using AutoMapper;
using iTrans.Transportation.Application.DTOs.Menu;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain.Entities;
using MediatR;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.Role.Commands
{
    public class CreateRoleCommand : IRequest<Response<int>>
    {
        public Guid? UserId { get; set; }
        public string NameEN { get; set; }
        public string NameTH { get; set; }
        public string MenuPermissions { get; set; }
    }

    public class CreateRoleCommandHandler : IRequestHandler<CreateRoleCommand, Response<int>>
    {
        private readonly IMapper _mapper;
        private readonly IRoleRepositoryAsync _roleRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IRoleMenuRepositoryAsync _roleMenuRepository;
        private readonly IMenuPermissionRepositoryAsync _menuPermissionRepository;
        public CreateRoleCommandHandler(IMapper mapper, IRoleRepositoryAsync roleRepository, IRoleMenuRepositoryAsync roleMenuRepository, IApplicationLogRepositoryAsync applicationLogRepository, IMenuPermissionRepositoryAsync menuPermissionRepository)
        {
            _mapper = mapper;
            _roleRepository = roleRepository;
            _applicationLogRepository = applicationLogRepository;
            _roleMenuRepository = roleMenuRepository;
            _menuPermissionRepository = menuPermissionRepository;
        }

        public async Task<Response<int>> Handle(CreateRoleCommand request, CancellationToken cancellationToken)
        {
            try
            {
                //var menuPermission =  _menuPermissionRepository.GetAllAsync();


                var role = _mapper.Map<Domain.Entities.Role>(request);
                role.CreatedBy = request.UserId.ToString();
                role.ModifiedBy = request.UserId.ToString();
                role.Created = DateTime.UtcNow;
                role.Modified = DateTime.UtcNow;

                var dataListObject = await _roleRepository.AddAsync(role);

                List<MenuPermissionViewModel> MenuPermissions = JsonConvert.DeserializeObject<List<MenuPermissionViewModel>>(request.MenuPermissions);
                foreach (var menuPermission in MenuPermissions)
                {
                    RoleMenu roleMenu = new RoleMenu();
                    roleMenu.RoleId = dataListObject.Id;
                    roleMenu.MenuId = menuPermission.MenuId;
                    roleMenu.MenuPermission = _mapper.Map<MenuPermission>(menuPermission);
                    roleMenu.CreatedBy = request.UserId.ToString();
                    roleMenu.ModifiedBy = request.UserId.ToString();
                    roleMenu.Created = DateTime.UtcNow;
                    roleMenu.Modified = DateTime.UtcNow;
                    await _roleMenuRepository.AddAsync(roleMenu);

                }

                var log = new CreateAppLog(_applicationLogRepository);
                log.Create("role", "role", this.ToString(), Newtonsoft.Json.JsonConvert.SerializeObject(role));

                return new Response<int>(role.Id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
