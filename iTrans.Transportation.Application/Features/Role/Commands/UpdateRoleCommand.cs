﻿using AutoMapper;
using iTrans.Transportation.Application.DTOs.Menu;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain.Entities;
using MediatR;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.Role.Commands
{
    public class UpdateRoleCommand : IRequest<Response<int>>
    {
        public Guid? UserId { get; set; }
        public int Id { get; set; }
        public string NameTH { get; set; }
        public string NameEN { get; set; }
        public string MenuPermissions { get; set; }
    }


    public class UpdateRoleCommandHandler : IRequestHandler<UpdateRoleCommand, Response<int>>
    {
        private readonly IMapper _mapper;
        private readonly IRoleRepositoryAsync _roleRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IRoleMenuRepositoryAsync _roleMenuRepository;
        public UpdateRoleCommandHandler(IMapper mapper, IRoleRepositoryAsync roleRepository, IRoleMenuRepositoryAsync roleMenuRepository, IApplicationLogRepositoryAsync applicationLogRepository)
        {
            _mapper = mapper;
            _roleRepository = roleRepository;
            _applicationLogRepository = applicationLogRepository;
            _roleMenuRepository = roleMenuRepository;
        }

        public async Task<Response<int>> Handle(UpdateRoleCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var role = (await _roleRepository.FindByCondition(x => x.Id == request.Id).ConfigureAwait(false)).FirstOrDefault();
                role.CreatedBy = request.UserId.ToString();
                role.ModifiedBy = request.UserId.ToString();
                role.Created = DateTime.UtcNow;
                role.Modified = DateTime.UtcNow;

                var dataListObject = await _roleRepository.AddAsync(role);


                var listRoleMenu = (await _roleMenuRepository.FindByCondition(x => x.RoleId == role.Id).ConfigureAwait(false)).ToList();

                foreach (var roleMenu in listRoleMenu)
                {
                    await _roleMenuRepository.DeleteAsync(roleMenu);
                }

                List<MenuPermissionViewModel> MenuPermissions = JsonConvert.DeserializeObject<List<MenuPermissionViewModel>>(request.MenuPermissions);
                foreach (var menuPermission in MenuPermissions)
                {
                    RoleMenu roleMenu = new RoleMenu();
                    roleMenu.RoleId = dataListObject.Id;
                    roleMenu.MenuId = menuPermission.MenuId;
                    roleMenu.MenuPermission = _mapper.Map<MenuPermission>(menuPermission);
                    roleMenu.CreatedBy = request.UserId.ToString();
                    roleMenu.ModifiedBy = request.UserId.ToString();
                    roleMenu.Created = DateTime.UtcNow;
                    roleMenu.Modified = DateTime.UtcNow;
                    await _roleMenuRepository.AddAsync(roleMenu);

                }

                var log = new CreateAppLog(_applicationLogRepository);
                log.Create("role", "role", this.ToString(), Newtonsoft.Json.JsonConvert.SerializeObject(role));

                return new Response<int>(role.Id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
