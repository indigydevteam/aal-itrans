﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.Role.Qureies
{
    public class GetAllRoleQuery : IRequest<Response<IEnumerable<Domain.Entities.Role>>>
    {
    }


    public class GetAllRoleQueryHandler : IRequestHandler<GetAllRoleQuery, Response<IEnumerable<Domain.Entities.Role>>>
    {
        private readonly IRoleRepositoryAsync _roleRepositoryAsync;
        public GetAllRoleQueryHandler(IRoleRepositoryAsync roleRepositoryAsync)
        {
            _roleRepositoryAsync = roleRepositoryAsync;
        }

        public async Task<Response<IEnumerable<Domain.Entities.Role>>> Handle(GetAllRoleQuery request, CancellationToken cancellationToken)
        {
            var listRole = await _roleRepositoryAsync.GetAllAsync();

            return new Response<IEnumerable<Domain.Entities.Role>>(listRole);
        }
    }
}
