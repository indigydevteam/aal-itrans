﻿using AutoMapper;
using iTrans.Transportation.Application.DTOs.Role;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.Role.Qureies
{
    public class GetRoleByIdQuery : IRequest<Response<RoleMenuViewModel>>
    {
        public int Id { get; set; }
    }

    public class GetAllRegisterInformationQueryHandler : IRequestHandler<GetRoleByIdQuery, Response<RoleMenuViewModel>>
    {
        private readonly IRoleRepositoryAsync _roleRepository;
        IRoleMenuRepositoryAsync _roleMenuRepository;
        IMenuPermissionRepositoryAsync _menuPermissionRepository;

        private readonly IMapper _mapper;
        public GetAllRegisterInformationQueryHandler(IRoleRepositoryAsync roleRepository, IMenuPermissionRepositoryAsync menuPermissionRepository, IRoleMenuRepositoryAsync roleMenuRepository, IMapper mapper)
        {
            _roleRepository = roleRepository;
            _roleMenuRepository = roleMenuRepository;
            _menuPermissionRepository = menuPermissionRepository;
            _mapper = mapper;
        }

        public async Task<Response<RoleMenuViewModel>> Handle(GetRoleByIdQuery request, CancellationToken cancellationToken)
        {
            var role = (await _roleRepository.FindByCondition(x => x.Id == request.Id)).FirstOrDefault();
            //var per = (await _menuPermissionRepository.GetAllAsync());

            var roleMenuPermission = (await _roleMenuRepository.FindByCondition(x => x.RoleId == request.Id)).ToList();


            RoleMenuViewModel viewModel = new RoleMenuViewModel
            {
                Id = role.Id,
                NameEN = role.NameEN,
                NameTH = role.NameTH,
                RoleMenuPermission = roleMenuPermission.Select(x => new RoleMenuPermission
                {
                    Id = x.Id,
                    RoleId = x.RoleId,
                    MenuId = x.MenuId,
                    MenuPermissionId = x.MenuPermission.Id,
             

                }).ToList()
            };

            return new Response<RoleMenuViewModel>(viewModel);
        }

    }
}
