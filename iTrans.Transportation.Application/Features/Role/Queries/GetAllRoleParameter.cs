﻿using AutoMapper;
using iTrans.Transportation.Application.DTOs.Role;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.Role.Qureies
{
    public class GetAllRoleParameter : IRequest<Response<IEnumerable<RoleViewModel>>>
    {
        public string Search { set; get; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
    }

    public class GetAllRoleHandler : IRequestHandler<GetAllRoleParameter, Response<IEnumerable<RoleViewModel>>>
    {
        private readonly IRoleRepositoryAsync _roleRepositoryAsync;
        private readonly IRoleMenuRepositoryAsync _roleMenuRepository;

        private readonly IMapper _mapper;
        private Expression<Func<Domain.Entities.Role, bool>> expression;
        public GetAllRoleHandler(IRoleRepositoryAsync roleRepositoryAsync, IRoleMenuRepositoryAsync roleMenuRepository,IMapper mapper)
        {
            _roleRepositoryAsync = roleRepositoryAsync;
            _roleMenuRepository = roleMenuRepository;
            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<RoleViewModel>>> Handle(GetAllRoleParameter request, CancellationToken cancellationToken)
        {
            var validFilter = _mapper.Map<GetAllRoleParameter>(request);
            if (validFilter.Search != null && validFilter.Search.Trim() != "")
            {
                expression = x => (x.NameTH.Contains(validFilter.Search.Trim()) || x.NameEN.Contains(validFilter.Search.Trim()));
            }

            int itemCount = _roleRepositoryAsync.GetItemCount(expression);
            var role = await _roleRepositoryAsync.FindByConditionWithPage(expression, validFilter.PageNumber, validFilter.PageSize);
           
            var rolememu = await _roleMenuRepository.GetAllAsync();






            var result = _mapper.Map<IEnumerable<RoleViewModel>>(role).Select(p => new RoleViewModel
            {
                Id = p.Id,
                NameTH = p.NameTH,
                NameEN = p.NameEN,
                Countmenu = rolememu.Where(x=>x.RoleId.Equals(p.Id)).Count(),
                ModifiedBy = p.ModifiedBy,
                Modified = p.Modified

            }).ToList();



            return new PagedResponse<IEnumerable<RoleViewModel>>(result, validFilter.PageNumber, validFilter.PageSize, itemCount);
        }
    }
}
