﻿using AutoMapper;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.Menu.Commands
{
    public class CreateMenuCommand : IRequest<Response<int>>
    {
        public Guid? UserId { get; set; }
        public string NameTH { get; set; }
        public string NameEN { get; set; }
        public List<string> Permissions { get; set; }

    }


    public class CreateMenuCommandHandler : IRequestHandler<CreateMenuCommand, Response<int>>
    {

        private readonly IMapper _mapper;
        private readonly IMenuRepositoryAsync _menuRepositoryAsync;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMenuPermissionRepositoryAsync _menuPermissionRepository;
        public CreateMenuCommandHandler(IMapper mapper, IMenuRepositoryAsync menuRepositoryAsync, IMenuPermissionRepositoryAsync menuPermissionRepository, IApplicationLogRepositoryAsync applicationLogRepositoryAsync)
        {
            _mapper = mapper;
            _menuRepositoryAsync = menuRepositoryAsync;
            _applicationLogRepository = applicationLogRepositoryAsync;
            _menuPermissionRepository = menuPermissionRepository;
        }

        public async Task<Response<int>> Handle(CreateMenuCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var menu = _mapper.Map<Domain.Menu>(request);
                menu.CreatedBy = request.UserId.ToString();
                menu.ModifiedBy = request.UserId.ToString();
                menu.Created = DateTime.UtcNow;
                menu.Modified = DateTime.UtcNow;
                var menuObject = await _menuRepositoryAsync.AddAsync(menu);

                foreach (var permission in request.Permissions)
                {
                    Domain.Entities.MenuPermission menuPermission = new Domain.Entities.MenuPermission();
                    menuPermission.MenuId = menuObject.Id;
                    menuPermission.Permission = permission;
                    menuPermission.CreatedBy = request.UserId.ToString();
                    menuPermission.ModifiedBy = request.UserId.ToString();
                    menuPermission.Created = DateTime.UtcNow;
                    menuPermission.Modified = DateTime.UtcNow;
                    await _menuPermissionRepository.AddAsync(menuPermission);
                }

                var log = new CreateAppLog(_applicationLogRepository);
                log.Create("menu", "menu", this.ToString(), Newtonsoft.Json.JsonConvert.SerializeObject(menuObject));
                return new Response<int>(menuObject.Id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}