﻿using AutoMapper;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Features.Role.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.Menu.Commands
{
    public class DeleteMenuCommand
    {
        public Guid? UserId { get; set; }
    }

    public class DeleteRoleCommandHandler : IRequestHandler<DeleteRoleCommand, Response<int>>
    {
        private readonly IMapper _mapper;
        private readonly IMenuRepositoryAsync _menuRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        public DeleteRoleCommandHandler(IMapper mapper, IMenuRepositoryAsync menuRepository, IApplicationLogRepositoryAsync applicationLogRepository)
        {
            _mapper = mapper;
            _menuRepository = menuRepository;
            _applicationLogRepository = applicationLogRepository;
        }

        public async Task<Response<int>> Handle(DeleteRoleCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var menu = (await _menuRepository.FindByCondition(x => x.Id == request.Id).ConfigureAwait(false)).FirstOrDefault();

                await _menuRepository.DeleteAsync(menu);

                var log = new CreateAppLog(_applicationLogRepository);

                log.Create("menu", "menu", this.ToString(), Newtonsoft.Json.JsonConvert.SerializeObject(menu));

                return new Response<int>(menu.Id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
