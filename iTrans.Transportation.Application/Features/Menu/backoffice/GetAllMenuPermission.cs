﻿using AutoMapper;
using iTrans.Transportation.Application.Constant.Permission;
using iTrans.Transportation.Application.DTOs.MenuPermission;
using iTrans.Transportation.Application.DTOs.Role;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.Menu.backoffice
{
    public class GetAllMenuPermission : IRequest<Response<IEnumerable<MenuPermissionViewModel>>>
    {
        public int Id { get; set; }
    }

    public class GetAllMenuPermissionHandler : IRequestHandler<GetAllMenuPermission, Response<IEnumerable<MenuPermissionViewModel>>>
    {
        private readonly IMenuPermissionRepositoryAsync _menuPermissionRepository;
        private readonly IMenuRepositoryAsync _menuRepositoryAsync;
        private readonly IRoleRepositoryAsync _roleRepository;
        private readonly IRoleMenuRepositoryAsync _roleMenuRepositoryAsync;



        private readonly IMapper _mapper;


        public GetAllMenuPermissionHandler(IMenuPermissionRepositoryAsync menuPermissionRepository, IRoleMenuRepositoryAsync roleMenuRepositoryAsync, IMenuRepositoryAsync menuRepositoryAsync, IRoleRepositoryAsync roleRepository, IMapper mapper)
        {
            _menuPermissionRepository = menuPermissionRepository;
            _menuRepositoryAsync = menuRepositoryAsync;
            _roleRepository = roleRepository;
            _roleMenuRepositoryAsync = roleMenuRepositoryAsync;
            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<MenuPermissionViewModel>>> Handle(GetAllMenuPermission request, CancellationToken cancellationToken)
        {
            try
            {
                var menuPermissions = await _menuPermissionRepository.GetAllAsync();
                var menu = await _menuRepositoryAsync.GetAllAsync();
                var roleMenu = await _roleMenuRepositoryAsync.GetAllAsync();

                var role = (await _roleRepository.FindByCondition(x => x.Id == request.Id)).FirstOrDefault();


                var result = menu.Select(p => new MenuPermissionViewModel
                {
                    Id = p.Id,
                    RoleId = request.Id,
                    Rolename = role !=null ? role.NameTH :" ",
                    MenuName = p.NameTH,
                    MenuNameEN = p.NameEN,
                    RoleMenuPermission = menuPermissions.Where(x => x.MenuId.Equals(p.Id)).Select(x => new RoleMenuPermission
                    {
                        Id = x.Id,
                        MenuId = x.MenuId,
                        MenuPermissionId = x.Id,
                        Is_Check = roleMenu.Where(r => r.MenuPermission.Id.Equals(x.Id) && r.MenuId.Equals(x.MenuId) && r.RoleId.Equals(request.Id)).Any(),
                        Permission = x.Permission,

                    }).ToList()

                });

                var data = _mapper.Map<IEnumerable<MenuPermissionViewModel>>(result);

                return new Response<IEnumerable<MenuPermissionViewModel>>(data);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
