﻿using AutoMapper;
using iTrans.Transportation.Application.DTOs.Menu;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.Menu.Queries
{
    public class GetAllMenuQueryParameter : IRequest<Response<IEnumerable<MenuViewModel>>>
    {
        public string Search { set; get; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
    }


    public class GetAllMenuHandler : IRequestHandler<GetAllMenuQueryParameter, Response<IEnumerable<MenuViewModel>>>
    {
        private readonly IMenuRepositoryAsync _menuRepositoryAsync;
        private readonly IMapper _mapper;
        private Expression<Func<Domain.Menu, bool>> expression;
        public GetAllMenuHandler(IMenuRepositoryAsync menuRepositoryAsync, IMapper mapper)
        {
            _menuRepositoryAsync = menuRepositoryAsync;
            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<MenuViewModel>>> Handle(GetAllMenuQueryParameter request, CancellationToken cancellationToken)
        {
            var validFilter = _mapper.Map<GetAllMenuQueryParameter>(request);
            if (validFilter.Search != null && validFilter.Search.Trim() != "")
            {
                expression = x => (x.NameTH.Contains(validFilter.Search.Trim()) || x.NameEN.Contains(validFilter.Search.Trim()));
            }

            int itemCount = _menuRepositoryAsync.GetItemCount(expression);
            var menu = await _menuRepositoryAsync.FindByConditionWithPage(expression, validFilter.PageNumber, validFilter.PageSize);

            var menuViewModel = _mapper.Map<IEnumerable<MenuViewModel>>(menu);

            return new PagedResponse<IEnumerable<MenuViewModel>>(menuViewModel, validFilter.PageNumber, validFilter.PageSize, itemCount);
        }
    }
}
