﻿using AutoMapper;
using iTrans.Transportation.Application.DTOs.Menu;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.Menu.Queries
{
    public class GetAllMenuQuery : IRequest<Response<IEnumerable<Domain.Menu>>>
    {

    }


    public class GetAllMenuQueryHandler : IRequestHandler<GetAllMenuQuery, Response<IEnumerable<Domain.Menu>>>
    {
        private readonly IMenuRepositoryAsync _menuRepositoryAsync;
        public GetAllMenuQueryHandler(IMenuRepositoryAsync menuRepositoryAsync, IMapper mapper)
        {
            _menuRepositoryAsync = menuRepositoryAsync;
        }

        public async Task<Response<IEnumerable<Domain.Menu>>> Handle(GetAllMenuQuery request, CancellationToken cancellationToken)
        {

            var menulist = await _menuRepositoryAsync.GetAllAsync();

            return new Response<IEnumerable<Domain.Menu>>(menulist);
        }
    }
}
