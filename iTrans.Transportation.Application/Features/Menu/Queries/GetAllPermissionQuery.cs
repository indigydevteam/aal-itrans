﻿using iTrans.Transportation.Application.Constant.Permission;
using iTrans.Transportation.Application.DTOs.Menu;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.Menu.Queries
{
    public class GetAllPermissionQuery : IRequest<Response<IEnumerable<Domain.Entities.MenuPermission>>>
    {
    }

    public class GetAllPermissionQueryHandler : IRequestHandler<GetAllPermissionQuery, Response<IEnumerable<Domain.Entities.MenuPermission>>>
    {
        IMenuPermissionRepositoryAsync _menuPermissionRepository;

        public GetAllPermissionQueryHandler(IMenuPermissionRepositoryAsync menuPermissionRepository)
        {
            _menuPermissionRepository = menuPermissionRepository;
        }

        public async Task<Response<IEnumerable<Domain.Entities.MenuPermission>>> Handle(GetAllPermissionQuery request, CancellationToken cancellationToken)
        {
            var menuPermissions = await _menuPermissionRepository.GetAllAsync();

            return new Response<IEnumerable<Domain.Entities.MenuPermission>>(menuPermissions);
        }
    }
}
