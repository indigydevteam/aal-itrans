﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.District.Backoffice;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;

namespace iTrans.Transportation.Application.Features.Districts.Backoffice.Queries
{
    public class GetAllDistrictParameter : IRequest<PagedResponse<IEnumerable<DistrictViewModel>>>
    {
        public string Search { set; get; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
    }
    public class GetAllDistrictParameterHandler : IRequestHandler<GetAllDistrictParameter, PagedResponse<IEnumerable<DistrictViewModel>>>
    {
        private readonly IDistrictRepositoryAsync _districtRepository;
        private readonly IMapper _mapper;
        private Expression<Func<District, bool>> expression;
        public GetAllDistrictParameterHandler(IDistrictRepositoryAsync districtRepository, IMapper mapper)
        {
            _districtRepository = districtRepository;
            _mapper = mapper;
        }

        public async Task<PagedResponse<IEnumerable<DistrictViewModel>>> Handle(GetAllDistrictParameter request, CancellationToken cancellationToken)
        {
            var validFilter = _mapper.Map<GetAllDistrictParameter>(request);
            if (validFilter.Search != null && validFilter.Search.Trim() != "")
            {
                expression = x => (x.Name_TH.Contains(validFilter.Search.Trim()) || x.Name_ENG.Contains(validFilter.Search.Trim()));
            }
            int itemCount = _districtRepository.GetItemCount(expression);

            //Expression<Func<District, bool>> expression = x => x.Name_TH != "";
            var district = await _districtRepository.FindByConditionWithPage(expression, validFilter.PageNumber, validFilter.PageSize);
            var districtViewModel = _mapper.Map<IEnumerable<DistrictViewModel>>(district);
            return new PagedResponse<IEnumerable<DistrictViewModel>>(districtViewModel, validFilter.PageNumber, validFilter.PageSize);
        }
    }
}
