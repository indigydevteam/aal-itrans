﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using iTrans.Transportation.Application.Interfaces.Repositories;
using FluentValidation;

namespace iTrans.Transportation.Application.Features.Districts.Backoffice.Queries
{
    public class BackofficeGetDistrictByIdQueryValidator : AbstractValidator<BackofficeGetDistrictByIdQuery>
    {
        private readonly IDistrictRepositoryAsync districtRepository;

        public BackofficeGetDistrictByIdQueryValidator(IDistrictRepositoryAsync districtRepository)
        {
            this.districtRepository = districtRepository;
            RuleFor(p => p.Id)
               .NotEmpty().WithMessage("{PropertyName} is required.")
               .NotNull()
               .MustAsync(IsDistrictExists).WithMessage("{PropertyName} not exists.");
        }

        private async Task<bool> IsDistrictExists(int districtId, CancellationToken cancellationToken)
        {
            var districtObject = (await districtRepository.FindByCondition(x => x.Id.Equals(districtId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            if (districtObject != null)
            {
                return true;
            }
            return false;
        }
    }
}
