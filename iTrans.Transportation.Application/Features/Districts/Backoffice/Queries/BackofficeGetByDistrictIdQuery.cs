﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.District.Backoffice;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.Districts.Backoffice.Queries
{
    public class BackofficeGetDistrictByIdQuery : IRequest<Response<DistrictViewModel>>
    {
        public int Id { get; set; }
    }
    public class BackofficeGetDistrictByIdQueryHandler : IRequestHandler<BackofficeGetDistrictByIdQuery, Response<DistrictViewModel>>
    {
        private readonly IDistrictRepositoryAsync _districtRepository;
        private readonly IMapper _mapper;
        public BackofficeGetDistrictByIdQueryHandler(IDistrictRepositoryAsync districtRepository, IMapper mapper)
        {
            _districtRepository = districtRepository;
            _mapper = mapper;
        }
        public async Task<Response<DistrictViewModel>> Handle(BackofficeGetDistrictByIdQuery request, CancellationToken cancellationToken)
        {
            var districtObject = (await _districtRepository.FindByCondition(x => x.Id.Equals(request.Id)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            return new Response<DistrictViewModel>(_mapper.Map<DistrictViewModel>(districtObject));
        }
    }
}
