﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;

namespace iTrans.Transportation.Application.Features.Districts.Backoffice.Commands
{
    public class UpdateDistrictCommand : IRequest<Response<int>>
    {
        public int Id { get; set; }
        public int ProvinceId { get; set; }
        public string Name_TH { get; set; }
        public string Name_ENG { get; set; }
        public bool Active { get; set; }
    }

    public class UpdateDistrictCommandHandler : IRequestHandler<UpdateDistrictCommand, Response<int>>
    {
        private readonly IProvinceRepositoryAsync _provinceRepository;
        private readonly IDistrictRepositoryAsync _districtRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;

        public UpdateDistrictCommandHandler(IProvinceRepositoryAsync provinceRepository, IDistrictRepositoryAsync districtRepository, IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _provinceRepository = provinceRepository;
            _districtRepository = districtRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(UpdateDistrictCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var district = (await _districtRepository.FindByCondition(x => x.Id == request.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (district == null)
                {
                    throw new ApiException($"District Not Found.");
                }
                else
                {
                    var province = (await _provinceRepository.FindByCondition(x => x.Id == request.ProvinceId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                    district.Province = province;
                    district.Name_TH = request.Name_TH;
                    district.Name_ENG = request.Name_ENG;
                    district.Active = request.Active;
                    district.Modified = DateTime.UtcNow;

                    await _districtRepository.UpdateAsync(district);
                    var log = new CreateAppLog(_applicationLogRepository);
                    log.Create("District", "District", "Edit", Newtonsoft.Json.JsonConvert.SerializeObject(district));
                    return new Response<int>(district.Id);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
