﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using iTrans.Transportation.Application.Interfaces.Repositories;
using FluentValidation;

namespace iTrans.Transportation.Application.Features.Districts.Queries
{
    public class GetDistrictByProvinceQueryValidator : AbstractValidator<GetDistrictByProvinceQuery>
    {
        private readonly IDistrictRepositoryAsync districtRepository;

        public GetDistrictByProvinceQueryValidator(IDistrictRepositoryAsync districtRepository)
        {
            this.districtRepository = districtRepository;
            RuleFor(p => p.ProvinceId)
               .NotEmpty().WithMessage("{PropertyName} is required.")
               .NotNull()
               .MustAsync(IsDistrictExists).WithMessage("{PropertyName} not exists.");
        }

        private async Task<bool> IsDistrictExists(int ProvinceId, CancellationToken cancellationToken)
        {
            var districtObject = (await districtRepository.FindByCondition(x => x.Province.Id == ProvinceId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            if (districtObject != null)
            {
                return true;
            }
            return false;
        }
    }
}
