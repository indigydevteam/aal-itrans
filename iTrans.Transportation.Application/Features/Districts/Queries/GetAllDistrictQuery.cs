﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.District;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.Districts.Queries
{
    public class GetAllDistrictQuery : IRequest<Response<IEnumerable<DistrictViewModel>>>
    {

    }
    public class GetAllDistrictQueryHandler : IRequestHandler<GetAllDistrictQuery, Response<IEnumerable<DistrictViewModel>>>
    {
        private readonly IDistrictRepositoryAsync _districtRepository;
        private readonly IMapper _mapper;
        public GetAllDistrictQueryHandler(IDistrictRepositoryAsync districtRepository, IMapper mapper)
        {
            _districtRepository = districtRepository;
            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<DistrictViewModel>>> Handle(GetAllDistrictQuery request, CancellationToken cancellationToken)
        {
            try
            {
                CultureInfo ci = CultureInfo.GetCultureInfo("th");
                bool ignoreCase = true; //whether comparison should be case-sensitive
                StringComparer comp = StringComparer.Create(ci, ignoreCase);

                var district = await _districtRepository.FindByCondition(x => x.Active == true);
                var districtViewModel = _mapper.Map<IEnumerable<DistrictViewModel>>(district);
                return new Response<IEnumerable<DistrictViewModel>>(districtViewModel.OrderBy(x => x.name_TH, comp));
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
    }
}
