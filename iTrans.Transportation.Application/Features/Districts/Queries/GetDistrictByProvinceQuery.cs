﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.District;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.Districts.Queries
{
    public class GetDistrictByProvinceQuery : IRequest<Response<IEnumerable<DistrictViewModel>>>
    {
      public int ProvinceId { set; get; }
    }
    public class GetDistrictByProvinceQueryHandler : IRequestHandler<GetDistrictByProvinceQuery, Response<IEnumerable<DistrictViewModel>>>
    {
        private readonly IDistrictRepositoryAsync _districtRepository;
        private readonly IMapper _mapper;
        public GetDistrictByProvinceQueryHandler(IDistrictRepositoryAsync districtRepository, IMapper mapper)
        {
            _districtRepository = districtRepository;
            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<DistrictViewModel>>> Handle(GetDistrictByProvinceQuery request, CancellationToken cancellationToken)
        {
            try
            {
                CultureInfo ci = CultureInfo.GetCultureInfo("th");
                bool ignoreCase = true; //whether comparison should be case-sensitive
                StringComparer comp = StringComparer.Create(ci, ignoreCase);

                var district = (await _districtRepository.FindByCondition(x => x.Province.Id == request.ProvinceId && x.Active == true).ConfigureAwait(false)).AsQueryable().ToList();
                var districtViewModel = _mapper.Map<IEnumerable<DistrictViewModel>>(district);
                return new Response<IEnumerable<DistrictViewModel>>(districtViewModel.OrderBy(x => x.name_TH, comp));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
