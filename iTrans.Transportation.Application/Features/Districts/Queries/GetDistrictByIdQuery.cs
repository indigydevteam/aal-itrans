﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.District;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.Districts.Queries
{
    public class GetDistrictByIdQuery : IRequest<Response<DistrictViewModel>>
    {
        public int Id { get; set; }
    }
    public class GetDistrictByIdQueryHandler : IRequestHandler<GetDistrictByIdQuery, Response<DistrictViewModel>>
    {
        private readonly IDistrictRepositoryAsync _districtRepository;
        private readonly IMapper _mapper;
        public GetDistrictByIdQueryHandler(IDistrictRepositoryAsync districtRepository, IMapper mapper)
        {
            _districtRepository = districtRepository;
            _mapper = mapper;
        }
        public async Task<Response<DistrictViewModel>> Handle(GetDistrictByIdQuery request, CancellationToken cancellationToken)
        {
            var districtObject = (await _districtRepository.FindByCondition(x => x.Id == request.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            return new Response<DistrictViewModel>(_mapper.Map<DistrictViewModel>(districtObject));
        }
    }
}
