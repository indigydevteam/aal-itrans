﻿using AutoMapper;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Interfaces.Services;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.Districts.Commands
{
    public partial class CreateDistrictCommand : IRequest<Response<int>>
    { 
        public int ProvinceId { get; set; }
        public string Name_TH { get; set; }
        public string Name_ENG { get; set; }
        public bool Active { get; set; }

    }
    public class CreateDistrictCommandHandler : IRequestHandler<CreateDistrictCommand, Response<int>>
    {
        private readonly IDistrictRepositoryAsync _districtRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;
        public CreateDistrictCommandHandler(IDistrictRepositoryAsync districtRepository, IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _districtRepository = districtRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(CreateDistrictCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var district = _mapper.Map<District>(request);
                district.Created = DateTime.UtcNow;
                district.Modified = DateTime.UtcNow;
                //country.CreatedBy = "xxxxxxx";
                //country.ModifiedBy = "xxxxxxx";
                var districtObject = await _districtRepository.AddAsync(district);
                var log = new CreateAppLog(_applicationLogRepository);
                log.Create("District", "District", "Create", Newtonsoft.Json.JsonConvert.SerializeObject(district));
                return new Response<int>(districtObject.Id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
