﻿using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.Districts.Commands
{
    public class DeleteDistrictByIdCommand : IRequest<Response<int>>
    {
        public int Id { get; set; }
        public class DeleteDistrictByIdCommandHandler : IRequestHandler<DeleteDistrictByIdCommand, Response<int>>
        {
            private readonly IDistrictRepositoryAsync _districtRepository;
            public DeleteDistrictByIdCommandHandler(IDistrictRepositoryAsync districtRepository)
            {
                _districtRepository = districtRepository;
            }
            public async Task<Response<int>> Handle(DeleteDistrictByIdCommand command, CancellationToken cancellationToken)
            {
                var district = (await _districtRepository.FindByCondition(x => x.Id == command.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (district == null)
                {
                    throw new ApiException($"District Not Found.");
                }
                else
                {
                    await _districtRepository.DeleteAsync(district);
                    return new Response<int>(district.Id);
                }
            }
        }
    }
}
