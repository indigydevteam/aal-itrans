﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.ApplicationLog;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
namespace iTrans.Transportation.Application.Features.ApplicationLogs.Queries
{
    public class GetAllApplicationLogQuery : IRequest<Response<IEnumerable<ApplicationLogViewModel>>>
    {

    }
    public class GetAllApplicationLogQueryHandler : IRequestHandler<GetAllApplicationLogQuery, Response<IEnumerable<ApplicationLogViewModel>>>
    {
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;
        public GetAllApplicationLogQueryHandler(IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<ApplicationLogViewModel>>> Handle(GetAllApplicationLogQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var ApplicationLog = await _applicationLogRepository.GetAllAsync();
                var ApplicationLogViewModel = _mapper.Map<IEnumerable<ApplicationLogViewModel>>(ApplicationLog);
                return new Response<IEnumerable<ApplicationLogViewModel>>(ApplicationLogViewModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
