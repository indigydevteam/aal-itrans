﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.ApplicationLog;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.ApplicationLogs.Queries
{
    public class GetApplicationLogByIdQuery : IRequest<Response<ApplicationLogViewModel>>
    {
        public int Id { get; set; }
    }
    public class GetApplicationLogByIdQueryHandler : IRequestHandler<GetApplicationLogByIdQuery, Response<ApplicationLogViewModel>>
    {
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;
        public GetApplicationLogByIdQueryHandler(IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
        }
        public async Task<Response<ApplicationLogViewModel>> Handle(GetApplicationLogByIdQuery request, CancellationToken cancellationToken)
        {
            var ApplicationLogObject = (await _applicationLogRepository.FindByCondition(x => x.Id.Equals(request.Id)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            return new Response<ApplicationLogViewModel>(_mapper.Map<ApplicationLogViewModel>(ApplicationLogObject));
        }
    }
}
