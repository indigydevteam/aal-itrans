﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.ApplicationLog.BackOffice;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using iTrans.Transportation.Domain.Entities;
using MediatR;

namespace iTrans.Transportation.Application.Features.ApplicationLogs.BackOffice
{
    public class GetAllApplicationLogSelectModule : IRequest<Response<IEnumerable<ApplicationLogSelectModuleViewModel>>>
    {
       
    }
    public class GetAllApplicationLogSelectModuleHandler : IRequestHandler<GetAllApplicationLogSelectModule, Response<IEnumerable<ApplicationLogSelectModuleViewModel>>>
    {
        private readonly IApplicationLogRepositoryAsync _ApplicationLogRepository;
        private readonly ISystemUserRepositoryAsync _userRepository;
        private readonly IDriverRepositoryAsync _driverRepository;
        private readonly ICustomerRepositoryAsync _customerRepository;
        private readonly IMapper _mapper;
        public GetAllApplicationLogSelectModuleHandler(IApplicationLogRepositoryAsync ApplicationLogRepository, ISystemUserRepositoryAsync userRepository, IDriverRepositoryAsync driverRepository, ICustomerRepositoryAsync customerRepository, IMapper mapper)
        {
            _ApplicationLogRepository = ApplicationLogRepository;
            _userRepository = userRepository;
            _driverRepository = driverRepository;
            _customerRepository = customerRepository;
            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<ApplicationLogSelectModuleViewModel>>> Handle(GetAllApplicationLogSelectModule request, CancellationToken cancellationToken)
        {
            var validFilter = _mapper.Map<GetAllApplicationLogSelectModule>(request);
        
            var applog = _ApplicationLogRepository.GetAllAsync().Result.ToList().OrderByDescending(x => x.Modified);

            var result = from x in applog
                         select new ApplicationLogSelectModuleViewModel
                         {
                             Module = x.Module
                         };
            result = result.GroupBy(item => item.Module).Select(group => new ApplicationLogSelectModuleViewModel { Module = group.Key }).ToList();

            return new Response<IEnumerable<ApplicationLogSelectModuleViewModel>>(result);
        }
    }
}
