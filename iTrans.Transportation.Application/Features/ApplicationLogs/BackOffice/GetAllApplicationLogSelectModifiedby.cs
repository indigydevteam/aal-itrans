﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.ApplicationLog.BackOffice;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using iTrans.Transportation.Domain.Entities;
using MediatR;

namespace iTrans.Transportation.Application.Features.ApplicationLogs.BackOffice
{
    public class GetAllApplicationLogSelectModifiedby : IRequest<Response<IEnumerable<ApplicationLogSelectModifiedByViewModel>>>
    {
      
    }
    public class GetAllApplicationLogSelectModifiedbyHandler : IRequestHandler<GetAllApplicationLogSelectModifiedby, Response<IEnumerable<ApplicationLogSelectModifiedByViewModel>>>
    {
        private readonly IApplicationLogRepositoryAsync _ApplicationLogRepository;
        private readonly ISystemUserRepositoryAsync _userRepository;
        private readonly IDriverRepositoryAsync _driverRepository;
        private readonly ICustomerRepositoryAsync _customerRepository;
        private readonly IMapper _mapper;
        public GetAllApplicationLogSelectModifiedbyHandler(IApplicationLogRepositoryAsync ApplicationLogRepository, ISystemUserRepositoryAsync userRepository, IDriverRepositoryAsync driverRepository, ICustomerRepositoryAsync customerRepository, IMapper mapper)
        {
            _ApplicationLogRepository = ApplicationLogRepository;
            _userRepository = userRepository;
            _driverRepository = driverRepository;
            _customerRepository = customerRepository;
            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<ApplicationLogSelectModifiedByViewModel>>> Handle(GetAllApplicationLogSelectModifiedby request, CancellationToken cancellationToken)
        {
            var validFilter = _mapper.Map<GetAllApplicationLogSelectModifiedby>(request);
            var user = _userRepository.GetAllAsync().Result.ToList();
            var driver = _driverRepository.GetAllAsync().Result.ToList();
            var customer = _customerRepository.GetAllAsync().Result.ToList();

            var query = (from u in user select new MapUserViewModel { Id = u.EmplID.ToString(), name = u.FirstNameEN+" "+u.LastNameEN })
                    .Concat(from d in driver select new MapUserViewModel { Id = d.Id.ToString(), name = d.FirstName + " " + d.LastName })
                    .Concat(from c in driver select new MapUserViewModel { Id = c.Id.ToString(), name = c.FirstName + " " + c.LastName });
           
            var applog = _ApplicationLogRepository.GetAllAsync().Result.ToList().OrderByDescending(x => x.Modified);

            var result = from x in applog
                         join q in query on x.ModifiedBy equals q.Id
                         select new ApplicationLogSelectModifiedByViewModel
                         {
                             ModifiedBy = q.name
                          };
            result  = result.GroupBy(item => item.ModifiedBy).Select(group => new ApplicationLogSelectModifiedByViewModel { ModifiedBy = group.Key}).ToList();

            return new Response<IEnumerable<ApplicationLogSelectModifiedByViewModel>>(result);
        }
    }
}
