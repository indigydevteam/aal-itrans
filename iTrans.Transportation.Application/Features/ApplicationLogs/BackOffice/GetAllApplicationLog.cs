﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.ApplicationLog.BackOffice;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using iTrans.Transportation.Domain.Entities;
using MediatR;

namespace iTrans.Transportation.Application.Features.ApplicationLogs.BackOffice
{
    public class GetAllApplicationLog : IRequest<PagedResponse<IEnumerable<ApplicationLogViewModel>>>
    {
        public string Module { set; get; }
        public DateTime startdate { set; get; }
        public DateTime enddate { set; get; }
        public string modifiedBy { set; get; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public int Active { set; get; }
        public string Column { set; get; }
    }
    public class GetAllApplicationLogHandler : IRequestHandler<GetAllApplicationLog, PagedResponse<IEnumerable<ApplicationLogViewModel>>>
    {
        private readonly IApplicationLogRepositoryAsync _ApplicationLogRepository;
        private readonly ISystemUserRepositoryAsync _userRepository;
        private readonly IDriverRepositoryAsync _driverRepository;
        private readonly ICustomerRepositoryAsync _customerRepository;
        private readonly IMapper _mapper;
        public GetAllApplicationLogHandler(IApplicationLogRepositoryAsync ApplicationLogRepository, ISystemUserRepositoryAsync userRepository, IDriverRepositoryAsync driverRepository, ICustomerRepositoryAsync customerRepository, IMapper mapper)
        {
            _ApplicationLogRepository = ApplicationLogRepository;
            _userRepository = userRepository;
            _driverRepository = driverRepository;
            _customerRepository = customerRepository;
            _mapper = mapper;
        }

        public async Task<PagedResponse<IEnumerable<ApplicationLogViewModel>>> Handle(GetAllApplicationLog request, CancellationToken cancellationToken)
        {
            var date = new DateTime();
            var validFilter = _mapper.Map<GetAllApplicationLog>(request);

            var user = _userRepository.GetAllAsync().Result.ToList();
            var driver = _driverRepository.GetAllAsync().Result.ToList();
            var customer = _customerRepository.GetAllAsync().Result.ToList();

            var query = (from u in user select new MapUserViewModel { Id = u.EmplID.ToString(), name = u.FirstNameEN+" "+u.LastNameEN })
                    .Concat(from d in driver select new MapUserViewModel { Id = d.Id.ToString(), name = d.FirstName + " " + d.LastName })
                    .Concat(from c in driver select new MapUserViewModel { Id = c.Id.ToString(), name = c.FirstName + " " + c.LastName });
           
            var applog = _ApplicationLogRepository.GetAllAsync().Result.ToList().OrderByDescending(x => x.Modified);
            var result = from x in applog

                         select new ApplicationLogViewModel
                         {
                             Id = x.Id,
                             Module = x.Module != null ? x.Module : "-",
                             Section = x.Section != null ? x.Section : "-",
                             Function = x.Function != null ? x.Function : "-",
                             Content = x.Content != null ? x.Content : "-",
                             ModifiedBy = query.Where(q => q.Id.Equals(x.ModifiedBy)).Select(q => q.name).FirstOrDefault() != null ? query.Where(q => q.Id.Equals(x.ModifiedBy)).Select(q => q.name).FirstOrDefault() : "-",
                             Created = x.Created

                         };

            var check = result.ToList();


            if (validFilter.Module != null && validFilter.Module.Trim() != "")
            {
                result = result.Where(x=>x.Module.Equals(validFilter.Module));
            }
            if (validFilter.enddate != date && validFilter.startdate != date)
            {
                result =result.Where(x=>x.Created > validFilter.startdate && x.Created < validFilter.enddate);
            }
            if (validFilter.modifiedBy != null && validFilter.modifiedBy.Trim() != "")
            {
                result =result.Where(x=>x.ModifiedBy.Equals(validFilter.modifiedBy));
            }
            if (validFilter.Module != null && validFilter.modifiedBy != null && validFilter.modifiedBy.Trim() != "" && validFilter.Module.Trim() != "")
            {
                result =result.Where(x=>x.Module.Equals(validFilter.Module) && x.ModifiedBy.Equals(validFilter.modifiedBy));
            }

            if (validFilter.enddate != date && validFilter.startdate != date && validFilter.Module != null && validFilter.Module.Trim() != "")
            {
                result =result.Where(x=>x.Created > validFilter.startdate && x.Created < validFilter.enddate && x.Module.Equals(validFilter.Module));
            }

            if (validFilter.enddate != date && validFilter.startdate != date && validFilter.modifiedBy != null && validFilter.modifiedBy.Trim() != "")
            {
                result =result.Where(x=>x.Created > validFilter.startdate && x.Created < validFilter.enddate && x.ModifiedBy.Equals(validFilter.modifiedBy));
            }

            if (validFilter.enddate != date && validFilter.startdate != date && validFilter.Module != null && validFilter.modifiedBy != null && validFilter.Module.Trim() != "" && validFilter.modifiedBy.Trim() != "")
            {
                result =result.Where(x=>x.Created > validFilter.startdate && x.Created < validFilter.enddate && x.Module.Equals(validFilter.Module) && x.ModifiedBy.Equals(validFilter.modifiedBy));
            }


            if (request.Column == "module" && request.Active == 2)
            {
                result = result.OrderByDescending(x => x.Module);
            }
            if (request.Column == "module" && request.Active == 3)
            {
                result = result.OrderBy(x => x.Module);
            }
            if (request.Column == "created" && request.Active == 2)
            {
                result = result.OrderByDescending(x => x.Created);
            }
            if (request.Column == "created" && request.Active == 3)
            {
                result = result.OrderBy(x => x.Created);
            }
            if (request.Column == "section" && request.Active == 2)
            {
                result = result.OrderByDescending(x => x.Section);
            }
            if (request.Column == "section" && request.Active == 3)
            {
                result = result.OrderBy(x => x.Section);
            }
            if (request.Column == "function" && request.Active == 2)
            {
                result = result.OrderByDescending(x => x.Function);
            }
            if (request.Column == "function" && request.Active == 3)
            {
                result = result.OrderBy(x => x.Function);
            }
            if (request.Column == "content" && request.Active == 2)
            {
                result = result.OrderByDescending(x => x.Content);
            }
            if (request.Column == "content" && request.Active == 3)
            {
                result = result.OrderBy(x => x.Content);
            }
            if (request.Column == "modifiedBy" && request.Active == 2)
            {
                result = result.OrderByDescending(x => x.ModifiedBy);
            }
            if (request.Column == "modifiedBy" && request.Active == 3)
            {
                result = result.OrderBy(x => x.ModifiedBy);
            }

            var itemCount = result.Count();
            result = result.Skip((validFilter.PageNumber - 1) * validFilter.PageSize).Take(validFilter.PageSize);

            return new PagedResponse<IEnumerable<ApplicationLogViewModel>>(result, validFilter.PageNumber, validFilter.PageSize, itemCount);
        }
    }
}
