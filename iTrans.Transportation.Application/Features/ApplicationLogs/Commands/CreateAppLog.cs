﻿using AutoMapper;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Interfaces.Services;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using iTrans.Transportation.Domain.Entities;
using MediatR;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
namespace iTrans.Transportation.Application.Features.ApplicationLogs.Commands
{
    public class CreateAppLog
    {
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;



        public CreateAppLog(IApplicationLogRepositoryAsync applicationLogRepository)
        {
            _applicationLogRepository = applicationLogRepository;
        }
        public void Create(string module,string section,string function, string content)
        {
            try
            {
               
                ApplicationLog applicationLog = new ApplicationLog();
                applicationLog.Module = module;
                applicationLog.Section = section;
                applicationLog.Function = function;
                applicationLog.Content = content;
                applicationLog.Created = DateTime.UtcNow;
                applicationLog.Modified = DateTime.UtcNow;
                //title.CreatedBy = "xxxxxxx";
                //title.ModifiedBy = "xxxxxxx";
                var ApplicationLogObject = _applicationLogRepository.AddAsync(applicationLog);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void CreateLog(string module, string section, string function, string content, string userId)
        {
            try
            {
                ApplicationLog applicationLog = new ApplicationLog();
                applicationLog.Module = module;
                applicationLog.Section = section;
                applicationLog.Function = function;
                applicationLog.Content = content;
                applicationLog.Created = DateTime.UtcNow;
                applicationLog.Modified = DateTime.UtcNow;
                applicationLog.ModifiedBy = userId != null ? userId : "-";
                applicationLog.CreatedBy = userId != null ? userId : "-";
                var ApplicationLogObject = _applicationLogRepository.AddAsync(applicationLog);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
       

    }
}
