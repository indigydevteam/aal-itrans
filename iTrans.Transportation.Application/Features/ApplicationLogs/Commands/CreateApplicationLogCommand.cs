﻿using AutoMapper;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Interfaces.Services;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using iTrans.Transportation.Domain.Entities;
using MediatR;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.ApplicationLogs.Commands
{
    public partial class CreateApplicationLogCommand : IRequest<Response<int>>
    {
        public virtual string Module { get; set; }
        public virtual string Section { get; set; }
        public virtual string Function { get; set; }
        public virtual string Content { get; set; }
    }
    public class CreateApplicationLogCommandHandler : IRequestHandler<CreateApplicationLogCommand, Response<int>>
    {
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;
        public CreateApplicationLogCommandHandler(IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(CreateApplicationLogCommand request, CancellationToken cancellationToken)
        {
            try
            {
                 
                var ApplicationLog = _mapper.Map<ApplicationLog>(request);
                ApplicationLog.Created = DateTime.UtcNow;
                ApplicationLog.Modified = DateTime.UtcNow;
                //title.CreatedBy = "xxxxxxx";
                //title.ModifiedBy = "xxxxxxx";
                var ApplicationLogObject = await _applicationLogRepository.AddAsync(ApplicationLog);
                return new Response<int>(ApplicationLogObject.Id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}