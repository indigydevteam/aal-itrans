﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;



namespace iTrans.Transportation.Application.Features.ApplicationLogs.Commands
{
    public class UpdateApplicationLogCommand : IRequest<Response<int>>
    {
        public  int Id { get; set; }
        public  string Module { get; set; }
        public  string Section { get; set; }
        public  string Function { get; set; }
        public  string Content { get; set; }

    }

    public class UpdateApplicationLogCommandHandler : IRequestHandler<UpdateApplicationLogCommand, Response<int>>
    {
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;

        public UpdateApplicationLogCommandHandler(IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(UpdateApplicationLogCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var ApplicationLog = (await _applicationLogRepository.FindByCondition(x => x.Id == request.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (ApplicationLog == null)
                {
                    throw new ApiException($"ApplicationLog Not Found.");
                }
                else
                {
                    ApplicationLog.Module = request.Module;
                    ApplicationLog.Section = request.Section;
                    ApplicationLog.Function = request.Function;
                    ApplicationLog.Content = request.Content;
                   


                    await _applicationLogRepository.UpdateAsync(ApplicationLog);
                    return new Response<int>(ApplicationLog.Id);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}