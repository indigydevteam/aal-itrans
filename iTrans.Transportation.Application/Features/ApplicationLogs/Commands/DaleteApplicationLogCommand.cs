﻿using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;


namespace iTrans.Transportation.Application.Features.ApplicationLogs.Commands
{
    class DeleteApplicationLogCommand : IRequest<Response<int>>
    {
        public int Id { get; set; }
        public class DeleteApplicationLogCommandHandler : IRequestHandler<DeleteApplicationLogCommand, Response<int>>
        {
            private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
            public DeleteApplicationLogCommandHandler(IApplicationLogRepositoryAsync applicationLogRepository)
            {
                _applicationLogRepository = applicationLogRepository;
            }
            public async Task<Response<int>> Handle(DeleteApplicationLogCommand command, CancellationToken cancellationToken)
            {
                var ApplicationLog = (await _applicationLogRepository.FindByCondition(x => x.Id == command.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (ApplicationLog == null)
                {
                    throw new ApiException($"ApplicationLog Not Found.");
                }
                else
                {
                    await _applicationLogRepository.DeleteAsync(ApplicationLog);
                    return new Response<int>(ApplicationLog.Id);
                }
            }
        }
    }
}