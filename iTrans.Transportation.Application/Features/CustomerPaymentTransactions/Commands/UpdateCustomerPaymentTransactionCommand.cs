﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;

namespace iTrans.Transportation.Application.Features.CustomerPaymentTransactions.Commands
{
    public class UpdateCustomerPaymentTransactionCommand : IRequest<Response<int>>
    {
        public int Id { get; set; }
        public int Customer_PaymentHistoryId { get; set; }
        public string Detail { get; set; }
    }
    public class UpdateCustomerPaymentTransactionCommandHandler : IRequestHandler<UpdateCustomerPaymentTransactionCommand, Response<int>>
    {
        private readonly ICustomerPaymentTransactionRepositoryAsync _customerPaymentTransactionRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;

        public UpdateCustomerPaymentTransactionCommandHandler(ICustomerPaymentTransactionRepositoryAsync customerPaymentTransactionRepository,IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _customerPaymentTransactionRepository = customerPaymentTransactionRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(UpdateCustomerPaymentTransactionCommand request, CancellationToken cancellationToken)
        {
            var customerPaymentTransaction = (await _customerPaymentTransactionRepository.FindByCondition(x => x.Id == request.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            if (customerPaymentTransaction == null)
            {
                throw new ApiException($"CustomerPaymentTransaction Not Found.");
            }
            else
            {
                customerPaymentTransaction.Customer_PaymentHistoryId = request.Customer_PaymentHistoryId;
                customerPaymentTransaction.Detail = request.Detail;
                customerPaymentTransaction.Modified = DateTime.UtcNow;

                await _customerPaymentTransactionRepository.UpdateAsync(customerPaymentTransaction);
                var log = new CreateAppLog(_applicationLogRepository);
                log.Create("Customer", "Customer Paymenttransaction", "Edit", Newtonsoft.Json.JsonConvert.SerializeObject(customerPaymentTransaction));
                return new Response<int>(customerPaymentTransaction.Id);
            }
        }
    }
}
