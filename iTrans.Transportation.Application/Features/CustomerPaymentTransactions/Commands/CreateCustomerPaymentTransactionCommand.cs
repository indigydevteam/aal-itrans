﻿using AutoMapper;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Interfaces.Services;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using iTrans.Transportation.Domain.Entities;
using MediatR;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.CustomerPaymentTransactions.Commands
{
    public partial class CreateCustomerPaymentTransactionCommand : IRequest<Response<int>>
    {
        public  int Customer_PaymentHistoryId { get; set; }
        public  string Detail { get; set; }

    }
    public class CreateCustomerPaymentTransactionCommandHandler : IRequestHandler<CreateCustomerPaymentTransactionCommand, Response<int>>
    {
        private readonly ICustomerPaymentTransactionRepositoryAsync _customerPaymentTransactionRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;
        public CreateCustomerPaymentTransactionCommandHandler(ICustomerPaymentTransactionRepositoryAsync customerPaymentTransactionRepository,IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _customerPaymentTransactionRepository = customerPaymentTransactionRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(CreateCustomerPaymentTransactionCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var customerPaymentTransaction = _mapper.Map<CustomerPaymentTransaction>(request);
                customerPaymentTransaction.Created = DateTime.UtcNow;
                customerPaymentTransaction.Modified = DateTime.UtcNow;
                var customerPaymentTransactionObject = await _customerPaymentTransactionRepository.AddAsync(customerPaymentTransaction);
                var log = new CreateAppLog(_applicationLogRepository);
                log.Create("Customer", "Customer Paymenttransaction", "Create", Newtonsoft.Json.JsonConvert.SerializeObject(customerPaymentTransaction));
                return new Response<int>(customerPaymentTransactionObject.Id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
