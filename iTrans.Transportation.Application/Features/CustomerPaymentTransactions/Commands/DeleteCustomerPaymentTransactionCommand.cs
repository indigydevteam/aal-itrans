﻿using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
namespace iTrans.Transportation.Application.Features.CustomerPaymentTransactions.Commands
{
    public class DeleteCustomerPaymentTransactionCommand : IRequest<Response<int>>
    {
        public int Id { get; set; }
        public class DeleteCustomerPaymentTransactionCommandHandler : IRequestHandler<DeleteCustomerPaymentTransactionCommand, Response<int>>
        {
            private readonly ICustomerPaymentTransactionRepositoryAsync _customerPaymentTransactionRepository;
            public DeleteCustomerPaymentTransactionCommandHandler(ICustomerPaymentTransactionRepositoryAsync customerPaymentTransactionRepository)
            {
                _customerPaymentTransactionRepository = customerPaymentTransactionRepository;
            }
            public async Task<Response<int>> Handle(DeleteCustomerPaymentTransactionCommand command, CancellationToken cancellationToken)
            {
                var customerPaymentTransaction = (await _customerPaymentTransactionRepository.FindByCondition(x => x.Id == command.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (customerPaymentTransaction == null)
                {
                    throw new ApiException($"CustomerPaymentTransaction Not Found.");
                }
                else
                {
                    await _customerPaymentTransactionRepository.DeleteAsync(customerPaymentTransaction);
                    return new Response<int>(customerPaymentTransaction.Id);
                }
            }
        }
    }
}
