﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.CustomerPaymentTransaction;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.CustomerPaymentTransactions.Queries
{
    public class GetAllCustomerPaymentTransactionQuery : IRequest<Response<IEnumerable<CustomerPaymentTransactionViewModel>>>
    {

    }
    public class GetAllCustomerPaymentTransactionQueryHandler : IRequestHandler<GetAllCustomerPaymentTransactionQuery, Response<IEnumerable<CustomerPaymentTransactionViewModel>>>
    {
        private readonly ICustomerPaymentTransactionRepositoryAsync _customerPaymentTransactionRepository;
        private readonly IMapper _mapper;
        public GetAllCustomerPaymentTransactionQueryHandler(ICustomerPaymentTransactionRepositoryAsync customerPaymentTransactionRepository, IMapper mapper)
        {
            _customerPaymentTransactionRepository = customerPaymentTransactionRepository;
            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<CustomerPaymentTransactionViewModel>>> Handle(GetAllCustomerPaymentTransactionQuery request, CancellationToken cancellationToken)
        {
            var customer = await _customerPaymentTransactionRepository.GetAllAsync();
            var customerPaymentTransactionViewModel = _mapper.Map<IEnumerable<CustomerPaymentTransactionViewModel>>(customer);
            return new Response<IEnumerable<CustomerPaymentTransactionViewModel>>(customerPaymentTransactionViewModel);
        }
    }
}
