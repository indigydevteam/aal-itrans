﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.CustomerPaymentTransaction;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.CustomerPaymentTransactiones.Queries
{
    public class GetCustomerPaymentTransactionByCustomerPaymentHistoryQuery : IRequest<Response<IEnumerable<CustomerPaymentTransactionViewModel>>>
    {
        public int CustomerPaymentHistoryId { get; set; }
    }
    public class GetCustomerPaymentTransactionByCustomerPaymentHistoryQueryHandler : IRequestHandler<GetCustomerPaymentTransactionByCustomerPaymentHistoryQuery, Response<IEnumerable<CustomerPaymentTransactionViewModel>>>
    {
        private readonly ICustomerPaymentTransactionRepositoryAsync _customerPaymentTransactionRepository;
        private readonly IMapper _mapper;
        public GetCustomerPaymentTransactionByCustomerPaymentHistoryQueryHandler(ICustomerPaymentTransactionRepositoryAsync customerPaymentTransactionRepository, IMapper mapper)
        {
            _customerPaymentTransactionRepository = customerPaymentTransactionRepository;
            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<CustomerPaymentTransactionViewModel>>> Handle(GetCustomerPaymentTransactionByCustomerPaymentHistoryQuery request, CancellationToken cancellationToken)
        {

            try
            {
                var customerPaymentTransaction = (await _customerPaymentTransactionRepository.FindByCondition(x => x.Customer_PaymentHistoryId.Equals(request.CustomerPaymentHistoryId)).ConfigureAwait(false)).AsQueryable().ToList();
                var customerPaymentTransactionViewModel = _mapper.Map<IEnumerable<CustomerPaymentTransactionViewModel>>(customerPaymentTransaction);
                return new Response<IEnumerable<CustomerPaymentTransactionViewModel>>(customerPaymentTransactionViewModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
