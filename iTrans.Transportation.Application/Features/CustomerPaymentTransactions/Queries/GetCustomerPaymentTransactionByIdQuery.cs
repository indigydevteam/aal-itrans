﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.CustomerPaymentTransaction;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.CustomerPaymentTransactiones.Queries
{
    public class GetCustomerPaymentTransactionByIdQuery : IRequest<Response<CustomerPaymentTransactionViewModel>>
    {
        public int Id { get; set; }

    }
    public class GetCustomerPaymentTransactionByIdQueryHandler : IRequestHandler<GetCustomerPaymentTransactionByIdQuery, Response<CustomerPaymentTransactionViewModel>>
    {
        private readonly ICustomerPaymentTransactionRepositoryAsync _customerPaymentTransactionRepository;
        private readonly IMapper _mapper;
        public GetCustomerPaymentTransactionByIdQueryHandler(ICustomerPaymentTransactionRepositoryAsync customerPaymentTransactionRepository, IMapper mapper)
        {
            _customerPaymentTransactionRepository = customerPaymentTransactionRepository;
            _mapper = mapper;
        }
        public async Task<Response<CustomerPaymentTransactionViewModel>> Handle(GetCustomerPaymentTransactionByIdQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var customerObject = (await _customerPaymentTransactionRepository.FindByCondition(x => x.Id.Equals(request.Id)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                return new Response<CustomerPaymentTransactionViewModel>(_mapper.Map<CustomerPaymentTransactionViewModel>(customerObject));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
