﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.OrderingAddressProduct;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.OrderingAddressProducts.Queries
{
    public class GetOrderingAddressProductByIdQuery : IRequest<Response<OrderingAddressProductViewModel>>
    {
        public int Id { get; set; }
    }
    public class GetOrderingAddressProductByIdQueryHandler : IRequestHandler<GetOrderingAddressProductByIdQuery, Response<OrderingAddressProductViewModel>>
    {
        private readonly IOrderingAddressProductRepositoryAsync _OrderingAddressProductRepository;
        private readonly IMapper _mapper;
        public GetOrderingAddressProductByIdQueryHandler(IOrderingAddressProductRepositoryAsync OrderingAddressProductRepository, IMapper mapper)
        {
            _OrderingAddressProductRepository = OrderingAddressProductRepository;
            _mapper = mapper;
        }
        public async Task<Response<OrderingAddressProductViewModel>> Handle(GetOrderingAddressProductByIdQuery request, CancellationToken cancellationToken)
        {
            var OrderingAddressProductObject = (await _OrderingAddressProductRepository.FindByCondition(x => x.Id.Equals(request.Id)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            return new Response<OrderingAddressProductViewModel>(_mapper.Map<OrderingAddressProductViewModel>(OrderingAddressProductObject));
        }
    }
}
