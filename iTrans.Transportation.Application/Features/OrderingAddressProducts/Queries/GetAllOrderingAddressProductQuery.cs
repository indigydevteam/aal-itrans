﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.OrderingAddressProduct;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
namespace iTrans.Transportation.Application.Features.OrderingAddressProducts.Queries
{
    public class GetAllOrderingAddressProductQuery : IRequest<Response<IEnumerable<OrderingAddressProductViewModel>>>
    {

    }
    public class GetAllOrderingAddressProductQueryHandler : IRequestHandler<GetAllOrderingAddressProductQuery, Response<IEnumerable<OrderingAddressProductViewModel>>>
    {
        private readonly IOrderingAddressProductRepositoryAsync _orderingAddressProductRepository;
        private readonly IMapper _mapper;
        public GetAllOrderingAddressProductQueryHandler(IOrderingAddressProductRepositoryAsync OrderingAddressProductRepository, IMapper mapper)
        {
            _orderingAddressProductRepository = OrderingAddressProductRepository;
            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<OrderingAddressProductViewModel>>> Handle(GetAllOrderingAddressProductQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var OrderingAddressProduct = await _orderingAddressProductRepository.GetAllAsync();
                var OrderingAddressProductViewModel = _mapper.Map<IEnumerable<OrderingAddressProductViewModel>>(OrderingAddressProduct);
                return new Response<IEnumerable<OrderingAddressProductViewModel>>(OrderingAddressProductViewModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
