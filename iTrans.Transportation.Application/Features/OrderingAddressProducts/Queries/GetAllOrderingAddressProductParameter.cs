﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.OrderingAddressProduct;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;

namespace iTrans.Transportation.Application.Features.OrderingAddressProducts.Queries
{
    public class GetAllOrderingAddressProductParameter : IRequest<PagedResponse<IEnumerable<OrderingAddressProductViewModel>>>
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
    }
    public class GetAllOrderingAddressProductParameterHandler : IRequestHandler<GetAllOrderingAddressProductParameter, PagedResponse<IEnumerable<OrderingAddressProductViewModel>>>
    {
        private readonly IOrderingAddressProductRepositoryAsync _OrderingAddressProductRepository;
        private readonly IMapper _mapper;
        public GetAllOrderingAddressProductParameterHandler(IOrderingAddressProductRepositoryAsync OrderingAddressProductRepository, IMapper mapper)
        {
            _OrderingAddressProductRepository = OrderingAddressProductRepository;
            _mapper = mapper;
        }

        public async Task<PagedResponse<IEnumerable<OrderingAddressProductViewModel>>> Handle(GetAllOrderingAddressProductParameter request, CancellationToken cancellationToken)
        {
            var validFilter = _mapper.Map<GetAllOrderingAddressProductParameter>(request);
            Expression<Func<OrderingAddressProduct, bool>> expression = x => x.Name != "";
            var OrderingAddressProduct = await _OrderingAddressProductRepository.FindByConditionWithPage(expression, validFilter.PageNumber, validFilter.PageSize);
            var OrderingAddressProductViewModel = _mapper.Map<IEnumerable<OrderingAddressProductViewModel>>(OrderingAddressProduct);
            return new PagedResponse<IEnumerable<OrderingAddressProductViewModel>>(OrderingAddressProductViewModel, validFilter.PageNumber, validFilter.PageSize);
        }
    }
}