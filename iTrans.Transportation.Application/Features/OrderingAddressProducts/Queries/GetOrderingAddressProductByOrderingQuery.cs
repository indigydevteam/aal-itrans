﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.OrderingAddress;
using iTrans.Transportation.Application.DTOs.OrderingAddressProduct;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
namespace iTrans.Transportation.Application.Features.OrderingAddressProducts.Queries
{
    public class GetOrderingAddressProductByOrderingAddressQuery : IRequest<Response<IEnumerable<OrderingAddressProductViewModel>>>
    {
        public int OrderingAddressId { set; get; }
    }
    public class GetOrderingAddressProductByOrderingAddressQueryHandler : IRequestHandler<GetOrderingAddressProductByOrderingAddressQuery, Response<IEnumerable<OrderingAddressProductViewModel>>>
    {
        private readonly IOrderingAddressProductRepositoryAsync _orderingAddressProductRepository;
        private readonly IMapper _mapper;
        public GetOrderingAddressProductByOrderingAddressQueryHandler(IOrderingAddressProductRepositoryAsync orderingAddressProductRepository, IMapper mapper)
        {
            _orderingAddressProductRepository = orderingAddressProductRepository;
            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<OrderingAddressProductViewModel>>> Handle(GetOrderingAddressProductByOrderingAddressQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var OrderingAddressProduct = (await _orderingAddressProductRepository.FindByCondition(x => x.OrderingAddress.Id.Equals(request.OrderingAddressId)).ConfigureAwait(false)).AsQueryable().ToList();
                var OrderingAddressProductViewModel = _mapper.Map<IEnumerable<OrderingAddressProductViewModel>>(OrderingAddressProduct);
                return new Response<IEnumerable<OrderingAddressProductViewModel>>(OrderingAddressProductViewModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}