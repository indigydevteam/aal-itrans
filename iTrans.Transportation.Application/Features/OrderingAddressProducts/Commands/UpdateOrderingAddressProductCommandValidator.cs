﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.OrderingAddressProducts.Commands
{
    public class UpdateOrderingAddressProductCommandValidator : AbstractValidator<UpdateOrderingAddressProductCommand>
    {
        private readonly IOrderingAddressProductRepositoryAsync orderingAddressProductRepository;

        public UpdateOrderingAddressProductCommandValidator(IOrderingAddressProductRepositoryAsync orderingAddressProductRepository)
        {
            this.orderingAddressProductRepository = orderingAddressProductRepository;

            RuleFor(p => p.Id)
              .NotEmpty().WithMessage("{PropertyName} is required.")
              .NotNull()
              .MustAsync(async (p, s, cancellation) =>
              {
                  return await IsExist(p.OrderingId, p.OrderingAddressId,p.Id).ConfigureAwait(false);
              }).WithMessage("Ordering Address Product is not exists.");

            RuleFor(p => p.OrderingId)
                .NotEmpty().WithMessage("{PropertyName} is required.")
                .NotNull();


            RuleFor(p => p.OrderingAddressId)
                .NotEmpty().WithMessage("{PropertyName} is required.")
                .NotNull();

        }

        private async Task<bool> IsExist(Guid OrderingId, int OrderingAddressId, int Id)
        {

            var orderingAddressObject = (await orderingAddressProductRepository.FindByCondition(x => x.Id.Equals(Id) && x.OrderingAddress.Id.Equals(OrderingAddressId) && x.OrderingAddress.Ordering.Id.Equals(OrderingId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            if (orderingAddressObject == null)
            {
                return false;
            }
            return true;
        }
    }
}
