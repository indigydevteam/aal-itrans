﻿using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.OrderingAddressProducts.Commands
{
    public class DeleteOrderingAddressProductCommand : IRequest<Response<int>>
    {
        public int Id { get; set; }
        public Guid OrderingId { get; set; }
        public int OrderingAddressId { get; set; }
        public class DeleteOrderingAddressProductCommandHandler : IRequestHandler<DeleteOrderingAddressProductCommand, Response<int>>
        {
            private readonly IOrderingAddressProductRepositoryAsync _orderingAddressProductRepository;
            public DeleteOrderingAddressProductCommandHandler(IOrderingAddressProductRepositoryAsync orderingAddressProductRepository)
            {
                _orderingAddressProductRepository = orderingAddressProductRepository;
            }
            public async Task<Response<int>> Handle(DeleteOrderingAddressProductCommand command, CancellationToken cancellationToken)
            {
                var OrderingAddressProduct = (await _orderingAddressProductRepository.FindByCondition(x => x.Id == command.Id && x.OrderingAddress.Id.Equals(command.OrderingAddressId) && x.OrderingAddress.Ordering.Id.Equals(command.OrderingId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (OrderingAddressProduct == null)
                {
                    throw new ApiException($"OrderingAddressProduct Not Found.");
                }
                else
                {
                    await _orderingAddressProductRepository.DeleteAsync(OrderingAddressProduct);
                    return new Response<int>(OrderingAddressProduct.Id);
                }
            }
        }
    }
}