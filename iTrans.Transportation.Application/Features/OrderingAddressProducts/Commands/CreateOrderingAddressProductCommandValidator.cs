﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.OrderingAddressProducts.Commands
{
    public class CreateOrderingAddressProductCommandValidator : AbstractValidator<CreateOrderingAddressProductCommand>
    {
        private readonly IOrderingAddressRepositoryAsync orderingAddressRepository;

        public CreateOrderingAddressProductCommandValidator(IOrderingAddressRepositoryAsync orderingAddressRepository)
        {
            this.orderingAddressRepository = orderingAddressRepository;

            RuleFor(p => p.OrderingId)
                .NotEmpty().WithMessage("{PropertyName} is required.")
                .NotNull();


            RuleFor(p => p.OrderingAddressId)
                .NotEmpty().WithMessage("{PropertyName} is required.")
                .NotNull()
                .MustAsync(async (p, s, cancellation) =>
                {
                    return await IsExist(p.OrderingId, p.OrderingAddressId).ConfigureAwait(false);
                }).WithMessage("Ordering Address is not exists.");

        }

        private async Task<bool> IsExist(Guid OrderingId, int OrderingAddressId)
        {

            var customerObject = (await orderingAddressRepository.FindByCondition(x => x.Ordering.Id.Equals(OrderingId) && x.Id.Equals(OrderingAddressId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            if (customerObject == null)
            {
                return false;
            }
            return true;
        }
    }
}
