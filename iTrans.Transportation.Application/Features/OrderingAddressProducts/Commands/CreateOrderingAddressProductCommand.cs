﻿using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Interfaces.Services;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using iTrans.Transportation.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.OrderingAddressProducts.Commands
{
    public partial class CreateOrderingAddressProductCommand : IRequest<Response<int>>
    {
        public Guid OrderingId { get; set; }
        public  int OrderingAddressId { get; set; }
        public  string Name { get; set; }
        public  int ProductTypeId { get; set; }
        public  string ProductTypeDetail { get; set; }
        public  int PackagingId { get; set; }
        public  string PackagingDetail { get; set; }
        public  int Width { get; set; }
        public  int Length { get; set; }
        public  int Height { get; set; }
        public  int Weight { get; set; }
        public  int Quantity { get; set; }
        public int Sequence { get; set; }
    }
    public class CreateOrderingAddressProductCommandHandler : IRequestHandler<CreateOrderingAddressProductCommand, Response<int>>
    {
        private readonly IOrderingAddressRepositoryAsync _orderingAddressRepository;
        private readonly IOrderingAddressProductRepositoryAsync _orderingAddressProductRepository;
        private readonly IProductTypeRepositoryAsync _productTypeRepository;
        private readonly IProductPackagingRepositoryAsync _productPackagingRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;
        public CreateOrderingAddressProductCommandHandler(IOrderingAddressRepositoryAsync orderingAddressRepository, IOrderingAddressProductRepositoryAsync orderingAddressProductRepository, IProductTypeRepositoryAsync productTypeRepository
            , IProductPackagingRepositoryAsync productPackagingRepository,IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _orderingAddressRepository = orderingAddressRepository;
            _orderingAddressProductRepository = orderingAddressProductRepository;
            _productTypeRepository = productTypeRepository;
            _productPackagingRepository = productPackagingRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(CreateOrderingAddressProductCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var data = (await _orderingAddressRepository.FindByCondition(x => x.Id == request.OrderingAddressId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (data == null)
                {
                    throw new ApiException($"Ordering Address Not Found.");
                }

                List<ProductType> productTypeList = (await _productTypeRepository.FindByCondition(x => x.Active.Equals(true)).ConfigureAwait(false)).AsQueryable().ToList();
                List<ProductPackaging> packagingList = (await _productPackagingRepository.FindByCondition(x => x.Active.Equals(true)).ConfigureAwait(false)).AsQueryable().ToList();

                var OrderingAddressProduct = _mapper.Map<OrderingAddressProduct>(request);
                OrderingAddressProduct.OrderingAddress = data;
                //OrderingAddressProduct.ProductType = productTypeList.Where(p => p.Id.Equals(request.ProductTypeId)).FirstOrDefault();
                //OrderingAddressProduct.Packaging = packagingList.Where(p => p.Id.Equals(request.PackagingId)).FirstOrDefault();

                OrderingAddressProduct.Created = DateTime.UtcNow;
                OrderingAddressProduct.Modified = DateTime.UtcNow;
                //title.CreatedBy = "xxxxxxx";
                //title.ModifiedBy = "xxxxxxx";
                var OrderingAddressProductObject = await _orderingAddressProductRepository.AddAsync(OrderingAddressProduct);
                var log = new CreateAppLog(_applicationLogRepository);
                log.Create("Ordering", "Ordering AddressProduct", "Create", Newtonsoft.Json.JsonConvert.SerializeObject(OrderingAddressProduct));
                return new Response<int>(OrderingAddressProductObject.Id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}