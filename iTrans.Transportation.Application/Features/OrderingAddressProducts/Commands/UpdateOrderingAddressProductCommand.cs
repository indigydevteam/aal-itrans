﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;

namespace iTrans.Transportation.Application.Features.OrderingAddressProducts.Commands
{
    public class UpdateOrderingAddressProductCommand : IRequest<Response<int>>
    {
        public  int Id { get; set; }
        public Guid OrderingId { get; set; }
        public int OrderingAddressId { get; set; }
        public string Name { get; set; }
        public int ProductTypeId { get; set; }
        public string ProductTypeDetail { get; set; }
        public int PackagingId { get; set; }
        public string PackagingDetail { get; set; }
        public int Width { get; set; }
        public int Length { get; set; }
        public int Height { get; set; }
        public int Weight { get; set; }
        public int Quantity { get; set; }
        public int Sequence { get; set; }
    }

    public class UpdateOrderingAddressProductCommandHandler : IRequestHandler<UpdateOrderingAddressProductCommand, Response<int>>
    {
        private readonly IOrderingAddressRepositoryAsync _orderingAddressRepository;
        private readonly IOrderingAddressProductRepositoryAsync _orderingAddressProductRepository;
        private readonly IProductTypeRepositoryAsync _productTypeRepository;
        private readonly IProductPackagingRepositoryAsync _productPackagingRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;

        public UpdateOrderingAddressProductCommandHandler(IOrderingAddressRepositoryAsync orderingAddressRepository, IOrderingAddressProductRepositoryAsync orderingAddressProductRepository, IProductTypeRepositoryAsync productTypeRepository
            , IProductPackagingRepositoryAsync productPackagingRepository,IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _orderingAddressRepository = orderingAddressRepository;
            _orderingAddressProductRepository = orderingAddressProductRepository;
            _productTypeRepository = productTypeRepository;
            _productPackagingRepository = productPackagingRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(UpdateOrderingAddressProductCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var OrderingAddressProduct = (await _orderingAddressProductRepository.FindByCondition(x => x.Id == request.Id && x.OrderingAddress.Id.Equals(request.OrderingAddressId) && x.OrderingAddress.Ordering.Id.Equals(request.OrderingId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (OrderingAddressProduct == null)
                {
                    throw new ApiException($"OrderingAddressProduct Not Found.");
                }
                else
                {
                    List<ProductType> productTypeList = (await _productTypeRepository.FindByCondition(x => x.Active.Equals(true)).ConfigureAwait(false)).AsQueryable().ToList();
                    List<ProductPackaging> packagingList = (await _productPackagingRepository.FindByCondition(x => x.Active.Equals(true)).ConfigureAwait(false)).AsQueryable().ToList();

                    OrderingAddressProduct.Name = request.Name;
                    //OrderingAddressProduct.ProductType = productTypeList.Where(p => p.Id.Equals(request.ProductTypeId)).FirstOrDefault();
                    OrderingAddressProduct.ProductTypeDetail = request.ProductTypeDetail;
                    //OrderingAddressProduct.Packaging = packagingList.Where(p => p.Id.Equals(request.PackagingId)).FirstOrDefault();
                    OrderingAddressProduct.PackagingDetail = request.PackagingDetail;
                    OrderingAddressProduct.Width = request.Width;
                    OrderingAddressProduct.Length = request.Length;
                    OrderingAddressProduct.Height = request.Height;
                    OrderingAddressProduct.Weight = request.Weight;
                    OrderingAddressProduct.Quantity = request.Quantity;
                    OrderingAddressProduct.Modified = DateTime.UtcNow;

                    await _orderingAddressProductRepository.UpdateAsync(OrderingAddressProduct);
                    var log = new CreateAppLog(_applicationLogRepository);
                    log.Create("Ordering", "Ordering AddressProduct", "Edit", Newtonsoft.Json.JsonConvert.SerializeObject(OrderingAddressProduct));
                    return new Response<int>(OrderingAddressProduct.Id);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}