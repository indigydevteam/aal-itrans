﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.ProblemTopic;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.ProblemTopics.Queries
{
    public class GetProblemTopicByIdQuery : IRequest<Response<ProblemTopicViewModel>>
    {
        public int Id { get; set; }
    }
    public class GetProblemTopicByIdQueryHandler : IRequestHandler<GetProblemTopicByIdQuery, Response<ProblemTopicViewModel>>
    {
        private readonly IProblemTopicRepositoryAsync _problemTopicRepository;
        private readonly IMapper _mapper;
        public GetProblemTopicByIdQueryHandler(IProblemTopicRepositoryAsync problemTopicRepository, IMapper mapper)
        {
            _problemTopicRepository = problemTopicRepository;
            _mapper = mapper;
        }
        public async Task<Response<ProblemTopicViewModel>> Handle(GetProblemTopicByIdQuery request, CancellationToken cancellationToken)
        {
            var problemTopicObject = (await _problemTopicRepository.FindByCondition(x => x.Id.Equals(request.Id)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            return new Response<ProblemTopicViewModel>(_mapper.Map<ProblemTopicViewModel>(problemTopicObject));
        }
    }
}
