﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.ProblemTopic;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;

namespace iTrans.Transportation.Application.Features.ProblemTopics.Queries
{
    public class GetAllProblemTopicParameter : IRequest<PagedResponse<IEnumerable<ProblemTopicViewModel>>>
    {
        public string Search { set; get; }

        public int PageNumber { get; set; }
        public int PageSize { get; set; }
    }
    public class GetAllProblemTopicParameterHandler : IRequestHandler<GetAllProblemTopicParameter, PagedResponse<IEnumerable<ProblemTopicViewModel>>>
    {
        private readonly IProblemTopicRepositoryAsync _problemTopicRepository;
        private readonly IMapper _mapper;
        private Expression<Func<ProblemTopic, bool>> expression;

        public GetAllProblemTopicParameterHandler(IProblemTopicRepositoryAsync problemTopicRepository, IMapper mapper)
        {
            _problemTopicRepository = problemTopicRepository;
            _mapper = mapper;
        }

        public async Task<PagedResponse<IEnumerable<ProblemTopicViewModel>>> Handle(GetAllProblemTopicParameter request, CancellationToken cancellationToken)
        {
            var validFilter = _mapper.Map<GetAllProblemTopicParameter>(request);
            if (validFilter.Search != null && validFilter.Search.Trim() != "")
            {
                expression = x => (x.Value_TH.Contains(validFilter.Search.Trim()) || x.Value_ENG.Contains(validFilter.Search.Trim()));
            }
            int itemCount = _problemTopicRepository.GetItemCount(expression);
            //Expression<Func<ProblemTopic, bool>> expression = x => x.Name_TH != "";
            var problemTopic = await _problemTopicRepository.FindByConditionWithPage(expression, validFilter.PageNumber, validFilter.PageSize);
            var problemTopicViewModel = _mapper.Map<IEnumerable<ProblemTopicViewModel>>(problemTopic);
            return new PagedResponse<IEnumerable<ProblemTopicViewModel>>(problemTopicViewModel, validFilter.PageNumber, validFilter.PageSize);
        }
    }
}
