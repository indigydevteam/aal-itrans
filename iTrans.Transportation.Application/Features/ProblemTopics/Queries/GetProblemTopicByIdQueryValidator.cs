﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using iTrans.Transportation.Application.Interfaces.Repositories;
using FluentValidation;

namespace iTrans.Transportation.Application.Features.ProblemTopics.Queries
{
    public class GetProblemTopicByIdQueryValidator : AbstractValidator<GetProblemTopicByIdQuery>
    {
        private readonly IProblemTopicRepositoryAsync problemTopicRepository;

        public GetProblemTopicByIdQueryValidator(IProblemTopicRepositoryAsync problemTopicRepository)
        {
            this.problemTopicRepository = problemTopicRepository;
            RuleFor(p => p.Id)
               .NotEmpty().WithMessage("{PropertyName} is required.")
               .NotNull()
               .MustAsync(IsProblemTopicExists).WithMessage("{PropertyName} not exists.");
        }

        private async Task<bool> IsProblemTopicExists(int ProblemTopicId, CancellationToken cancellationToken)
        {
            var userObject = (await problemTopicRepository.FindByCondition(x => x.Id.Equals(ProblemTopicId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            if (userObject != null)
            {
                return true;
            }
            return false;
        }
    }
}
