﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.ProblemTopic;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.ProblemTopics.Queries
{
    public class GetAllProblemTopicQuery : IRequest<Response<IEnumerable<ProblemTopicViewModel>>>
    {

    }
    public class GetAllProblemTopicQueryHandler : IRequestHandler<GetAllProblemTopicQuery, Response<IEnumerable<ProblemTopicViewModel>>>
    {
        private readonly IProblemTopicRepositoryAsync _problemTopicRepository;
        private readonly IMapper _mapper;
        public GetAllProblemTopicQueryHandler(IProblemTopicRepositoryAsync problemTopicRepository, IMapper mapper)
        {
            _problemTopicRepository = problemTopicRepository;
            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<ProblemTopicViewModel>>> Handle(GetAllProblemTopicQuery request, CancellationToken cancellationToken)
        {
            var problemTopic = (await _problemTopicRepository.FindByCondition(x => x.Active == true).ConfigureAwait(false)).AsQueryable().ToList();
            var problemTopicViewModel = _mapper.Map<IEnumerable<ProblemTopicViewModel>>(problemTopic);
            return new Response<IEnumerable<ProblemTopicViewModel>>(problemTopicViewModel);
        }
    }
}
