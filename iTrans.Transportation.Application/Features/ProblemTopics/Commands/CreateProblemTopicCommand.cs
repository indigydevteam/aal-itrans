﻿using AutoMapper;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Interfaces.Services;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.ProblemTopics.Commands
{
    public partial class CreateProblemTopicCommand : IRequest<Response<int>>
    {
        public string Module { get; set; }
        public string Value_TH { get; set; }
        public string Value_ENG { get; set; }
        public int Sequence { get; set; }
        public bool Active { get; set; }

    }
    public class CreateProblemTopicCommandHandler : IRequestHandler<CreateProblemTopicCommand, Response<int>>
    {
        private readonly IProblemTopicRepositoryAsync _problemTopicRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;
        public CreateProblemTopicCommandHandler(IProblemTopicRepositoryAsync problemTopicRepository, IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _problemTopicRepository = problemTopicRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(CreateProblemTopicCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var problemTopic = _mapper.Map<ProblemTopic>(request);
                problemTopic.Created = DateTime.UtcNow;
                problemTopic.Modified = DateTime.UtcNow;
                //title.CreatedBy = "xxxxxxx";
                //title.ModifiedBy = "xxxxxxx";
                var problemTopicObject = await _problemTopicRepository.AddAsync(problemTopic);
                var log = new CreateAppLog(_applicationLogRepository);
                log.Create("ProblemTopics", "ProblemTopics", "Create", Newtonsoft.Json.JsonConvert.SerializeObject(request));
                return new Response<int>(problemTopicObject.Id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
