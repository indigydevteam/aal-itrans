﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;


namespace iTrans.Transportation.Application.Features.ProblemTopics.Commands
{
    public class UpdateProblemTopicCommandValidator : AbstractValidator<UpdateProblemTopicCommand>
    {
        private readonly IProblemTopicRepositoryAsync problemTopicRepository;
        public UpdateProblemTopicCommandValidator(IProblemTopicRepositoryAsync problemTopicRepository)
        {
            this.problemTopicRepository = problemTopicRepository;

            RuleFor(p => p.Value_TH)
               .NotEmpty().WithMessage("{PropertyName} is required.")
               .NotNull()
               .MaximumLength(50).WithMessage("{PropertyName} must not exceed 50 characters.");


            RuleFor(p => p.Value_ENG)
                .NotEmpty().WithMessage("{PropertyName} is required.")
                .NotNull()
                .MaximumLength(50).WithMessage("{PropertyName} must not exceed 50 characters.");
        }
    }
}
