﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;

namespace iTrans.Transportation.Application.Features.ProblemTopics.Commands
{
    public class UpdateProblemTopicCommand : IRequest<Response<int>>
    {
        public int Id { get; set; }
        public string Module { get; set; }
        public string Value_TH { get; set; }
        public string Value_ENG { get; set; }
        public int Sequence { get; set; }
        public bool Active { get; set; }

    }

    public class UpdateProblemTopicCommandHandler : IRequestHandler<UpdateProblemTopicCommand, Response<int>>
    {
        private readonly IProblemTopicRepositoryAsync _problemTopicRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;

        public UpdateProblemTopicCommandHandler(IProblemTopicRepositoryAsync problemTopicRepository, IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _problemTopicRepository = problemTopicRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(UpdateProblemTopicCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var problemTopic = (await _problemTopicRepository.FindByCondition(x => x.Id == request.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (problemTopic == null)
                {
                    throw new ApiException($"ProblemTopic Not Found.");
                }
                else
                {
                    problemTopic.Value_TH = request.Value_TH;
                    problemTopic.Value_ENG = request.Value_ENG;
                    problemTopic.Sequence = request.Sequence;
                    problemTopic.Active = request.Active;
                    problemTopic.Module = request.Module;
                    problemTopic.Modified = DateTime.UtcNow;

                    await _problemTopicRepository.UpdateAsync(problemTopic);
                    var log = new CreateAppLog(_applicationLogRepository);
                    log.Create("ProblemTopics", "ProblemTopics", "Create", Newtonsoft.Json.JsonConvert.SerializeObject(request));
                    return new Response<int>(problemTopic.Id);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
