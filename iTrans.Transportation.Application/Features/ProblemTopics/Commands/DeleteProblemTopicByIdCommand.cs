﻿using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.ProblemTopics.Commands
{
    public class DeleteProblemTopicByIdCommand : IRequest<Response<int>>
    {
        public int Id { get; set; }
        public class DeleteProblemTopicByIdCommandHandler : IRequestHandler<DeleteProblemTopicByIdCommand, Response<int>>
        {
            private readonly IProblemTopicRepositoryAsync _problemTopicRepository;
            public DeleteProblemTopicByIdCommandHandler(IProblemTopicRepositoryAsync problemTopicRepository)
            {
                _problemTopicRepository = problemTopicRepository;
            }
            public async Task<Response<int>> Handle(DeleteProblemTopicByIdCommand command, CancellationToken cancellationToken)
            {
                var problemTopic = (await _problemTopicRepository.FindByCondition(x => x.Id == command.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (problemTopic == null)
                {
                    throw new ApiException($"ProblemTopic Not Found.");
                }
                else
                {
                    await _problemTopicRepository.DeleteAsync(problemTopic);
                    return new Response<int>(problemTopic.Id);
                }
            }
        }
    }
}
