﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.DriverAddresses.Commands
{
    public class CreateDriverAddressCommandValidator : AbstractValidator<CreateDriverAddressCommand>
    {
        private readonly IDriverAddressRepositoryAsync driverAddressRepository;
        private readonly IDriverRepositoryAsync driverRepository;

        public CreateDriverAddressCommandValidator(IDriverAddressRepositoryAsync driverAddressRepository, IDriverRepositoryAsync driverRepository)
        {
            this.driverAddressRepository = driverAddressRepository;
            this.driverRepository = driverRepository;

            RuleFor(p => p.DriverId)
                .NotNull().WithMessage("{PropertyName} is required.")
                .MustAsync(IsExistDriver).WithMessage("{PropertyName} no exists.");

            RuleFor(p => p.PostCode)
               .NotEmpty().WithMessage("{PropertyName} is required.")
               .NotNull()
               .MaximumLength(250).WithMessage("{PropertyName} must not exceed 250 characters.");
        }

        private async Task<bool> IsExistDriver(Guid value, CancellationToken cancellationToken)
        {
            var customerObject = (await driverRepository.FindByCondition(x => x.Id.Equals(value)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            if (customerObject == null)
            {
                return false;
            }
            return true;
        }

        private async Task<bool> IsUnique(string value, CancellationToken cancellationToken)
        {
            var customerAddressObject = (await driverAddressRepository.FindByCondition(x => x.AddressName.ToLower() == value.ToLower()).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            if (customerAddressObject != null)
            {
                return false;
            }
            return true;
        }
    }
}
