﻿using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.DriverAddresses.Commands
{
    public class DeleteDriverAddressByIdCommand : IRequest<Response<int>>
    {
        public Guid? UserId { get; set; }
        public int Id { get; set; }
        public Guid DriverId { get; set; }
        public class DeleteDriverAddressByIdCommandHandler : IRequestHandler<DeleteDriverAddressByIdCommand, Response<int>>
        {
            private readonly IDriverAddressRepositoryAsync _driverAddressRepository;
            private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
            public DeleteDriverAddressByIdCommandHandler(IDriverAddressRepositoryAsync driverAddressRepository, IApplicationLogRepositoryAsync applicationLogRepository)
            {
                _driverAddressRepository = driverAddressRepository;
                _applicationLogRepository = applicationLogRepository;
            }
            public async Task<Response<int>> Handle(DeleteDriverAddressByIdCommand command, CancellationToken cancellationToken)
            {
                var driverAddress = (await _driverAddressRepository.FindByCondition(x => x.Id == command.Id && x.DriverId.Equals(command.DriverId) && (x.AddressType != "register" || x.AddressType != "register-tax")).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (driverAddress == null)
                {
                    throw new ApiException($"DriverAddress Not Found.");
                }
                else
                {
                    await _driverAddressRepository.DeleteAsync(driverAddress);
                    if (driverAddress.IsMainData)
                    {
                        var address = (await _driverAddressRepository.FindByCondition(x => x.DriverId == command.DriverId && x.AddressType.Contains(driverAddress.AddressType)).ConfigureAwait(false)).OrderByDescending(x => x.Created).AsQueryable().FirstOrDefault();
                        if (address != null)
                        {
                            address.IsMainData = true;
                            await _driverAddressRepository.UpdateAsync(address);
                        }
                    }
                    var log = new CreateAppLog(_applicationLogRepository);
                    log.CreateLog("Driver", "Driver Addresses", "Edit", Newtonsoft.Json.JsonConvert.SerializeObject(command), command.UserId != null ? command.UserId.ToString() : "");
                    return new Response<int>(driverAddress.Id);
                }
            }
        }
    }
}
