﻿using AutoMapper;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Interfaces.Services;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.DriverAddresses.Commands
{
    public partial class CreateDriverAddressCommand : IRequest<Response<int>>
    {
        public Guid? UserId { get; set; }
        public Guid DriverId { get; set; }
        public int CountryId { get; set; }
        public int ProvinceId { get; set; }
        public int DistrictId { get; set; }
        public int SubdistrictId { get; set; }
        public string PostCode { get; set; }
        public string Road { get; set; }
        public string Alley { get; set; }
        public string Address { get; set; }
        public string Branch { get; set; }
        public string AddressType { get; set; }
        public string AddressName { get; set; }
        public string ContactPerson { get; set; }
        public string ContactPhoneNumber { get; set; }
        public string ContactEmail { get; set; }
        public string Maps { get; set; }
        public bool IsMainData { get; set; }
        public int Sequence { get; set; }
        public string CountryCode { get; set; }
    }
    public class CreateDriverAddressCommandHandler : IRequestHandler<CreateDriverAddressCommand, Response<int>>
    {
        private readonly IDriverAddressRepositoryAsync _driverAddressRepository;
        private readonly ICountryRepositoryAsync _countryRepository;
        private readonly IProvinceRepositoryAsync _provinceRepository;
        private readonly IDistrictRepositoryAsync _districtRepository;
        private readonly ISubdistrictRepositoryAsync _subdistrictRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;
        public CreateDriverAddressCommandHandler(IDriverAddressRepositoryAsync driverAddressRepository, ICountryRepositoryAsync countryRepository, IProvinceRepositoryAsync provinceRepository, IDistrictRepositoryAsync districtRepository, ISubdistrictRepositoryAsync subdistrictRepository,IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _driverAddressRepository = driverAddressRepository;
            _countryRepository = countryRepository;
            _provinceRepository = provinceRepository;
            _districtRepository = districtRepository;
            _subdistrictRepository = subdistrictRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(CreateDriverAddressCommand request, CancellationToken cancellationToken)
        {
            var driverAddress = _mapper.Map<DriverAddress>(request);
            Country country = (await _countryRepository.FindByCondition(x => x.Id.Equals(request.CountryId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            Province province = (await _provinceRepository.FindByCondition(x => x.Id.Equals(request.ProvinceId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            District district = (await _districtRepository.FindByCondition(x => x.Id.Equals(request.DistrictId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            Subdistrict subdistrict = (await _subdistrictRepository.FindByCondition(x => x.Id.Equals(request.SubdistrictId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();

            string phoneNumber = request.ContactPhoneNumber != null ? request.ContactPhoneNumber.Trim() : "";
            if (request.ContactPhoneNumber != null && request.ContactPhoneNumber.Trim().Length == 10 && request.ContactPhoneNumber.Trim().First() == '0')
            {
                phoneNumber = request.ContactPhoneNumber.Trim().Remove(0, 1);

            }
            driverAddress.Country = country;
            driverAddress.Province = province;
            driverAddress.District = district;
            driverAddress.Subdistrict = subdistrict;
            driverAddress.PostCode = request.PostCode;
            driverAddress.Road = request.Road;
            driverAddress.Alley = request.Alley;
            driverAddress.Address = request.Address;
            driverAddress.Branch = request.Branch;
            driverAddress.AddressType = request.AddressType;
            driverAddress.AddressName = request.AddressName;
            driverAddress.ContactPerson = request.ContactPerson;
            driverAddress.ContactPhoneCode = request.CountryCode;
            driverAddress.ContactPhoneNumber = phoneNumber;
            driverAddress.ContactEmail = request.ContactEmail;
            driverAddress.Maps = request.Maps;
            driverAddress.IsMainData = request.IsMainData;
            driverAddress.Sequence = request.Sequence;
            driverAddress.Created = DateTime.UtcNow;
            driverAddress.Modified = DateTime.UtcNow;
            driverAddress.CreatedBy = request.UserId != null ? request.UserId.ToString() : "";
            driverAddress.ModifiedBy = request.UserId != null ? request.UserId.ToString() : "";

            if (driverAddress.IsMainData)
            {
                (await _driverAddressRepository.CreateSQLQuery("UPDATE Driver_Address SET IsMainData  = 0 where DriverId = '" + driverAddress.DriverId.ToString() + "' and AddressType like '%" + driverAddress.AddressType.Trim() + "%' ").ConfigureAwait(false)).ExecuteUpdate();
            }
            var DriverAddressObject = await _driverAddressRepository.AddAsync(driverAddress);
            var log = new CreateAppLog(_applicationLogRepository);
            log.CreateLog("Driver", "Driver Addresses", "Create", Newtonsoft.Json.JsonConvert.SerializeObject(request), request.UserId != null ? request.UserId.ToString() : "");
            return new Response<int>(DriverAddressObject.Id);
        }
    }
}
