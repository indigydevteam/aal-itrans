﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.DriverAddresses.Commands
{
    public class UpdateDriverAddressCommandValidator : AbstractValidator<UpdateDriverAddressCommand>
    {
        private readonly IDriverAddressRepositoryAsync driverAddressRepository;
        public UpdateDriverAddressCommandValidator(IDriverAddressRepositoryAsync driverAddressRepository)
        {
            this.driverAddressRepository = driverAddressRepository;

            RuleFor(p => p.DriverId)
                .NotNull().WithMessage("{PropertyName} is required.");


            RuleFor(p => p.PostCode)
               .NotEmpty().WithMessage("{PropertyName} is required.")
               .NotNull()
               .MaximumLength(250).WithMessage("{PropertyName} must not exceed 250 characters.");
        }
    }
}
