﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;

namespace iTrans.Transportation.Application.Features.DriverAddresses.Commands
{
    public class UpdateDriverAddressCommand : IRequest<Response<int>>
    {
        public Guid? UserId { get; set; }
        public int Id { get; set; }
        public Guid DriverId { get; set; }
        public int CountryId { get; set; }
        public int ProvinceId { get; set; }
        public int DistrictId { get; set; }
        public int SubdistrictId { get; set; }
        public string PostCode { get; set; }
        public string Road { get; set; }
        public string Alley { get; set; }
        public string Address { get; set; }
        public string Branch { get; set; }
        public string AddressType { get; set; }
        public string AddressName { get; set; }
        public string ContactPerson { get; set; }
        public string ContactPhoneNumber { get; set; }
        public string ContactEmail { get; set; }
        public string Maps { get; set; }
        public bool IsMainData { get; set; }
        public int Sequence { get; set; }
    }

    public class UpdateDriverAddressCommandHandler : IRequestHandler<UpdateDriverAddressCommand, Response<int>>
    {
        private readonly IDriverAddressRepositoryAsync _driverAddressRepository;
        private readonly ICountryRepositoryAsync _countryRepository;
        private readonly IProvinceRepositoryAsync _provinceRepository;
        private readonly IDistrictRepositoryAsync _districtRepository;
        private readonly ISubdistrictRepositoryAsync _subdistrictRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;

        public UpdateDriverAddressCommandHandler(IDriverAddressRepositoryAsync driverAddressRepository, ICountryRepositoryAsync countryRepository, IProvinceRepositoryAsync provinceRepository, IDistrictRepositoryAsync districtRepository, ISubdistrictRepositoryAsync subdistrictRepository, IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _driverAddressRepository = driverAddressRepository;
            _countryRepository = countryRepository;
            _provinceRepository = provinceRepository;
            _districtRepository = districtRepository;
            _subdistrictRepository = subdistrictRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(UpdateDriverAddressCommand request, CancellationToken cancellationToken)
        {
            var driverAddress = (await _driverAddressRepository.FindByCondition(x => x.Id == request.Id && x.DriverId.Equals(request.DriverId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            if (driverAddress == null)
            {
                throw new ApiException($"DriverAddress Not Found.");
            }
            else
            {
                if (request.IsMainData)
                {
                    (await _driverAddressRepository.CreateSQLQuery("UPDATE Driver_Address SET IsMainData  = 0 where DriverId = '" + request.DriverId.ToString() + "' and AddressType like '%" + request.AddressType.Trim() + "%' ").ConfigureAwait(false)).ExecuteUpdate();
                }
                Country country = (await _countryRepository.FindByCondition(x => x.Id.Equals(request.CountryId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                Province province = (await _provinceRepository.FindByCondition(x => x.Id.Equals(request.ProvinceId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                District district = (await _districtRepository.FindByCondition(x => x.Id.Equals(request.DistrictId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                Subdistrict subdistrict = (await _subdistrictRepository.FindByCondition(x => x.Id.Equals(request.SubdistrictId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();

                driverAddress.DriverId = request.DriverId;
                driverAddress.Country = country;
                driverAddress.Province = province;
                driverAddress.District = district;
                driverAddress.Subdistrict = subdistrict;

                string phoneNumber = request.ContactPhoneNumber != null ? request.ContactPhoneNumber.Trim() : "";
                if (request.ContactPhoneNumber != null && request.ContactPhoneNumber.Trim().Length == 10 && request.ContactPhoneNumber.Trim().First() == '0')
                {
                    phoneNumber = request.ContactPhoneNumber.Trim().Remove(0, 1);

                }

                if (request.PostCode != null)
                {
                    driverAddress.PostCode = request.PostCode;
                }
                if (request.Road != null)
                {
                    driverAddress.Road = request.Road;
                }
                if (request.Alley != null)
                {
                    driverAddress.Alley = request.Alley;
                }
                if (request.Address != null)
                {
                    driverAddress.Address = request.Address;
                }
                if (request.Branch != null)
                {
                    driverAddress.Branch = request.Branch;
                }
                if (request.AddressType != null)
                {
                    if (driverAddress.AddressType != null && driverAddress.AddressType.Contains("register"))
                    {

                    }
                    else
                        driverAddress.AddressType = request.AddressType;
                }
                if (request.AddressName != null)
                {
                    driverAddress.AddressName = request.AddressName;
                }
                if (request.ContactPerson != null)
                {
                    driverAddress.ContactPerson = request.ContactPerson;
                }
                driverAddress.ContactPhoneNumber = phoneNumber;

                if (request.ContactEmail != null)
                {
                    driverAddress.ContactEmail = request.ContactEmail;
                }
                if (request.Maps != null)
                {
                    driverAddress.Maps = request.Maps;
                }
                driverAddress.Sequence = request.Sequence;
                driverAddress.IsMainData = request.IsMainData;
                driverAddress.Modified = DateTime.UtcNow;
                driverAddress.ModifiedBy = request.UserId != null ? request.UserId.ToString() : "";
                await _driverAddressRepository.UpdateAsync(driverAddress);

                var log = new CreateAppLog(_applicationLogRepository);
                log.CreateLog("Driver", "Driver Addresses", "Edit", Newtonsoft.Json.JsonConvert.SerializeObject(request), request.UserId != null ? request.UserId.ToString() : "");
                return new Response<int>(driverAddress.Id);
            }
        }
    }
}
