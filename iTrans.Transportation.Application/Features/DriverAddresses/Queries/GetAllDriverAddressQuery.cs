﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.DriverAddress;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.DriverAddresses.Queries
{
    public class GetAllDriverAddressQuery : IRequest<Response<IEnumerable<DriverAddressViewModel>>>
    {
        
    }
    public class GetAllDriverAddressQueryHandler : IRequestHandler<GetAllDriverAddressQuery, Response<IEnumerable<DriverAddressViewModel>>>
    {
        private readonly IDriverAddressRepositoryAsync _driverAddressRepository;
        private readonly IMapper _mapper;
        public GetAllDriverAddressQueryHandler(IDriverAddressRepositoryAsync driverAddressRepository, IMapper mapper)
        {
            _driverAddressRepository = driverAddressRepository;
            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<DriverAddressViewModel>>> Handle(GetAllDriverAddressQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var validFilter = _mapper.Map<GetAllDriverAddressQuery>(request);
                var driver = await _driverAddressRepository.GetAllAsync();
                var driverAddressViewModel = _mapper.Map<IEnumerable<DriverAddressViewModel>>(driver);
                return new Response<IEnumerable<DriverAddressViewModel>>(driverAddressViewModel);
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
    }
}
