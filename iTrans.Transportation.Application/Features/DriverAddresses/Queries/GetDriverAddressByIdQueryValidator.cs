﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using iTrans.Transportation.Application.Interfaces.Repositories;
using FluentValidation;

namespace iTrans.Transportation.Application.Features.DriverAddresses.Queries
{
    public class GetDriverAddressByIdQueryValidator : AbstractValidator<GetDriverAddressByIdQuery>
    {
        private readonly IDriverAddressRepositoryAsync driverAddressRepository;

        public GetDriverAddressByIdQueryValidator(IDriverAddressRepositoryAsync driverAddressRepository)
        {
            this.driverAddressRepository = driverAddressRepository;
            RuleFor(p => p.Id)
               .NotEmpty().WithMessage("{PropertyName} is required.")
               .NotNull()
               .MustAsync(IsDriverAddressExists).WithMessage("{PropertyName} not exists.");
        }

        private async Task<bool> IsDriverAddressExists(int DriverId, CancellationToken cancellationToken)
        {
            var userObject = (await driverAddressRepository.FindByCondition(x => x.Id.Equals(DriverId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            if (userObject != null)
            {
                return true;
            }
            return false;
        }
    }
}
