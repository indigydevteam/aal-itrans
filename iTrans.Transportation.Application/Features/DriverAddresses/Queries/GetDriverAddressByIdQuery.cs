﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.DriverAddress;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.DriverAddresses.Queries
{
    public class GetDriverAddressByIdQuery : IRequest<Response<DriverAddressViewModel>>
    {
        public int Id { get; set; }
        public Guid DriverId { get; set; }
    }
    public class GetDriverAddressByIdQueryHandler : IRequestHandler<GetDriverAddressByIdQuery, Response<DriverAddressViewModel>>
    {
        private readonly IDriverAddressRepositoryAsync _driverAddressRepository;
        private readonly IMapper _mapper;
        public GetDriverAddressByIdQueryHandler(IDriverAddressRepositoryAsync DriverAddressRepository, IMapper mapper)
        {
            _driverAddressRepository = DriverAddressRepository;
            _mapper = mapper;
        }
        public async Task<Response<DriverAddressViewModel>> Handle(GetDriverAddressByIdQuery request, CancellationToken cancellationToken)
        {
            var DriverObject = (await _driverAddressRepository.FindByCondition(x => x.Id.Equals(request.Id) && x.DriverId.Equals(request.DriverId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            return new Response<DriverAddressViewModel>(_mapper.Map<DriverAddressViewModel>(DriverObject));
        }
    }
}
