﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.DriverAddress;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using LinqKit;
using MediatR;

namespace iTrans.Transportation.Application.Features.DriverAddresses.Queries
{
    public class GetDriverAddressByDriverQuery : IRequest<Response<IEnumerable<DriverAddressViewModel>>>
    {
        public Guid DriverId { get; set; }
        public string Search { set; get; }
        public int PageNumber { set; get; }
        public int PageSize { set; get; }
    }
    public class GetDriverAddressByDriverQueryHandler : IRequestHandler<GetDriverAddressByDriverQuery, Response<IEnumerable<DriverAddressViewModel>>>
    {
        private readonly IDriverAddressRepositoryAsync _driverAddressRepository;
        private readonly IMapper _mapper;
        public GetDriverAddressByDriverQueryHandler(IDriverAddressRepositoryAsync driverAddressRepository, IMapper mapper)
        {
            _driverAddressRepository = driverAddressRepository;
            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<DriverAddressViewModel>>> Handle(GetDriverAddressByDriverQuery request, CancellationToken cancellationToken)
        {
            try
            {
                Expression<Func<Domain.DriverAddress, bool>> expression = PredicateBuilder.New<Domain.DriverAddress>(false);
                expression = x => x.DriverId.Equals(request.DriverId);
                if (request.Search != null && request.Search.Trim() != "")
                {
                    string search = request.Search.Trim();
                    expression = expression.And(a => (a.Country.Name_TH.Contains(search) || a.Country.Name_ENG.Contains(search)) ||
                                                     (a.Province.Name_TH.Contains(search) || a.Province.Name_ENG.Contains(search)) ||
                                                     (a.District.Name_TH.Contains(search) || a.District.Name_ENG.Contains(search)) ||
                                                     (a.Subdistrict.Name_TH.Contains(search) || a.Subdistrict.Name_ENG.Contains(search)) ||
                                                     a.PostCode.Contains(search) || a.Road.Contains(search) || a.Alley.Contains(search) ||
                                                     a.Address.Contains(search) || a.ContactPerson.Contains(search) || a.ContactPhoneNumber.Contains(search) ||
                                                     a.ContactEmail.Contains(search)
                                    );
                }

                var driverAddress = await _driverAddressRepository.FindByConditionWithPage(expression, request.PageNumber, request.PageSize);

                //var driverAddress = (await _driverAddressRepository.FindByCondition(x => x.DriverId.Equals(request.DriverId)).ConfigureAwait(false)).AsQueryable().ToList();
                var driverAddressViewModel = _mapper.Map<IEnumerable<DriverAddressViewModel>>(driverAddress);
                return new Response<IEnumerable<DriverAddressViewModel>>(driverAddressViewModel);
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
    }
}
