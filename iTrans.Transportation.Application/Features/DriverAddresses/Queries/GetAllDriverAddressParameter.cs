﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.DriverAddress;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;

namespace iTrans.Transportation.Application.Features.DriverAddresses.Queries
{
    public class GetAllDriverAddressParameter : IRequest<PagedResponse<IEnumerable<DriverAddressViewModel>>>
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
    }
    public class GetAllDriverAddressParameterHandler : IRequestHandler<GetAllDriverAddressParameter, PagedResponse<IEnumerable<DriverAddressViewModel>>>
    {
        private readonly IDriverAddressRepositoryAsync _driverAddressRepository;
        private readonly IMapper _mapper;
        public GetAllDriverAddressParameterHandler(IDriverAddressRepositoryAsync driverAddressRepository, IMapper mapper)
        {
            _driverAddressRepository = driverAddressRepository;
            _mapper = mapper;
        }

        public async Task<PagedResponse<IEnumerable<DriverAddressViewModel>>> Handle(GetAllDriverAddressParameter request, CancellationToken cancellationToken)
        {
            var validFilter = _mapper.Map<GetAllDriverAddressParameter>(request);
            Expression<Func<DriverAddress, bool>> expression = x => x.DriverId != null;
            var driverAddress = await _driverAddressRepository.FindByConditionWithPage(expression, validFilter.PageNumber, validFilter.PageSize);
            var driverAddressViewModel = _mapper.Map<IEnumerable<DriverAddressViewModel>>(driverAddress);
            return new PagedResponse<IEnumerable<DriverAddressViewModel>>(driverAddressViewModel, validFilter.PageNumber, validFilter.PageSize);
        }
    }
}
