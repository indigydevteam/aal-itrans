﻿using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
namespace iTrans.Transportation.Application.Features.DriverPaymentHistories.Commands
{
    public class DeleteDriverPaymentHistoryCommand : IRequest<Response<int>>
    {
        public int Id { get; set; }
        public int PaymentId { get; set; }
        public class DeleteDriverPaymentHistoryCommandHandler : IRequestHandler<DeleteDriverPaymentHistoryCommand, Response<int>>
        {
            private readonly IDriverPaymentHistoryRepositoryAsync _DriverPaymentHistoryRepository;
            public DeleteDriverPaymentHistoryCommandHandler(IDriverPaymentHistoryRepositoryAsync DriverPaymentHistoryRepository)
            {
                _DriverPaymentHistoryRepository = DriverPaymentHistoryRepository;
            }
            public async Task<Response<int>> Handle(DeleteDriverPaymentHistoryCommand command, CancellationToken cancellationToken)
            {
                var DriverPaymentHistory = (await _DriverPaymentHistoryRepository.FindByCondition(x => x.Id == command.Id && x.Payment.Id.Equals(command.PaymentId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (DriverPaymentHistory == null)
                {
                    throw new ApiException($"Payment History Not Found.");
                }
                else
                {
                    await _DriverPaymentHistoryRepository.DeleteAsync(DriverPaymentHistory);
                    return new Response<int>(DriverPaymentHistory.Id);
                }
            }
        }
    }
}
