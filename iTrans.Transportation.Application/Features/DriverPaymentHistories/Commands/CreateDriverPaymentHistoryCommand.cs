﻿using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Interfaces.Services;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using iTrans.Transportation.Domain.Entities;
using MediatR;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.DriverPaymentHistories.Commands
{
    public partial class CreateDriverPaymentHistoryCommand : IRequest<Response<int>>
    {

        public Guid DriverId { get; set; }
        public int PaymentId { get; set; }
        public string Detail { get; set; }

    }
    public class CreateDriverPaymentHistoryCommandHandler : IRequestHandler<CreateDriverPaymentHistoryCommand, Response<int>>
    {
        private readonly IDriverPaymentRepositoryAsync _driverPaymentRepository;
        private readonly IDriverPaymentHistoryRepositoryAsync _driverPaymentHistoryRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;
        public CreateDriverPaymentHistoryCommandHandler(IDriverPaymentRepositoryAsync driverPaymentRepository, IDriverPaymentHistoryRepositoryAsync driverPaymentHistoryRepository
            , IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _driverPaymentRepository = driverPaymentRepository;
            _driverPaymentHistoryRepository = driverPaymentHistoryRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(CreateDriverPaymentHistoryCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var payment = (await _driverPaymentRepository.FindByCondition(x => x.Id.Equals(request.PaymentId) && x.Driver.Id.Equals(request.DriverId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (payment == null)
                {
                    throw new ApiException($"Playment Not Found.");
                }
                var driverPaymentHistory = _mapper.Map<DriverPaymentHistory>(request);
                driverPaymentHistory.Payment = payment;
                driverPaymentHistory.Created = DateTime.UtcNow;
                driverPaymentHistory.Modified = DateTime.UtcNow;
                var DriverPaymentHistoryObject = await _driverPaymentHistoryRepository.AddAsync(driverPaymentHistory);
                var log = new CreateAppLog(_applicationLogRepository);
                log.Create("Driver", "Driver Paymenthistory", "Create", Newtonsoft.Json.JsonConvert.SerializeObject(request));
                return new Response<int>(DriverPaymentHistoryObject.Id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
