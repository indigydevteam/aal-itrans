﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;

namespace iTrans.Transportation.Application.Features.DriverPaymentHistories.Commands
{
    public class UpdateDriverPaymentHistoryCommand : IRequest<Response<int>>
    {
        public int Id { get; set; }
        public int PaymentId { get; set; }
        public string Detail { get; set; }
    }
    public class UpdateDriverPaymentHistoryCommandHandler : IRequestHandler<UpdateDriverPaymentHistoryCommand, Response<int>>
    {
        private readonly IDriverPaymentHistoryRepositoryAsync _driverPaymentHistoryRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;

        public UpdateDriverPaymentHistoryCommandHandler(IDriverPaymentHistoryRepositoryAsync driverPaymentHistoryRepository,IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _driverPaymentHistoryRepository = driverPaymentHistoryRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(UpdateDriverPaymentHistoryCommand request, CancellationToken cancellationToken)
        {
            var DriverPaymentHistory = (await _driverPaymentHistoryRepository.FindByCondition(x => x.Id == request.Id && x.Payment.Id.Equals(request.PaymentId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            if (DriverPaymentHistory == null)
            {
                throw new ApiException($"Payment History Not Found.");
            }
            else
            {
               // DriverPaymentHistory.DriverId = request.DriverId;
                DriverPaymentHistory.Detail = request.Detail;
                DriverPaymentHistory.Modified = DateTime.UtcNow;
                await _driverPaymentHistoryRepository.UpdateAsync(DriverPaymentHistory);
                var log = new CreateAppLog(_applicationLogRepository);
                log.Create("Driver", "Driver Paymenthistory", "Edit", Newtonsoft.Json.JsonConvert.SerializeObject(request));
                return new Response<int>(DriverPaymentHistory.Id);
            }
        }
    }
}
