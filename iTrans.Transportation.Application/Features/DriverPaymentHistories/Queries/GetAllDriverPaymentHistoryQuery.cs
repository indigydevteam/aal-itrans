﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.DriverPaymentHistory;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.DriverPaymentHistorys.Queries
{
    public class GetAllDriverPaymentHistoryQuery : IRequest<Response<IEnumerable<DriverPaymentHistoryViewModel>>>
    {

    }
    public class GetAllDriverPaymentHistoryQueryHandler : IRequestHandler<GetAllDriverPaymentHistoryQuery, Response<IEnumerable<DriverPaymentHistoryViewModel>>>
    {
        private readonly IDriverPaymentHistoryRepositoryAsync _DriverPaymentHistoryRepository;
        private readonly IMapper _mapper;
        public GetAllDriverPaymentHistoryQueryHandler(IDriverPaymentHistoryRepositoryAsync DriverPaymentHistoryRepository, IMapper mapper)
        {
            _DriverPaymentHistoryRepository = DriverPaymentHistoryRepository;
            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<DriverPaymentHistoryViewModel>>> Handle(GetAllDriverPaymentHistoryQuery request, CancellationToken cancellationToken)
        {
            var Driver = await _DriverPaymentHistoryRepository.GetAllAsync();
            var DriverPaymentHistoryViewModel = _mapper.Map<IEnumerable<DriverPaymentHistoryViewModel>>(Driver);
            return new Response<IEnumerable<DriverPaymentHistoryViewModel>>(DriverPaymentHistoryViewModel);
        }
    }
}
