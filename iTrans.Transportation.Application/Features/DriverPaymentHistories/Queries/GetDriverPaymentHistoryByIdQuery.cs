﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.DriverPaymentHistory;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.DriverPaymentHistoryes.Queries
{
    public class GetDriverPaymentHistoryByIdQuery : IRequest<Response<DriverPaymentHistoryViewModel>>
    {
        public int PaymentId { get; set; }
        public int Id { get; set; }

    }
    public class GetDriverPaymentHistoryByIdQueryHandler : IRequestHandler<GetDriverPaymentHistoryByIdQuery, Response<DriverPaymentHistoryViewModel>>
    {
        private readonly IDriverPaymentHistoryRepositoryAsync _driverPaymentHistoryRepository;
        private readonly IMapper _mapper;
        public GetDriverPaymentHistoryByIdQueryHandler(IDriverPaymentHistoryRepositoryAsync driverPaymentHistoryRepository, IMapper mapper)
        {
            _driverPaymentHistoryRepository = driverPaymentHistoryRepository;
            _mapper = mapper;
        }
        public async Task<Response<DriverPaymentHistoryViewModel>> Handle(GetDriverPaymentHistoryByIdQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var driverObject = (await _driverPaymentHistoryRepository.FindByCondition(x => x.Id.Equals(request.Id) && x.Payment.Id.Equals(request.PaymentId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                return new Response<DriverPaymentHistoryViewModel>(_mapper.Map<DriverPaymentHistoryViewModel>(driverObject));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
