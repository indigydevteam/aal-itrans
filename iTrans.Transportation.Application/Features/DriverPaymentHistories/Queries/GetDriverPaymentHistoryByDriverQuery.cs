﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.DriverPaymentHistory;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.DriverPaymentHistoryes.Queries
{
    public class GetDriverPaymentHistoryByDriverQuery : IRequest<Response<IEnumerable<DriverPaymentHistoryViewModel>>>
    {
        public Guid DriverId { get; set; }
    }
    public class GetDriverPaymentHistoryByDriverQueryHandler : IRequestHandler<GetDriverPaymentHistoryByDriverQuery, Response<IEnumerable<DriverPaymentHistoryViewModel>>>
    {
        private readonly IDriverPaymentHistoryRepositoryAsync _driverPaymentHistoryRepository;
        private readonly IMapper _mapper;
        public GetDriverPaymentHistoryByDriverQueryHandler(IDriverPaymentHistoryRepositoryAsync driverPaymentHistoryRepository, IMapper mapper)
        {
            _driverPaymentHistoryRepository = driverPaymentHistoryRepository;
            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<DriverPaymentHistoryViewModel>>> Handle(GetDriverPaymentHistoryByDriverQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var driverPaymentHistory = (await _driverPaymentHistoryRepository.FindByCondition(x => x.Payment.Driver.Id.Equals(request.DriverId)).ConfigureAwait(false)).AsQueryable().ToList();
                var driverPaymentHistoryViewModel = _mapper.Map<IEnumerable<DriverPaymentHistoryViewModel>>(driverPaymentHistory);
                return new Response<IEnumerable<DriverPaymentHistoryViewModel>>(driverPaymentHistoryViewModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
