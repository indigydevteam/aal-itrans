﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;

namespace iTrans.Transportation.Application.Features.CorporateTypes.Backoffice.Commands
{
    public class UpdateCorporateTypeCommand : IRequest<Response<int>>
    {
        public Guid? UserId { get; set; }
        public int Id { get; set; }
        public string Name_TH { get; set; }
        public string Name_ENG { get; set; }
        public int Sequence { get; set; }
        public bool Active { get; set; }
        public bool Specified { get; set; }

    }

    public class UpdateCorporateTypeCommandHandler : IRequestHandler<UpdateCorporateTypeCommand, Response<int>>
    {
        private readonly ICorporateTypeRepositoryAsync _corporateTypeRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly ISystemUserRepositoryAsync _userRepository;
        private readonly IMapper _mapper;

        public UpdateCorporateTypeCommandHandler(ICorporateTypeRepositoryAsync corporateTypeRepository, IApplicationLogRepositoryAsync applicationLogRepository, ISystemUserRepositoryAsync userRepository, IMapper mapper)
        {
            _corporateTypeRepository = corporateTypeRepository;
            _applicationLogRepository = applicationLogRepository;
            _userRepository = userRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(UpdateCorporateTypeCommand request, CancellationToken cancellationToken)
        {

            try
            {
                var corporateType = (await _corporateTypeRepository.FindByCondition(x => x.Id == request.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (corporateType == null)
                {
                    throw new ApiException($"CorporateType Not Found.");
                }
                else
                {
                    corporateType.Name_TH = request.Name_TH;
                    corporateType.Name_ENG = request.Name_ENG;
                    corporateType.Sequence = request.Sequence;
                    corporateType.Active = request.Active;
                    corporateType.Specified = request.Specified;
                    corporateType.Modified = DateTime.UtcNow;

                    await _corporateTypeRepository.UpdateAsync(corporateType);
                    var log = new CreateAppLog(_applicationLogRepository);
                    //var nameUser = _userRepository.GetAllAsync().Result.Where(u => u.EmplID.Equals(request.UserId)).Select(u => u.MiddleNameEN).FirstOrDefault();
                    log.CreateLog("CorporateType", "CorporateType", "Create", Newtonsoft.Json.JsonConvert.SerializeObject(corporateType), request.UserId.ToString());
                    return new Response<int>(corporateType.Id);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
