﻿using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.CorporateTypes.Backoffice.Commands
{
    public class DeleteCorporateTypeByIdCommand : IRequest<Response<int>>
    {
        public Guid? UserId { get; set; }
        public int Id { get; set; }
        public class DeleteCorporateTypeByIdCommandHandler : IRequestHandler<DeleteCorporateTypeByIdCommand, Response<int>>
        {
            private readonly ICorporateTypeRepositoryAsync _corporateTypeRepository;
            private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
            public DeleteCorporateTypeByIdCommandHandler(ICorporateTypeRepositoryAsync corporateTypeRepository, IApplicationLogRepositoryAsync applicationLogRepository)
            {
                _corporateTypeRepository = corporateTypeRepository;
                _applicationLogRepository = applicationLogRepository;
            }
            public async Task<Response<int>> Handle(DeleteCorporateTypeByIdCommand command, CancellationToken cancellationToken)
            {
                var corporateType = (await _corporateTypeRepository.FindByCondition(x => x.Id == command.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (corporateType == null)
                {
                    throw new ApiException($"CorporateType Not Found.");
                }
                else
                {
                    try
                    {
                        corporateType.Active = false;
                        corporateType.IsDelete = true;
                        corporateType.Modified = DateTime.UtcNow;
                        corporateType.ModifiedBy = command.UserId != null ? command.UserId.GetValueOrDefault().ToString() : "";
                        await _corporateTypeRepository.UpdateAsync(corporateType);
                        var log = new CreateAppLog(_applicationLogRepository);
                        log.CreateLog("CorporateType", "CorporateType", "Delete", Newtonsoft.Json.JsonConvert.SerializeObject(command), command.UserId == null ? "-" : command.UserId.Value.ToString());
                        return new Response<int>(corporateType.Id);
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
            }
        }
    }
}
