﻿using AutoMapper;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Interfaces.Services;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.CorporateTypes.Backoffice.Commands
{
    public partial class CreateCorporateTypeCommand : IRequest<Response<int>>
    {
        public Guid? UserId { get; set; }
        public string Name_TH { get; set; }
        public string Name_ENG { get; set; }
        public int Sequence { get; set; }
        public bool Active { get; set; }
        public  bool Specified { get; set; }

    }
    public class CreateCorporateTypeCommandHandler : IRequestHandler<CreateCorporateTypeCommand, Response<int>>
    {
        private readonly ICorporateTypeRepositoryAsync _corporateTypeRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly ISystemUserRepositoryAsync _userRepository;

        private readonly IMapper _mapper;
        public CreateCorporateTypeCommandHandler(ICorporateTypeRepositoryAsync corporateTypeRepository, IApplicationLogRepositoryAsync applicationLogRepository, ISystemUserRepositoryAsync userRepository, IMapper mapper)
        {
            _corporateTypeRepository = corporateTypeRepository;
            _applicationLogRepository = applicationLogRepository;
            _userRepository = userRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(CreateCorporateTypeCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var corporateType = _mapper.Map<CorporateType>(request);
                corporateType.Created = DateTime.UtcNow;
                corporateType.Modified = DateTime.UtcNow;
         
                var corporateTypeObject = await _corporateTypeRepository.AddAsync(corporateType);
                var log = new CreateAppLog(_applicationLogRepository);
                //var nameUser = _userRepository.GetAllAsync().Result.Where(u => u.EmplID.Equals(request.UserId)).Select(u => u.MiddleNameEN).FirstOrDefault();
                log.CreateLog("CorporateType", "CorporateType", "Create", Newtonsoft.Json.JsonConvert.SerializeObject(request), request.UserId.ToString());
                return new Response<int>(corporateTypeObject.Id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
