﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.CorporateType;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.CorporateTypes.Backoffice.Queries
{
    public class GetAllCorporateTypeQuery : IRequest<Response<IEnumerable<CorporateTypeViewModel>>>
    {

    }
    public class GetAllCorporateTypeQueryHandler : IRequestHandler<GetAllCorporateTypeQuery, Response<IEnumerable<CorporateTypeViewModel>>>
    {
        private readonly ICorporateTypeRepositoryAsync _corporateTypeRepository;
        private readonly IMapper _mapper;
        public GetAllCorporateTypeQueryHandler(ICorporateTypeRepositoryAsync corporateTypeRepository, IMapper mapper)
        {
            _corporateTypeRepository = corporateTypeRepository;
            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<CorporateTypeViewModel>>> Handle(GetAllCorporateTypeQuery request, CancellationToken cancellationToken)
        {
            var corporateType = (await _corporateTypeRepository.FindByCondition(x => x.Active == true).ConfigureAwait(false)).AsQueryable().ToList();
            var corporateTypeViewModel = _mapper.Map<IEnumerable<CorporateTypeViewModel>>(corporateType);
            return new Response<IEnumerable<CorporateTypeViewModel>>(corporateTypeViewModel);
        }
    }
}
