﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.CorporateType;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.CorporateTypes.Backoffice.Queries
{
    public class GetCorporateTypeByIdQuery : IRequest<Response<CorporateTypeViewModel>>
    {
        public int Id { get; set; }
    }
    public class GetCorporateTypeByIdQueryHandler : IRequestHandler<GetCorporateTypeByIdQuery, Response<CorporateTypeViewModel>>
    {
        private readonly ICorporateTypeRepositoryAsync _corporateTypeRepository;
        private readonly IMapper _mapper;
        public GetCorporateTypeByIdQueryHandler(ICorporateTypeRepositoryAsync corporateTypeRepository, IMapper mapper)
        {
            _corporateTypeRepository = corporateTypeRepository;
            _mapper = mapper;
        }
        public async Task<Response<CorporateTypeViewModel>> Handle(GetCorporateTypeByIdQuery request, CancellationToken cancellationToken)
        {
            var corporateTypeObject = (await _corporateTypeRepository.FindByCondition(x => x.Id.Equals(request.Id)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            return new Response<CorporateTypeViewModel>(_mapper.Map<CorporateTypeViewModel>(corporateTypeObject));
        }
    }
}
