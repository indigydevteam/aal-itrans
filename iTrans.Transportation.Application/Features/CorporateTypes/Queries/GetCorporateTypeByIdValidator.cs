﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using iTrans.Transportation.Application.Interfaces.Repositories;
using FluentValidation;

namespace iTrans.Transportation.Application.Features.CorporateTypes.Queries
{
    public class GetCorporateTypeByIdValidator : AbstractValidator<GetCorporateTypeById>
    {
        private readonly ICorporateTypeRepositoryAsync corporateTypeRepository;

        public GetCorporateTypeByIdValidator(ICorporateTypeRepositoryAsync corporateTypeRepository)
        {
            this.corporateTypeRepository = corporateTypeRepository;
            RuleFor(p => p.Id)
               .NotEmpty().WithMessage("{PropertyName} is required.")
               .NotNull()
               .MustAsync(IsCorporateTypeExists).WithMessage("{PropertyName} not exists.");
        }

        private async Task<bool> IsCorporateTypeExists(int CorporateTypeId, CancellationToken cancellationToken)
        {
            var userObject = (await corporateTypeRepository.FindByCondition(x => x.Id.Equals(CorporateTypeId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            if (userObject != null)
            {
                return true;
            }
            return false;
        }
    }
}
