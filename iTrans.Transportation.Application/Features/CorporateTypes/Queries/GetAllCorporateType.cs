﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.CorporateType;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.CorporateTypes.Queries
{
    public class GetAllCorporateType : IRequest<Response<IEnumerable<CorporateTypeViewModel>>>
    {

    }
    public class GetAllCorporateTypeHandler : IRequestHandler<GetAllCorporateType, Response<IEnumerable<CorporateTypeViewModel>>>
    {
        private readonly ICorporateTypeRepositoryAsync _corporateTypeRepository;
        private readonly IMapper _mapper;
        public GetAllCorporateTypeHandler(ICorporateTypeRepositoryAsync corporateTypeRepository, IMapper mapper)
        {
            _corporateTypeRepository = corporateTypeRepository;
            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<CorporateTypeViewModel>>> Handle(GetAllCorporateType request, CancellationToken cancellationToken)
        {
            var corporateType = (await _corporateTypeRepository.FindByCondition(x => x.Active == true).ConfigureAwait(false)).AsQueryable().ToList();
            var corporateTypeViewModel = _mapper.Map<IEnumerable<CorporateTypeViewModel>>(corporateType);
            return new Response<IEnumerable<CorporateTypeViewModel>>(corporateTypeViewModel);
        }
    }
}
