﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.CorporateType;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;

namespace iTrans.Transportation.Application.Features.CorporateTypes.Queries
{
    public class GetAllCorporateTypeParameter : IRequest<PagedResponse<IEnumerable<CorporateTypeViewModel>>>
    {
        public string Search { set; get; }

        public int PageNumber { get; set; }
        public int PageSize { get; set; }
    }
    public class GetAllCorporateTypeParameterHandler : IRequestHandler<GetAllCorporateTypeParameter, PagedResponse<IEnumerable<CorporateTypeViewModel>>>
    {
        private readonly ICorporateTypeRepositoryAsync _corporateTypeRepository;
        private readonly IMapper _mapper;
        private Expression<Func<CorporateType, bool>> expression;

        public GetAllCorporateTypeParameterHandler(ICorporateTypeRepositoryAsync corporateTypeRepository, IMapper mapper)
        {
            _corporateTypeRepository = corporateTypeRepository;
            _mapper = mapper;
        }

        public async Task<PagedResponse<IEnumerable<CorporateTypeViewModel>>> Handle(GetAllCorporateTypeParameter request, CancellationToken cancellationToken)
        {
            var validFilter = _mapper.Map<GetAllCorporateTypeParameter>(request);
            if (validFilter.Search != null && validFilter.Search.Trim() != "")
            {
                expression = x => (x.Name_ENG.Contains(validFilter.Search.Trim()) || x.Name_TH.Contains(validFilter.Search.Trim()) || x.Sequence.ToString().Contains(validFilter.Search.Trim()));
            }
            int itemCount = _corporateTypeRepository.GetItemCount(expression);
            //Expression<Func<CorporateType, bool>> expression = x => x.Name_TH != "";
            var corporateType = await _corporateTypeRepository.FindByConditionWithPage(expression, validFilter.PageNumber, validFilter.PageSize);
            var corporateTypeViewModel = _mapper.Map<IEnumerable<CorporateTypeViewModel>>(corporateType);
            return new PagedResponse<IEnumerable<CorporateTypeViewModel>>(corporateTypeViewModel, validFilter.PageNumber, validFilter.PageSize, itemCount);
        }
    }
}
