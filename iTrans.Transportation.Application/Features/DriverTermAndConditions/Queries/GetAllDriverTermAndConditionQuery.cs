﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.DriverTermAndCondition;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.DriverTermAndConditions.Queries
{
    public class GetAllDriverTermAndConditionQuery : IRequest<Response<IEnumerable<DriverTermAndConditionViewModel>>>
    {

    }
    public class GetAllDriverTermAndConditionQueryHandler : IRequestHandler<GetAllDriverTermAndConditionQuery, Response<IEnumerable<DriverTermAndConditionViewModel>>>
    {
        private readonly IDriverTermAndConditionRepositoryAsync _driverTermAndConditionRepository;
        private readonly IMapper _mapper;
        public GetAllDriverTermAndConditionQueryHandler(IDriverTermAndConditionRepositoryAsync driverTermAndConditionRepository, IMapper mapper)
        {
            _driverTermAndConditionRepository = driverTermAndConditionRepository;
            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<DriverTermAndConditionViewModel>>> Handle(GetAllDriverTermAndConditionQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var validFilter = _mapper.Map<GetAllDriverTermAndConditionQuery>(request);
                var driver = await _driverTermAndConditionRepository.GetAllAsync();
                var driverTermAndConditionViewModel = _mapper.Map<IEnumerable<DriverTermAndConditionViewModel>>(driver);
                return new Response<IEnumerable<DriverTermAndConditionViewModel>>(driverTermAndConditionViewModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
