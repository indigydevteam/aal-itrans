﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.DriverTermAndCondition;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.DriverTermAndConditions.Queries
{
    public class GetDriverTermAndConditionByIdQuery : IRequest<Response<DriverTermAndConditionViewModel>>
    {
        public Guid DriverId { get; set; }
        public int Id { get; set; }
    }
    public class GetDriverTermAndConditionByIdQueryHandler : IRequestHandler<GetDriverTermAndConditionByIdQuery, Response<DriverTermAndConditionViewModel>>
    {
        private readonly IDriverTermAndConditionRepositoryAsync _driverTermAndConditionRepository;
        private readonly IMapper _mapper;
        public GetDriverTermAndConditionByIdQueryHandler(IDriverTermAndConditionRepositoryAsync driverTermAndConditionRepository, IMapper mapper)
        {
            _driverTermAndConditionRepository = driverTermAndConditionRepository;
            _mapper = mapper;
        }
        public async Task<Response<DriverTermAndConditionViewModel>> Handle(GetDriverTermAndConditionByIdQuery request, CancellationToken cancellationToken)
        {
            var driverObject = (await _driverTermAndConditionRepository.FindByCondition(x => x.Id.Equals(request.Id) && x.Driver.Id.Equals(request.DriverId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            return new Response<DriverTermAndConditionViewModel>(_mapper.Map<DriverTermAndConditionViewModel>(driverObject));
        }
    }
}
