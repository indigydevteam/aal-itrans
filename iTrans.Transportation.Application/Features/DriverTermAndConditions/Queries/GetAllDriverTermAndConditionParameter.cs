﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.DriverTermAndCondition;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using iTrans.Transportation.Domain.Entities;
using MediatR;

namespace iTrans.Transportation.Application.Features.DriverTermAndConditions.Queries
{
    public class GetAllDriverTermAndConditionParameter : IRequest<PagedResponse<IEnumerable<DriverTermAndConditionViewModel>>>
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
    }
    public class GetAllDriverTermAndConditionParameterHandler : IRequestHandler<GetAllDriverTermAndConditionParameter, PagedResponse<IEnumerable<DriverTermAndConditionViewModel>>>
    {
        private readonly IDriverTermAndConditionRepositoryAsync _driverTermAndConditionRepository;
        private readonly IMapper _mapper;
        public GetAllDriverTermAndConditionParameterHandler(IDriverTermAndConditionRepositoryAsync driverTermAndConditionRepository, IMapper mapper)
        {
            _driverTermAndConditionRepository = driverTermAndConditionRepository;
            _mapper = mapper;
        }

        public async Task<PagedResponse<IEnumerable<DriverTermAndConditionViewModel>>> Handle(GetAllDriverTermAndConditionParameter request, CancellationToken cancellationToken)
        {
            var validFilter = _mapper.Map<GetAllDriverTermAndConditionParameter>(request);
            Expression<Func<DriverTermAndCondition, bool>> expression = x => x.Driver.Id != null;
            var driverTermAndCondition = await _driverTermAndConditionRepository.FindByConditionWithPage(expression, validFilter.PageNumber, validFilter.PageSize);
            var driverTermAndConditionViewModel = _mapper.Map<IEnumerable<DriverTermAndConditionViewModel>>(driverTermAndCondition);
            return new PagedResponse<IEnumerable<DriverTermAndConditionViewModel>>(driverTermAndConditionViewModel, validFilter.PageNumber, validFilter.PageSize);
        }
    }
}
