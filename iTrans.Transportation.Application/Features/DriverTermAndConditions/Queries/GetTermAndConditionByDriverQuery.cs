﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.DriverTermAndCondition;
using iTrans.Transportation.Application.DTOs.TermAndCondition;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using iTrans.Transportation.Domain.Entities;
using MediatR;

namespace iTrans.Transportation.Application.Features.DriverTermAndConditions.Queries
{
    public class GetTermAndConditionByDriverQuery : IRequest<Response<IEnumerable<DriverTermAndConditionViewModel>>>
    {
        public Guid DriverId { get; set; }
    }
    public class GetTermAndConditionByDriverQueryHandler : IRequestHandler<GetTermAndConditionByDriverQuery, Response<IEnumerable<DriverTermAndConditionViewModel>>>
    {
        private readonly IDriverTermAndConditionRepositoryAsync _driverTermAndConditionRepository;
        private readonly ITermAndConditionRepositoryAsync _termAndConditionRepository;
        private readonly IMapper _mapper;
        public GetTermAndConditionByDriverQueryHandler(IDriverTermAndConditionRepositoryAsync driverTermAndConditionRepository, ITermAndConditionRepositoryAsync termAndConditionRepository, IMapper mapper)
        {
            _driverTermAndConditionRepository = driverTermAndConditionRepository;
            _termAndConditionRepository = termAndConditionRepository;
            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<DriverTermAndConditionViewModel>>> Handle(GetTermAndConditionByDriverQuery request, CancellationToken cancellationToken)
        {
            try
            {
                //var data = (await _termAndConditionRepository.FindByCondition(x => x.Active == true && x.Module.Trim().ToLower() == "driver").ConfigureAwait(false)).AsQueryable().ToList().OrderBy(x => x.Sequence).OrderBy(x => x.Version).OrderBy(x => x.Modified);
                //List<TermAndCondition> termAndConditions = new List<TermAndCondition>();
                //foreach (TermAndCondition termAndCondition in data)
                //{
                //    var existItem = termAndConditions.Where(x => x.Section == termAndCondition.Section).FirstOrDefault();
                //    if (existItem == null)
                //    {
                //        termAndConditions.Add(termAndCondition);
                //    }
                //    else
                //    {
                //        int n;
                //        var version1 = new Version(int.TryParse(termAndCondition.Version, out n) ? termAndCondition.Version + ".0" : termAndCondition.Version);
                //        var version2 = new Version(int.TryParse(existItem.Version, out n) ? existItem.Version + ".0" : existItem.Version);
                //        var result = version1.CompareTo(version2);
                //        if (result > 0)
                //        {
                //            termAndConditions.Remove(existItem);
                //            termAndConditions.Add(termAndCondition);
                //        }
                //    }
                //};
                //List<TermAndCondition> nonAcceptAermAndConditions = new List<TermAndCondition>();
                //List<DriverTermAndCondition> driverTermAndConditions = new List<DriverTermAndCondition>();
                //foreach (TermAndCondition item in termAndConditions)
                //{
                //    var driverTermAndCondition = (await _driverTermAndConditionRepository.FindByCondition(x => x.Driver.Id.Equals(request.DriverId) && x.Section == item.Section, x => x.Created, true)
                //        .ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                //    if (driverTermAndCondition != null)
                //    {
                //        driverTermAndConditions.Add(driverTermAndCondition);
                //        if (!driverTermAndCondition.Version.Contains("."))
                //        {
                //            driverTermAndCondition.Version = driverTermAndCondition.Version + ".0";
                //        }
                //        if (!item.Version.Contains("."))
                //        {
                //            item.Version = item.Version + ".0";
                //        }
                //        Version customerTermAndConditionVersion = new Version(driverTermAndCondition.Version);
                //        Version TermAndConditionVersion = new Version(item.Version);
                //        if (new Version(driverTermAndCondition.Version) < new Version(item.Version))
                //        {
                //            nonAcceptAermAndConditions.Add(item);
                //        }
                //        else if (new Version(driverTermAndCondition.Version) == new Version(item.Version) && !driverTermAndCondition.IsUserAccept)
                //        {
                //            nonAcceptAermAndConditions.Add(item);
                //        }
                //    }
                //    else
                //    {
                //        nonAcceptAermAndConditions.Add(item);
                //    }
                //}
                //AcceptTermAndConditionViewModel resultTermAndCondition = new AcceptTermAndConditionViewModel();
                //resultTermAndCondition.termAndCondition = _mapper.Map<List<DriverTermAndConditionViewModel>>(driverTermAndConditions);
                //resultTermAndCondition.nonAcceptTermAndCondition = _mapper.Map<List<TermAndConditionViewModel>>(nonAcceptAermAndConditions);
                //return new Response<AcceptTermAndConditionViewModel>(resultTermAndCondition);

                //var validFilter = _mapper.Map<GetTermAndConditionByDriverQuery>(request);
                //var driver =  (await _driverTermAndConditionRepository.FindByCondition(x => x.Driver.Id.Equals(request.DriverId)).ConfigureAwait(false)).AsQueryable().ToList();
                //var driverTermAndConditionViewModel = _mapper.Map<IEnumerable<DriverTermAndConditionViewModel>>(driver);
                //return new Response<IEnumerable<DriverTermAndConditionViewModel>>(driverTermAndConditionViewModel);

                var data = (await _termAndConditionRepository.FindByCondition(x => x.Active == true && x.Module.Trim().ToLower() == "driver").ConfigureAwait(false)).AsQueryable().ToList().OrderBy(x => x.Sequence).OrderBy(x => x.Version).OrderBy(x => x.Modified);
                List<TermAndCondition> termAndConditions = new List<TermAndCondition>();
                foreach (TermAndCondition termAndCondition in data)
                {
                    var existItem = termAndConditions.Where(x => x.Section == termAndCondition.Section).FirstOrDefault();
                    if (existItem == null)
                    {
                        termAndConditions.Add(termAndCondition);
                    }
                    else
                    {
                        int n;
                        var version1 = new Version(int.TryParse(termAndCondition.Version, out n) ? termAndCondition.Version + ".0" : termAndCondition.Version);
                        var version2 = new Version(int.TryParse(existItem.Version, out n) ? existItem.Version + ".0" : existItem.Version);
                        var result = version1.CompareTo(version2);
                        if (result > 0)
                        {
                            termAndConditions.Remove(existItem);
                            termAndConditions.Add(termAndCondition);
                        }
                    }
                };

                List<DriverTermAndConditionViewModel> driverTermAndConditions = new List<DriverTermAndConditionViewModel>();
                foreach (TermAndCondition item in termAndConditions)
                {
                    var driverTermAndCondition = (await _driverTermAndConditionRepository.FindByCondition(x => x.Driver.Id.Equals(request.DriverId) && x.Section == item.Section && x.Version == item.Version)
                        .ConfigureAwait(false)).OrderByDescending(x => x.Created).AsQueryable().FirstOrDefault();
                    if (driverTermAndCondition == null)
                    {
                        DriverTermAndConditionViewModel itemTermAndCondition = new DriverTermAndConditionViewModel()
                        {
                            id = 0,
                            termAndConditionId = item.Id,
                            name_TH = item.Name_TH,
                            name_ENG = item.Name_ENG,
                            section = item.Section,
                            version = item.Version,
                            isAccept = item.IsAccept,
                            isUserAccept = false,
                            isComplete = false
                        };
                        driverTermAndConditions.Add(itemTermAndCondition);
                    }
                    else
                    {
                        var itemTermAndCondition = _mapper.Map<DriverTermAndConditionViewModel>(driverTermAndCondition);
                        itemTermAndCondition.isComplete = true;
                        driverTermAndConditions.Add(itemTermAndCondition);
                    }
                }
                return new Response<IEnumerable<DriverTermAndConditionViewModel>>(driverTermAndConditions);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
