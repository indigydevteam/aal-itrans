﻿using AutoMapper;
using iTrans.Transportation.Application.DTOs.Registers;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using iTrans.Transportation.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.DriverTermAndConditions.Commands
{
    public partial class CreateDriverTermAndConditionCommand : IRequest<Response<Guid>>
    {
        public  Guid DriverId { get; set; }
        public List<CreateTermAndConditionViewModel> TermAndConditions { get; set; }
    }
    public class CreateDriverTermAndConditionCommandHandler : IRequestHandler<CreateDriverTermAndConditionCommand, Response<Guid>>
    {
        private readonly IDriverRepositoryAsync _driverRepository;
        private readonly IDriverTermAndConditionRepositoryAsync _driverTermAndConditionRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;
        public CreateDriverTermAndConditionCommandHandler(IDriverRepositoryAsync driverRepository,IDriverTermAndConditionRepositoryAsync driverTermAndConditionRepository,IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _driverRepository = driverRepository;
            _driverTermAndConditionRepository = driverTermAndConditionRepository;
            _applicationLogRepository = applicationLogRepository; 
            _mapper = mapper;
        }

        public async Task<Response<Guid>> Handle(CreateDriverTermAndConditionCommand request, CancellationToken cancellationToken)
        {
            var driverObject = (await _driverRepository.FindByCondition(x => x.Id.Equals(request.DriverId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            if (driverObject == null)
            {
                throw new ApiException($"Driver Not Found.");
            }
            if (request.TermAndConditions != null)
            {
                foreach (CreateTermAndConditionViewModel termAndConditionViewModel in request.TermAndConditions)
                {
                    DriverTermAndCondition termAndCondition = new DriverTermAndCondition
                    {
                        Driver = driverObject,
                        TermAndConditionId = termAndConditionViewModel.TermAndConditionId > 0 ? termAndConditionViewModel.TermAndConditionId : termAndConditionViewModel.Id,
                        Name_TH = termAndConditionViewModel.Name_TH,
                        Name_ENG = termAndConditionViewModel.Name_ENG,
                        Section = termAndConditionViewModel.Section,
                        Version = termAndConditionViewModel.Version,
                        IsAccept = termAndConditionViewModel.IsAccept,
                        IsUserAccept = termAndConditionViewModel.IsUserAccept,
                        CreatedBy = request.DriverId != null ? request.DriverId.ToString() : "-",
                        Created = DateTime.Now,
                        ModifiedBy = request.DriverId != null ? request.DriverId.ToString() : "-",
                        Modified = DateTime.Now
                    };
                    var dataListObject = await _driverTermAndConditionRepository.AddAsync(termAndCondition);

                }
                var log = new CreateAppLog(_applicationLogRepository);
                log.Create("Driver", "Driver TermAndConditions", "Create", Newtonsoft.Json.JsonConvert.SerializeObject(request));
                return new Response<Guid>(driverObject.Id);
            }
            else
            {
                throw new ApiException($"TermAndConditions Not null.");
            }
        }
    }
}