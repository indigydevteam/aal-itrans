﻿using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.DriverTermAndConditions.Commands
{
    public class CancelDriverTermAndConditionCommand : IRequest<Response<bool>>
    {
        public Guid DriverId { get; set; }
        public string Section { get; set; }
        public string Version { get; set; }

        public class CancelDriverTermAndConditionCommandHandler : IRequestHandler<CancelDriverTermAndConditionCommand, Response<bool>>
        {
            private readonly IDriverTermAndConditionRepositoryAsync _driverTermAndConditionRepository;
            public CancelDriverTermAndConditionCommandHandler(IDriverTermAndConditionRepositoryAsync driverTermAndConditionRepository)
            {
                _driverTermAndConditionRepository = driverTermAndConditionRepository;
            }
            public async Task<Response<bool>> Handle(CancelDriverTermAndConditionCommand command, CancellationToken cancellationToken)
            {

                (await _driverTermAndConditionRepository.CreateSQLQuery("DELETE Driver_TermAndCondition where DriverId = '" + command.DriverId.ToString() + "' and Section = '"+ command.Section + "' and Version = '" + command.Version + "'").ConfigureAwait(false)).ExecuteUpdate(); ;
                return new Response<bool>(true);

            }
        }
    }
}
