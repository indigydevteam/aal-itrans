﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;

namespace iTrans.Transportation.Application.Features.DriverTermAndConditions.Commands
{
    public class UpdateDriverTermAndConditionCommand : IRequest<Response<int>>
    {
        public  int Id { get; set; }
        public  Guid DriverId { get; set; }
        public  string TermAndCondition { get; set; }
    }

    public class UpdateDriverTermAndConditionCommandHandler : IRequestHandler<UpdateDriverTermAndConditionCommand, Response<int>>
    {
        private readonly IDriverTermAndConditionRepositoryAsync _driverTermAndConditionRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;

        public UpdateDriverTermAndConditionCommandHandler(IDriverTermAndConditionRepositoryAsync driverTermAndConditionRepository,IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _driverTermAndConditionRepository = driverTermAndConditionRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(UpdateDriverTermAndConditionCommand request, CancellationToken cancellationToken)
        {
            var driverTermAndCondition = (await _driverTermAndConditionRepository.FindByCondition(x => x.Id == request.Id && x.Driver.Id.Equals(request.DriverId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            if (driverTermAndCondition == null)
            {
                throw new ApiException($"DriverTermAndCondition Not Found.");
            }
            else
            {
             
                driverTermAndCondition.Driver.Id = request.DriverId;
                //driverTermAndCondition.TermAndCondition = request.TermAndCondition;
                driverTermAndCondition.Modified =DateTime.UtcNow;
         
                await _driverTermAndConditionRepository.UpdateAsync(driverTermAndCondition);
                var log = new CreateAppLog(_applicationLogRepository);
                log.Create("Driver", "Driver TermAndcCondition", "Edit", Newtonsoft.Json.JsonConvert.SerializeObject(driverTermAndCondition));
                return new Response<int>(driverTermAndCondition.Id);
            }
        }
    }
}
