﻿using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.DriverTermAndConditions.Commands
{
    public class DeleteDriverTermAndConditionCommand : IRequest<Response<int>>
    {
        public int Id { get; set; }

        public class DeleteDriverTermAndConditionCommandHandler : IRequestHandler<DeleteDriverTermAndConditionCommand, Response<int>>
        {
            private readonly IDriverTermAndConditionRepositoryAsync _driverTermAndConditionRepository;
            public DeleteDriverTermAndConditionCommandHandler(IDriverTermAndConditionRepositoryAsync driverTermAndConditionRepository)
            {
                _driverTermAndConditionRepository = driverTermAndConditionRepository;
            }
            public async Task<Response<int>> Handle(DeleteDriverTermAndConditionCommand command, CancellationToken cancellationToken)
            {
                var driverTermAndCondition = (await _driverTermAndConditionRepository.FindByCondition(x => x.Id == command.Id ).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (driverTermAndCondition == null)
                {
                    throw new ApiException($"DriverTermAndCondition Not Found.");
                }
                else
                {
                    await _driverTermAndConditionRepository.DeleteAsync(driverTermAndCondition);
                    return new Response<int>(driverTermAndCondition.Id);
                }
            }
        }
    }
}
