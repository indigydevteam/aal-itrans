﻿using AutoMapper;
using iTrans.Transportation.Application.DTOs.CarList;
using iTrans.Transportation.Application.DTOs.UserLevel;
using iTrans.Transportation.Application.Features.CarLists.Queries;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using iTrans.Transportation.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.UserLevels.Backoffice
{
    public class GetAllUserLevel : IRequest<PagedResponse<IEnumerable<UserLevelViewModelConditionWithPage>>>
    {
        public string Search { set; get; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public int Active { set; get; }
        public string Column { set; get; }
    }
    public class GetAllUserLevelHandler : IRequestHandler<GetAllUserLevel, PagedResponse<IEnumerable<UserLevelViewModelConditionWithPage>>>
    {
        private readonly IUserLevelRepositoryAsync _userLevelRepository;
        private readonly IMapper _mapper;

        public GetAllUserLevelHandler(IUserLevelRepositoryAsync userLevelRepository, IMapper mapper)
        {
            _userLevelRepository = userLevelRepository;
            _mapper = mapper;
        }

        public async Task<PagedResponse<IEnumerable<UserLevelViewModelConditionWithPage>>> Handle(GetAllUserLevel request, CancellationToken cancellationToken)
        {
            var validFilter = _mapper.Map<GetAllUserLevel>(request);
            var result = from x in _userLevelRepository.GetAllAsync().Result.ToList()
                         select new UserLevelViewModelConditionWithPage
                         {
                             id = x.Id,
                             module = x.Module != null ? x.Module:"-",
                             level = x.Level.ToString() != null ? x.Level : 0,
                             name_TH = x.Name_TH != null ? x.Name_TH : "-",
                             name_ENG = x.Name_ENG != null ? x.Name_ENG : "-",
                             active = x.Active

                         };
            if (validFilter.Search != null && validFilter.Search.Trim() != "")
            {
                result = result.Where(x => x.module.Contains(validFilter.Search.Trim()) || x.level.Equals(validFilter.Search.Trim()) || x.name_TH.Contains(validFilter.Search.Trim())
                         || x.name_ENG.Contains(validFilter.Search.Trim())).ToList();
            }
            if (request.Column == "module" && request.Active == 3)
            {
                result = result.OrderBy(x => x.module);
            }
            if (request.Column == "module" && request.Active == 2)
            {
                result = result.OrderByDescending(x => x.module);
            }
            if (request.Column == "level" && request.Active == 3)
            {
                result = result.OrderBy(x => x.level);
            }
            if (request.Column == "level" && request.Active == 2)
            {
                result = result.OrderByDescending(x => x.level);
            }
            if (request.Column == "name_TH" && request.Active == 3)
            {
                result = result.OrderBy(x => x.name_TH);
            }
            if (request.Column == "name_TH" && request.Active == 2)
            {
                result = result.OrderByDescending(x => x.name_TH);
            }
            if (request.Column == "name_ENG" && request.Active == 3)
            {
                result = result.OrderBy(x => x.name_ENG);
            }
            if (request.Column == "name_ENG" && request.Active == 2)
            {
                result = result.OrderByDescending(x => x.name_ENG);
            }
            
            var itemCount = result.Count();
            result = result.Skip((validFilter.PageNumber - 1) * validFilter.PageSize).Take(validFilter.PageSize);


            return new PagedResponse<IEnumerable<UserLevelViewModelConditionWithPage>>(result, validFilter.PageNumber, validFilter.PageSize, itemCount);
        }
    }
}
