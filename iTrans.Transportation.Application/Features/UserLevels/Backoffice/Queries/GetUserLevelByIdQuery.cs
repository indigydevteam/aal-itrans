﻿using AutoMapper;
using iTrans.Transportation.Application.DTOs.Backoffice.UserLevel;
using iTrans.Transportation.Application.DTOs.UserLevelCondition;
using iTrans.Transportation.Application.Features.CarLists.Queries;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.UserLevels.Backoffice.Queries
{
    public class BackofficeGetUserLevelByIdQuery : IRequest<Response<UserLevelInformationViewModel>>
    {
        public int Id { get; set; }
    }
    public class BackofficeGetUserLevelByIdQueryHandler : IRequestHandler<BackofficeGetUserLevelByIdQuery, Response<UserLevelInformationViewModel>>
    {
        private readonly IUserLevelRepositoryAsync _userlevelRepository;
        private readonly ICustomerLevelCharacteristicRepositoryAsync _customerLevelCharacteristicRepository;
        private readonly IDriverLevelCharacteristicRepositoryAsync _driverLevelCharacteristicRepository;
        private readonly IUserLevelConditionRepositoryAsync _userLevelConditionRepository;
        private readonly IMapper _mapper;
        public BackofficeGetUserLevelByIdQueryHandler(IUserLevelRepositoryAsync userLevelRepository, IUserLevelConditionRepositoryAsync userLevelconditionRepository
            , ICustomerLevelCharacteristicRepositoryAsync customerLevelCharacteristicRepository, IDriverLevelCharacteristicRepositoryAsync driverLevelCharacteristicRepository, IMapper mapper)
        {
            _userlevelRepository = userLevelRepository;
            _userLevelConditionRepository = userLevelconditionRepository;
            _customerLevelCharacteristicRepository = customerLevelCharacteristicRepository;
            _driverLevelCharacteristicRepository = driverLevelCharacteristicRepository;
            _mapper = mapper;
        }
        public async Task<Response<UserLevelInformationViewModel>> Handle(BackofficeGetUserLevelByIdQuery request, CancellationToken cancellationToken)
        {

            var userLevelObject = (await _userlevelRepository.FindByCondition(x => x.Id.Equals(request.Id)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            var userLevel = _mapper.Map<UserLevelInformationViewModel>(userLevelObject);
            if (userLevelObject != null)
            {
                if (userLevelObject.Module == "customer")
                {
                    var customerLevelCharacteristic = (await _customerLevelCharacteristicRepository.FindByCondition(x => x.Level.Equals(userLevelObject.Level)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                    if (customerLevelCharacteristic != null)
                    {
                        userLevel.customerStar = customerLevelCharacteristic.Star;
                        userLevel.customerRequestCarPerWeek = customerLevelCharacteristic.RequestCarPerWeek;
                        userLevel.customerRequestCarPerMonth = customerLevelCharacteristic.RequestCarPerMonth;
                        userLevel.customerRequestCarPerYear = customerLevelCharacteristic.RequestCarPerYear;
                        userLevel.customerCancelPerWeek = customerLevelCharacteristic.CancelPerWeek;
                        userLevel.customerCancelPerMonth = customerLevelCharacteristic.CancelPerMonth;
                        userLevel.customerCancelPerYear = customerLevelCharacteristic.CancelPerYear;
                        userLevel.customerOrderingValuePerWeek = customerLevelCharacteristic.OrderingValuePerWeek;
                        userLevel.customerOrderingValuePerMonth = customerLevelCharacteristic.OrderingValuePerMonth;
                        userLevel.customerOrderingValuePerYear = customerLevelCharacteristic.OrderingValuePerYear;
                        userLevel.customerDiscount = customerLevelCharacteristic.Discount;
                        userLevel.customerFine = customerLevelCharacteristic.Fine;
                    }
                }
                else if (userLevelObject.Module == "driver")
                {
                    var driverLevelCharacteristic = (await _driverLevelCharacteristicRepository.FindByCondition(x => x.Level.Equals(userLevelObject.Level)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                    if (driverLevelCharacteristic != null)
                    {
                        userLevel.driverStar = driverLevelCharacteristic.Star;
                        userLevel.driverAcceptJobPerWeek = driverLevelCharacteristic.AcceptJobPerWeek;
                        userLevel.driverAcceptJobPerMonth = driverLevelCharacteristic.AcceptJobPerMonth;
                        userLevel.driverAcceptJobPerYear = driverLevelCharacteristic.AcceptJobPerYear;
                        userLevel.driverComplaintPerWeek = driverLevelCharacteristic.ComplaintPerWeek;
                        userLevel.driverComplaintPerMonth = driverLevelCharacteristic.ComplaintPerMonth;
                        userLevel.driverComplaintPerYear = driverLevelCharacteristic.ComplaintPerYear;
                        userLevel.driverRejectPerWeek = driverLevelCharacteristic.RejectPerWeek;
                        userLevel.driverRejectPerMonth = driverLevelCharacteristic.RejectPerMonth;
                        userLevel.driverRejectPerYear = driverLevelCharacteristic.RejectPerYear;
                        userLevel.driverCancelPerWeek = driverLevelCharacteristic.CancelPerWeek;
                        userLevel.driverCancelPerMonth = driverLevelCharacteristic.CancelPerMonth;
                        userLevel.driverCancelPerYear = driverLevelCharacteristic.CancelPerYear;
                        userLevel.driverInsuranceValue = driverLevelCharacteristic.InsuranceValue;
                        userLevel.driverCommission = driverLevelCharacteristic.Commission;
                        userLevel.driverFine = driverLevelCharacteristic.Fine;
                        userLevel.driverDiscount = driverLevelCharacteristic.Discount;
                    }
                }
            }

            return new Response<UserLevelInformationViewModel>(userLevel);
        }
    }
}
