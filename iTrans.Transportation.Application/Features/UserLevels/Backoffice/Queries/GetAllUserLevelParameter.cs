﻿using AutoMapper;
using iTrans.Transportation.Application.DTOs.Backoffice.UserLevel;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.UserLevels.Backoffice.Queries
{
  public  class GetAllUserLevelParameter : IRequest<PagedResponse<IEnumerable<UserLevelViewModel>>>
    {
        public string Search { set; get; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public int Active { set; get; }
        public string Column { set; get; }
    }
    public class GetAllUserLevelParameterHandler : IRequestHandler<GetAllUserLevelParameter, PagedResponse<IEnumerable<UserLevelViewModel>>>
    {
        private readonly IUserLevelRepositoryAsync _userLevelRepository;
        private readonly IMapper _mapper;
        private Expression<Func<UserLevel, bool>> expression;

        public GetAllUserLevelParameterHandler(IUserLevelRepositoryAsync userLevelRepository, IMapper mapper)
        {
            _userLevelRepository = userLevelRepository;
            _mapper = mapper;
        }

        public async Task<PagedResponse<IEnumerable<UserLevelViewModel>>> Handle(GetAllUserLevelParameter request, CancellationToken cancellationToken)
        {
            var validFilter = _mapper.Map<GetAllUserLevelParameter>(request);
            var result = from x in _userLevelRepository.GetAllAsync().Result.ToList().Where(x => x.IsDelete == false).ToList().OrderByDescending(x => x.Modified)
                         select new UserLevelViewModel
                         {
                             id = x.Id,
                             module = x.Module != null ? x.Module : "-",
                             name_TH = x.Name_TH != null ? x.Name_TH : "-",
                             name_ENG = x.Name_ENG != null ? x.Name_ENG : "-",
                             level = x.Level,
                             star = x.Star,
                             active = x.Active,
                             created = x.Created,
                             modified = x.Modified
                         };

            if (validFilter.Search != null && validFilter.Search.Trim() != "")
            {
                result = result.Where(x => x.name_ENG.ToLower().Contains(validFilter.Search.Trim()) || x.name_TH.ToLower().Contains(validFilter.Search.Trim())
                                         || x.module.ToLower().Contains(validFilter.Search.Trim())
                ).ToList();
            }
            if (request.Column == "name_TH" && request.Active == 3)
            {
                result = result.OrderBy(x => x.name_TH);
            }
            if (request.Column == "name_TH" && request.Active == 2)
            {
                result = result.OrderByDescending(x => x.name_TH);
            }
            if (request.Column == "name_ENG" && request.Active == 3)
            {
                result = result.OrderBy(x => x.name_ENG);
            }
            if (request.Column == "name_ENG" && request.Active == 2)
            {
                result = result.OrderByDescending(x => x.name_ENG);
            }
            if (request.Column == "module" && request.Active == 3)
            {
                result = result.OrderBy(x => x.module);
            }
            if (request.Column == "module" && request.Active == 2)
            {
                result = result.OrderByDescending(x => x.module);
            }
            if (request.Column == "created" && request.Active == 3)
            {
                result = result.OrderBy(x => x.created);
            }
            if (request.Column == "created" && request.Active == 2)
            {
                result = result.OrderByDescending(x => x.created);
            }
            if (request.Column == "modified" && request.Active == 3)
            {
                result = result.OrderBy(x => x.modified);
            }
            if (request.Column == "modified" && request.Active == 2)
            {
                result = result.OrderByDescending(x => x.modified);
            }
            if (request.Column == "star" && request.Active == 3)
            {
                result = result.OrderBy(x => x.star);
            }
            if (request.Column == "star" && request.Active == 2)
            {
                result = result.OrderByDescending(x => x.star);
            }

            var itemCount = result.Count();
            result = result.Skip((validFilter.PageNumber - 1) * validFilter.PageSize).Take(validFilter.PageSize);

            return new PagedResponse<IEnumerable<UserLevelViewModel>>(result, validFilter.PageNumber, validFilter.PageSize, itemCount);

            //if (validFilter.Search != null && validFilter.Search.Trim() != "")
            //{
            //    expression = x => (x.Name_TH.Contains(validFilter.Search.Trim()) || x.Name_ENG.Contains(validFilter.Search.Trim()) || x.Module.Contains(validFilter.Search.Trim()));
            //}
            //int itemCount = _userLevelRepository.GetItemCount(expression);
            ////Expression<Func<UserLevel, bool>> expression = x => x.Name_TH != "";
            //var userLevel = await _userLevelRepository.FindByConditionWithPage(expression, validFilter.PageNumber, validFilter.PageSize);
            //var UserLevelViewModel = _mapper.Map<IEnumerable<UserLevelViewModel>>(userLevel);
            //return new PagedResponse<IEnumerable<UserLevelViewModel>>(UserLevelViewModel.OrderByDescending(x => x.modified), validFilter.PageNumber, validFilter.PageSize, itemCount);
        }
    }
}
