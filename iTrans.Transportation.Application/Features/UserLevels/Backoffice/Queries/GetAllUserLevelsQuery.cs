﻿using AutoMapper;
using iTrans.Transportation.Application.DTOs.Backoffice.UserLevel;
using iTrans.Transportation.Application.Features.CarLists.Queries;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.UserLevels.Backoffice.Queries
{
    public class GetUserLevelsQuery : IRequest<Response<IEnumerable<UserLevelInformationViewModel>>>
    {
        public string Search { set; get; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
    }
    public class GetUserLevelsQueryHandler : IRequestHandler<GetUserLevelsQuery, Response<IEnumerable<UserLevelInformationViewModel>>>
    {
        private readonly IUserLevelRepositoryAsync _userlevelRepository;
        private readonly IMapper _mapper;
        private Expression<Func<UserLevel, bool>> expression;
        public GetUserLevelsQueryHandler(IUserLevelRepositoryAsync userLevelRepository, IMapper mapper)
        {
            _userlevelRepository = userLevelRepository;
            _mapper = mapper;
        }
        public async Task<Response<IEnumerable<UserLevelInformationViewModel>>> Handle(GetUserLevelsQuery request, CancellationToken cancellationToken)
        {
            if (request.Search != null && request.Search.Trim() != "")
            {
                expression = x => (x.Name_TH.Contains(request.Search.Trim()) || x.Name_ENG.Contains(request.Search.Trim()) || x.Module.Contains(request.Search.Trim()));
            }
            int itemCount = _userlevelRepository.GetItemCount(expression);
            var userLevelObject = await _userlevelRepository.FindByConditionWithPage(expression, request.PageNumber, request.PageSize);
            var userLevelViewModel = _mapper.Map<IEnumerable<UserLevelInformationViewModel>>(userLevelObject);
            return new Response<IEnumerable<UserLevelInformationViewModel>>(userLevelViewModel);
        }
    }
}