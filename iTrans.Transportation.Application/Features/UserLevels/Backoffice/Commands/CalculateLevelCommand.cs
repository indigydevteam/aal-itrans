﻿using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.UserLevels.Backoffice.Commands
{
    public class CalculateUserLevelCommand : IRequest<Response<bool>>
    {
        
    }
    public class CalculateUserLevelCommandHandler : IRequestHandler<CalculateUserLevelCommand, Response<bool>>
    {
        private readonly IUserLevelRepositoryAsync _userLevelRepository;
        private readonly IMapper _mapper;

        public CalculateUserLevelCommandHandler(IUserLevelRepositoryAsync userLevelRepository,IMapper mapper)
        {
            _userLevelRepository = userLevelRepository;
            _mapper = mapper;
        }

        public async Task<Response<bool>> Handle(CalculateUserLevelCommand request, CancellationToken cancellationToken)
        {
            try
            {
                (await _userLevelRepository.CreateSQLQuery(@"EXEC [dbo].[SP_CALCULATE_LEVEL] ").ConfigureAwait(false)).ExecuteUpdate();

                return new Response<bool>(true);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
