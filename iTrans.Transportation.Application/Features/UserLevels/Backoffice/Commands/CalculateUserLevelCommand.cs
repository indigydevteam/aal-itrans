﻿using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.UserLevels.Backoffice.Commands
{
    public class CalculateLevelCommand : IRequest<Response<bool>>
    {
        public string PeriodYear { get; set; }
        public string PeriodMonth { get; set; }
        public string PeriodDay { get; set; }
    }
    public class CalculateLevelCommandHandler : IRequestHandler<CalculateLevelCommand, Response<bool>>
    {
        private readonly ICustomerRepositoryAsync _customerRepository;
        private readonly IDriverRepositoryAsync _driverRepository;
        private readonly ICustomerLevelCharacteristicRepositoryAsync _customerLevelCharacteristicRepository;
        private readonly ICustomerActivityRepositoryAsync _customerActivityRepository;
        private readonly IDriverLevelCharacteristicRepositoryAsync _driverLevelCharacteristicRepository;
        private readonly IDriverActivityRepositoryAsync _driverActivityRepository;
        private readonly ICustomerLevelHistoryRepositoryAsync _customerLevelHistoryRepository;
        private readonly IDriverLevelHistoryRepositoryAsync _driverLevelHistoryRepository;
        private readonly IUserLevelRepositoryAsync _userLevelRepository;
        private readonly IMapper _mapper;

        public CalculateLevelCommandHandler(ICustomerLevelCharacteristicRepositoryAsync customerLevelCharacteristicRepository, IDriverLevelCharacteristicRepositoryAsync driverLevelCharacteristicRepository
            , ICustomerActivityRepositoryAsync customerActivityRepository, IDriverActivityRepositoryAsync driverActivityRepository
            , ICustomerLevelHistoryRepositoryAsync customerLevelHistoryRepository, IDriverLevelHistoryRepositoryAsync driverLevelHistoryRepository
            , ICustomerRepositoryAsync customerRepository, IDriverRepositoryAsync driverRepository, IUserLevelRepositoryAsync userLevelRepository
            , IMapper mapper)
        {
            _customerLevelCharacteristicRepository = customerLevelCharacteristicRepository;
            _driverLevelCharacteristicRepository = driverLevelCharacteristicRepository;
            _customerActivityRepository = customerActivityRepository;
            _driverActivityRepository = driverActivityRepository;
            _customerLevelHistoryRepository = customerLevelHistoryRepository;
            _driverLevelHistoryRepository = driverLevelHistoryRepository;
            _customerRepository = customerRepository;
            _driverRepository = driverRepository;
            _userLevelRepository = userLevelRepository;
            _mapper = mapper;

        }

        public async Task<Response<bool>> Handle(CalculateLevelCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var calDate = new DateTime(int.Parse(request.PeriodYear), int.Parse(request.PeriodMonth), int.Parse(request.PeriodDay));
                var firstDate = calDate;
                var lastDate = calDate.AddMonths(1).AddDays(-1);
                int addDay = 0;

                switch (calDate.DayOfWeek)
                {
                    case DayOfWeek.Monday:
                        addDay = 0;
                        break;
                    case DayOfWeek.Tuesday:
                        addDay = -1;
                        break;
                    case DayOfWeek.Wednesday:
                        addDay = -2;
                        break;
                    case DayOfWeek.Thursday:
                        addDay = -3;
                        break;
                    case DayOfWeek.Friday:
                        addDay = -4;
                        break;
                    case DayOfWeek.Saturday:
                        addDay = -5;
                        break;
                    case DayOfWeek.Sunday:
                        addDay = -6;
                        break;
                }
                firstDate = calDate.AddDays(addDay);

                addDay = 0;
                switch (lastDate.DayOfWeek)
                {
                    case DayOfWeek.Monday:
                        addDay = -1;
                        break;
                    case DayOfWeek.Tuesday:
                        addDay = -2;
                        break;
                    case DayOfWeek.Wednesday:
                        addDay = -3;
                        break;
                    case DayOfWeek.Thursday:
                        addDay = -4;
                        break;
                    case DayOfWeek.Friday:
                        addDay = -5;
                        break;
                    case DayOfWeek.Saturday:
                        addDay = -6;
                        break;
                    case DayOfWeek.Sunday:
                        addDay = 0;
                        break;
                }

                int loopCount = (int)Math.Ceiling((lastDate - firstDate).TotalDays / 7);
                #region customer
                var customers = await _customerRepository.GetAllAsync();
                var customerLevels = (await _userLevelRepository
                            .FindByCondition(x => x.Module == "customer" && x.Active && x.IsCalculateLevel)
                            .ConfigureAwait(false)).AsQueryable().ToList();
                List<int> clevels = new List<int>();
                foreach (var item in customerLevels)
                {
                    clevels.Add(item.Level);
                }
                var customerLevelCharacteristics = (await _customerLevelCharacteristicRepository
                            .FindByCondition(x => clevels.Contains(x.Level))
                            .ConfigureAwait(false)).AsQueryable().ToList();
                #region test
                //Guid customerId = new Guid("04D51922-B3AE-405C-9CE1-C17444812908");
                //var customer = (await _customerRepository.FindByCondition(x => x.Id == customerId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                //if (customer != null)
                //{
                //    var customerActivites = (await _customerActivityRepository.FindByCondition(x => x.CustomerId == customer.Id && x.Created >= firstDate && x.Created <= lastDate)
                //        .ConfigureAwait(false)).AsQueryable().ToList();
                //    int calLevel = 0;
                //    int rating = 0;
                //    int star = 0;
                //    int requestCar = 0;
                //    int cancel = 0;
                //    decimal orderingValue = 0;

                //    if (customerActivites.Count > 0)
                //    {

                //        star = customerActivites
                //                                .GroupBy(d => d.CustomerId)
                //                                .Select(x => x.Sum(s => s.Star)).FirstOrDefault();
                //        requestCar = customerActivites
                //                                 .GroupBy(d => d.CustomerId)
                //                                 .Select(x => x.Sum(s => s.RequestCar)).FirstOrDefault();

                //        cancel = customerActivites
                //                                 .GroupBy(d => d.CustomerId)
                //                                 .Select(x => x.Sum(s => s.Cancel)).FirstOrDefault();
                //        orderingValue = customerActivites
                //                                .GroupBy(d => d.CustomerId)
                //                                .Select(x => x.Sum(s => s.OrderingValue)).FirstOrDefault();
                //        if (requestCar > 0)
                //        {
                //            rating = (star / requestCar);
                //        }
                //        calDate = firstDate;
                //        foreach (var level in customerLevelCharacteristics)
                //        {
                //            if (calLevel < level.Level)
                //            {
                //                bool isWeekPass = false;
                //                for (int i = 1; i < loopCount; i++)
                //                {
                //                    var activitesPerWeek = customerActivites.Where(x => x.Created >= calDate && x.Created >= calDate.AddDays(7)).ToList();
                //                    calDate = calDate.AddDays(7);
                //                    if (activitesPerWeek.Count > 0)
                //                    {
                //                        var requestCarPerWeek = activitesPerWeek
                //                                 .GroupBy(d => d.CustomerId)
                //                                 .Select(x => x.Sum(s => s.RequestCar)).FirstOrDefault();
                //                        if (requestCarPerWeek >= level.RequestCarPerWeek)
                //                        {
                //                            isWeekPass = true;
                //                        }
                //                    }
                //                }

                //                if (rating >= level.Star && requestCar >= level.RequestCarPerMonth && cancel <= level.CancelPerMonth && orderingValue >= level.OrderingValuePerMonth)
                //                {

                //                    calLevel = level.Level;
                //                }
                //            }
                //        }
                //    }
                //    if (calLevel > 0 && customer.VerifyStatus == 2)
                //    {
                //        customer.Level = calLevel;
                //        customer.Rating = rating.ToString();
                //    }
                //    else
                //    {
                //        if (customer.VerifyStatus == 2)
                //        {
                //            calLevel = 2;
                //        }
                //        else
                //        {
                //            calLevel = 1;
                //        }
                //    }
                //    customer.RequestCarPerMonth = 0;
                //    customer.CancelAmountPerMonth = 0;
                //    customer.OrderValuePerMonth = 0;
                //    customer.Level = calLevel;
                //    await _customerRepository.UpdateAsync(customer);
                //    CustomerLevelHistory customerLevelHistory = new CustomerLevelHistory()
                //    {
                //        CustomerId = customer.Id,
                //        Level = customer.Level,
                //        Star = star,
                //        Rating = rating,
                //        RequestCar = requestCar,
                //        Cancel = cancel,
                //        OrderingValue = orderingValue,
                //        Created = DateTime.Now,
                //        CreatedBy = "System",
                //        Modified = DateTime.Now,
                //        ModifiedBy = "System"
                //    };
                //    await _customerLevelHistoryRepository.AddAsync(customerLevelHistory);

                //}
                #endregion

                foreach (Customer item in customers)
                {
                    var customerActivites = (await _customerActivityRepository.FindByCondition(x => x.CustomerId == item.Id && x.Created >= firstDate && x.Created <= lastDate)
                        .ConfigureAwait(false)).AsQueryable().ToList();
                    int calLevel = 0;
                    int rating = 0;
                    int star = 0;
                    int requestCar = 0;
                    int cancel = 0;
                    decimal orderingValue = 0;

                    if (customerActivites.Count > 0)
                    {

                        star = customerActivites
                                                .GroupBy(d => d.CustomerId)
                                                .Select(x => x.Sum(s => s.Star)).FirstOrDefault();
                        requestCar = customerActivites
                                                 .GroupBy(d => d.CustomerId)
                                                 .Select(x => x.Sum(s => s.RequestCar)).FirstOrDefault();

                        cancel = customerActivites
                                                 .GroupBy(d => d.CustomerId)
                                                 .Select(x => x.Sum(s => s.Cancel)).FirstOrDefault();
                        orderingValue = customerActivites
                                                .GroupBy(d => d.CustomerId)
                                                .Select(x => x.Sum(s => s.OrderingValue)).FirstOrDefault();
                        if (requestCar > 0)
                        {
                            rating = (star / requestCar);
                        }
                        calDate = firstDate;
                        foreach (var level in customerLevelCharacteristics)
                        {
                            if (calLevel < level.Level)
                            {
                                bool isWeekPass = false;
                                for (int i = 1; i < loopCount; i++)
                                {
                                    var activitesPerWeek = customerActivites.Where(x => x.Created >= calDate && x.Created >= calDate.AddDays(7)).ToList();
                                    calDate = calDate.AddDays(7);
                                    if (activitesPerWeek.Count > 0)
                                    {
                                        var requestCarPerWeek = activitesPerWeek
                                                 .GroupBy(d => d.CustomerId)
                                                 .Select(x => x.Sum(s => s.RequestCar)).FirstOrDefault();
                                        if (requestCarPerWeek >= level.RequestCarPerWeek)
                                        {
                                            isWeekPass = true;
                                        }
                                    }
                                }

                                if (rating >= level.Star && requestCar >= level.RequestCarPerMonth && cancel <= level.CancelPerMonth && orderingValue >= level.OrderingValuePerMonth)
                                {

                                    calLevel = level.Level;
                                }
                            }
                        }
                    }
                    if (calLevel > 0 && item.VerifyStatus == 2)
                    {
                        item.Level = calLevel;
                        item.Rating = rating.ToString();
                    }
                    else
                    {
                        if (item.VerifyStatus == 2)
                        {
                            calLevel = 2;
                        }
                        else
                        {
                            calLevel = 1;
                        }
                    }
                    item.RequestCarPerMonth = 0;
                    item.CancelAmountPerMonth = 0;
                    item.OrderValuePerMonth = 0;
                    item.Level = calLevel;
                    await _customerRepository.UpdateAsync(item);
                    CustomerLevelHistory customerLevelHistory = new CustomerLevelHistory()
                    {
                        CustomerId = item.Id,
                        Level = item.Level,
                        Star = star,
                        Rating = rating,
                        RequestCar = requestCar,
                        Cancel = cancel,
                        OrderingValue = orderingValue,
                        Created = DateTime.Now,
                        CreatedBy = "System",
                        Modified = DateTime.Now,
                        ModifiedBy = "System"
                    };
                    await _customerLevelHistoryRepository.AddAsync(customerLevelHistory);

                }
                #endregion

                #region driver
                var drivers = await _driverRepository.GetAllAsync();
                var driverLevels = (await _userLevelRepository
                            .FindByCondition(x => x.Module == "driver" && x.Active && x.IsCalculateLevel)
                            .ConfigureAwait(false)).AsQueryable().ToList();
                List<int> dlevels = new List<int>();
                foreach (var item in driverLevels)
                {
                    dlevels.Add(item.Level);
                }
                var driverLevelCharacteristics = (await _driverLevelCharacteristicRepository
                            .FindByCondition(x => dlevels.Contains(x.Level))
                            .ConfigureAwait(false)).AsQueryable().ToList();
                #region test
                //Guid driverId = new Guid("F18F7BA0-DEAE-48B3-9F35-AE036A4B041F");
                //var driver = (await _driverRepository.FindByCondition(x => x.Id == driverId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                //if(driver != null)
                //{
                //    var driverActivites = (await _driverActivityRepository.FindByCondition(x => x.DriverId == driver.Id && x.Created >= firstDate && x.Created <= lastDate).ConfigureAwait(false)).AsQueryable().ToList();
                //    int calLevel = 0;
                //    int rating = 0;
                //    int star = 0;
                //    int acceptJob = 0;
                //    int cancel = 0;
                //    decimal insuranceValue = 0;
                //    if (driverActivites.Count > 0)
                //    {

                //        star = driverActivites
                //                                .GroupBy(d => d.DriverId)
                //                                .Select(x => x.Sum(s => s.Star)).FirstOrDefault();
                //        acceptJob = driverActivites
                //                                 .GroupBy(d => d.DriverId)
                //                                 .Select(x => x.Sum(s => s.AcceptJob)).FirstOrDefault();

                //        cancel = driverActivites
                //                                 .GroupBy(d => d.DriverId)
                //                                 .Select(x => x.Sum(s => s.Cancel)).FirstOrDefault();
                //        insuranceValue = driverActivites
                //                                .GroupBy(d => d.DriverId)
                //                                .Select(x => x.Sum(s => s.InsuranceValue)).FirstOrDefault();
                //        if (acceptJob > 0)
                //        {
                //            rating = (star / acceptJob);
                //        }
                //        foreach (var level in driverLevelCharacteristics)
                //        {
                //            if (calLevel < level.Level)
                //            {
                //                bool isWeekPass = false;
                //                for (int i = 1; i < loopCount; i++)
                //                {
                //                    var activitesPerWeek = driverActivites.Where(x => x.Created >= calDate && x.Created >= calDate.AddDays(7)).ToList();
                //                    calDate = calDate.AddDays(7);
                //                    if (activitesPerWeek.Count > 0)
                //                    {
                //                        var requestCarPerWeek = activitesPerWeek
                //                                 .GroupBy(d => d.DriverId)
                //                                 .Select(x => x.Sum(s => s.AcceptJob)).FirstOrDefault();
                //                        if (requestCarPerWeek >= level.AcceptJobPerWeek)
                //                        {
                //                            isWeekPass = true;
                //                        }
                //                    }
                //                }

                //                if (rating >= level.Star && acceptJob >= level.AcceptJobPerMonth && cancel <= level.CancelPerMonth && insuranceValue >= level.InsuranceValue)
                //                {
                //                    calLevel = level.Level;
                //                }
                //            }
                //        }
                //    }
                //    if (calLevel > 0 && driver.VerifyStatus == 2)
                //    {
                //        driver.Level = calLevel;
                //        driver.Rating = rating.ToString();
                //    }
                //    else
                //    {
                //        if (driver.VerifyStatus == 2)
                //        {
                //            calLevel = 2;
                //        }
                //        else
                //        {
                //            calLevel = 1;
                //        }
                //    }
                //    driver.AcceptJobPerMonth = 0;
                //    driver.CancelPerMonth = 0;
                //    driver.InsuranceValue = 0;
                //    driver.Level = calLevel;
                //    await _driverRepository.UpdateAsync(driver);
                //    DriverLevelHistory driverLevelHistory = new DriverLevelHistory()
                //    {
                //        DriverId = driver.Id,
                //        Level = driver.Level,
                //        Star = star,
                //        Rating = rating,
                //        AcceptJob = acceptJob,
                //        Cancel = cancel,
                //        InsuranceValue = insuranceValue
                //    };
                //    await _driverLevelHistoryRepository.AddAsync(driverLevelHistory);

                //}
                #endregion

                foreach (Driver item in drivers)
                {
                    var driverActivites = (await _driverActivityRepository.FindByCondition(x => x.DriverId == item.Id && x.Created >= firstDate && x.Created <= lastDate).ConfigureAwait(false)).AsQueryable().ToList();
                    int calLevel = 0;
                    int rating = 0;
                    int star = 0;
                    int acceptJob = 0;
                    int cancel = 0;
                    decimal insuranceValue = 0;
                    if (driverActivites.Count > 0)
                    {

                        star = driverActivites
                                                .GroupBy(d => d.DriverId)
                                                .Select(x => x.Sum(s => s.Star)).FirstOrDefault();
                        acceptJob = driverActivites
                                                 .GroupBy(d => d.DriverId)
                                                 .Select(x => x.Sum(s => s.AcceptJob)).FirstOrDefault();

                        cancel = driverActivites
                                                 .GroupBy(d => d.DriverId)
                                                 .Select(x => x.Sum(s => s.Cancel)).FirstOrDefault();
                        insuranceValue = driverActivites
                                                .GroupBy(d => d.DriverId)
                                                .Select(x => x.Sum(s => s.InsuranceValue)).FirstOrDefault();
                        if (acceptJob > 0)
                        {
                            rating = (star / acceptJob);
                        }
                        foreach (var level in driverLevelCharacteristics)
                        {
                            if (calLevel < level.Level)
                            {
                                bool isWeekPass = false;
                                for (int i = 1; i < loopCount; i++)
                                {
                                    var activitesPerWeek = driverActivites.Where(x => x.Created >= calDate && x.Created >= calDate.AddDays(7)).ToList();
                                    calDate = calDate.AddDays(7);
                                    if (activitesPerWeek.Count > 0)
                                    {
                                        var requestCarPerWeek = activitesPerWeek
                                                 .GroupBy(d => d.DriverId)
                                                 .Select(x => x.Sum(s => s.AcceptJob)).FirstOrDefault();
                                        if (requestCarPerWeek >= level.AcceptJobPerWeek)
                                        {
                                            isWeekPass = true;
                                        }
                                    }
                                }

                                if (rating >= level.Star && acceptJob >= level.AcceptJobPerMonth && cancel <= level.CancelPerMonth && insuranceValue >= level.InsuranceValue)
                                {
                                    calLevel = level.Level;
                                }
                            }
                        }
                    }
                    if (calLevel > 0 && item.VerifyStatus == 2)
                    {
                        item.Level = calLevel;
                        item.Rating = rating.ToString();
                    }
                    else
                    {
                        if (item.VerifyStatus == 2)
                        {
                            calLevel = 2;
                        }
                        else
                        {
                            calLevel = 1;
                        }
                    }
                    item.AcceptJobPerMonth = 0;
                    item.CancelPerMonth = 0;
                    item.InsuranceValue = 0;
                    item.Level = calLevel;
                    await _driverRepository.UpdateAsync(item);
                    DriverLevelHistory driverLevelHistory = new DriverLevelHistory()
                    {
                        DriverId = item.Id,
                        Level = item.Level,
                        Star = star,
                        Rating = rating,
                        AcceptJob = acceptJob,
                        Cancel = cancel,
                        InsuranceValue = insuranceValue
                    };
                    await _driverLevelHistoryRepository.AddAsync(driverLevelHistory);

                }
                #endregion
                return new Response<bool>(true);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
