﻿using AutoMapper;
using iTrans.Transportation.Application.DTOs.UserLevel;
using iTrans.Transportation.Application.DTOs.UserLevelCondition;
using iTrans.Transportation.Application.Features.CarLists.Queries;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.UserLevels.Queries
{
   public class GetUserLevelByIdQuery : IRequest<Response<UserLevelInformationViewModel>>
    {
        public int Id { get; set; }
    }
    public class GetUserLevelByIdQueryHandler : IRequestHandler<GetUserLevelByIdQuery, Response<UserLevelInformationViewModel>>
    {
        private readonly IUserLevelRepositoryAsync _userlevelRepository;
        private readonly IUserLevelConditionRepositoryAsync _userLevelConditionRepository;
        private readonly IMapper _mapper;
        public GetUserLevelByIdQueryHandler(IUserLevelRepositoryAsync userLevelRepository, IUserLevelConditionRepositoryAsync userLevelconditionRepository, IMapper mapper)
        {
            _userlevelRepository = userLevelRepository;
            _userLevelConditionRepository = userLevelconditionRepository;
            _mapper = mapper;
        }
        public async Task<Response<UserLevelInformationViewModel>> Handle(GetUserLevelByIdQuery request, CancellationToken cancellationToken)
        {
          
            var userLeveltObject = (await _userlevelRepository.FindByCondition(x => x.Id.Equals(request.Id)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();

            //var userlevel = (await _userLevelConditionRepository.FindByCondition(x => x.UserLevelId.Equals(userLeveltObject.Level) && x.Active == true).ConfigureAwait(false)).AsQueryable().ToList();
            // var userLevelCondition = _mapper.Map<List<Condition>>(userlevel.OrderBy(x => x.Sequence));


            UserLevelInformationViewModel data = new UserLevelInformationViewModel()
            {
                id = userLeveltObject.Id,
                module = userLeveltObject.Module,
                level = userLeveltObject.Level,
                name_TH = userLeveltObject.Name_TH,
                name_ENG = userLeveltObject.Name_ENG,
                //Condition = userLevelCondition

            };

            return new Response<UserLevelInformationViewModel>(_mapper.Map<UserLevelInformationViewModel>(data));
        }
    }
}
