﻿using AutoMapper;
using iTrans.Transportation.Application.DTOs.UserLevel;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.UserLevels.Queries
{
    public class GetAllUserLevelQuery : IRequest<Response<IEnumerable<UserLevelViewModel>>>
    {

    }
    public class GetAllUserLevelQueryHandler : IRequestHandler<GetAllUserLevelQuery, Response<IEnumerable<UserLevelViewModel>>>
    {
        private readonly IUserLevelRepositoryAsync _userLevelRepository;
        private readonly IMapper _mapper;
        public GetAllUserLevelQueryHandler(IUserLevelRepositoryAsync userLevelRepository, IMapper mapper)
        {
            _userLevelRepository = userLevelRepository;
            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<UserLevelViewModel>>> Handle(GetAllUserLevelQuery request, CancellationToken cancellationToken)
        {
            var userlevel = await _userLevelRepository.GetAllAsync();
            var UserLevelViewModel = _mapper.Map<IEnumerable<UserLevelViewModel>>(userlevel);
            return new Response<IEnumerable<UserLevelViewModel>>(UserLevelViewModel);
        }
    }
}
