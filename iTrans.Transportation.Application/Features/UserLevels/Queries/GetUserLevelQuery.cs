﻿using AutoMapper;
using iTrans.Transportation.Application.DTOs.UserLevel;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.UserLevels.Queries
{
    public class GetUserLevelQuery : IRequest<Response<IEnumerable<UserLevelViewModel>>>
    {
        public string module { set; get; }
    }
    public class GetUserLevelQueryHandler : IRequestHandler<GetUserLevelQuery, Response<IEnumerable<UserLevelViewModel>>>
    {
        private readonly IUserLevelRepositoryAsync _userLevelRepository;
        private readonly IMapper _mapper;
        public GetUserLevelQueryHandler(IUserLevelRepositoryAsync userLevelRepository, IMapper mapper)
        {
            _userLevelRepository = userLevelRepository;
            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<UserLevelViewModel>>> Handle(GetUserLevelQuery request, CancellationToken cancellationToken)
        {
            var userlevel = (await _userLevelRepository.FindByCondition(x => x.Module.Trim().ToLower() == request.module.Trim().ToLower()).ConfigureAwait(false)).AsQueryable().ToList();
            var UserLevelViewModel = _mapper.Map<IEnumerable<UserLevelViewModel>>(userlevel.OrderBy(x => x.Level));
            return new Response<IEnumerable<UserLevelViewModel>>(UserLevelViewModel);
        }
    }
}
