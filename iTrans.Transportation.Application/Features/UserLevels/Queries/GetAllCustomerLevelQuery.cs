﻿using AutoMapper;
using iTrans.Transportation.Application.DTOs.UserLevel;
using iTrans.Transportation.Application.Features.CarLists.Queries;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.UserLevels.Queries
{
    public class GetAllCustomerLevelQuery : IRequest<Response<IEnumerable<UserLevelInformationViewModel>>>
    {
        
    }
    public class GetAllCustomerLevelQueryHandler : IRequestHandler<GetAllCustomerLevelQuery, Response<IEnumerable<UserLevelInformationViewModel>>>
    {
        private readonly IUserLevelRepositoryAsync _userlevelRepository;
        private readonly IMapper _mapper;
        public GetAllCustomerLevelQueryHandler(IUserLevelRepositoryAsync userLevelRepository, IMapper mapper)
        {
            _userlevelRepository = userLevelRepository;
            _mapper = mapper;
        }
        public async Task<Response<IEnumerable<UserLevelInformationViewModel>>> Handle(GetAllCustomerLevelQuery request, CancellationToken cancellationToken)
        {
            var customerLevelObject = (await _userlevelRepository.FindByCondition(x => x.Module == "customer" && x.Active == true).ConfigureAwait(false)).AsQueryable().ToList();
            var customerLevelViewModel = _mapper.Map<IEnumerable<UserLevelInformationViewModel>>(customerLevelObject);
            return new Response<IEnumerable<UserLevelInformationViewModel>>(customerLevelViewModel);
        }
    }
}