﻿using AutoMapper;
using iTrans.Transportation.Application.DTOs.UserLevel;
using iTrans.Transportation.Application.Features.CarLists.Queries;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.UserLevels.Queries
{
    public class GetDriverLevelQuery : IRequest<Response<UserLevelInformationViewModel>>
    {
        public int Level { get; set; }
    }
    public class GetDriverLevelQueryHandler : IRequestHandler<GetDriverLevelQuery, Response<UserLevelInformationViewModel>>
    {
        private readonly IUserLevelRepositoryAsync _userlevelRepository;
        private readonly IUserLevelConditionRepositoryAsync _userLevelConditionRepository;
        private readonly IMapper _mapper;
        public GetDriverLevelQueryHandler(IUserLevelRepositoryAsync userLevelRepository, IUserLevelConditionRepositoryAsync userLevelconditionRepository, IMapper mapper)
        {
            _userlevelRepository = userLevelRepository;
            _userLevelConditionRepository = userLevelconditionRepository;
            _mapper = mapper;
        }
        public async Task<Response<UserLevelInformationViewModel>> Handle(GetDriverLevelQuery request, CancellationToken cancellationToken)
        {
            var userLeveltObject = (await _userlevelRepository.FindByCondition(x => x.Level.Equals(request.Level) && x.Module == "driver" && x.Active == true).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            var driverLevelViewModel = _mapper.Map<UserLevelInformationViewModel>(userLeveltObject);
            return new Response<UserLevelInformationViewModel>(driverLevelViewModel);

            // var userlevel = (await _userLevelConditionRepository.FindByCondition(x => x.UserLevelId.Equals(userLeveltObject.Level) && x.Active == true).ConfigureAwait(false)).AsQueryable().ToList();
            // var userLevelCondition = _mapper.Map<List<Condition>>(userlevel.OrderBy(x => x.Sequence));


            //UserLevelInformationViewModel data = new UserLevelInformationViewModel()
            //{
            //    Id = userLeveltObject.Id,
            //    Level = userLeveltObject.Level,
            //    Name_TH = userLeveltObject.Name_TH,
            //    Name_ENG = userLeveltObject.Name_ENG,
            //    //Condition = userLevelCondition

            //};

            //return new Response<UserLevelInformationViewModel>(_mapper.Map<UserLevelInformationViewModel>(data));
        }
    }
}