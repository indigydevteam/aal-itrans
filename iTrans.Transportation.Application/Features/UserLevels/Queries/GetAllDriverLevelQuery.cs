﻿using AutoMapper;
using iTrans.Transportation.Application.DTOs.UserLevel;
using iTrans.Transportation.Application.Features.CarLists.Queries;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.UserLevels.Queries
{
    public class GetAllDriverLevelQuery : IRequest<Response<IEnumerable<UserLevelInformationViewModel>>>
    {
        
    }
    public class GetAllDriverLevelQueryHandler : IRequestHandler<GetAllDriverLevelQuery, Response<IEnumerable<UserLevelInformationViewModel>>>
    {
        private readonly IUserLevelRepositoryAsync _userlevelRepository;
        private readonly IMapper _mapper;
        public GetAllDriverLevelQueryHandler(IUserLevelRepositoryAsync userLevelRepository, IMapper mapper)
        {
            _userlevelRepository = userLevelRepository;
            _mapper = mapper;
        }
        public async Task<Response<IEnumerable<UserLevelInformationViewModel>>> Handle(GetAllDriverLevelQuery request, CancellationToken cancellationToken)
        {
            var driverLevelObject = (await _userlevelRepository.FindByCondition(x => x.Module == "driver" && x.Active == true).ConfigureAwait(false)).AsQueryable().ToList();
            var driverLevelViewModel = _mapper.Map<IEnumerable<UserLevelInformationViewModel>>(driverLevelObject);
            return new Response<IEnumerable<UserLevelInformationViewModel>>(driverLevelViewModel);
        }
    }
}