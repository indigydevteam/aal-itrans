﻿using AutoMapper;
using iTrans.Transportation.Application.DTOs.UserLevel;
using iTrans.Transportation.Application.Features.CarLists.Queries;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.UserLevels.Queries
{
    public class GetCustomerLevelQuery : IRequest<Response<UserLevelInformationViewModel>>
    {
        public int Level { get; set; }
    }
    public class GetCustomerLevelQueryHandler : IRequestHandler<GetCustomerLevelQuery, Response<UserLevelInformationViewModel>>
    {
        private readonly IUserLevelRepositoryAsync _userlevelRepository;
        private readonly IUserLevelConditionRepositoryAsync _userLevelConditionRepository;
        private readonly IMapper _mapper;
        public GetCustomerLevelQueryHandler(IUserLevelRepositoryAsync userLevelRepository, IUserLevelConditionRepositoryAsync userLevelConditionRepository, IMapper mapper)
        {
            _userlevelRepository = userLevelRepository;
            _userLevelConditionRepository = userLevelConditionRepository;
            _mapper = mapper;
        }
        public async Task<Response<UserLevelInformationViewModel>> Handle(GetCustomerLevelQuery request, CancellationToken cancellationToken)
        {
            var userLeveltObject = (await _userlevelRepository.FindByCondition(x => x.Level.Equals(request.Level) && x.Module == "customer" && x.Active == true).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            var customerLevelViewModel = _mapper.Map<UserLevelInformationViewModel>(userLeveltObject);
            return new Response<UserLevelInformationViewModel>(customerLevelViewModel);

            //var userlevel = (await _userLevelConditionRepository.FindByCondition(x => x.UserLevelId.Equals(userLeveltObject.Level) && x.Active == true).ConfigureAwait(false)).AsQueryable().ToList();
            //var userLevelCondition = _mapper.Map<List<Condition>>(userlevel.OrderBy(x => x.Sequence));


            //UserLevelInformationViewModel data = new UserLevelInformationViewModel()
            //{
            //    Id = userLeveltObject.Id,
            //    Level = userLeveltObject.Level,
            //    Name_TH = userLeveltObject.Name_TH,
            //    Name_ENG = userLeveltObject.Name_ENG,
            //    //Condition = userLevelCondition

            //};

            //return new Response<UserLevelInformationViewModel>(_mapper.Map<UserLevelInformationViewModel>(data));
        }
    }
}