﻿using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.UserLevels.Commands
{
  public  class DeleteUserLevelByIdCommand : IRequest<Response<int>>
    {
        public int Id { get; set; }
        public class DeleteUserLevelByIdCommandHandler : IRequestHandler<DeleteUserLevelByIdCommand, Response<int>>
        {
            private readonly IUserLevelRepositoryAsync _userLevelRepository;
            public DeleteUserLevelByIdCommandHandler(IUserLevelRepositoryAsync userLevelRepository)
            {
                _userLevelRepository = userLevelRepository;
            }
            public async Task<Response<int>> Handle(DeleteUserLevelByIdCommand command, CancellationToken cancellationToken)
            {
                var data = (await _userLevelRepository.FindByCondition(x => x.Id == command.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (data == null)
                {
                    throw new ApiException($"UserLevel Not Found.");
                }
                else
                {
                    await _userLevelRepository.DeleteAsync(data);
                    return new Response<int>(data.Id);
                }
            }
        }
    }
}
