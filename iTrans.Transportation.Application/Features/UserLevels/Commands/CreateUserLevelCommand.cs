﻿using AutoMapper;
using iTrans.Transportation.Application.DTOs.UserLevelCondition;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using iTrans.Transportation.Domain.Entities;
using MediatR;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.UserLevels.Commands
{

    public partial class CreateUserLevelCommand : IRequest<Response<int>>
    {
        public Guid? UserId { get; set; }
        public string Module { get; set; }
        public int Level { get; set; }
        public string Name_TH { get; set; }
        public string Name_ENG { get; set; }
        public int Star { get; set; }
        public bool Active { get; set; }
        public int? CustomerRequestCarPerWeek { get; set; }
        public int? CustomerRequestCarPerMonth { get; set; }
        public int? CustomerRequestCarPerYear { get; set; }
        public int? CustomerCancelPerWeek { get; set; }
        public int? CustomerCancelPerMonth { get; set; }
        public int? CustomerCancelPerYear { get; set; }
        public int? CustomerOrderingValuePerWeek { get; set; }
        public int? CustomerOrderingValuePerMonth { get; set; }
        public int? CustomerOrderingValuePerYear { get; set; }
        public int? CustomerFine { get; set; }
        public int? CustomerDiscount { get; set; }

        public int? DriverAcceptJobPerWeek { get; set; }
        public int? DriverAcceptJobPerMonth { get; set; }
        public int? DriverAcceptJobPerYear { get; set; }
        public int? DriverCancelPerWeek { get; set; }
        public int? DriverCancelPerMonth { get; set; }
        public int? DriverCancelPerYear { get; set; }
        public int? DriverComplaintPerWeek { get; set; }
        public int? DriverComplaintPerMonth { get; set; }
        public int? DriverComplaintPerYear { get; set; }
        public int? DriverRejectPerWeek { get; set; }
        public int? DriverRejectPerMonth { get; set; }
        public int? DriverRejectPerYear { get; set; }
        public int? DriverCommission { get; set; }
        public int? DriverDiscount { get; set; }
        public int? DriverFine { get; set; }

        public string Conditions { get; set; }
    }
    public class CreateUserLevelCommandHandler : IRequestHandler<CreateUserLevelCommand, Response<int>>
    {
        private readonly IUserLevelRepositoryAsync _userLevelRepository;
        private readonly IUserLevelConditionRepositoryAsync _userLevelConditionRepository;
        private readonly ICustomerLevelCharacteristicRepositoryAsync _customerLevelCharacteristicRepository;
        private readonly IDriverLevelCharacteristicRepositoryAsync _driverLevelCharacteristicRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;
        public CreateUserLevelCommandHandler(IUserLevelRepositoryAsync userLevelRepository, IUserLevelConditionRepositoryAsync userLevelConditionRepository, IApplicationLogRepositoryAsync applicationLogRepository
            , ICustomerLevelCharacteristicRepositoryAsync customerLevelCharacteristicRepository, IDriverLevelCharacteristicRepositoryAsync driverLevelCharacteristicRepository, IMapper mapper)
        {
            _userLevelRepository = userLevelRepository;
            _userLevelConditionRepository = userLevelConditionRepository;
            _applicationLogRepository = applicationLogRepository;
            _customerLevelCharacteristicRepository = customerLevelCharacteristicRepository;
            _driverLevelCharacteristicRepository = driverLevelCharacteristicRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(CreateUserLevelCommand request, CancellationToken cancellationToken)
        {
            try
            {
                bool datalevel = (await _userLevelRepository.FindByCondition(x => x.Level == request.Level && x.Module == request.Module).ConfigureAwait(false)).AsQueryable().Any();

                if (!datalevel)
                {
                    UserLevel userLevel = new UserLevel()
                    {
                        Module = request.Module,
                        Level = request.Level,
                        Name_TH = request.Name_TH,
                        Name_ENG = request.Name_ENG,
                        Star = request.Star,
                        Active = request.Active,
                        CreatedBy = request.UserId != null ? request.UserId.GetValueOrDefault().ToString() : "",
                        Created = DateTime.Now,
                        ModifiedBy = request.UserId != null ? request.UserId.GetValueOrDefault().ToString() : "",
                        Modified = DateTime.Now,
                    };
                    var dataObject = await _userLevelRepository.AddAsync(userLevel);
                    if (dataObject.Id > 0)
                    {
                        if (request.Module == "customer")
                        {
                            CustomerLevelCharacteristic customerLevelCharacteristic = new CustomerLevelCharacteristic()
                            {
                                Level = request.Level,
                                Star = request.Star,
                                RequestCarPerWeek = request.CustomerRequestCarPerWeek == null ? 0 : request.CustomerRequestCarPerWeek.Value,
                                RequestCarPerMonth = request.CustomerRequestCarPerMonth == null ? 0 : request.CustomerRequestCarPerMonth.Value,
                                RequestCarPerYear = request.CustomerRequestCarPerYear == null ? 0 : request.CustomerRequestCarPerYear.Value,
                                CancelPerWeek = request.CustomerCancelPerWeek == null ? 0 : request.CustomerCancelPerWeek.Value,
                                CancelPerMonth = request.CustomerCancelPerMonth == null ? 0 : request.CustomerCancelPerMonth.Value,
                                CancelPerYear = request.CustomerCancelPerYear == null ? 0 : request.CustomerCancelPerYear.Value,
                                OrderingValuePerWeek = request.CustomerOrderingValuePerWeek == null ? 0 : request.CustomerOrderingValuePerWeek.Value,
                                OrderingValuePerMonth = request.CustomerOrderingValuePerMonth == null ? 0 : request.CustomerOrderingValuePerMonth.Value,
                                OrderingValuePerYear = request.CustomerOrderingValuePerYear == null ? 0 : request.CustomerOrderingValuePerYear.Value,
                                Fine = request.CustomerFine == null ? 0 : request.CustomerFine.Value,
                                Discount = request.CustomerDiscount == null ? 0 : request.CustomerDiscount.Value,
                                CreatedBy = request.UserId != null ? request.UserId.GetValueOrDefault().ToString() : "",
                                Created = DateTime.Now,
                                ModifiedBy = request.UserId != null ? request.UserId.GetValueOrDefault().ToString() : "",
                                Modified = DateTime.Now,
                            };
                            await _customerLevelCharacteristicRepository.AddAsync(customerLevelCharacteristic);
                        }
                        else if (request.Module == "driver")
                        {
                            DriverLevelCharacteristic driverLevelCharacteristic = new DriverLevelCharacteristic()
                            {
                                Level = request.Level,
                                Star = request.Star,
                                AcceptJobPerWeek = request.DriverAcceptJobPerWeek == null ? 0 : request.DriverAcceptJobPerWeek.Value,
                                AcceptJobPerMonth = request.DriverAcceptJobPerMonth == null ? 0 : request.DriverAcceptJobPerMonth.Value,
                                AcceptJobPerYear = request.DriverAcceptJobPerYear == null ? 0 : request.DriverAcceptJobPerYear.Value,
                                CancelPerWeek = request.DriverCancelPerWeek == null ? 0 : request.DriverCancelPerWeek.Value,
                                CancelPerMonth = request.DriverCancelPerMonth == null ? 0 : request.DriverCancelPerMonth.Value,
                                CancelPerYear = request.DriverCancelPerYear == null ? 0 : request.DriverCancelPerYear.Value,
                                ComplaintPerWeek = request.DriverComplaintPerWeek == null ? 0 : request.DriverComplaintPerWeek.Value,
                                ComplaintPerMonth = request.DriverComplaintPerMonth == null ? 0 : request.DriverComplaintPerMonth.Value,
                                ComplaintPerYear = request.DriverComplaintPerYear == null ? 0 : request.DriverComplaintPerYear.Value,
                                RejectPerWeek = request.DriverRejectPerWeek == null ? 0 : request.DriverRejectPerWeek.Value,
                                RejectPerMonth = request.DriverRejectPerMonth == null ? 0 : request.DriverRejectPerMonth.Value,
                                RejectPerYear = request.DriverRejectPerYear == null ? 0 : request.DriverRejectPerYear.Value,
                                Discount = request.DriverDiscount == null ? 0 : request.DriverDiscount.Value,
                                Commission = request.DriverCommission == null ? 0 : request.DriverCommission.Value,
                                Fine = request.DriverFine == null ? 0 : request.DriverFine.Value,

                                CreatedBy = request.UserId != null ? request.UserId.GetValueOrDefault().ToString() : "",
                                Created = DateTime.Now,
                                ModifiedBy = request.UserId != null ? request.UserId.GetValueOrDefault().ToString() : "",
                                Modified = DateTime.Now,
                            };
                            await _driverLevelCharacteristicRepository.AddAsync(driverLevelCharacteristic);
                        }
                        var conditions = JsonConvert.DeserializeObject<List<UserLevelCondition>>(request.Conditions);
                        int i = 0;
                        foreach (UserLevelCondition condition in conditions)
                        {
                            i = i + 1;
                            condition.UserLevel = dataObject;
                            condition.Sequence = i;
                            condition.Active = true;
                            condition.CreatedBy = request.UserId != null ? request.UserId.GetValueOrDefault().ToString() : "";
                            condition.Created = DateTime.Now;
                            condition.ModifiedBy = request.UserId != null ? request.UserId.GetValueOrDefault().ToString() : "";
                            condition.Modified = DateTime.Now;
                            await _userLevelConditionRepository.AddAsync(condition);
                        }
                    }

                    var log = new CreateAppLog(_applicationLogRepository);
                    log.CreateLog("User", "User Level", "Create", Newtonsoft.Json.JsonConvert.SerializeObject(userLevel),request.UserId.ToString());
                    //return new Response<int>(dataListObject.Id);
                    return new Response<int>(dataObject.Id);
                }
                else
                {
                    throw new ApiException($"Duplicate Data");

                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}