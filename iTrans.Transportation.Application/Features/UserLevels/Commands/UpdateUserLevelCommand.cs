﻿using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.UserLevels.Commands
{
    public class UpdateUserLevelCommand : IRequest<Response<int>>
    {
        public Guid? UserId { get; set; }
        public int Id { get; set; }
        public string Module { get; set; }
        public int Level { get; set; }
        public string Name_TH { get; set; }
        public string Name_ENG { get; set; }
        public int Star { get; set; }
        public bool Active { get; set; }
        public int? CustomerRequestCarPerWeek { get; set; }
        public int? CustomerRequestCarPerMonth { get; set; }
        public int? CustomerRequestCarPerYear { get; set; }
        public int? CustomerCancelPerWeek { get; set; }
        public int? CustomerCancelPerMonth { get; set; }
        public int? CustomerCancelPerYear { get; set; }
        public int? CustomerOrderingValuePerWeek { get; set; }
        public int? CustomerOrderingValuePerMonth { get; set; }
        public int? CustomerOrderingValuePerYear { get; set; }
        public int? CustomerFine { get; set; }
        public int? CustomerDiscount { get; set; }

        public int? DriverAcceptJobPerWeek { get; set; }
        public int? DriverAcceptJobPerMonth { get; set; }
        public int? DriverAcceptJobPerYear { get; set; }
        public int? DriverCancelPerWeek { get; set; }
        public int? DriverCancelPerMonth { get; set; }
        public int? DriverCancelPerYear { get; set; }
        public int? DriverComplaintPerWeek { get; set; }
        public int? DriverComplaintPerMonth { get; set; }
        public int? DriverComplaintPerYear { get; set; }
        public int? DriverRejectPerWeek { get; set; }
        public int? DriverRejectPerMonth { get; set; }
        public int? DriverRejectPerYear { get; set; }
        public int? DriverCommission { get; set; }
        public int? DriverDiscount { get; set; }
        public int? DriverFine { get; set; }
        public string Conditions { get; set; }
    }
    public class UpdateUserLevelCommandHandler : IRequestHandler<UpdateUserLevelCommand, Response<int>>
    {
        private readonly IUserLevelRepositoryAsync _userLevelRepository;
        private readonly IUserLevelConditionRepositoryAsync _userLevelConditionRepository;
        private readonly ICustomerLevelCharacteristicRepositoryAsync _customerLevelCharacteristicRepository;
        private readonly IDriverLevelCharacteristicRepositoryAsync _driverLevelCharacteristicRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;

        public UpdateUserLevelCommandHandler(IUserLevelRepositoryAsync userLevelRepository, IUserLevelConditionRepositoryAsync userLevelConditionRepository
            , ICustomerLevelCharacteristicRepositoryAsync customerLevelCharacteristicRepository, IDriverLevelCharacteristicRepositoryAsync driverLevelCharacteristicRepository, IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _userLevelRepository = userLevelRepository;
            _applicationLogRepository = applicationLogRepository;
            _customerLevelCharacteristicRepository = customerLevelCharacteristicRepository;
            _driverLevelCharacteristicRepository = driverLevelCharacteristicRepository;
            _userLevelConditionRepository = userLevelConditionRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(UpdateUserLevelCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var userLevel = (await _userLevelRepository.FindByCondition(x => x.Id == request.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (userLevel == null)
                {
                    throw new ApiException($"UserLevel Not Found.");
                }
                bool datalevel = (await _userLevelRepository.FindByCondition(x => x.Level.Equals(request.Level) && x.Id != request.Id && x.Module == request.Module).ConfigureAwait(false)).AsQueryable().Any();
 
                if (datalevel == true)
                {
                    throw new ApiException($"Duplicate Data");
                }
                else
                {
                    userLevel.Module = request.Module;
                    userLevel.Level = request.Level;
                    userLevel.Name_TH = request.Name_TH;
                    userLevel.Name_ENG = request.Name_ENG;
                    userLevel.Star = request.Star;
                    userLevel.Active = request.Active;
                    userLevel.CreatedBy = request.UserId != null ? request.UserId.GetValueOrDefault().ToString() : "";
                    userLevel.Created = DateTime.Now;
                    userLevel.ModifiedBy = request.UserId != null ? request.UserId.GetValueOrDefault().ToString() : "";
                    userLevel.Modified = DateTime.Now;
                    if (userLevel.Conditions == null || userLevel.Conditions.Count() == 0)
                    {
                        userLevel.Conditions = new List<UserLevelCondition>();
                    }
                    //else
                    //{
                    //    userLevel.Conditions.Clear();
                    //    await _userLevelRepository.UpdateAsync(userLevel);
                    //}
                    //(await _userLevelConditionRepository.CreateSQLQuery("DELETE UserLevelCondition where UserLevelId = '" + userLevel.Id.ToString() + "'").ConfigureAwait(false)).ExecuteUpdate();
                    var conditions = JsonConvert.DeserializeObject<List<UserLevelCondition>>(request.Conditions);
                    int i = 0;
                    foreach (UserLevelCondition condition in conditions)
                    {
                        i = i + 1;
                        var existCondition = userLevel.Conditions.Where(x => x.Sequence == i).FirstOrDefault();
                        if (existCondition == null)
                        {
                            condition.UserLevel = userLevel;
                            condition.Sequence = i;
                            condition.Active = true;
                            condition.CreatedBy = request.UserId != null ? request.UserId.GetValueOrDefault().ToString() : "";
                            condition.Created = DateTime.Now;
                            condition.ModifiedBy = request.UserId != null ? request.UserId.GetValueOrDefault().ToString() : "";
                            condition.Modified = DateTime.Now;
                            userLevel.Conditions.Add(condition);
                        }
                        else
                        {
                            existCondition.Characteristics_ENG = condition.Characteristics_ENG;
                            existCondition.Characteristics_TH = condition.Characteristics_TH;
                            existCondition.Active = true;
                            existCondition.ModifiedBy = request.UserId != null ? request.UserId.GetValueOrDefault().ToString() : "";
                            existCondition.Modified = DateTime.Now;
                        }
                    }
                    if (request.Module == "customer")
                    {
                        var customerLevelCharacteristic = (await _customerLevelCharacteristicRepository.FindByCondition(x => x.Level == request.Level).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                        if (customerLevelCharacteristic != null)
                        {
                            customerLevelCharacteristic.Star = request.Star;
                            customerLevelCharacteristic.RequestCarPerWeek = request.CustomerRequestCarPerWeek == null ? 0 : request.CustomerRequestCarPerWeek.Value;
                            customerLevelCharacteristic.RequestCarPerMonth = request.CustomerRequestCarPerMonth == null ? 0 : request.CustomerRequestCarPerMonth.Value;
                            customerLevelCharacteristic.RequestCarPerYear = request.CustomerRequestCarPerYear == null ? 0 : request.CustomerRequestCarPerYear.Value;
                            customerLevelCharacteristic.CancelPerWeek = request.CustomerCancelPerWeek == null ? 0 : request.CustomerCancelPerWeek.Value;
                            customerLevelCharacteristic.CancelPerMonth = request.CustomerCancelPerMonth == null ? 0 : request.CustomerCancelPerMonth.Value;
                            customerLevelCharacteristic.CancelPerYear = request.CustomerCancelPerYear == null ? 0 : request.CustomerCancelPerYear.Value;
                            customerLevelCharacteristic.OrderingValuePerWeek = request.CustomerOrderingValuePerWeek == null ? 0 : request.CustomerOrderingValuePerWeek.Value;
                            customerLevelCharacteristic.OrderingValuePerMonth = request.CustomerOrderingValuePerMonth == null ? 0 : request.CustomerOrderingValuePerMonth.Value;
                            customerLevelCharacteristic.OrderingValuePerYear = request.CustomerOrderingValuePerYear == null ? 0 : request.CustomerOrderingValuePerYear.Value;
                            customerLevelCharacteristic.Fine = request.CustomerFine == null ? 0 : request.CustomerFine.Value;
                            customerLevelCharacteristic.Discount = request.CustomerDiscount == null ? 0 : request.CustomerDiscount.Value;
                            customerLevelCharacteristic.CreatedBy = request.UserId != null ? request.UserId.GetValueOrDefault().ToString() : "";
                            customerLevelCharacteristic.Created = DateTime.Now;
                            customerLevelCharacteristic.ModifiedBy = request.UserId != null ? request.UserId.GetValueOrDefault().ToString() : "";
                            customerLevelCharacteristic.Modified = DateTime.Now;

                            await _customerLevelCharacteristicRepository.UpdateAsync(customerLevelCharacteristic);
                        }
                        else
                        {
                            CustomerLevelCharacteristic newLevelCharacteristic = new CustomerLevelCharacteristic()
                            {
                                Level = request.Level,
                                Star = request.Star,
                                RequestCarPerWeek = request.CustomerRequestCarPerWeek == null ? 0 : request.CustomerRequestCarPerWeek.Value,
                                RequestCarPerMonth = request.CustomerRequestCarPerMonth == null ? 0 : request.CustomerRequestCarPerMonth.Value,
                                RequestCarPerYear = request.CustomerRequestCarPerYear == null ? 0 : request.CustomerRequestCarPerYear.Value,
                                CancelPerWeek = request.CustomerCancelPerWeek == null ? 0 : request.CustomerCancelPerWeek.Value,
                                CancelPerMonth = request.CustomerCancelPerMonth == null ? 0 : request.CustomerCancelPerMonth.Value,
                                CancelPerYear = request.CustomerCancelPerYear == null ? 0 : request.CustomerCancelPerYear.Value,
                                OrderingValuePerWeek = request.CustomerOrderingValuePerWeek == null ? 0 : request.CustomerOrderingValuePerWeek.Value,
                                OrderingValuePerMonth = request.CustomerOrderingValuePerMonth == null ? 0 : request.CustomerOrderingValuePerMonth.Value,
                                OrderingValuePerYear = request.CustomerOrderingValuePerYear == null ? 0 : request.CustomerOrderingValuePerYear.Value,
                                Fine = request.CustomerFine == null ? 0 : request.CustomerFine.Value,
                                Discount = request.CustomerDiscount == null ? 0 : request.CustomerDiscount.Value,
                                CreatedBy = request.UserId != null ? request.UserId.GetValueOrDefault().ToString() : "",
                                Created = DateTime.Now,
                                ModifiedBy = request.UserId != null ? request.UserId.GetValueOrDefault().ToString() : "",
                                Modified = DateTime.Now,
                            };
                            await _customerLevelCharacteristicRepository.AddAsync(newLevelCharacteristic);
                        }
                    }
                    else if (request.Module == "driver")
                    {
                        var driverLevelCharacteristic = (await _driverLevelCharacteristicRepository.FindByCondition(x => x.Level == request.Level).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                        if (driverLevelCharacteristic != null)
                        {
                            driverLevelCharacteristic.Star = request.Star;
                            driverLevelCharacteristic.AcceptJobPerWeek = request.DriverAcceptJobPerWeek == null ? 0 : request.DriverAcceptJobPerWeek.Value;
                            driverLevelCharacteristic.AcceptJobPerMonth = request.DriverAcceptJobPerMonth == null ? 0 : request.DriverAcceptJobPerMonth.Value;
                            driverLevelCharacteristic.AcceptJobPerYear = request.DriverAcceptJobPerYear == null ? 0 : request.DriverAcceptJobPerYear.Value;
                            driverLevelCharacteristic.CancelPerWeek = request.DriverCancelPerWeek == null ? 0 : request.DriverCancelPerWeek.Value;
                            driverLevelCharacteristic.CancelPerMonth = request.DriverCancelPerMonth == null ? 0 : request.DriverCancelPerMonth.Value;
                            driverLevelCharacteristic.CancelPerYear = request.DriverCancelPerYear == null ? 0 : request.DriverCancelPerYear.Value;
                            driverLevelCharacteristic.ComplaintPerWeek = request.DriverComplaintPerWeek == null ? 0 : request.DriverComplaintPerWeek.Value;
                            driverLevelCharacteristic.ComplaintPerMonth = request.DriverComplaintPerMonth == null ? 0 : request.DriverComplaintPerMonth.Value;
                            driverLevelCharacteristic.ComplaintPerYear = request.DriverComplaintPerYear == null ? 0 : request.DriverComplaintPerYear.Value;
                            driverLevelCharacteristic.RejectPerWeek = request.DriverRejectPerWeek == null ? 0 : request.DriverRejectPerWeek.Value;
                            driverLevelCharacteristic.RejectPerMonth = request.DriverRejectPerMonth == null ? 0 : request.DriverRejectPerMonth.Value;
                            driverLevelCharacteristic.RejectPerYear = request.DriverRejectPerYear == null ? 0 : request.DriverRejectPerYear.Value;
                            driverLevelCharacteristic.Discount = request.DriverDiscount == null ? 0 : request.DriverDiscount.Value;
                            driverLevelCharacteristic.Commission = request.DriverCommission == null ? 0 : request.DriverCommission.Value;
                            driverLevelCharacteristic.Fine = request.DriverFine == null ? 0 : request.DriverFine.Value;
                            driverLevelCharacteristic.ModifiedBy = request.UserId != null ? request.UserId.GetValueOrDefault().ToString() : "";
                            driverLevelCharacteristic.Modified = DateTime.Now;
                            await _driverLevelCharacteristicRepository.UpdateAsync(driverLevelCharacteristic);
                        }
                        else
                        {
                            DriverLevelCharacteristic newLevelCharacteristic = new DriverLevelCharacteristic()
                            {
                                Level = request.Level,
                                Star = request.Star,
                                AcceptJobPerWeek = request.DriverAcceptJobPerWeek == null ? 0 : request.DriverAcceptJobPerWeek.Value,
                                AcceptJobPerMonth = request.DriverAcceptJobPerMonth == null ? 0 : request.DriverAcceptJobPerMonth.Value,
                                AcceptJobPerYear = request.DriverAcceptJobPerYear == null ? 0 : request.DriverAcceptJobPerYear.Value,
                                CancelPerWeek = request.DriverCancelPerWeek == null ? 0 : request.DriverCancelPerWeek.Value,
                                CancelPerMonth = request.DriverCancelPerMonth == null ? 0 : request.DriverCancelPerMonth.Value,
                                CancelPerYear = request.DriverCancelPerYear == null ? 0 : request.DriverCancelPerYear.Value,
                                ComplaintPerWeek = request.DriverComplaintPerWeek == null ? 0 : request.DriverComplaintPerWeek.Value,
                                ComplaintPerMonth = request.DriverComplaintPerMonth == null ? 0 : request.DriverComplaintPerMonth.Value,
                                ComplaintPerYear = request.DriverComplaintPerYear == null ? 0 : request.DriverComplaintPerYear.Value,
                                RejectPerWeek = request.DriverRejectPerWeek == null ? 0 : request.DriverRejectPerWeek.Value,
                                RejectPerMonth = request.DriverRejectPerMonth == null ? 0 : request.DriverRejectPerMonth.Value,
                                RejectPerYear = request.DriverRejectPerYear == null ? 0 : request.DriverRejectPerYear.Value,
                                Discount = request.DriverDiscount == null ? 0 : request.DriverDiscount.Value,
                                Commission = request.DriverCommission == null ? 0 : request.DriverCommission.Value,
                                Fine = request.DriverFine == null ? 0 : request.DriverFine.Value,
                                CreatedBy = request.UserId != null ? request.UserId.GetValueOrDefault().ToString() : "",
                                Created = DateTime.Now,
                                ModifiedBy = request.UserId != null ? request.UserId.GetValueOrDefault().ToString() : "",
                                Modified = DateTime.Now,
                            };
                            await _driverLevelCharacteristicRepository.AddAsync(newLevelCharacteristic);
                        }
                    }
                    await _userLevelRepository.UpdateAsync(userLevel);

                    var log = new CreateAppLog(_applicationLogRepository);
                    log.CreateLog("User", "UserLevel", "Edit", Newtonsoft.Json.JsonConvert.SerializeObject(request), request.UserId.ToString());
                    return new Response<int>(userLevel.Id);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
