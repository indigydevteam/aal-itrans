﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.Country;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.Countries.Queries
{
    public class GetCountryByIdQuery : IRequest<Response<CountryViewModel>>
    {
        public int Id { get; set; }
    }
    public class GetCountryByIdQueryHandler : IRequestHandler<GetCountryByIdQuery, Response<CountryViewModel>>
    {
        private readonly ICountryRepositoryAsync _countryRepository;
        private readonly IMapper _mapper;
        public GetCountryByIdQueryHandler(ICountryRepositoryAsync countryRepository, IMapper mapper)
        {
            _countryRepository = countryRepository;
            _mapper = mapper;
        }
        public async Task<Response<CountryViewModel>> Handle(GetCountryByIdQuery request, CancellationToken cancellationToken)
        {
            var countryObject = (await _countryRepository.FindByCondition(x => x.Id.Equals(request.Id)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            return new Response<CountryViewModel>(_mapper.Map<CountryViewModel>(countryObject));
        }
    }
}
