﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.Country;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.Countries.Queries
{
    public class GetAllCountryQuery : IRequest<Response<IEnumerable<CountryViewModel>>>
    {

    }
    public class GetAllCountryQueryHandler : IRequestHandler<GetAllCountryQuery, Response<IEnumerable<CountryViewModel>>>
    {
        private readonly ICountryRepositoryAsync _countryRepository;
        private readonly IMapper _mapper;
        public GetAllCountryQueryHandler(ICountryRepositoryAsync countryRepository, IMapper mapper)
        {
            _countryRepository = countryRepository;
            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<CountryViewModel>>> Handle(GetAllCountryQuery request, CancellationToken cancellationToken)
        {
            var country = await _countryRepository.FindByCondition(x => x.Active == true);
            var countryViewModel = _mapper.Map<IEnumerable<CountryViewModel>>(country);
            return new Response<IEnumerable<CountryViewModel>>(countryViewModel);
        }
    }
}
