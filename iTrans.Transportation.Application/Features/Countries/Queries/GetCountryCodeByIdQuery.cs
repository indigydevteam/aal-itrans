﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.Country;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.Countries.Queries
{
    public class GetCountryCodeByIdQuery : IRequest<Response<IEnumerable<CountryCodeViewModel>>>
    {
        public int Id { get; set; }
    }
    public class GetCountryCodeByIdQueryHandler : IRequestHandler<GetCountryCodeByIdQuery, Response<IEnumerable<CountryCodeViewModel>>>
    {
        private readonly ICountryRepositoryAsync _countryRepository;
        private readonly IMapper _mapper;
        public GetCountryCodeByIdQueryHandler(ICountryRepositoryAsync countryRepository, IMapper mapper)
        {
            _countryRepository = countryRepository;
            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<CountryCodeViewModel>>> Handle(GetCountryCodeByIdQuery request, CancellationToken cancellationToken)
        {
            var country = (await _countryRepository.FindByCondition(x=> x.Active == true && x.Id == request.Id).ConfigureAwait(false)).AsQueryable().ToList();
            var result = _mapper.Map<IEnumerable<CountryCodeViewModel>>(country);

            return new Response<IEnumerable<CountryCodeViewModel>>(result);
            //return new Response<IEnumerable<string>>(countryViewModel.Select(x => x.code));
        }
    }
}
