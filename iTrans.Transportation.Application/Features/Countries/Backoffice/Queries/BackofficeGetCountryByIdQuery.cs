﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.Country.Backoffice;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.Countries.Backoffice.Queries
{
    public class BackofficeGetCountryByIdQuery : IRequest<Response<CountryViewModel>>
    {
        public int Id { get; set; }
    }
    public class BackofficeGetCountryByIdQueryHandler : IRequestHandler<BackofficeGetCountryByIdQuery, Response<CountryViewModel>>
    {
        private readonly ICountryRepositoryAsync _countryRepository;
        private readonly IMapper _mapper;
        public BackofficeGetCountryByIdQueryHandler(ICountryRepositoryAsync countryRepository, IMapper mapper)
        {
            _countryRepository = countryRepository;
            _mapper = mapper;
        }
        public async Task<Response<CountryViewModel>> Handle(BackofficeGetCountryByIdQuery request, CancellationToken cancellationToken)
        {
            var countryObject = (await _countryRepository.FindByCondition(x => x.Id.Equals(request.Id)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            return new Response<CountryViewModel>(_mapper.Map<CountryViewModel>(countryObject));
        }
    }
}
