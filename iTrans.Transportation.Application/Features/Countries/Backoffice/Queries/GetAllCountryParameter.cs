﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.Country.Backoffice;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;

namespace iTrans.Transportation.Application.Features.Countries.Backoffice.Queries
{
    public class GetAllCountryParameter : IRequest<PagedResponse<IEnumerable<CountryViewModel>>>
    {
        public string Search { set; get; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
    }
    public class GetAllCountryParameterHandler : IRequestHandler<GetAllCountryParameter, PagedResponse<IEnumerable<CountryViewModel>>>
    {
        private readonly ICountryRepositoryAsync _countryRepository;
        private readonly IMapper _mapper;
        private Expression<Func<Country, bool>> expression;
        public GetAllCountryParameterHandler(ICountryRepositoryAsync countryRepository, IMapper mapper)
        {
            _countryRepository = countryRepository;
            _mapper = mapper;
        }

        public async Task<PagedResponse<IEnumerable<CountryViewModel>>> Handle(GetAllCountryParameter request, CancellationToken cancellationToken)
        {
            var validFilter = _mapper.Map<GetAllCountryParameter>(request);
            if (validFilter.Search != null && validFilter.Search.Trim() != "")
            {
                expression = x => (x.Name_TH.Contains(validFilter.Search.Trim()) || x.Name_ENG.Contains(validFilter.Search.Trim()) || x.Code.Contains(validFilter.Search.Trim()));
            }
            int itemCount = _countryRepository.GetItemCount(expression);
         
            //Expression<Func<Country, bool>> expression = x => x.Name_TH != "";
            var country = await _countryRepository.FindByConditionWithPage(expression, validFilter.PageNumber, validFilter.PageSize);
            var countryViewModel = _mapper.Map<IEnumerable<CountryViewModel>>(country);
            return new PagedResponse<IEnumerable<CountryViewModel>>(countryViewModel, validFilter.PageNumber, validFilter.PageSize, itemCount);
        }
    }
}
