﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using iTrans.Transportation.Application.Interfaces.Repositories;
using FluentValidation;

namespace iTrans.Transportation.Application.Features.Countries.Backoffice.Queries
{
    public class BackofficeGetCountryByIdQueryValidator : AbstractValidator<BackofficeGetCountryByIdQuery>
    {
        private readonly ICountryRepositoryAsync countryRepository;

        public BackofficeGetCountryByIdQueryValidator(ICountryRepositoryAsync countryRepository)
        {
            this.countryRepository = countryRepository;
            RuleFor(p => p.Id)
               .NotEmpty().WithMessage("{PropertyName} is required.")
               .NotNull()
               .MustAsync(IsCountryExists).WithMessage("{PropertyName} not exists.");
        }

        private async Task<bool> IsCountryExists(int CountryId, CancellationToken cancellationToken)
        {
            var userObject = (await countryRepository.FindByCondition(x => x.Id.Equals(CountryId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            if (userObject != null)
            {
                return true;
            }
            return false;
        }
    }
}
