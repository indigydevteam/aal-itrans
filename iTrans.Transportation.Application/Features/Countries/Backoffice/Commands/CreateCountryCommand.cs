﻿using AutoMapper;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Interfaces.Services;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.Countries.Backoffice.Commands
{
    public partial class CreateCountryCommand : IRequest<Response<int>>
    {
        public string Name_TH { get; set; }
        public string Name_ENG { get; set; }
        public string Code { get; set; }
        public bool Active { get; set; }

    }
    public class CreateCountryCommandHandler : IRequestHandler<CreateCountryCommand, Response<int>>
    {
        private readonly ICountryRepositoryAsync _countryRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;
        public CreateCountryCommandHandler(ICountryRepositoryAsync countryRepository, IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _countryRepository = countryRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(CreateCountryCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var country = _mapper.Map<Country>(request);
                country.Created = DateTime.UtcNow;
                country.Modified = DateTime.UtcNow;
                //country.CreatedBy = "xxxxxxx";
                //country.ModifiedBy = "xxxxxxx";
                var countryObject = await _countryRepository.AddAsync(country);
                var log = new CreateAppLog(_applicationLogRepository);
                log.Create("Country", "Country", "Create", Newtonsoft.Json.JsonConvert.SerializeObject(country));
                return new Response<int>(countryObject.Id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
