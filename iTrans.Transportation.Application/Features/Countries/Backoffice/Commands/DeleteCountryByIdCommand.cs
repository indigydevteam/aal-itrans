﻿using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.Countries.Backoffice.Commands
{
    public class DeleteCountryByIdCommand : IRequest<Response<int>>
    {
        public int Id { get; set; }
        public class DeleteCountryByIdCommandHandler : IRequestHandler<DeleteCountryByIdCommand, Response<int>>
        {
            private readonly ICountryRepositoryAsync _countryRepository;
            public DeleteCountryByIdCommandHandler(ICountryRepositoryAsync countryRepository)
            {
                _countryRepository = countryRepository;
            }
            public async Task<Response<int>> Handle(DeleteCountryByIdCommand command, CancellationToken cancellationToken)
            {
                var country = (await _countryRepository.FindByCondition(x => x.Id == command.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (country == null)
                {
                    throw new ApiException($"Country Not Found.");
                }
                else
                {
                    await _countryRepository.DeleteAsync(country);
                    return new Response<int>(country.Id);
                }
            }
        }
    }
}
