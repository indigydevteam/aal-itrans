﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;

namespace iTrans.Transportation.Application.Features.Countries.Backoffice.Commands
{
    public class UpdateCountryCommand : IRequest<Response<int>>
    {
        public int Id { get; set; }
        public string Name_TH { get; set; }
        public string Name_ENG { get; set; }
        public string Code { get; set; }
        public bool Active { get; set; }
    }

    public class UpdateCountryCommandHandler : IRequestHandler<UpdateCountryCommand, Response<int>>
    {
        private readonly ICountryRepositoryAsync _countryRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;

        public UpdateCountryCommandHandler(ICountryRepositoryAsync countryRepository, IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _countryRepository = countryRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(UpdateCountryCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var country = (await _countryRepository.FindByCondition(x => x.Id == request.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (country == null)
                {
                    throw new ApiException($"Country Not Found.");
                }
                else
                {
                    country.Name_TH = request.Name_TH;
                    country.Name_ENG = request.Name_ENG;
                    country.Code = request.Code;
                    country.Active = request.Active;

                    await _countryRepository.UpdateAsync(country);
                    var log = new CreateAppLog(_applicationLogRepository);
                    log.Create("Country", "Country", "Edit", Newtonsoft.Json.JsonConvert.SerializeObject(country));
                    return new Response<int>(country.Id);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
