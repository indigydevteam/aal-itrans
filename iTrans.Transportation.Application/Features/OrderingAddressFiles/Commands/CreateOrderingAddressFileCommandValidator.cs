﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
namespace iTrans.Transportation.Application.Features.OrderingAddressFiles.Commands
{
    public class CreateOrderingAddressFileCommandValidator : AbstractValidator<CreateOrderingAddressFileCommand>
    {
        private readonly IOrderingAddressFileRepositoryAsync orderingAddressFileRepository;

        public CreateOrderingAddressFileCommandValidator(IOrderingAddressFileRepositoryAsync orderingAddressFileRepository)
        {
            this.orderingAddressFileRepository = orderingAddressFileRepository;

            RuleFor(p => p.OrderingAddressId)
                .NotNull().WithMessage("{PropertyName} is required.");
        }
    }
}
