﻿using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;

namespace iTrans.Transportation.Application.Features.OrderingAddressFiles.Commands
{
    public class DeleteOrderingAddressFileByIdCommand : IRequest<Response<int>>
    {
        public List<int> Id { get; set; }
        public Guid OrderingId { get; set; }
        public int OrderingAddressId { get; set; }
        public class DeleteOrderingAddressFileByIdCommandHandler : IRequestHandler<DeleteOrderingAddressFileByIdCommand, Response<int>>
        {
            private readonly IOrderingAddressFileRepositoryAsync _orderingAddressFileRepository;
            private readonly IConfiguration _configuration;
            public DeleteOrderingAddressFileByIdCommandHandler(IOrderingAddressFileRepositoryAsync orderingAddressFileRepository, IConfiguration configuration)
            {
                _orderingAddressFileRepository = orderingAddressFileRepository;
                _configuration = configuration;
            }
            public async Task<Response<int>> Handle(DeleteOrderingAddressFileByIdCommand command, CancellationToken cancellationToken)
            {
                var getContentPath = _configuration.GetSection("ContentPath");
                string contentPath = getContentPath.Value;

                foreach (int id in command.Id) {
                    var orderingAddressFile = (await _orderingAddressFileRepository.FindByCondition(x => x.Id == id && x.OrderingAddress.Id.Equals(command.OrderingAddressId) && x.OrderingAddress.Ordering.Id.Equals(command.OrderingId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                    if (orderingAddressFile == null)
                    {
                        throw new ApiException($"OrderingAddressFile Not Found.");
                    }
                    else
                    {
                        File.Delete(Path.Combine(contentPath, orderingAddressFile.FilePath));
                        await _orderingAddressFileRepository.DeleteAsync(orderingAddressFile);
                        
                    }
                }
                return new Response<int>(command.OrderingAddressId);
            }
        }
    }
}
