﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;

namespace iTrans.Transportation.Application.Features.OrderingAddressFiles.Commands
{
    public class UpdateOrderingAddressFileCommand : IRequest<Response<int>>
    {
        public Guid OrderingId { get; set; }
        public int OrderingAddressProductId { get; set; }
        public int OrderingAddressId { get; set; }
        public List<IFormFile> files { get; set; }
    }

    public class UpdateOrderingAddressFileCommandHandler : IRequestHandler<UpdateOrderingAddressFileCommand, Response<int>>
    {
        private readonly IOrderingAddressFileRepositoryAsync _orderingAddressFileRepository;
        private readonly IOrderingAddressRepositoryAsync _orderingAddressRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;
        private readonly IConfiguration _configuration;
        public UpdateOrderingAddressFileCommandHandler(IOrderingAddressFileRepositoryAsync orderingAddressFileRepository, IOrderingAddressRepositoryAsync orderingAddressRepository,IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper
            , IConfiguration configuration)
        {
            _orderingAddressFileRepository = orderingAddressFileRepository;
            _orderingAddressRepository = orderingAddressRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
            _configuration = configuration;
        }

        public async Task<Response<int>> Handle(UpdateOrderingAddressFileCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var getContentPath = _configuration.GetSection("ContentPath");
                string contentPath = getContentPath.Value;

                var orderingAddress = (await _orderingAddressRepository.FindByCondition(x => x.Id == request.OrderingAddressId && x.Ordering.Id.Equals(request.OrderingId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (orderingAddress == null)
                {
                    throw new ApiException($"Ordering Address  Not Found.");
                }
                var orderingAddressFiles = (await _orderingAddressFileRepository.FindByCondition(x => x.OrderingAddress.Id == request.OrderingAddressId && x.OrderingAddress.Ordering.Id.Equals(request.OrderingId)).ConfigureAwait(false)).AsQueryable().ToList();
                foreach (OrderingAddressFile orderingAddressFile in orderingAddressFiles)
                {
                    File.Delete(Path.Combine(contentPath, orderingAddressFile.FilePath));
                    await _orderingAddressFileRepository.DeleteAsync(orderingAddressFile);
                }
                if (request.files != null)
                {
                    string folderPath = contentPath + "Ordering/" + orderingAddress.Ordering.Id.ToString() + "/OrderingAddress/" + orderingAddress.Id;
                    if (!Directory.Exists(folderPath))
                    {
                        Directory.CreateDirectory(folderPath);
                    }

                    int fileCount = 0;
                    foreach (IFormFile file in request.files)
                    {
                        fileCount = fileCount + 1;
                        string filePath = Path.Combine(folderPath, file.FileName);
                        using (FileStream fileStream = new FileStream(filePath, FileMode.Create))
                        {
                            await file.CopyToAsync(fileStream);
                            FileInfo fi = new FileInfo(filePath);

                            OrderingAddressFile orderingAddressFile = new OrderingAddressFile
                            {
                                OrderingAddress = orderingAddress,
                                OrderingAddressProductId = request.OrderingAddressProductId,
                                FileName = file.FileName,
                                ContentType = file.ContentType,
                                FilePath = "Ordering/" + orderingAddress.Ordering.Id.ToString() + "/OrderingAddress/" + orderingAddress.Id + "/" + file.FileName,
                                FileEXT = fi.Extension
                            };
                            orderingAddressFile.Modified = DateTime.UtcNow;
                            var orderingAddressFileObject = await _orderingAddressFileRepository.AddAsync(orderingAddressFile);
                            var log = new CreateAppLog(_applicationLogRepository);
                            log.Create("Ordering", "Ordering Adrressfile ", "Edit", Newtonsoft.Json.JsonConvert.SerializeObject(request));
                        }
                    }
                }

                return new Response<int>(orderingAddress.Id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
