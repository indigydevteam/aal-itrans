﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;


namespace iTrans.Transportation.Application.Features.OrderingAddressFiles.Commands
{
    public class UpdateOrderingAddressFileCommandValidator : AbstractValidator<UpdateOrderingAddressFileCommand>
    {
        private readonly IOrderingAddressFileRepositoryAsync orderingAddressFileRepository;

        public UpdateOrderingAddressFileCommandValidator(IOrderingAddressFileRepositoryAsync orderingAddressFileRepository)
        {
            this.orderingAddressFileRepository = orderingAddressFileRepository;

            RuleFor(p => p.OrderingAddressId)
                .NotNull().WithMessage("{PropertyName} is required.");

        }

        private async Task<bool> IsUnique(string value, CancellationToken cancellationToken)
        {
            var orderingAddressFileObject = (await orderingAddressFileRepository.FindByCondition(x => x.FileName.ToLower() == value.ToLower()).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            if (orderingAddressFileObject != null)
            {
                return false;
            }
            return true;
        }
    }
}
