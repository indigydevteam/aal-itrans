﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.OrderingAddressFile;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.OrderingAddressFiles.Queries
{
    public class GetAllOrderingAddressFileQuery : IRequest<Response<IEnumerable<OrderingAddressFileViewModel>>>
    {
         
    }
    public class GetAllOrderingAddressFileQueryHandler : IRequestHandler<GetAllOrderingAddressFileQuery, Response<IEnumerable<OrderingAddressFileViewModel>>>
    {
        private readonly IOrderingAddressFileRepositoryAsync _orderingAddressFileRepository;
        private readonly IMapper _mapper;
        public GetAllOrderingAddressFileQueryHandler(IOrderingAddressFileRepositoryAsync orderingAddressFileRepository, IMapper mapper)
        {
            _orderingAddressFileRepository = orderingAddressFileRepository;
            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<OrderingAddressFileViewModel>>> Handle(GetAllOrderingAddressFileQuery request, CancellationToken cancellationToken)
        {
            var orderingAddressFile = await _orderingAddressFileRepository.GetAllAsync();
            var orderingAddressFileViewModel = _mapper.Map<IEnumerable<OrderingAddressFileViewModel>>(orderingAddressFile);
            return new Response<IEnumerable<OrderingAddressFileViewModel>>(orderingAddressFileViewModel);
        }
    }
}
