﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using iTrans.Transportation.Application.Interfaces.Repositories;
using FluentValidation;

namespace iTrans.Transportation.Application.Features.OrderingAddressFiles.Queries
{
    public class GetOrderingAddressFileByIdQueryValidator : AbstractValidator<GetOrderingAddressFileByIdQuery>
    {
        private readonly IOrderingAddressFileRepositoryAsync orderingAddressFileRepository;

        public GetOrderingAddressFileByIdQueryValidator(IOrderingAddressFileRepositoryAsync orderingAddressFileRepository)
        {
            this.orderingAddressFileRepository = orderingAddressFileRepository;
            RuleFor(p => p.Id)
               .NotEmpty().WithMessage("{PropertyName} is required.")
               .NotNull()
               .MustAsync(IsOrderingAddressFileExists).WithMessage("{PropertyName} not exists.");
        }

        private async Task<bool> IsOrderingAddressFileExists(int Id, CancellationToken cancellationToken)
        {
            var userObject = (await orderingAddressFileRepository.FindByCondition(x => x.Id.Equals(Id)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            if (userObject != null)
            {
                return true;
            }
            return false;
        }
    }
}
