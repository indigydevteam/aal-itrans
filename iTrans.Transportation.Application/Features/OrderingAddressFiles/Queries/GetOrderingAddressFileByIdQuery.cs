﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.OrderingAddressFile;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.OrderingAddressFiles.Queries
{
   public class GetOrderingAddressFileByIdQuery : IRequest<Response<OrderingAddressFileViewModel>>
    {
        public int Id { get; set; }
    }
    public class GetOrderingAddressFileByIdQueryHandler : IRequestHandler<GetOrderingAddressFileByIdQuery, Response<OrderingAddressFileViewModel>>
    {
        private readonly IOrderingAddressFileRepositoryAsync _orderingAddressFileRepository;
        private readonly IMapper _mapper;
        public GetOrderingAddressFileByIdQueryHandler(IOrderingAddressFileRepositoryAsync orderingAddressFileRepository, IMapper mapper)
        {
            _orderingAddressFileRepository = orderingAddressFileRepository;
            _mapper = mapper;
        }
        public async Task<Response<OrderingAddressFileViewModel>> Handle(GetOrderingAddressFileByIdQuery request, CancellationToken cancellationToken)
        {
            var orderingAddressFileObject = (await _orderingAddressFileRepository.FindByCondition(x => x.Id.Equals(request.Id)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            return new Response<OrderingAddressFileViewModel>(_mapper.Map<OrderingAddressFileViewModel>(orderingAddressFileObject));
        }
    }
}
