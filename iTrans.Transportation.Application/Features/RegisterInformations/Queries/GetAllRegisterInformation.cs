﻿using AutoMapper;
using iTrans.Transportation.Application.DTOs.RegisterInformation;
using iTrans.Transportation.Application.DTOs.UserLevel;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.RegisterInformations.Queries
{
   public class GetAllRegisterInformation : IRequest<Response<IEnumerable<RegisterInformationViewModel>>>
    {

    }
    public class GetAllRegisterInformationHandler : IRequestHandler<GetAllRegisterInformation, Response<IEnumerable<RegisterInformationViewModel>>>
    {
        private readonly IRegisterInformationRepositoryAsync _registerInformationRepository;
        private readonly IMapper _mapper;
        public GetAllRegisterInformationHandler(IRegisterInformationRepositoryAsync registerInformationRepository, IMapper mapper)
        {
            _registerInformationRepository = registerInformationRepository;
            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<RegisterInformationViewModel>>> Handle(GetAllRegisterInformation request, CancellationToken cancellationToken)
        {
            var data = await _registerInformationRepository.GetAllAsync();
            var RegisterInformationViewModell = _mapper.Map<IEnumerable<RegisterInformationViewModel>>(data);
            return new Response<IEnumerable<RegisterInformationViewModel>>(RegisterInformationViewModell);
        }
    }
}
