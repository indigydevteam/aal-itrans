﻿using AutoMapper;
using iTrans.Transportation.Application.DTOs.RegisterInformation;
using iTrans.Transportation.Application.DTOs.UserLevel;
using iTrans.Transportation.Application.Features.CarLists.Queries;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.RegisterInformations.Queries
{
   public class GetRegisterInformationById : IRequest<Response<RegisterInformationViewModel>>
    {
        public int Id { get; set; }
    }
    public class GetRegisterInformationByIdHandler : IRequestHandler<GetRegisterInformationById, Response<RegisterInformationViewModel>>
    {
        private readonly IRegisterInformationRepositoryAsync _registerInformationRepository;
        private readonly IMapper _mapper;
        public GetRegisterInformationByIdHandler(IRegisterInformationRepositoryAsync registerInformationRepository, IMapper mapper)
        {
            _registerInformationRepository = registerInformationRepository;
            _mapper = mapper;
        }
        public async Task<Response<RegisterInformationViewModel>> Handle(GetRegisterInformationById request, CancellationToken cancellationToken)
        {
            var DataObject = (await _registerInformationRepository.FindByCondition(x => x.Id.Equals(request.Id)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            return new Response<RegisterInformationViewModel>(_mapper.Map<RegisterInformationViewModel>(DataObject));
        }
    }
}
