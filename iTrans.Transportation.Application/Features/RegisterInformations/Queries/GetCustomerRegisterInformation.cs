﻿using AutoMapper;
using iTrans.Transportation.Application.DTOs.RegisterInformation;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.RegisterInformations.Queries
{
    public class GetCustomerRegisterInformation : IRequest<Response<IEnumerable<RegisterInformationViewModel>>>
    {
    }
    public class GetCustomerRegisterInformationHandler : IRequestHandler<GetCustomerRegisterInformation, Response<IEnumerable<RegisterInformationViewModel>>>
    {
        private readonly IRegisterInformationRepositoryAsync _registerInformationRepository;
        private readonly IMapper _mapper;
        public GetCustomerRegisterInformationHandler(IRegisterInformationRepositoryAsync registerInformationRepository, IMapper mapper)
        {
            _registerInformationRepository = registerInformationRepository;
            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<RegisterInformationViewModel>>> Handle(GetCustomerRegisterInformation request, CancellationToken cancellationToken)
        {
            var data = (await _registerInformationRepository.FindByCondition(x => x.Module.Equals("customer") && x.Active == true).ConfigureAwait(false)).AsQueryable().ToList().OrderBy(x => x.Sequence);
            var RegisterInformationViewModel = _mapper.Map<IEnumerable<RegisterInformationViewModel>>(data);
            return new Response<IEnumerable<RegisterInformationViewModel>>(RegisterInformationViewModel);
        }
    }
}
