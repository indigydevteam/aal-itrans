﻿using AutoMapper;
using iTrans.Transportation.Application.DTOs.Backoffice.RegisterInformation;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using iTrans.Transportation.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.RegisterInformations.Backoffice.Queries
{
    public class GetAllRegisterInformationParameter : IRequest<PagedResponse<IEnumerable<RegisterInformationViewModel>>>
    {
        public string Search { set; get; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public int Active { set; get; }
        public string Column { set; get; }
    }
    public class GetAllRegisterInformationParameterHandler : IRequestHandler<GetAllRegisterInformationParameter, PagedResponse<IEnumerable<RegisterInformationViewModel>>>
    {
        private readonly IRegisterInformationRepositoryAsync _registerInformationRepository;
        private readonly IMapper _mapper;
        private Expression<Func<RegisterInformation, bool>> expression;

        public GetAllRegisterInformationParameterHandler(IRegisterInformationRepositoryAsync registerInformationRepository, IMapper mapper)
        {
            _registerInformationRepository = registerInformationRepository;
            _mapper = mapper;
        }

        public async Task<PagedResponse<IEnumerable<RegisterInformationViewModel>>> Handle(GetAllRegisterInformationParameter request, CancellationToken cancellationToken)
        {
            var result = from x in _registerInformationRepository.GetAllAsync().Result.ToList().Where(x => x.IsDelete == false).ToList().OrderByDescending(x => x.Modified)
                         select new RegisterInformationViewModel
                         {
                             Id = x.Id,
                             Module = x.Module,
                             Title_TH = x.Title_TH != null ? x.Title_TH : "-",
                             Information_TH = x.Information_TH != null ? x.Information_TH : "-",
                             Title_ENG = x.Title_ENG != null ? x.Title_ENG : "-",
                             Information_ENG = x.Information_ENG != null ? x.Information_ENG : "-",
                             Sequence = x.Sequence,
                             Active = x.Active,
                             Created = x.Created,
                             Modified = x.Modified
                         };


            if (request.Search != null && request.Search.Trim() != "")
            {
                result = result.Where(x => x.Module.ToLower().Contains(request.Search.ToLower().Trim()) ||
                        x.Title_TH.ToLower().Contains(request.Search.ToLower().Trim()) || x.Information_TH.ToLower().Contains(request.Search.ToLower().Trim())
                        || x.Title_ENG.ToLower().Contains(request.Search.ToLower().Trim()) || x.Information_ENG.ToLower().Contains(request.Search.ToLower().Trim())
                        ).ToList();
            }
            if (request.Column == "module" && request.Active == 3)
            {
                result = result.OrderBy(x => x.Module);
            }
            if (request.Column == "module" && request.Active == 2)
            {
                result = result.OrderBy(x => x.Module);
            }
            if (request.Column == "title_TH" && request.Active == 3)
            {
                result = result.OrderBy(x => x.Title_TH);
            }
            if (request.Column == "title_TH" && request.Active == 2)
            {
                result = result.OrderByDescending(x => x.Title_TH);
            }
            if (request.Column == "information_TH" && request.Active == 3)
            {
                result = result.OrderBy(x => x.Information_TH);
            }
            if (request.Column == "information_TH" && request.Active == 2)
            {
                result = result.OrderByDescending(x => x.Information_TH);
            }
            if (request.Column == "title_ENG" && request.Active == 3)
            {
                result = result.OrderBy(x => x.Title_ENG);
            }
            if (request.Column == "title_ENG" && request.Active == 2)
            {
                result = result.OrderByDescending(x => x.Title_ENG);
            }
            if (request.Column == "information_ENG" && request.Active == 3)
            {
                result = result.OrderBy(x => x.Information_ENG);
            }
            if (request.Column == "information_ENG" && request.Active == 2)
            {
                result = result.OrderByDescending(x => x.Information_ENG);
            }
            if (request.Column == "sequence" && request.Active == 3)
            {
                result = result.OrderBy(x => x.Sequence);
            }
            if (request.Column == "sequence" && request.Active == 3)
            {
                result = result.OrderBy(x => x.Sequence);
            }
            if (request.Column == "sequence" && request.Active == 2)
            {
                result = result.OrderByDescending(x => x.Sequence);
            }
            if (request.Column == "created" && request.Active == 3)
            {
                result = result.OrderBy(x => x.Sequence);
            }
            if (request.Column == "created" && request.Active == 2)
            {
                result = result.OrderByDescending(x => x.Sequence);
            }
            if (request.Column == "modified" && request.Active == 3)
            {
                result = result.OrderBy(x => x.Sequence);
            }
            if (request.Column == "modified" && request.Active == 2)
            {
                result = result.OrderByDescending(x => x.Sequence);
            }

            var itemCount = result.Count();
            result = result.Skip((request.PageNumber - 1) * request.PageSize).Take(request.PageSize);
            return new PagedResponse<IEnumerable<RegisterInformationViewModel>>(result, request.PageNumber, request.PageSize, itemCount);
        }
    }
}
