﻿using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
namespace iTrans.Transportation.Application.Features.RegisterInformations.Backoffice.Commands
{
   public class DeleteRegisterInformationCommand : IRequest<Response<int>>
    {
        public int Id { get; set; }
        public class DeleteRegisterDocumentByIdCommandHandler : IRequestHandler<DeleteRegisterInformationCommand, Response<int>>
        {
            private readonly IRegisterInformationRepositoryAsync _registerImformationRepository;
            public DeleteRegisterDocumentByIdCommandHandler(IRegisterInformationRepositoryAsync registerInformationRepository)
            {
                _registerImformationRepository = registerInformationRepository;
            }
            public async Task<Response<int>> Handle(DeleteRegisterInformationCommand command, CancellationToken cancellationToken)
            {
                var data = (await _registerImformationRepository.FindByCondition(x => x.Id == command.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (data == null)
                {
                    throw new ApiException($"RegisterImformation Not Found.");
                }
                else
                {
                    await _registerImformationRepository.DeleteAsync(data);
                    return new Response<int>(data.Id);
                }
            }
        }
    }
}