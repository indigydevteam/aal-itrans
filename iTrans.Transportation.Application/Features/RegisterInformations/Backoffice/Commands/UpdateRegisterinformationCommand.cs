﻿using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.RegisterInformations.Backoffice.Commands
{
   public class UpdateRegisterinformationCommand : IRequest<Response<int>>
    {
        public Guid? UserId { get; set; }
        public int Id { get; set; }
        public virtual string Module { get; set; }
        public virtual string Title_TH { get; set; }
        public virtual string Information_TH { get; set; }
        public virtual string Title_ENG { get; set; }
        public virtual string Information_ENG { get; set; }
        public virtual string Picture { get; set; }
        public virtual int Sequence { get; set; }
        public virtual bool Active { get; set; }
    }
    public class UpdateRegisterInformationCommandHandler : IRequestHandler<UpdateRegisterinformationCommand, Response<int>>
    {
        private readonly IRegisterInformationRepositoryAsync _registerImformationRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;

        public UpdateRegisterInformationCommandHandler(IRegisterInformationRepositoryAsync registerInformationRepository,IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _registerImformationRepository = registerInformationRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(UpdateRegisterinformationCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var data = (await _registerImformationRepository.FindByCondition(x => x.Id == request.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (data == null)
                {
                    throw new ApiException($"RegisterInformation Not Found.");
                }
                else
                {
                    data.Module = request.Module;
                    data.Information_TH = request.Information_TH;
                    data.Information_ENG = request.Information_ENG;
                    data.Title_TH = request.Title_TH;
                    data.Title_ENG = request.Title_ENG;
                    data.Active = request.Active;
                    data.Sequence = request.Sequence;
                    data.Modified = DateTime.UtcNow;
                    data.ModifiedBy = request.UserId == null ? "" : request.UserId.Value.ToString();

                    await _registerImformationRepository.UpdateAsync(data);
                    var log = new CreateAppLog(_applicationLogRepository);
                    log.CreateLog("RegisterInformation", "Register Information", "Edit", Newtonsoft.Json.JsonConvert.SerializeObject(request), request.UserId.ToString());
                    return new Response<int>(data.Id);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}