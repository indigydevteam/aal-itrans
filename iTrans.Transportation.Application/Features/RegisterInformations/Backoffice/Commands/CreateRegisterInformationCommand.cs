﻿using AutoMapper;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using iTrans.Transportation.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.RegisterInformations.Backoffice.Commands
{
   public partial class CreateRegisterInformationCommand : IRequest<Response<int>>
    {
        public virtual string Module { get; set; }
        public virtual string Title_TH { get; set; }
        public virtual string Information_TH { get; set; }
        public virtual string Title_ENG { get; set; }
        public virtual string Information_ENG { get; set; }
        public virtual string Picture { get; set; }
        public virtual int Sequence { get; set; }
        public virtual bool Active { get; set; }
    }
    public class CreateRegisterInformationCommandHandler : IRequestHandler<CreateRegisterInformationCommand, Response<int>>
    {
        private readonly IRegisterInformationRepositoryAsync _registerImformationRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;
        public CreateRegisterInformationCommandHandler(IRegisterInformationRepositoryAsync registerInformationRepository,IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _registerImformationRepository = registerInformationRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(CreateRegisterInformationCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var data = _mapper.Map<RegisterInformation>(request);
                data.Created = DateTime.UtcNow;
                data.Modified = DateTime.UtcNow;
                //title.CreatedBy = "xxxxxxx";
                //title.ModifiedBy = "xxxxxxx";
                var dataListObject = await _registerImformationRepository.AddAsync(data);
                var log = new CreateAppLog(_applicationLogRepository);
                log.Create("RegisterInformation", "Register Information", "Create", Newtonsoft.Json.JsonConvert.SerializeObject(data));
                return new Response<int>(dataListObject.Id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}