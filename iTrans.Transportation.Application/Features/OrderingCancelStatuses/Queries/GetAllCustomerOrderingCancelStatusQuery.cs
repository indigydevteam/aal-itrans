﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.OrderingCancelStatus;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
namespace iTrans.Transportation.Application.Features.OrderingCancelStatuses.Queries
{
    public class GetAllCustomerOrderingCancelStatusQuery : IRequest<Response<IEnumerable<OrderingCancelStatusViewModel>>>
    {

    }
    public class GetAllCustomerOrderingCancelStatusQueryHandler : IRequestHandler<GetAllCustomerOrderingCancelStatusQuery, Response<IEnumerable<OrderingCancelStatusViewModel>>>
    {
        private readonly IOrderingCancelStatusRepositoryAsync _orderingCancelStatusRepository;
        private readonly IMapper _mapper;
        public GetAllCustomerOrderingCancelStatusQueryHandler(IOrderingCancelStatusRepositoryAsync OrderingCancelStatusRepository, IMapper mapper)
        {
            _orderingCancelStatusRepository = OrderingCancelStatusRepository;
            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<OrderingCancelStatusViewModel>>> Handle(GetAllCustomerOrderingCancelStatusQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var OrderingCancelStatus = (await _orderingCancelStatusRepository.FindByCondition(x => x.Module == "customer" && x.Active == true).ConfigureAwait(false)).AsQueryable().ToList();
                var OrderingCancelStatusViewModel = _mapper.Map<IEnumerable<OrderingCancelStatusViewModel>>(OrderingCancelStatus);
                return new Response<IEnumerable<OrderingCancelStatusViewModel>>(OrderingCancelStatusViewModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
