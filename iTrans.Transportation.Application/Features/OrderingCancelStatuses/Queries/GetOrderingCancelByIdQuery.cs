﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.OrderingCancelStatus;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;


namespace iTrans.Transportation.Application.Features.OrderingCancelStatusCancelStatuses.Queries
{
    public class GetOrderingCancelStatusByIdQuery : IRequest<Response<OrderingCancelStatusViewModel>>
    {
        public int Id { get; set; }
    }
    public class GetOrderingCancelStatusByIdQueryHandler : IRequestHandler<GetOrderingCancelStatusByIdQuery, Response<OrderingCancelStatusViewModel>>
    {
        private readonly IOrderingCancelStatusRepositoryAsync _orderingCancelStatusRepository;
        private readonly IMapper _mapper;
        public GetOrderingCancelStatusByIdQueryHandler(IOrderingCancelStatusRepositoryAsync orderingCancelStatusRepository, IMapper mapper)
        {
            _orderingCancelStatusRepository = orderingCancelStatusRepository;
            _mapper = mapper;
        }
        public async Task<Response<OrderingCancelStatusViewModel>> Handle(GetOrderingCancelStatusByIdQuery request, CancellationToken cancellationToken)
        {
            var DataObject = (await _orderingCancelStatusRepository.FindByCondition(x => x.Id.Equals(request.Id) ).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            return new Response<OrderingCancelStatusViewModel>(_mapper.Map<OrderingCancelStatusViewModel>(DataObject));
        }
    }
}
