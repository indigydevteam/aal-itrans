﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.OrderingCancelStatus;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using iTrans.Transportation.Domain.Entities;
using MediatR;

namespace iTrans.Transportation.Application.Features.OrderingCancelStatuses.Queries
{
    public class GetAllOrderingCancelStatusParameter : IRequest<PagedResponse<IEnumerable<OrderingCancelStatusViewModel>>>
    {
        public string Search { set; get; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
    }
    public class GetAllOrderingCancelStatusParameterHandler : IRequestHandler<GetAllOrderingCancelStatusParameter, PagedResponse<IEnumerable<OrderingCancelStatusViewModel>>>
    {
        private readonly IOrderingCancelStatusRepositoryAsync _orderingCancelStatusRepository;
        private readonly IMapper _mapper;
        private Expression<Func<OrderingCancelStatus, bool>> expression;

        public GetAllOrderingCancelStatusParameterHandler(IOrderingCancelStatusRepositoryAsync OrderingCancelStatusRepository, IMapper mapper)
        {
            _orderingCancelStatusRepository = OrderingCancelStatusRepository;
            _mapper = mapper;
        }

        public async Task<PagedResponse<IEnumerable<OrderingCancelStatusViewModel>>> Handle(GetAllOrderingCancelStatusParameter request, CancellationToken cancellationToken)
        {
            var validFilter = _mapper.Map<GetAllOrderingCancelStatusParameter>(request);
            if (validFilter.Search != null && validFilter.Search.Trim() != "")
            {
                expression = x => (x.Name_TH.Contains(validFilter.Search.Trim()) || x.Name_ENG.Contains(validFilter.Search.Trim()) || x.Module.Contains(validFilter.Search.Trim()) || x.Sequence.ToString().Contains(validFilter.Search.Trim()));
            }
            int itemCount = _orderingCancelStatusRepository.GetItemCount(expression);
            var OrderingCancelStatus = await _orderingCancelStatusRepository.FindByConditionWithPage(expression, validFilter.PageNumber, validFilter.PageSize);

            var OrderingCancelStatusViewModel = _mapper.Map<IEnumerable<OrderingCancelStatusViewModel>>(OrderingCancelStatus);
            return new PagedResponse<IEnumerable<OrderingCancelStatusViewModel>>(OrderingCancelStatusViewModel, validFilter.PageNumber, validFilter.PageSize, itemCount);
        }
    }
}
