﻿using AutoMapper;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Interfaces.Services;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using iTrans.Transportation.Domain.Entities;
using MediatR;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.OrderingCancelStatuses.Commands
{
    public partial class CreateOrderingCancelStatusCommand : IRequest<Response<int>>
    {
        public  Guid? UserId { get; set; }
        public  string Module { get; set; }
        public  string Name_TH { get; set; }
        public  string Name_ENG { get; set; }
        public  int Sequence { get; set; }
        public  bool Active { get; set; }
    }
    public class CreateOrderingCancelStatusCommandHandler : IRequestHandler<CreateOrderingCancelStatusCommand, Response<int>>
    {
        private readonly IOrderingCancelStatusRepositoryAsync _OrderingCancelStatusRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;
        public CreateOrderingCancelStatusCommandHandler(IOrderingCancelStatusRepositoryAsync OrderingCancelStatusRepository,IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _OrderingCancelStatusRepository = OrderingCancelStatusRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(CreateOrderingCancelStatusCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var OrderingCancelStatus = _mapper.Map<OrderingCancelStatus>(request);
                OrderingCancelStatus.Created = DateTime.UtcNow;
                OrderingCancelStatus.Modified = DateTime.UtcNow;
                //title.CreatedBy = "xxxxxxx";
                //title.ModifiedBy = "xxxxxxx";
                var OrderingCancelStatusObject = await _OrderingCancelStatusRepository.AddAsync(OrderingCancelStatus);
                var log = new CreateAppLog(_applicationLogRepository);
                log.CreateLog("Ordering", "Ordering CancelStatus ","Create", Newtonsoft.Json.JsonConvert.SerializeObject(request),request.UserId.ToString());
                return new Response<int>(OrderingCancelStatusObject.Id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
