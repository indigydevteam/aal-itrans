﻿using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.OrderingCancelStatuses.Commands
{
    public class DeleteOrderingCancelStatusCommand : IRequest<Response<int>>
    {
        public int Id { get; set; }
        public class DeleteOrderingCancelStatusCommandHandler : IRequestHandler<DeleteOrderingCancelStatusCommand, Response<int>>
        {
            private readonly IOrderingCancelStatusRepositoryAsync _OrderingCancelStatusRepository;
            public DeleteOrderingCancelStatusCommandHandler(IOrderingCancelStatusRepositoryAsync OrderingCancelStatusRepository)
            {
                _OrderingCancelStatusRepository = OrderingCancelStatusRepository;
            }
            public async Task<Response<int>> Handle(DeleteOrderingCancelStatusCommand command, CancellationToken cancellationToken)
            {
                var OrderingCancelStatus = (await _OrderingCancelStatusRepository.FindByCondition(x => x.Id == command.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (OrderingCancelStatus == null)
                {
                    throw new ApiException($"OrderingCancelStatus Not Found.");
                }
                else
                {
                    await _OrderingCancelStatusRepository.DeleteAsync(OrderingCancelStatus);
                    return new Response<int>(OrderingCancelStatus.Id);
                }
            }
        }
    }
}
