﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;


namespace iTrans.Transportation.Application.Features.OrderingCancelStatuses.Commands
{
    public class UpdateOrderingCancelStatusCommand : IRequest<Response<int>>
    {
        public Guid? UserId { get; set; }
        public int Id { get; set; }
        public string Module { get; set; }
        public string Name_TH { get; set; }
        public string Name_ENG { get; set; }
        public int Sequence { get; set; }
        public bool Active { get; set; }
    }

    public class UpdateOrderingCancelStatusCommandHandler : IRequestHandler<UpdateOrderingCancelStatusCommand, Response<int>>
    {
        private readonly IOrderingCancelStatusRepositoryAsync _OrderingCancelStatusRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;

        public UpdateOrderingCancelStatusCommandHandler(IOrderingCancelStatusRepositoryAsync orderingCancelStatusRepository,IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _OrderingCancelStatusRepository = orderingCancelStatusRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(UpdateOrderingCancelStatusCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var OrderingCancelStatus = (await _OrderingCancelStatusRepository.FindByCondition(x => x.Id == request.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (OrderingCancelStatus == null)
                {
                    throw new ApiException($"OrderingCancelStatus Not Found.");
                }
                else
                {

                    OrderingCancelStatus.Module = request.Module;
                    OrderingCancelStatus.Name_TH = request.Name_TH;
                    OrderingCancelStatus.Name_ENG = request.Name_ENG;
                    OrderingCancelStatus.Active = request.Active;    
                    OrderingCancelStatus.Sequence = request.Sequence;
                    OrderingCancelStatus.Modified = DateTime.UtcNow;

                    await _OrderingCancelStatusRepository.UpdateAsync(OrderingCancelStatus);
                    var log = new CreateAppLog(_applicationLogRepository);
                    log.CreateLog("Ordering", "Ordering CancelStatus ", "Create", Newtonsoft.Json.JsonConvert.SerializeObject(request),request.UserId.ToString());
                    return new Response<int>(OrderingCancelStatus.Id);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}