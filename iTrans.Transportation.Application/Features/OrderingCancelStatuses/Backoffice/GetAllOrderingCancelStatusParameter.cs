﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.OrderingCancelStatus;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using iTrans.Transportation.Domain.Entities;
using MediatR;

namespace iTrans.Transportation.Application.Features.OrderingCancelStatuses.Backoffice
{
    public class GetAllOrderingCancelStatusParameter : IRequest<PagedResponse<IEnumerable<OrderingCancelStatusViewModel>>>
    {
        public string Search { set; get; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public int Active { set; get; }
        public string Column { set; get; }
    }
    public class GetAllOrderingCancelStatusParameterHandler : IRequestHandler<GetAllOrderingCancelStatusParameter, PagedResponse<IEnumerable<OrderingCancelStatusViewModel>>>
    {
        private readonly IOrderingCancelStatusRepositoryAsync _orderingCancelStatusRepository;
        private readonly IMapper _mapper;

        public GetAllOrderingCancelStatusParameterHandler(IOrderingCancelStatusRepositoryAsync OrderingCancelStatusRepository, IMapper mapper)
        {
            _orderingCancelStatusRepository = OrderingCancelStatusRepository;
            _mapper = mapper;
        }

        public async Task<PagedResponse<IEnumerable<OrderingCancelStatusViewModel>>> Handle(GetAllOrderingCancelStatusParameter request, CancellationToken cancellationToken)
        {
            //var validFilter = _mapper.Map<GetAllOrderingCancelStatus>(request);
            var result = from x in _orderingCancelStatusRepository.GetAllAsync().Result.ToList().OrderByDescending(x => x.Modified)
                         select new OrderingCancelStatusViewModel
                         {
                            Id = x.Id,
                             Module =x.Module != null ? x.Module:"-",
                             Name_TH=x.Name_TH != null ? x.Name_TH : "-",
                             Name_ENG =x.Name_ENG != null ? x.Name_ENG : "-",
                             Sequence =x.Sequence.ToString() != null ? x.Sequence : 0,
                             Active = x.Active
                         };


            if (request.Search != null && request.Search.Trim() != "")
            {
                result = result.Where(x => x.Module.Contains(request.Search.Trim()) || x.Module.Contains(request.Search.Trim()) || x.Name_TH.Contains(request.Search.Trim())
                         || x.Name_ENG.Contains(request.Search.Trim()) || x.Sequence.Equals(request.Search.Trim())).ToList();
            }
            if (request.Column == "module" && request.Active == 3)
            {
                result = result.OrderBy(x => x.Module);
            }
            if (request.Column == "module" && request.Active == 2)
            {
                result = result.OrderByDescending(x => x.Module);
            }
            if (request.Column == "name_TH" && request.Active == 3)
            {
                result = result.OrderBy(x => x.Name_TH);
            }
            if (request.Column == "name_TH" && request.Active == 2)
            {
                result = result.OrderByDescending(x => x.Name_TH);
            }
            if (request.Column == "name_ENG" && request.Active == 3)
            {
                result = result.OrderBy(x => x.Name_ENG);
            }
            if (request.Column == "name_ENG" && request.Active == 2)
            {
                result = result.OrderByDescending(x => x.Name_ENG);
            }
            if (request.Column == "sequence" && request.Active == 3)
            {
                result = result.OrderBy(x => x.Sequence);
            }
            if (request.Column == "sequence" && request.Active == 2)
            {
                result = result.OrderByDescending(x => x.Sequence);
            }

            var itemCount = result.Count();
            result = result.Skip((request.PageNumber - 1) * request.PageSize).Take(request.PageSize);
            return new PagedResponse<IEnumerable<OrderingCancelStatusViewModel>>(result, request.PageNumber, request.PageSize, itemCount);
        }
    }
}
