﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.OrderingCancelStatus;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using iTrans.Transportation.Domain.Entities;
using MediatR;

namespace iTrans.Transportation.Application.Features.OrderingCancelStatuses.Backoffice
{
    public class GetAllOrderingCancelStatus : IRequest<Response<IEnumerable<OrderingCancelStatusViewModel>>>
    {
    }
    public class GetAllOrderingCancelStatusHandler : IRequestHandler<GetAllOrderingCancelStatus, Response<IEnumerable<OrderingCancelStatusViewModel>>>
    {
        private readonly IOrderingCancelStatusRepositoryAsync _orderingCancelStatusRepository;
        private readonly IMapper _mapper;

        public GetAllOrderingCancelStatusHandler(IOrderingCancelStatusRepositoryAsync OrderingCancelStatusRepository, IMapper mapper)
        {
            _orderingCancelStatusRepository = OrderingCancelStatusRepository;
            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<OrderingCancelStatusViewModel>>> Handle(GetAllOrderingCancelStatus request, CancellationToken cancellationToken)
        {
            var result = (await _orderingCancelStatusRepository.FindByCondition(x => x.Active == true).ConfigureAwait(false)).AsQueryable().ToList();
            var orderingCancelStatuses = _mapper.Map<IEnumerable<OrderingCancelStatusViewModel>>(result);
            return new Response<IEnumerable<OrderingCancelStatusViewModel>>(orderingCancelStatuses);
        }
    }
}
