﻿using AutoMapper;
using iTrans.Transportation.Application.DTOs.Location;
using iTrans.Transportation.Application.DTOs.Country;
using iTrans.Transportation.Application.DTOs.District;
using iTrans.Transportation.Application.DTOs.Province;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using iTrans.Transportation.Application.DTOs.Subdistrict;
using iTrans.Transportation.Application.Helper;

namespace iTrans.Transportation.Application.Features.Locations
{
    public class GetLocationQuery : IRequest<Response<LocationViewModel>>
    {
        public string location { get; set; }
        public bool trim { get; set; }
    }
    public class GetLocationQueryHandler : IRequestHandler<GetLocationQuery, Response<LocationViewModel>>
    {
        private readonly ICountryRepositoryAsync _countryRepository;
        private readonly IProvinceRepositoryAsync _provinceRepository;
        private readonly IDistrictRepositoryAsync _districtRepository;
        private readonly ISubdistrictRepositoryAsync _subdistrictRepository;
        private readonly IConfiguration _configuration;
        private readonly IMapper _mapper;
        public GetLocationQueryHandler(ICountryRepositoryAsync countryRepository, IProvinceRepositoryAsync provinceRepository
            , IDistrictRepositoryAsync districtRepository, ISubdistrictRepositoryAsync subdistrictRepository, IConfiguration configuration, IMapper mapper)
        {
            _countryRepository = countryRepository;
            _provinceRepository = provinceRepository;
            _districtRepository = districtRepository;
            _subdistrictRepository = subdistrictRepository;
            _configuration = configuration;
            _mapper = mapper;
        }

        public async Task<Response<LocationViewModel>> Handle(GetLocationQuery request, CancellationToken cancellationToken)
        {
            try
            {
                int detectArea = Convert.ToInt32(_configuration.GetSection("DetectArea").Value);
                string api_key = _configuration.GetSection("GoogleAPIKEY").Value;
                if (string.IsNullOrEmpty(request.location))
                    return null;
                HttpClient hc = new HttpClient();
                var locate = request.location.Split(',');
                decimal latitude = Convert.ToDecimal(locate[0]);
                decimal longitude = Convert.ToDecimal(locate[1]);


                string url = "https://maps.googleapis.com/maps/api/geocode/json?latlng=" + latitude + "," + longitude + "&key=" + api_key + "&language=th";
                //string url2 = "https://maps.googleapis.com/maps/api/geocode/json?latlng=" + latitude + "," + longitude + "&key=" + api_key + "&language=en_us";

                List<string> output = new List<string>();
                HttpResponseMessage response = hc.GetAsync(url).Result;
                //HttpResponseMessage response2 = hc.GetAsync(url2).Result;
                LocationViewModel locationViewModle = new LocationViewModel();
                if (response.IsSuccessStatusCode)
                {
                    var responseContent = response.Content;
                    string s = responseContent.ReadAsStringAsync().Result;

                    var model = JsonConvert.DeserializeObject<Data>(s);
                    if (model.results.Count() > 0)
                    {
                        if (model.results[0].address_components != null)
                        {
                            //int resultCount = model.results[0].address_components.Count() > 5 ? 5 : model.results[0].address_components.Count();
                            //string countryStr = (resultCount - 1) > 0 ? model.results[0].address_components[resultCount - 1].long_name.Replace("ประเทศ", "").Trim() : "";
                            //string provinceStr = (resultCount - 2) > 0 ? model.results[0].address_components[resultCount - 2].long_name.Replace("จังหวัด", "").Replace("จ.", "").Trim() : "";
                            //string districtStr = (resultCount - 3) > 0 ? model.results[0].address_components[resultCount - 3].long_name.Replace("อำเภอ", "").Replace("อ.", "").Trim() : "";
                            //string subdistrictStr = (resultCount - 4) > 0 ? model.results[0].address_components[resultCount - 4].long_name.Replace("ตำบล", "").Replace("ต.", "").Trim() : "";
                            //string postcode = "";
                            Country country = new Country();
                            Province province = new Province();
                            Subdistrict subdistrict = new Subdistrict();
                            District district = new District();
                            string road = "";
                            string alley = "";
                            string address = "";
                            string postcode = "";
                            int i = 0;
                            while (i < model.results[0].address_components.Count())
                            {
                                if (model.results[0].address_components[i].long_name.Contains("ซอย") || model.results[0].address_components[i].long_name.Contains("ซ."))
                                {
                                    alley = model.results[0].address_components[i].long_name.Replace("ซอย", "").Replace("ซ.", "").Trim();
                                }
                                else if (model.results[0].address_components[i].long_name.Contains("ถนน") || model.results[0].address_components[i].long_name.Contains("ถ."))
                                {
                                    road = model.results[0].address_components[i].long_name.Replace("ถนน", "").Replace("ถ.", "").Trim();
                                }
                                else if (model.results[0].address_components[i].long_name.Contains("ตำบล") || model.results[0].address_components[i].long_name.Contains("ต.")
                                    || model.results[0].address_components[i].long_name.Contains("แขวง"))
                                {
                                    string subdistrictStr = model.results[0].address_components[i].long_name.Replace("ตำบล", "").Replace("ต.", "").Replace("แขวง", "").Trim();
                                    subdistrict = (await _subdistrictRepository.FindByCondition(x => x.Name_TH.Contains(subdistrictStr)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                                }
                                else if (model.results[0].address_components[i].long_name.Contains("อำเภอ") || model.results[0].address_components[i].long_name.Contains("อ.")
                                    || model.results[0].address_components[i].long_name.Contains("เขต"))
                                {
                                    string districtStr = model.results[0].address_components[i].long_name.Replace("อำเภอ", "").Replace("อ.", "").Replace("เขต", "").Trim();
                                     district = (await _districtRepository.FindByCondition(x => x.Name_TH.Contains(districtStr)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                                }
                                else if (model.results[0].address_components[i].long_name.Contains("ประเทศ") )
                                {
                                    country = (await _countryRepository.FindByCondition(x => x.Name_TH.Contains(model.results[0].address_components[i].long_name.Replace("ประเทศ", "").Trim())).ConfigureAwait(false)).AsQueryable().FirstOrDefault();  
                                }
                                else
                                {
                                    var tmpProvince = (await _provinceRepository.FindByCondition(x => x.Name_TH.Contains(model.results[0].address_components[i].long_name.Replace("จังหวัด", "").Replace("จ.", "").Trim())).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                                    if(tmpProvince != null)
                                    {
                                        province = tmpProvince;
                                    }
                                    else
                                    {
                                        var tmpSubdistrict = (await _subdistrictRepository.FindByCondition(x => x.PostCode.Contains(model.results[0].address_components[i].long_name.Trim())).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                                        if(tmpSubdistrict == null)
                                        {
                                            address = model.results[0].address_components[i].long_name.Trim();
                                        }
                                    }
                                }
                                i = i + 1;
                            }

                            //Country country = (await _countryRepository.FindByCondition(x => x.Name_TH.Contains(countryStr)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                            //Province province = (await _provinceRepository.FindByCondition(x => x.Name_TH.Contains(provinceStr)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                            //District district = (await _districtRepository.FindByCondition(x => x.Name_TH.Contains(districtStr)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                            //Subdistrict subdistrict = (await _subdistrictRepository.FindByCondition(x => x.Name_TH.Contains(subdistrictStr)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                            //if (subdistrict != null) postcode = subdistrict.PostCode;

                            locationViewModle.country = _mapper.Map<CountryViewModel>(country);
                            locationViewModle.province = _mapper.Map<ProvinceViewModel>(province);
                            locationViewModle.district = _mapper.Map<DistrictViewModel>(district);
                            locationViewModle.subdistrict = _mapper.Map<SubdistrictViewModel>(subdistrict);
                            locationViewModle.postCode = (subdistrict != null) ? subdistrict.PostCode : "";
                            locationViewModle.address = address;
                            locationViewModle.road = road;
                            locationViewModle.alley = alley;
                        }

                    }
                }
                //if (response2.IsSuccessStatusCode)
                //{
                //    var responseContent = response2.Content;
                //    string s = responseContent.ReadAsStringAsync().Result;

                //    var model = JsonConvert.DeserializeObject<Data>(s);
                //    var result = model.results[1].formatted_address;
                //   output.Add(result);
                //}

                return new Response<LocationViewModel>(locationViewModle);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
    public class AddressComponent
    {
        public string long_name { get; set; }
        public string short_name { get; set; }
        public IList<string> types { get; set; }
    }

    public class Northeast
    {
        public double lat { get; set; }
        public double lng { get; set; }
    }

    public class Southwest
    {
        public double lat { get; set; }
        public double lng { get; set; }
    }

    public class Bounds
    {
        public Northeast northeast { get; set; }
        public Southwest southwest { get; set; }
    }

    public class Location
    {
        public double lat { get; set; }
        public double lng { get; set; }
    }

    public class Viewport
    {
        public Northeast northeast { get; set; }
        public Southwest southwest { get; set; }
    }

    public class Geometry
    {
        public Bounds bounds { get; set; }
        public Location location { get; set; }
        public string location_type { get; set; }
        public Viewport viewport { get; set; }
    }

    public class Result
    {
        public IList<AddressComponent> address_components { get; set; }
        public string formatted_address { get; set; }
        public Geometry geometry { get; set; }
        public string place_id { get; set; }
        public IList<string> types { get; set; }
    }

    public class Data
    {
        public IList<Result> results { get; set; }
        public string status { get; set; }
    }
}