﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.DriverAnnouncementLocation;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using iTrans.Transportation.Domain.Entities;
using MediatR;

namespace iTrans.Transportation.Application.Features.DriverAnnouncementLocations.Queries
{
    public class GetAllDriverAnnouncementLocationParameter : IRequest<PagedResponse<IEnumerable<DriverAnnouncementLocationViewModel>>>
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
    }
    public class GetAllDriverAnnouncementLocationParameterHandler : IRequestHandler<GetAllDriverAnnouncementLocationParameter, PagedResponse<IEnumerable<DriverAnnouncementLocationViewModel>>>
    {
        private readonly IDriverAnnouncementLocationRepositoryAsync _driverAnnouncementLocationRepository;
        private readonly IMapper _mapper;
        public GetAllDriverAnnouncementLocationParameterHandler(IDriverAnnouncementLocationRepositoryAsync driverAnnouncementLocationRepository, IMapper mapper)
        {
            _driverAnnouncementLocationRepository = driverAnnouncementLocationRepository;
            _mapper = mapper;
        }

        public async Task<PagedResponse<IEnumerable<DriverAnnouncementLocationViewModel>>> Handle(GetAllDriverAnnouncementLocationParameter request, CancellationToken cancellationToken)
        {
            var validFilter = _mapper.Map<GetAllDriverAnnouncementLocationParameter>(request);
            Expression<Func<DriverAnnouncementLocation, bool>> expression = x => x.DriverAnnouncement != null;
            var driverAnnouncementLocation = await _driverAnnouncementLocationRepository.FindByConditionWithPage(expression, validFilter.PageNumber, validFilter.PageSize);
            var driverAnnouncementLocationViewModel = _mapper.Map<IEnumerable<DriverAnnouncementLocationViewModel>>(driverAnnouncementLocation);
            return new PagedResponse<IEnumerable<DriverAnnouncementLocationViewModel>>(driverAnnouncementLocationViewModel, validFilter.PageNumber, validFilter.PageSize);
        }
    }
}
