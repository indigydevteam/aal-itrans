﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.DriverAnnouncementLocation;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.DriverAnnouncementLocations.Queries
{
    public class GetAllDriverAnnouncementLocationQuery : IRequest<Response<IEnumerable<DriverAnnouncementLocationViewModel>>>
    {

    }
    public class GetAllDriverAnnouncementLocationQueryHandler : IRequestHandler<GetAllDriverAnnouncementLocationQuery, Response<IEnumerable<DriverAnnouncementLocationViewModel>>>
    {
        private readonly IDriverAnnouncementLocationRepositoryAsync _driverAnnouncementLocationRepository;
        private readonly IMapper _mapper;
        public GetAllDriverAnnouncementLocationQueryHandler(IDriverAnnouncementLocationRepositoryAsync driverAnnouncementLocationRepository, IMapper mapper)
        {
            _driverAnnouncementLocationRepository = driverAnnouncementLocationRepository;
            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<DriverAnnouncementLocationViewModel>>> Handle(GetAllDriverAnnouncementLocationQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var validFilter = _mapper.Map<GetAllDriverAnnouncementLocationQuery>(request);
                var driver = await _driverAnnouncementLocationRepository.GetAllAsync();
                var driverAnnouncementLocationViewModel = _mapper.Map<IEnumerable<DriverAnnouncementLocationViewModel>>(driver);
                return new Response<IEnumerable<DriverAnnouncementLocationViewModel>>(driverAnnouncementLocationViewModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
