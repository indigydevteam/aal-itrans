﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.DriverAnnouncementLocation;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.DriverAnnouncementLocations.Queries
{
    public class GetDriverAnnouncementLocationByIdQuery : IRequest<Response<DriverAnnouncementLocationViewModel>>
    {
        public int Id { get; set; }
    }
    public class GetDriverAnnouncementLocationByIdQueryHandler : IRequestHandler<GetDriverAnnouncementLocationByIdQuery, Response<DriverAnnouncementLocationViewModel>>
    {
        private readonly IDriverAnnouncementLocationRepositoryAsync _driverAnnouncementLocationRepository;
        private readonly IMapper _mapper;
        public GetDriverAnnouncementLocationByIdQueryHandler(IDriverAnnouncementLocationRepositoryAsync driverAnnouncementLocationRepository, IMapper mapper)
        {
            _driverAnnouncementLocationRepository = driverAnnouncementLocationRepository;
            _mapper = mapper;
        }
        public async Task<Response<DriverAnnouncementLocationViewModel>> Handle(GetDriverAnnouncementLocationByIdQuery request, CancellationToken cancellationToken)
        {
            var driverObject = (await _driverAnnouncementLocationRepository.FindByCondition(x => x.Id.Equals(request.Id)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            return new Response<DriverAnnouncementLocationViewModel>(_mapper.Map<DriverAnnouncementLocationViewModel>(driverObject));
        }
    }
}
