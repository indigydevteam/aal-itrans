﻿using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.DriverAnnouncementLocations.Commands
{
    public class DeleteDriverAnnouncementLocationCommand : IRequest<Response<int>>
    {
        public int Id { get; set; }
        public class DeleteDriverAnnouncementLocationCommandHandler : IRequestHandler<DeleteDriverAnnouncementLocationCommand, Response<int>>
        {
            private readonly IDriverAnnouncementLocationRepositoryAsync _driverAnnouncementLocationRepository;
            public DeleteDriverAnnouncementLocationCommandHandler(IDriverAnnouncementLocationRepositoryAsync driverAnnouncementLocationRepository)
            {
                _driverAnnouncementLocationRepository = driverAnnouncementLocationRepository;
            }
            public async Task<Response<int>> Handle(DeleteDriverAnnouncementLocationCommand command, CancellationToken cancellationToken)
            {
                var driverAnnouncementLocation = (await _driverAnnouncementLocationRepository.FindByCondition(x => x.Id == command.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (driverAnnouncementLocation == null)
                {
                    throw new ApiException($"DriverAnnouncementLocation Not Found.");
                }
                else
                {
                    await _driverAnnouncementLocationRepository.DeleteAsync(driverAnnouncementLocation);
                    return new Response<int>(driverAnnouncementLocation.Id);
                }
            }
        }
    }
}
