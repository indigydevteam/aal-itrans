﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;

namespace iTrans.Transportation.Application.Features.DriverAnnouncementLocations.Commands
{
    public class UpdateDriverAnnouncementLocationCommand : IRequest<Response<int>>
    {
        public  int Id { get; set; }
        public  int DriverAnnouncementId { get; set; }
        public  Country Country { get; set; }
        public  Province Province { get; set; }
        public  District District { get; set; }
        public  Subdistrict Subdistrict { get; set; }
        public  string LocationType { get; set; }
        public  string Note { get; set; }
        public  int Sequence { get; set; }
        public DateTime SendDate { get; set; }
        public DateTime ReceiveDate { get; set; }

    }

    public class UpdateDriverAnnouncementLocationCommandHandler : IRequestHandler<UpdateDriverAnnouncementLocationCommand, Response<int>>
    {
        private readonly IDriverAnnouncementLocationRepositoryAsync _driverAnnouncementLocationRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;

        public UpdateDriverAnnouncementLocationCommandHandler(IDriverAnnouncementLocationRepositoryAsync driverAnnouncementLocationRepository, IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _driverAnnouncementLocationRepository = driverAnnouncementLocationRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(UpdateDriverAnnouncementLocationCommand request, CancellationToken cancellationToken)
        {
            var driverAnnouncementLocation = (await _driverAnnouncementLocationRepository.FindByCondition(x => x.Id == request.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            if (driverAnnouncementLocation == null)
            {
                throw new ApiException($"DriverAnnouncementLocation Not Found.");
            }
            else
            {
                //driverAnnouncementLocation.DriverAnnouncementId = request.DriverAnnouncementId;
                driverAnnouncementLocation.Modified = DateTime.UtcNow;
                await _driverAnnouncementLocationRepository.UpdateAsync(driverAnnouncementLocation);
                var log = new CreateAppLog(_applicationLogRepository);
                log.Create("Driver", "Driver Announcementlocation", "Edit", Newtonsoft.Json.JsonConvert.SerializeObject(driverAnnouncementLocation));
                return new Response<int>(driverAnnouncementLocation.Id);
            }
        }
    }
}
