﻿using AutoMapper;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Interfaces.Services;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using iTrans.Transportation.Domain.Entities;
using MediatR;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.DriverAnnouncementLocations.Commands
{
    public partial class CreateDriverAnnouncementLocationCommand : IRequest<Response<int>>
    {
        public  int DriverAnnouncementId { get; set; }
        public  Country Country { get; set; }
        public  Province Province { get; set; }
        public  District District { get; set; }
        public  Subdistrict Subdistrict { get; set; }
        public  string LocationType { get; set; }
        public  string Note { get; set; }
        public  int Sequence { get; set; }
        public  DateTime SendDate { get; set; }
        public  DateTime ReceiveDate { get; set; }

    }
    public class CreateDriverAnnouncementLocationCommandHandler : IRequestHandler<CreateDriverAnnouncementLocationCommand, Response<int>>
    {
        private readonly IDriverAnnouncementLocationRepositoryAsync _driverAnnouncementLocationRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;
        public CreateDriverAnnouncementLocationCommandHandler(IDriverAnnouncementLocationRepositoryAsync driverAnnouncementLocationRepository, IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _driverAnnouncementLocationRepository = driverAnnouncementLocationRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(CreateDriverAnnouncementLocationCommand request, CancellationToken cancellationToken)
        {
            var driverAnnouncementLocation = _mapper.Map<DriverAnnouncementLocation>(request);
            driverAnnouncementLocation.Created = DateTime.UtcNow;
            driverAnnouncementLocation.Modified = DateTime.UtcNow;
            var driverAnnouncementLocationObject = await _driverAnnouncementLocationRepository.AddAsync(driverAnnouncementLocation);
            var log = new CreateAppLog(_applicationLogRepository);
            log.Create("Driver", "Driver Announcementlocation", "Craete", Newtonsoft.Json.JsonConvert.SerializeObject(driverAnnouncementLocation));
            return new Response<int>(driverAnnouncementLocationObject.Id);
        }
    }
}
