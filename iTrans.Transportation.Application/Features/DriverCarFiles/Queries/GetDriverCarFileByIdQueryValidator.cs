﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using iTrans.Transportation.Application.Interfaces.Repositories;
using FluentValidation;

namespace iTrans.Transportation.Application.Features.DriverCarFiles.Queries
{
    public class GetDriverCarFileByIdQueryValidator : AbstractValidator<GetDriverCarFileByIdQuery>
    {
        private readonly IDriverCarFileRepositoryAsync driverCarFileRepository;

        public GetDriverCarFileByIdQueryValidator(IDriverCarFileRepositoryAsync driverCarFileRepository)
        {
            this.driverCarFileRepository = driverCarFileRepository;
            RuleFor(p => p.Id)
               .NotEmpty().WithMessage("{PropertyName} is required.")
               .NotNull()
               .MustAsync(IsDriverCarFileExists).WithMessage("{PropertyName} not exists.");
        }

        private async Task<bool> IsDriverCarFileExists(int Id, CancellationToken cancellationToken)
        {
            var userObject = (await driverCarFileRepository.FindByCondition(x => x.Id.Equals(Id)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            if (userObject != null)
            {
                return true;
            }
            return false;
        }
    }
}
