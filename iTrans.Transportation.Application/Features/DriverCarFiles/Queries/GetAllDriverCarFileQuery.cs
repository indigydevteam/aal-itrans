﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.DriverCarFile;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.DriverCarFiles.Queries
{
    public class GetAllDriverCarFileQuery : IRequest<Response<IEnumerable<DriverCarFileViewModel>>>
    {
         
    }
    public class GetAllDriverCarFileQueryHandler : IRequestHandler<GetAllDriverCarFileQuery, Response<IEnumerable<DriverCarFileViewModel>>>
    {
        private readonly IDriverCarFileRepositoryAsync _driverCarFileRepository;
        private readonly IMapper _mapper;
        public GetAllDriverCarFileQueryHandler(IDriverCarFileRepositoryAsync driverCarFileRepository, IMapper mapper)
        {
            _driverCarFileRepository = driverCarFileRepository;
            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<DriverCarFileViewModel>>> Handle(GetAllDriverCarFileQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var driver = await _driverCarFileRepository.GetAllAsync();
                var driverCarFileViewModel = _mapper.Map<IEnumerable<DriverCarFileViewModel>>(driver);
                return new Response<IEnumerable<DriverCarFileViewModel>>(driverCarFileViewModel);
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
    }
}
