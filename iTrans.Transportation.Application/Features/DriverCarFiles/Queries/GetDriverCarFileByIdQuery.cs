﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.DriverCarFile;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.DriverCarFiles.Queries
{
   public class GetDriverCarFileByIdQuery : IRequest<Response<DriverCarFileViewModel>>
    {
        public int Id { get; set; }
    }
    public class GetDriverCarFileByIdQueryHandler : IRequestHandler<GetDriverCarFileByIdQuery, Response<DriverCarFileViewModel>>
    {
        private readonly IDriverCarFileRepositoryAsync _driverCarFileRepository;
        private readonly IMapper _mapper;
        public GetDriverCarFileByIdQueryHandler(IDriverCarFileRepositoryAsync driverCarFileRepository, IMapper mapper)
        {
            _driverCarFileRepository = driverCarFileRepository;
            _mapper = mapper;
        }
        public async Task<Response<DriverCarFileViewModel>> Handle(GetDriverCarFileByIdQuery request, CancellationToken cancellationToken)
        {
            var driverCarFileObject = (await _driverCarFileRepository.FindByCondition(x => x.Id.Equals(request.Id)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            return new Response<DriverCarFileViewModel>(_mapper.Map<DriverCarFileViewModel>(driverCarFileObject));
        }
    }
}
