﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;

namespace iTrans.Transportation.Application.Features.DriverCarFiles.Commands
{
    public class UpdateDriverCarFileCommand : IRequest<Response<int>>
    {
        public int Id { get; set; }
        public int DriverCarId { get; set; }
        public string FileName { get; set; }
        public string ContentType { get; set; }
        public string FilePath { get; set; }
        public string FileEXT { get; set; }
        public string DocumentType { get; set; }
        public int Sequence { get; set; }
    }

    public class UpdateDriverCarFileCommandHandler : IRequestHandler<UpdateDriverCarFileCommand, Response<int>>
    {
        private readonly IDriverCarFileRepositoryAsync _driverCarFileRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;

        public UpdateDriverCarFileCommandHandler(IDriverCarFileRepositoryAsync driverCarFileRepository, IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _driverCarFileRepository = driverCarFileRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(UpdateDriverCarFileCommand request, CancellationToken cancellationToken)
        {
            var driverCarFile = (await _driverCarFileRepository.FindByCondition(x => x.Id == request.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            if (driverCarFile == null)
            {
                throw new ApiException($"DriverCarFile Not Found.");
            }
            else
            {
                //driverCarFile.DriverCar.Id = request.DriverCarId;
                driverCarFile.FileName = request.FileName;
                driverCarFile.ContentType = request.ContentType;
                driverCarFile.FilePath = request.FilePath;
                driverCarFile.FileEXT = request.FileEXT;
                driverCarFile.DocumentType = request.DocumentType;
                driverCarFile.Sequence = request.Sequence;
                driverCarFile.Modified = DateTime.UtcNow;
                await _driverCarFileRepository.UpdateAsync(driverCarFile);
                var log = new CreateAppLog(_applicationLogRepository);
                log.Create("Drivercar", "Drivercar File", "Edit", Newtonsoft.Json.JsonConvert.SerializeObject(driverCarFile));
                return new Response<int>(driverCarFile.Id);
            }
        }
    }
}
