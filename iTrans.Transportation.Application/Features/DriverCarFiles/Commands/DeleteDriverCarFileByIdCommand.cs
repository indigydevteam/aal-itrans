﻿using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
namespace iTrans.Transportation.Application.Features.DriverCarFiles.Commands
{
    public class DeleteDriverCarFileByIdCommand : IRequest<Response<int>>
    {
        public int Id { get; set; }
        public class DeleteDriverCarFileByIdCommandHandler : IRequestHandler<DeleteDriverCarFileByIdCommand, Response<int>>
        {
            private readonly IDriverCarFileRepositoryAsync _driverCarFileRepository;
            public DeleteDriverCarFileByIdCommandHandler(IDriverCarFileRepositoryAsync driverCarFileRepository)
            {
                _driverCarFileRepository = driverCarFileRepository;
            }
            public async Task<Response<int>> Handle(DeleteDriverCarFileByIdCommand command, CancellationToken cancellationToken)
            {
                var driverCarFile = (await _driverCarFileRepository.FindByCondition(x => x.Id == command.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (driverCarFile == null)
                {
                    throw new ApiException($"DriverCarFile Not Found.");
                }
                else
                {
                    await _driverCarFileRepository.DeleteAsync(driverCarFile);
                    return new Response<int>(driverCarFile.Id);
                }
            }
        }
    }
}
