﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
namespace iTrans.Transportation.Application.Features.DriverCarFiles.Commands
{
    public class CreateDriverCarFileCommandValidator : AbstractValidator<CreateDriverCarFileCommand>
    {
        private readonly IDriverCarFileRepositoryAsync driverCarFileRepository;

        public CreateDriverCarFileCommandValidator(IDriverCarFileRepositoryAsync driverCarFileRepository)
        {
            this.driverCarFileRepository = driverCarFileRepository;

            RuleFor(p => p.DriverId)
                .NotNull().WithMessage("{PropertyName} is required.");

            RuleFor(p => p.FileName)
                .NotEmpty().WithMessage("{PropertyName} is required.")
                .NotNull()
                .MustAsync(IsUnique).WithMessage("{PropertyName} already exists.");

            RuleFor(p => p.ContentType)
               .NotEmpty().WithMessage("{PropertyName} is required.")
               .NotNull();

            RuleFor(p => p.FilePath)
               .NotEmpty().WithMessage("{PropertyName} is required.")
               .NotNull();

            RuleFor(p => p.FileEXT)
               .NotEmpty().WithMessage("{PropertyName} is required.")
               .NotNull();

            RuleFor(p => p.DocumentType)
               .NotEmpty().WithMessage("{PropertyName} is required.")
               .NotNull();
        }

        private async Task<bool> IsUnique(string value, CancellationToken cancellationToken)
        {
            var driverCarFileObject = (await driverCarFileRepository.FindByCondition(x => x.FileName.ToLower() == value.ToLower()).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            if (driverCarFileObject != null)
            {
                return false;
            }
            return true;
        }
    }
}
