﻿using AutoMapper;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Interfaces.Services;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.DriverCarFiles.Commands
{
    public partial class CreateDriverCarFileCommand : IRequest<Response<int>>
    {
        public int Id { get; set; }
        public int DriverId { get; set; }
        public string FileName { get; set; }
        public  string ContentType { get; set; }
        public  string FilePath { get; set; }
        public  string FileEXT { get; set; }
        public  string DocumentType { get; set; }
        public  int Sequence { get; set; }

    }
    public class CreateDriverCarFileCommandHandler : IRequestHandler<CreateDriverCarFileCommand, Response<int>>
    {
        private readonly IDriverCarFileRepositoryAsync _driverCarFileRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;
        public CreateDriverCarFileCommandHandler(IDriverCarFileRepositoryAsync driverCarFileRepository, IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _driverCarFileRepository = driverCarFileRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(CreateDriverCarFileCommand request, CancellationToken cancellationToken)
        {
            var driverCarFile = _mapper.Map<DriverCarFile>(request);
            driverCarFile.Created = DateTime.UtcNow;
            driverCarFile.Modified = DateTime.UtcNow;
            var driverCarFileObject = await _driverCarFileRepository.AddAsync(driverCarFile);
            var log = new CreateAppLog(_applicationLogRepository);
            log.Create("Drivercar", "Drivercar File", "Create", Newtonsoft.Json.JsonConvert.SerializeObject(driverCarFile));
            return new Response<int>(driverCarFileObject.Id);
        }
    }
}
