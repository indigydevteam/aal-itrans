﻿using AutoMapper;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Interfaces.Services;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.CarTypes.Backoffice.Commands
{
    public partial class CreateCarTypeCommand : IRequest<Response<int>>
    {
        public Guid? UserId { get; set; }
        public string Name_TH { get; set; }
        public string Name_ENG { get; set; }
        public bool IsTemperatureInfo { get; set; }
        public bool IsContainer { get; set; }
        public int Sequence { get; set; }
        public bool Active { get; set; }
        public IFormFile CarTypeImage { get; set; }
        public string ContentDirectory { get; set; }
    }
    public class CreateCarTypeCommandHandler : IRequestHandler<CreateCarTypeCommand, Response<int>>
    {
        private readonly ICarTypeRepositoryAsync _carTypeRepository;
        private readonly ICarTypeFileRepositoryAsync _carTypeFileRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;
        public CreateCarTypeCommandHandler(ICarTypeRepositoryAsync carTypeRepository, IApplicationLogRepositoryAsync applicationLogRepository, ICarTypeFileRepositoryAsync carTypeFileRepository, IMapper mapper)
        {
            _carTypeRepository = carTypeRepository;
            _applicationLogRepository = applicationLogRepository;
            _carTypeFileRepository = carTypeFileRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(CreateCarTypeCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var carType = _mapper.Map<CarType>(request);
                carType.Created = DateTime.UtcNow;
                carType.Modified = DateTime.UtcNow;
                carType.IsDelete = false;
                carType.CreatedBy = request.UserId != null ? request.UserId.GetValueOrDefault().ToString() : "";
                carType.ModifiedBy = request.UserId != null ? request.UserId.GetValueOrDefault().ToString() : "";
                var carTypeObject = await _carTypeRepository.AddAsync(carType);


                //string folderPath = request.ContentDirectory + "CarType/" + request.Name_TH.ToString();
                //if (!Directory.Exists(folderPath))
                //{
                //    Directory.CreateDirectory(folderPath);
                //}
                //if (request.CarTypeImage != null)
                //{
                     
                //        string filePath = Path.Combine(folderPath, request.CarTypeImage.FileName);
                //        using (FileStream fileStream = new FileStream(filePath, FileMode.Create))
                //        {
                //            await request.CarTypeImage.CopyToAsync(fileStream);
                //            FileInfo fi = new FileInfo(filePath);

                //            CarTypeFile carTypeFile = new CarTypeFile
                //            {
                //                CarType = carType,
                //                FileName = request.CarTypeImage.FileName,
                //                ContentType = request.CarTypeImage.ContentType,
                //                FilePath = "CarType/" + request.Name_TH.ToString() + "/" + request.CarTypeImage.FileName,
                //                DirectoryPath = "CarType/" + request.Name_TH.ToString() + "/",
                //                FileEXT = fi.Extension,
                //            };
                //            await _carTypeFileRepository.AddAsync(carTypeFile);
                //        }
                //}

                var log = new CreateAppLog(_applicationLogRepository);
                log.CreateLog("Car", "Car Type", "Create", Newtonsoft.Json.JsonConvert.SerializeObject(request),request.UserId.ToString());
                return new Response<int>(carTypeObject.Id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
