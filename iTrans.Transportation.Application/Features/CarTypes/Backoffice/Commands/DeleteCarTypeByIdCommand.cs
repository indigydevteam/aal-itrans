﻿using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.CarTypes.Backoffice.Commands
{
    public class DeleteCarTypeByIdCommand : IRequest<Response<int>>
    {
        public Guid? UserId { get; set; }
        public int Id { get; set; }
        public class DeleteCarTypeByIdCommandHandler : IRequestHandler<DeleteCarTypeByIdCommand, Response<int>>
        {
            private readonly ICarTypeRepositoryAsync _carTypeRepository;
            private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
            public DeleteCarTypeByIdCommandHandler(ICarTypeRepositoryAsync carTypeRepository, IApplicationLogRepositoryAsync applicationLogRepository)
            {
                _carTypeRepository = carTypeRepository;
                _applicationLogRepository = applicationLogRepository;
            }
            public async Task<Response<int>> Handle(DeleteCarTypeByIdCommand command, CancellationToken cancellationToken)
            {
                var carType = (await _carTypeRepository.FindByCondition(x => x.Id == command.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (carType == null)
                {
                    throw new ApiException($"CarType Not Found.");
                }
                else
                {

                    carType.Active = false;
                    carType.IsDelete = true;
                    carType.Modified = DateTime.UtcNow;
                    carType.ModifiedBy = command.UserId != null ? command.UserId.GetValueOrDefault().ToString() : "";
                    await _carTypeRepository.UpdateAsync(carType);
                    var log = new CreateAppLog(_applicationLogRepository);
                    log.CreateLog("CarType", "CarType", "Delete", Newtonsoft.Json.JsonConvert.SerializeObject(command), command.UserId == null ? "-" : command.UserId.Value.ToString());
                    return new Response<int>(carType.Id);
                }
            }
        }
    }
}
