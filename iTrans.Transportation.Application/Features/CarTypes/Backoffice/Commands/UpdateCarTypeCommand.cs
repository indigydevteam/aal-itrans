﻿using System;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;
using Microsoft.AspNetCore.Http;

namespace iTrans.Transportation.Application.Features.CarTypes.Backoffice.Commands
{
    public class UpdateCarTypeCommand : IRequest<Response<int>>
    {
        public Guid? UserId { get; set; }
        public int Id { get; set; }
        public string Name_TH { get; set; }
        public string Name_ENG { get; set; }
        public bool IsTemperatureInfo { get; set; }
        public bool IsContainer { get; set; }
        public int Sequence { get; set; }
        public bool Active { get; set; }
        public IFormFile CarTypeImage { get; set; }
        public string ContentDirectory { get; set; }
    }

    public class UpdateCarTypeCommandHandler : IRequestHandler<UpdateCarTypeCommand, Response<int>>
    {
        private readonly ICarTypeRepositoryAsync _carTypeRepository;
        private readonly ICarTypeFileRepositoryAsync _carTypeFileRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;

        public UpdateCarTypeCommandHandler(ICarTypeRepositoryAsync carTypeRepository, IApplicationLogRepositoryAsync applicationLogRepository, ICarTypeFileRepositoryAsync carTypeFileRepository, IMapper mapper)
        {
            _carTypeRepository = carTypeRepository;
            _applicationLogRepository = applicationLogRepository;
            _carTypeFileRepository = carTypeFileRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(UpdateCarTypeCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var carType = (await _carTypeRepository.FindByCondition(x => x.Id == request.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (carType == null)
                {
                    throw new ApiException($"CarType Not Found.");
                }
                else
                {
                    carType.Name_TH = request.Name_TH;
                    carType.Name_ENG = request.Name_ENG;
                    carType.IsTemperatureInfo = request.IsTemperatureInfo;
                    carType.IsContainer = request.IsContainer;
                    carType.Sequence = request.Sequence;
                    carType.Active = request.Active;
                    carType.Modified = DateTime.UtcNow;
                    carType.ModifiedBy = request.UserId != null ? request.UserId.GetValueOrDefault().ToString() : "";
                    //string folderPath = request.ContentDirectory + "CarType/" + request.Name_TH.ToString();
                    //if (!Directory.Exists(folderPath))
                    //{
                    //    Directory.CreateDirectory(folderPath);
                    //}
                    //if (request.CarTypeImage != null)
                    //{
                    //    var file = _carTypeFileRepository.FindByCondition(f => f.CarType.Id.Equals(request.Id)).Result.FirstOrDefault();
                    //    if (file != null)
                    //    {

                    //        string filePath = Path.Combine(folderPath, request.CarTypeImage.FileName);
                    //        using (FileStream fileStream = new FileStream(filePath, FileMode.Create))
                    //        {
                    //            await request.CarTypeImage.CopyToAsync(fileStream);
                    //            FileInfo fi = new FileInfo(filePath);

                    //            file.CarType = carType;
                    //            file.FileName = request.CarTypeImage.FileName;
                    //            file.ContentType = request.CarTypeImage.ContentType;
                    //            file.FilePath = "CarType/" + request.Name_TH.ToString() + "/" + request.CarTypeImage.FileName;
                    //            file.DirectoryPath = "CarType/" + request.Name_TH.ToString() + "/";
                    //            file.FileEXT = fi.Extension;


                    //            await _carTypeFileRepository.UpdateAsync(file);
                    //        }
                    //    }
                    //    else
                    //    {
                    //        string filePath = Path.Combine(folderPath, request.CarTypeImage.FileName);
                    //        using (FileStream fileStream = new FileStream(filePath, FileMode.Create))
                    //        {
                    //            await request.CarTypeImage.CopyToAsync(fileStream);
                    //            FileInfo fi = new FileInfo(filePath);

                    //            CarTypeFile carTypeFile = new CarTypeFile
                    //            {
                    //                CarType = carType,
                    //                FileName = request.CarTypeImage.FileName,
                    //                ContentType = request.CarTypeImage.ContentType,
                    //                FilePath = "carType/" + request.Name_TH.ToString() + "/" + request.CarTypeImage.FileName,
                    //                DirectoryPath = "carType/" + request.Name_TH.ToString() + "/",
                    //                FileEXT = fi.Extension,

                    //            };
                    //            await _carTypeFileRepository.AddAsync(carTypeFile);
                    //        }
                    //    }
                    //}


                    await _carTypeRepository.UpdateAsync(carType);
                    var log = new CreateAppLog(_applicationLogRepository);
                    log.CreateLog("Car", "Car Type", "Edit", Newtonsoft.Json.JsonConvert.SerializeObject(request),request.UserId.ToString());
                    return new Response<int>(carType.Id);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
