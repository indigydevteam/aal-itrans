﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.CarTypes.Backoffice.Commands
{
    public class CreateCarTypeCommandValidator : AbstractValidator<CreateCarTypeCommand>
    {
        private readonly ICarTypeRepositoryAsync carTypeRepository;

        public CreateCarTypeCommandValidator(ICarTypeRepositoryAsync carTypeRepository)
        {
            this.carTypeRepository = carTypeRepository;

            RuleFor(p => p.Name_TH)
                .NotEmpty().WithMessage("{PropertyName} is required.")
                .NotNull()
                .MaximumLength(50).WithMessage("{PropertyName} must not exceed 50 characters.");


            RuleFor(p => p.Name_ENG)
                .NotEmpty().WithMessage("{PropertyName} is required.")
                .NotNull()
                .MaximumLength(50).WithMessage("{PropertyName} must not exceed 50 characters.");
        }


    }
}
