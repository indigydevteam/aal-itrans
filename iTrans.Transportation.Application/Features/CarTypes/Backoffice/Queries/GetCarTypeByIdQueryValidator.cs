﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using iTrans.Transportation.Application.Interfaces.Repositories;
using FluentValidation;

namespace iTrans.Transportation.Application.Features.CarTypes.Backoffice.Queries
{
    public class GetCarTypeByIdQueryValidator : AbstractValidator<GetCarTypeByIdQuery>
    {
        private readonly ICarTypeRepositoryAsync carTypeRepository;

        public GetCarTypeByIdQueryValidator(ICarTypeRepositoryAsync carTypeRepository)
        {
            this.carTypeRepository = carTypeRepository;
            RuleFor(p => p.Id)
               .NotEmpty().WithMessage("{PropertyName} is required.")
               .NotNull()
               .MustAsync(IsCarTypeExists).WithMessage("{PropertyName} not exists.");
        }

        private async Task<bool> IsCarTypeExists(int CarTypeId, CancellationToken cancellationToken)
        {
            var userObject = (await carTypeRepository.FindByCondition(x => x.Id.Equals(CarTypeId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            if (userObject != null)
            {
                return true;
            }
            return false;
        }
    }
}
