﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.Backoffice.CarType;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;

namespace iTrans.Transportation.Application.Features.CarTypes.Backoffice.Queries
{
    public class GetCarTypesQuery : IRequest<Response<IEnumerable<CarTypeViewModel>>>
    {
        public string Search { set; get; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public int Active { set; get; }
        public string Column { set; get; }
    }
    public class GetCarTypesQueryHandler : IRequestHandler<GetCarTypesQuery, Response<IEnumerable<CarTypeViewModel>>>
    {
        private readonly ICarTypeRepositoryAsync _carTypeRepository;
        private readonly IMapper _mapper;
        private Expression<Func<CarType, bool>> expression;
        public GetCarTypesQueryHandler(ICarTypeRepositoryAsync carTypeRepository, IMapper mapper)
        {
            _carTypeRepository = carTypeRepository;
            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<CarTypeViewModel>>> Handle(GetCarTypesQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var carType = (await _carTypeRepository.FindByCondition(x => x.IsDelete == false).ConfigureAwait(false)).AsQueryable().ToList();
                var carTypeViewModel = _mapper.Map<IEnumerable<CarTypeViewModel>>(carType);
                return new Response<IEnumerable<CarTypeViewModel>>(carTypeViewModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
