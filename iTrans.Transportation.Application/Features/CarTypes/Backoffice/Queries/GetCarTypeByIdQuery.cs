﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.CarType;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.CarTypes.Backoffice.Queries
{
    public class GetCarTypeByIdQuery : IRequest<Response<CarTypeViewModel>>
    {
        public int Id { get; set; }
    }
    public class GetCarTypeByIdQueryHandler : IRequestHandler<GetCarTypeByIdQuery, Response<CarTypeViewModel>>
    {
        private readonly ICarTypeRepositoryAsync _carTypeRepository;
        private readonly IMapper _mapper;
        public GetCarTypeByIdQueryHandler(ICarTypeRepositoryAsync carTypeRepository, IMapper mapper)
        {
            _carTypeRepository = carTypeRepository;
            _mapper = mapper;
        }
        public async Task<Response<CarTypeViewModel>> Handle(GetCarTypeByIdQuery request, CancellationToken cancellationToken)
        {
            var carTypeObject = (await _carTypeRepository.FindByCondition(x => x.Id.Equals(request.Id) ).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            if (carTypeObject == null)
            {
                throw new ApiException($"CarType Not Found.");
            }
            return new Response<CarTypeViewModel>(_mapper.Map<CarTypeViewModel>(carTypeObject));
        }
    }
}
