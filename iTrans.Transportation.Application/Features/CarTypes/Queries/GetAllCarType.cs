﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.CarType;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.CarTypes.Queries
{
    public class GetAllCarType : IRequest<Response<IEnumerable<CarTypeViewModel>>>
    {

    }
    public class GetAllCarTypeHandler : IRequestHandler<GetAllCarType, Response<IEnumerable<CarTypeViewModel>>>
    {
        private readonly ICarTypeRepositoryAsync _carTypeRepository;
        private readonly IMapper _mapper;
        public GetAllCarTypeHandler(ICarTypeRepositoryAsync carTypeRepository, IMapper mapper)
        {
            _carTypeRepository = carTypeRepository;
            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<CarTypeViewModel>>> Handle(GetAllCarType request, CancellationToken cancellationToken)
        {
            try
            {
                var carType = (await _carTypeRepository.FindByCondition(x => x.Active == true).ConfigureAwait(false)).AsQueryable().ToList();
                var carTypeViewModel = _mapper.Map<IEnumerable<CarTypeViewModel>>(carType);
                return new Response<IEnumerable<CarTypeViewModel>>(carTypeViewModel);
            }
            catch(Exception ex )
            {
                throw ex;
            }
        }
    }
}
