﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.CarType;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.CarTypes.Queries
{
    public class GetCarTypeById : IRequest<Response<CarTypeViewModel>>
    {
        public int Id { get; set; }
    }
    public class GetCarTypeByIdHandler : IRequestHandler<GetCarTypeById, Response<CarTypeViewModel>>
    {
        private readonly ICarTypeRepositoryAsync _carTypeRepository;
        private readonly IMapper _mapper;
        public GetCarTypeByIdHandler(ICarTypeRepositoryAsync carTypeRepository, IMapper mapper)
        {
            _carTypeRepository = carTypeRepository;
            _mapper = mapper;
        }
        public async Task<Response<CarTypeViewModel>> Handle(GetCarTypeById request, CancellationToken cancellationToken)
        {
            var carTypeObject = (await _carTypeRepository.FindByCondition(x => x.Id.Equals(request.Id) ).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            if (carTypeObject == null)
            {
                throw new ApiException($"CarType Not Found.");
            }
            return new Response<CarTypeViewModel>(_mapper.Map<CarTypeViewModel>(carTypeObject));
        }
    }
}
