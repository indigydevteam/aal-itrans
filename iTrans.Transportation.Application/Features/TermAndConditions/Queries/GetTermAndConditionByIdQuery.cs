﻿using AutoMapper;
using iTrans.Transportation.Application.DTOs.RegisterInformation;
using iTrans.Transportation.Application.DTOs.TermAndCondition;
using iTrans.Transportation.Application.DTOs.UserLevel;
using iTrans.Transportation.Application.Features.CarLists.Queries;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;


namespace iTrans.Transportation.Application.Features.TermAndConditions.Queries
{
    public class GetTermAndConditionByIdQuery : IRequest<Response<TermAndConditionViewModel>>
    {
        public int Id { get; set; }
    }
    public class GetTermAndConditionByIdQueryHandler : IRequestHandler<GetTermAndConditionByIdQuery, Response<TermAndConditionViewModel>>
    {
        private readonly ITermAndConditionRepositoryAsync _termAndConditionRepository;
        private readonly IMapper _mapper;
        public GetTermAndConditionByIdQueryHandler(ITermAndConditionRepositoryAsync termAndConditionRepository, IMapper mapper)
        {
            _termAndConditionRepository = termAndConditionRepository;
            _mapper = mapper;
        }
        public async Task<Response<TermAndConditionViewModel>> Handle(GetTermAndConditionByIdQuery request, CancellationToken cancellationToken)
        {
            var DataObject = (await _termAndConditionRepository.FindByCondition(x => x.Id.Equals(request.Id)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            return new Response<TermAndConditionViewModel>(_mapper.Map<TermAndConditionViewModel>(DataObject));
        }
    }
}
