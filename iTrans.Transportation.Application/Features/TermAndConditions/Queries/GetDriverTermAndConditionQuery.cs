﻿using AutoMapper;
using iTrans.Transportation.Application.DTOs.RegisterInformation;
using iTrans.Transportation.Application.DTOs.TermAndCondition;
using iTrans.Transportation.Application.DTOs.UserLevel;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.TermAndConditions.Queries
{
    public class GetDriverTermAndConditionQuery : IRequest<Response<IEnumerable<TermAndConditionViewModel>>>
    {
    }
    public class GetDriverTermAndConditionQueryHandler : IRequestHandler<GetDriverTermAndConditionQuery, Response<IEnumerable<TermAndConditionViewModel>>>
    {
        private readonly ITermAndConditionRepositoryAsync _termAndConditionRepository;
        private readonly IMapper _mapper;
        public GetDriverTermAndConditionQueryHandler(ITermAndConditionRepositoryAsync termAndConditionRepository, IMapper mapper)
        {
            _termAndConditionRepository = termAndConditionRepository;
            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<TermAndConditionViewModel>>> Handle(GetDriverTermAndConditionQuery request, CancellationToken cancellationToken)
        {
            var data = (await _termAndConditionRepository.FindByCondition(x => x.Active == true && x.Module.Trim().ToLower() == "driver" ).ConfigureAwait(false)).AsQueryable().ToList();
            List<TermAndCondition> termAndConditions = new List<TermAndCondition>();
            foreach (TermAndCondition termAndCondition in data)
            {
                var existItem = termAndConditions.Where(x => x.Section == termAndCondition.Section).FirstOrDefault();
                if (existItem == null)
                {
                    termAndConditions.Add(termAndCondition);
                }
                else
                {
                    int n;
                    var version1 = new Version(int.TryParse(termAndCondition.Version, out n) ? termAndCondition.Version + ".0" : termAndCondition.Version);
                    var version2 = new Version(int.TryParse(existItem.Version, out n) ? existItem.Version + ".0" : existItem.Version);
                    var result = version1.CompareTo(version2);
                    if (result > 0)
                    {
                        termAndConditions.Remove(existItem);
                        termAndConditions.Add(termAndCondition);
                    }
                }
            }
            var TermAndConditionViewModell = _mapper.Map<IEnumerable<TermAndConditionViewModel>>(data.OrderBy(x => x.Sequence));
            return new Response<IEnumerable<TermAndConditionViewModel>>(TermAndConditionViewModell);
        }
    }
}
