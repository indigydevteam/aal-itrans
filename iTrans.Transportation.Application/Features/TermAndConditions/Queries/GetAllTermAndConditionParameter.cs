﻿using AutoMapper;
using iTrans.Transportation.Application.DTOs.CarList;
using iTrans.Transportation.Application.DTOs.TermAndCondition;
using iTrans.Transportation.Application.DTOs.UserLevel;
using iTrans.Transportation.Application.Features.CarLists.Queries;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using iTrans.Transportation.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.TermAndConditions.Queries
{
  public  class GetAllTermAndConditionParameter : IRequest<PagedResponse<IEnumerable<TermAndConditionViewModel>>>
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
    }
    public class GetAllTermAndConditionParameterHandler : IRequestHandler<GetAllTermAndConditionParameter, PagedResponse<IEnumerable<TermAndConditionViewModel>>>
    {
        private readonly ITermAndConditionRepositoryAsync _termAndConditionRepository;
        private readonly IMapper _mapper;
        public GetAllTermAndConditionParameterHandler(ITermAndConditionRepositoryAsync termAndConditionRepository, IMapper mapper)
        {
            _termAndConditionRepository = termAndConditionRepository;
            _mapper = mapper;
        }

        public async Task<PagedResponse<IEnumerable<TermAndConditionViewModel>>> Handle(GetAllTermAndConditionParameter request, CancellationToken cancellationToken)
        {
            var validFilter = _mapper.Map<GetAllTermAndConditionParameter>(request);
            Expression<Func<TermAndCondition, bool>> expression = x => x.Name_TH != "";
            var data = await _termAndConditionRepository.FindByConditionWithPage(expression, validFilter.PageNumber, validFilter.PageSize);
            var TermAndConditionViewModel = _mapper.Map<IEnumerable<TermAndConditionViewModel>>(data);
            return new PagedResponse<IEnumerable<TermAndConditionViewModel>>(TermAndConditionViewModel, validFilter.PageNumber, validFilter.PageSize);
        }
    }
}
