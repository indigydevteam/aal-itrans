﻿using AutoMapper;
using iTrans.Transportation.Application.DTOs.RegisterInformation;
using iTrans.Transportation.Application.DTOs.TermAndCondition;
using iTrans.Transportation.Application.DTOs.UserLevel;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.TermAndConditions.Queries
{
    public class GetAllTermAndConditionQuery : IRequest<Response<IEnumerable<TermAndConditionViewModel>>>
    {
    }
    public class GetAllTermAndConditionQueryHandler : IRequestHandler<GetAllTermAndConditionQuery, Response<IEnumerable<TermAndConditionViewModel>>>
    {
        private readonly ITermAndConditionRepositoryAsync _termAndConditionRepository;
        private readonly IMapper _mapper;
        public GetAllTermAndConditionQueryHandler(ITermAndConditionRepositoryAsync termAndConditionRepository, IMapper mapper)
        {
            _termAndConditionRepository = termAndConditionRepository;
            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<TermAndConditionViewModel>>> Handle(GetAllTermAndConditionQuery request, CancellationToken cancellationToken)
        {
            var data = (await _termAndConditionRepository.FindByCondition(x => x.Active == true && x.IsPublic == true).ConfigureAwait(false)).AsQueryable().ToList();
            var TermAndConditionViewModell = _mapper.Map<IEnumerable<TermAndConditionViewModel>>(data.OrderBy(x => x.Sequence));
            return new Response<IEnumerable<TermAndConditionViewModel>>(TermAndConditionViewModell);
        }
    }
}
