﻿using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
namespace iTrans.Transportation.Application.Features.TermAndConditions.Commands
{
   public class DeleteTermAndConditionCommand : IRequest<Response<int>>
    {
        public int Id { get; set; }
        public class DeleteTermAndConditionByIdCommandHandler : IRequestHandler<DeleteTermAndConditionCommand, Response<int>>
        {
            private readonly ITermAndConditionRepositoryAsync _termAndConditionRepository;
            public DeleteTermAndConditionByIdCommandHandler(ITermAndConditionRepositoryAsync termAndConditionRepository)
            {
                _termAndConditionRepository = termAndConditionRepository;
            }
            public async Task<Response<int>> Handle(DeleteTermAndConditionCommand command, CancellationToken cancellationToken)
            {
                var data = (await _termAndConditionRepository.FindByCondition(x => x.Id == command.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (data == null)
                {
                    throw new ApiException($"TermAndCondition Not Found.");
                }
                else
                {
                    await _termAndConditionRepository.DeleteAsync(data);
                    return new Response<int>(data.Id);
                }
            }
        }
    }
}