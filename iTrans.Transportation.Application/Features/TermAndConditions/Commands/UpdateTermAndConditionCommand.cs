﻿using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.TermAndConditions.Commands
{
   public class UpdateTermAndConditionCommand : IRequest<Response<int>>
    {
        public Guid? UserId { get; set; }
        public int Id { get; set; }
        public virtual string Module { get; set; }
        public virtual string Name_TH { get; set; }
        public virtual string Name_ENG { get; set; }
        public virtual string version { get; set; }
        public virtual bool isAccept { get; set; }
        public string Section { get; set; }
        public virtual int Sequence { get; set; }
        public virtual bool Active { get; set; }
    }
    public class UpdateTermAndConditionCommandHandler : IRequestHandler<UpdateTermAndConditionCommand, Response<int>>
    {
        private readonly ITermAndConditionRepositoryAsync _termAndConditionRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;

        public UpdateTermAndConditionCommandHandler(ITermAndConditionRepositoryAsync termAndConditionRepository,IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _termAndConditionRepository = termAndConditionRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(UpdateTermAndConditionCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var data = (await _termAndConditionRepository.FindByCondition(x => x.Id == request.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (data == null)
                {
                    throw new ApiException($"TermAndCondition Not Found.");
                }
                else
                {
                    data.Module = request.Module;
                    data.Name_TH = request.Name_TH;
                    data.Name_ENG = request.Name_ENG;
                    data.Section = request.Section;
                    data.Active = request.Active;
                    data.Sequence = request.Sequence;
                    data.Version = request.version;
                    data.IsAccept = request.isAccept;

                    data.Modified = DateTime.UtcNow;

                    await _termAndConditionRepository.UpdateAsync(data);
                    var log = new CreateAppLog(_applicationLogRepository);
                    log.CreateLog("TermAndCondition", "TermAndCondition", "Edit", Newtonsoft.Json.JsonConvert.SerializeObject(data),request.UserId.ToString());
                    return new Response<int>(data.Id);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}