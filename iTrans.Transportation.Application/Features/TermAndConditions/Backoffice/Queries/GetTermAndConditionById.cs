﻿using AutoMapper;
using iTrans.Transportation.Application.DTOs.RegisterInformation;
using iTrans.Transportation.Application.DTOs.TermAndCondition;
using iTrans.Transportation.Application.DTOs.UserLevel;
using iTrans.Transportation.Application.Features.CarLists.Queries;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;


namespace iTrans.Transportation.Application.Features.TermAndConditions.Backoffice
{
    public class GetTermAndConditionById : IRequest<Response<TermAndConditionViewModel>>
    {
        public int Id { get; set; }

    }
    public class GetTermAndConditionByIdHandler : IRequestHandler<GetTermAndConditionById, Response<TermAndConditionViewModel>>
    {
        private readonly ITermAndConditionRepositoryAsync _termAndConditionRepository;
        private readonly IMapper _mapper;
        public GetTermAndConditionByIdHandler(ITermAndConditionRepositoryAsync termAndConditionRepository, IMapper mapper)
        {
            _termAndConditionRepository = termAndConditionRepository;
            _mapper = mapper;
        }
        public async Task<Response<TermAndConditionViewModel>> Handle(GetTermAndConditionById request, CancellationToken cancellationToken)
        {
            var result = (await _termAndConditionRepository.FindByCondition(x => x.Id.Equals(request.Id)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
          
            if(result != null)
            {
                // ไม่ต้องส่ง section มาเเค่เอาได้ก็ได้เเล้ว ส่งไปเเสดงจบ
                var datas = (await _termAndConditionRepository.FindByCondition(x => x.Active == true).ConfigureAwait(false)).AsQueryable().ToList();
                List<TermAndCondition> termAndConditions = new List<TermAndCondition>();
                foreach (TermAndCondition termAndCondition in datas)
                {
                    var existItem = termAndConditions.Where(x => x.Section == termAndCondition.Section).FirstOrDefault();
                    if (existItem == null)
                    {
                        termAndConditions.Add(termAndCondition);
                    }
                    else
                    {
                        int n;
                        var version1 = new Version(int.TryParse(termAndCondition.Version, out n) ? termAndCondition.Version + ".0" : termAndCondition.Version);
                        var version2 = new Version(int.TryParse(existItem.Version, out n) ? existItem.Version + ".0" : existItem.Version);
                        var results = version1.CompareTo(version2);
                        if (results > 0)
                        {
                            termAndConditions.Remove(existItem);
                            termAndConditions.Add(termAndCondition);
                        }
                    }
                }

                result.Version = String.Format("{0}.0.{1}", 1, 1);
                var term = termAndConditions.Where(x => x.Section.Contains(result.Section)).Select(x => x.Version).FirstOrDefault();
                if (term != null)
                {
                    var fist = term.Split('.').FirstOrDefault();
                    var last = Int16.Parse(term.Split('.').LastOrDefault());
                    result.Version = String.Format("{0}.0.{1}", fist, last + 1);
                }

            }
            return new Response<TermAndConditionViewModel>(_mapper.Map<TermAndConditionViewModel>(result));
        }
    }
}
