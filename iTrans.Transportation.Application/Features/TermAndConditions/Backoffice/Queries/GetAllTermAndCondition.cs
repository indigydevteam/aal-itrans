﻿using AutoMapper;
using iTrans.Transportation.Application.DTOs.RegisterInformation;
using iTrans.Transportation.Application.DTOs.TermAndCondition;
using iTrans.Transportation.Application.DTOs.UserLevel;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.TermAndConditions.Backoffice
{
    public class GetAllTermAndCondition : IRequest<Response<IEnumerable<TermAndConditionViewModel>>>
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public string Search { set; get; }
        public int Active { set; get; }
        public string Column { set; get; }
        public string Module { set; get; }
    }
    public class GetAllTermAndConditionHandler : IRequestHandler<GetAllTermAndCondition, Response<IEnumerable<TermAndConditionViewModel>>>
    {
        private readonly ITermAndConditionRepositoryAsync _termAndConditionRepository;
        private readonly IMapper _mapper;
        public GetAllTermAndConditionHandler(ITermAndConditionRepositoryAsync termAndConditionRepository, IMapper mapper)
        {
            _termAndConditionRepository = termAndConditionRepository;
            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<TermAndConditionViewModel>>> Handle(GetAllTermAndCondition request, CancellationToken cancellationToken)
        {
            var validFilter = _mapper.Map<GetAllTermAndCondition>(request);

           
            var result = from x in _termAndConditionRepository.GetAllAsync().Result.ToList().OrderByDescending(x => x.Modified)
                         where x.Section.Equals(validFilter.Module)
                         select new TermAndConditionViewModel
                         {
                             Id = x.Id,
                             Version = x.Version != null ? x.Version : "-",
                             Name_TH = x.Name_TH != null ? x.Name_TH : "-",
                             Created = x.Created.ToString("yyyy/MM/dd HH:MM") != null ? x.Created.ToString("yyyy/MM/dd HH:MM") : "-",
                             CreatedBy = x.CreatedBy != null ? x.CreatedBy : "-",
                             IsPublic = x.IsPublic,

                         };
            if (request.Column == "version" && request.Active == 3)
            {
                result = result.OrderBy(x => x.Version);
            }
            if (request.Column == "version" && request.Active == 2)
            {
                result = result.OrderByDescending(x => x.Version);
            }
            if (request.Column == "name_TH" && request.Active == 3)
            {
                result = result.OrderBy(x => x.Name_TH);
            }
            if (request.Column == "name_TH" && request.Active == 2)
            {
                result = result.OrderByDescending(x => x.Name_TH);
            }
            if (request.Column == "created" && request.Active == 3)
            {
                result = result.OrderBy(x => x.Created);
            }
            if (request.Column == "created" && request.Active == 2)
            {
                result = result.OrderByDescending(x => x.Created);
            }
            if (request.Column == "createdBy" && request.Active == 3)
            {
                result = result.OrderBy(x => x.CreatedBy);
            }
            if (request.Column == "createdBy" && request.Active == 2)
            {
                result = result.OrderByDescending(x => x.CreatedBy);
            }

            var itemCount = result.Count();
            result = result.Skip((validFilter.PageNumber - 1) * validFilter.PageSize).Take(validFilter.PageSize);

            return new PagedResponse<IEnumerable<TermAndConditionViewModel>>(result, validFilter.PageNumber, validFilter.PageSize, itemCount);
        }
    }
}
