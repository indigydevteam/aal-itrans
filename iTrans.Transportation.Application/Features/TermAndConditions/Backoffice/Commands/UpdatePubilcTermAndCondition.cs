﻿using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.TermAndConditions.Backoffice
{
    public class UpdatePubilcTermAndCondition : IRequest<Response<int>>
    {
        public Guid? UserId { get; set; }
        public int Id { get; set; }
   
    }
    public class UpdatePubilcTermAndConditionHandler : IRequestHandler<UpdatePubilcTermAndCondition, Response<int>>
    {
        private readonly ITermAndConditionRepositoryAsync _termAndConditionRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;

        public UpdatePubilcTermAndConditionHandler(ITermAndConditionRepositoryAsync termAndConditionRepository, IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _termAndConditionRepository = termAndConditionRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(UpdatePubilcTermAndCondition request, CancellationToken cancellationToken)
        {
            try
            {
                var data = (await _termAndConditionRepository.FindByCondition(x => x.Id == request.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (data == null)
                {
                    throw new ApiException($"TermAndCondition Not Found.");
                }
                else
                {
                  
                        // ไม่ต้องส่ง section มาเเค่เอาได้ก็ได้เเล้ว ส่งไปเเสดงจบ
                        var datas = (await _termAndConditionRepository.FindByCondition(x => x.Active == true).ConfigureAwait(false)).AsQueryable().ToList();
                        List<TermAndCondition> termAndConditions = new List<TermAndCondition>();
                        foreach (TermAndCondition termAndCondition in datas)
                        {
                            var existItem = termAndConditions.Where(x => x.Section == termAndCondition.Section).FirstOrDefault();
                            if (existItem == null)
                            {
                                termAndConditions.Add(termAndCondition);
                            }
                            else
                            {
                                int n;
                                var version1 = new Version(int.TryParse(termAndCondition.Version, out n) ? termAndCondition.Version + ".0" : termAndCondition.Version);
                                var version2 = new Version(int.TryParse(existItem.Version, out n) ? existItem.Version + ".0" : existItem.Version);
                                var results = version1.CompareTo(version2);
                                if (results > 0)
                                {
                                    termAndConditions.Remove(existItem);
                                    termAndConditions.Add(termAndCondition);
                                }
                            }
                        }

                        data.Version = String.Format("{0}.0.{1}", 1, 1);
                        var term = termAndConditions.Where(x => x.Section.Contains(data.Section)).Select(x => x.Version).FirstOrDefault();
                        if (term != null)
                        {
                            var fist = term.Split('.').FirstOrDefault();
                            var last = Int16.Parse(term.Split('.').LastOrDefault());
                            data.Version = String.Format("{0}.0.{1}", fist, last + 1);
                        }

                    
                    data.Active = true;
                    data.Modified = DateTime.Now;

                    await _termAndConditionRepository.UpdateAsync(data);
                    var log = new CreateAppLog(_applicationLogRepository);
                    log.CreateLog("TermAndCondition", "TermAndCondition", "Edit", Newtonsoft.Json.JsonConvert.SerializeObject(data),request.UserId.ToString());
                    return new Response<int>(data.Id);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}