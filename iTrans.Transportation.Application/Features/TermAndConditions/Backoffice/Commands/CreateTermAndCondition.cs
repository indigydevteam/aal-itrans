﻿using AutoMapper;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using iTrans.Transportation.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.TermAndConditions.Backoffice.Commands
{
    public partial class CreateTermAndCondition: IRequest<Response<int>>
    {
    
        public  Guid? UserId { get; set; }
        public  string Module { get; set; }
        public  string Name_TH { get; set; }
        public  string Name_ENG { get; set; }
        public string Section { get; set; }
        public  int Sequence { get; set; }

    }
    public class CreateTermAndConditionHandler : IRequestHandler<CreateTermAndCondition, Response<int>>
    {
        private readonly ITermAndConditionRepositoryAsync _TermAndConditionRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;
        public CreateTermAndConditionHandler(ITermAndConditionRepositoryAsync TermAndConditionRepository,IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _TermAndConditionRepository = TermAndConditionRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(CreateTermAndCondition request, CancellationToken cancellationToken)
        {
            try
            {
                var TermAndCondition = _mapper.Map<TermAndCondition>(request);
                TermAndCondition.IsPublic = false;
                TermAndCondition.Active = false;
                TermAndCondition.Version = "-";
                TermAndCondition.Created = DateTime.UtcNow;
                TermAndCondition.Modified = DateTime.UtcNow;
              
                var TermAndConditionObject = await _TermAndConditionRepository.AddAsync(TermAndCondition);
                var log = new CreateAppLog(_applicationLogRepository);
                log.CreateLog("TermAndCondition", "TermAndCondition", "Create", Newtonsoft.Json.JsonConvert.SerializeObject(request),request.UserId.ToString());
                return new Response<int>(TermAndConditionObject.Id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
