﻿using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.OrderingInsurances.Commands
{
    public class DeleteOrderingInsuranceCommand : IRequest<Response<int>>
    {
        public int Id { get; set; }
        public class DeleteOrderingInsuranceCommandHandler : IRequestHandler<DeleteOrderingInsuranceCommand, Response<int>>
        {
            private readonly IOrderingInsuranceRepositoryAsync _orderingInsuranceRepository;
            public DeleteOrderingInsuranceCommandHandler(IOrderingInsuranceRepositoryAsync orderingInsuranceRepository)
            {
                _orderingInsuranceRepository = orderingInsuranceRepository;
            }
            public async Task<Response<int>> Handle(DeleteOrderingInsuranceCommand command, CancellationToken cancellationToken)
            {
                var OrderingInsurance = (await _orderingInsuranceRepository.FindByCondition(x => x.Id == command.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (OrderingInsurance == null)
                {
                    throw new ApiException($"OrderingInsurance Not Found.");
                }
                else
                {
                    await _orderingInsuranceRepository.DeleteAsync(OrderingInsurance);
                    return new Response<int>(OrderingInsurance.Id);
                }
            }
        }
    }
}