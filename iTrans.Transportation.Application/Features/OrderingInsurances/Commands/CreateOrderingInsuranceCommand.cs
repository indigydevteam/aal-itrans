﻿using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Interfaces.Services;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using iTrans.Transportation.Domain.Entities;
using MediatR;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.OrderingInsurances.Commands
{
    public partial class CreateOrderingInsuranceCommand : IRequest<Response<int>>
    {

        public Guid OrderingId { get; set; }
        public string ProductName { get; set; }
        public int ProductTypeId { get; set; }
        public string WarrantyPeriod { get; set; }
        public decimal InsurancePremium { get; set; }
        public decimal InsuranceLimit { get; set; }
    }
    public class CreateOrderingInsuranceCommandHandler : IRequestHandler<CreateOrderingInsuranceCommand, Response<int>>
    {
        private readonly IOrderingInsuranceRepositoryAsync _orderingInsuranceRepository;
        private readonly IOrderingRepositoryAsync _orderingRepository;
        private readonly IProductTypeRepositoryAsync _productTypeRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;
        public CreateOrderingInsuranceCommandHandler(IOrderingInsuranceRepositoryAsync orderingInsuranceRepository, IOrderingRepositoryAsync orderingRepository, IProductTypeRepositoryAsync productTypeRepository,IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _orderingInsuranceRepository = orderingInsuranceRepository;
            _orderingRepository = orderingRepository;
            _productTypeRepository = productTypeRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(CreateOrderingInsuranceCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var OrderingInsurance = _mapper.Map<OrderingInsurance>(request);
                Ordering ordering = (await _orderingRepository.FindByCondition(x => x.Id.Equals(request.OrderingId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (ordering == null)
                {
                    throw new ApiException($"Ordering Not Found.");
                }
                OrderingInsurance.ProductType = (await _productTypeRepository.FindByCondition(x => x.Id.Equals(request.ProductTypeId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                OrderingInsurance.Ordering = ordering;
                OrderingInsurance.Created = DateTime.UtcNow;
                OrderingInsurance.Modified = DateTime.UtcNow;
                //title.CreatedBy = "xxxxxxx";
                //title.ModifiedBy = "xxxxxxx";
                var OrderingInsuranceObject = await _orderingInsuranceRepository.AddAsync(OrderingInsurance);
                var log = new CreateAppLog(_applicationLogRepository);
                log.Create("Ordering", "Ordering Insurance ", "Create", Newtonsoft.Json.JsonConvert.SerializeObject(request));
                return new Response<int>(OrderingInsuranceObject.Id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}