﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;

namespace iTrans.Transportation.Application.Features.OrderingInsurances.Commands
{
    public class UpdateOrderingInsuranceCommand : IRequest<Response<int>>
    {
        public int Id { get; set; }
        public Guid OrderingId { get; set; }
        public string ProductName { get; set; }
        public int ProductTypeId { get; set; }
        public string WarrantyPeriod { get; set; }
        public decimal InsurancePremium { get; set; }
        public decimal InsuranceLimit { get; set; }

    }

    public class UpdateOrderingInsuranceCommandHandler : IRequestHandler<UpdateOrderingInsuranceCommand, Response<int>>
    {
        private readonly IOrderingInsuranceRepositoryAsync _orderingInsuranceRepository;
        private readonly IProductTypeRepositoryAsync _productTypeRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;

        public UpdateOrderingInsuranceCommandHandler(IOrderingInsuranceRepositoryAsync orderingInsuranceRepository, IProductTypeRepositoryAsync productTypeRepository,IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _orderingInsuranceRepository = orderingInsuranceRepository;
            _productTypeRepository = productTypeRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(UpdateOrderingInsuranceCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var OrderingInsurance = (await _orderingInsuranceRepository.FindByCondition(x => x.Id == request.Id && x.Ordering.Id.Equals(request.OrderingId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (OrderingInsurance == null)
                {
                    throw new ApiException($"OrderingInsurance Not Found.");
                }
                else
                {
                    //OrderingInsurance.OrderingId = request.OrderingId;
                    //OrderingInsurance.ProductType = request.ProductType;
                    OrderingInsurance.ProductType = (await _productTypeRepository.FindByCondition(x => x.Id.Equals(request.ProductTypeId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                    OrderingInsurance.WarrantyPeriod = request.WarrantyPeriod;
                    OrderingInsurance.InsurancePremium = request.InsurancePremium;
                    OrderingInsurance.InsurancePremium = request.InsurancePremium;
                    OrderingInsurance.Modified = DateTime.UtcNow;
                  
                    await _orderingInsuranceRepository.UpdateAsync(OrderingInsurance);
                    var log = new CreateAppLog(_applicationLogRepository);
                    log.Create("Ordering", "Ordering Insurance ", "Edit", Newtonsoft.Json.JsonConvert.SerializeObject(OrderingInsurance));
                    return new Response<int>(OrderingInsurance.Id);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}