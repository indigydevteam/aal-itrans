﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.OrderingInsurance;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
namespace iTrans.Transportation.Application.Features.OrderingInsurances.Queries
{
    public class GetAllOrderingInsuranceQuery : IRequest<Response<IEnumerable<OrderingInsuranceViewModel>>>
    {

    }
    public class GetAllOrderingInsuranceQueryHandler : IRequestHandler<GetAllOrderingInsuranceQuery, Response<IEnumerable<OrderingInsuranceViewModel>>>
    {
        private readonly IOrderingInsuranceRepositoryAsync _orderingInsuranceRepository;
        private readonly IMapper _mapper;
        public GetAllOrderingInsuranceQueryHandler(IOrderingInsuranceRepositoryAsync OrderingInsuranceRepository, IMapper mapper)
        {
            _orderingInsuranceRepository = OrderingInsuranceRepository;
            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<OrderingInsuranceViewModel>>> Handle(GetAllOrderingInsuranceQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var OrderingInsurance = await _orderingInsuranceRepository.GetAllAsync();
                var OrderingInsuranceViewModel = _mapper.Map<IEnumerable<OrderingInsuranceViewModel>>(OrderingInsurance);
                return new Response<IEnumerable<OrderingInsuranceViewModel>>(OrderingInsuranceViewModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
