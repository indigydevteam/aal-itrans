﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.Ordering;
using iTrans.Transportation.Application.DTOs.OrderingInsurance;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
namespace iTrans.Transportation.Application.Features.OrderingInsurances.Queries
{
    public class GetOrderingInsuranceByOrderingQuery : IRequest<Response<IEnumerable<OrderingInsuranceViewModel>>>
    {
        public Guid OrderingId { set; get; }
    }
    public class GetOrderingInsuranceByOrderingQueryHandler : IRequestHandler<GetOrderingInsuranceByOrderingQuery, Response<IEnumerable<OrderingInsuranceViewModel>>>
    {
        private readonly IOrderingInsuranceRepositoryAsync _orderingInsuranceRepository;
        private readonly IMapper _mapper;
        public GetOrderingInsuranceByOrderingQueryHandler(IOrderingInsuranceRepositoryAsync orderingInsuranceRepository, IMapper mapper)
        {
            _orderingInsuranceRepository = orderingInsuranceRepository;
            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<OrderingInsuranceViewModel>>> Handle(GetOrderingInsuranceByOrderingQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var OrderingInsurance = (await _orderingInsuranceRepository.FindByCondition(x => x.Ordering.Id.Equals(request.OrderingId)).ConfigureAwait(false)).AsQueryable().ToList();
                var OrderingInsuranceViewModel = _mapper.Map<IEnumerable<OrderingInsuranceViewModel>>(OrderingInsurance);
                return new Response<IEnumerable<OrderingInsuranceViewModel>>(OrderingInsuranceViewModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}