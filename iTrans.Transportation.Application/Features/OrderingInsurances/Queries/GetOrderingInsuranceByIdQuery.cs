﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.OrderingInsurance;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.OrderingInsurances.Queries
{
    public class GetOrderingInsuranceByIdQuery : IRequest<Response<OrderingInsuranceViewModel>>
    {
        public int Id { get; set; }
        public Guid OrderId { get; set; }
    }
    public class GetOrderingInsuranceByIdQueryHandler : IRequestHandler<GetOrderingInsuranceByIdQuery, Response<OrderingInsuranceViewModel>>
    {
        private readonly IOrderingInsuranceRepositoryAsync _OrderingInsuranceRepository;
        private readonly IMapper _mapper;
        public GetOrderingInsuranceByIdQueryHandler(IOrderingInsuranceRepositoryAsync OrderingInsuranceRepository, IMapper mapper)
        {
            _OrderingInsuranceRepository = OrderingInsuranceRepository;
            _mapper = mapper;
        }
        public async Task<Response<OrderingInsuranceViewModel>> Handle(GetOrderingInsuranceByIdQuery request, CancellationToken cancellationToken)
        {
            var OrderingInsuranceObject = (await _OrderingInsuranceRepository.FindByCondition(x => x.Id.Equals(request.Id) && x.Ordering.Id.Equals(request.OrderId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            return new Response<OrderingInsuranceViewModel>(_mapper.Map<OrderingInsuranceViewModel>(OrderingInsuranceObject));
        }
    }
}
