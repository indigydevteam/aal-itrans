﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.OrderingInsurance;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;

namespace iTrans.Transportation.Application.Features.OrderingInsurances.Queries
{
    public class GetAllOrderingInsuranceParameter : IRequest<PagedResponse<IEnumerable<OrderingInsuranceViewModel>>>
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
    }
    public class GetAllOrderingInsuranceParameterHandler : IRequestHandler<GetAllOrderingInsuranceParameter, PagedResponse<IEnumerable<OrderingInsuranceViewModel>>>
    {
        private readonly IOrderingInsuranceRepositoryAsync _OrderingInsuranceRepository;
        private readonly IMapper _mapper;
        public GetAllOrderingInsuranceParameterHandler(IOrderingInsuranceRepositoryAsync OrderingInsuranceRepository, IMapper mapper)
        {
            _OrderingInsuranceRepository = OrderingInsuranceRepository;
            _mapper = mapper;
        }

        public async Task<PagedResponse<IEnumerable<OrderingInsuranceViewModel>>> Handle(GetAllOrderingInsuranceParameter request, CancellationToken cancellationToken)
        {
            var validFilter = _mapper.Map<GetAllOrderingInsuranceParameter>(request);
            Expression<Func<OrderingInsurance, bool>> expression = x => x.WarrantyPeriod != "";
            var OrderingInsurance = await _OrderingInsuranceRepository.FindByConditionWithPage(expression, validFilter.PageNumber, validFilter.PageSize);
            var OrderingInsuranceViewModel = _mapper.Map<IEnumerable<OrderingInsuranceViewModel>>(OrderingInsurance);
            return new PagedResponse<IEnumerable<OrderingInsuranceViewModel>>(OrderingInsuranceViewModel, validFilter.PageNumber, validFilter.PageSize);
        }
    }
}