﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.EnergySavingDevice;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.EnergySavingDevices.Queries
{
    public class GetEnergySavingDeviceListQuery : IRequest<Response<IEnumerable<EnergySavingDeviceViewModel>>>
    {

    }
    public class GetEnergySavingDeviceListQueryHandler : IRequestHandler<GetEnergySavingDeviceListQuery, Response<IEnumerable<EnergySavingDeviceViewModel>>>
    {
        private readonly IEnergySavingDeviceRepositoryAsync _energySavingDeviceRepository;
        private readonly IMapper _mapper;
        public GetEnergySavingDeviceListQueryHandler(IEnergySavingDeviceRepositoryAsync energySavingDeviceRepository, IMapper mapper)
        {
            _energySavingDeviceRepository = energySavingDeviceRepository;
            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<EnergySavingDeviceViewModel>>> Handle(GetEnergySavingDeviceListQuery request, CancellationToken cancellationToken)
        {
            var energySavingDevice = (await _energySavingDeviceRepository.FindByCondition(x => x.Active == true).ConfigureAwait(false)).AsQueryable().ToList();
            var energySavingDeviceViewModel = _mapper.Map<IEnumerable<EnergySavingDeviceViewModel>>(energySavingDevice);
            return new Response<IEnumerable<EnergySavingDeviceViewModel>>(energySavingDeviceViewModel);
        }
    }
}
