﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.EnergySavingDevice;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.EnergySavingDevices.Backoffice.Queries
{
    public class GetEnergySavingDeviceByIdQuery : IRequest<Response<EnergySavingDeviceViewModel>>
    {
        public int Id { get; set; }
    }
    public class GetEnergySavingDeviceByIdQueryHandler : IRequestHandler<GetEnergySavingDeviceByIdQuery, Response<EnergySavingDeviceViewModel>>
    {
        private readonly IEnergySavingDeviceRepositoryAsync _energySavingDeviceRepository;
        private readonly IMapper _mapper;
        public GetEnergySavingDeviceByIdQueryHandler(IEnergySavingDeviceRepositoryAsync energySavingDeviceRepository, IMapper mapper)
        {
            _energySavingDeviceRepository = energySavingDeviceRepository;
            _mapper = mapper;
        }
        public async Task<Response<EnergySavingDeviceViewModel>> Handle(GetEnergySavingDeviceByIdQuery request, CancellationToken cancellationToken)
        {
            var energySavingDeviceObject = (await _energySavingDeviceRepository.FindByCondition(x => x.Id.Equals(request.Id)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            return new Response<EnergySavingDeviceViewModel>(_mapper.Map<EnergySavingDeviceViewModel>(energySavingDeviceObject));
        }
    }
}
