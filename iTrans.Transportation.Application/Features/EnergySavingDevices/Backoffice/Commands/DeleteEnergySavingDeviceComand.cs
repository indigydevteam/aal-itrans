﻿using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;


namespace iTrans.Transportation.Application.Features.EnergySavingDevices.Backoffice.Commands
{
   public class DeleteEnergySavingDeviceComand : IRequest<Response<int>>
    {
        public Guid? UserId { get; set; }
        public int Id { get; set; }
        public class DeleteEnergySavingDeviceComandHandler : IRequestHandler<DeleteEnergySavingDeviceComand, Response<int>>
        {
            private readonly IEnergySavingDeviceRepositoryAsync _energySavingDeviceRepository;
            private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
            public DeleteEnergySavingDeviceComandHandler(IEnergySavingDeviceRepositoryAsync EnergySavingDeviceRepository, IApplicationLogRepositoryAsync applicationLogRepository)
            {
                _energySavingDeviceRepository = EnergySavingDeviceRepository;
                _applicationLogRepository = applicationLogRepository;
            }
            public async Task<Response<int>> Handle(DeleteEnergySavingDeviceComand command, CancellationToken cancellationToken)
            {
                var energySavingDevice = (await _energySavingDeviceRepository.FindByCondition(x => x.Id == command.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (energySavingDevice == null)
                {
                    throw new ApiException($"EnergySavingDevice Not Found.");
                }
                else
                {
                    energySavingDevice.Active = false;
                    energySavingDevice.IsDelete = true;
                    energySavingDevice.Modified = DateTime.UtcNow;
                    energySavingDevice.ModifiedBy = command.UserId != null ? command.UserId.GetValueOrDefault().ToString() : "";
                    await _energySavingDeviceRepository.UpdateAsync(energySavingDevice);

                    var log = new CreateAppLog(_applicationLogRepository);
                    log.CreateLog("EnergySavingDevice", "EnergySavingDevice", "Delete", Newtonsoft.Json.JsonConvert.SerializeObject(command), command.UserId == null ? "-" : command.UserId.Value.ToString());
                    return new Response<int>(energySavingDevice.Id);
                }
            }
        }
    }
}