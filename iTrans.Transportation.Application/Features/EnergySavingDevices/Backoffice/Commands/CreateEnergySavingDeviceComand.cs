﻿using AutoMapper;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Domain;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;

namespace iTrans.Transportation.Application.Features.EnergySavingDevices.Backoffice.Commands
{
    public partial class CreateEnergySavingDeviceCommand : IRequest<Response<int>>
    {
        public Guid? UserId { get; set; }
        public string Name_TH { get; set; }
        public string Name_ENG { get; set; }
        public int Sequence { get; set; }
        public bool Active { get; set; }
        public bool Specified { get; set; }

    }
    public class CreateEnergySavingDeviceCommandHandler : IRequestHandler<CreateEnergySavingDeviceCommand, Response<int>>
    {
        private readonly IEnergySavingDeviceRepositoryAsync _energySavingDeviceRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;
        public CreateEnergySavingDeviceCommandHandler(IEnergySavingDeviceRepositoryAsync energySavingDeviceRepository, IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _energySavingDeviceRepository = energySavingDeviceRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(CreateEnergySavingDeviceCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var energySavingDevice = _mapper.Map<EnergySavingDevice>(request);
                energySavingDevice.Created = DateTime.UtcNow;
                energySavingDevice.Modified = DateTime.UtcNow;
                energySavingDevice.CreatedBy = request.UserId == null ? "" : request.UserId.Value.ToString();
                energySavingDevice.ModifiedBy = request.UserId == null ? "" : request.UserId.Value.ToString();

                var energySavingDeviceObject = await _energySavingDeviceRepository.AddAsync(energySavingDevice);
                var log = new CreateAppLog(_applicationLogRepository);
                log.CreateLog("Driver", "Driver TermAndcCondition", "Edit", Newtonsoft.Json.JsonConvert.SerializeObject(request),request.UserId.ToString());
                return new Response<int>(energySavingDeviceObject.Id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
