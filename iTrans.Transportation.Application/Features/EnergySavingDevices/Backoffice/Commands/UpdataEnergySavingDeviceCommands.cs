﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;

namespace iTrans.Transportation.Application.Features.EnergySavingDevices.Backoffice.Commands
{
    public class UpdateEnergySavingDeviceCommand : IRequest<Response<int>>
    {
        public Guid? UserId { get; set; }
        public int Id { get; set; }
        public string Name_TH { get; set; }
        public string Name_ENG { get; set; }
        public int Sequence { get; set; }
        public bool Active { get; set; }
        public bool Specified { get; set; }

    }

    public class UpdateEnergySavingDeviceCommandHandler : IRequestHandler<UpdateEnergySavingDeviceCommand, Response<int>>
    {
        private readonly IEnergySavingDeviceRepositoryAsync _energySavingDeviceRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;

        public UpdateEnergySavingDeviceCommandHandler(IEnergySavingDeviceRepositoryAsync energySavingDeviceRepository, IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _energySavingDeviceRepository = energySavingDeviceRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(UpdateEnergySavingDeviceCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var energySavingDevice = (await _energySavingDeviceRepository.FindByCondition(x => x.Id == request.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (energySavingDevice == null)
                {
                    throw new ApiException($"EnergySavingDevice Not Found.");
                }
                else
                {
                    energySavingDevice.Name_TH = request.Name_TH;
                    energySavingDevice.Name_ENG = request.Name_ENG;
                    energySavingDevice.Sequence = request.Sequence;
                    energySavingDevice.Active = request.Active;
                    energySavingDevice.Specified = request.Specified;
                    energySavingDevice.Modified = DateTime.UtcNow;
                    energySavingDevice.ModifiedBy = request.UserId == null ? "" : request.UserId.Value.ToString();
                    await _energySavingDeviceRepository.UpdateAsync(energySavingDevice);
                    var log = new CreateAppLog(_applicationLogRepository);
                    log.CreateLog("EnergySavingDevice", "EnergySavingDevice", "Edit", Newtonsoft.Json.JsonConvert.SerializeObject(request),request.UserId.ToString());
                    return new Response<int>(energySavingDevice.Id);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
