﻿using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Interfaces.Services;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using iTrans.Transportation.Domain.Entities;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;

namespace iTrans.Transportation.Application.Features.OrderingProducts.Commands
{
    public partial class CreateOrderingProductCommand : IRequest<Response<int>>
    {

        public Guid OrderingId { get; set; }
        public string Name { get; set; }
        public int ProductTypeId { get; set; }
        public string ProductTypeDetail { get; set; }
        public int PackagingId { get; set; }
        public string PackagingDetail { get; set; }
        public int Width { get; set; }
        public int Length { get; set; }
        public int Height { get; set; }
        public int Weight { get; set; }
        public int Quantity { get; set; }
        public int Sequence { get; set; }
        public List<IFormFile> files { get; set; }
    }
    public class CreateOrderingProductCommandHandler : IRequestHandler<CreateOrderingProductCommand, Response<int>>
    {
        private readonly IOrderingRepositoryAsync _orderingRepository;
        private readonly IOrderingProductRepositoryAsync _orderingProductRepository;
        private readonly IProductTypeRepositoryAsync _productTypeRepository;
        private readonly IProductPackagingRepositoryAsync _productPackagingRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;
        private readonly IConfiguration _configuration;
        public CreateOrderingProductCommandHandler(IOrderingRepositoryAsync orderingRepository, IOrderingProductRepositoryAsync orderingProductRepository, IProductTypeRepositoryAsync productTypeRepository
            , IProductPackagingRepositoryAsync productPackagingRepository,IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper, IConfiguration configuration)
        {
            _orderingRepository = orderingRepository;
            _orderingProductRepository = orderingProductRepository;
            _productTypeRepository = productTypeRepository;
            _productPackagingRepository = productPackagingRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
            _configuration = configuration;
        }

        public async Task<Response<int>> Handle(CreateOrderingProductCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var getContentPath = _configuration.GetSection("ContentPath");
                string contentPath = getContentPath.Value;
                var data = (await _orderingRepository.FindByCondition(x => x.Id.Equals(request.OrderingId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (data == null)
                {
                    throw new ApiException($"Ordering Not Found.");
                }

                List<ProductType> productTypeList = (await _productTypeRepository.FindByCondition(x => x.Active.Equals(true)).ConfigureAwait(false)).AsQueryable().ToList();
                List<ProductPackaging> packagingList = (await _productPackagingRepository.FindByCondition(x => x.Active.Equals(true)).ConfigureAwait(false)).AsQueryable().ToList();



                var OrderingProduct = _mapper.Map<OrderingProduct>(request);
                OrderingProduct.Ordering = data;
                //OrderingProduct.ProductType = productTypeList.Where(p => p.Id.Equals(request.ProductTypeId)).FirstOrDefault();
                //OrderingProduct.Packaging = packagingList.Where(p => p.Id.Equals(request.PackagingId)).FirstOrDefault();
                OrderingProduct.Created = DateTime.UtcNow;
                OrderingProduct.Modified = DateTime.UtcNow;
                if (request.files != null)
                {
                    string folderPath = contentPath + "Ordering/" + request.OrderingId.ToString() + "/Product/" + request.Sequence;
                    if (!Directory.Exists(folderPath))
                    {
                        Directory.CreateDirectory(folderPath);
                    }
                    int fileCount = 0;
                    List<OrderingProductFile> productFiles = new List<OrderingProductFile>();
                    foreach (IFormFile file in request.files)
                    {
                        fileCount = fileCount + 1;
                        string filePath = Path.Combine(folderPath, file.FileName);
                        using (FileStream fileStream = new FileStream(filePath, FileMode.Create))
                        {
                            await file.CopyToAsync(fileStream);
                            FileInfo fi = new FileInfo(filePath);

                            OrderingProductFile orderingProductFile = new OrderingProductFile
                            {
                                OrderingProduct = OrderingProduct,
                                FileName = file.FileName,
                                ContentType = file.ContentType,
                                FilePath = "Ordering/" + request.OrderingId.ToString() + "/Product/" + request.Sequence + "/" + file.FileName,
                                DirectoryPath = "Ordering/" + request.OrderingId.ToString() + "/Product/" + request.Sequence + "/",
                                FileEXT = fi.Extension
                            };
                            productFiles.Add(orderingProductFile);
                        }
                    }
                    OrderingProduct.ProductFiles = productFiles;
                }
         
                var OrderingProductObject = await _orderingProductRepository.AddAsync(OrderingProduct);
                var log = new CreateAppLog(_applicationLogRepository);
                log.Create("Ordering", "Ordering Product ","Create", Newtonsoft.Json.JsonConvert.SerializeObject(OrderingProduct));
                return new Response<int>(OrderingProductObject.Id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}