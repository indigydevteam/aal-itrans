﻿using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;

namespace iTrans.Transportation.Application.Features.OrderingProducts.Commands
{
    public class DeleteOrderingProductCommand : IRequest<Response<int>>
    {
        public int Id { get; set; }
        public Guid OrderingId { get; set; }
        public class DeleteOrderingProductCommandHandler : IRequestHandler<DeleteOrderingProductCommand, Response<int>>
        {
            private readonly IOrderingProductRepositoryAsync _orderingProductRepository;
            private readonly IOrderingProductFileRepositoryAsync _orderingProductFileRepository;
            private readonly IConfiguration _configuration;
            public DeleteOrderingProductCommandHandler(IOrderingProductRepositoryAsync orderingProductRepository, IOrderingProductFileRepositoryAsync orderingProductFileRepository, IConfiguration configuration)
            {
                _orderingProductRepository = orderingProductRepository;
                _orderingProductFileRepository = orderingProductFileRepository;
                _configuration = configuration;
            }
            public async Task<Response<int>> Handle(DeleteOrderingProductCommand command, CancellationToken cancellationToken)
            {
                var getContentPath = _configuration.GetSection("ContentPath");
                string contentPath = getContentPath.Value;

                var OrderingProduct = (await _orderingProductRepository.FindByCondition(x => x.Id == command.Id && x.Ordering.Id.Equals(command.OrderingId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (OrderingProduct == null)
                {
                    throw new ApiException($"OrderingProduct Not Found.");
                }
                else
                {
                    var orderingProductFiles = (await _orderingProductFileRepository.FindByCondition(x => x.OrderingProduct.Id == command.Id && x.OrderingProduct.Ordering.Id.Equals(command.OrderingId)).ConfigureAwait(false)).AsQueryable().ToList();
                    foreach (OrderingProductFile orderingProductFile in orderingProductFiles)
                    {
                        File.Delete(Path.Combine(contentPath, orderingProductFile.FilePath));
                        await _orderingProductFileRepository.DeleteAsync(orderingProductFile);
                    }
                    await _orderingProductRepository.DeleteAsync(OrderingProduct);
                    return new Response<int>(OrderingProduct.Id);
                }
            }
        }
    }
}