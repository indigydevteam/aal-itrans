﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.OrderingProduct;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
namespace iTrans.Transportation.Application.Features.OrderingProducts.Queries
{
    public class GetAllOrderingProductQuery : IRequest<Response<IEnumerable<OrderingProductViewModel>>>
    {

    }
    public class GetAllOrderingProductQueryHandler : IRequestHandler<GetAllOrderingProductQuery, Response<IEnumerable<OrderingProductViewModel>>>
    {
        private readonly IOrderingProductRepositoryAsync _orderingProductRepository;
        private readonly IMapper _mapper;
        public GetAllOrderingProductQueryHandler(IOrderingProductRepositoryAsync OrderingProductRepository, IMapper mapper)
        {
            _orderingProductRepository = OrderingProductRepository;
            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<OrderingProductViewModel>>> Handle(GetAllOrderingProductQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var OrderingProduct = await _orderingProductRepository.GetAllAsync();
                var OrderingProductViewModel = _mapper.Map<IEnumerable<OrderingProductViewModel>>(OrderingProduct);
                return new Response<IEnumerable<OrderingProductViewModel>>(OrderingProductViewModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
