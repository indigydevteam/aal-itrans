﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.OrderingProduct;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.OrderingProducts.Queries
{
    public class GetOrderingProductByIdQuery : IRequest<Response<OrderingProductViewModel>>
    {
        public int Id { get; set; }
    }
    public class GetOrderingProductByIdQueryHandler : IRequestHandler<GetOrderingProductByIdQuery, Response<OrderingProductViewModel>>
    {
        private readonly IOrderingProductRepositoryAsync _OrderingProductRepository;
        private readonly IMapper _mapper;
        public GetOrderingProductByIdQueryHandler(IOrderingProductRepositoryAsync OrderingProductRepository, IMapper mapper)
        {
            _OrderingProductRepository = OrderingProductRepository;
            _mapper = mapper;
        }
        public async Task<Response<OrderingProductViewModel>> Handle(GetOrderingProductByIdQuery request, CancellationToken cancellationToken)
        {
            var OrderingProductObject = (await _OrderingProductRepository.FindByCondition(x => x.Id.Equals(request.Id)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            return new Response<OrderingProductViewModel>(_mapper.Map<OrderingProductViewModel>(OrderingProductObject));
        }
    }
}
