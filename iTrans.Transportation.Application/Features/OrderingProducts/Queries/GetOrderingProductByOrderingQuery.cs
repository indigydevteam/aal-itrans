﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.Ordering;
using iTrans.Transportation.Application.DTOs.OrderingProduct;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
namespace iTrans.Transportation.Application.Features.OrderingProducts.Queries
{
    public class GetOrderingProductByOrderingQuery : IRequest<Response<IEnumerable<OrderingProductViewModel>>>
    {
        public int OrderingId { set; get; }
    }
    public class GetOrderingProductByOrderingQueryHandler : IRequestHandler<GetOrderingProductByOrderingQuery, Response<IEnumerable<OrderingProductViewModel>>>
    {
        private readonly IOrderingProductRepositoryAsync _orderingProductRepository;
        private readonly IMapper _mapper;
        public GetOrderingProductByOrderingQueryHandler(IOrderingProductRepositoryAsync orderingProductRepository, IMapper mapper)
        {
            _orderingProductRepository = orderingProductRepository;
            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<OrderingProductViewModel>>> Handle(GetOrderingProductByOrderingQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var OrderingProduct = (await _orderingProductRepository.FindByCondition(x => x.Ordering.Id.Equals(request.OrderingId)).ConfigureAwait(false)).AsQueryable().ToList();
                var OrderingProductViewModel = _mapper.Map<IEnumerable<OrderingProductViewModel>>(OrderingProduct);
                return new Response<IEnumerable<OrderingProductViewModel>>(OrderingProductViewModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}