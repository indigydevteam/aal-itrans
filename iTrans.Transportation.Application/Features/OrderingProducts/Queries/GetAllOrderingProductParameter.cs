﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.OrderingProduct;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;

namespace iTrans.Transportation.Application.Features.OrderingProducts.Queries
{
    public class GetAllOrderingProductParameter : IRequest<PagedResponse<IEnumerable<OrderingProductViewModel>>>
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
    }
    public class GetAllOrderingProductParameterHandler : IRequestHandler<GetAllOrderingProductParameter, PagedResponse<IEnumerable<OrderingProductViewModel>>>
    {
        private readonly IOrderingProductRepositoryAsync _OrderingProductRepository;
        private readonly IMapper _mapper;
        public GetAllOrderingProductParameterHandler(IOrderingProductRepositoryAsync OrderingProductRepository, IMapper mapper)
        {
            _OrderingProductRepository = OrderingProductRepository;
            _mapper = mapper;
        }

        public async Task<PagedResponse<IEnumerable<OrderingProductViewModel>>> Handle(GetAllOrderingProductParameter request, CancellationToken cancellationToken)
        {
            var validFilter = _mapper.Map<GetAllOrderingProductParameter>(request);
            Expression<Func<OrderingProduct, bool>> expression = x => x.Name != "";
            var OrderingProduct = await _OrderingProductRepository.FindByConditionWithPage(expression, validFilter.PageNumber, validFilter.PageSize);
            var OrderingProductViewModel = _mapper.Map<IEnumerable<OrderingProductViewModel>>(OrderingProduct);
            return new PagedResponse<IEnumerable<OrderingProductViewModel>>(OrderingProductViewModel, validFilter.PageNumber, validFilter.PageSize);
        }
    }
}