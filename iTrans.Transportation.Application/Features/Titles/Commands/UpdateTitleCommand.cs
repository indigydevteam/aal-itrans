﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;

namespace iTrans.Transportation.Application.Features.Titles.Commands
{
    public class UpdateTitleCommand : IRequest<Response<int>>
    {
        public int Id { get; set; }
        public string Name_TH { get; set; }
        public string Name_ENG { get; set; }
        public int Sequence { get; set; }
        public bool Active { get; set; }
        public bool Specified { get; set; }

    }

    public class UpdateTitleCommandHandler : IRequestHandler<UpdateTitleCommand, Response<int>>
    {
        private readonly ITitleRepositoryAsync _titleRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;

        public UpdateTitleCommandHandler(ITitleRepositoryAsync titleRepository,IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _titleRepository = titleRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(UpdateTitleCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var title = (await _titleRepository.FindByCondition(x => x.Id == request.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (title == null)
                {
                    throw new ApiException($"Title Not Found.");
                }
                else
                {
                    title.Name_TH = request.Name_TH;
                    title.Name_ENG = request.Name_ENG;
                    title.Sequence = request.Sequence;
                    title.Active = request.Active;
                    title.Specified = request.Specified;
                    title.Modified = DateTime.UtcNow;
                    
                    await _titleRepository.UpdateAsync(title);
                    var log = new CreateAppLog(_applicationLogRepository);
                    log.Create("Title", "Title", "Edit", Newtonsoft.Json.JsonConvert.SerializeObject(title));
                    return new Response<int>(title.Id);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
