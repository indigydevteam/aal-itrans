﻿using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.Titles.Commands
{
    public class DeleteTitleByIdCommand : IRequest<Response<int>>
    {
        public int Id { get; set; }
        public class DeleteTitleByIdCommandHandler : IRequestHandler<DeleteTitleByIdCommand, Response<int>>
        {
            private readonly ITitleRepositoryAsync _titleRepository;
            public DeleteTitleByIdCommandHandler(ITitleRepositoryAsync titleRepository)
            {
                _titleRepository = titleRepository;
            }
            public async Task<Response<int>> Handle(DeleteTitleByIdCommand command, CancellationToken cancellationToken)
            {
                var title = (await _titleRepository.FindByCondition(x => x.Id == command.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (title == null)
                {
                    throw new ApiException($"Title Not Found.");
                }
                else
                {
                    await _titleRepository.DeleteAsync(title);
                    return new Response<int>(title.Id);
                }
            }
        }
    }
}
