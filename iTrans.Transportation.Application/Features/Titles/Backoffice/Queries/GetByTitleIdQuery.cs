﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.Backoffice.Title;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.Titles.Backoffice.Queries
{
    public class BackofficeGetTitleByIdQuery : IRequest<Response<TitleViewModel>>
    {
        public int Id { get; set; }
    }
    public class BackofficeGetTitleByIdQueryHandler : IRequestHandler<BackofficeGetTitleByIdQuery, Response<TitleViewModel>>
    {
        private readonly ITitleRepositoryAsync _titleRepository;
        private readonly IMapper _mapper;
        public BackofficeGetTitleByIdQueryHandler(ITitleRepositoryAsync titleRepository, IMapper mapper)
        {
            _titleRepository = titleRepository;
            _mapper = mapper;
        }
        public async Task<Response<TitleViewModel>> Handle(BackofficeGetTitleByIdQuery request, CancellationToken cancellationToken)
        {
            var titleObject = (await _titleRepository.FindByCondition(x => x.Id.Equals(request.Id)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            return new Response<TitleViewModel>(_mapper.Map<TitleViewModel>(titleObject));
        }
    }
}
