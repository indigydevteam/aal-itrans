﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.Backoffice.Title;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;

namespace iTrans.Transportation.Application.Features.Titles.Backoffice.Queries
{
    public class GetAllTitleParameter : IRequest<PagedResponse<IEnumerable<TitleViewModel>>>
    {
        public string Search { set; get; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public int Active { set; get; }
        public string Column { set; get; }
    }
    public class GetAllTitleParameterHandler : IRequestHandler<GetAllTitleParameter, PagedResponse<IEnumerable<TitleViewModel>>>
    {
        private readonly ITitleRepositoryAsync _titleRepository;
        private readonly IMapper _mapper;
        private Expression<Func<Title, bool>> expression;
        public GetAllTitleParameterHandler(ITitleRepositoryAsync titleRepository, IMapper mapper)
        {
            _titleRepository = titleRepository;
            _mapper = mapper;
        }

        public async Task<PagedResponse<IEnumerable<TitleViewModel>>> Handle(GetAllTitleParameter request, CancellationToken cancellationToken)
        {
            var validFilter = _mapper.Map<GetAllTitleParameter>(request);

            var result = from x in _titleRepository.GetAllAsync().Result.ToList().Where(x => x.IsDelete == false).ToList().OrderByDescending(x => x.Modified)
                         select new TitleViewModel
                         {
                             Id = x.Id,
                             Name_TH = x.Name_TH != null ? x.Name_TH : "-",
                             Name_ENG = x.Name_ENG != null ? x.Name_ENG : "-",
                             Sequence = x.Sequence,
                             Active = x.Active,
                             Created = x.Created,
                             Modified = x.Modified
                         };


            if (validFilter.Search != null && validFilter.Search.Trim() != "")
            {
                result = result.Where(x => x.Name_ENG.ToLower().Contains(validFilter.Search.Trim()) || x.Name_TH.ToLower().Contains(validFilter.Search.Trim())).ToList();
            }
            if (request.Column == "name_TH" && request.Active == 3)
            {
                result = result.OrderBy(x => x.Name_TH);
            }
            if (request.Column == "name_TH" && request.Active == 2)
            {
                result = result.OrderByDescending(x => x.Name_TH);
            }
            if (request.Column == "name_ENG" && request.Active == 3)
            {
                result = result.OrderBy(x => x.Name_ENG);
            }
            if (request.Column == "name_ENG" && request.Active == 2)
            {
                result = result.OrderByDescending(x => x.Name_ENG);
            }
            if (request.Column == "sequence" && request.Active == 3)
            {
                result = result.OrderBy(x => x.Sequence);
            }
            if (request.Column == "sequence" && request.Active == 2)
            {
                result = result.OrderByDescending(x => x.Sequence);
            }
            if (request.Column == "created" && request.Active == 3)
            {
                result = result.OrderBy(x => x.Created);
            }
            if (request.Column == "created" && request.Active == 2)
            {
                result = result.OrderByDescending(x => x.Created);
            }
            if (request.Column == "modified" && request.Active == 3)
            {
                result = result.OrderBy(x => x.Modified);
            }
            if (request.Column == "modified" && request.Active == 2)
            {
                result = result.OrderByDescending(x => x.Modified);
            }

            var itemCount = result.Count();
            result = result.Skip((validFilter.PageNumber - 1) * validFilter.PageSize).Take(validFilter.PageSize);

            return new PagedResponse<IEnumerable<TitleViewModel>>(result, validFilter.PageNumber, validFilter.PageSize, itemCount);
        }
    }
}
