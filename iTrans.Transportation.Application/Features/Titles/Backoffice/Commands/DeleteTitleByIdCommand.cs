﻿using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.Titles.Backoffice.Commands
{
    public class DeleteTitleByIdCommand : IRequest<Response<int>>
    {
        public Guid? UserId { get; set; }
        public int Id { get; set; }
        public class DeleteTitleByIdCommandHandler : IRequestHandler<DeleteTitleByIdCommand, Response<int>>
        {
            private readonly ITitleRepositoryAsync _titleRepository;
            private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
            public DeleteTitleByIdCommandHandler(ITitleRepositoryAsync titleRepository, IApplicationLogRepositoryAsync applicationLogRepository)
            {
                _titleRepository = titleRepository;
                _applicationLogRepository = applicationLogRepository;
            }
            public async Task<Response<int>> Handle(DeleteTitleByIdCommand command, CancellationToken cancellationToken)
            {
                var title = (await _titleRepository.FindByCondition(x => x.Id == command.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (title == null)
                {
                    throw new ApiException($"Title Not Found.");
                }
                else
                {
                    title.Active = false;
                    title.IsDelete = true;
                    title.Modified = DateTime.UtcNow;
                    title.ModifiedBy = command.UserId != null ? command.UserId.GetValueOrDefault().ToString() : "";
                    await _titleRepository.UpdateAsync(title);
                    var log = new CreateAppLog(_applicationLogRepository);
                    log.CreateLog("Title", "Title", "Delete", Newtonsoft.Json.JsonConvert.SerializeObject(command), command.UserId == null? "-": command.UserId.Value.ToString());
                    return new Response<int>(title.Id);
                }
            }
        }
    }
}
