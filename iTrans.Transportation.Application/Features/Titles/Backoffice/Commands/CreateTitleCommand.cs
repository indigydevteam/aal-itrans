﻿using AutoMapper;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Interfaces.Services;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;


namespace iTrans.Transportation.Application.Features.Titles.Backoffice.Commands
{
    public partial class CreateTitleCommand : IRequest<Response<int>>
    {
        public Guid? UserId { get; set; }
        public string Name_TH { get; set; }
        public string Name_ENG { get; set; }
        public int Sequence { get; set; }
        public bool Active { get; set; }
        public  bool Specified { get; set; }


    }
    public class CreateTitleCommandHandler : IRequestHandler<CreateTitleCommand, Response<int>>
    {
        private readonly ITitleRepositoryAsync _titleRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;
        public CreateTitleCommandHandler(ITitleRepositoryAsync titleRepository,IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _titleRepository = titleRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(CreateTitleCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var title = _mapper.Map<Title>(request);
                title.Created = DateTime.UtcNow;
                title.Modified = DateTime.UtcNow;
                title.CreatedBy = request.UserId != null ? request.UserId.GetValueOrDefault().ToString() : "";
                title.ModifiedBy = request.UserId != null ? request.UserId.GetValueOrDefault().ToString() : "";
                title.IsDelete = false;
                var titleObject = await _titleRepository.AddAsync(title);
                var log = new CreateAppLog(_applicationLogRepository);
                log.CreateLog("Title", "Title", "Create", Newtonsoft.Json.JsonConvert.SerializeObject(request), request.UserId == null ? "-" : request.UserId.Value.ToString());
                return new Response<int>(titleObject.Id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
