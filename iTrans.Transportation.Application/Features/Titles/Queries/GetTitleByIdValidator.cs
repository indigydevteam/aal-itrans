﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using iTrans.Transportation.Application.Interfaces.Repositories;
using FluentValidation;

namespace iTrans.Transportation.Application.Features.Titles.Queries
{
    public class GetTitleByIdValidator : AbstractValidator<GetTitleById>
    {
        private readonly ITitleRepositoryAsync titleRepository;

        public GetTitleByIdValidator(ITitleRepositoryAsync titleRepository)
        {
            this.titleRepository = titleRepository;
            RuleFor(p => p.Id)
               .NotEmpty().WithMessage("{PropertyName} is required.")
               .NotNull()
               .MustAsync(IsTitleExists).WithMessage("{PropertyName} not exists.");
        }

        private async Task<bool> IsTitleExists(int TitleId, CancellationToken cancellationToken)
        {
            var userObject = (await titleRepository.FindByCondition(x => x.Id.Equals(TitleId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            if (userObject != null)
            {
                return true;
            }
            return false;
        }
    }
}
