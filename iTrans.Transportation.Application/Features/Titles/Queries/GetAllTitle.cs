﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.Title;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.Titles.Queries
{
    public class GetAllTitle : IRequest<Response<IEnumerable<TitleViewModel>>>
    {
        public string Language { get; set; }
    }
    public class GetAllTitleHandler : IRequestHandler<GetAllTitle, Response<IEnumerable<TitleViewModel>>>
    {
        private readonly ITitleRepositoryAsync _titleRepository;
        private readonly IMapper _mapper;
        public GetAllTitleHandler(ITitleRepositoryAsync titleRepository, IMapper mapper)
        {
            _titleRepository = titleRepository;
            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<TitleViewModel>>> Handle(GetAllTitle request, CancellationToken cancellationToken)
        {
            var title = await _titleRepository.GetAllAsync();
            var titleViewModel = _mapper.Map<IEnumerable<TitleViewModel>>(title.Where(x => x.Active == true && x.IsDelete == false).OrderBy(x => x.Name_TH).ToList());
            if(request.Language != null && request.Language.Trim().ToLower() == "en")
            {
                titleViewModel = titleViewModel.OrderBy( x => x.name_ENG).ToList();
            }
            return new Response<IEnumerable<TitleViewModel>>(titleViewModel);
        }
    }
}
