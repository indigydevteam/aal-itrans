﻿using AutoMapper;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Interfaces.Services;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.OrderingProductFiles.Commands
{
    public partial class CreateOrderingProductFileCommand : IRequest<Response<int>>
    {
        public int Id { get; set; }
        public int OrderingProductId { get; set; }
        public string FileName { get; set; }
        public  string ContentType { get; set; }
        public  string FilePath { get; set; }
        public  string FileEXT { get; set; }

    }
    public class CreateOrderingProductFileCommandHandler : IRequestHandler<CreateOrderingProductFileCommand, Response<int>>
    {
        private readonly IOrderingProductFileRepositoryAsync _orderingProductFileRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;
        public CreateOrderingProductFileCommandHandler(IOrderingProductFileRepositoryAsync orderingProductFileRepository,IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _orderingProductFileRepository = orderingProductFileRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(CreateOrderingProductFileCommand request, CancellationToken cancellationToken)
        {
            var orderingProductFile = _mapper.Map<OrderingProductFile>(request);
            orderingProductFile.Created = DateTime.UtcNow;
            orderingProductFile.Modified = DateTime.UtcNow;
            var orderingProductFileObject = await _orderingProductFileRepository.AddAsync(orderingProductFile);
            var log = new CreateAppLog(_applicationLogRepository);
            log.Create("Ordering", "Ordering Productfile ", "Create", Newtonsoft.Json.JsonConvert.SerializeObject(orderingProductFile));
            return new Response<int>(orderingProductFileObject.Id);
        }
    }
}
