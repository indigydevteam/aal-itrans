﻿using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
namespace iTrans.Transportation.Application.Features.OrderingProductFiles.Commands
{
    public class DeleteOrderingProductFileByIdCommand : IRequest<Response<int>>
    {
        public int Id { get; set; }
        public class DeleteOrderingProductFileByIdCommandHandler : IRequestHandler<DeleteOrderingProductFileByIdCommand, Response<int>>
        {
            private readonly IOrderingProductFileRepositoryAsync _orderingProductFileRepository;
            public DeleteOrderingProductFileByIdCommandHandler(IOrderingProductFileRepositoryAsync orderingProductFileRepository)
            {
                _orderingProductFileRepository = orderingProductFileRepository;
            }
            public async Task<Response<int>> Handle(DeleteOrderingProductFileByIdCommand command, CancellationToken cancellationToken)
            {
                var orderingProductFile = (await _orderingProductFileRepository.FindByCondition(x => x.Id == command.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (orderingProductFile == null)
                {
                    throw new ApiException($"OrderingProductFile Not Found.");
                }
                else
                {
                    await _orderingProductFileRepository.DeleteAsync(orderingProductFile);
                    return new Response<int>(orderingProductFile.Id);
                }
            }
        }
    }
}
