﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
namespace iTrans.Transportation.Application.Features.OrderingProductFiles.Commands
{
    public class CreateOrderingProductFileCommandValidator : AbstractValidator<CreateOrderingProductFileCommand>
    {
        private readonly IOrderingProductFileRepositoryAsync orderingProductFileRepository;

        public CreateOrderingProductFileCommandValidator(IOrderingProductFileRepositoryAsync orderingProductFileRepository)
        {
            this.orderingProductFileRepository = orderingProductFileRepository;

            RuleFor(p => p.OrderingProductId)
                .NotNull().WithMessage("{PropertyName} is required.");

            RuleFor(p => p.FileName)
                .NotEmpty().WithMessage("{PropertyName} is required.")
                .NotNull();

            RuleFor(p => p.ContentType)
               .NotEmpty().WithMessage("{PropertyName} is required.")
               .NotNull();

            RuleFor(p => p.FilePath)
               .NotEmpty().WithMessage("{PropertyName} is required.")
               .NotNull();

            RuleFor(p => p.FileEXT)
               .NotEmpty().WithMessage("{PropertyName} is required.")
               .NotNull();
        }
    }
}
