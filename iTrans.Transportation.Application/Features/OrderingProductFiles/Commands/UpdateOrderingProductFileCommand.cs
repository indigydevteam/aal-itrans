﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;

namespace iTrans.Transportation.Application.Features.OrderingProductFiles.Commands
{
    public class UpdateOrderingProductFileCommand : IRequest<Response<int>>
    {
        public int Id { get; set; }
        public int OrderingProductId { get; set; }
        public string FileName { get; set; }
        public string ContentType { get; set; }
        public string FilePath { get; set; }
        public string FileEXT { get; set; }
    }

    public class UpdateOrderingProductFileCommandHandler : IRequestHandler<UpdateOrderingProductFileCommand, Response<int>>
    {
        private readonly IOrderingProductFileRepositoryAsync _orderingProductFileRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;

        public UpdateOrderingProductFileCommandHandler(IOrderingProductFileRepositoryAsync orderingProductFileRepository,IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _orderingProductFileRepository = orderingProductFileRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(UpdateOrderingProductFileCommand request, CancellationToken cancellationToken)
        {
            var orderingProductFile = (await _orderingProductFileRepository.FindByCondition(x => x.Id == request.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            if (orderingProductFile == null)
            {
                throw new ApiException($"OrderingProductFile Not Found.");
            }
            else
            {
                //orderingProductFile.OrderingProductId = request.OrderingProductId;
                orderingProductFile.FileName = request.FileName;
                orderingProductFile.ContentType = request.ContentType;
                orderingProductFile.FilePath = request.FilePath;
                orderingProductFile.Modified = DateTime.UtcNow;
                await _orderingProductFileRepository.UpdateAsync(orderingProductFile);
                var log = new CreateAppLog(_applicationLogRepository);
                log.Create("Ordering", "Ordering Productfile", "Edit", Newtonsoft.Json.JsonConvert.SerializeObject(orderingProductFile));
                return new Response<int>(orderingProductFile.Id);
            }
        }
    }
}
