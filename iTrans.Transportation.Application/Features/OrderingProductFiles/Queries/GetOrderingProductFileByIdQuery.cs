﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.OrderingProductFile;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.OrderingProductFiles.Queries
{
   public class GetOrderingProductFileByIdQuery : IRequest<Response<OrderingProductFileViewModel>>
    {
        public int Id { get; set; }
    }
    public class GetOrderingProductFileByIdQueryHandler : IRequestHandler<GetOrderingProductFileByIdQuery, Response<OrderingProductFileViewModel>>
    {
        private readonly IOrderingProductFileRepositoryAsync _orderingProductFileRepository;
        private readonly IMapper _mapper;
        public GetOrderingProductFileByIdQueryHandler(IOrderingProductFileRepositoryAsync orderingProductFileRepository, IMapper mapper)
        {
            _orderingProductFileRepository = orderingProductFileRepository;
            _mapper = mapper;
        }
        public async Task<Response<OrderingProductFileViewModel>> Handle(GetOrderingProductFileByIdQuery request, CancellationToken cancellationToken)
        {
            var orderingProductFileObject = (await _orderingProductFileRepository.FindByCondition(x => x.Id.Equals(request.Id)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            return new Response<OrderingProductFileViewModel>(_mapper.Map<OrderingProductFileViewModel>(orderingProductFileObject));
        }
    }
}
