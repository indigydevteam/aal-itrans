﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.OrderingProductFile;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.OrderingProductFiles.Queries
{
    public class GetAllOrderingProductFileQuery : IRequest<Response<IEnumerable<OrderingProductFileViewModel>>>
    {
         
    }
    public class GetAllOrderingProductFileQueryHandler : IRequestHandler<GetAllOrderingProductFileQuery, Response<IEnumerable<OrderingProductFileViewModel>>>
    {
        private readonly IOrderingProductFileRepositoryAsync _orderingProductFileRepository;
        private readonly IMapper _mapper;
        public GetAllOrderingProductFileQueryHandler(IOrderingProductFileRepositoryAsync orderingProductFileRepository, IMapper mapper)
        {
            _orderingProductFileRepository = orderingProductFileRepository;
            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<OrderingProductFileViewModel>>> Handle(GetAllOrderingProductFileQuery request, CancellationToken cancellationToken)
        {
            var orderingProductFile = await _orderingProductFileRepository.GetAllAsync();
            var orderingProductFileViewModel = _mapper.Map<IEnumerable<OrderingProductFileViewModel>>(orderingProductFile);
            return new Response<IEnumerable<OrderingProductFileViewModel>>(orderingProductFileViewModel);
        }
    }
}
