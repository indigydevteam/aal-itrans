﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using iTrans.Transportation.Application.Interfaces.Repositories;
using FluentValidation;

namespace iTrans.Transportation.Application.Features.OrderingProductFiles.Queries
{
    public class GetOrderingProductFileByIdQueryValidator : AbstractValidator<GetOrderingProductFileByIdQuery>
    {
        private readonly IOrderingProductFileRepositoryAsync orderingProductFileRepository;

        public GetOrderingProductFileByIdQueryValidator(IOrderingProductFileRepositoryAsync orderingProductFileRepository)
        {
            this.orderingProductFileRepository = orderingProductFileRepository;
            RuleFor(p => p.Id)
               .NotEmpty().WithMessage("{PropertyName} is required.")
               .NotNull()
               .MustAsync(IsOrderingProductFileExists).WithMessage("{PropertyName} not exists.");
        }

        private async Task<bool> IsOrderingProductFileExists(int Id, CancellationToken cancellationToken)
        {
            var userObject = (await orderingProductFileRepository.FindByCondition(x => x.Id.Equals(Id)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            if (userObject != null)
            {
                return true;
            }
            return false;
        }
    }
}
