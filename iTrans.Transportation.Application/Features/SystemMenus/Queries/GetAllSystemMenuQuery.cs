﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.SystemMenu;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.SystemMenus.Queries
{
    public class GetAllSystemMenuQuery : IRequest<Response<IEnumerable<SystemMenuViewModel>>>
    {

    }
    public class GetAllSystemMenuQueryHandler : IRequestHandler<GetAllSystemMenuQuery, Response<IEnumerable<SystemMenuViewModel>>>
    {
        private readonly ISystemMenuRepositoryAsync _systemMenuRepository;
        private readonly IMapper _mapper;
        public GetAllSystemMenuQueryHandler(ISystemMenuRepositoryAsync systemMenuRepository, IMapper mapper)
        {
            _systemMenuRepository = systemMenuRepository;
            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<SystemMenuViewModel>>> Handle(GetAllSystemMenuQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var systemMenu = await _systemMenuRepository.FindByCondition(x => x.Active == true);
                var SystemMenuViewModel = _mapper.Map<IEnumerable<SystemMenuViewModel>>(systemMenu);
                return new Response<IEnumerable<SystemMenuViewModel>>(SystemMenuViewModel);
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
    }
}
