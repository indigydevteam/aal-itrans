﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.SystemMenu;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.SystemMenus.Queries
{
    public class GetSystemMenuByIdQuery : IRequest<Response<SystemMenuViewModel>>
    {
        public int Id { get; set; }
    }
    public class GetSystemMenuByIdQueryHandler : IRequestHandler<GetSystemMenuByIdQuery, Response<SystemMenuViewModel>>
    {
        private readonly ISystemMenuRepositoryAsync _systemMenuRepository;
        private readonly IMapper _mapper;
        public GetSystemMenuByIdQueryHandler(ISystemMenuRepositoryAsync systemMenuRepository, IMapper mapper)
        {
            _systemMenuRepository = systemMenuRepository;
            _mapper = mapper;
        }
        public async Task<Response<SystemMenuViewModel>> Handle(GetSystemMenuByIdQuery request, CancellationToken cancellationToken)
        {
            var systemMenuObject = (await _systemMenuRepository.FindByCondition(x => x.Id.Equals(request.Id)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            return new Response<SystemMenuViewModel>(_mapper.Map<SystemMenuViewModel>(systemMenuObject));
        }
    }
}
