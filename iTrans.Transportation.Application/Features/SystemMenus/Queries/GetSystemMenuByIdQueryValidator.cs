﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using iTrans.Transportation.Application.Interfaces.Repositories;
using FluentValidation;

namespace iTrans.Transportation.Application.Features.SystemMenus.Queries
{
    public class GetSystemMenuByIdQueryValidator : AbstractValidator<GetSystemMenuByIdQuery>
    {
        private readonly ISystemMenuRepositoryAsync systemMenuRepository;

        public GetSystemMenuByIdQueryValidator(ISystemMenuRepositoryAsync systemMenuRepository)
        {
            this.systemMenuRepository = systemMenuRepository;
            RuleFor(p => p.Id)
               .NotEmpty().WithMessage("{PropertyName} is required.")
               .NotNull()
               .MustAsync(IsSystemMenuExists).WithMessage("{SystemMenuName} not exists.");
        }

        private async Task<bool> IsSystemMenuExists(int systemMenuId, CancellationToken cancellationToken)
        {
            var systemMenuObject = (await systemMenuRepository.FindByCondition(x => x.Id.Equals(systemMenuId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            if (systemMenuObject != null)
            {
                return true;
            }
            return false;
        }
    }
}
