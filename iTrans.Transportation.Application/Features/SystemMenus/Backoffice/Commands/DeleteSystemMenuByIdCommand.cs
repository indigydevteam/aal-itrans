﻿using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.SystemMenus.Backoffice.Commands
{
    public class DeleteSystemMenuByIdCommand : IRequest<Response<int>>
    {
        public int Id { get; set; }
        public class DeleteSystemMenuByIdCommandHandler : IRequestHandler<DeleteSystemMenuByIdCommand, Response<int>>
        {
            private readonly ISystemMenuRepositoryAsync _systemMenuRepository;
            public DeleteSystemMenuByIdCommandHandler(ISystemMenuRepositoryAsync systemMenuRepository)
            {
                _systemMenuRepository = systemMenuRepository;
            }
            public async Task<Response<int>> Handle(DeleteSystemMenuByIdCommand command, CancellationToken cancellationToken)
            {
                var systemMenu = (await _systemMenuRepository.FindByCondition(x => x.Id == command.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (systemMenu == null)
                {
                    throw new ApiException($"SystemMenu Not Found.");
                }
                else
                {
                    await _systemMenuRepository.DeleteAsync(systemMenu);
                    return new Response<int>(systemMenu.Id);
                }
            }
        }
    }
}
