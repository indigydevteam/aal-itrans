﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;

namespace iTrans.Transportation.Application.Features.SystemMenus.Backoffice.Commands
{
    public class UpdateSystemMenuCommand : IRequest<Response<int>>
    {
        public int Id { get; set; }
        public string Name_TH { get; set; }
        public string Name_ENG { get; set; }
        public bool Active { get; set; }
    }

    public class UpdateSystemMenuCommandHandler : IRequestHandler<UpdateSystemMenuCommand, Response<int>>
    {
        private readonly ISystemMenuRepositoryAsync _systemMenuRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;

        public UpdateSystemMenuCommandHandler(ISystemMenuRepositoryAsync systemMenuRepository, IApplicationLogRepositoryAsync applicationLogRepository,IMapper mapper)
        {
            _systemMenuRepository = systemMenuRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(UpdateSystemMenuCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var systemMenu = (await _systemMenuRepository.FindByCondition(x => x.Id == request.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (systemMenu == null)
                {
                    throw new ApiException($"SystemMenu Not Found.");
                }
                else
                {
                    systemMenu.Name_TH = request.Name_TH;
                    systemMenu.Name_ENG = request.Name_ENG;
                    systemMenu.Active = request.Active;
                    systemMenu.Modified = DateTime.UtcNow;

                    await _systemMenuRepository.UpdateAsync(systemMenu);


                    var log = new CreateAppLog(_applicationLogRepository);
                    log.Create("SystemMenu", "SystemMenu", "Edit", Newtonsoft.Json.JsonConvert.SerializeObject(request));
                    return new Response<int>(systemMenu.Id);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
