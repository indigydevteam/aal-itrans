﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using iTrans.Transportation.Application.Interfaces.Repositories;
using FluentValidation;

namespace iTrans.Transportation.Application.Features.SystemMenus.Backoffice.Queries
{
    public class GetSystemMenuByIdQueryValidator : AbstractValidator<BackofficeGetSystemMenuByIdQuery>
    {
        private readonly ISystemMenuRepositoryAsync systemMenuRepository;

        public GetSystemMenuByIdQueryValidator(ISystemMenuRepositoryAsync systemMenuRepository)
        {
            this.systemMenuRepository = systemMenuRepository;
            RuleFor(p => p.Id)
               .NotEmpty().WithMessage("{PropertyName} is required.")
               .NotNull()
               .MustAsync(IsSystemMenuExists).WithMessage("{PropertyName} not exists.");
        }

        private async Task<bool> IsSystemMenuExists(int systemMenuId, CancellationToken cancellationToken)
        {
            var systemMenuObject = (await systemMenuRepository.FindByCondition(x => x.Id.Equals(systemMenuId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            if (systemMenuObject != null)
            {
                return true;
            }
            return false;
        }
    }
}
