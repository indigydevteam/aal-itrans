﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.SystemMenu;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;

namespace iTrans.Transportation.Application.Features.SystemMenus.Backoffice.Queries
{
    public class GetAllSystemMenuParameter : IRequest<PagedResponse<IEnumerable<SystemMenuViewModel>>>
    {
        public string Search { set; get; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
    }
    public class GetAllSystemMenuParameterHandler : IRequestHandler<GetAllSystemMenuParameter, PagedResponse<IEnumerable<SystemMenuViewModel>>>
    {
        private readonly ISystemMenuRepositoryAsync _systemMenuRepository;
        private readonly IMapper _mapper;
        private Expression<Func<SystemMenu, bool>> expression;
        public GetAllSystemMenuParameterHandler(ISystemMenuRepositoryAsync systemMenuRepository, IMapper mapper)
        {
            _systemMenuRepository = systemMenuRepository;
            _mapper = mapper;
        }

        public async Task<PagedResponse<IEnumerable<SystemMenuViewModel>>> Handle(GetAllSystemMenuParameter request, CancellationToken cancellationToken)
        {
            var validFilter = _mapper.Map<GetAllSystemMenuParameter>(request);
            if (validFilter.Search != null && validFilter.Search.Trim() != "")
            {
                expression = x => (x.Name_TH.Contains(validFilter.Search.Trim()) || x.Name_ENG.Contains(validFilter.Search.Trim()));
            }
            int itemCount = _systemMenuRepository.GetItemCount(expression);

            var systemMenu = await _systemMenuRepository.FindByConditionWithPage(expression, validFilter.PageNumber, validFilter.PageSize);
            var systemMenuViewModel = _mapper.Map<IEnumerable<SystemMenuViewModel>>(systemMenu);
            return new PagedResponse<IEnumerable<SystemMenuViewModel>>(systemMenuViewModel, validFilter.PageNumber, validFilter.PageSize);
        }
    }
}
