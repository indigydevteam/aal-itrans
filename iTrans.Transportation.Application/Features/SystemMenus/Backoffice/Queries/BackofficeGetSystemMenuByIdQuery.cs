﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.SystemMenu;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.SystemMenus.Backoffice.Queries
{
    public class BackofficeGetSystemMenuByIdQuery : IRequest<Response<SystemMenuViewModel>>
    {
        public int Id { get; set; }
    }
    public class BackofficeGetSystemMenuByIdQueryHandler : IRequestHandler<BackofficeGetSystemMenuByIdQuery, Response<SystemMenuViewModel>>
    {
        private readonly ISystemMenuRepositoryAsync _systemMenuRepository;
        private readonly IMapper _mapper;
        public BackofficeGetSystemMenuByIdQueryHandler(ISystemMenuRepositoryAsync systemMenuRepository, IMapper mapper)
        {
            _systemMenuRepository = systemMenuRepository;
            _mapper = mapper;
        }
        public async Task<Response<SystemMenuViewModel>> Handle(BackofficeGetSystemMenuByIdQuery request, CancellationToken cancellationToken)
        {
            var systemMenuObject = (await _systemMenuRepository.FindByCondition(x => x.Id.Equals(request.Id)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            return new Response<SystemMenuViewModel>(_mapper.Map<SystemMenuViewModel>(systemMenuObject));
        }
    }
}
