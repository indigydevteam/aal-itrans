﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
namespace iTrans.Transportation.Application.Features.SystemMenus.Commands
{
    public class CreateSystemMenuCommandValidator : AbstractValidator<CreateSystemMenuCommand>
    {
        private readonly ISystemMenuRepositoryAsync systemMenuRepository;

        public CreateSystemMenuCommandValidator(ISystemMenuRepositoryAsync systemMenuRepository)
        {
            this.systemMenuRepository = systemMenuRepository;

            RuleFor(p => p.Name_TH)
                .NotEmpty().WithMessage("{PropertyName} is required.")
                .NotNull()
                .MaximumLength(50).WithMessage("{PropertyName} must not exceed 50 characters.");


            RuleFor(p => p.Name_ENG)
                .NotEmpty().WithMessage("{PropertyName} is required.")
                .NotNull()
                .MaximumLength(50).WithMessage("{PropertyName} must not exceed 50 characters.");
        }


    }
}
