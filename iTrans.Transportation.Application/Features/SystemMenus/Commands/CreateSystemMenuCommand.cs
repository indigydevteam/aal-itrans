﻿using AutoMapper;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Interfaces.Services;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.SystemMenus.Commands
{
    public partial class CreateSystemMenuCommand : IRequest<Response<int>>
    { 
        public string Name_TH { get; set; }
        public string Name_ENG { get; set; }
        public bool Active { get; set; }

    }
    public class CreateSystemMenuCommandHandler : IRequestHandler<CreateSystemMenuCommand, Response<int>>
    {
        private readonly ISystemMenuRepositoryAsync _systemMenuRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;
        public CreateSystemMenuCommandHandler(ISystemMenuRepositoryAsync systemMenuRepository, IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _systemMenuRepository = systemMenuRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
        }


        public async Task<Response<int>> Handle(CreateSystemMenuCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var systemMenu = _mapper.Map<SystemMenu>(request);
                systemMenu.Created = DateTime.UtcNow;
                systemMenu.Modified = DateTime.UtcNow;
                //country.CreatedBy = "xxxxxxx";
                //country.ModifiedBy = "xxxxxxx";
                var systemMenuObject = await _systemMenuRepository.AddAsync(systemMenu);

                var log = new CreateAppLog(_applicationLogRepository);
                log.Create("SystemMenu", "SystemMenu", "Create", Newtonsoft.Json.JsonConvert.SerializeObject(request));
                return new Response<int>(systemMenu.Id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
