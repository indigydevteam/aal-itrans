﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.CarDescription;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;

namespace iTrans.Transportation.Application.Features.CarDescriptions.Queries
{
    public class GetAllCarDescriptionParameter : IRequest<PagedResponse<IEnumerable<CarDescriptionViewModel>>>
    {
        public string Search { set; get; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
    }
    public class GetAllCarDescriptionParameterHandler : IRequestHandler<GetAllCarDescriptionParameter, PagedResponse<IEnumerable<CarDescriptionViewModel>>>
    {
        private readonly ICarDescriptionRepositoryAsync _carDescriptionRepository;
        private readonly IMapper _mapper;
        private Expression<Func<CarDescription, bool>> expression;

        public GetAllCarDescriptionParameterHandler(ICarDescriptionRepositoryAsync carDescriptionRepository, IMapper mapper)
        {
            _carDescriptionRepository = carDescriptionRepository;
            _mapper = mapper;
        }

        public async Task<PagedResponse<IEnumerable<CarDescriptionViewModel>>> Handle(GetAllCarDescriptionParameter request, CancellationToken cancellationToken)
        {
            var validFilter = _mapper.Map<GetAllCarDescriptionParameter>(request);
            if (validFilter.Search != null && validFilter.Search.Trim() != "")
            {
                expression = x => (x.Name_TH.Contains(validFilter.Search.Trim()) || x.Name_ENG.Contains(validFilter.Search.Trim()) || x.CarList.Name_ENG.Contains(validFilter.Search.Trim()) || x.CarList.Name_TH.Contains(validFilter.Search.Trim()) || x.CarList.CarType.Name_TH.Contains(validFilter.Search.Trim()) || x.Sequence.ToString().Contains(validFilter.Search.Trim()) || x.CarList.CarType.Name_ENG.Contains(validFilter.Search.Trim()));
            }
            int itemCount = _carDescriptionRepository.GetItemCount(expression);

            // Expression<Func<CarDescription, bool>> expression = x => x.Name_TH != "";
            var carDescription = await _carDescriptionRepository.FindByConditionWithPage(expression, validFilter.PageNumber, validFilter.PageSize);
            var carDescriptionViewModel = _mapper.Map<IEnumerable<CarDescriptionViewModel>>(carDescription);
            return new PagedResponse<IEnumerable<CarDescriptionViewModel>>(carDescriptionViewModel, validFilter.PageNumber, validFilter.PageSize, itemCount);
        }
    }
}
