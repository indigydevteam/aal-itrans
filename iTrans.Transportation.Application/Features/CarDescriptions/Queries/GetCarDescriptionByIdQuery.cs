﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.CarDescription.Backoffice;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.CarDescriptions.Queries
{
    public class GetCarDescriptionByIdQuery : IRequest<Response<CarDescriptionViewModel>>
    {
        public int Id { get; set; }
    }
    public class GetCarDescriptionByIdQueryHandler : IRequestHandler<GetCarDescriptionByIdQuery, Response<CarDescriptionViewModel>>
    {
        private readonly ICarDescriptionRepositoryAsync _carDescriptionRepository;
        private readonly IMapper _mapper;
        public GetCarDescriptionByIdQueryHandler(ICarDescriptionRepositoryAsync carDescriptionRepository, IMapper mapper)
        {
            _carDescriptionRepository = carDescriptionRepository;
            _mapper = mapper;
        }
        public async Task<Response<CarDescriptionViewModel>> Handle(GetCarDescriptionByIdQuery request, CancellationToken cancellationToken)
        {
            var carDescriptionObject = (await _carDescriptionRepository.FindByCondition(x => x.Id.Equals(request.Id)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            return new Response<CarDescriptionViewModel>(_mapper.Map<CarDescriptionViewModel>(carDescriptionObject));
        }
    }
}
