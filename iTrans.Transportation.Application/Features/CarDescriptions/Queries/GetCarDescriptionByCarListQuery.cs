﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.CarDescription;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.CarDescriptions.Queries
{
    public class GetCarDescriptionByCarListQuery : IRequest<Response<IEnumerable<CarDescriptionViewModel>>>
    {
        public int CarListId { get; set; }
    }
    public class GetCarDescriptionByCarListQueryHandler : IRequestHandler<GetCarDescriptionByCarListQuery, Response<IEnumerable<CarDescriptionViewModel>>>
    {
        private readonly ICarDescriptionRepositoryAsync _carDescriptionRepository;
        private readonly IMapper _mapper;
        public GetCarDescriptionByCarListQueryHandler(ICarDescriptionRepositoryAsync carDescriptionRepository, IMapper mapper)
        {
            _carDescriptionRepository = carDescriptionRepository;
            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<CarDescriptionViewModel>>> Handle(GetCarDescriptionByCarListQuery request, CancellationToken cancellationToken)
        {
            var carDescription = (await _carDescriptionRepository.FindByCondition(x => x.CarList.Id.Equals(request.CarListId) && x.Active == true).ConfigureAwait(false)).OrderBy(x => x.Sequence).AsQueryable().ToList();
            var carDescriptionViewModel = _mapper.Map<IEnumerable<CarDescriptionViewModel>>(carDescription);
            return new Response<IEnumerable<CarDescriptionViewModel>>(carDescriptionViewModel);
        }
    }
}