﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using iTrans.Transportation.Application.Interfaces.Repositories;
using FluentValidation;

namespace iTrans.Transportation.Application.Features.CarDescriptions.Queries
{
    public class GetCarDescriptionByIdQueryValidator : AbstractValidator<GetCarDescriptionByIdQuery>
    {
        private readonly ICarDescriptionRepositoryAsync carDescriptionRepository;

        public GetCarDescriptionByIdQueryValidator(ICarDescriptionRepositoryAsync carDescriptionRepository)
        {
            this.carDescriptionRepository = carDescriptionRepository;
            RuleFor(p => p.Id)
               .NotEmpty().WithMessage("{PropertyName} is required.")
               .NotNull()
               .MustAsync(IsCarDescriptionExists).WithMessage("{PropertyName} not exists.");
        }

        private async Task<bool> IsCarDescriptionExists(int CarDescriptionId, CancellationToken cancellationToken)
        {
            var userObject = (await carDescriptionRepository.FindByCondition(x => x.Id.Equals(CarDescriptionId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            if (userObject != null)
            {
                return true;
            }
            return false;
        }
    }
}
