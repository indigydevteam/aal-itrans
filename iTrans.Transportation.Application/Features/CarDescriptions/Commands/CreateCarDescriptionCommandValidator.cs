﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.CarDescriptions.Commands
{
    public class CreateCarDescriptionCommandValidator : AbstractValidator<CreateCarDescriptionCommand>
    {
        private readonly ICarListRepositoryAsync carListRepository;

        public CreateCarDescriptionCommandValidator(ICarListRepositoryAsync carListRepository)
        {
            this.carListRepository = carListRepository;

            RuleFor(p => p.Name_TH)
                .NotEmpty().WithMessage("{PropertyName} is required.")
                .NotNull()
                .MaximumLength(50).WithMessage("{PropertyName} must not exceed 50 characters.");


            RuleFor(p => p.Name_ENG)
                .NotEmpty().WithMessage("{PropertyName} is required.")
                .NotNull()
                .MaximumLength(50).WithMessage("{PropertyName} must not exceed 50 characters.");

            RuleFor(p => p.CarListId)
               .NotNull().WithMessage("{PropertyName} is required.")
               .MustAsync(IsExistCarList).WithMessage("{PropertyName} not exists.");
        }

        private async Task<bool> IsExistCarList(int carListId, CancellationToken cancellationToken)
        {
            var carListObject = (await carListRepository.FindByCondition(x => x.Id == carListId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            if (carListObject == null)
            {
                return false;
            }
            return true;
        }
    }
}
