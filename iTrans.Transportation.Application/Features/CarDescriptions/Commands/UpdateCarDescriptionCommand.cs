﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;

namespace iTrans.Transportation.Application.Features.CarDescriptions.Commands
{
    public class UpdateCarDescriptionCommand : IRequest<Response<int>>
    {
        public Guid? UserId { get; set; }
        public int Id { get; set; }
        public int CarListId { get; set; }
        public string Name_TH { get; set; }
        public string Name_ENG { get; set; }
        public int Sequence { get; set; }
        public bool Active { get; set; }
        public virtual bool Specified { get; set; }

    }

    public class UpdateCarDescriptionCommandHandler : IRequestHandler<UpdateCarDescriptionCommand, Response<int>>
    {
        private readonly ICarDescriptionRepositoryAsync _carDescriptionRepository;
        private readonly ICarListRepositoryAsync _carListRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;

        public UpdateCarDescriptionCommandHandler(ICarDescriptionRepositoryAsync carDescriptionRepository, ICarListRepositoryAsync carListRepository, IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _carDescriptionRepository = carDescriptionRepository;
            _carListRepository = carListRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(UpdateCarDescriptionCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var carDescription = (await _carDescriptionRepository.FindByCondition(x => x.Id == request.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (carDescription == null)
                {
                    throw new ApiException($"CarDescriptiont Not Found.");
                }
                else
                {
                    var carList = (await _carListRepository.FindByCondition(x => x.Id == request.CarListId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                    if (carList == null)
                    {
                        throw new ApiException($"carList Not Found.");
                    }
                    carDescription.Name_TH = request.Name_TH;
                    carDescription.Name_ENG = request.Name_ENG;
                    carDescription.CarList = carList;
                    carDescription.Sequence = request.Sequence;
                    carDescription.Active = request.Active;
                    carDescription.Specified = request.Specified;

                    await _carDescriptionRepository.UpdateAsync(carDescription);
                    var log = new CreateAppLog(_applicationLogRepository);
                    log.CreateLog("Car", "Car Description", "Edit", Newtonsoft.Json.JsonConvert.SerializeObject(carDescription),request.UserId.ToString());
                    return new Response<int>(carDescription.Id);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
