﻿using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.CarDescriptions.Commands
{
    public class DeleteCarDescriptionByIdCommand : IRequest<Response<int>>
    {
        public int Id { get; set; }
        public class DeleteCarDescriptionByIdCommandHandler : IRequestHandler<DeleteCarDescriptionByIdCommand, Response<int>>
        {
            private readonly ICarDescriptionRepositoryAsync _carDescriptionRepository;
            public DeleteCarDescriptionByIdCommandHandler(ICarDescriptionRepositoryAsync carDescriptionRepository)
            {
                _carDescriptionRepository = carDescriptionRepository;
            }
            public async Task<Response<int>> Handle(DeleteCarDescriptionByIdCommand command, CancellationToken cancellationToken)
            {
                var carDescription = (await _carDescriptionRepository.FindByCondition(x => x.Id == command.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (carDescription == null)
                {
                    throw new ApiException($"CarDescription Not Found.");
                }
                else
                {
                    await _carDescriptionRepository.DeleteAsync(carDescription);
                    return new Response<int>(carDescription.Id);
                }
            }
        }
    }
}
