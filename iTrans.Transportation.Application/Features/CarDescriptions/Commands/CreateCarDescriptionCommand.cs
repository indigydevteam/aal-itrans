﻿using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Interfaces.Services;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.CarDescriptions.Commands
{
    public partial class CreateCarDescriptionCommand : IRequest<Response<int>>
    {
        public Guid? UserId { get; set; }
        public int CarListId { get; set; }
        public string Name_TH { get; set; }
        public string Name_ENG { get; set; }
        public int Sequence { get; set; }
        public bool Active { get; set; }
        public  bool Specified { get; set; }


    }
    public class CreateCarDescriptionCommandHandler : IRequestHandler<CreateCarDescriptionCommand, Response<int>>
    {
        private readonly ICarListRepositoryAsync _carListRepository;
        private readonly ICarDescriptionRepositoryAsync _carDescriptionRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;
        public CreateCarDescriptionCommandHandler(ICarDescriptionRepositoryAsync carDescriptionRepository, ICarListRepositoryAsync carListRepository, IApplicationLogRepositoryAsync applicationLogRepository,IMapper mapper)
        {
            _carListRepository = carListRepository;
            _carDescriptionRepository = carDescriptionRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(CreateCarDescriptionCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var carList = (await _carListRepository.FindByCondition(x => x.Id == request.CarListId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (carList == null)
                {
                    throw new ApiException($"CarList Not Found.");
                }
                var carDescription = _mapper.Map<CarDescription>(request);
                carDescription.CarList = carList;
                carDescription.Created = DateTime.UtcNow;
                carDescription.Modified = DateTime.UtcNow;
                //title.CreatedBy = "xxxxxxx";
                //title.ModifiedBy = "xxxxxxx";
                var carDescriptionObject = await _carDescriptionRepository.AddAsync(carDescription);
                var log = new CreateAppLog(_applicationLogRepository);
                log.CreateLog("Car", "Car Description", "Create", Newtonsoft.Json.JsonConvert.SerializeObject(carDescription),request.UserId.ToString());
                return new Response<int>(carDescriptionObject.Id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
