﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.Contact;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.Contacts.Queries
{
    public class GetContactQuery : IRequest<Response<ContactViewModel>>
    {
    }
    public class GetContactQueryHandler : IRequestHandler<GetContactQuery, Response<ContactViewModel>>
    {
        private readonly IContactRepositoryAsync _ContactRepository;
        private readonly IMapper _mapper;
        public GetContactQueryHandler(IContactRepositoryAsync ContactRepository, IMapper mapper)
        {
            _ContactRepository = ContactRepository;
            _mapper = mapper;
        }
        public async Task<Response<ContactViewModel>> Handle(GetContactQuery request, CancellationToken cancellationToken)
        {
            var ContactObject = (await _ContactRepository.GetAllAsync());
            return new Response<ContactViewModel>(_mapper.Map<ContactViewModel>(ContactObject.FirstOrDefault()));
        }
    }
}
