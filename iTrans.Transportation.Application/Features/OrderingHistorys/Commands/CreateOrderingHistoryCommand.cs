﻿using AutoMapper;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Interfaces.Services;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using iTrans.Transportation.Domain.Entities;
using MediatR;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.OrderingHistorys.Commands
{
   public partial class CreateOrderingHistoryCommand : IRequest<Response<int>>
    {
        public  Guid OrderingId { get; set; }
        public  Guid DriverId { get; set; }
        public  Guid CustomerId { get; set; }
        public  int ProductCBM { get; set; }
        public  float ProductTotalWeight { get; set; }
        public  decimal OrderingPrice { get; set; }
        public  int OrderingStatus { get; set; }
        public  string AdditionalDetail { get; set; }
        public  string Note { get; set; }
    }
    public class CreateOrderingHistoryCommandHandler : IRequestHandler<CreateOrderingHistoryCommand, Response<int>>
    {
        private readonly IOrderingHistoryRepositoryAsync _orderingHistoryRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;
        public CreateOrderingHistoryCommandHandler(IOrderingHistoryRepositoryAsync orderingHistoryRepository, IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _orderingHistoryRepository = orderingHistoryRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(CreateOrderingHistoryCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var OrderingHistory = _mapper.Map<OrderingHistory>(request);
                OrderingHistory.Created = DateTime.UtcNow;
                OrderingHistory.Modified = DateTime.UtcNow;
                //title.CreatedBy = "xxxxxxx";
                //title.ModifiedBy = "xxxxxxx";
                var OrderingHistoryObject = await _orderingHistoryRepository.AddAsync(OrderingHistory);
                var log = new CreateAppLog(_applicationLogRepository);
                log.Create("Ordering", "Ordering History", "Create", Newtonsoft.Json.JsonConvert.SerializeObject(OrderingHistory));
                return new Response<int>(OrderingHistoryObject.Id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}