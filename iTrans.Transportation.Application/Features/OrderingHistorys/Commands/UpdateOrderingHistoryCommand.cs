﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;

namespace iTrans.Transportation.Application.Features.OrderingHistorys.Commands
{
  public  class UpdateOrderingHistoryCommand : IRequest<Response<int>>
    {
        public  int Id { get; set; }
        public Guid OrderingId { get; set; }
        public Guid DriverId { get; set; }
        public  Guid CustomerId { get; set; }
        public  int ProductCBM { get; set; }
        public  float ProductTotalWeight { get; set; }
        public  decimal OrderingPrice { get; set; }
        public  int OrderingStatus { get; set; }
        public  string AdditionalDetail { get; set; }
        public  string Note { get; set; }

    }

    public class UpdateOrderingHistoryCommandHandler : IRequestHandler<UpdateOrderingHistoryCommand, Response<int>>
    {
        private readonly IOrderingHistoryRepositoryAsync _orderingHistoryRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;

        public UpdateOrderingHistoryCommandHandler(IOrderingHistoryRepositoryAsync orderingHistoryRepository,IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _orderingHistoryRepository = orderingHistoryRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(UpdateOrderingHistoryCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var OrderingHistory = (await _orderingHistoryRepository.FindByCondition(x => x.Id == request.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (OrderingHistory == null)
                {
                    throw new ApiException($"OrderingHistory Not Found.");
                }
                else
                {
                    OrderingHistory.OrderingId = request.OrderingId;
                    OrderingHistory.DriverId = request.DriverId;
                    OrderingHistory.CustomerId = request.CustomerId;
                    OrderingHistory.ProductCBM = request.ProductCBM;
                    OrderingHistory.ProductTotalWeight = request.ProductTotalWeight;
                    OrderingHistory.OrderingPrice = request.OrderingPrice;
                    OrderingHistory.OrderingStatus = request.OrderingStatus;
                    OrderingHistory.AdditionalDetail = request.AdditionalDetail;
                    OrderingHistory.Note = request.Note;
                  
                    await _orderingHistoryRepository.UpdateAsync(OrderingHistory);
                    var log = new CreateAppLog(_applicationLogRepository);
                    log.Create("Ordering", "Ordering History ", "Edit", Newtonsoft.Json.JsonConvert.SerializeObject(OrderingHistory));
                    return new Response<int>(OrderingHistory.Id);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}