﻿using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.OrderingHistorys.Commands
{
   public class DeleteOrderingHistoryCommand : IRequest<Response<int>>
    {
        public int Id { get; set; }
        public class DeleteOrderingHistoryCommandHandler : IRequestHandler<DeleteOrderingHistoryCommand, Response<int>>
        {
            private readonly IOrderingHistoryRepositoryAsync _orderingHistoryRepository;
            public DeleteOrderingHistoryCommandHandler(IOrderingHistoryRepositoryAsync orderingHistoryRepository)
            {
                _orderingHistoryRepository = orderingHistoryRepository;
            }
            public async Task<Response<int>> Handle(DeleteOrderingHistoryCommand command, CancellationToken cancellationToken)
            {
                var OrderingHistory = (await _orderingHistoryRepository.FindByCondition(x => x.Id == command.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (OrderingHistory == null)
                {
                    throw new ApiException($"OrderingHistory Not Found.");
                }
                else
                {
                    await _orderingHistoryRepository.DeleteAsync(OrderingHistory);
                    return new Response<int>(OrderingHistory.Id);
                }
            }
        }
    }
}