﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.Ordering;
using iTrans.Transportation.Application.DTOs.OrderingHistory;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
namespace iTrans.Transportation.Application.Features.OrderingHistorys.Queries
{
    public class GetOrderingHistoryByOrderingQuery : IRequest<Response<IEnumerable<OrderingHistoryViewModel>>>
    {
        public int OrderingId { set; get; }
    }
    public class GetOrderingHistoryByOrderingQueryHandler : IRequestHandler<GetOrderingHistoryByOrderingQuery, Response<IEnumerable<OrderingHistoryViewModel>>>
    {
        private readonly IOrderingHistoryRepositoryAsync _orderingHistoryRepository;
        private readonly IMapper _mapper;
        public GetOrderingHistoryByOrderingQueryHandler(IOrderingHistoryRepositoryAsync orderingHistoryRepository, IMapper mapper)
        {
            _orderingHistoryRepository = orderingHistoryRepository;
            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<OrderingHistoryViewModel>>> Handle(GetOrderingHistoryByOrderingQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var OrderingHistory = (await _orderingHistoryRepository.FindByCondition(x => x.OrderingId.Equals(request.OrderingId)).ConfigureAwait(false)).AsQueryable().ToList();
                var OrderingHistoryViewModel = _mapper.Map<IEnumerable<OrderingHistoryViewModel>>(OrderingHistory);
                return new Response<IEnumerable<OrderingHistoryViewModel>>(OrderingHistoryViewModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}