﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.OrderingHistory;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;

namespace iTrans.Transportation.Application.Features.OrderingHistorys.Queries
{
    public class GetAllOrderingHistoryParameter : IRequest<PagedResponse<IEnumerable<OrderingHistoryViewModel>>>
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
    }
    public class GetAllOrderingHistoryParameterHandler : IRequestHandler<GetAllOrderingHistoryParameter, PagedResponse<IEnumerable<OrderingHistoryViewModel>>>
    {
        private readonly IOrderingHistoryRepositoryAsync _OrderingHistoryRepository;
        private readonly IMapper _mapper;
        public GetAllOrderingHistoryParameterHandler(IOrderingHistoryRepositoryAsync OrderingHistoryRepository, IMapper mapper)
        {
            _OrderingHistoryRepository = OrderingHistoryRepository;
            _mapper = mapper;
        }

        public async Task<PagedResponse<IEnumerable<OrderingHistoryViewModel>>> Handle(GetAllOrderingHistoryParameter request, CancellationToken cancellationToken)
        {
            try
            {
                var validFilter = _mapper.Map<GetAllOrderingHistoryParameter>(request);
                Expression<Func<OrderingHistory, bool>> expression = x => x.TrackingCode != "";
                var OrderingHistory = await _OrderingHistoryRepository.FindByConditionWithPage(expression, validFilter.PageNumber, validFilter.PageSize);
                var OrderingHistoryViewModel = _mapper.Map<IEnumerable<OrderingHistoryViewModel>>(OrderingHistory);
                return new PagedResponse<IEnumerable<OrderingHistoryViewModel>>(OrderingHistoryViewModel, validFilter.PageNumber, validFilter.PageSize);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}