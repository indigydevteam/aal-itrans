﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.OrderingHistory;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
namespace iTrans.Transportation.Application.Features.OrderingHistorys.Queries
{
    public class GetAllOrderingHistoryQuery : IRequest<Response<IEnumerable<OrderingHistoryViewModel>>>
    {

    }
    public class GetAllOrderingHistoryQueryHandler : IRequestHandler<GetAllOrderingHistoryQuery, Response<IEnumerable<OrderingHistoryViewModel>>>
    {
        private readonly IOrderingHistoryRepositoryAsync _orderingHistoryRepository;
        private readonly IMapper _mapper;
        public GetAllOrderingHistoryQueryHandler(IOrderingHistoryRepositoryAsync OrderingHistoryRepository, IMapper mapper)
        {
            _orderingHistoryRepository = OrderingHistoryRepository;
            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<OrderingHistoryViewModel>>> Handle(GetAllOrderingHistoryQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var OrderingHistory = await _orderingHistoryRepository.GetAllAsync();
                var OrderingHistoryViewModel = _mapper.Map<IEnumerable<OrderingHistoryViewModel>>(OrderingHistory);
                return new Response<IEnumerable<OrderingHistoryViewModel>>(OrderingHistoryViewModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
