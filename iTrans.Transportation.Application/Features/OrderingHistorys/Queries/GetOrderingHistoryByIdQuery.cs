﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.OrderingHistory;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.OrderingHistorys.Queries
{
    public class GetOrderingHistoryByIdQuery : IRequest<Response<OrderingHistoryViewModel>>
    {
        public int Id { get; set; }
    }
    public class GetOrderingHistoryByIdQueryHandler : IRequestHandler<GetOrderingHistoryByIdQuery, Response<OrderingHistoryViewModel>>
    {
        private readonly IOrderingHistoryRepositoryAsync _OrderingHistoryRepository;
        private readonly IMapper _mapper;
        public GetOrderingHistoryByIdQueryHandler(IOrderingHistoryRepositoryAsync OrderingHistoryRepository, IMapper mapper)
        {
            _OrderingHistoryRepository = OrderingHistoryRepository;
            _mapper = mapper;
        }
        public async Task<Response<OrderingHistoryViewModel>> Handle(GetOrderingHistoryByIdQuery request, CancellationToken cancellationToken)
        {
            var OrderingHistoryObject = (await _OrderingHistoryRepository.FindByCondition(x => x.Id.Equals(request.Id)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            return new Response<OrderingHistoryViewModel>(_mapper.Map<OrderingHistoryViewModel>(OrderingHistoryObject));
        }
    }
}
