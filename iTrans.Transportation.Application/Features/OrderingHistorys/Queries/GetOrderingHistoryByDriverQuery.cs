﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.Ordering;
using iTrans.Transportation.Application.DTOs.OrderingHistory;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
namespace iTrans.Transportation.Application.Features.OrderingHistorys.Queries
{
    public class GetOrderingHistoryByDriverQuery : IRequest<Response<OrderingHistoryViewModel>>
    {
        public Guid DriverId { get; set; }
    }
    public class GetOrderingHistoryByDriverIdQueryHandler : IRequestHandler<GetOrderingHistoryByDriverQuery, Response<OrderingHistoryViewModel>>
    {
        private readonly IOrderingHistoryRepositoryAsync _orderingHistoryRepository;
        private readonly IMapper _mapper;
        public GetOrderingHistoryByDriverIdQueryHandler(IOrderingHistoryRepositoryAsync orderingHistoryRepository, IMapper mapper)
        {
            _orderingHistoryRepository = orderingHistoryRepository;
            _mapper = mapper;
        }
        public async Task<Response<OrderingHistoryViewModel>> Handle(GetOrderingHistoryByDriverQuery request, CancellationToken cancellationToken)
        {
            var driverObject = (await _orderingHistoryRepository.FindByCondition(x => x.DriverId.Equals(request.DriverId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            return new Response<OrderingHistoryViewModel>(_mapper.Map<OrderingHistoryViewModel>(driverObject));
        }
    }
}
