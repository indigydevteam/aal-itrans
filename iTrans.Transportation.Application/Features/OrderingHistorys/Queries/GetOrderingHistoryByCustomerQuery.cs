﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.Ordering;
using iTrans.Transportation.Application.DTOs.OrderingHistory;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
namespace iTrans.Transportation.Application.Features.OrderingHistorys.Queries
{
   public class GetOrderingHistoryByCustomerQuery : IRequest<Response<OrderingHistoryViewModel>>
    {
        public Guid CustomerId { get; set; }
    }
    public class GetOrderingHistoryByCustomerIdIdQueryHandler : IRequestHandler<GetOrderingHistoryByCustomerQuery, Response<OrderingHistoryViewModel>>
    {
        private readonly IOrderingHistoryRepositoryAsync _orderingHistoryRepository;
        private readonly IMapper _mapper;
        public GetOrderingHistoryByCustomerIdIdQueryHandler(IOrderingHistoryRepositoryAsync orderingHistoryRepository, IMapper mapper)
        {
            _orderingHistoryRepository = orderingHistoryRepository;
            _mapper = mapper;
        }
        public async Task<Response<OrderingHistoryViewModel>> Handle(GetOrderingHistoryByCustomerQuery request, CancellationToken cancellationToken)
        {
            var CustomerIdObject = (await _orderingHistoryRepository.FindByCondition(x => x.CustomerId.Equals(request.CustomerId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            return new Response<OrderingHistoryViewModel>(_mapper.Map<OrderingHistoryViewModel>(CustomerIdObject));
        }
    }
}
