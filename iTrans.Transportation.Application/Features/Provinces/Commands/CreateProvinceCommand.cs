﻿using AutoMapper;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Interfaces.Services;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.Provinces.Commands
{
    public partial class CreateProvinceCommand : IRequest<Response<int>>
    { 
        public int CountryId { get; set; }
        public string Name_TH { get; set; }
        public string Name_ENG { get; set; }
        public bool Active { get; set; }

    }
    public class CreateProvinceCommandHandler : IRequestHandler<CreateProvinceCommand, Response<int>>
    {
        private readonly IProvinceRepositoryAsync _provinceRepository;
        private readonly IMapper _mapper;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        public CreateProvinceCommandHandler(IProvinceRepositoryAsync provinceRepository, IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _provinceRepository = provinceRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
        }


        public async Task<Response<int>> Handle(CreateProvinceCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var province = _mapper.Map<Province>(request);
                province.Created = DateTime.UtcNow;
                province.Modified = DateTime.UtcNow;
                //country.CreatedBy = "xxxxxxx";
                //country.ModifiedBy = "xxxxxxx";
                var provinceObject = await _provinceRepository.AddAsync(province);
                var log = new CreateAppLog(_applicationLogRepository);
                log.Create("Province","Province","Create",Newtonsoft.Json.JsonConvert.SerializeObject(province));
                return new Response<int>(provinceObject.Id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
