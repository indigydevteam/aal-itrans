﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;

namespace iTrans.Transportation.Application.Features.Provinces.Commands
{
    public class UpdateProvinceCommand : IRequest<Response<int>>
    {
        public int Id { get; set; }
        public int CountryId { get; set; }
        public int RegionId { get; set; }
        public string Name_TH { get; set; }
        public string Name_ENG { get; set; }
        public bool Active { get; set; }
    }

    public class UpdateProvinceCommandHandler : IRequestHandler<UpdateProvinceCommand, Response<int>>
    {
        private readonly IProvinceRepositoryAsync _provinceRepository;
        private readonly IRegionRepositoryAsync _regionRepository;
        private readonly ICountryRepositoryAsync _countryRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;

        public UpdateProvinceCommandHandler(IRegionRepositoryAsync regionRepository, ICountryRepositoryAsync countryRepository, IProvinceRepositoryAsync provinceRepository,IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _regionRepository = regionRepository;
            _countryRepository = countryRepository;
            _provinceRepository = provinceRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(UpdateProvinceCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var province = (await _provinceRepository.FindByCondition(x => x.Id == request.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (province == null)
                {
                    throw new ApiException($"Province Not Found.");
                }
                else
                {
                    var country = (await _countryRepository.FindByCondition(x => x.Id == request.CountryId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                    province.Country = country;
                    var region = (await _regionRepository.FindByCondition(x => x.Id == request.RegionId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                    province.Region = region;
                    province.Name_TH = request.Name_TH;
                    province.Name_ENG = request.Name_ENG;
                    province.Active = request.Active;
                    province.Modified = DateTime.UtcNow;

                    await _provinceRepository.UpdateAsync(province);
                    var log = new CreateAppLog(_applicationLogRepository);
                    log.Create("Province", "Province","Edit", Newtonsoft.Json.JsonConvert.SerializeObject(province));
                    return new Response<int>(province.Id);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
