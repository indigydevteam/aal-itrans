﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.Province.Backoffice;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.Provinces.Backoffice.Queries
{
    public class BackofficeGetProvinceByIdQuery : IRequest<Response<ProvinceViewModel>>
    {
        public int Id { get; set; }
    }
    public class BackofficeGetProvinceByIdQueryHandler : IRequestHandler<BackofficeGetProvinceByIdQuery, Response<ProvinceViewModel>>
    {
        private readonly IProvinceRepositoryAsync _provinceRepository;
        private readonly IMapper _mapper;
        public BackofficeGetProvinceByIdQueryHandler(IProvinceRepositoryAsync provinceRepository, IMapper mapper)
        {
            _provinceRepository = provinceRepository;
            _mapper = mapper;
        }
        public async Task<Response<ProvinceViewModel>> Handle(BackofficeGetProvinceByIdQuery request, CancellationToken cancellationToken)
        {
            var provinceObject = (await _provinceRepository.FindByCondition(x => x.Id.Equals(request.Id)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            return new Response<ProvinceViewModel>(_mapper.Map<ProvinceViewModel>(provinceObject));
        }
    }
}
