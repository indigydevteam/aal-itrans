﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.Province.Backoffice;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;

namespace iTrans.Transportation.Application.Features.Provinces.Backoffice.Queries
{
    public class GetAllProvinceParameter : IRequest<PagedResponse<IEnumerable<ProvinceViewModel>>>
    {
        public string Search { set; get; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
    }
    public class GetAllProvinceParameterHandler : IRequestHandler<GetAllProvinceParameter, PagedResponse<IEnumerable<ProvinceViewModel>>>
    {
        private readonly IProvinceRepositoryAsync _provinceRepository;
        private readonly IMapper _mapper;
        private Expression<Func<Province, bool>> expression;
        public GetAllProvinceParameterHandler(IProvinceRepositoryAsync provinceRepository, IMapper mapper)
        {
            _provinceRepository = provinceRepository;
            _mapper = mapper;
        }

        public async Task<PagedResponse<IEnumerable<ProvinceViewModel>>> Handle(GetAllProvinceParameter request, CancellationToken cancellationToken)
        {
            var validFilter = _mapper.Map<GetAllProvinceParameter>(request);
            if (validFilter.Search != null && validFilter.Search.Trim() != "")
            {
                expression = x => (x.Name_TH.Contains(validFilter.Search.Trim()) || x.Name_ENG.Contains(validFilter.Search.Trim()));
            }
            int itemCount = _provinceRepository.GetItemCount(expression);

            //Expression<Func<Province, bool>> expression = x => x.Name_TH != "";
            var province = await _provinceRepository.FindByConditionWithPage(expression, validFilter.PageNumber, validFilter.PageSize);
            var provinceViewModel = _mapper.Map<IEnumerable<ProvinceViewModel>>(province);
            return new PagedResponse<IEnumerable<ProvinceViewModel>>(provinceViewModel, validFilter.PageNumber, validFilter.PageSize, itemCount);
        }
    }
}
