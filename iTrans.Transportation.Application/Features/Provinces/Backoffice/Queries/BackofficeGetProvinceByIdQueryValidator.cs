﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using iTrans.Transportation.Application.Interfaces.Repositories;
using FluentValidation;

namespace iTrans.Transportation.Application.Features.Provinces.Backoffice.Queries
{
    public class GetProvinceByIdQueryValidator : AbstractValidator<BackofficeGetProvinceByIdQuery>
    {
        private readonly IProvinceRepositoryAsync provinceRepository;

        public GetProvinceByIdQueryValidator(IProvinceRepositoryAsync provinceRepository)
        {
            this.provinceRepository = provinceRepository;
            RuleFor(p => p.Id)
               .NotEmpty().WithMessage("{PropertyName} is required.")
               .NotNull()
               .MustAsync(IsProvinceExists).WithMessage("{PropertyName} not exists.");
        }

        private async Task<bool> IsProvinceExists(int provinceId, CancellationToken cancellationToken)
        {
            var provinceObject = (await provinceRepository.FindByCondition(x => x.Id.Equals(provinceId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            if (provinceObject != null)
            {
                return true;
            }
            return false;
        }
    }
}
