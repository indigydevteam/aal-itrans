﻿using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Interfaces.Services;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.Provinces.Backoffice.Commands
{
    public partial class CreateProvinceCommand : IRequest<Response<int>>
    { 
        public int CountryId { get; set; }
        public int RegionId { get; set; }
        public string Name_TH { get; set; }
        public string Name_ENG { get; set; }
        public bool Active { get; set; }

    }
    public class CreateProvinceCommandHandler : IRequestHandler<CreateProvinceCommand, Response<int>>
    {
        private readonly IProvinceRepositoryAsync _provinceRepository;
        private readonly ICountryRepositoryAsync _countryRepository;
        private readonly IRegionRepositoryAsync _regionRepository;
        private readonly IMapper _mapper;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        public CreateProvinceCommandHandler(IRegionRepositoryAsync regionRepository, ICountryRepositoryAsync countryRepository, IProvinceRepositoryAsync provinceRepository, IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _provinceRepository = provinceRepository;
            _regionRepository = regionRepository;
            _countryRepository = countryRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
        }


        public async Task<Response<int>> Handle(CreateProvinceCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var country = (await _countryRepository.FindByCondition(x => x.Id == request.CountryId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                var region = (await _regionRepository.FindByCondition(x => x.Id == request.RegionId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (country == null)
                {
                    throw new ApiException($"Country Not Found.");
                }
                else if (region == null)
                {
                    throw new ApiException($"Region Not Found.");
                }
                else
                {
                    var province = _mapper.Map<Province>(request);
                    province.Country = country;
                    province.Region = region;
                    province.Created = DateTime.UtcNow;
                    province.Modified = DateTime.UtcNow;
                    //country.CreatedBy = "xxxxxxx";
                    //country.ModifiedBy = "xxxxxxx";
                    var provinceObject = await _provinceRepository.AddAsync(province);
                    var log = new CreateAppLog(_applicationLogRepository);
                    log.Create("rovince", "province", "Create", Newtonsoft.Json.JsonConvert.SerializeObject(province));
                    return new Response<int>(provinceObject.Id);
                }
                
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
