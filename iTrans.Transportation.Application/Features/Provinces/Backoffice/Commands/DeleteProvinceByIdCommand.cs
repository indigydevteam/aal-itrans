﻿using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.Provinces.Backoffice.Commands
{
    public class DeleteProvinceByIdCommand : IRequest<Response<int>>
    {
        public int Id { get; set; }
        public class DeleteProvinceByIdCommandHandler : IRequestHandler<DeleteProvinceByIdCommand, Response<int>>
        {
            private readonly IProvinceRepositoryAsync _provinceRepository;
            public DeleteProvinceByIdCommandHandler(IProvinceRepositoryAsync provinceRepository)
            {
                _provinceRepository = provinceRepository;
            }
            public async Task<Response<int>> Handle(DeleteProvinceByIdCommand command, CancellationToken cancellationToken)
            {
                var province = (await _provinceRepository.FindByCondition(x => x.Id == command.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (province == null)
                {
                    throw new ApiException($"Province Not Found.");
                }
                else
                {
                    await _provinceRepository.DeleteAsync(province);
                    return new Response<int>(province.Id);
                }
            }
        }
    }
}
