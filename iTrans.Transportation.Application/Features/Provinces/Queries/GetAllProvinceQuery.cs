﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.Province;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.Provinces.Queries
{
    public class GetAllProvinceQuery : IRequest<Response<IEnumerable<ProvinceViewModel>>>
    {

    }
    public class GetAllProvinceQueryHandler : IRequestHandler<GetAllProvinceQuery, Response<IEnumerable<ProvinceViewModel>>>
    {
        private readonly IProvinceRepositoryAsync _provinceRepository;
        private readonly IMapper _mapper;
        public GetAllProvinceQueryHandler(IProvinceRepositoryAsync provinceRepository, IMapper mapper)
        {
            _provinceRepository = provinceRepository;
            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<ProvinceViewModel>>> Handle(GetAllProvinceQuery request, CancellationToken cancellationToken)
        {
            try
            {
                CultureInfo ci = CultureInfo.GetCultureInfo("th");
                bool ignoreCase = true; //whether comparison should be case-sensitive
                StringComparer comp = StringComparer.Create(ci, ignoreCase);

                var province = await _provinceRepository.FindByCondition(x => x.Active == true);
                var provinceViewModel = _mapper.Map<IEnumerable<ProvinceViewModel>>(province);
                return new Response<IEnumerable<ProvinceViewModel>>(provinceViewModel.OrderBy(x => x.name_TH, comp));
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
    }
}
