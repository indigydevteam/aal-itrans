﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using iTrans.Transportation.Application.Interfaces.Repositories;
using FluentValidation;

namespace iTrans.Transportation.Application.Features.Provinces.Queries
{
    public class GetProvinceByRegionQueryValidator : AbstractValidator<GetProvinceByRegionQuery>
    {
        private readonly IProvinceRepositoryAsync provinceRepository;

        public GetProvinceByRegionQueryValidator(IProvinceRepositoryAsync provinceRepository)
        {
            this.provinceRepository = provinceRepository;
            RuleFor(p => p.regionId)
               .NotEmpty().WithMessage("{PropertyName} is required.")
               .NotNull()
               .MustAsync(IsProvinceExists).WithMessage("{PropertyName} not exists.");
        }

        private async Task<bool> IsProvinceExists(int regionId, CancellationToken cancellationToken)
        {
            var provinceObject = (await provinceRepository.FindByCondition(x => x.Region.Id.Equals(regionId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            if (provinceObject != null)
            {
                return true;
            }
            return false;
        }
    }
}
