﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using iTrans.Transportation.Application.Interfaces.Repositories;
using FluentValidation;

namespace iTrans.Transportation.Application.Features.Provinces.Queries
{
    public class GetProvinceByCountryQueryValidator : AbstractValidator<GetProvinceByCountryQuery>
    {
        private readonly IProvinceRepositoryAsync provinceRepository;

        public GetProvinceByCountryQueryValidator(IProvinceRepositoryAsync provinceRepository)
        {
            this.provinceRepository = provinceRepository;
            RuleFor(p => p.countryId)
               .NotEmpty().WithMessage("{PropertyName} is required.")
               .NotNull()
               .MustAsync(IsProvinceExists).WithMessage("{PropertyName} not exists.");
        }

        private async Task<bool> IsProvinceExists(int countryId, CancellationToken cancellationToken)
        {
            var provinceObject = (await provinceRepository.FindByCondition(x => x.Country.Id.Equals(countryId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            if (provinceObject != null)
            {
                return true;
            }
            return false;
        }
    }
}
