﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.Province;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.Provinces.Queries
{
    public class GetProvinceByIdQuery : IRequest<Response<ProvinceViewModel>>
    {
        public int Id { get; set; }
    }
    public class GetProvinceByIdQueryHandler : IRequestHandler<GetProvinceByIdQuery, Response<ProvinceViewModel>>
    {
        private readonly IProvinceRepositoryAsync _provinceRepository;
        private readonly IMapper _mapper;
        public GetProvinceByIdQueryHandler(IProvinceRepositoryAsync provinceRepository, IMapper mapper)
        {
            _provinceRepository = provinceRepository;
            _mapper = mapper;
        }
        public async Task<Response<ProvinceViewModel>> Handle(GetProvinceByIdQuery request, CancellationToken cancellationToken)
        {
            var provinceObject = (await _provinceRepository.FindByCondition(x => x.Id == request.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            return new Response<ProvinceViewModel>(_mapper.Map<ProvinceViewModel>(provinceObject));
        }
    }
}
