﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.CustomerPayment;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.CustomerPaymentes.Queries
{
    public class GetCustomerPaymentByCustomerQuery : IRequest<Response<IEnumerable<CustomerPaymentViewModel>>>
    {
        public Guid CustomerId { get; set; }
    }
    public class GetCustomerPaymentByCustomerQueryHandler : IRequestHandler<GetCustomerPaymentByCustomerQuery, Response<IEnumerable<CustomerPaymentViewModel>>>
    {
        private readonly ICustomerPaymentRepositoryAsync _customerPaymentRepository;
        private readonly IMapper _mapper;
        public GetCustomerPaymentByCustomerQueryHandler(ICustomerPaymentRepositoryAsync customerPaymentRepository, IMapper mapper)
        {
            _customerPaymentRepository = customerPaymentRepository;
            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<CustomerPaymentViewModel>>> Handle(GetCustomerPaymentByCustomerQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var customerPayment = (await _customerPaymentRepository.FindByCondition(x => x.Customer.Id.Equals(request.CustomerId)).ConfigureAwait(false)).AsQueryable().ToList();
                var customerPaymentViewModel = _mapper.Map<IEnumerable<CustomerPaymentViewModel>>(customerPayment);
                return new Response<IEnumerable<CustomerPaymentViewModel>>(customerPaymentViewModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
