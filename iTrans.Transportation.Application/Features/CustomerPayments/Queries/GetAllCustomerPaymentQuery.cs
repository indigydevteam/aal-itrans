﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.CustomerPayment;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.CustomerPayments.Queries
{
    public class GetAllCustomerPaymentQuery : IRequest<Response<IEnumerable<CustomerPaymentViewModel>>>
    {

    }
    public class GetAllCustomerPaymentQueryHandler : IRequestHandler<GetAllCustomerPaymentQuery, Response<IEnumerable<CustomerPaymentViewModel>>>
    {
        private readonly ICustomerPaymentRepositoryAsync _customerPaymentRepository;
        private readonly IMapper _mapper;
        public GetAllCustomerPaymentQueryHandler(ICustomerPaymentRepositoryAsync customerPaymentRepository, IMapper mapper)
        {
            _customerPaymentRepository = customerPaymentRepository;
            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<CustomerPaymentViewModel>>> Handle(GetAllCustomerPaymentQuery request, CancellationToken cancellationToken)
        {
            var customer = await _customerPaymentRepository.GetAllAsync();
            var customerPaymentViewModel = _mapper.Map<IEnumerable<CustomerPaymentViewModel>>(customer);
            return new Response<IEnumerable<CustomerPaymentViewModel>>(customerPaymentViewModel);
        }
    }
}
