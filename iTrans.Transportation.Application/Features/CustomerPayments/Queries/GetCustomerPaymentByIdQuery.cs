﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.CustomerPayment;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.CustomerPaymentes.Queries
{
    public class GetCustomerPaymentByIdQuery : IRequest<Response<CustomerPaymentViewModel>>
    {
        public Guid CustomerId { get; set; }
        public int Id { get; set; }
     
    }
    public class GetCustomerPaymentByIdQueryHandler : IRequestHandler<GetCustomerPaymentByIdQuery, Response<CustomerPaymentViewModel>>
    {
        private readonly ICustomerPaymentRepositoryAsync _customerPaymentRepository;
        private readonly IMapper _mapper;
        public GetCustomerPaymentByIdQueryHandler(ICustomerPaymentRepositoryAsync customerPaymentRepository, IMapper mapper)
        {
            _customerPaymentRepository = customerPaymentRepository;
            _mapper = mapper;
        }
        public async Task<Response<CustomerPaymentViewModel>> Handle(GetCustomerPaymentByIdQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var customerObject = (await _customerPaymentRepository.FindByCondition(x => x.Id.Equals(request.Id) && x.Customer.Id.Equals(request.CustomerId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                return new Response<CustomerPaymentViewModel>(_mapper.Map<CustomerPaymentViewModel>(customerObject));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
