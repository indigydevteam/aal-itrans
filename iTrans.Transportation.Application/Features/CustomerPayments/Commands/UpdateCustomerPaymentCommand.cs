﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;

namespace iTrans.Transportation.Application.Features.CustomerPayments.Commands
{
    public class UpdateCustomerPaymentCommand : IRequest<Response<int>>
    {
        public int Id { get; set; }
        public  Guid CustomerId { get; set; }
        public int TypeId { get; set; }
        public string Bank { get; set; }
        public string AccountType { get; set; }
        public string Value { get; set; }
        public decimal Amount { get; set; }
        public  string Description { get; set; }
    }
    public class UpdateCustomerPaymentCommandHandler : IRequestHandler<UpdateCustomerPaymentCommand, Response<int>>
    {
        private readonly ICustomerPaymentRepositoryAsync _customerPaymentRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IPaymentTypeRepositoryAsync _paymentTypeRepository;
        private readonly IMapper _mapper;

        public UpdateCustomerPaymentCommandHandler(ICustomerPaymentRepositoryAsync customerPaymentRepository,
            IPaymentTypeRepositoryAsync paymentTypeRepository, IApplicationLogRepositoryAsync applicationLogRepository,IMapper mapper)
        {
            _customerPaymentRepository = customerPaymentRepository;
            _applicationLogRepository = applicationLogRepository;
            _paymentTypeRepository = paymentTypeRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(UpdateCustomerPaymentCommand request, CancellationToken cancellationToken)
        {
            var customerPayment = (await _customerPaymentRepository.FindByCondition(x => x.Id == request.Id && x.Customer.Id.Equals(request.CustomerId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            if (customerPayment == null)
            {
                throw new ApiException($"Payment Not Found.");
            }
            else
            {
                var paymentType = (await _paymentTypeRepository.FindByCondition(x => x.Id.Equals(request.TypeId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                customerPayment.Type = paymentType;
                customerPayment.Description = request.Description;
                customerPayment.Bank = request.Bank;
                customerPayment.AccountType = request.AccountType;
                customerPayment.Value = request.Value;
                customerPayment.Amount = request.Amount;
                customerPayment.Modified = DateTime.UtcNow;
                await _customerPaymentRepository.UpdateAsync(customerPayment);
                var log = new CreateAppLog(_applicationLogRepository);
                log.Create("Customer", "Customer Payment", "Edit", Newtonsoft.Json.JsonConvert.SerializeObject(request));
                return new Response<int>(customerPayment.Id);
            }
        }
    }
}
