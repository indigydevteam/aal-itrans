﻿using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
namespace iTrans.Transportation.Application.Features.CustomerPayments.Commands
{
    public class DeleteCustomerPaymentCommand : IRequest<Response<int>>
    {
        public Guid CustomerId { get; set; }
        public int Id { get; set; }
        public class DeleteCustomerPaymentCommandHandler : IRequestHandler<DeleteCustomerPaymentCommand, Response<int>>
        {
            private readonly ICustomerPaymentRepositoryAsync _customerPaymentRepository;
            public DeleteCustomerPaymentCommandHandler(ICustomerPaymentRepositoryAsync customerPaymentRepository)
            {
                _customerPaymentRepository = customerPaymentRepository;
            }
            public async Task<Response<int>> Handle(DeleteCustomerPaymentCommand command, CancellationToken cancellationToken)
            {
                var customerPayment = (await _customerPaymentRepository.FindByCondition(x => x.Id == command.Id && x.Customer.Id.Equals(command.CustomerId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (customerPayment == null)
                {
                    throw new ApiException($"Payment Not Found.");
                }
                else
                {
                    await _customerPaymentRepository.DeleteAsync(customerPayment);
                    return new Response<int>(customerPayment.Id);
                }
            }
        }
    }
}
