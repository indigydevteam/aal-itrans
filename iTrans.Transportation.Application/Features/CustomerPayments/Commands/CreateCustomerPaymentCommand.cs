﻿using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Interfaces.Services;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using iTrans.Transportation.Domain.Entities;
using MediatR;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.CustomerPayments.Commands
{
    public partial class CreateCustomerPaymentCommand : IRequest<Response<int>>
    {

        public Guid CustomerId { get; set; }
        public int TypeId { get; set; }
        public string Bank { get; set; }
        public string AccountType { get; set; }
        public string Value { get; set; }
        public decimal Amount { get; set; }
        public string Description { get; set; }

    }
    public class CreateCustomerPaymentCommandHandler : IRequestHandler<CreateCustomerPaymentCommand, Response<int>>
    {
        private readonly ICustomerRepositoryAsync _customerRepository;
        private readonly ICustomerPaymentRepositoryAsync _customerPaymentRepository;
        private readonly IPaymentTypeRepositoryAsync _paymentTypeRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;
        public CreateCustomerPaymentCommandHandler(ICustomerRepositoryAsync customerRepository,ICustomerPaymentRepositoryAsync customerPaymentRepository,
            IPaymentTypeRepositoryAsync paymentTypeRepository, IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _customerRepository = customerRepository;
            _customerPaymentRepository = customerPaymentRepository;
            _applicationLogRepository = applicationLogRepository;
            _paymentTypeRepository = paymentTypeRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(CreateCustomerPaymentCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var customer = (await _customerRepository.FindByCondition(x => x.Id.Equals(request.CustomerId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if(customer == null)
                {
                    throw new ApiException($"Customer Not Found.");
                }
                var paymentType = (await _paymentTypeRepository.FindByCondition(x => x.Id.Equals(request.TypeId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                var customerPayment = _mapper.Map<CustomerPayment>(request);
                customerPayment.Type = paymentType;
                customerPayment.Customer = customer;
                customerPayment.Created = DateTime.UtcNow;
                customerPayment.Modified = DateTime.UtcNow;
                var customerPaymentObject = await _customerPaymentRepository.AddAsync(customerPayment);
                var log = new CreateAppLog(_applicationLogRepository);
                log.Create("Customer", "Customer Payment", "Create", Newtonsoft.Json.JsonConvert.SerializeObject(request));
                return new Response<int>(customerPaymentObject.Id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
