﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.ContainerSpecification;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;

namespace iTrans.Transportation.Application.Features.ContainerSpecifications.Queries
{
    public class GetAllContainerSpecificationParameter : IRequest<PagedResponse<IEnumerable<ContainerSpecificationViewModel>>>
    {
        public string Search { set; get; }

        public int PageNumber { get; set; }
        public int PageSize { get; set; }
    }
    public class GetAllContainerSpecificationParameterHandler : IRequestHandler<GetAllContainerSpecificationParameter, PagedResponse<IEnumerable<ContainerSpecificationViewModel>>>
    {
        private readonly IContainerSpecificationRepositoryAsync _containerSpecificationRepository;
        private readonly IMapper _mapper;
        private Expression<Func<ContainerSpecification, bool>> expression;

        public GetAllContainerSpecificationParameterHandler(IContainerSpecificationRepositoryAsync containerSpecificationRepository, IMapper mapper)
        {
            _containerSpecificationRepository = containerSpecificationRepository;
            _mapper = mapper;
        }

        public async Task<PagedResponse<IEnumerable<ContainerSpecificationViewModel>>> Handle(GetAllContainerSpecificationParameter request, CancellationToken cancellationToken)
        {
            var validFilter = _mapper.Map<GetAllContainerSpecificationParameter>(request);
            if (validFilter.Search != null && validFilter.Search.Trim() != "")
            {
                expression = x => (x.Name_ENG.Contains(validFilter.Search.Trim()) || x.Name_TH.Contains(validFilter.Search.Trim()) || x.Sequence.ToString().Contains(validFilter.Search.Trim()));
            }
            int itemCount = _containerSpecificationRepository.GetItemCount(expression);
            //Expression<Func<ContainerSpecification, bool>> expression = x => x.Name_TH != "";
            var ContainerSpecification = await _containerSpecificationRepository.FindByConditionWithPage(expression, validFilter.PageNumber, validFilter.PageSize);
            var ContainerSpecificationViewModel = _mapper.Map<IEnumerable<ContainerSpecificationViewModel>>(ContainerSpecification);
            return new PagedResponse<IEnumerable<ContainerSpecificationViewModel>>(ContainerSpecificationViewModel, validFilter.PageNumber, validFilter.PageSize, itemCount);
        }
    }
}
