﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.ContainerSpecification;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.ContainerSpecifications.Queries
{
    public class GetContainerSpecificationByIdQuery : IRequest<Response<ContainerSpecificationViewModel>>
    {
        public int Id { get; set; }
    }
    public class GetContainerSpecificationByIdQueryHandler : IRequestHandler<GetContainerSpecificationByIdQuery, Response<ContainerSpecificationViewModel>>
    {
        private readonly IContainerSpecificationRepositoryAsync _containerSpecificationRepository;
        private readonly IMapper _mapper;
        public GetContainerSpecificationByIdQueryHandler(IContainerSpecificationRepositoryAsync containerSpecificationRepository, IMapper mapper)
        {
            _containerSpecificationRepository = containerSpecificationRepository;
            _mapper = mapper;
        }
        public async Task<Response<ContainerSpecificationViewModel>> Handle(GetContainerSpecificationByIdQuery request, CancellationToken cancellationToken)
        {
            var ContainerSpecificationObject = (await _containerSpecificationRepository.FindByCondition(x => x.Id.Equals(request.Id)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            return new Response<ContainerSpecificationViewModel>(_mapper.Map<ContainerSpecificationViewModel>(ContainerSpecificationObject));
        }
    }
}
