﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.ContainerSpecification;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.ContainerSpecifications.Queries
{
    public class GetAllContainerSpecificationQuery : IRequest<Response<IEnumerable<ContainerSpecificationViewModel>>>
    {

    }
    public class GetAllContainerSpecificationQueryHandler : IRequestHandler<GetAllContainerSpecificationQuery, Response<IEnumerable<ContainerSpecificationViewModel>>>
    {
        private readonly IContainerSpecificationRepositoryAsync _containerSpecificationRepository;
        private readonly IMapper _mapper;
        public GetAllContainerSpecificationQueryHandler(IContainerSpecificationRepositoryAsync containerSpecificationRepository, IMapper mapper)
        {
            _containerSpecificationRepository = containerSpecificationRepository;
            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<ContainerSpecificationViewModel>>> Handle(GetAllContainerSpecificationQuery request, CancellationToken cancellationToken)
        {
            var ContainerSpecification = (await _containerSpecificationRepository.FindByCondition(x => x.Active == true).ConfigureAwait(false)).AsQueryable().ToList();
            var ContainerSpecificationViewModel = _mapper.Map<IEnumerable<ContainerSpecificationViewModel>>(ContainerSpecification);
            return new Response<IEnumerable<ContainerSpecificationViewModel>>(ContainerSpecificationViewModel);
        }
    }
}
