﻿using AutoMapper;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Domain;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;

namespace iTrans.Transportation.Application.Features.ContainerSpecifications.Commands
{
    public partial class CreateContainerSpecificationCommand : IRequest<Response<int>>
    {
        public Guid? UserId { get; set; }
        public string Name_TH { get; set; }
        public string Name_ENG { get; set; }
        public int Sequence { get; set; }
        public bool Active { get; set; }
        public bool Specified { get; set; }

    }
    public class CreateContainerSpecificationCommandHandler : IRequestHandler<CreateContainerSpecificationCommand, Response<int>>
    {
        private readonly IContainerSpecificationRepositoryAsync _containerSpecificationRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;
        public CreateContainerSpecificationCommandHandler(IContainerSpecificationRepositoryAsync containerSpecificationRepository, IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _containerSpecificationRepository = containerSpecificationRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(CreateContainerSpecificationCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var ContainerSpecification = _mapper.Map<ContainerSpecification>(request);
                ContainerSpecification.Created = DateTime.UtcNow;
                ContainerSpecification.Modified = DateTime.UtcNow;
                //title.CreatedBy = "xxxxxxx";
                //title.ModifiedBy = "xxxxxxx";
                var ContainerSpecificationObject = await _containerSpecificationRepository.AddAsync(ContainerSpecification);
                var log = new CreateAppLog(_applicationLogRepository);
                log.CreateLog("ContainerSpecification", "ContainerSpecification", "Create", Newtonsoft.Json.JsonConvert.SerializeObject(ContainerSpecificationObject),request.UserId.ToString());
                return new Response<int>(ContainerSpecificationObject.Id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
