﻿using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.ContainerSpecifications.Commands
{
    public class DeleteContainerSpecificationIdCommand : IRequest<Response<int>>
    {
        public int Id { get; set; }
        public class DeleteContainerSpecificationIdCommandHandler : IRequestHandler<DeleteContainerSpecificationIdCommand, Response<int>>
        {
            private readonly IContainerSpecificationRepositoryAsync _containerSpecificationRepository;
            public DeleteContainerSpecificationIdCommandHandler(IContainerSpecificationRepositoryAsync ContainerSpecificationRepository)
            {
                _containerSpecificationRepository = ContainerSpecificationRepository;
            }
            public async Task<Response<int>> Handle(DeleteContainerSpecificationIdCommand command, CancellationToken cancellationToken)
            {
                var ContainerSpecification = (await _containerSpecificationRepository.FindByCondition(x => x.Id == command.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (ContainerSpecification == null)
                {
                    throw new ApiException($"ContainerSpecification Not Found.");
                }
                else
                {
                    await _containerSpecificationRepository.DeleteAsync(ContainerSpecification);
                    return new Response<int>(ContainerSpecification.Id);
                }
            }
        }
    }
}
