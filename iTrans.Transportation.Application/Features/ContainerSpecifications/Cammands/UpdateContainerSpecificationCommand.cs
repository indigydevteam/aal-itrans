﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;

namespace iTrans.Transportation.Application.Features.ContainerSpecifications.Commands
{
    public class UpdateContainerSpecificationCommand : IRequest<Response<int>>
    {
        public Guid? UserId { get; set; }
        public int Id { get; set; }
        public string Name_TH { get; set; }
        public string Name_ENG { get; set; }
        public int Sequence { get; set; }
        public bool Active { get; set; }
        public bool Specified { get; set; }

    }

    public class UpdateContainerSpecificationCommandHandler : IRequestHandler<UpdateContainerSpecificationCommand, Response<int>>
    {
        private readonly IContainerSpecificationRepositoryAsync _containerSpecificationRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;

        public UpdateContainerSpecificationCommandHandler(IContainerSpecificationRepositoryAsync containerSpecificationRepository, IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _containerSpecificationRepository = containerSpecificationRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(UpdateContainerSpecificationCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var ContainerSpecification = (await _containerSpecificationRepository.FindByCondition(x => x.Id == request.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (ContainerSpecification == null)
                {
                    throw new ApiException($"ContainerSpecification Not Found.");
                }
                else
                {
                    ContainerSpecification.Name_TH = request.Name_TH;
                    ContainerSpecification.Name_ENG = request.Name_ENG;
                    ContainerSpecification.Sequence = request.Sequence;
                    ContainerSpecification.Active = request.Active;
                    ContainerSpecification.Specified = request.Specified;
                    ContainerSpecification.Modified = DateTime.UtcNow;

                    await _containerSpecificationRepository.UpdateAsync(ContainerSpecification);
                    var log = new CreateAppLog(_applicationLogRepository);
                    log.CreateLog("ContainerSpecification", "ContainerSpecification", "Edit", Newtonsoft.Json.JsonConvert.SerializeObject(ContainerSpecification),request.UserId.ToString());
                    return new Response<int>(ContainerSpecification.Id);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
