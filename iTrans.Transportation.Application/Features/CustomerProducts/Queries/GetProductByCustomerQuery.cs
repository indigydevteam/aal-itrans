﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.CustomerProduct;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using LinqKit;
using MediatR;
namespace iTrans.Transportation.Application.Features.CustomerProducts.Queries
{
    public class GetProductByCustomerQuery : IRequest<Response<IEnumerable<CustomerProductViewModel>>>
    {
        public Guid CustomerId { set; get; }
        public string Search { set; get; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
    }
    public class GetProductByCustomerQueryHandler : IRequestHandler<GetProductByCustomerQuery, Response<IEnumerable<CustomerProductViewModel>>>
    {
        private readonly ICustomerProductRepositoryAsync _customerProductRepository;
        private readonly IMapper _mapper;
        public GetProductByCustomerQueryHandler(ICustomerProductRepositoryAsync customerProductRepository, IMapper mapper)
        {
            _customerProductRepository = customerProductRepository;
            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<CustomerProductViewModel>>> Handle(GetProductByCustomerQuery request, CancellationToken cancellationToken)
        {
            var validFilter = _mapper.Map<GetProductByCustomerQuery>(request);
            Expression<Func<CustomerProduct, bool>> expression = PredicateBuilder.New<CustomerProduct>(false);
            expression = x => x.CustomerId == request.CustomerId;
            if (validFilter.Search != null && validFilter.Search.Trim() != "")
            {
                string search = validFilter.Search.Trim().ToLower().Replace(" ","");
                expression = expression.And(x => x.Name.Trim().ToLower().Replace(" ", "").Contains(search) || x.ProductType.Name_TH.Trim().ToLower().Replace(" ", "").Contains(search) || x.ProductType.Name_ENG.Trim().ToLower().Replace(" ", "").Contains(search)
                                              || x.Packaging.Name_TH.Trim().ToLower().Replace(" ", "").Contains(search) || x.Packaging.Name_ENG.Trim().ToLower().Replace(" ", "").Contains(search)
                                              || x.ProductTypeDetail.Trim().ToLower().Replace(" ", "").Contains(search) || x.PackagingDetail.Trim().ToLower().Replace(" ", "").Contains(search));
            }
            var customerProductObject = await _customerProductRepository.FindByConditionWithPage(expression, validFilter.PageNumber, validFilter.PageSize);
 
            var customerProductViewModel = _mapper.Map<IEnumerable<CustomerProductViewModel>>(customerProductObject);
            return new Response<IEnumerable<CustomerProductViewModel>>(customerProductViewModel);
        }
    }
}
