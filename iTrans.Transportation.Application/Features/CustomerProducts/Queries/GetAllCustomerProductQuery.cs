﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.CustomerProduct;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
namespace iTrans.Transportation.Application.Features.CustomerProducts.Queries
{
    public class GetAllCustomerProductQuery : IRequest<Response<IEnumerable<CustomerProductViewModel>>>
    {
    }
    public class GetAllCustomerProductQueryHandler : IRequestHandler<GetAllCustomerProductQuery, Response<IEnumerable<CustomerProductViewModel>>>
    {
        private readonly ICustomerProductRepositoryAsync _customerProductRepository;
        private readonly IMapper _mapper;
        public GetAllCustomerProductQueryHandler(ICustomerProductRepositoryAsync customerProductRepository, IMapper mapper)
        {
            _customerProductRepository = customerProductRepository;
            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<CustomerProductViewModel>>> Handle(GetAllCustomerProductQuery request, CancellationToken cancellationToken)
        {
            var customer = await _customerProductRepository.GetAllAsync();
            var customerProductViewModel = _mapper.Map<IEnumerable<CustomerProductViewModel>>(customer);
            return new Response<IEnumerable<CustomerProductViewModel>>(customerProductViewModel);
        }
    }
}
