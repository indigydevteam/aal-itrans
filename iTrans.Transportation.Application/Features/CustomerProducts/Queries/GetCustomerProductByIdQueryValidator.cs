﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using iTrans.Transportation.Application.Interfaces.Repositories;
using FluentValidation;
namespace iTrans.Transportation.Application.Features.CustomerProducts.Queries
{
    public class GetCustomerProductByIdQueryValidator : AbstractValidator<GetCustomerProductByIdQuery>
    {
        private readonly ICustomerProductRepositoryAsync customerProductRepository;

        public GetCustomerProductByIdQueryValidator(ICustomerProductRepositoryAsync customerProductRepository)
        {
            this.customerProductRepository = customerProductRepository;
            RuleFor(p => p.Id)
               .NotEmpty().WithMessage("{PropertyName} is required.")
               .NotNull()
               .MustAsync(IsCustomerProductExists).WithMessage("{PropertyName} not exists.");
        }

        private async Task<bool> IsCustomerProductExists(int CustomerId, CancellationToken cancellationToken)
        {
            var userObject = (await customerProductRepository.FindByCondition(x => x.Id.Equals(CustomerId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            if (userObject != null)
            {
                return true;
            }
            return false;
        }
    }
}
