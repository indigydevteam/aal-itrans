﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.CustomerProduct;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;


namespace iTrans.Transportation.Application.Features.CustomerProducts.Queries
{
    public class GetCustomerProductByIdQuery : IRequest<Response<CustomerProductViewModel>>
    {
        public int Id { get; set; }
    }
    public class GetCustomerProductByIdQueryHandler : IRequestHandler<GetCustomerProductByIdQuery, Response<CustomerProductViewModel>>
    {
        private readonly ICustomerProductRepositoryAsync _customerProductRepository;
        private readonly IMapper _mapper;
        public GetCustomerProductByIdQueryHandler(ICustomerProductRepositoryAsync customerProductRepository, IMapper mapper)
        {
            _customerProductRepository = customerProductRepository;
            _mapper = mapper;
        }
        public async Task<Response<CustomerProductViewModel>> Handle(GetCustomerProductByIdQuery request, CancellationToken cancellationToken)
        {
            var customerObject = (await _customerProductRepository.FindByCondition(x => x.Id.Equals(request.Id)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            return new Response<CustomerProductViewModel>(_mapper.Map<CustomerProductViewModel>(customerObject));
        }
    }
}
