﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.CustomerProduct;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using LinqKit;
using MediatR;
namespace iTrans.Transportation.Application.Features.CustomerProducts.Queries
{
    public class GetCustomerProductByCustomerQuery : IRequest<Response<IEnumerable<CustomerProductViewModel>>>
    {
        public Guid CustomerId { get; set; }
        public string Search { set; get; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
    }
    public class GetCustomerProductByCustomerQueryHandler : IRequestHandler<GetCustomerProductByCustomerQuery, Response<IEnumerable<CustomerProductViewModel>>>
    {
        private readonly ICustomerProductRepositoryAsync _customerProductRepository;
        private readonly IMapper _mapper;
        public GetCustomerProductByCustomerQueryHandler(ICustomerProductRepositoryAsync customerProductRepository, IMapper mapper)
        {
            _customerProductRepository = customerProductRepository;
            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<CustomerProductViewModel>>> Handle(GetCustomerProductByCustomerQuery request, CancellationToken cancellationToken)
        {
            var validFilter = _mapper.Map<GetCustomerProductByCustomerQuery>(request);
            Expression<Func<CustomerProduct, bool>> expression = PredicateBuilder.New<CustomerProduct>(false);
            expression = x => x.CustomerId == request.CustomerId;
            if (validFilter.Search != null && validFilter.Search.Trim() != "")
            {
                expression = expression.And(x => x.Name.Trim().ToLower().Contains(validFilter.Search) || x.ProductType.Name_TH.Trim().ToLower().Contains(validFilter.Search) || x.ProductType.Name_ENG.Trim().ToLower().Contains(validFilter.Search)
                                              || x.Packaging.Name_TH.Trim().ToLower().Contains(validFilter.Search) || x.Packaging.Name_ENG.Trim().ToLower().Contains(validFilter.Search)
                                              || x.ProductTypeDetail.Trim().ToLower().Contains(validFilter.Search) || x.PackagingDetail.Trim().ToLower().Contains(validFilter.Search));
            }
            var customerProductObject = await _customerProductRepository.FindByConditionWithPage(expression, validFilter.PageNumber, validFilter.PageSize);
            var customerProductViewModel = _mapper.Map<IEnumerable<CustomerProductViewModel>>(customerProductObject);
            return new Response<IEnumerable<CustomerProductViewModel>>(customerProductViewModel);
        }
    }
}
