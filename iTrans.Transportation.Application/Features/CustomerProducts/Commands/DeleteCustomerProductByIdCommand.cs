﻿using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.CustomerProducts.Commands
{
    public class DeleteCustomerProductByIdCommand : IRequest<Response<int>>
    {
        public int Id { get; set; }
        public Guid CustomerId { get; set; }
        public class DeleteCustomerProductByIdCommandHandler : IRequestHandler<DeleteCustomerProductByIdCommand, Response<int>>
        {
            private readonly ICustomerProductRepositoryAsync _customerProductRepository;
            public DeleteCustomerProductByIdCommandHandler(ICustomerProductRepositoryAsync customerProductRepository)
            {
                _customerProductRepository = customerProductRepository;
            }
            public async Task<Response<int>> Handle(DeleteCustomerProductByIdCommand command, CancellationToken cancellationToken)
            {
                var customerProduct = (await _customerProductRepository.FindByCondition(x => x.Id == command.Id && x.CustomerId.Equals(command.CustomerId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (customerProduct == null)
                {
                    throw new ApiException($"CustomerProduct Not Found.");
                }
                else
                {
                    await _customerProductRepository.DeleteAsync(customerProduct);
                    return new Response<int>(customerProduct.Id);
                }
            }
        }
    }
}
