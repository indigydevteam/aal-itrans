﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.CustomerProducts.Commands
{
    public class UpdateCustomerProductCommandValidator : AbstractValidator<UpdateCustomerProductCommand>
    {
        private readonly ICustomerProductRepositoryAsync customerProductRepository;

        public UpdateCustomerProductCommandValidator(ICustomerProductRepositoryAsync customerProductRepository)
        {
            this.customerProductRepository = customerProductRepository;

            RuleFor(p => p.CustomerId)
                .NotNull().WithMessage("{PropertyName} is required.");

            RuleFor(p => p.Name)
                .NotEmpty().WithMessage("{PropertyName} is required.")
                .NotNull()
                .MaximumLength(250).WithMessage("{PropertyName} must not exceed 250 characters.");

            RuleFor(p => p.ProductTypeId)
               .NotNull().WithMessage("{PropertyName} is required.");

            RuleFor(p => p.PackagingId)
               .NotNull().WithMessage("{PropertyName} is required.");
        }

        private async Task<bool> IsUnique(string value, CancellationToken cancellationToken)
        {
            var customerProductObject = (await customerProductRepository.FindByCondition(x => x.Name.ToLower() == value.ToLower()).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            if (customerProductObject != null)
            {
                return false;
            }
            return true;
        }
    }
}
