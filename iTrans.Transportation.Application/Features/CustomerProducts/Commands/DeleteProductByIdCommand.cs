﻿using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;

namespace iTrans.Transportation.Application.Features.CustomerProducts.Commands
{
    public class DeleteProductByIdCommand : IRequest<Response<int>>
    {
        public int Id { get; set; }
        public Guid CustomerId { get; set; }
        public class DeleteProductByIdCommandHandler : IRequestHandler<DeleteProductByIdCommand, Response<int>>
        {
            private readonly ICustomerProductRepositoryAsync _customerProductRepository;
            private readonly ICustomerProductFileRepositoryAsync _customerProductFileRepository;
            private readonly IConfiguration _configuration;
            public DeleteProductByIdCommandHandler(ICustomerProductRepositoryAsync customerProductRepository, ICustomerProductFileRepositoryAsync customerProductFileRepository, IConfiguration configuration)
            {
                _customerProductRepository = customerProductRepository;
                _customerProductFileRepository = customerProductFileRepository;
                _configuration = configuration;
            }
            public async Task<Response<int>> Handle(DeleteProductByIdCommand command, CancellationToken cancellationToken)
            {
                var getContentPath = _configuration.GetSection("ContentPath");
                string contentPath = getContentPath.Value;

                var customerProduct = (await _customerProductRepository.FindByCondition(x => x.Id == command.Id && x.CustomerId.Equals(command.CustomerId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (customerProduct == null)
                {
                    throw new ApiException($"CustomerProduct Not Found.");
                }
                else
                {
                    await _customerProductRepository.DeleteAsync(customerProduct);
                    string folderPath = contentPath + "customerproduct/" + customerProduct.Id.ToString();
                    if (!Directory.Exists(folderPath))
                    {
                        Directory.CreateDirectory(folderPath);
                    }
                    var customerFiles = (await _customerProductFileRepository.FindByCondition(x => x.CustomerProductId.Equals(customerProduct.Id)).ConfigureAwait(false)).AsQueryable().ToList();
                    foreach (CustomerProductFile customerFile in customerFiles)
                    {
                        if (File.Exists(Path.Combine(contentPath, customerFile.FilePath)))
                        {
                            File.Delete(Path.Combine(contentPath, customerFile.FilePath));
                        }
                        await _customerProductFileRepository.DeleteAsync(customerFile);

                    }
                    (await _customerProductFileRepository.CreateSQLQuery("DELETE Customer_ProductFile where CustomerProductId = '" + customerProduct.Id.ToString() + "'").ConfigureAwait(false)).ExecuteUpdate();
                    return new Response<int>(customerProduct.Id);
                }
            }
        }
    }
}
