﻿using AutoMapper;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Interfaces.Services;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;

namespace iTrans.Transportation.Application.Features.CustomerProducts.Commands
{
    public partial class CreateProductCommand : IRequest<Response<int>>
    {
        public Guid? UserId { get; set; }
        public Guid CustomerId { get; set; }
        public string Name { get; set; }
        public int ProductTypeId { get; set; }
        public string ProductTypeDetail { get; set; }
        public int PackagingId { get; set; }
        public string PackagingDetail { get; set; }
        public int? Width { get; set; }
        public int? Length { get; set; }
        public int? Height { get; set; }
        public decimal Weight { get; set; }
        public int Quantity { get; set; }
        public List<IFormFile> files { get; set; }

    }
    public class CreateProductCommandHandler : IRequestHandler<CreateProductCommand, Response<int>>
    {
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly ICustomerProductRepositoryAsync _customerProductRepository;
        private readonly ICustomerProductFileRepositoryAsync _customerProductFileRepository;
        private readonly IProductTypeRepositoryAsync _productTypeRepository;
        private readonly IProductPackagingRepositoryAsync _productPackagingRepository;
        private readonly IMapper _mapper;
        private readonly IConfiguration _configuration;
        public CreateProductCommandHandler(ICustomerProductRepositoryAsync customerProductRepository, ICustomerProductFileRepositoryAsync customerProductFileRepository
            , IProductTypeRepositoryAsync productTypeRepository, IProductPackagingRepositoryAsync productPackagingRepository, IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper
            , IConfiguration configuration)
        {
            _applicationLogRepository = applicationLogRepository;
            _customerProductRepository = customerProductRepository;
            _customerProductFileRepository = customerProductFileRepository;
            _productTypeRepository = productTypeRepository;
            _productPackagingRepository = productPackagingRepository;
            _mapper = mapper;
            _configuration = configuration;
        }

        public async Task<Response<int>> Handle(CreateProductCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var getContentPath = _configuration.GetSection("ContentPath");
                string contentPath = getContentPath.Value;

                var customerProduct = _mapper.Map<CustomerProduct>(request);
                customerProduct.ProductType = (await _productTypeRepository.FindByCondition(x => x.Id.Equals(request.ProductTypeId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                customerProduct.Packaging = (await _productPackagingRepository.FindByCondition(x => x.Id.Equals(request.PackagingId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                customerProduct.Created = DateTime.UtcNow;
                customerProduct.CreatedBy = request.UserId != null ? request.UserId.Value.ToString():"";
                customerProduct.Modified = DateTime.UtcNow;
                customerProduct.ModifiedBy = request.UserId != null ? request.UserId.Value.ToString() : "";
                var customerProductObject = await _customerProductRepository.AddAsync(customerProduct);
               
                if (customerProductObject != null && customerProductObject.Id != 0)
                {
                    string folderPath = contentPath + "customerproduct/" + customerProductObject.Id.ToString();
                    if (!Directory.Exists(folderPath))
                    {
                        Directory.CreateDirectory(folderPath);
                    }
                    int fileCount = 0;
                    if (request.files != null)
                    {
                        foreach (IFormFile file in request.files)
                        {
                            fileCount = fileCount + 1;
                            string fileName = file.FileName;
                            string contentType = file.ContentType;
                            if (fileName.Trim() == "" || fileName.ToLower().Trim() == "blob")
                            {
                                fileName = DateTime.Now.ToString("ddMMyyyyHHmmss") + fileCount.ToString("D" + 4) + ".jpg";
                                contentType = "image/jpeg";
                            }
                            string filePath = Path.Combine(folderPath, fileName);
                            using (FileStream fileStream = new FileStream(filePath, FileMode.Create))
                            {
                                await file.CopyToAsync(fileStream);
                                FileInfo fi = new FileInfo(filePath);

                                CustomerProductFile customerProductFile = new CustomerProductFile
                                {
                                    CustomerProductId = customerProductObject.Id,
                                    FileName = fileName,
                                    ContentType = contentType,
                                    FilePath = "customerproduct/" + customerProductObject.Id.ToString() + "/" + fileName,
                                    FileEXT = fi.Extension,
                                };
                                var customerFileObject = await _customerProductFileRepository.AddAsync(customerProductFile);
                            }
                        }
                    }
                }
                var log = new CreateAppLog(_applicationLogRepository);
                log.Create("Customer", "Customer Product", "Create", Newtonsoft.Json.JsonConvert.SerializeObject(request));
                return new Response<int>(customerProductObject.Id);
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
    }
}
