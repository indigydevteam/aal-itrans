﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;

namespace iTrans.Transportation.Application.Features.CustomerProducts.Commands
{
    public class UpdateProductCommand : IRequest<Response<int>>
    {
        public Guid? UserId { get; set; }
        public int Id { get; set; }
        public Guid CustomerId { get; set; }
        public string Name { get; set; }
        public int ProductTypeId { get; set; }
        public string ProductTypeDetail { get; set; }
        public int PackagingId { get; set; }
        public string PackagingDetail { get; set; }
        public int Width { get; set; }
        public int Length { get; set; }
        public int Height { get; set; }
        public decimal Weight { get; set; }
        public int Quantity { get; set; }
        public string DeleteFiles { get; set; }
        public List<IFormFile> files { get; set; }
    }

    public class UpdateProductCommandHandler : IRequestHandler<UpdateProductCommand, Response<int>>
    {
        private readonly ICustomerProductRepositoryAsync _customerProductRepository;
        private readonly ICustomerProductFileRepositoryAsync _customerProductFileRepository;
        private readonly IProductTypeRepositoryAsync _productTypeRepository;
        private readonly IProductPackagingRepositoryAsync _productPackagingRepository;
        public readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;
        private readonly IConfiguration _configuration;

        public UpdateProductCommandHandler(ICustomerProductRepositoryAsync customerProductRepository, ICustomerProductFileRepositoryAsync customerProductFileRepository, IApplicationLogRepositoryAsync applicationLogRepository
            , IProductTypeRepositoryAsync productTypeRepository, IProductPackagingRepositoryAsync productPackagingRepository, IMapper mapper, IConfiguration configuration)
        {
            _customerProductRepository = customerProductRepository;
            _customerProductFileRepository = customerProductFileRepository;
            _productTypeRepository = productTypeRepository;
            _productPackagingRepository = productPackagingRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
            _configuration = configuration;
        }

        public async Task<Response<int>> Handle(UpdateProductCommand request, CancellationToken cancellationToken)
        {
            var getContentPath = _configuration.GetSection("ContentPath");
            string contentPath = getContentPath.Value;
            var customerProduct = (await _customerProductRepository.FindByCondition(x => x.Id == request.Id && x.CustomerId.Equals(request.CustomerId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            if (customerProduct == null)
            {
                throw new ApiException($"CustomerProduct Not Found.");
            }
            else
            {
                var productType = (await _productTypeRepository.FindByCondition(x => x.Id == request.ProductTypeId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                var productPackaging = (await _productPackagingRepository.FindByCondition(x => x.Id == request.PackagingId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                customerProduct.Name = request.Name;
                customerProduct.ProductType = productType;
                customerProduct.ProductTypeDetail = request.ProductTypeDetail;
                customerProduct.Packaging = productPackaging;
                customerProduct.PackagingDetail = request.PackagingDetail;
                customerProduct.Width = request.Width;
                customerProduct.Length = request.Length;
                customerProduct.Height = request.Height;
                customerProduct.Weight = request.Weight;
                customerProduct.Quantity = request.Quantity;
                customerProduct.Modified = DateTime.UtcNow;
                customerProduct.ModifiedBy = request.UserId != null ? request.UserId.Value.ToString() : "";
                await _customerProductRepository.UpdateAsync(customerProduct);
                string folderPath = contentPath + "customerproduct/" + customerProduct.Id.ToString();
                if (!Directory.Exists(folderPath))
                {
                    Directory.CreateDirectory(folderPath);
                }
                if (request.DeleteFiles != null && request.DeleteFiles.Trim() != "")
                {
                    string[] deleteFiles = request.DeleteFiles.Split(",");
                    foreach (string delFile in deleteFiles)
                    {
                        var customerFile = (await _customerProductFileRepository.FindByCondition(x => x.FileName.Trim().ToLower() == delFile.Trim().ToLower() && x.CustomerProductId.Equals(customerProduct.Id)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                        if (customerFile != null)
                        {
                            File.Delete(Path.Combine(contentPath, customerFile.FilePath));
                            await _customerProductFileRepository.DeleteAsync(customerFile);
                        }
                    }
                }
                int fileCount = 0;
                if (request.files != null)
                {
                    foreach (IFormFile file in request.files)
                    {
                        fileCount = fileCount + 1;
                        string fileName = file.FileName;
                        string contentType = file.ContentType;
                        if (fileName.Trim() == "" || fileName.ToLower().Trim() == "blob")
                        {
                            fileName = DateTime.Now.ToString("ddMMyyyyHHmmss") + fileCount.ToString("D" + 4) + ".jpg";
                            contentType = "image/jpeg";
                        }
                        string filePath = Path.Combine(folderPath, fileName);
                        using (FileStream fileStream = new FileStream(filePath, FileMode.Create))
                        {
                            await file.CopyToAsync(fileStream);
                            FileInfo fi = new FileInfo(filePath);

                            CustomerProductFile customerFile = new CustomerProductFile
                            {
                                CustomerProductId = customerProduct.Id,
                                FileName = fileName,
                                ContentType = contentType,
                                FilePath = "customerproduct/" + request.Id.ToString() + "/" + fileName,
                                FileEXT = fi.Extension,
                            };
                            var customerFileObject = await _customerProductFileRepository.AddAsync(customerFile);
                        }
                    }
                }
                var log = new CreateAppLog(_applicationLogRepository);
                log.Create("Customer", "Customer Product", "Edit", Newtonsoft.Json.JsonConvert.SerializeObject(request));
                return new Response<int>(customerProduct.Id);
            }
        }
    }
}
