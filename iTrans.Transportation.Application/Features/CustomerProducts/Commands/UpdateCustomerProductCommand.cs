﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;

namespace iTrans.Transportation.Application.Features.CustomerProducts.Commands
{
    public class UpdateCustomerProductCommand : IRequest<Response<int>>
    {
        public int Id { get; set; }
        public Guid CustomerId { get; set; }
        public string Name { get; set; }
        public int ProductTypeId { get; set; }
        public string ProductTypeDetail { get; set; }
        public int PackagingId { get; set; }
        public string PackagingDetail { get; set; }
        public int Width { get; set; }
        public int Length { get; set; }
        public int Height { get; set; }
        public decimal Weight { get; set; }
        public int Quantity { get; set; }
    }

    public class UpdateCustomerProductCommandHandler : IRequestHandler<UpdateCustomerProductCommand, Response<int>>
    {
        private readonly ICustomerProductRepositoryAsync _customerProductRepository;
        private readonly IProductTypeRepositoryAsync _productTypeRepository;
        private readonly IProductPackagingRepositoryAsync _productPackagingRepository;
        public readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;

        public UpdateCustomerProductCommandHandler(ICustomerProductRepositoryAsync customerProductRepository,IApplicationLogRepositoryAsync applicationLogRepository
            ,IProductTypeRepositoryAsync  productTypeRepository, IProductPackagingRepositoryAsync productPackagingRepository, IMapper mapper)
        {
            _customerProductRepository = customerProductRepository;
            _productTypeRepository = productTypeRepository;
            _productPackagingRepository = productPackagingRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(UpdateCustomerProductCommand request, CancellationToken cancellationToken)
        {
            var customerProduct = (await _customerProductRepository.FindByCondition(x => x.Id == request.Id && x.CustomerId.Equals(request.CustomerId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            if (customerProduct == null)
            {
                throw new ApiException($"CustomerProduct Not Found.");
            }
            else
            {
                var productType = (await _productTypeRepository.FindByCondition(x => x.Id == request.ProductTypeId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                var productPackaging = (await _productPackagingRepository.FindByCondition(x => x.Id == request.PackagingId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                customerProduct.Name = request.Name;
                customerProduct.ProductType = productType;
                customerProduct.ProductTypeDetail = request.ProductTypeDetail;
                customerProduct.Packaging = productPackaging;
                customerProduct.PackagingDetail = request.PackagingDetail;
                customerProduct.Width = request.Width;
                customerProduct.Length = request.Length;
                customerProduct.Height = request.Height;
                customerProduct.Weight = request.Weight;
                customerProduct.Quantity = request.Quantity;
                customerProduct.Modified = DateTime.UtcNow;
                customerProduct.ModifiedBy = request.CustomerId.ToString();
                await _customerProductRepository.UpdateAsync(customerProduct);
                var log = new CreateAppLog(_applicationLogRepository);
                log.Create("Customer", "Customer Product", "Edit", Newtonsoft.Json.JsonConvert.SerializeObject(request));
                return new Response<int>(customerProduct.Id);
            }
        }
    }
}
