﻿using AutoMapper;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Interfaces.Services;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.CustomerProducts.Commands
{
    public partial class CreateCustomerProductCommand : IRequest<Response<int>>
    {
        public Guid CustomerId { get; set; }
        public string Name { get; set; }
        public int ProductTypeId { get; set; }
        public string ProductTypeDetail { get; set; }
        public int PackagingId { get; set; }
        public string PackagingDetail { get; set; }
        public int Width { get; set; }
        public int Length { get; set; }
        public int Height { get; set; }
        public decimal Weight { get; set; }
        public int Quantity { get; set; }

    }
    public class CreateCustomerProductCommandHandler : IRequestHandler<CreateCustomerProductCommand, Response<int>>
    {
        private readonly ICustomerProductRepositoryAsync _customerProductRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;
        public CreateCustomerProductCommandHandler(ICustomerProductRepositoryAsync customerProductRepository, IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _customerProductRepository = customerProductRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(CreateCustomerProductCommand request, CancellationToken cancellationToken)
        {
            var customerProduct = _mapper.Map<CustomerProduct>(request);
            customerProduct.Created = DateTime.UtcNow;
            var customerProductObject = await _customerProductRepository.AddAsync(customerProduct);
            var log = new CreateAppLog(_applicationLogRepository);
            log.Create("Customer", "Customer Product", "Create", Newtonsoft.Json.JsonConvert.SerializeObject(request));
            return new Response<int>(customerProductObject.Id);
        }
    }
}
