﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.ProductType;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.ProductTypes.Queries
{
    public class GetAllProductType : IRequest<Response<IEnumerable<ProductTypeViewModel>>>
    {

    }
    public class GetAllProductTypeHandler : IRequestHandler<GetAllProductType, Response<IEnumerable<ProductTypeViewModel>>>
    {
        private readonly IProductTypeRepositoryAsync _productTypeRepository;
        private readonly IMapper _mapper;
        public GetAllProductTypeHandler(IProductTypeRepositoryAsync productTypeRepository, IMapper mapper)
        {
            _productTypeRepository = productTypeRepository;
            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<ProductTypeViewModel>>> Handle(GetAllProductType request, CancellationToken cancellationToken)
        {
            var productType = (await _productTypeRepository.FindByCondition(x => x.Active == true).ConfigureAwait(false)).AsQueryable().ToList();
            var productTypeViewModel = _mapper.Map<IEnumerable<ProductTypeViewModel>>(productType);
            return new Response<IEnumerable<ProductTypeViewModel>>(productTypeViewModel);
        }
    }
}
