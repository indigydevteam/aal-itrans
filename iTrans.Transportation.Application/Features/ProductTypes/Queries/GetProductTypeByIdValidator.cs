﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using iTrans.Transportation.Application.Interfaces.Repositories;
using FluentValidation;

namespace iTrans.Transportation.Application.Features.ProductTypes.Queries
{
    public class GetProductTypeByIdValidator : AbstractValidator<GetProductTypeById>
    {
        private readonly IProductTypeRepositoryAsync productTypeRepository;

        public GetProductTypeByIdValidator(IProductTypeRepositoryAsync productTypeRepository)
        {
            this.productTypeRepository = productTypeRepository;
            RuleFor(p => p.Id)
               .NotEmpty().WithMessage("{PropertyName} is required.")
               .NotNull()
               .MustAsync(IsProductTypeExists).WithMessage("{PropertyName} not exists.");
        }

        private async Task<bool> IsProductTypeExists(int ProductTypeId, CancellationToken cancellationToken)
        {
            var userObject = (await productTypeRepository.FindByCondition(x => x.Id.Equals(ProductTypeId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            if (userObject != null)
            {
                return true;
            }
            return false;
        }
    }
}
