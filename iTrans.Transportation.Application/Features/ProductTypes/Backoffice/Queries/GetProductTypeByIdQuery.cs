﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.ProductType;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.ProductTypes.Backoffice.Queries
{
    public class GetProductTypeByIdQuery : IRequest<Response<ProductTypeViewModel>>
    {
        public int Id { get; set; }
    }
    public class GetProductTypeByIdQueryHandler : IRequestHandler<GetProductTypeByIdQuery, Response<ProductTypeViewModel>>
    {
        private readonly IProductTypeRepositoryAsync _productTypeRepository;
        private readonly IMapper _mapper;
        public GetProductTypeByIdQueryHandler(IProductTypeRepositoryAsync productTypeRepository, IMapper mapper)
        {
            _productTypeRepository = productTypeRepository;
            _mapper = mapper;
        }
        public async Task<Response<ProductTypeViewModel>> Handle(GetProductTypeByIdQuery request, CancellationToken cancellationToken)
        {
            var productTypeObject = (await _productTypeRepository.FindByCondition(x => x.Id.Equals(request.Id)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            if (productTypeObject == null)
            {
                throw new ApiException($"ProductType Not Found.");
            }
            return new Response<ProductTypeViewModel>(_mapper.Map<ProductTypeViewModel>(productTypeObject));
        }
    }
}
