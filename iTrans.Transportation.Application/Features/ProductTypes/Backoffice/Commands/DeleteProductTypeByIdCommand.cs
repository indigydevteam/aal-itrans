﻿using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.ProductTypes.Backoffice.Commands
{
    public class DeleteProductTypeByIdCommand : IRequest<Response<int>>
    {
        public Guid? UserId { get; set; }
        public int Id { get; set; }
        public class DeleteProductTypeByIdCommandHandler : IRequestHandler<DeleteProductTypeByIdCommand, Response<int>>
        {
            private readonly IProductTypeRepositoryAsync _productTypeRepository;
            private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
            public DeleteProductTypeByIdCommandHandler(IProductTypeRepositoryAsync productTypeRepository, IApplicationLogRepositoryAsync applicationLogRepository)
            {
                _productTypeRepository = productTypeRepository;
                _applicationLogRepository = applicationLogRepository;
            }
            public async Task<Response<int>> Handle(DeleteProductTypeByIdCommand command, CancellationToken cancellationToken)
            {
                var productType = (await _productTypeRepository.FindByCondition(x => x.Id == command.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (productType == null)
                {
                    throw new ApiException($"ProductType Not Found.");
                }
                else
                {
                    productType.Active = false;
                    productType.IsDelete = true;
                    productType.Modified = DateTime.UtcNow;
                    productType.ModifiedBy = command.UserId != null ? command.UserId.GetValueOrDefault().ToString() : "";
                    await _productTypeRepository.UpdateAsync(productType);
                    var log = new CreateAppLog(_applicationLogRepository);
                    log.CreateLog("ProductType", "ProductType", "Delete", Newtonsoft.Json.JsonConvert.SerializeObject(command), command.UserId == null ? "-" : command.UserId.Value.ToString());
                    return new Response<int>(productType.Id);
                }
            }
        }
    }
}
