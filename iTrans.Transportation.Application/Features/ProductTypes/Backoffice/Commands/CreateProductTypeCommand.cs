﻿using AutoMapper;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Interfaces.Services;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.ProductTypes.Backoffice.Commands
{
    public partial class CreateProductTypeCommand : IRequest<Response<int>>
    {
        public Guid? UserId { get; set; }
        public string Name_TH { get; set; }
        public string Name_ENG { get; set; }
        public int Sequence { get; set; }
        public bool Active { get; set; }
        public  bool Specified { get; set; }
        public IFormFile ProductImage { get; set; }
        public string ContentDirectory { get; set; }



    }
    public class CreateProductTypeCommandHandler : IRequestHandler<CreateProductTypeCommand, Response<int>>
    {
        private readonly IProductTypeRepositoryAsync _productTypeRepository;
        private readonly IProductTypeFileRepositoryAsync _productTypeFileRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;
        public CreateProductTypeCommandHandler(IProductTypeRepositoryAsync productTypeRepository,IApplicationLogRepositoryAsync applicationLogRepository, IProductTypeFileRepositoryAsync productTypeFileRepository, IMapper mapper)
        {
            _productTypeRepository = productTypeRepository;
            _applicationLogRepository = applicationLogRepository;
            _productTypeFileRepository = productTypeFileRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(CreateProductTypeCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var productType = _mapper.Map<ProductType>(request);
                productType.Created = DateTime.UtcNow;
                productType.Modified = DateTime.UtcNow;
                productType.CreatedBy = request.UserId == null ? "" : request.UserId.Value.ToString();
                productType.ModifiedBy = request.UserId == null ? "" : request.UserId.Value.ToString();

                var productTypeObject = await _productTypeRepository.AddAsync(productType);

                if (request.ProductImage != null)
                {
                    string folderPath = request.ContentDirectory + "ProductType/" + request.Name_TH.ToString();
                    if (!Directory.Exists(folderPath))
                    {
                        Directory.CreateDirectory(folderPath);
                    }
                    string filePath = Path.Combine(folderPath, request.ProductImage.FileName);
                    using (FileStream fileStream = new FileStream(filePath, FileMode.Create))
                    {
                        await request.ProductImage.CopyToAsync(fileStream);
                        FileInfo fi = new FileInfo(filePath);

                        ProductTypeFile productTypeFile = new ProductTypeFile
                        {
                            ProductType = productType,
                            FileName = request.ProductImage.FileName,
                            ContentType = request.ProductImage.ContentType,
                            FilePath = "ProductType/" + request.Name_TH.ToString() + "/" + request.ProductImage.FileName,
                            DirectoryPath = "ProductType/" + request.Name_TH.ToString() + "/",
                            FileEXT = fi.Extension,
                            Created = DateTime.Now,
                            CreatedBy = request.UserId != null ? request.UserId.Value.ToString() : "",
                            Modified = DateTime.Now,
                            ModifiedBy = request.UserId != null ? request.UserId.Value.ToString() : ""
                        };
                        await _productTypeFileRepository.AddAsync(productTypeFile);
                    }
                }


                var log = new CreateAppLog(_applicationLogRepository);
                log.CreateLog("ProductType", "Product Type", "Create", Newtonsoft.Json.JsonConvert.SerializeObject(request),request.UserId.ToString());
                return new Response<int>(productTypeObject.Id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
