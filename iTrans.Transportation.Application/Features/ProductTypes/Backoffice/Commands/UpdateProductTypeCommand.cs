﻿using System;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;
using Microsoft.AspNetCore.Http;

namespace iTrans.Transportation.Application.Features.ProductTypes.Backoffice.Commands
{
    public class UpdateProductTypeCommand : IRequest<Response<int>>
    {
        public Guid? UserId { get; set; }
        public int Id { get; set; }
        public string Name_TH { get; set; }
        public string Name_ENG { get; set; }
        public int Sequence { get; set; }
        public bool Active { get; set; }
        public bool Specified { get; set; }
        public IFormFile ProductImage { get; set; }
        public string ContentDirectory { get; set; }
    }

    public class UpdateProductTypeCommandHandler : IRequestHandler<UpdateProductTypeCommand, Response<int>>
    {
        private readonly IProductTypeRepositoryAsync _productTypeRepository;
        private readonly IProductTypeFileRepositoryAsync _productTypeFileRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;

        public UpdateProductTypeCommandHandler(IProductTypeRepositoryAsync productTypeRepository, IApplicationLogRepositoryAsync applicationLogRepository, IProductTypeFileRepositoryAsync productTypeFileRepository, IMapper mapper)
        {
            _productTypeRepository = productTypeRepository;
            _applicationLogRepository = applicationLogRepository;
            _productTypeFileRepository = productTypeFileRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(UpdateProductTypeCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var productType = (await _productTypeRepository.FindByCondition(x => x.Id == request.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (productType == null)
                {
                    throw new ApiException($"ProductType Not Found.");
                }
                else
                {
                    productType.Name_TH = request.Name_TH;
                    productType.Name_ENG = request.Name_ENG;
                    productType.Sequence = request.Sequence;
                    productType.Active = request.Active;
                    productType.Specified = request.Specified;
                    productType.Modified = DateTime.UtcNow;
                    productType.ModifiedBy = request.UserId == null ? "" : request.UserId.Value.ToString();
                    await _productTypeRepository.UpdateAsync(productType);

                   
                    if (request.ProductImage != null)
                    {
                        string folderPath = request.ContentDirectory + "ProductType/" + request.Name_TH.ToString();
                        if (!Directory.Exists(folderPath))
                        {
                            Directory.CreateDirectory(folderPath);
                        }

                        var file = _productTypeFileRepository.FindByCondition(f => f.ProductType.Id.Equals(request.Id)).Result.FirstOrDefault();
                        if (file != null)
                        {
                            if (File.Exists(Path.Combine(request.ContentDirectory, file.FilePath)))
                                File.Delete(Path.Combine(request.ContentDirectory, file.FilePath));

                            string filePath = Path.Combine(folderPath, request.ProductImage.FileName);
                            using (FileStream fileStream = new FileStream(filePath, FileMode.Create))
                            {
                                await request.ProductImage.CopyToAsync(fileStream);
                                FileInfo fi = new FileInfo(filePath);

                                file.FileName = request.ProductImage.FileName;
                                file.ContentType = request.ProductImage.ContentType;
                                file.FilePath = "ProductType/" + request.Name_TH.ToString() + "/" + request.ProductImage.FileName;
                                file.DirectoryPath = "ProductType/" + request.Name_TH.ToString() + "/";
                                file.FileEXT = fi.Extension;
                                file.Modified = DateTime.Now;
                                file.ModifiedBy = request.UserId != null ? request.UserId.Value.ToString() : "";

                                await _productTypeFileRepository.UpdateAsync(file);
                            }
                        }
                        else
                        {
                            string filePath = Path.Combine(folderPath, request.ProductImage.FileName);
                            using (FileStream fileStream = new FileStream(filePath, FileMode.Create))
                            {
                                await request.ProductImage.CopyToAsync(fileStream);
                                FileInfo fi = new FileInfo(filePath);

                                ProductTypeFile productTypeFile = new ProductTypeFile
                                {
                                    ProductType = productType,
                                    FileName = request.ProductImage.FileName,
                                    ContentType = request.ProductImage.ContentType,
                                    FilePath = "carType/" + request.Name_TH.ToString() + "/" + request.ProductImage.FileName,
                                    DirectoryPath = "carType/" + request.Name_TH.ToString() + "/",
                                    FileEXT = fi.Extension,
                                    Created = DateTime.Now,
                                    CreatedBy = request.UserId != null ? request.UserId.Value.ToString() : "",
                                    Modified = DateTime.Now,
                                    ModifiedBy = request.UserId != null ? request.UserId.Value.ToString() : ""

                                };
                                await _productTypeFileRepository.AddAsync(productTypeFile);
                            }
                        }
                    }



                    var log = new CreateAppLog(_applicationLogRepository);
                    log.CreateLog("ProductType", "Product Tyoe", "Edit", Newtonsoft.Json.JsonConvert.SerializeObject(request), request.UserId.ToString());
                    return new Response<int>(productType.Id);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
