﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.ProductType;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;

namespace iTrans.Transportation.Application.Features.ProductTypes.Backoffice
{
    public class GetAllProductType : IRequest<PagedResponse<IEnumerable<ProductTypeViewModel>>>
    {
        public string Search { set; get; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public int Active { set; get; }
        public string Column { set; get; }
    }
    public class GetAllProductTypeHandler : IRequestHandler<GetAllProductType, PagedResponse<IEnumerable<ProductTypeViewModel>>>
    {
        private readonly IProductTypeRepositoryAsync _productTypeRepository;
        private readonly IMapper _mapper;

        public GetAllProductTypeHandler(IProductTypeRepositoryAsync productTypeRepository, IMapper mapper)
        {
            _productTypeRepository = productTypeRepository;
            _mapper = mapper;
        }

        public async Task<PagedResponse<IEnumerable<ProductTypeViewModel>>> Handle(GetAllProductType request, CancellationToken cancellationToken)
        {
            var validFilter = _mapper.Map<GetAllProductType>(request);
            var result = from x in _productTypeRepository.GetAllAsync().Result.ToList().OrderByDescending(x => x.Modified)
                         select new ProductTypeViewModel
                         {
                             id = x.Id,
                             name_TH = x.Name_TH != null ? x.Name_TH : "-",
                             name_ENG = x.Name_ENG != null ? x.Name_ENG : "-",
                             sequence = x.Sequence.ToString() != null ? x.Sequence : 0,
                             active = x.Active
                         };


            if (validFilter.Search != null && validFilter.Search.Trim() != "")
            {
                result = result.Where(x => x.name_TH.Contains(validFilter.Search.Trim())
                         || x.name_ENG.Contains(validFilter.Search.Trim()) || x.sequence.Equals(validFilter.Search.Trim())).ToList();
            }

            if (request.Column == "name_TH" && request.Active == 3)
            {
                result = result.OrderBy(x => x.name_TH);
            }
            if (request.Column == "name_TH" && request.Active == 2)
            {
                result = result.OrderByDescending(x => x.name_TH);
            }
            if (request.Column == "name_ENG" && request.Active == 3)
            {
                result = result.OrderBy(x => x.name_ENG);
            }
            if (request.Column == "name_ENG" && request.Active == 2)
            {
                result = result.OrderByDescending(x => x.name_ENG);
            }
            if (request.Column == "sequence" && request.Active == 3)
            {
                result = result.OrderBy(x => x.sequence);
            }
            if (request.Column == "sequence" && request.Active == 2)
            {
                result = result.OrderByDescending(x => x.sequence);
            }

            var itemCount = result.Count();
            result = result.Skip((validFilter.PageNumber - 1) * validFilter.PageSize).Take(validFilter.PageSize);
            return new PagedResponse<IEnumerable<ProductTypeViewModel>>(result, validFilter.PageNumber, validFilter.PageSize, itemCount);
        }
    }
}
