﻿using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
namespace iTrans.Transportation.Application.Features.CustomerProductFiles.Commands
{
    public class DeleteCustomerProductFileByIdCommand : IRequest<Response<int>>
    {
        public int Id { get; set; }
        public class DeleteCustomerProductFileByIdCommandHandler : IRequestHandler<DeleteCustomerProductFileByIdCommand, Response<int>>
        {
            private readonly ICustomerProductFileRepositoryAsync _customerProductFileRepository;
            public DeleteCustomerProductFileByIdCommandHandler(ICustomerProductFileRepositoryAsync customerProductFileRepository)
            {
                _customerProductFileRepository = customerProductFileRepository;
            }
            public async Task<Response<int>> Handle(DeleteCustomerProductFileByIdCommand command, CancellationToken cancellationToken)
            {
                var customerProductFile = (await _customerProductFileRepository.FindByCondition(x => x.Id == command.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (customerProductFile == null)
                {
                    throw new ApiException($"CustomerProductFile Not Found.");
                }
                else
                {
                    await _customerProductFileRepository.DeleteAsync(customerProductFile);
                    return new Response<int>(customerProductFile.Id);
                }
            }
        }
    }
}
