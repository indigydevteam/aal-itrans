﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;


namespace iTrans.Transportation.Application.Features.CustomerProductFiles.Commands
{
    public class UpdateCustomerProductFileCommandValidator : AbstractValidator<UpdateCustomerProductFileCommand>
    {
        private readonly ICustomerProductFileRepositoryAsync customerProductFileRepository;

        public UpdateCustomerProductFileCommandValidator(ICustomerProductFileRepositoryAsync customerProductFileRepository)
        {
            this.customerProductFileRepository = customerProductFileRepository;

            RuleFor(p => p.CustomerProductId)
                .NotNull().WithMessage("{PropertyName} is required.");

            RuleFor(p => p.FileName)
                .NotEmpty().WithMessage("{PropertyName} is required.")
                .NotNull();

            RuleFor(p => p.ContentType)
               .NotEmpty().WithMessage("{PropertyName} is required.")
               .NotNull();

            RuleFor(p => p.FilePath)
               .NotEmpty().WithMessage("{PropertyName} is required.")
               .NotNull();

            RuleFor(p => p.FileEXT)
               .NotEmpty().WithMessage("{PropertyName} is required.")
               .NotNull();
        }

        private async Task<bool> IsUnique(string value, CancellationToken cancellationToken)
        {
            var customerProductFileObject = (await customerProductFileRepository.FindByCondition(x => x.FileName.ToLower() == value.ToLower()).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            if (customerProductFileObject != null)
            {
                return false;
            }
            return true;
        }
    }
}
