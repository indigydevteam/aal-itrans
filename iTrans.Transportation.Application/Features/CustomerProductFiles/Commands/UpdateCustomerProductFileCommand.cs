﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;

namespace iTrans.Transportation.Application.Features.CustomerProductFiles.Commands
{
    public class UpdateCustomerProductFileCommand : IRequest<Response<int>>
    {
        public int Id { get; set; }
        public int CustomerProductId { get; set; }
        public string FileName { get; set; }
        public string ContentType { get; set; }
        public string FilePath { get; set; }
        public string FileEXT { get; set; }
    }

    public class UpdateCustomerProductFileCommandHandler : IRequestHandler<UpdateCustomerProductFileCommand, Response<int>>
    {
        private readonly ICustomerProductFileRepositoryAsync _customerProductFileRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;

        public UpdateCustomerProductFileCommandHandler(ICustomerProductFileRepositoryAsync customerProductFileRepository, IApplicationLogRepositoryAsync applicationLogRepository,IMapper mapper)
        {
            _customerProductFileRepository = customerProductFileRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(UpdateCustomerProductFileCommand request, CancellationToken cancellationToken)
        {
            var customerProductFile = (await _customerProductFileRepository.FindByCondition(x => x.Id == request.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            if (customerProductFile == null)
            {
                throw new ApiException($"CustomerProductFile Not Found.");
            }
            else
            {
                customerProductFile.CustomerProductId = request.CustomerProductId;
                customerProductFile.FileName = request.FileName;
                customerProductFile.ContentType = request.ContentType;
                customerProductFile.FilePath = request.FilePath;
                customerProductFile.FileEXT = request.FileEXT;
                customerProductFile.Modified = DateTime.UtcNow;
                await _customerProductFileRepository.UpdateAsync(customerProductFile);
                var log = new CreateAppLog(_applicationLogRepository);
                log.Create("Customer", "Customer ProductFile", "Edit", Newtonsoft.Json.JsonConvert.SerializeObject(customerProductFile));
                return new Response<int>(customerProductFile.Id);
            }
        }
    }
}
