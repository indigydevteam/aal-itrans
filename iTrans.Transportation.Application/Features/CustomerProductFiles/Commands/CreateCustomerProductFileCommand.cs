﻿using AutoMapper;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Interfaces.Services;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.CustomerProductFiles.Commands
{
    public partial class CreateCustomerProductFileCommand : IRequest<Response<int>>
    {
        public int Id { get; set; }
        public int CustomerProductId { get; set; }
        public string FileName { get; set; }
        public  string ContentType { get; set; }
        public  string FilePath { get; set; }
        public  string FileEXT { get; set; }

    }
    public class CreateCustomerProductFileCommandHandler : IRequestHandler<CreateCustomerProductFileCommand, Response<int>>
    {
        private readonly ICustomerProductFileRepositoryAsync _customerProductFileRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;
        public CreateCustomerProductFileCommandHandler(ICustomerProductFileRepositoryAsync customerProductFileRepository, IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _customerProductFileRepository = customerProductFileRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(CreateCustomerProductFileCommand request, CancellationToken cancellationToken)
        {
            var customerProductFile = _mapper.Map<CustomerProductFile>(request);
            customerProductFile.Created = DateTime.UtcNow;
            customerProductFile.Modified = DateTime.UtcNow;
            var customerProductFileObject = await _customerProductFileRepository.AddAsync(customerProductFile);
            var log = new CreateAppLog(_applicationLogRepository);
            log.Create("Customer", "Customer ProductFile", "Create", Newtonsoft.Json.JsonConvert.SerializeObject(customerProductFile));
            return new Response<int>(customerProductFileObject.Id);
        }
    }
}
