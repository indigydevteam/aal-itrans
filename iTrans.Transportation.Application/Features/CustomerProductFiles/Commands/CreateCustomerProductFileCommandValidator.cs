﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
namespace iTrans.Transportation.Application.Features.CustomerProductFiles.Commands
{
    public class CreateCustomerProductFileCommandValidator : AbstractValidator<CreateCustomerProductFileCommand>
    {
        private readonly ICustomerProductFileRepositoryAsync customerProductFileRepository;

        public CreateCustomerProductFileCommandValidator(ICustomerProductFileRepositoryAsync customerProductFileRepository)
        {
            this.customerProductFileRepository = customerProductFileRepository;

            RuleFor(p => p.CustomerProductId)
                .NotNull().WithMessage("{PropertyName} is required.");

            RuleFor(p => p.FileName)
                .NotEmpty().WithMessage("{PropertyName} is required.")
                .NotNull();

            RuleFor(p => p.ContentType)
               .NotEmpty().WithMessage("{PropertyName} is required.")
               .NotNull();

            RuleFor(p => p.FilePath)
               .NotEmpty().WithMessage("{PropertyName} is required.")
               .NotNull();

            RuleFor(p => p.FileEXT)
               .NotEmpty().WithMessage("{PropertyName} is required.")
               .NotNull();
        }
    }
}
