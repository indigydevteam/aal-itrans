﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.CustomerProductFile;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.CustomerProductFiles.Queries
{
   public class GetCustomerProductFileByIdQuery : IRequest<Response<CustomerProductFileViewModel>>
    {
        public int Id { get; set; }
    }
    public class GetCustomerProductFileByIdQueryHandler : IRequestHandler<GetCustomerProductFileByIdQuery, Response<CustomerProductFileViewModel>>
    {
        private readonly ICustomerProductFileRepositoryAsync _customerProductFileRepository;
        private readonly IMapper _mapper;
        public GetCustomerProductFileByIdQueryHandler(ICustomerProductFileRepositoryAsync customerProductFileRepository, IMapper mapper)
        {
            _customerProductFileRepository = customerProductFileRepository;
            _mapper = mapper;
        }
        public async Task<Response<CustomerProductFileViewModel>> Handle(GetCustomerProductFileByIdQuery request, CancellationToken cancellationToken)
        {
            var customerProductFileObject = (await _customerProductFileRepository.FindByCondition(x => x.Id.Equals(request.Id)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            return new Response<CustomerProductFileViewModel>(_mapper.Map<CustomerProductFileViewModel>(customerProductFileObject));
        }
    }
}
