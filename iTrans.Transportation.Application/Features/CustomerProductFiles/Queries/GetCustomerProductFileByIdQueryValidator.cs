﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using iTrans.Transportation.Application.Interfaces.Repositories;
using FluentValidation;

namespace iTrans.Transportation.Application.Features.CustomerProductFiles.Queries
{
    public class GetCustomerProductFileByIdQueryValidator : AbstractValidator<GetCustomerProductFileByIdQuery>
    {
        private readonly ICustomerProductFileRepositoryAsync customerProductFileRepository;

        public GetCustomerProductFileByIdQueryValidator(ICustomerProductFileRepositoryAsync customerProductFileRepository)
        {
            this.customerProductFileRepository = customerProductFileRepository;
            RuleFor(p => p.Id)
               .NotEmpty().WithMessage("{PropertyName} is required.")
               .NotNull()
               .MustAsync(IsCustomerProductFileExists).WithMessage("{PropertyName} not exists.");
        }

        private async Task<bool> IsCustomerProductFileExists(int Id, CancellationToken cancellationToken)
        {
            var userObject = (await customerProductFileRepository.FindByCondition(x => x.Id.Equals(Id)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            if (userObject != null)
            {
                return true;
            }
            return false;
        }
    }
}
