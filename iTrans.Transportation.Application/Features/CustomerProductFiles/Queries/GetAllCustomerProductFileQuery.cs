﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.CustomerProductFile;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.CustomerProductFiles.Queries
{
    public class GetAllCustomerProductFileQuery : IRequest<Response<IEnumerable<CustomerProductFileViewModel>>>
    {
         
    }
    public class GetAllCustomerProductFileQueryHandler : IRequestHandler<GetAllCustomerProductFileQuery, Response<IEnumerable<CustomerProductFileViewModel>>>
    {
        private readonly ICustomerProductFileRepositoryAsync _customerProductFileRepository;
        private readonly IMapper _mapper;
        public GetAllCustomerProductFileQueryHandler(ICustomerProductFileRepositoryAsync customerProductFileRepository, IMapper mapper)
        {
            _customerProductFileRepository = customerProductFileRepository;
            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<CustomerProductFileViewModel>>> Handle(GetAllCustomerProductFileQuery request, CancellationToken cancellationToken)
        {
            var customerProductFile = await _customerProductFileRepository.GetAllAsync();
            var customerProductFileViewModel = _mapper.Map<IEnumerable<CustomerProductFileViewModel>>(customerProductFile);
            return new Response<IEnumerable<CustomerProductFileViewModel>>(customerProductFileViewModel);
        }
    }
}
