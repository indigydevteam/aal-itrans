﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.DriverPaymentTransaction;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.DriverPaymentTransactiones.Queries
{
    public class GetDriverPaymentTransactionByIdQuery : IRequest<Response<DriverPaymentTransactionViewModel>>
    {
        public int Id { get; set; }

    }
    public class GetDriverPaymentTransactionByIdQueryHandler : IRequestHandler<GetDriverPaymentTransactionByIdQuery, Response<DriverPaymentTransactionViewModel>>
    {
        private readonly IDriverPaymentTransactionRepositoryAsync _DriverPaymentTransactionRepository;
        private readonly IMapper _mapper;
        public GetDriverPaymentTransactionByIdQueryHandler(IDriverPaymentTransactionRepositoryAsync DriverPaymentTransactionRepository, IMapper mapper)
        {
            _DriverPaymentTransactionRepository = DriverPaymentTransactionRepository;
            _mapper = mapper;
        }
        public async Task<Response<DriverPaymentTransactionViewModel>> Handle(GetDriverPaymentTransactionByIdQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var DriverObject = (await _DriverPaymentTransactionRepository.FindByCondition(x => x.Id.Equals(request.Id)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                return new Response<DriverPaymentTransactionViewModel>(_mapper.Map<DriverPaymentTransactionViewModel>(DriverObject));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
