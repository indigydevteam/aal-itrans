﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.DriverPaymentTransaction;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.DriverPaymentTransactiones.Queries
{
    public class GetDriverPaymentTransactionByDriverPaymentHistoryQuery : IRequest<Response<IEnumerable<DriverPaymentTransactionViewModel>>>
    {
        public int DriverPaymentHistoryId { get; set; }
    }
    public class GetDriverPaymentTransactionByDriverPaymentHistoryQueryHandler : IRequestHandler<GetDriverPaymentTransactionByDriverPaymentHistoryQuery, Response<IEnumerable<DriverPaymentTransactionViewModel>>>
    {
        private readonly IDriverPaymentTransactionRepositoryAsync _DriverPaymentTransactionRepository;
        private readonly IMapper _mapper;
        public GetDriverPaymentTransactionByDriverPaymentHistoryQueryHandler(IDriverPaymentTransactionRepositoryAsync DriverPaymentTransactionRepository, IMapper mapper)
        {
            _DriverPaymentTransactionRepository = DriverPaymentTransactionRepository;
            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<DriverPaymentTransactionViewModel>>> Handle(GetDriverPaymentTransactionByDriverPaymentHistoryQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var DriverPaymentTransaction = (await _DriverPaymentTransactionRepository.FindByCondition(x => x.Driver_PaymentHistoryId.Equals(request.DriverPaymentHistoryId)).ConfigureAwait(false)).AsQueryable().ToList();
                var DriverPaymentTransactionViewModel = _mapper.Map<IEnumerable<DriverPaymentTransactionViewModel>>(DriverPaymentTransaction);
                return new Response<IEnumerable<DriverPaymentTransactionViewModel>>(DriverPaymentTransactionViewModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
