﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.DriverPaymentTransaction;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.DriverPaymentTransactions.Queries
{
    public class GetAllDriverPaymentTransactionQuery : IRequest<Response<IEnumerable<DriverPaymentTransactionViewModel>>>
    {

    }
    public class GetAllDriverPaymentTransactionQueryHandler : IRequestHandler<GetAllDriverPaymentTransactionQuery, Response<IEnumerable<DriverPaymentTransactionViewModel>>>
    {
        private readonly IDriverPaymentTransactionRepositoryAsync _DriverPaymentTransactionRepository;
        private readonly IMapper _mapper;
        public GetAllDriverPaymentTransactionQueryHandler(IDriverPaymentTransactionRepositoryAsync DriverPaymentTransactionRepository, IMapper mapper)
        {
            _DriverPaymentTransactionRepository = DriverPaymentTransactionRepository;
            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<DriverPaymentTransactionViewModel>>> Handle(GetAllDriverPaymentTransactionQuery request, CancellationToken cancellationToken)
        {
            var Driver = await _DriverPaymentTransactionRepository.GetAllAsync();
            var DriverPaymentTransactionViewModel = _mapper.Map<IEnumerable<DriverPaymentTransactionViewModel>>(Driver);
            return new Response<IEnumerable<DriverPaymentTransactionViewModel>>(DriverPaymentTransactionViewModel);
        }
    }
}
