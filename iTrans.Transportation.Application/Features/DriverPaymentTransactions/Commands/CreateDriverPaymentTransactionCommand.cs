﻿using AutoMapper;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Interfaces.Services;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using iTrans.Transportation.Domain.Entities;
using MediatR;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.DriverPaymentTransactions.Commands
{
    public partial class CreateDriverPaymentTransactionCommand : IRequest<Response<int>>
    {
        public int Driver_PaymentHistoryId { get; set; }
        public string Detail { get; set; }

    }
    public class CreateDriverPaymentTransactionCommandHandler : IRequestHandler<CreateDriverPaymentTransactionCommand, Response<int>>
    {
        private readonly IDriverPaymentTransactionRepositoryAsync _DriverPaymentTransactionRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;
        public CreateDriverPaymentTransactionCommandHandler(IDriverPaymentTransactionRepositoryAsync DriverPaymentTransactionRepository,IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _DriverPaymentTransactionRepository = DriverPaymentTransactionRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(CreateDriverPaymentTransactionCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var DriverPaymentTransaction = _mapper.Map<DriverPaymentTransaction>(request);
                DriverPaymentTransaction.Created = DateTime.UtcNow;
                DriverPaymentTransaction.Modified = DateTime.UtcNow;
                var DriverPaymentTransactionObject = await _DriverPaymentTransactionRepository.AddAsync(DriverPaymentTransaction);
                var log = new CreateAppLog(_applicationLogRepository);
                log.Create("Driver", "Driver PaymentTransaction", "Create", Newtonsoft.Json.JsonConvert.SerializeObject(DriverPaymentTransaction));
                return new Response<int>(DriverPaymentTransactionObject.Id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
