﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;

namespace iTrans.Transportation.Application.Features.DriverPaymentTransactions.Commands
{
    public class UpdateDriverPaymentTransactionCommand : IRequest<Response<int>>
    {
        public int Id { get; set; }
        public int Driver_PaymentHistoryId { get; set; }
        public string Detail { get; set; }
    }
    public class UpdateDriverPaymentTransactionCommandHandler : IRequestHandler<UpdateDriverPaymentTransactionCommand, Response<int>>
    {
        private readonly IDriverPaymentTransactionRepositoryAsync _DriverPaymentTransactionRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;

        public UpdateDriverPaymentTransactionCommandHandler(IDriverPaymentTransactionRepositoryAsync DriverPaymentTransactionRepository,IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _DriverPaymentTransactionRepository = DriverPaymentTransactionRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(UpdateDriverPaymentTransactionCommand request, CancellationToken cancellationToken)
        {
            var DriverPaymentTransaction = (await _DriverPaymentTransactionRepository.FindByCondition(x => x.Id == request.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            if (DriverPaymentTransaction == null)
            {
                throw new ApiException($"DriverPaymentTransaction Not Found.");
            }
            else
            {
                DriverPaymentTransaction.Driver_PaymentHistoryId = request.Driver_PaymentHistoryId;
                DriverPaymentTransaction.Detail = request.Detail;
                DriverPaymentTransaction.Modified = DateTime.UtcNow;
                await _DriverPaymentTransactionRepository.UpdateAsync(DriverPaymentTransaction);
                var log = new CreateAppLog(_applicationLogRepository);
                log.Create("Driver", "Driver PaymentTransaction", "Edit", Newtonsoft.Json.JsonConvert.SerializeObject(DriverPaymentTransaction));
                return new Response<int>(DriverPaymentTransaction.Id);
            }
        }
    }
}
