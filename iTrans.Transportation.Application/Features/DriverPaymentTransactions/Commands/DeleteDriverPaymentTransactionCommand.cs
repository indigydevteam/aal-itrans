﻿using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
namespace iTrans.Transportation.Application.Features.DriverPaymentTransactions.Commands
{
    public class DeleteDriverPaymentTransactionCommand : IRequest<Response<int>>
    {
        public int Id { get; set; }
        public class DeleteDriverPaymentTransactionCommandHandler : IRequestHandler<DeleteDriverPaymentTransactionCommand, Response<int>>
        {
            private readonly IDriverPaymentTransactionRepositoryAsync _DriverPaymentTransactionRepository;
            public DeleteDriverPaymentTransactionCommandHandler(IDriverPaymentTransactionRepositoryAsync DriverPaymentTransactionRepository)
            {
                _DriverPaymentTransactionRepository = DriverPaymentTransactionRepository;
            }
            public async Task<Response<int>> Handle(DeleteDriverPaymentTransactionCommand command, CancellationToken cancellationToken)
            {
                var DriverPaymentTransaction = (await _DriverPaymentTransactionRepository.FindByCondition(x => x.Id == command.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (DriverPaymentTransaction == null)
                {
                    throw new ApiException($"DriverPaymentTransaction Not Found.");
                }
                else
                {
                    await _DriverPaymentTransactionRepository.DeleteAsync(DriverPaymentTransaction);
                    return new Response<int>(DriverPaymentTransaction.Id);
                }
            }
        }
    }
}
