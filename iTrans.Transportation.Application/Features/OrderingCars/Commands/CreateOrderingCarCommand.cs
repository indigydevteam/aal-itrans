﻿using AutoMapper;
//using iTrans.Transportation.Application.DTOs.OrderingCarDriver;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Interfaces.Services;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using iTrans.Transportation.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.OrderingCars.Commands
{
    public partial class CreateOrderingCarCommand : IRequest<Response<int>>
    {
        public  Guid OrderingId { get; set; }
        public  string CarRegistration { get; set; }
        public  DateTime ActExpiry { get; set; }
        public  int CarTypeId { get; set; }
        public  int CarListId { get; set; }
        public  int CarDescriptionId { get; set; }
        public  string CarDescriptionDetail { get; set; }
        public  int CarFeatureId { get; set; }
        public  string CarFeatureDetail { get; set; }
        public  int CarSpecificationId { get; set; }
        public  string CarSpecificationDetail { get; set; }
        public  int Width { get; set; }
        public  int Length { get; set; }
        public  int Height { get; set; }
        public  string DriverName { get; set; }
        public  string DriverPhoneCode { get; set; }
        public  string DriverPhoneNumber { get; set; }
        public  bool ProductInsurance { get; set; }
        public  decimal ProductInsuranceAmount { get; set; }
        public  string Note { get; set; }
        //public  CreateDriverViewModel Driver { get; set; }

    }
    public class CreateOrderingCarCommandHandler : IRequestHandler<CreateOrderingCarCommand, Response<int>>
    {
        private readonly ICarTypeRepositoryAsync _carTypeRepository;
        private readonly ICarListRepositoryAsync _carListRepository;
        private readonly ICarDescriptionRepositoryAsync _carDescriptionRepository;
        private readonly ICarFeatureRepositoryAsync _carFeatureRepository;
        private readonly ICarSpecificationRepositoryAsync _carSpecificationRepository;
        private readonly IOrderingRepositoryAsync _orderingRepository;
        private readonly IOrderingCarRepositoryAsync _orderingCarRepository;
        private readonly ITitleRepositoryAsync _titleRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;
        public CreateOrderingCarCommandHandler(IOrderingRepositoryAsync orderingRepository,IOrderingCarRepositoryAsync orderingCarRepository, ICarTypeRepositoryAsync carTypeRepository, ICarListRepositoryAsync carListRepository, ICarDescriptionRepositoryAsync carDescriptionRepository
            , ICarFeatureRepositoryAsync carFeatureRepository, ICarSpecificationRepositoryAsync carSpecificationRepository, ITitleRepositoryAsync titleRepository,IApplicationLogRepositoryAsync applicationLogRepository ,IMapper mapper)
        {
            _orderingRepository = orderingRepository;
            _orderingCarRepository = orderingCarRepository;
            _carTypeRepository = carTypeRepository;
            _carListRepository = carListRepository;
            _carDescriptionRepository = carDescriptionRepository;
            _carFeatureRepository = carFeatureRepository;
            _carSpecificationRepository = carSpecificationRepository;
            _titleRepository = titleRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(CreateOrderingCarCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var data = (await _orderingRepository.FindByCondition(x => x.Id == request.OrderingId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (data == null)
                {
                    throw new ApiException($"Ordering Not Found.");
                }
                var OrderingCar = _mapper.Map<OrderingCar>(request);
                OrderingCar.Ordering = data;
                OrderingCar.CarType = (await _carTypeRepository.FindByCondition(x => x.Id.Equals(request.CarTypeId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                OrderingCar.CarList = (await _carListRepository.FindByCondition(x => x.Id.Equals(request.CarListId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                OrderingCar.CarDescription = (await _carDescriptionRepository.FindByCondition(x => x.Id.Equals(request.CarDescriptionId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                OrderingCar.CarFeature = (await _carFeatureRepository.FindByCondition(x => x.Id.Equals(request.CarFeatureId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                OrderingCar.CarSpecification = (await _carSpecificationRepository.FindByCondition(x => x.Id.Equals(request.CarSpecificationId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                OrderingCar.Created = DateTime.UtcNow;
                OrderingCar.Modified = DateTime.UtcNow;
                //if (request.Driver != null)
                //{
                //    var driver = _mapper.Map<OrderingCarDriver>(request.Driver);
                //    driver.OrderingCar = OrderingCar;
                //    driver.Title = (await _titleRepository.FindByCondition(x => x.Id.Equals(request.Driver.TitleId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                //    OrderingCar.Drivers = new List<OrderingCarDriver>();
                //    OrderingCar.Drivers.Add(driver);
                //}
                
                //OrderingCar.CreatedBy = "xxxxxxx";
                //OrderingCar.ModifiedBy = "xxxxxxx";
                var OrderingCarObject = await _orderingCarRepository.AddAsync(OrderingCar);
                var log = new CreateAppLog(_applicationLogRepository);
                log.Create("Ordering", "OrderingCar ", "Creaet", Newtonsoft.Json.JsonConvert.SerializeObject(OrderingCarObject));
                return new Response<int>(OrderingCarObject.Id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
