﻿using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.OrderingCars.Commands
{
   public class DeleteOrderingCarCommand : IRequest<Response<int>>
    {
        public int Id { get; set; }
        public Guid OrderId { get; set; }
        public class DeleteOrderingCarCommandHandler : IRequestHandler<DeleteOrderingCarCommand, Response<int>>
        {
            private readonly IOrderingCarRepositoryAsync _orderingCarRepository;
            //private readonly IOrderingCarDriverRepositoryAsync _orderingCarDriverRepository;
            public DeleteOrderingCarCommandHandler(IOrderingCarRepositoryAsync orderingCarRepository)//, IOrderingCarDriverRepositoryAsync orderingCarDriverRepository)
            {
                _orderingCarRepository = orderingCarRepository;
                //_orderingCarDriverRepository = orderingCarDriverRepository;
            }
            public async Task<Response<int>> Handle(DeleteOrderingCarCommand command, CancellationToken cancellationToken) 
            {
                var OrderingCar = (await _orderingCarRepository.FindByCondition(x => x.Id == command.Id && x.Ordering.Id.Equals(command.OrderId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (OrderingCar == null)
                {
                    throw new ApiException($"OrderingCar Not Found.");
                }
                else
                {
                    //(await _orderingCarDriverRepository.CreateSQLQuery("DELETE Ordering_CarDriver where OrderingCarId = '" + command.Id.ToString() + "'").ConfigureAwait(false)).ExecuteUpdate();
                    await _orderingCarRepository.DeleteAsync(OrderingCar);
                    return new Response<int>(OrderingCar.Id);
                }
            }
        }
    }
}
