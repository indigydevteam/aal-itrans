﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
//using iTrans.Transportation.Application.DTOs.OrderingCarDriver;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;

namespace iTrans.Transportation.Application.Features.OrderingCars.Commands
{
    public class UpdateOrderingCarCommand : IRequest<Response<int>>
    {
        public virtual int Id { get; set; }
        public Guid OrderingId { get; set; }
        public string CarRegistration { get; set; }
        public DateTime ActExpiry { get; set; }
        public int CarTypeId { get; set; }
        public int CarListId { get; set; }
        public int CarDescriptionId { get; set; }
        public string CarDescriptionDetail { get; set; }
        public int CarFeatureId { get; set; }
        public string CarFeatureDetail { get; set; }
        public int CarSpecificationId { get; set; }
        public string CarSpecificationDetail { get; set; }
        public int Width { get; set; }
        public int Length { get; set; }
        public int Height { get; set; }
        public string DriverName { get; set; }
        public string DriverPhoneCode { get; set; }
        public string DriverPhoneNumber { get; set; }
        public bool ProductInsurance { get; set; }
        public decimal ProductInsuranceAmount { get; set; }
        public string Note { get; set; }
        //public CreateDriverViewModel Driver { get; set; }

    }

    public class UpdateOrderingCarCommandHandler : IRequestHandler<UpdateOrderingCarCommand, Response<int>>
    {
        private readonly ICarTypeRepositoryAsync _carTypeRepository;
        private readonly ICarListRepositoryAsync _carListRepository;
        private readonly ICarDescriptionRepositoryAsync _carDescriptionRepository;
        private readonly ICarFeatureRepositoryAsync _carFeatureRepository;
        private readonly ICarSpecificationRepositoryAsync _carSpecificationRepository;
        private readonly IOrderingCarRepositoryAsync _orderingCarRepository;
        private readonly ITitleRepositoryAsync _titleRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;

        public UpdateOrderingCarCommandHandler(IOrderingCarRepositoryAsync orderingCarRepository, ICarTypeRepositoryAsync carTypeRepository, ICarListRepositoryAsync carListRepository
            , ICarDescriptionRepositoryAsync carDescriptionRepository, ICarFeatureRepositoryAsync carFeatureRepository, ICarSpecificationRepositoryAsync carSpecificationRepository
            , ITitleRepositoryAsync titleRepository,IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _orderingCarRepository = orderingCarRepository;
            _carTypeRepository = carTypeRepository;
            _carListRepository = carListRepository;
            _carDescriptionRepository = carDescriptionRepository;
            _carFeatureRepository = carFeatureRepository;
            _carSpecificationRepository = carSpecificationRepository;
            _titleRepository = titleRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(UpdateOrderingCarCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var OrderingCar = (await _orderingCarRepository.FindByCondition(x => x.Id == request.Id && x.Ordering.Id.Equals(request.OrderingId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (OrderingCar == null)
                {
                    throw new ApiException($"OrderingCar Not Found.");
                }
                else
                {
                    OrderingCar.CarType = (await _carTypeRepository.FindByCondition(x => x.Id.Equals(request.CarTypeId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                    OrderingCar.CarList = (await _carListRepository.FindByCondition(x => x.Id.Equals(request.CarListId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                    OrderingCar.CarDescription = (await _carDescriptionRepository.FindByCondition(x => x.Id.Equals(request.CarDescriptionId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                    OrderingCar.CarFeature = (await _carFeatureRepository.FindByCondition(x => x.Id.Equals(request.CarFeatureId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                    OrderingCar.CarSpecification = (await _carSpecificationRepository.FindByCondition(x => x.Id.Equals(request.CarSpecificationId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                    OrderingCar.CarRegistration = request.CarRegistration;
                    OrderingCar.ActExpiry = request.ActExpiry;
                    OrderingCar.CarDescriptionDetail = request.CarDescriptionDetail;
                    OrderingCar.CarFeatureDetail = request.CarFeatureDetail;
                    OrderingCar.CarSpecificationDetail = request.CarSpecificationDetail;
                    OrderingCar.Width = request.Width;
                    OrderingCar.Length = request.Length;
                    OrderingCar.Height = request.Height;
                    OrderingCar.DriverName = request.DriverName;
                    OrderingCar.DriverPhoneCode = request.DriverPhoneCode;
                    OrderingCar.DriverPhoneNumber = request.DriverPhoneNumber;
                    OrderingCar.ProductInsurance = request.ProductInsurance;
                    OrderingCar.ProductInsuranceAmount = request.ProductInsuranceAmount;
                    OrderingCar.Note = request.Note;
                    //if (request.Driver != null)
                    //{
                    //    if (OrderingCar.Drivers.Count == 0)
                    //    {
                    //        var driver = _mapper.Map<OrderingCarDriver>(request.Driver);
                    //        driver.OrderingCar = OrderingCar;
                    //        driver.Title = (await _titleRepository.FindByCondition(x => x.Id.Equals(request.Driver.TitleId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                    //        OrderingCar.Drivers.Add(driver);
                    //    }
                    //    else
                    //    {
                    //        OrderingCar.Drivers[0].Title = (await _titleRepository.FindByCondition(x => x.Id.Equals(request.Driver.TitleId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                    //        OrderingCar.Drivers[0].FirstName = request.Driver.FirstName;
                    //        OrderingCar.Drivers[0].MiddleName = request.Driver.MiddleName;
                    //        OrderingCar.Drivers[0].LastName = request.Driver.LastName;
                    //        OrderingCar.Drivers[0].Name = request.Driver.Name;
                    //        OrderingCar.Drivers[0].IdentityNumber = request.Driver.IdentityNumber;
                    //        OrderingCar.Drivers[0].PhoneCode = request.Driver.PhoneCode;
                    //        OrderingCar.Drivers[0].PhoneNumber = request.Driver.PhoneNumber;
                    //        OrderingCar.Drivers[0].Email = request.Driver.Email;
                    //    }
                    //}
                    OrderingCar.Modified = DateTime.UtcNow;

                    await _orderingCarRepository.UpdateAsync(OrderingCar);
                    var log = new CreateAppLog(_applicationLogRepository);
                    log.Create("Ordering", "OrderingCar", "Edit", Newtonsoft.Json.JsonConvert.SerializeObject(OrderingCar));
                    return new Response<int>(OrderingCar.Id);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}