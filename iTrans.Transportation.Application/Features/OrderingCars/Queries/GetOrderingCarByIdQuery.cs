﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.OrderingCar;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.OrderingCars.Queries
{
   public class GetOrderingCarByIdQuery : IRequest<Response<OrderingCarViewModel>>
    {
        public int Id { get; set; }
    }
    public class GetOrderingCarByIdQueryHandler : IRequestHandler<GetOrderingCarByIdQuery, Response<OrderingCarViewModel>>
    {
        private readonly IOrderingCarRepositoryAsync _OrderingCarRepository;
        private readonly IMapper _mapper;
        public GetOrderingCarByIdQueryHandler(IOrderingCarRepositoryAsync OrderingCarRepository, IMapper mapper)
        {
            _OrderingCarRepository = OrderingCarRepository;
            _mapper = mapper;
        }
        public async Task<Response<OrderingCarViewModel>> Handle(GetOrderingCarByIdQuery request, CancellationToken cancellationToken)
        {
            var OrderingCarObject = (await _OrderingCarRepository.FindByCondition(x => x.Id.Equals(request.Id)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            return new Response<OrderingCarViewModel>(_mapper.Map<OrderingCarViewModel>(OrderingCarObject));
        }
    }
}
