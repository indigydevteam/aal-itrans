﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.OrderingCar;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;


namespace iTrans.Transportation.Application.Features.OrderingCars.Queries
{
  public  class GetAllOrderingCarParameter : IRequest<PagedResponse<IEnumerable<OrderingCarViewModel>>>
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
    }
    public class GetAllOrderingCarParameterHandler : IRequestHandler<GetAllOrderingCarParameter, PagedResponse<IEnumerable<OrderingCarViewModel>>>
    {
        private readonly IOrderingCarRepositoryAsync _OrderingCarRepository;
        private readonly IMapper _mapper;
        public GetAllOrderingCarParameterHandler(IOrderingCarRepositoryAsync OrderingCarRepository, IMapper mapper)
        {
            _OrderingCarRepository = OrderingCarRepository;
            _mapper = mapper;
        }

        public async Task<PagedResponse<IEnumerable<OrderingCarViewModel>>> Handle(GetAllOrderingCarParameter request, CancellationToken cancellationToken)
        {
            var validFilter = _mapper.Map<GetAllOrderingCarParameter>(request);
            Expression<Func<OrderingCar, bool>> expression = x => x.CarRegistration != "";
            var OrderingCar = await _OrderingCarRepository.FindByConditionWithPage(expression, validFilter.PageNumber, validFilter.PageSize);
            var OrderingCarViewModel = _mapper.Map<IEnumerable<OrderingCarViewModel>>(OrderingCar);
            return new PagedResponse<IEnumerable<OrderingCarViewModel>>(OrderingCarViewModel, validFilter.PageNumber, validFilter.PageSize);
        }
    }
}
