﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.Ordering;
using iTrans.Transportation.Application.DTOs.OrderingCar;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
namespace iTrans.Transportation.Application.Features.OrderingCars.Queries
{
   public class GetOrderingCarByOrderingQuery : IRequest<Response<IEnumerable<OrderingCarViewModel>>>
    {
        public int OrderingId { set; get; }
    }
    public class GetOrderingCarByOrderingQueryHandler : IRequestHandler<GetOrderingCarByOrderingQuery, Response<IEnumerable<OrderingCarViewModel>>>
    {
        private readonly IOrderingCarRepositoryAsync _orderingCarRepository;
        private readonly IMapper _mapper;
        public GetOrderingCarByOrderingQueryHandler(IOrderingCarRepositoryAsync orderingCarRepository, IMapper mapper)
        {
            _orderingCarRepository = orderingCarRepository;
            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<OrderingCarViewModel>>> Handle(GetOrderingCarByOrderingQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var OrderingCar = (await _orderingCarRepository.FindByCondition(x => x.Ordering.Id.Equals(request.OrderingId)).ConfigureAwait(false)).AsQueryable().ToList();
                var OrderingCarViweModel = _mapper.Map<IEnumerable<OrderingCarViewModel>>(OrderingCar);
                return new Response<IEnumerable<OrderingCarViewModel>>(OrderingCarViweModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}