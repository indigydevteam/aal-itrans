﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.OrderingCar;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.OrderingCars.Queries
{
   public class GetAllOrderingCarQuery : IRequest<Response<IEnumerable<OrderingCarViewModel>>>
    {

    }
    public class GetAllOrderingCarQueryHandler : IRequestHandler<GetAllOrderingCarQuery, Response<IEnumerable<OrderingCarViewModel>>>
    {
        private readonly IOrderingCarRepositoryAsync _orderingCarRepository;
        private readonly IMapper _mapper;
        public GetAllOrderingCarQueryHandler(IOrderingCarRepositoryAsync OrderingCarRepository, IMapper mapper)
        {
            _orderingCarRepository = OrderingCarRepository;
            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<OrderingCarViewModel>>> Handle(GetAllOrderingCarQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var OrderingCar = await _orderingCarRepository.GetAllAsync();
                var OrderingCarViewModel = _mapper.Map<IEnumerable<OrderingCarViewModel>>(OrderingCar);
                return new Response<IEnumerable<OrderingCarViewModel>>(OrderingCarViewModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
