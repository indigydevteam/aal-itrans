﻿using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
namespace iTrans.Transportation.Application.Features.DriverProblemFiles.Commands
{
    public class DeleteDriverProblemFileByIdCommand : IRequest<Response<int>>
    {
        public int Id { get; set; }
        public class DeleteDriverProblemFileByIdCommandHandler : IRequestHandler<DeleteDriverProblemFileByIdCommand, Response<int>>
        {
            private readonly IDriverProblemFileRepositoryAsync _DriverProblemFileRepository;
            public DeleteDriverProblemFileByIdCommandHandler(IDriverProblemFileRepositoryAsync DriverProblemFileRepository)
            {
                _DriverProblemFileRepository = DriverProblemFileRepository;
            }
            public async Task<Response<int>> Handle(DeleteDriverProblemFileByIdCommand command, CancellationToken cancellationToken)
            {
                var DriverProblemFile = (await _DriverProblemFileRepository.FindByCondition(x => x.Id == command.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (DriverProblemFile == null)
                {
                    throw new ApiException($"DriverProblemFile Not Found.");
                }
                else
                {
                    await _DriverProblemFileRepository.DeleteAsync(DriverProblemFile);
                    return new Response<int>(DriverProblemFile.Id);
                }
            }
        }
    }
}
