﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;


namespace iTrans.Transportation.Application.Features.DriverProblemFiles.Commands
{
    public class UpdateDriverProblemFileCommandValidator : AbstractValidator<UpdateDriverProblemFileCommand>
    {
        private readonly IDriverProblemFileRepositoryAsync driverProblemFileRepository;

        public UpdateDriverProblemFileCommandValidator(IDriverProblemFileRepositoryAsync driverProblemFileRepository)
        {
            this.driverProblemFileRepository = driverProblemFileRepository;

            RuleFor(p => p.DriverProblemId)
                .NotNull().WithMessage("{PropertyName} is required.");

            RuleFor(p => p.FileName)
                .NotEmpty().WithMessage("{PropertyName} is required.")
                .NotNull();

            RuleFor(p => p.ContentType)
               .NotEmpty().WithMessage("{PropertyName} is required.")
               .NotNull();

            RuleFor(p => p.FilePath)
               .NotEmpty().WithMessage("{PropertyName} is required.")
               .NotNull();

            RuleFor(p => p.FileEXT)
               .NotEmpty().WithMessage("{PropertyName} is required.")
               .NotNull();

            RuleFor(p => p.DirectoryPath)
               .NotEmpty().WithMessage("{PropertyName} is required.")
               .NotNull();
        }

        private async Task<bool> IsUnique(string value, CancellationToken cancellationToken)
        {
            var driverProblemFileObject = (await driverProblemFileRepository.FindByCondition(x => x.FileName.ToLower() == value.ToLower()).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            if (driverProblemFileObject != null)
            {
                return false;
            }
            return true;
        }
    }
}
