﻿using AutoMapper;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Interfaces.Services;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.DriverProblemFiles.Commands
{
    public partial class CreateDriverProblemFileCommand : IRequest<Response<int>>
    {
        public int Id { get; set; }
        public Guid DriverProblemId { get; set; }
        public string FileName { get; set; }
        public  string ContentType { get; set; }
        public  string FilePath { get; set; }
        public  string FileEXT { get; set; }
        public  string DirectoryPath { get; set; }
        public  int Sequence { get; set; }

    }
    public class CreateDriverProblemFileCommandHandler : IRequestHandler<CreateDriverProblemFileCommand, Response<int>>
    {
        private readonly IDriverProblemFileRepositoryAsync _driverProblemFileRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;
        public CreateDriverProblemFileCommandHandler(IDriverProblemFileRepositoryAsync driverProblemFileRepository,IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _driverProblemFileRepository = driverProblemFileRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(CreateDriverProblemFileCommand request, CancellationToken cancellationToken)
        {
            var driverProblemFile = _mapper.Map<DriverProblemFile>(request);
            driverProblemFile.Created = DateTime.UtcNow;
            driverProblemFile.Modified = DateTime.UtcNow;
            var driverProblemFileObject = await _driverProblemFileRepository.AddAsync(driverProblemFile);
            var log = new CreateAppLog(_applicationLogRepository);
            log.Create("Driver", "Driver Problemfile", "Create", Newtonsoft.Json.JsonConvert.SerializeObject(driverProblemFile));
            return new Response<int>(driverProblemFileObject.Id);
        }
    }
}
