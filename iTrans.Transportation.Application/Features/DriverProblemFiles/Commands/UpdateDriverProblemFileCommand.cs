﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;

namespace iTrans.Transportation.Application.Features.DriverProblemFiles.Commands
{
    public class UpdateDriverProblemFileCommand : IRequest<Response<int>>
    {
        public int Id { get; set; }
        public int DriverProblemId { get; set; }
        public string FileName { get; set; }
        public string ContentType { get; set; }
        public string FilePath { get; set; }
        public string FileEXT { get; set; }
        public string DirectoryPath { get; set; }
        public int Sequence { get; set; }
    }

    public class UpdateDriverProblemFileCommandHandler : IRequestHandler<UpdateDriverProblemFileCommand, Response<int>>
    {
        private readonly IDriverProblemFileRepositoryAsync _driverProblemFileRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;

        public UpdateDriverProblemFileCommandHandler(IDriverProblemFileRepositoryAsync driverProblemFileRepository, IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _driverProblemFileRepository = driverProblemFileRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(UpdateDriverProblemFileCommand request, CancellationToken cancellationToken)
        {
            var driverProblemFile = (await _driverProblemFileRepository.FindByCondition(x => x.Id == request.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            if (driverProblemFile == null)
            {
                throw new ApiException($"DriverProblemFile Not Found.");
            }
            else
            {
                //driverProblemFile.DriverProblemId = request.DriverProblemId;
                driverProblemFile.FileName = request.FileName;
                driverProblemFile.ContentType = request.ContentType;
                driverProblemFile.FilePath = request.FilePath;
                driverProblemFile.FileEXT = request.FileEXT;
                driverProblemFile.DirectoryPath = request.DirectoryPath;
                driverProblemFile.Sequence = request.Sequence;
                driverProblemFile.Modified = DateTime.UtcNow;
                await _driverProblemFileRepository.UpdateAsync(driverProblemFile);
                var log = new CreateAppLog(_applicationLogRepository);
                log.Create("Driver", "Driver Problemfile", "Edit", Newtonsoft.Json.JsonConvert.SerializeObject(request));
                return new Response<int>(driverProblemFile.Id);
            }
        }
    }
}
