﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using iTrans.Transportation.Application.Interfaces.Repositories;
using FluentValidation;

namespace iTrans.Transportation.Application.Features.DriverProblemFiles.Queries
{
    public class GetDriverProblemFileByIdQueryValidator : AbstractValidator<GetDriverProblemFileByIdQuery>
    {
        private readonly IDriverProblemFileRepositoryAsync driverProblemFileRepository;

        public GetDriverProblemFileByIdQueryValidator(IDriverProblemFileRepositoryAsync driverProblemFileRepository)
        {
            this.driverProblemFileRepository = driverProblemFileRepository;
            RuleFor(p => p.Id)
               .NotEmpty().WithMessage("{PropertyName} is required.")
               .NotNull()
               .MustAsync(IsDriverProblemFileExists).WithMessage("{PropertyName} not exists.");
        }

        private async Task<bool> IsDriverProblemFileExists(int Id, CancellationToken cancellationToken)
        {
            var userObject = (await driverProblemFileRepository.FindByCondition(x => x.Id.Equals(Id)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            if (userObject != null)
            {
                return true;
            }
            return false;
        }
    }
}
