﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.DriverProblemFile;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.DriverProblemFiles.Queries
{
   public class GetDriverProblemFileByIdQuery : IRequest<Response<DriverProblemFileViewModel>>
    {
        public int Id { get; set; }
    }
    public class GetDriverProblemFileByIdQueryHandler : IRequestHandler<GetDriverProblemFileByIdQuery, Response<DriverProblemFileViewModel>>
    {
        private readonly IDriverProblemFileRepositoryAsync _driverProblemFileRepository;
        private readonly IMapper _mapper;
        public GetDriverProblemFileByIdQueryHandler(IDriverProblemFileRepositoryAsync driverProblemFileRepository, IMapper mapper)
        {
            _driverProblemFileRepository = driverProblemFileRepository;
            _mapper = mapper;
        }
        public async Task<Response<DriverProblemFileViewModel>> Handle(GetDriverProblemFileByIdQuery request, CancellationToken cancellationToken)
        {
            var driverProblemFileObject = (await _driverProblemFileRepository.FindByCondition(x => x.Id.Equals(request.Id)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            return new Response<DriverProblemFileViewModel>(_mapper.Map<DriverProblemFileViewModel>(driverProblemFileObject));
        }
    }
}
