﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.DriverProblemFile;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.DriverProblemFiles.Queries
{
    public class GetAllDriverProblemFileQuery : IRequest<Response<IEnumerable<DriverProblemFileViewModel>>>
    {
         
    }
    public class GetAllDriverProblemFileQueryHandler : IRequestHandler<GetAllDriverProblemFileQuery, Response<IEnumerable<DriverProblemFileViewModel>>>
    {
        private readonly IDriverProblemFileRepositoryAsync _driverProblemFileRepository;
        private readonly IMapper _mapper;
        public GetAllDriverProblemFileQueryHandler(IDriverProblemFileRepositoryAsync driverProblemFileRepository, IMapper mapper)
        {
            _driverProblemFileRepository = driverProblemFileRepository;
            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<DriverProblemFileViewModel>>> Handle(GetAllDriverProblemFileQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var driver = await _driverProblemFileRepository.GetAllAsync();
                var driverProblemFileViewModel = _mapper.Map<IEnumerable<DriverProblemFileViewModel>>(driver);
                return new Response<IEnumerable<DriverProblemFileViewModel>>(driverProblemFileViewModel);
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
    }
}
