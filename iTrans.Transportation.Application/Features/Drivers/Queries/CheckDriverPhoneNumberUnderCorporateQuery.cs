﻿using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.Drivers.Queries
{
    public class CheckDriverPhoneNumberUnderCorporateQuery : IRequest<Response<Guid>>
    {
        public string PhoneCode { get; set; }
        public string PhoneNumber { get; set; }
    }
    public class CheckDriverPhoneNumberUnderCorporateQueryHandler : IRequestHandler<CheckDriverPhoneNumberUnderCorporateQuery, Response<Guid>>
    {
        private readonly IDriverRepositoryAsync _driverRepository;
        private readonly IMapper _mapper;

        public CheckDriverPhoneNumberUnderCorporateQueryHandler(IDriverRepositoryAsync driverdRepository, IMapper mapper)
        {
            _driverRepository = driverdRepository;
            _mapper = mapper;
        }

        public async Task<Response<Guid>> Handle(CheckDriverPhoneNumberUnderCorporateQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var corporate = (await _driverRepository.FindByCondition(x => x.DriverType == "corporate" &&  x.PhoneNumber == request.PhoneNumber && x.PhoneCode == request.PhoneCode).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (corporate == null)
                {
                    return new Response<Guid>(null);
                }
                else
                {
                    return new Response<Guid>(corporate.Id);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
