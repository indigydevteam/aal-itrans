﻿using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.Drivers.Queries
{
    public class CheckDriverPhoneNumberQuery : IRequest<Response<bool>>
    {
        public Guid Id { get; set; }
        public string IdentityNumber { get; set; }
        public string PhoneCode { get; set; }
        public string PhoneNumber { get; set; }
    }
    public class CheckDriverPhoneNumberQueryHandler : IRequestHandler<CheckDriverPhoneNumberQuery, Response<bool>>
    {
        private readonly IDriverRepositoryAsync _driverRepository;
        private readonly IMapper _mapper;

        public CheckDriverPhoneNumberQueryHandler(IDriverRepositoryAsync driverdRepository, IMapper mapper)
        {
            _driverRepository = driverdRepository;
            _mapper = mapper;
        }

        public async Task<Response<bool>> Handle(CheckDriverPhoneNumberQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var driver = (await _driverRepository.FindByCondition(x => x.Id.Equals(request.Id) && x .IdentityNumber == request.IdentityNumber &&  x.PhoneNumber == request.PhoneNumber && x.PhoneCode == request.PhoneCode).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (driver == null)
                {
                    return new Response<bool>(false);
                }
                else
                {
                    return new Response<bool>(true);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
