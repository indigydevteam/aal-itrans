﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using iTrans.Transportation.Application.Helper;
using System.Text.RegularExpressions;
using Microsoft.Extensions.Configuration;

namespace iTrans.Transportation.Application.Features.Drivers.Queries
{
    public class GetDriverInformationByPhoneNumberQueryValidator : AbstractValidator<GetDriverInformationByPhoneNumberQuery>
    {
        private readonly IDriverRepositoryAsync driverRepository;
        private readonly IConfiguration _configuration;

        public GetDriverInformationByPhoneNumberQueryValidator( IDriverRepositoryAsync driverRepository, IConfiguration configuration)
        {
            this.driverRepository = driverRepository;
            _configuration = configuration;

            RuleFor(p => p.phoneNumber)
                .NotEmpty().WithMessage("{PropertyName} is required.")
                .NotNull()
                .MustAsync(IsExistPhoneNumber).WithMessage("{PropertyName} is not exists.");

 
        }
        private async Task<bool> IsExistPhoneNumber(string phoneNumber, CancellationToken cancellationToken)
        {
            var driverObject = (await driverRepository.FindByCondition(x => x.PhoneNumber.Trim().ToLower() == phoneNumber.Trim().ToLower() && !x.IsDelete).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            if (driverObject == null)
            {
                return false;
            }
            return true;

        }
    }
}
