﻿using AutoMapper;
using iTrans.Transportation.Application.DTOs.RegisterInformation;
using iTrans.Transportation.Application.DTOs.TermAndCondition;
using iTrans.Transportation.Application.DTOs.UserLevel;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.Drivers.Queries
{
    public class CheckDriverTermAndCondition : IRequest<Response<IEnumerable<UserTermAndConditionViewModel>>>
    {
        public Guid Id { set; get; }
    }
    public class CheckDriverTermAndConditionHandler : IRequestHandler<CheckDriverTermAndCondition, Response<IEnumerable<UserTermAndConditionViewModel>>>
    {
        private readonly IDriverTermAndConditionRepositoryAsync _driverTermAndConditionRepository;
        private readonly ITermAndConditionRepositoryAsync _termAndConditionRepository;
        private readonly IMapper _mapper;
        public CheckDriverTermAndConditionHandler(ITermAndConditionRepositoryAsync termAndConditionRepository, IDriverTermAndConditionRepositoryAsync driverTermAndConditionRepository, IMapper mapper)
        {
            _driverTermAndConditionRepository = driverTermAndConditionRepository;
            _termAndConditionRepository = termAndConditionRepository;
            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<UserTermAndConditionViewModel>>> Handle(CheckDriverTermAndCondition request, CancellationToken cancellationToken)
        {
            try
            {
                var data = (await _termAndConditionRepository.FindByCondition(x => x.Active == true && x.Module.Trim().ToLower() == "driver").ConfigureAwait(false)).AsQueryable().ToList();
                List<TermAndCondition> termAndConditions = new List<TermAndCondition>();
                foreach (TermAndCondition termAndCondition in data)
                {
                    var existItem = termAndConditions.Where(x => x.Section == termAndCondition.Section).FirstOrDefault();
                    if (existItem == null)
                    {
                        termAndConditions.Add(termAndCondition);
                    }
                    else
                    {
                        int n;
                        var version1 = new Version(int.TryParse(termAndCondition.Version, out n) ? termAndCondition.Version + ".0" : termAndCondition.Version);
                        var version2 = new Version(int.TryParse(existItem.Version, out n) ? existItem.Version + ".0" : existItem.Version);
                        var result = version1.CompareTo(version2);
                        if (result > 0)
                        {
                            termAndConditions.Remove(existItem);
                            termAndConditions.Add(termAndCondition);
                        }
                    }
                }

                var driverTermAndCondition = (await _driverTermAndConditionRepository.FindByCondition(x => x.Driver.Id.Equals(request.Id)).ConfigureAwait(false)).AsQueryable().ToList();

                List<UserTermAndConditionViewModel> results = new List<UserTermAndConditionViewModel>();
                foreach (TermAndCondition item in termAndConditions)
                {
                    UserTermAndConditionViewModel termAndConditionViewModel = _mapper.Map<UserTermAndConditionViewModel>(item);
                    var existTermAndCondition = driverTermAndCondition.Where(x => x.TermAndConditionId == item.Id  && x.Version == item.Version).FirstOrDefault();
                    if (existTermAndCondition == null)
                    {
                        termAndConditionViewModel.isUserAccept = false;
                    }
                    else
                    {
                        termAndConditionViewModel.isUserAccept = true;
                    }
                    results.Add(termAndConditionViewModel);
                }

                var TermAndConditionViewModel = _mapper.Map<IEnumerable<UserTermAndConditionViewModel>>(results.OrderBy(x => x.Section).OrderBy(x => x.Sequence));
                return new Response<IEnumerable<UserTermAndConditionViewModel>>(TermAndConditionViewModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
