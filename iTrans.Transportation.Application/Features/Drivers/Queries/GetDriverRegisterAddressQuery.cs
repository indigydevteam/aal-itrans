﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.DriverAddress;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.DriverAddresses.Queries
{
    public class GetDriverRegisterAddressQuery : IRequest<Response<DriverAddressViewModel>>
    {
        public Guid DriverId { get; set; }
    }
    public class GetDriverRegisterAddressQueryHandler : IRequestHandler<GetDriverRegisterAddressQuery, Response<DriverAddressViewModel>>
    {
        private readonly IDriverAddressRepositoryAsync _driverAddressRepository;
        private readonly IMapper _mapper;
        public GetDriverRegisterAddressQueryHandler(IDriverAddressRepositoryAsync driverAddressRepository, IMapper mapper)
        {
            _driverAddressRepository = driverAddressRepository;
            _mapper = mapper;
        }
        public async Task<Response<DriverAddressViewModel>> Handle(GetDriverRegisterAddressQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var driverObject = (await _driverAddressRepository.FindByCondition(x => x.DriverId.Equals(request.DriverId) && x.AddressType == "register").ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                return new Response<DriverAddressViewModel>(_mapper.Map<DriverAddressViewModel>(driverObject));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
