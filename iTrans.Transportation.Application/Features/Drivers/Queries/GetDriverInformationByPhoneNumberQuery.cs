﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.Driver;
using iTrans.Transportation.Application.DTOs.DriverCar;
using iTrans.Transportation.Application.DTOs.UserLevel;
using iTrans.Transportation.Application.DTOs.VerifyStatus;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.Drivers.Queries
{
    public class GetDriverInformationByPhoneNumberQuery : IRequest<Response<DriverInformationViewModel>>
    {
        public string  phoneNumber { get; set; }
    }
    public class GetDriverInformationByPhoneNumberQueryHandler : IRequestHandler<GetDriverInformationByPhoneNumberQuery, Response<DriverInformationViewModel>>
    {
        private readonly IDriverRepositoryAsync _driverRepository;
        private readonly IUserLevelRepositoryAsync _userLevelRepository;
        private readonly IMapper _mapper;
        public GetDriverInformationByPhoneNumberQueryHandler(IDriverRepositoryAsync driverRepository, IUserLevelRepositoryAsync userLevelRepository, IMapper mapper)
        {
            _driverRepository = driverRepository;
            _userLevelRepository = userLevelRepository;
            _mapper = mapper;
        }
        public async Task<Response<DriverInformationViewModel>> Handle(GetDriverInformationByPhoneNumberQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var driverObject = (await _driverRepository.FindByCondition(x => x.PhoneNumber.Trim().ToLower() == request.phoneNumber.Trim().ToLower() && !x.IsDelete).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (driverObject == null)
                {
                    throw new ApiException($"Driver Not Found.");
                }
                var userLevels = (await _userLevelRepository.FindByCondition(x => x.Module == "driver" && x.Active == true).ConfigureAwait(false)).AsQueryable().OrderBy(x => x.Level).ToList();
                DriverInformationViewModel driverInformationObject = _mapper.Map<DriverInformationViewModel>(driverObject);
                driverInformationObject.haveCorparate = false;
                if (driverInformationObject != null)
                {
                    driverInformationObject.files.RemoveAll(f => f.IsApprove == false);
                    var folderDetails = Path.Combine(Directory.GetCurrentDirectory(), $"Shared\\VerifyStatus.json");
                    var statusJson = System.IO.File.ReadAllText(folderDetails);
                    List<VerifyStatusViewModel> verifyStatus = Newtonsoft.Json.JsonConvert.DeserializeObject<List<VerifyStatusViewModel>>(statusJson);

                    driverInformationObject.verifyStatusObj = verifyStatus.Where(o => o.id == driverInformationObject.verifyStatus).FirstOrDefault();

                    driverInformationObject.driverLevel = _mapper.Map<UserLevelViewModel>(userLevels.Where(x => x.Level.Equals(driverInformationObject.level)).FirstOrDefault());
                    driverInformationObject.nextLevel = _mapper.Map<UserLevelViewModel>(userLevels.Where(x => x.Level.Equals(driverInformationObject.level + 1)).FirstOrDefault());
                    if (driverInformationObject.nextLevel != null)
                    {
                        driverInformationObject.nextLevel.star = driverInformationObject.nextLevel.star - driverInformationObject.star;
                    }
                    if (driverObject.Corparate != null)
                    {
                       var corparateInformation =  ((await _driverRepository.FindByCondition(x => x.Id.Equals(driverObject.Corparate)).ConfigureAwait(false)).AsQueryable().FirstOrDefault());
                        driverInformationObject.corparateInformation = _mapper.Map<DriverCorparateInformationViewModel>(corparateInformation);
                        if (driverInformationObject.corparateInformation != null)
                        {
                            driverInformationObject.haveCorparate = true;

                            if (driverInformationObject.corparateInformation.phoneCode == "+66" && driverInformationObject.corparateInformation.phoneNumber != null
                                && driverInformationObject.corparateInformation.phoneNumber.Trim() != "")
                            {
                                driverInformationObject.corparateInformation.phoneNumber = "0" + driverInformationObject.corparateInformation.phoneNumber;
                            }
                        }
                    }
                    if (driverInformationObject.phoneCode == "+66" && driverInformationObject.phoneNumber != null && driverInformationObject.phoneNumber.Trim() != "")
                    {
                        driverInformationObject.phoneNumber = "0" + driverInformationObject.phoneNumber;
                    }
                    foreach (DriverCarViewModel car in driverInformationObject.cars)
                    {
                        if (car.driverPhoneCode == "+66" && car.driverPhoneNumber != null && car.driverPhoneNumber.Trim() != "")
                        {
                            car.driverPhoneNumber = "0" + car.driverPhoneNumber;
                        }
                    }
                }
                return new Response<DriverInformationViewModel>(_mapper.Map<DriverInformationViewModel>(driverInformationObject));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
