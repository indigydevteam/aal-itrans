﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.Driver;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;


namespace iTrans.Transportation.Application.Features.Drivers.Queries
{
    public class GetAllDriverQuery : IRequest<Response<IEnumerable<DriverViewModel>>>
    {
       
    }
    public class GetAllDriverQueryHandler : IRequestHandler<GetAllDriverQuery, Response<IEnumerable<DriverViewModel>>>
    {
        private readonly IDriverRepositoryAsync _driverRepository;
        private readonly IMapper _mapper;
        public GetAllDriverQueryHandler(IDriverRepositoryAsync driverRepository, IMapper mapper)
        {
            _driverRepository = driverRepository;
            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<DriverViewModel>>> Handle(GetAllDriverQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var driver = await _driverRepository.GetAllAsync();
                var driverViewModel = _mapper.Map<IEnumerable<DriverViewModel>>(driver);
                return new Response<IEnumerable<DriverViewModel>>(driverViewModel);
            }catch(Exception ex)
            {
                throw ex;
            }
        }
    }
}
