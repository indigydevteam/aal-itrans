﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.Driver;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.Drivers.Queries
{
    public class GetDriverByIdQuery : IRequest<Response<DriverViewModel>>
    {
        public Guid Id { get; set; }
    }
    public class GetDriverByIdQueryHandler : IRequestHandler<GetDriverByIdQuery, Response<DriverViewModel>>
    {
        private readonly IDriverRepositoryAsync _driverRepository;
        private readonly IMapper _mapper;
        public GetDriverByIdQueryHandler(IDriverRepositoryAsync driverRepository, IMapper mapper)
        {
            _driverRepository = driverRepository;
            _mapper = mapper;
        }
        public async Task<Response<DriverViewModel>> Handle(GetDriverByIdQuery request, CancellationToken cancellationToken)
        {
            var driverObject = (await _driverRepository.FindByCondition(x => x.Id.Equals(request.Id)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            return new Response<DriverViewModel>(_mapper.Map<DriverViewModel>(driverObject));
        }
    }
}
