﻿using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.Drivers.Queries
{
    public class CheckExistDriverIdentityNumberQuery : IRequest<Response<bool>>
    {
        public string IdentityNumber { get; set; }
    }
    public class CheckExistDriverIdentityNumberQueryHandler : IRequestHandler<CheckExistDriverIdentityNumberQuery, Response<bool>>
    {
        private readonly IDriverRepositoryAsync _driverRepository;
        private readonly IMapper _mapper;

        public CheckExistDriverIdentityNumberQueryHandler(IDriverRepositoryAsync customerdRepository, IMapper mapper)
        {
            _driverRepository = customerdRepository;
            _mapper = mapper;
        }

        public async Task<Response<bool>> Handle(CheckExistDriverIdentityNumberQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var customer = (await _driverRepository.FindByCondition(x => x.IdentityNumber == request.IdentityNumber).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (customer == null)
                {
                    return new Response<bool>(false);
                }
                else
                {
                    return new Response<bool>(true);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
