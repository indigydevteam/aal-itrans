﻿using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Helper;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.Drivers.Queries
{
    public class VerifyDriverPasswordQuery : IRequest<Response<bool>>
    {
        public Guid Id { get; set; }
        public string Password { get; set; }
    }
    public class VerifyDriverPasswordQueryHandler : IRequestHandler<VerifyDriverPasswordQuery, Response<bool>>
    {
        private readonly IDriverRepositoryAsync _driverRepository;
        private readonly ILoginErrorLogRepositoryAsync _loginErrorLogRepository;
        private readonly IConfiguration _configuration;
        private readonly IMapper _mapper;

        public VerifyDriverPasswordQueryHandler(IDriverRepositoryAsync driverdRepository, ILoginErrorLogRepositoryAsync loginErrorLogRepository, IConfiguration configuration, IMapper mapper)
        {
            _driverRepository = driverdRepository;
            _loginErrorLogRepository = loginErrorLogRepository;
            _configuration = configuration;
            _mapper = mapper;
        }

        public async Task<Response<bool>> Handle(VerifyDriverPasswordQuery request, CancellationToken cancellationToken)
        {
            try
            {
                bool result = false;
                string username = "";
                var driver = (await _driverRepository.FindByCondition(x => x.Id.Equals(request.Id)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (driver != null)
                {
                    username = driver.PhoneNumber;
                    if (request.Password != null && request.Password.Trim() != "" && driver.Password == request.Password.Trim())
                    {
                        result = true;
                    }
                }
                LoginErrorLogHelper loginErrorLog = new LoginErrorLogHelper();
                var IsAccountLocked = loginErrorLog.IsAccountLocked(username, _loginErrorLogRepository);
                IsAccountLocked.Wait();

                if (result && !IsAccountLocked.Result)
                {
                    loginErrorLog.ClearIncorrect(username, _loginErrorLogRepository);
                    return new Response<bool>(result);
                }
                else
                {
                    var msg = loginErrorLog.UpdateIncorrect(username, _loginErrorLogRepository, _configuration);
                    msg.Wait();
                    return new Response<bool>(result, msg.Result.Item1, msg.Result.Item2);
                }

            }
            catch (Exception ex)
            {
                return new Response<bool>(false, "รหัสผ่านไม่ถูกต้อง", "Incorrect password.");
            }
        }
    }
}
