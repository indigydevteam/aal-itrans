﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.Driver;
using iTrans.Transportation.Application.DTOs.DriverCar;
using iTrans.Transportation.Application.DTOs.DriverTermAndCondition;
using iTrans.Transportation.Application.DTOs.TermAndCondition;
using iTrans.Transportation.Application.DTOs.UserLevel;
using iTrans.Transportation.Application.DTOs.VerifyStatus;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using iTrans.Transportation.Domain.Entities;
using MediatR;

namespace iTrans.Transportation.Application.Features.Drivers.Queries
{
    public class GetDriverInformationByAuthQuery : IRequest<Response<DriverInformationViewModel>>
    {
        public Guid Id { get; set; }
    }
    public class GetDriverInformationByAuthQueryHandler : IRequestHandler<GetDriverInformationByAuthQuery, Response<DriverInformationViewModel>>
    {
        private readonly IDriverRepositoryAsync _driverRepository;
        private readonly IUserLevelRepositoryAsync _userLevelRepository;
        private readonly IDriverTermAndConditionRepositoryAsync _driverTermAndConditionRepository;
        private readonly ITermAndConditionRepositoryAsync _termAndConditionRepository;
        private readonly IMapper _mapper;
        public GetDriverInformationByAuthQueryHandler(IDriverRepositoryAsync driverRepository, IUserLevelRepositoryAsync userLevelRepository
            , IDriverTermAndConditionRepositoryAsync driverTermAndConditionRepository, ITermAndConditionRepositoryAsync termAndConditionRepository, IMapper mapper)
        {
            _driverRepository = driverRepository;
            _userLevelRepository = userLevelRepository;
            _driverTermAndConditionRepository = driverTermAndConditionRepository;
            _termAndConditionRepository = termAndConditionRepository;
            _mapper = mapper;
        }
        public async Task<Response<DriverInformationViewModel>> Handle(GetDriverInformationByAuthQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var driverObject = (await _driverRepository.FindByCondition(x => x.Id.Equals(request.Id) && !x.IsDelete).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                var userLevels = (await _userLevelRepository.FindByCondition(x => x.Module == "driver" && x.Active == true).ConfigureAwait(false)).AsQueryable().OrderBy(x => x.Level).ToList();
                DriverInformationViewModel driverInformationObject = _mapper.Map<DriverInformationViewModel>(driverObject);
                driverInformationObject.haveCorparate = false;
                if (driverInformationObject != null)
                {
                    #region term and con
                    var data = (await _termAndConditionRepository.FindByCondition(x => x.Active == true && x.Module.Trim().ToLower() == "driver").ConfigureAwait(false)).AsQueryable().ToList().OrderBy(x => x.Sequence).OrderBy(x => x.Version).OrderBy(x => x.Modified);
                    List<TermAndCondition> termAndConditions = new List<TermAndCondition>();
                    foreach (TermAndCondition termAndCondition in data)
                    {
                        var existItem = termAndConditions.Where(x => x.Section == termAndCondition.Section).FirstOrDefault();
                        if (existItem == null)
                        {
                            termAndConditions.Add(termAndCondition);
                        }
                        else
                        {
                            int n;
                            var version1 = new Version(int.TryParse(termAndCondition.Version, out n) ? termAndCondition.Version + ".0" : termAndCondition.Version);
                            var version2 = new Version(int.TryParse(existItem.Version, out n) ? existItem.Version + ".0" : existItem.Version);
                            var result = version1.CompareTo(version2);
                            if (result > 0)
                            {
                                termAndConditions.Remove(existItem);
                                termAndConditions.Add(termAndCondition);
                            }
                        }
                    };

                    List<DriverTermAndConditionViewModel> driverTermAndConditions = new List<DriverTermAndConditionViewModel>();
                    foreach (TermAndCondition item in termAndConditions)
                    {
                        var driverTermAndCondition = (await _driverTermAndConditionRepository.FindByCondition(x => x.Driver.Id.Equals(request.Id) && x.Section == item.Section && x.Version == item.Version)
                            .ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                        if (driverTermAndCondition == null)
                        {
                            DriverTermAndConditionViewModel itemTermAndCondition = new DriverTermAndConditionViewModel()
                            {
                                id = 0,
                                termAndConditionId = item.Id,
                                name_TH = item.Name_TH,
                                name_ENG = item.Name_ENG,
                                section = item.Section,
                                version = item.Version,
                                isAccept = item.IsAccept,
                                isUserAccept = false,
                                isComplete = false
                            };
                            driverTermAndConditions.Add(itemTermAndCondition);
                        }
                        else
                        {
                            var itemTermAndCondition = _mapper.Map<DriverTermAndConditionViewModel>(driverTermAndCondition);
                            itemTermAndCondition.isComplete = true;
                            driverTermAndConditions.Add(itemTermAndCondition);
                        }
                    }
                    driverInformationObject.termAndConditions = driverTermAndConditions;
                    #endregion #region term and con

                    if (driverInformationObject.files != null)
                    {
                        driverInformationObject.files.RemoveAll(f => f.IsDelete);
                    }
                    var folderDetails = Path.Combine(Directory.GetCurrentDirectory(), $"Shared\\VerifyStatus.json");
                    var statusJson = System.IO.File.ReadAllText(folderDetails);
                    List<VerifyStatusViewModel> verifyStatus = Newtonsoft.Json.JsonConvert.DeserializeObject<List<VerifyStatusViewModel>>(statusJson);

                    driverInformationObject.verifyStatusObj = verifyStatus.Where(o => o.id == driverInformationObject.verifyStatus).FirstOrDefault();

                    driverInformationObject.driverLevel = _mapper.Map<UserLevelViewModel>(userLevels.Where(x => x.Level.Equals(driverInformationObject.level)).FirstOrDefault());
                    driverInformationObject.nextLevel = _mapper.Map<UserLevelViewModel>(userLevels.Where(x => x.Level.Equals(driverInformationObject.level + 1)).FirstOrDefault());
                    if (driverInformationObject.nextLevel != null)
                    {
                        driverInformationObject.nextLevel.star = driverInformationObject.nextLevel.star - driverInformationObject.star;
                    }
                    if (driverObject.Corparate != null)
                    {
                       var corparateInformation =  ((await _driverRepository.FindByCondition(x => x.Id.Equals(driverObject.Corparate)).ConfigureAwait(false)).AsQueryable().FirstOrDefault());
                        driverInformationObject.corparateInformation = _mapper.Map<DriverCorparateInformationViewModel>(corparateInformation);
                        if (driverInformationObject.corparateInformation != null)
                        {
                            driverInformationObject.haveCorparate = true;

                            if (driverInformationObject.corparateInformation.phoneCode == "+66" && driverInformationObject.corparateInformation.phoneNumber != null
                                && driverInformationObject.corparateInformation.phoneNumber.Trim() != "")
                            {
                                driverInformationObject.corparateInformation.phoneNumber = "0" + driverInformationObject.corparateInformation.phoneNumber;
                            }
                        }
                    }
                    if (driverInformationObject.phoneCode == "+66" && driverInformationObject.phoneNumber != null && driverInformationObject.phoneNumber.Trim() != "")
                    {
                        driverInformationObject.phoneNumber = "0" + driverInformationObject.phoneNumber;
                    }
                    foreach (DriverCarViewModel car in driverInformationObject.cars)
                    {
                        if (car.driverPhoneCode == "+66" && car.driverPhoneNumber != null && car.driverPhoneNumber.Trim() != "")
                        {
                            car.driverPhoneNumber = "0" + car.driverPhoneNumber;
                        }
                    }
                } 
                return new Response<DriverInformationViewModel>(driverInformationObject);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
