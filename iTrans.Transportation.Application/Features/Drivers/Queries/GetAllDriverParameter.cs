﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.Driver;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;

namespace iTrans.Transportation.Application.Features.Drivers.Queries
{
    public class GetAllDriverParameter : IRequest<PagedResponse<IEnumerable<DriverViewModel>>>
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public string Search { get; set; }
    }
    public class GetAllDriverParameterHandler : IRequestHandler<GetAllDriverParameter, PagedResponse<IEnumerable<DriverViewModel>>>
    {
        private readonly IDriverRepositoryAsync _driverRepository;
        private readonly IMapper _mapper;
        private Expression<Func<Driver, bool>> expression;

        public GetAllDriverParameterHandler(IDriverRepositoryAsync driverRepository, IMapper mapper)
        {
            _driverRepository = driverRepository;
            _mapper = mapper;
        }

        public async Task<PagedResponse<IEnumerable<DriverViewModel>>> Handle(GetAllDriverParameter request, CancellationToken cancellationToken)
        {
            var validFilter = _mapper.Map<GetAllDriverParameter>(request);
            if (validFilter.Search != null && validFilter.Search.Trim() != "")
            {
                expression = x => (x.FirstName.Contains(validFilter.Search.Trim()) || x.LastName.Contains(validFilter.Search.Trim()) || x.Name.Contains(validFilter.Search.Trim()));
            }
            int itemCount = _driverRepository.GetItemCount(expression);
            //Expression<Func<Driver, bool>> expression = x => x.Name != "";
            var driver = await _driverRepository.FindByConditionWithPage(expression, validFilter.PageNumber, validFilter.PageSize);
            var driverViewModel = _mapper.Map<IEnumerable<DriverViewModel>>(driver);
            return new PagedResponse<IEnumerable<DriverViewModel>>(driverViewModel, validFilter.PageNumber, validFilter.PageSize, itemCount);
        }
    }
}
