﻿using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.Drivers.Queries
{
    public class ConfirmDriverPasswordQuery : IRequest<Response<bool>>
    {
        public Guid Id { get; set; }
        public string Password { get; set; }
    }
    public class ConfirmDriverPasswordQueryHandler : IRequestHandler<ConfirmDriverPasswordQuery, Response<bool>>
    {
        private readonly IDriverRepositoryAsync _driverRepository;
        private readonly IMapper _mapper;

        public ConfirmDriverPasswordQueryHandler(IDriverRepositoryAsync driverdRepository,  IMapper mapper)
        {
            _driverRepository = driverdRepository;
            _mapper = mapper;
        }

        public async Task<Response<bool>> Handle(ConfirmDriverPasswordQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var driver = (await _driverRepository.FindByCondition(x => x.Id == request.Id && x.Password == request.Password).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (driver == null)
                {
                    return new Response<bool>(false);
                }
                else
                {
                    return new Response<bool>(true);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
