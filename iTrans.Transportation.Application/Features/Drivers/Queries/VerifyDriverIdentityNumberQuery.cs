﻿using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Helper;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.Drivers.Queries
{
    public class VerifyDriverIdentityNumberQuery : IRequest<Response<bool>>
    {
        public Guid Id { get; set; }
        public string IdentityNumber { get; set; }
    }
    public class VerifyDriverIdentityNumberQueryHandler : IRequestHandler<VerifyDriverIdentityNumberQuery, Response<bool>>
    {
        private readonly IDriverRepositoryAsync _driverRepository;
        private readonly IConfiguration _configuration;
        private readonly IMapper _mapper;

        public VerifyDriverIdentityNumberQueryHandler(IDriverRepositoryAsync driverRepository, IMapper mapper, IConfiguration configuration)
        {
            _driverRepository = driverRepository;
            _configuration = configuration;
            _mapper = mapper;
        }

        public async Task<Response<bool>> Handle(VerifyDriverIdentityNumberQuery request, CancellationToken cancellationToken)
        {
            try
            {
                bool result = true;
                var isDevelopment = _configuration.GetSection("IsDevelopment");

                if (request.IdentityNumber != null && request.IdentityNumber != "")
                {
                    string identityNumber = AESMgr.Decrypt(request.IdentityNumber);

                    Regex rexPersonal = new Regex(@"^[0-9]{13}$");
                    if (rexPersonal.IsMatch(identityNumber))
                    {
                        int sum = 0;

                        for (int i = 0; i < 12; i++)
                        {
                            sum += int.Parse(identityNumber[i].ToString()) * (13 - i);
                        }

                        result = (int.Parse(identityNumber[12].ToString()) == ((11 - (sum % 11)) % 10));

                    }
                    if (isDevelopment != null && isDevelopment.Value == "true")
                    {
                        result = true;
                    }
                    if (result)
                    {
                        var driver = (await _driverRepository.FindByCondition(x => x.IdentityNumber == request.IdentityNumber && x.Id.Equals(request.Id) && x.IsDelete == false).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                        //var driver = (await _driverRepository.FindByCondition(x => x.IdentityNumber == request.IdentityNumber).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                        if (driver == null)
                        {
                            result = false;
                        }
                    }
                }
                if (result)
                {
                    return new Response<bool>(result);
                }
                else
                {
                    return new Response<bool>(result, "เลขที่บัตรประชาชนหรือเลขที่นิติบุคคลไม่ถูกต้อง", "Invalid IDcard or CorperateId number.");
                }
            }
            catch (Exception ex)
            {
                return new Response<bool>(false, "เลขที่บัตรประชาชนหรือเลขที่นิติบุคคลไม่ถูกต้อง", "Invalid IDcard or CorperateId number.");
            }
        }
    }
}
