﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using iTrans.Transportation.Application.Interfaces.Repositories;
using FluentValidation;

namespace iTrans.Transportation.Application.Features.Drivers.Queries
{
    public class GetDriverInformationByAuthQueryValidator : AbstractValidator<GetDriverInformationByAuthQuery>
    {
        private readonly IDriverRepositoryAsync driverRepository;

        public GetDriverInformationByAuthQueryValidator(IDriverRepositoryAsync driverRepository)
        {
            this.driverRepository = driverRepository;
            RuleFor(p => p.Id)
               .NotEmpty().WithMessage("{PropertyName} is required.")
               .NotNull()
               .MustAsync(IsDriverExists).WithMessage("{PropertyName} not exists.");
        }

        private async Task<bool> IsDriverExists(Guid Id, CancellationToken cancellationToken)
        {
             
                var userObject = (await driverRepository.FindByCondition(x => x.Id.Equals(Id) && !x.IsDelete).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (userObject != null)
                {
                    return true;
                }
                return false;
             
        }
    }
}
