﻿using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.Drivers.Queries
{
    public class CheckExistDriverPhoneNumberQuery : IRequest<Response<bool>>
    {
        public string phoneNumber { get; set; }
    }
    public class CheckExistDriverPhoneNumberQueryHandler : IRequestHandler<CheckExistDriverPhoneNumberQuery, Response<bool>>
    {
        private readonly IDriverRepositoryAsync _driverRepository;
        private readonly IMapper _mapper;

        public CheckExistDriverPhoneNumberQueryHandler(IDriverRepositoryAsync driverdRepository, IMapper mapper)
        {
            _driverRepository = driverdRepository;
            _mapper = mapper;
        }

        public async Task<Response<bool>> Handle(CheckExistDriverPhoneNumberQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var driver = (await _driverRepository.FindByCondition(x => x.PhoneNumber == request.phoneNumber).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (driver == null)
                {
                    return new Response<bool>(false);
                }
                else
                {
                    return new Response<bool>(true);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
