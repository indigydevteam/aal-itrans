﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.Driver;
using iTrans.Transportation.Application.DTOs.UserLevel;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.Drivers.Queries
{
    public class GetDriverInformationByIdQuery : IRequest<Response<DriverInformationViewModel>>
    {
        public Guid Id { get; set; }
    }
    public class GetDriverInformationByIdQueryHandler : IRequestHandler<GetDriverInformationByIdQuery, Response<DriverInformationViewModel>>
    {
        private readonly IDriverRepositoryAsync _driverRepository;
        private readonly IUserLevelRepositoryAsync _userLevelRepository;
        private readonly IMapper _mapper;
        public GetDriverInformationByIdQueryHandler(IDriverRepositoryAsync driverRepository, IUserLevelRepositoryAsync userLevelRepository, IMapper mapper)
        {
            _driverRepository = driverRepository;
            _userLevelRepository = userLevelRepository;
            _mapper = mapper;
        }
        public async Task<Response<DriverInformationViewModel>> Handle(GetDriverInformationByIdQuery request, CancellationToken cancellationToken)
        {
            var driverObject = (await _driverRepository.FindByCondition(x => x.Id.Equals(request.Id)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            DriverInformationViewModel customerInformationObject = _mapper.Map<DriverInformationViewModel>(driverObject);
            if (customerInformationObject != null)
                customerInformationObject.driverLevel = _mapper.Map<UserLevelViewModel>((await _userLevelRepository.FindByCondition(x => x.Level.Equals(customerInformationObject.level) && x.Active == true).ConfigureAwait(false)).AsQueryable().FirstOrDefault());
            return new Response<DriverInformationViewModel>(customerInformationObject);
        }
    }
}
