﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.Driver;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.Drivers.Queries
{
    public class GetDriverWithAddressByIdQuery : IRequest<Response<DriverWithAddressViewModel>>
    {
        public Guid Id { get; set; }
    }
    public class GetDriverWithAddressByIdQueryHandler : IRequestHandler<GetDriverWithAddressByIdQuery, Response<DriverWithAddressViewModel>>
    {
        private readonly IDriverRepositoryAsync _driverRepository;
        //private readonly IDriverAddressRepositoryAsync _driverAddressRepository;
        private readonly IMapper _mapper;
        public GetDriverWithAddressByIdQueryHandler(IDriverRepositoryAsync driverRepository, IMapper mapper)//, IDriverAddressRepositoryAsync driverAddressRepository, IMapper mapper)
        {
            _driverRepository = driverRepository;
            //_driverAddressRepository = driverAddressRepository;
            _mapper = mapper;
        }
        public async Task<Response<DriverWithAddressViewModel>> Handle(GetDriverWithAddressByIdQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var driverObject = (await _driverRepository.FindByCondition(x => x.Id.Equals(request.Id)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                //if (driverObject != null)
                    //driverObject.Addresses = (await _driverAddressRepository.FindByCondition(x => x.DriverId.Equals(driverObject.Id)).ConfigureAwait(false)).AsQueryable().ToList();
                return new Response<DriverWithAddressViewModel>(_mapper.Map<DriverWithAddressViewModel>(driverObject));
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
    }
}
