﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using iTrans.Transportation.Application.Interfaces.Repositories;
using FluentValidation;
namespace iTrans.Transportation.Application.Features.Drivers.Queries
{
    public class GetDriverWithArressByIdQueryValidator : AbstractValidator<GetDriverWithAddressByIdQuery>
    {
        private readonly IDriverRepositoryAsync driverRepository;

        public GetDriverWithArressByIdQueryValidator(IDriverRepositoryAsync driverRepository)
        {
            this.driverRepository = driverRepository;
            RuleFor(p => p.Id)
               .NotEmpty().WithMessage("{PropertyName} is required.")
               .NotNull()
               .MustAsync(IsDriverExists).WithMessage("{PropertyName} not exists.");
        }

        private async Task<bool> IsDriverExists(Guid DriverId, CancellationToken cancellationToken)
        {
            var userObject = (await driverRepository.FindByCondition(x => x.Id.Equals(DriverId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            if (userObject != null)
            {
                return true;
            }
            return false;
        }
    }
}
