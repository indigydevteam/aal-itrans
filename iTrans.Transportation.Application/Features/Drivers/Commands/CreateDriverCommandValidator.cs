﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.Drivers.Commands
{
    public class CreateDriverCommandValidator : AbstractValidator<CreateDriverCommand>
    {
        private readonly IDriverRepositoryAsync driverRepository;

        public CreateDriverCommandValidator(IDriverRepositoryAsync driverRepository)
        {
            this.driverRepository = driverRepository;

            RuleFor(p => p.PhoneNumber)
                .NotEmpty().WithMessage("{PropertyName} is required.")
                .NotNull()
                .MaximumLength(50).WithMessage("{PropertyName} must not exceed 50 characters.");
                

            RuleFor(p => p.Name)
                .NotEmpty().WithMessage("{PropertyName} is required.")
                .NotNull()
                .MaximumLength(50).WithMessage("{PropertyName} must not exceed 50 characters.");

            RuleFor(p => p.IdentityNumber)
               .NotEmpty().WithMessage("{PropertyName} is required.")
               .NotNull()
               .MaximumLength(50).WithMessage("{PropertyName} must not exceed 50 characters.")
               .MustAsync(IsUniqueIdentityNumber).WithMessage("{PropertyName} already exists.");
        }

        private async Task<bool> IsUniqueIdentityNumber(string identityNumber, CancellationToken cancellationToken)
        {
            var driverObject = (await driverRepository.FindByCondition(x => x.IdentityNumber.ToLower() == identityNumber.ToLower() && x.IsDelete == false).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            if (driverObject != null)
            {
                return false;
            }
            return true;
        }
    }
}
