﻿using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.Drivers.Commands
{
    public class DeleteDriverByIdCommand : IRequest<Response<Guid>>
    {
        public Guid Id { get; set; }
        public class DeleteDriverByIdCommandHandler : IRequestHandler<DeleteDriverByIdCommand, Response<Guid>>
        {
            private readonly IDriverRepositoryAsync _driverRepository;
            public DeleteDriverByIdCommandHandler(IDriverRepositoryAsync driverRepository)
            {
                _driverRepository = driverRepository;
            }
            public async Task<Response<Guid>> Handle(DeleteDriverByIdCommand command, CancellationToken cancellationToken)
            {
                var driver = (await _driverRepository.FindByCondition(x => x.Id == command.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (driver == null)
                {
                    throw new ApiException($"Driver Not Found.");
                }
                else
                {
                    await _driverRepository.DeleteAsync(driver);
                    return new Response<Guid>(driver.Id);
                }
            }
        }
    }
}
