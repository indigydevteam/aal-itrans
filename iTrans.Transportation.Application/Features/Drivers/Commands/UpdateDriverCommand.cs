﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;

namespace iTrans.Transportation.Application.Features.Drivers.Commands
{
    public class UpdateDriverCommand : IRequest<Response<Guid>>
    {
        public Guid? UserId { get; set; }
        public Guid Id { get; set; }
        public string Password { get; set; }
        public string DriverType { get; set; }
        public int RoleId { get; set; }
        public int CorporateTypeId { get; set; }
        public int Title { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Name { get; set; }
        public string IdentityNumber { get; set; }
        public int ContactPersonTitle { get; set; }
        public string ContactPersonFirstName { get; set; }
        public string ContactPersonMiddleName { get; set; }
        public string ContactPersonLastName { get; set; }
        public DateTime Birthday { get; set; }
        public string PhoneCode { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public int Level { get; set; }
        public string Facbook { get; set; }
        public string Line { get; set; }
        public string Twitter { get; set; }
        public string Whatapp { get; set; }
    }

    public class UpdateDriverCommandHandler : IRequestHandler<UpdateDriverCommand, Response<Guid>>
    {
        private readonly IDriverRepositoryAsync _driverRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;

        public UpdateDriverCommandHandler(IDriverRepositoryAsync driverRepository, IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _driverRepository = driverRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
        }

        public async Task<Response<Guid>> Handle(UpdateDriverCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var driver = (await _driverRepository.FindByCondition(x => x.Id == request.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (driver == null)
                {
                    throw new ApiException($"Driver Not Found.");
                }
                else
                {
                    driver.Password = request.Password;
                    driver.DriverType = request.DriverType;
                    //driver.RoleId = request.RoleId;
                    //driver.CorporateTypeId = request.CorporateTypeId;
                    //driver.Title = request.Title;
                    driver.FirstName = request.FirstName;
                    driver.MiddleName = request.MiddleName;
                    driver.LastName = request.LastName;
                    driver.Name = request.Name;
                    driver.IdentityNumber = request.IdentityNumber;
                    //driver.ContactPersonTitle = request.ContactPersonTitle;
                    driver.ContactPersonFirstName = request.ContactPersonFirstName;
                    driver.ContactPersonMiddleName = request.ContactPersonMiddleName;
                    driver.ContactPersonLastName = request.ContactPersonLastName;
                    driver.Birthday = request.Birthday;
                    driver.PhoneCode = request.PhoneCode;
                    driver.PhoneNumber = request.PhoneNumber;
                    driver.Email = request.Email;
                    await _driverRepository.UpdateAsync(driver);
                    var log = new CreateAppLog(_applicationLogRepository);
                    log.CreateLog("driver", "driver", this.ToString(), Newtonsoft.Json.JsonConvert.SerializeObject(driver),request.UserId.ToString());
                    return new Response<Guid>(driver.Id);
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
    }
}