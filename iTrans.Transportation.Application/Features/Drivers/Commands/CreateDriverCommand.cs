﻿using AutoMapper;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Interfaces.Services;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.Drivers.Commands
{
    public partial class CreateDriverCommand : IRequest<Response<Guid>>
    {
        public Guid? UserId  { get; set; }
        public string Password { get; set; }
        public string DriverType { get; set; }
        public int RoleId { get; set; }
        public int CorporateTypeId { get; set; }
        public int Title { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Name { get; set; }
        public string IdentityNumber { get; set; }
        public int ContactPersonTitle { get; set; }
        public string ContactPersonFirstName { get; set; }
        public string ContactPersonMiddleName { get; set; }
        public string ContactPersonLastName { get; set; }
        public DateTime Birthday { get; set; }
        public string PhoneCode { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public  string Facbook { get; set; }
        public string Line { get; set; }
        public string Twitter { get; set; }
        public string Whatapp { get; set; }

    }
    public class CreateDriverCommandHandler : IRequestHandler<CreateDriverCommand, Response<Guid>>
    {
        private readonly IDriverRepositoryAsync _driverRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;
        public CreateDriverCommandHandler(IDriverRepositoryAsync driverRepository,IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _driverRepository = driverRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
        }

        public async Task<Response<Guid>> Handle(CreateDriverCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var driver = _mapper.Map<Driver>(request);
                driver.Created = DateTime.UtcNow;
                driver.Modified = DateTime.UtcNow;
                //driver.CreatedBy = "xxxxxxx";
                //driver.ModifiedBy = "xxxxxxx";
                var driverObject = await _driverRepository.AddAsync(driver);
                var log = new CreateAppLog(_applicationLogRepository);
                log.CreateLog("driver", "driver", this.ToString(), Newtonsoft.Json.JsonConvert.SerializeObject(driver),request.UserId.ToString());
                return new Response<Guid>(driverObject.Id);
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
    }
}
