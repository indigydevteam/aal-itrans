﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;

namespace iTrans.Transportation.Application.Features.Drivers.Commands
{
    public class VerifyDriverStatusCommand : IRequest<Response<Guid>>
    {
        public Guid Id { get; set; }
    }

    public class VerifyDriverStatusCommandHandler : IRequestHandler<VerifyDriverStatusCommand, Response<Guid>>
    {
        private readonly IDriverRepositoryAsync _driverRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        public VerifyDriverStatusCommandHandler(IDriverRepositoryAsync driverRepository, IApplicationLogRepositoryAsync applicationLogRepository)
        {
            _driverRepository = driverRepository;
            _applicationLogRepository = applicationLogRepository;
        }

        public async Task<Response<Guid>> Handle(VerifyDriverStatusCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var driver = (await _driverRepository.FindByCondition(x => x.Id == request.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (driver == null)
                {
                    throw new ApiException($"Driver Not Found.");
                }
                else
                {
                    driver.VerifyStatus = 0;
                    await _driverRepository.UpdateAsync(driver);
                    var log = new CreateAppLog(_applicationLogRepository);
                    log.Create("Driver", "Driver Verify Status", "Edit", Newtonsoft.Json.JsonConvert.SerializeObject(request));

                    return new Response<Guid>(driver.Id);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
