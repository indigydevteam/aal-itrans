﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;

namespace iTrans.Transportation.Application.Features.Stars.Commands
{
    public class UpdateDriverStarBIdCommand : IRequest<Response<Guid>>
    {
        public Guid? UserId { get; set; }
        public Guid Id { get; set; }
        public Guid OrderingId { get; set; }
        public int Star { get; set; }
    }

    public class UpdateDriverStarBIdCommandHandler : IRequestHandler<UpdateDriverStarBIdCommand, Response<Guid>>
    {
        private readonly IDriverRepositoryAsync _driverRepository;
        private readonly IOrderingRepositoryAsync _orderingRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IDriverActivityRepositoryAsync _driverActivityRepository;
        private readonly IMapper _mapper;

        public UpdateDriverStarBIdCommandHandler(IDriverRepositoryAsync driverdRepository,  IOrderingRepositoryAsync orderingRepository, IApplicationLogRepositoryAsync applicationLogRepository
            , IMapper mapper , IDriverActivityRepositoryAsync driverActivityRepository
            )
        {
            _driverRepository = driverdRepository;
            _applicationLogRepository = applicationLogRepository;
            _orderingRepository = orderingRepository;
            _driverActivityRepository = driverActivityRepository;
            _mapper = mapper;
        }

        public async Task<Response<Guid>> Handle(UpdateDriverStarBIdCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var objDriver = (await _driverRepository.FindByCondition(x => x.Id == request.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (objDriver == null)
                {
                    throw new ApiException($"Data Not Found.");
                }
                else
                {
                    var orderingCount = (await _orderingRepository.FindByCondition(x => x.Driver.Id == request.Id && (x.Status == 15 || x.Status == 16)).ConfigureAwait(false)).AsQueryable().Count();
                    var ordering = (await _orderingRepository.FindByCondition(x => x.Id == request.OrderingId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                    if (ordering != null)
                    {
                        ordering.DriverRanking = request.Star;
                        await _orderingRepository.UpdateAsync(ordering);
                    }
                    objDriver.Star = objDriver.Star + request.Star;
                     
                    if (orderingCount > 0)
                    {
                        float rating = 0;
                        rating = objDriver.Star / orderingCount;
                        
                        if (rating >= 5)
                        {
                            objDriver.Grade ="A";
                            objDriver.Rating = "5";
                        }
                        else if (rating > 4 && rating < 5)
                        {
                            objDriver.Grade = "B";
                            objDriver.Rating = "4";
                        }
                        else if (rating > 3 && rating < 4)
                        {
                            objDriver.Grade = "C";
                            objDriver.Rating = "3";
                        }
                        else if (rating > 2 && rating < 3)
                        {
                            objDriver.Grade = "D";
                            objDriver.Rating = "2";
                        }
                        else if (rating < 2)
                        {
                            objDriver.Grade = "E";
                            objDriver.Rating = "1";
                        }
                    }
                    await _driverRepository.UpdateAsync(objDriver);

                    var driverActivity = (await _driverActivityRepository.FindByCondition(x => x.DriverId.Equals(request.Id) && x.Date == DateTime.Now.ToString("yyyy'-'MM'-'dd")).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                    if (driverActivity == null)
                    {
                        DriverActivity newDriverActivity = new DriverActivity
                        {
                            DriverId = request.Id,
                            Date = DateTime.Now.ToString("yyyy'-'MM'-'dd"),
                            Star = request.Star,
                            AcceptJob = 0,
                            Complaint = 0,
                            Reject = 0,
                            Cancel = 0,
                            InsuranceValue = 0,
                            Created = DateTime.Now,
                            CreatedBy = request.UserId != null ? request.UserId.Value.ToString() : "-",
                            Modified = DateTime.Now,
                            ModifiedBy = request.UserId != null ? request.UserId.Value.ToString() : "-",
                        };
                        await _driverActivityRepository.AddAsync(newDriverActivity);
                    }
                    else
                    {
                        driverActivity.Star = driverActivity.Star + request.Star;
                        await _driverActivityRepository.UpdateAsync(driverActivity);
                    }

                    var log = new CreateAppLog(_applicationLogRepository);
                    log.Create("driver", "driver update star", this.ToString(), Newtonsoft.Json.JsonConvert.SerializeObject(request));
                    return new Response<Guid>(objDriver.Id);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}