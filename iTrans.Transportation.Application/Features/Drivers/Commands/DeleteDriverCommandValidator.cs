﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.Drivers.Commands
{
    public class DeleteDriverCommandValidator : AbstractValidator<DeleteDriverCommand>
    {
        private readonly IDriverRepositoryAsync driverRepository;
        private readonly IOrderingRepositoryAsync orderingRepository;
        public DeleteDriverCommandValidator(IDriverRepositoryAsync driverRepository, IOrderingRepositoryAsync orderingRepository)
        {
            this.driverRepository = driverRepository;
            this.orderingRepository = orderingRepository;
            RuleFor(p => p.Id)
               .NotEmpty().WithMessage("{PropertyName} is required.")
               .NotNull()
               .MustAsync(IsExistUser).WithMessage("{PropertyName} is not exists.")
                .MustAsync(IsOrderingInProcess).WithMessage("Your have ordering in process.");
        }

        private async Task<bool> IsExistUser(Guid? userId, CancellationToken cancellationToken)
        {
            if (userId == null) return false;
            var driverObject = (await driverRepository.FindByCondition(x => x.Id == userId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            if (driverObject == null)
            {
                return false;
            }
            return true;
        }
        private async Task<bool> IsOrderingInProcess(Guid? userId, CancellationToken cancellationToken)
        {
            int corderingCount = orderingRepository.GetItemCount(x => x.Driver.Id == userId && x.Status != 16 && x.Status != 15);
            if (corderingCount == 0)
            {
                return true;
            }
            return false;
        }
    }
}
