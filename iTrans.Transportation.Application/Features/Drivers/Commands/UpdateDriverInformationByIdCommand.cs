﻿using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Interfaces.Services;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;

namespace iTrans.Transportation.Application.Features.Drivers.Commands
{
    public partial class UpdateDriverInformationByIdCommand : IRequest<Response<Guid>>
    {

        public Guid? UserId { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public Guid Id { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public int CorporateTypeId { get; set; }
        public int TitleId { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string FirstName { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string MiddleName { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string LastName { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string Name { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public int IdentityType { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string IdentityNumber { get; set; }
        public int ContactPersonTitleId { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string ContactPersonFirstName { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string ContactPersonMiddleName { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string ContactPersonLastName { get; set; }
        public DateTime Birthday { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string PhoneCode { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string PhoneNumber { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string Email { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string Facbook { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string Line { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string Twitter { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string Whatapp { get; set; }
        public int CountryId { get; set; }
        public int ProvinceId { get; set; }
        public int DistrictId { get; set; }
        public int SubdistrictId { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string PostCode { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string Road { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string Alley { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string Address { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string idCard { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string drivingLicense { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string driverPicture { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string companyCertificate { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string NP20 { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string ProfilePicture { get; set; }
        public List<IFormFile> files { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string DeleteFiles { get; set; }
    }

    public class UpdateDriverInformationByIdCommandHandler : IRequestHandler<UpdateDriverInformationByIdCommand, Response<Guid>>
    {
        private readonly IDriverRepositoryAsync _driverRepository;
        private readonly IDriverAddressRepositoryAsync _driverAddressRepository;
        private readonly IDriverCarRepositoryAsync _drivercarRepository;
        private readonly IDriverFileRepositoryAsync _driverFileRepository;
        private readonly ICorporateTypeRepositoryAsync _corporateTypeRepository;
        private readonly ITitleRepositoryAsync _titleRepository;
        private readonly ICountryRepositoryAsync _countryRepository;
        private readonly IProvinceRepositoryAsync _provinceRepository;
        private readonly IDistrictRepositoryAsync _districtRepository;
        private readonly ISubdistrictRepositoryAsync _subdistrictRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;
        private readonly IConfiguration _configuration;
        public UpdateDriverInformationByIdCommandHandler(
            IDriverRepositoryAsync driverRepository, IDriverAddressRepositoryAsync driverAddressRepository, IDriverFileRepositoryAsync driverFileRepository, ICorporateTypeRepositoryAsync corporateTypeRepository, ITitleRepositoryAsync titleRepository
            , ICountryRepositoryAsync countryRepository, IProvinceRepositoryAsync provinceRepository, IDistrictRepositoryAsync districtRepository, IDriverCarRepositoryAsync drivercarRepository
            , ISubdistrictRepositoryAsync subdistrictRepository, IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper, IConfiguration configuration)
        {
            _driverRepository = driverRepository;
            _driverAddressRepository = driverAddressRepository;
            _driverFileRepository = driverFileRepository;
            _driverRepository = driverRepository;
            _corporateTypeRepository = corporateTypeRepository;
            _titleRepository = titleRepository;
            _countryRepository = countryRepository;
            _provinceRepository = provinceRepository;
            _districtRepository = districtRepository;
            _subdistrictRepository = subdistrictRepository;
            _applicationLogRepository = applicationLogRepository;
            _drivercarRepository = drivercarRepository;
            _mapper = mapper;
            _configuration = configuration;
        }

        public async Task<Response<Guid>> Handle(UpdateDriverInformationByIdCommand request, CancellationToken cancellationToken)
        {
            var getContentPath = _configuration.GetSection("ContentPath");
            string contentPath = getContentPath.Value;
            try
            {
                var driver = (await _driverRepository.FindByCondition(x => x.Id == request.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (driver == null)
                {
                    throw new ApiException($"Driver Not Found.");
                }
                else
                {
                    CorporateType corporateType = (await _corporateTypeRepository.FindByCondition(x => x.Id.Equals(request.CorporateTypeId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                    Title title = (await _titleRepository.FindByCondition(x => x.Id.Equals(request.TitleId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                    Title contactPersonTitle = (await _titleRepository.FindByCondition(x => x.Id.Equals(request.ContactPersonTitleId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                    Country country = (await _countryRepository.FindByCondition(x => x.Id.Equals(request.CountryId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                    Province province = (await _provinceRepository.FindByCondition(x => x.Id.Equals(request.ProvinceId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                    District district = (await _districtRepository.FindByCondition(x => x.Id.Equals(request.DistrictId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                    Subdistrict subdistrict = (await _subdistrictRepository.FindByCondition(x => x.Id.Equals(request.SubdistrictId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                    string phoneNumber = request.PhoneNumber != null ? request.PhoneNumber : "";
                    if (request.PhoneCode == "+66" && request.PhoneNumber != null && request.PhoneNumber.Length > 9 && request.PhoneNumber.First() == '0')
                    {
                        phoneNumber = request.PhoneNumber.Remove(0, 1);
                    }
                    driver.CorporateType = corporateType;
                    driver.Title = title;
                    driver.FirstName = request.FirstName;
                    driver.MiddleName = request.MiddleName;
                    driver.LastName = request.LastName;
                    driver.Name = request.Name;
                    driver.IdentityType = request.IdentityType;
                    driver.IdentityNumber = request.IdentityNumber;
                    driver.ContactPersonTitle = contactPersonTitle;
                    driver.ContactPersonFirstName = request.ContactPersonFirstName;
                    driver.ContactPersonMiddleName = request.ContactPersonMiddleName;
                    driver.ContactPersonLastName = request.ContactPersonLastName;
                    driver.Birthday = request.Birthday;
                    driver.PhoneCode = request.PhoneCode;
                    driver.PhoneNumber = phoneNumber;
                    driver.Email = request.Email;
                    driver.Facbook = request.Facbook;
                    driver.Line = request.Line;
                    driver.Twitter = request.Twitter;
                    driver.Whatapp = request.Whatapp;
                    driver.Modified = DateTime.UtcNow;
                    driver.ModifiedBy = request.UserId != null ? request.UserId.Value.ToString() : "";


                    //if (driver.Corparate == null || driver.Corparate == new Guid())
                    //{
                    //    var car = (await _drivercarRepository.FindByCondition(x => x.Driver.Id == request.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                    //    if (car != null)
                    //    {
                    //        car.DriverName = driver.Name;
                    //        car.DriverIdentityNumber = driver.IdentityNumber;
                    //        car.DriverPhoneCode = driver.PhoneCode;
                    //        car.DriverPhoneNumber = driver.PhoneCode;
                    //    }
                    //}

                    if (driver.DriverType == "personal")
                    {
                        var car = (await _drivercarRepository.FindByCondition(x => x.Driver.Id == request.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                        if (car != null)
                        {
                            car.DriverName = driver.Name;
                            car.DriverIdentityNumber = driver.IdentityNumber;
                            car.DriverPhoneCode = driver.PhoneCode;
                            car.DriverPhoneNumber = driver.PhoneNumber;
                        }
                    }

                    if (request.files != null)
                    {
                        var verifyStatus = driver.VerifyStatus;
                        foreach (IFormFile file in request.files)
                        {
                            string documentType = GetDocumentType(request, file.FileName);
                            if (documentType != "profilepicture")
                            {
                                verifyStatus = 0;
                            }
                        }
                        driver.VerifyStatus = verifyStatus;
                    }

                    await _driverRepository.UpdateAsync(driver);
                    var log = new CreateAppLog(_applicationLogRepository);

                    var driverAddress = (await _driverAddressRepository.FindByCondition(x => x.DriverId == request.Id && x.AddressType == "register").ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                    if (driverAddress == null)
                    {
                        DriverAddress address = new DriverAddress
                        {
                            DriverId = request.Id,
                            Country = country,
                            Province = province,
                            District = district,
                            Subdistrict = subdistrict,
                            PostCode = request.PostCode,
                            Road = request.Road,
                            Alley = request.Alley,
                            Address = request.Address,
                            Branch = "",
                            AddressType = "register",
                            AddressName = "",
                            ContactPerson = "",
                            ContactPhoneNumber = "",
                            ContactEmail = "",
                            IsMainData = true,
                            Maps = "",
                            Sequence = 1,
                            Created = DateTime.UtcNow,
                            Modified = DateTime.UtcNow,
                            CreatedBy = request.UserId != null ? request.UserId.Value.ToString() : "",
                            ModifiedBy = request.UserId != null ? request.UserId.Value.ToString() : ""
                        };
                        var driverAddressObject = await _driverAddressRepository.AddAsync(address);
                    }
                    else
                    {
                        driverAddress.DriverId = request.Id;
                        driverAddress.Country = country;
                        driverAddress.Province = province;
                        driverAddress.District = district;
                        driverAddress.Subdistrict = subdistrict;
                        driverAddress.PostCode = request.PostCode;
                        driverAddress.Road = request.Road;
                        driverAddress.Alley = request.Alley;
                        driverAddress.Address = request.Address;
                        driverAddress.IsMainData = true;
                        await _driverAddressRepository.UpdateAsync(driverAddress);
                    }
                    var driverTaxAddress = (await _driverAddressRepository.FindByCondition(x => x.DriverId == request.Id && x.AddressType == "register-tax").ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                    if (driverTaxAddress == null)
                    {
                        DriverAddress address = new DriverAddress
                        {
                            DriverId = request.Id,
                            Country = country,
                            Province = province,
                            District = district,
                            Subdistrict = subdistrict,
                            PostCode = request.PostCode,
                            Road = request.Road,
                            Alley = request.Alley,
                            Address = request.Address,
                            Branch = "",
                            AddressType = "register-tax",
                            AddressName = "",
                            ContactPerson = "",
                            ContactPhoneNumber = "",
                            ContactEmail = "",
                            IsMainData = true,
                            Maps = "",
                            Sequence = 1,
                            Created = DateTime.UtcNow,
                            Modified = DateTime.UtcNow,
                            CreatedBy = request.UserId != null ? request.UserId.Value.ToString() : "",
                            ModifiedBy = request.UserId != null ? request.UserId.Value.ToString() : ""
                        };
                        var driverAddressObject = await _driverAddressRepository.AddAsync(address);
                    }
                    else
                    {
                        driverTaxAddress.DriverId = request.Id;
                        driverTaxAddress.Country = country;
                        driverTaxAddress.Province = province;
                        driverTaxAddress.District = district;
                        driverTaxAddress.Subdistrict = subdistrict;
                        driverTaxAddress.PostCode = request.PostCode;
                        driverTaxAddress.Road = request.Road;
                        driverTaxAddress.Alley = request.Alley;
                        driverTaxAddress.Address = request.Address;
                        driverTaxAddress.IsMainData = true;
                        await _driverAddressRepository.UpdateAsync(driverTaxAddress);
                    }
                    string folderPath = contentPath + "driver/" + request.Id.ToString();
                    if (!Directory.Exists(folderPath))
                    {
                        Directory.CreateDirectory(folderPath);
                    }
                    if (request.DeleteFiles != null && request.DeleteFiles.Trim() != "")
                    {
                        string[] deleteFiles = request.DeleteFiles.Split(",");
                        foreach (string delFile in deleteFiles)
                        {
                            var driverFile = (await _driverFileRepository.FindByCondition(x => x.FileName.Trim().ToLower() == delFile.Trim().ToLower() && x.DriverId.Equals(request.Id)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                            if (driverFile != null)
                            {
                                if (driverFile.IsApprove == false)
                                {
                                    if (File.Exists(Path.Combine(contentPath, driverFile.FilePath)))
                                        File.Delete(Path.Combine(contentPath, driverFile.FilePath));
                                    await _driverFileRepository.DeleteAsync(driverFile);
                                }
                                else
                                {
                                    driverFile.IsDelete = true;
                                    await _driverFileRepository.UpdateAsync(driverFile);
                                }
                                //if (File.Exists(Path.Combine(contentPath, driverFile.FilePath)))
                                //    File.Delete(Path.Combine(contentPath, driverFile.FilePath));
                                //await _driverFileRepository.DeleteAsync(driverFile);
                            }
                        }
                    }
                    if (request.ProfilePicture != null && request.ProfilePicture.Trim() != "")
                    {
                        var driverProfilePictures = driver.Files.Where(x => x.DocumentType == "profilepicture").ToList();
                        foreach (DriverFile driverProfilePicture in driverProfilePictures)
                        {
                            if (File.Exists(Path.Combine(contentPath, driverProfilePicture.FilePath)))
                                File.Delete(Path.Combine(contentPath, driverProfilePicture.FilePath));
                        }
                        (await _driverFileRepository.CreateSQLQuery("DELETE Driver_File where DocumentType = 'profilepicture' and DriverId = '" + request.Id.ToString() + "'").ConfigureAwait(false)).ExecuteUpdate();
                    }
                    int fileCount = 0;
                    if (request.files != null)
                    {
                        foreach (IFormFile file in request.files)
                        {

                            fileCount = fileCount + 1;
                            string filePath = Path.Combine(folderPath, file.FileName);
                            using (FileStream fileStream = new FileStream(filePath, FileMode.Create))
                            {
                                await file.CopyToAsync(fileStream);
                                FileInfo fi = new FileInfo(filePath);
                                string documentType = GetDocumentType(request, file.FileName);
                                if (driver.DriverType == "corporate" && documentType == "IdCard")
                                {
                                    documentType = "CompanyCertificate";
                                }
                                DriverFile driverFile = new DriverFile
                                {
                                    DriverId = request.Id,
                                    FileName = file.FileName,
                                    ContentType = file.ContentType,
                                    FilePath = "driver/" + request.Id.ToString() + "/" + file.FileName,
                                    DirectoryPath = "driver/" + request.Id.ToString() + "/",
                                    FileEXT = fi.Extension,
                                    DocumentType = documentType,
                                    Sequence = fileCount,
                                    Created = DateTime.UtcNow,
                                    Modified = DateTime.UtcNow,
                                    CreatedBy = request.UserId != null ? request.UserId.Value.ToString() : "",
                                    ModifiedBy = request.UserId != null ? request.UserId.Value.ToString() : ""
                                };
                                var driverFileObject = await _driverFileRepository.AddAsync(driverFile);
                            }
                        }
                    }
                    log.CreateLog("driver", "driver", "edit", Newtonsoft.Json.JsonConvert.SerializeObject(request), request.UserId.ToString());
                    return new Response<Guid>(driver.Id);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GetDocumentType(UpdateDriverInformationByIdCommand request, string fileName)
        {
            string result = "";
            if (request.idCard != null)
            {
                string[] idCard = request.idCard.Split(",");
                var existIdCard = idCard.Where(x => x == fileName).FirstOrDefault();
                if (existIdCard != null)
                {
                    result = "IdCard";
                    return result;
                }
            }
            if (request.companyCertificate != null)
            {
                string[] companyCertificate = request.companyCertificate.Split(",");
                var existCompanyCertificate = companyCertificate.Where(x => x == fileName).FirstOrDefault();
                if (existCompanyCertificate != null)
                {
                    result = "CompanyCertificate";
                    return result;
                }
            }
            if (request.drivingLicense != null)
            {
                string[] drivingLicense = request.drivingLicense.Split(",");
                var existDrivingLicensee = drivingLicense.Where(x => x == fileName).FirstOrDefault();
                if (existDrivingLicensee != null)
                {
                    result = "DrivingLicense";
                    return result;
                }
            }
            if (request.driverPicture != null)
            {
                string[] driverPicture = request.driverPicture.Split(",");
                var existDriverPicture = driverPicture.Where(x => x == fileName).FirstOrDefault();
                if (existDriverPicture != null)
                {
                    result = "DriverPicture";
                    return result;
                }
            }
            if (request.NP20 != null)
            {
                string[] NP20 = request.NP20.Split(",");
                var existNP20 = NP20.Where(x => x == fileName).FirstOrDefault();
                if (existNP20 != null)
                {
                    result = "NP20";
                    return result;
                }
            }
            if (request.ProfilePicture != null)
            {
                string[] ProfilePicture = request.ProfilePicture.Split(",");
                var existProfilePicture = ProfilePicture.Where(x => x == fileName).FirstOrDefault();
                if (existProfilePicture != null)
                {
                    result = "profilepicture";
                    return result;
                }
            }
            return result;
        }
    }
}
