﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;

namespace iTrans.Transportation.Application.Features.Drivers.Commands
{
    public class DeleteDriverCommand : IRequest<Response<bool>>
    {
        public Guid? Id { get; set; }
    }

    public class DeleteDriverCommandHandler : IRequestHandler<DeleteDriverCommand, Response<bool>>
    {
        private readonly IDriverRepositoryAsync _driverRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;

        public DeleteDriverCommandHandler(IDriverRepositoryAsync driverRepository, IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _driverRepository = driverRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
        }

        public async Task<Response<bool>> Handle(DeleteDriverCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var driver = (await _driverRepository.FindByCondition(x => x.Id == request.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (driver == null)
                {
                    throw new ApiException($"Driver Not Found.");
                }
                else
                {
                    driver.IsDelete = true;

                   (await _driverRepository.CreateSQLQuery("UPDATE Driver SET [IsDelete] = 0 WHERE [Corparate] = '" + driver.Id.ToString() + "'").ConfigureAwait(false)).ExecuteUpdate();
                    await _driverRepository.UpdateAsync(driver);
                    var log = new CreateAppLog(_applicationLogRepository);
                    log.CreateLog("Driver", "Driver", "Edit", Newtonsoft.Json.JsonConvert.SerializeObject(request),request.Id.ToString());
                    return new Response<bool>(true);
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
    }
}