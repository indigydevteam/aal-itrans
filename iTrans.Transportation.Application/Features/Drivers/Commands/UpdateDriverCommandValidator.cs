﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.Drivers.Commands
{
    public class UpdateDriverCommandValidator : AbstractValidator<UpdateDriverCommand>
    {
        private readonly IDriverRepositoryAsync driverRepository;
        public UpdateDriverCommandValidator(IDriverRepositoryAsync driverRepository)
        {
            this.driverRepository = driverRepository;

            RuleFor(p => p.IdentityNumber)
                .NotEmpty().WithMessage("{PropertyName} is required.")
                .NotNull()
                .MaximumLength(50).WithMessage("{PropertyName} must not exceed 50 characters.");

            RuleFor(p => p.PhoneNumber)
                .NotEmpty().WithMessage("{PropertyName} is required.")
                .NotNull()
                .MaximumLength(50).WithMessage("{PropertyName} must not exceed 50 characters.");

            RuleFor(p => p.IdentityNumber)
                .MustAsync(async (p, s, cancellation) =>
                {
                    return await IsUniqueIdentityNumber(p.Id,p.IdentityNumber).ConfigureAwait(false);
                }).WithMessage("{PropertyName} already exists.");
        }

        private async Task<bool> IsUniqueIdentityNumber(Guid id,string IdentityNumber)
        {
            var driverObject = (await driverRepository.FindByCondition(x => x.IdentityNumber.ToLower() == IdentityNumber.ToLower() && x.Id != id && x.IsDelete == false).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            if (driverObject != null)
            {
                return false;
            }
            return true;
        }

        
    }
}
