﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;

namespace iTrans.Transportation.Application.Features.Drivers.Commands
{
    public class UpdateDriverLevelCommand : IRequest<Response<Guid>>
    {
        public Guid? UserId { get; set; }
        public Guid Id { get; set; }
        public string DocumentType { get; set; }
        public List<IFormFile> Files { get; set; }

    }

    public class UpdateDriverLevelCommandHandler : IRequestHandler<UpdateDriverLevelCommand, Response<Guid>>
    {
        private readonly IDriverRepositoryAsync _driverRepository;
        private readonly IDriverFileRepositoryAsync _driverFileRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;
        private readonly IConfiguration _configuration;

        public UpdateDriverLevelCommandHandler(IDriverRepositoryAsync driverRepository, IDriverFileRepositoryAsync driverFileRepository, IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper, IConfiguration configuration)
        {
            _driverRepository = driverRepository;
            _driverFileRepository = driverFileRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
            _configuration = configuration;
        }

        public async Task<Response<Guid>> Handle(UpdateDriverLevelCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var driver = (await _driverRepository.FindByCondition(x => x.Id == request.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (driver == null)
                {
                    throw new ApiException($"Data Not Found.");
                }
                else
                {
                    string currentTimeStr = DateTime.Now.ToString("yyyyMMddHHmmss");
                    if (request.Files != null && request.Files.Count > 0)
                    {
                        var getContentPath = _configuration.GetSection("ContentPath");
                        var contentPath = getContentPath.Value;

                        string folderPath = contentPath + "driver/" + driver.Id.ToString() + "/" + currentTimeStr;
                        if (!Directory.Exists(folderPath))
                        {
                            Directory.CreateDirectory(folderPath);
                        }
                        int fileCount = 0;

                        foreach (IFormFile file in request.Files)
                        {
                            fileCount = fileCount + 1;
                            string filePath = Path.Combine(folderPath, file.FileName);
                            using (FileStream fileStream = new FileStream(filePath, FileMode.Create))
                            {
                                await file.CopyToAsync(fileStream);
                                FileInfo fi = new FileInfo(filePath);

                                DriverFile driverFile = new DriverFile
                                {
                                    DriverId = driver.Id,
                                    FileName = file.FileName,
                                    ContentType = file.ContentType,
                                    FilePath = "driver/" + driver.Id.ToString() + "/" + currentTimeStr + "/" + file.FileName,
                                    DirectoryPath = "driver/" + driver.Id.ToString() + "/" + currentTimeStr + "/",
                                    FileEXT = fi.Extension,
                                    DocumentType = request.DocumentType,
                                    Sequence = fileCount,
                                };
                                var driverFileObject = await _driverFileRepository.AddAsync(driverFile);
                            }
                        }

                        await _driverRepository.UpdateAsync(driver);
                        var log = new CreateAppLog(_applicationLogRepository);
                        log.CreateLog("driver", "driver update level", "edit", Newtonsoft.Json.JsonConvert.SerializeObject(request),request.UserId.ToString());
                    }

                    return new Response<Guid>(driver.Id);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
