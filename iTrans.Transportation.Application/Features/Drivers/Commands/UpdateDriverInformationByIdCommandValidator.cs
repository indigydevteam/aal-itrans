﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using iTrans.Transportation.Application.Helper;
using System.Text.RegularExpressions;
using Microsoft.Extensions.Configuration;

namespace iTrans.Transportation.Application.Features.Drivers.Commands
{
    public class UpdateDriverInformationByIdCommandValidator : AbstractValidator<UpdateDriverInformationByIdCommand>
    {
        private readonly IDriverRepositoryAsync driverRepository;
        private readonly IConfiguration _configuration;

        public UpdateDriverInformationByIdCommandValidator( IDriverRepositoryAsync driverRepository, IConfiguration configuration)
        {
            this.driverRepository = driverRepository;
            _configuration = configuration;

            RuleFor(p => p.PhoneNumber)
                .NotEmpty().WithMessage("{PropertyName} is required.")
                .NotNull()
                .MaximumLength(50).WithMessage("{PropertyName} must not exceed 50 characters.")
                .MustAsync(async (p, s, cancellation) =>
                {
                    return await IsUniquePhoneNumber(p.Id, p.PhoneNumber).ConfigureAwait(false);
                }).WithMessage("{PropertyName} already exists.");


            RuleFor(p => p.Name)
                .NotEmpty().WithMessage("{PropertyName} is required.")
                .NotNull()
                .MaximumLength(50).WithMessage("{PropertyName} must not exceed 50 characters.");

            RuleFor(p => p.IdentityNumber)
               .NotEmpty().WithMessage("{PropertyName} is required.")
               .NotNull()
               .MaximumLength(50).WithMessage("{PropertyName} must not exceed 50 characters.")
               .MustAsync(async (p, s, cancellation) =>
               {
                   return await IsUniqueIdentityNumber(p.Id, p.IdentityType, p.IdentityNumber).ConfigureAwait(false);
               }).WithMessage("{PropertyName} already exists.");
        }

        private async Task<bool> IsUniqueIdentityNumber(Guid id, int identityType, string identityNumber)
        {
            bool result = true;
            var isDevelopment = _configuration.GetSection("IsDevelopment");
            if (identityType == 1)
            {
                string DeCodeidentityNumber = AESMgr.Decrypt(identityNumber);

                Regex rexPersonal = new Regex(@"^[0-9]{13}$");
                if (rexPersonal.IsMatch(DeCodeidentityNumber))
                {
                    int sum = 0;

                    for (int i = 0; i < 12; i++)
                    {
                        sum += int.Parse(DeCodeidentityNumber[i].ToString()) * (13 - i);
                    }

                    result = (int.Parse(DeCodeidentityNumber[12].ToString()) == ((11 - (sum % 11)) % 10));
                }
            }
            if (isDevelopment != null && isDevelopment.Value == "true")
            {
                result = true;
            }
            if (result)
            {
                var driverObject = (await driverRepository.FindByCondition(x => x.IdentityNumber.Trim().ToLower() == identityNumber.Trim().ToLower() && !x.Id.Equals(id) && !x.IsDelete && (x.Corparate == null || x.Corparate == new Guid())).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (driverObject != null)
                {
                    result = false;
                }
            }
            return result;
        }
        private async Task<bool> IsUniquePhoneNumber(Guid id, string phoneNumber)
        {
            var driverObject = (await driverRepository.FindByCondition(x => x.PhoneNumber.Trim().ToLower() == phoneNumber.Trim().ToLower() && !x.Id.Equals(id) && !x.IsDelete).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            if (driverObject != null)
            {
                return false;
            }
            return true;

        }
    }
}
