﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;

namespace iTrans.Transportation.Application.Features.driverPassWords.Commands
{
    public class UpdatedriverPasswordByIdCommand : IRequest<Response<Guid>>
    {
        public Guid? Id { get; set; }
        public string Password { get; set; }

    }

    public class UpdatedriverPasswordByIdCommandHandler : IRequestHandler<UpdatedriverPasswordByIdCommand, Response<Guid>>
    {
        private readonly IDriverRepositoryAsync _driverRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;

        public UpdatedriverPasswordByIdCommandHandler(IDriverRepositoryAsync driverdRepository,IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _driverRepository = driverdRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
        }

        public async Task<Response<Guid>> Handle(UpdatedriverPasswordByIdCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var driverPassWord = (await _driverRepository.FindByCondition(x => x.Id == request.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (driverPassWord == null)
                {
                    throw new ApiException($"driverPassWord Not Found.");
                }
                else
                {
                    driverPassWord.Password = request.Password;
                    driverPassWord.Modified = DateTime.UtcNow;
                    await _driverRepository.UpdateAsync(driverPassWord);
                    var log = new CreateAppLog(_applicationLogRepository);
                    log.CreateLog("driver", "driver", this.ToString(), Newtonsoft.Json.JsonConvert.SerializeObject(request),request.Id.ToString());
                    return new Response<Guid>(driverPassWord.Id);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}