﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.Driver;
using iTrans.Transportation.Application.DTOs.Driver.Backoffice;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.Drivers.Backoffice.Queries
{
   public class GetDriverByVerifyStatus : IRequest<Response<DriverByVerifyStatusViewModel>>
    {

    }
    public class GetDriverByVerifyStatusHandler : IRequestHandler<GetDriverByVerifyStatus, Response<DriverByVerifyStatusViewModel>>
    {
        private readonly IDriverRepositoryAsync _driverRepository;

        private readonly IMapper _mapper;
        public GetDriverByVerifyStatusHandler(IDriverRepositoryAsync DriverRepository, IMapper mapper)
        {
            _driverRepository = DriverRepository;
            _mapper = mapper;
        }

        public async Task<Response<DriverByVerifyStatusViewModel>> Handle(GetDriverByVerifyStatus request, CancellationToken cancellationToken)
        {
            var Driver = await _driverRepository.GetAllAsync();
            var result = new DriverByVerifyStatusViewModel();

            result.presonalstatustrue = Driver.Where(c => c.VerifyStatus.Equals(1) && c.DriverType.Equals("personal")).Count();
            result.presonalstatusfalse = Driver.Where(c => c.VerifyStatus.Equals(2) && c.DriverType.Equals("personal")).Count();
            result.corporatestatustrue = Driver.Where(c => c.VerifyStatus.Equals(1) && c.DriverType.Equals("corporate")).Count();
            result.corporatestatusfalse = Driver.Where(c => c.VerifyStatus.Equals(2) && c.DriverType.Equals("corporate")).Count();

            return new Response<DriverByVerifyStatusViewModel>(result);
        }
    }
}