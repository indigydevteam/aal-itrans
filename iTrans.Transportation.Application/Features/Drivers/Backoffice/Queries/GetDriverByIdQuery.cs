﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.Driver.Backoffice;
using iTrans.Transportation.Application.DTOs.DriverTermAndCondition;
using iTrans.Transportation.Application.DTOs.UserLevel;
using iTrans.Transportation.Application.DTOs.VerifyStatus;
using iTrans.Transportation.Application.Helper;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.Drivers.Backoffice.Queries
{
    public class GetDriverByIdBackOfficeQuery : IRequest<Response<DriverViewModel>>
    {
        public Guid Id { get; set; }
        public string ContentDirectory { get; set; }

    }
    public class GetDriverByIdBackOfficeQueryHandler : IRequestHandler<GetDriverByIdBackOfficeQuery, Response<DriverViewModel>>
    {
        private readonly IDriverRepositoryAsync _driverRepository;
        private readonly IUserLevelRepositoryAsync _userLevelRepository;
        private readonly IDriverTermAndConditionRepositoryAsync _driverTermAndConditionRepository;
        private readonly IDriverCarRepositoryAsync _driverCarRepository;
        private readonly IMapper _mapper;
        public GetDriverByIdBackOfficeQueryHandler(IDriverRepositoryAsync driverRepository, IUserLevelRepositoryAsync userLevelRepository, IDriverTermAndConditionRepositoryAsync driverTermAndConditionRepository, IDriverCarRepositoryAsync driverCarRepository, IMapper mapper)
        {
            _driverRepository = driverRepository;
            _userLevelRepository = userLevelRepository;
            _driverTermAndConditionRepository = driverTermAndConditionRepository;
            _driverCarRepository = driverCarRepository;
            _mapper = mapper;
        }
        public async Task<Response<DriverViewModel>> Handle(GetDriverByIdBackOfficeQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var folderDetails = Path.Combine(Directory.GetCurrentDirectory(), $"Shared\\VerifyStatus.json");
                var statusJson = System.IO.File.ReadAllText(folderDetails);
                List<VerifyStatusViewModel> verifyStatus = Newtonsoft.Json.JsonConvert.DeserializeObject<List<VerifyStatusViewModel>>(statusJson);

                var driverObject = (await _driverRepository.FindByCondition(x => x.Id.Equals(request.Id)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();

                var driverterm = _mapper.Map<DriverTermAndConditionViewModel>((await _driverTermAndConditionRepository.FindByCondition(x => x.Driver.Id.Equals(request.Id)).ConfigureAwait(false)).AsQueryable().FirstOrDefault());

                DriverViewModel driver = _mapper.Map<DriverViewModel>(driverObject);
                if (driver != null)
                {
                    driver.IdentityNumber = AESMgr.Decrypt(driver.IdentityNumber);
                    if (driver.Files != null) driver.Files = driver.Files.Where(x => x.IsDelete == false).ToList();
                    driver.DriverLevel = _mapper.Map<UserLevelViewModel>((await _userLevelRepository.FindByCondition(x => x.Level.Equals(driver.Level) && x.Active == true && x.Module == "driver").ConfigureAwait(false)).AsQueryable().FirstOrDefault());
                    driver.Addresses = driver.Addresses.Where(x => x.addressType == "register").ToList();
                    driver.VerifyStatusObj = _mapper.Map<VerifyStatusViewModel>(verifyStatus.Where(f => f.id.Equals(driver.VerifyStatus)).FirstOrDefault());
                    if (driver.PhoneCode == "+66" && driver.PhoneNumber != null && driver.PhoneNumber.Trim() != "")
                    {
                        driver.PhoneNumber = "0" + driver.PhoneNumber;
                    }
                    //driver.PhoneNumber = "0" +driver.PhoneNumber;
                    //driver.CountCar = _driverCarRepository.GetAllAsync().Result.Where(c => c.Owner.Id.Equals(driver.Id)).Count();
                    if (driver.DriverType == "personal")
                    {
                         driver.CountCar = _driverCarRepository.GetItemCount(c => c.Driver.Id == driver.Id);
                    }
                    else
                    {
                        driver.CountCar =  _driverCarRepository.GetItemCount(c => c.Owner.Id == driver.Id);
                    }
                    
                    //if (driver.Files.Where(x => x.documentType.Equals("profilepicture")).Select(x => x.filePath).FirstOrDefault() != null)
                    //{

                    //    driver.imageProfile = ConvertImage.ConvertImageURLToBase64(request.ContentDirectory + "/" + driver.Files.Where(x => x.documentType.Equals("profilepicture") && !x.IsDelete).Select(x => x.filePath).FirstOrDefault());

                    //}
                    //if (driver.Files.Where(x => x.documentType.Equals("IdCard")).Select(x => x.filePath).FirstOrDefault() != null)
                    //{
                    //    driver.IdCard = ConvertImage.ConvertImageURLToBase64(request.ContentDirectory + "/" + driver.Files.Where(x => x.documentType.Equals("IdCard") && !x.IsDelete).Select(x => x.filePath).FirstOrDefault());
                    //    //if (driver.VerifyStatus == 0)
                    //    //{
                    //    //    driver.IdCard = ConvertImage.ConvertImageURLToBase64(request.ContentDirectory + "/" + driver.Files.Where(x => x.documentType.Equals("IdCard") && !x.IsDelete).Select(x => x.filePath).FirstOrDefault());
                    //    //}
                    //    //else
                    //    //{
                    //    //    driver.IdCard = ConvertImage.ConvertImageURLToBase64(request.ContentDirectory + "/" + driver.Files.Where(x => x.documentType.Equals("IdCard") && x.IsApprove).Select(x => x.filePath).FirstOrDefault());
                    //    //}
                    //}
                    //if (driver.Files.Where(x => x.documentType.Equals("DriverPicture")).Select(x => x.filePath).FirstOrDefault() != null)
                    //{
                    //    driver.DriverPicture = ConvertImage.ConvertImageURLToBase64(request.ContentDirectory + "/" + driver.Files.Where(x => x.documentType.Equals("DriverPicture") && !x.IsDelete).Select(x => x.filePath).FirstOrDefault());
                    //    //if (driver.VerifyStatus == 0)
                    //    //{
                    //    //    driver.DriverPicture = ConvertImage.ConvertImageURLToBase64(request.ContentDirectory + "/" + driver.Files.Where(x => x.documentType.Equals("DriverPicture") && !x.IsApprove && !x.IsDelete).Select(x => x.filePath).FirstOrDefault());
                    //    //}
                    //    //else
                    //    //{
                    //    //    driver.DriverPicture = ConvertImage.ConvertImageURLToBase64(request.ContentDirectory + "/" + driver.Files.Where(x => x.documentType.Equals("DriverPicture") && !x.IsDelete).Select(x => x.filePath).FirstOrDefault());
                    //    //}
                    //}
                    //if (driver.Files.Where(x => x.documentType.Equals("DrivingLicense")).Select(x => x.filePath).FirstOrDefault() != null)
                    //{
                    //    driver.DrivingLicense = ConvertImage.ConvertImageURLToBase64(request.ContentDirectory + "/" + driver.Files.Where(x => x.documentType.Equals("DrivingLicense") && !x.IsDelete).Select(x => x.filePath).FirstOrDefault());
                    //    //if (driver.VerifyStatus == 0)
                    //    //{
                    //    //    driver.DrivingLicense = ConvertImage.ConvertImageURLToBase64(request.ContentDirectory + "/" + driver.Files.Where(x => x.documentType.Equals("DrivingLicense") && !x.IsApprove && !x.IsDelete).Select(x => x.filePath).FirstOrDefault());
                    //    //}
                    //    //else
                    //    //{
                    //    //    driver.DrivingLicense = ConvertImage.ConvertImageURLToBase64(request.ContentDirectory + "/" + driver.Files.Where(x => x.documentType.Equals("DrivingLicense") && !x.IsDelete).Select(x => x.filePath).FirstOrDefault());
                    //    //}
                    //}
                    //if (driver.Files.Where(x => x.documentType.Equals("CompanyCertificate")).Select(x => x.filePath).FirstOrDefault() != null)
                    //{
                    //    driver.CompanyCertificate = ConvertImage.ConvertImageURLToBase64(request.ContentDirectory + "/" + driver.Files.Where(x => x.documentType.Equals("CompanyCertificate") && !x.IsDelete).Select(x => x.filePath).FirstOrDefault());
                    //    //if (driver.VerifyStatus == 0)
                    //    //{
                    //    //    driver.CompanyCertificate = ConvertImage.ConvertImageURLToBase64(request.ContentDirectory + "/" + driver.Files.Where(x => x.documentType.Equals("CompanyCertificate") && !x.IsApprove && !x.IsDelete).Select(x => x.filePath).FirstOrDefault());
                    //    //}
                    //    //else
                    //    //{
                    //    //    driver.CompanyCertificate = ConvertImage.ConvertImageURLToBase64(request.ContentDirectory + "/" + driver.Files.Where(x => x.documentType.Equals("CompanyCertificate") && !x.IsDelete).Select(x => x.filePath).FirstOrDefault());
                    //    //}
                    //}
                    //if (driver.Files.Where(x => x.documentType.Equals("NP20")).Select(x => x.filePath).FirstOrDefault() != null)
                    //{
                    //    driver.NP20 = ConvertImage.ConvertImageURLToBase64(request.ContentDirectory + "/" + driver.Files.Where(x => x.documentType.Equals("NP20") && !x.IsDelete).Select(x => x.filePath).FirstOrDefault());
                    //    //if (driver.VerifyStatus == 0)
                    //    //{
                    //    //    driver.NP20 = ConvertImage.ConvertImageURLToBase64(request.ContentDirectory + "/" + driver.Files.Where(x => x.documentType.Equals("NP20")).Select(x => x.filePath).FirstOrDefault());
                    //    //}
                    //    //else
                    //    //{
                    //    //    driver.NP20 = ConvertImage.ConvertImageURLToBase64(request.ContentDirectory + "/" + driver.Files.Where(x => x.documentType.Equals("NP20")).Select(x => x.filePath).FirstOrDefault());
                    //    //}
                    //}
                }

                return new Response<DriverViewModel>(driver);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
