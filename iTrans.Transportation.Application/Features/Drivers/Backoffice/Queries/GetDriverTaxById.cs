﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.Driver.Backoffice;
using iTrans.Transportation.Application.DTOs.DriverAddress;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.Drivers.Backoffice.Queries
{
    public class GetDriverTaxById : IRequest<Response<TaxDriverAddressViewModel>>
    {
        public Guid driverId { get; set; }
    }
    public class GetDriverTaxByIdHandler : IRequestHandler<GetDriverTaxById, Response<TaxDriverAddressViewModel>>
    {
        private readonly IDriverAddressRepositoryAsync _driverAddressRepository;
        private readonly IDriverRepositoryAsync _driverRepository;

        private readonly IMapper _mapper;
        public GetDriverTaxByIdHandler(IDriverAddressRepositoryAsync driverAddressRepository, IDriverRepositoryAsync driverRepository, IMapper mapper)
        {
            _driverAddressRepository = driverAddressRepository;
            _driverRepository = driverRepository;
            _mapper = mapper;
        }
        public async Task<Response<TaxDriverAddressViewModel>> Handle(GetDriverTaxById request, CancellationToken cancellationToken)
        {
            var driver = _mapper.Map<DriverViewModel>((await _driverRepository.FindByCondition(x => x.Id.Equals(request.driverId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault());
            var driverpersonal = _mapper.Map<DriverViewModel>((await _driverRepository.FindByCondition(x => x.Id.Equals(request.driverId) && x.DriverType == "personal ").ConfigureAwait(false)).AsQueryable().FirstOrDefault());
            var drivercorporate = _mapper.Map<DriverViewModel>((await _driverRepository.FindByCondition(x => x.Id.Equals(request.driverId) && x.DriverType == "corporate").ConfigureAwait(false)).AsQueryable().FirstOrDefault());
            var result = new TaxDriverAddressViewModel();

            if (driverpersonal != null)
            {

                result.driverId = driver.Id;
                result.driverType = driver.DriverType;
                result.personal = driverpersonal.Addresses.Where(x => x.addressType == "tax" && x.isMainData == true).FirstOrDefault();
                if (result.personal == null)
                {
                    result.personal = driverpersonal.Addresses.Where(x => x.addressType == "register" && x.isMainData == true).FirstOrDefault();
                }

            }
            else if (drivercorporate != null)
            {

                result.driverId = driver.Id;
                result.driverType = driver.DriverType;
                result.corporate = drivercorporate.Addresses.Where(x => x.addressType == "tax").ToList();
                if (result.corporate.Count() ==0)
                {
                    result.corporate = drivercorporate.Addresses.Where(x => x.addressType == "register" && x.isMainData == true).ToList();
                }

            }
            return new Response<TaxDriverAddressViewModel>(_mapper.Map<TaxDriverAddressViewModel>(result));
        }
    }
}
