﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.Driver.Backoffice;
using iTrans.Transportation.Application.DTOs.DriverCar;
using iTrans.Transportation.Application.DTOs.DriverFile;
using iTrans.Transportation.Application.DTOs.UserLevel;
using iTrans.Transportation.Application.DTOs.VerifyStatus;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Helper;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;

namespace iTrans.Transportation.Application.Features.Drivers.Queries
{
    public class GetAllDriver : IRequest<PagedResponse<IEnumerable<DriverViewModel>>>
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public string Search { get; set; }
        public string Drivertype { get; set; }
        public int Active { set; get; }
        public string Column { set; get; }

    }
    public class GetAllDriverHandler : IRequestHandler<GetAllDriver, PagedResponse<IEnumerable<DriverViewModel>>>
    {
        private readonly IDriverRepositoryAsync _driverRepository;
        private readonly IDriverFileRepositoryAsync _driverFileRepository;
        private readonly IUserLevelRepositoryAsync _userLevelRepository;
        private readonly IDriverCarRepositoryAsync _driverCarRepository;
        private readonly IMapper _mapper;

        public GetAllDriverHandler(IDriverRepositoryAsync driverRepository, IUserLevelRepositoryAsync userLevelRepository, IDriverFileRepositoryAsync driverFileRepository, IDriverCarRepositoryAsync driverCarRepository, IMapper mapper)
        {
            _driverRepository = driverRepository;
            _driverFileRepository = driverFileRepository;
            _userLevelRepository = userLevelRepository;
            _driverCarRepository = driverCarRepository;
            _mapper = mapper;
        }

        public async Task<PagedResponse<IEnumerable<DriverViewModel>>> Handle(GetAllDriver request, CancellationToken cancellationToken)
        {
            try
            {
                var folderDetails = Path.Combine(Directory.GetCurrentDirectory(), $"Shared\\VerifyStatus.json");
                var statusJson = System.IO.File.ReadAllText(folderDetails);
                List<VerifyStatusViewModel> verifyStatus = Newtonsoft.Json.JsonConvert.DeserializeObject<List<VerifyStatusViewModel>>(statusJson);
                var validFilter = _mapper.Map<GetAllDriver>(request);
                var file = _mapper.Map<List<DriverFileViewModel>>((await _driverFileRepository.GetAllAsync()));

                var result = from x in _driverRepository.GetAllAsync().Result.Where(x => x.DriverType.Contains(validFilter.Drivertype)).ToList().OrderByDescending(x => x.Modified)
                             join u in _userLevelRepository.GetAllAsync().Result.Where(x => x.Module == "driver" && x.Active == true) on x.Level equals u.Level
                             join v in verifyStatus on x.VerifyStatus equals v.id
                             select new DriverViewModel
                             {
                                 Id = x.Id,
                                 DriverType = x.DriverType != null ? x.DriverType : "-",
                                 Name = x.Name != null ? x.Name.Replace("  ", " ") : "-",
                                 PhoneNumber = String.Format("{0}{1}", x.PhoneCode, x.PhoneNumber),
                                 IdentityNumber = x.IdentityNumber != null ? AESMgr.Decrypt(x.IdentityNumber) : "-",
                                 Created = x.Created.ToString("yyyy/MM/dd HH:MM") != null ? x.Created.ToString("yyyy/MM/dd HH:MM") : "-",
                                 Files = file.Where(f => f.driverId.Equals(x.Id)).ToList(),
                                 LevelName = u.Name_TH != null ? u.Name_TH : "-",
                                 VerifyStatusName = v.th != null ? v.th : "-",
                                 VerifyStatusColor = v.color != null ? v.color : "-",
                                 CountCar = _driverCarRepository.GetAllAsync().Result.Where(c => c.Owner != null && c.Owner.Id.Equals(x.Id)).Count(),
                                 Status = x.Status
                             };
                if (validFilter.Search != null && validFilter.Search.Trim() != "")
                {
                    result = result.Where(l => l.DriverType.Trim().ToLower().Contains(validFilter.Search.Trim().ToLower()) || l.LevelName.Trim().ToLower().Contains(validFilter.Search.Trim().ToLower())
                           || l.VerifyStatusName.Trim().ToLower().Contains(validFilter.Search.Trim().ToLower()) || l.Name.Trim().ToLower().Contains(validFilter.Search.Trim().ToLower())
                           || l.PhoneNumber.Trim().ToLower().Contains(validFilter.Search.Trim().ToLower()) || l.IdentityNumber.Trim().ToLower().Contains(validFilter.Search.Trim().ToLower())
                           || l.Created.Trim().ToLower().Contains(validFilter.Search.Trim().ToLower()) || l.CountCar.Equals(validFilter.Search.Trim())).ToList();
                }
                var itemCount = result.Count();
                if (request.Column == "name" && request.Active == 2)
                {
                    result = result.OrderByDescending(x => x.Name);
                }
                if (request.Column == "name" && request.Active == 3)
                {
                    result = result.OrderBy(x => x.Name);
                }
                if (request.Column == "identityNumber" && request.Active == 2)
                {
                    result = result.OrderByDescending(x => x.IdentityNumber);
                }
                if (request.Column == "phoneNumber" && request.Active == 3)
                {
                    result = result.OrderBy(x => x.PhoneNumber);
                }
                if (request.Column == "phoneNumber" && request.Active == 2)
                {
                    result = result.OrderByDescending(x => x.PhoneNumber);
                }
                if (request.Column == "driverType" && request.Active == 3)
                {
                    result = result.OrderBy(x => x.DriverType);
                }
                if (request.Column == "driverType" && request.Active == 2)
                {
                    result = result.OrderByDescending(x => x.DriverType);
                }
                if (request.Column == "levelName" && request.Active == 3)
                {
                    result = result.OrderBy(x => x.LevelName);
                }
                if (request.Column == "levelName" && request.Active == 2)
                {
                    result = result.OrderByDescending(x => x.LevelName);
                }
                if (request.Column == "verifyStatusName" && request.Active == 3)
                {
                    result = result.OrderBy(x => x.VerifyStatusName);
                }
                if (request.Column == "verifyStatusName" && request.Active == 2)
                {
                    result = result.OrderByDescending(x => x.VerifyStatusName);
                }
                if (request.Column == "countCar" && request.Active == 3)
                {
                    result = result.OrderBy(x => x.CountCar);
                }
                if (request.Column == "countCar" && request.Active == 2)
                {
                    result = result.OrderByDescending(x => x.CountCar);
                }
                if (request.Column == "created" && request.Active == 3)
                {
                    result = result.OrderBy(x => x.Created);
                }
                if (request.Column == "created" && request.Active == 2)
                {
                    result = result.OrderByDescending(x => x.Created);
                }


                result = result.Skip((validFilter.PageNumber - 1) * validFilter.PageSize).Take(validFilter.PageSize);

                return new PagedResponse<IEnumerable<DriverViewModel>>(result, validFilter.PageNumber, validFilter.PageSize, itemCount);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
