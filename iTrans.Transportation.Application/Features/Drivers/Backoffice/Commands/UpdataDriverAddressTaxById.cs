﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.Driver.Backoffice;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;

namespace iTrans.Transportation.Application.Features.Drivers.Backoffice.Commands
{
    public class UpdataDriverAddressTaxById : IRequest<Response<int>>
    {
        public Guid? UserId { get; set; }
        public List<LocationViewModel> location { get; set; }
    }

    public class UpdataDriverAddressByIdHandler : IRequestHandler<UpdataDriverAddressTaxById, Response<int>>
    {
        private readonly IDriverAddressRepositoryAsync _driverAddressRepository;
        private readonly ICountryRepositoryAsync _countryRepository;
        private readonly IProvinceRepositoryAsync _provinceRepository;
        private readonly IDistrictRepositoryAsync _districtRepository;
        private readonly ISubdistrictRepositoryAsync _subdistrictRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;

        public UpdataDriverAddressByIdHandler(IDriverAddressRepositoryAsync driverAddressRepository, ICountryRepositoryAsync countryRepository, IProvinceRepositoryAsync provinceRepository, IDistrictRepositoryAsync districtRepository, ISubdistrictRepositoryAsync subdistrictRepository, IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _driverAddressRepository = driverAddressRepository;
            _countryRepository = countryRepository;
            _provinceRepository = provinceRepository;
            _districtRepository = districtRepository;
            _subdistrictRepository = subdistrictRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(UpdataDriverAddressTaxById request, CancellationToken cancellationToken)
        {
            foreach (var i in request.location)
            {
               

                var driverAddress = (await _driverAddressRepository.FindByCondition(x => x.Id == i.Id && x.DriverId.Equals(i.DriverId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            
            if (driverAddress == null)
            {
                throw new ApiException($"DriverAddress Not Found.");
            }
            else
            {
                    if (i.IsMainData)
                {
                    (await _driverAddressRepository.CreateSQLQuery("UPDATE Driver_Address SET IsMainData  = 0 where DriverId = '" + i.DriverId.ToString() + "' and AddressType = '" + i.AddressType.Trim() + "' ").ConfigureAwait(false)).ExecuteUpdate();
                }
                Country country = (await _countryRepository.FindByCondition(x => x.Id.Equals(i.CountryId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                Province province = (await _provinceRepository.FindByCondition(x => x.Id.Equals(i.ProvinceId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                District district = (await _districtRepository.FindByCondition(x => x.Id.Equals(i.DistrictId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                Subdistrict subdistrict = (await _subdistrictRepository.FindByCondition(x => x.Id.Equals(i.SubdistrictId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();



                driverAddress.DriverId = i.DriverId;
                driverAddress.Branch = i.Branch;
                driverAddress.Country = country;
                driverAddress.Province = province;
                driverAddress.District = district;
                driverAddress.Subdistrict = subdistrict;
                driverAddress.PostCode = i.PostCode;
                driverAddress.Road = i.Road;
                driverAddress.Alley = i.Alley;
                driverAddress.Address = i.Address;
                driverAddress.AddressType = i.AddressType;
                driverAddress.IsMainData = i.IsMainData;
                driverAddress.Sequence = i.Sequence;
                driverAddress.Modified = DateTime.Now;

                if (i.AddressType != "tax")
                {

                    DriverAddress address = new DriverAddress
                    {
                        DriverId = i.DriverId,
                        Country = country,
                        Province = province,
                        District = district,
                        Subdistrict = subdistrict,
                        PostCode = i.PostCode,
                        Road = i.Road,
                        Alley = i.Alley,
                        Address = i.Address,
                        Branch = "",
                        AddressType = "tax",
                        AddressName = "",
                        ContactPerson = "",
                        ContactPhoneNumber = "",
                        ContactEmail = "",
                        IsMainData = true,
                        Maps = "",
                        Sequence = 1,
                        Created = DateTime.Now,
                        Modified = DateTime.Now,
                        CreatedBy = "xxxxxxx",
                        ModifiedBy = "xxxxxxx"
                    };

                    await _driverAddressRepository.AddAsync(address);

                }
                else
                {

                    await _driverAddressRepository.UpdateAsync(driverAddress);

                }

              }
           }
            var log = new CreateAppLog(_applicationLogRepository);
            log.CreateLog("driver", "driver", "edit", Newtonsoft.Json.JsonConvert.SerializeObject(request), request.UserId.ToString());

            return new Response<int>(request.location[0].Id);

        }
    }
}
