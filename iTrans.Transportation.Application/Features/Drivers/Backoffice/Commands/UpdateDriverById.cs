﻿using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Interfaces.Services;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using iTrans.Transportation.Application.DTOs.OrderingStatus;
using iTrans.Transportation.Application.DTOs.Ordering;
using iTrans.Transportation.Application.DTOs.OrderingProduct;
using iTrans.Transportation.Application.DTOs.CarType;
using iTrans.Transportation.Application.DTOs.CarList;
using iTrans.Transportation.Application.DTOs.ProductType;
using Title = iTrans.Transportation.Domain.Title;
using iTrans.Transportation.Application.DTOs.ProductPackaging;
using iTrans.Transportation.Application.DTOs.OrderingAddress;
using iTrans.Transportation.Application.Helper;

namespace iTrans.Transportation.Application.Features.Drivers.Backoffice.Commands
{
    public partial class UpdateDriverById : IRequest<Response<Guid>>
    {
        public Guid? UserId { get; set; }
        public Guid Id { get; set; }
        public int CorporateTypeId { get; set; }
        public int TitleId { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Name { get; set; }
        public int IdentityType { get; set; }
        public string IdentityNumber { get; set; }
        public DateTime Birthday { get; set; }
        public string PhoneCode { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string Facbook { get; set; }
        public string Line { get; set; }
        public string Twitter { get; set; }
        public string Whatapp { get; set; }
        public string Wechat { get; set; }
        public int Level { get; set; }
        public int CountryId { get; set; }
        public int ProvinceId { get; set; }
        public int DistrictId { get; set; }
        public int SubdistrictId { get; set; }
        public int ContactPersonTitleId { get; set; }
        public string ContactPersonFirstName { get; set; }
        public string ContactPersonMiddleName { get; set; }
        public string ContactPersonLastName { get; set; }

        public string PostCode { get; set; }
        public string Road { get; set; }
        public string Alley { get; set; }
        public string Address { get; set; }
        public string idCard { get; set; }
        public bool Status { get; set; }
        public string drivingLicense { get; set; }
        public string driverPicture { get; set; }

        public int VerifyStatus { get; set; }

        public string APP_ID { get; set; }
        public string REST_API_KEY { get; set; }
        public string AUTH_ID { get; set; }
        public string ContentDirectory { get; set; }
        public IFormFile ProfilePicture { get; set; }

    }

    public class UpdateDriverByIdCommandHandler : IRequestHandler<UpdateDriverById, Response<Guid>>
    {
        private readonly IDriverRepositoryAsync _driverRepository;
        private readonly IOrderingRepositoryAsync _orderingRepository;
        private readonly IDriverAddressRepositoryAsync _driverAddressRepository;
        private readonly IDriverFileRepositoryAsync _driverFileRepository;
        private readonly ICorporateTypeRepositoryAsync _corporateTypeRepository;
        private readonly ITitleRepositoryAsync _titleRepository;
        private readonly ICountryRepositoryAsync _countryRepository;
        private readonly IProvinceRepositoryAsync _provinceRepository;
        private readonly IDistrictRepositoryAsync _districtRepository;
        private readonly ISubdistrictRepositoryAsync _subdistrictRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;
        private readonly IConfiguration _configuration;
        private readonly INotificationRepositoryAsync _notificationRepository;
        private readonly INotificationUserRepositoryAsync _notificationUserRepository;
        private readonly IProductTypeRepositoryAsync _productTypeRepository;
        private readonly IProductPackagingRepositoryAsync _productPackagingRepository;
        public UpdateDriverByIdCommandHandler(
            IDriverRepositoryAsync driverRepository, IDriverAddressRepositoryAsync driverAddressRepository, IDriverFileRepositoryAsync driverFileRepository, ICorporateTypeRepositoryAsync corporateTypeRepository
            , ITitleRepositoryAsync titleRepository, ICountryRepositoryAsync countryRepository, IProvinceRepositoryAsync provinceRepository, IDistrictRepositoryAsync districtRepository
            , INotificationRepositoryAsync notificationRepository, INotificationUserRepositoryAsync notificationUserRepository, ISubdistrictRepositoryAsync subdistrictRepository
            , IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper, IConfiguration configuration, IOrderingRepositoryAsync orderingRepository
            , IProductTypeRepositoryAsync productTypeRepository, IProductPackagingRepositoryAsync productPackagingRepository)
        {
            _driverRepository = driverRepository;
            _driverAddressRepository = driverAddressRepository;
            _driverFileRepository = driverFileRepository;
            _driverRepository = driverRepository;
            _corporateTypeRepository = corporateTypeRepository;
            _titleRepository = titleRepository;
            _countryRepository = countryRepository;
            _provinceRepository = provinceRepository;
            _districtRepository = districtRepository;
            _subdistrictRepository = subdistrictRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
            _configuration = configuration;
            _notificationRepository = notificationRepository;
            _notificationUserRepository = notificationUserRepository;
            _orderingRepository = orderingRepository;
            _productTypeRepository = productTypeRepository;
            _productPackagingRepository = productPackagingRepository;

        }

        public async Task<Response<Guid>> Handle(UpdateDriverById request, CancellationToken cancellationToken)
        {
           

            try
            {
                var driver = (await _driverRepository.FindByCondition(x => x.Id == request.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (driver == null)
                {
                    throw new ApiException($"Driver Not Found.");
                }
                else
                {

                    if (request.CorporateTypeId != 0)
                    {
                        CorporateType corporateType = (await _corporateTypeRepository.FindByCondition(x => x.Id.Equals(request.CorporateTypeId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                        driver.CorporateType = corporateType;
                    }

                    Title title = (await _titleRepository.FindByCondition(x => x.Id.Equals(request.TitleId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                    Title contactPersonTitle = (await _titleRepository.FindByCondition(x => x.Id.Equals(request.ContactPersonTitleId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                    Country country = (await _countryRepository.FindByCondition(x => x.Id.Equals(request.CountryId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                    Province province = (await _provinceRepository.FindByCondition(x => x.Id.Equals(request.ProvinceId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                    District district = (await _districtRepository.FindByCondition(x => x.Id.Equals(request.DistrictId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                    Subdistrict subdistrict = (await _subdistrictRepository.FindByCondition(x => x.Id.Equals(request.SubdistrictId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();

                    var IdentityNumber = AESMgr.Encrypt(request.IdentityNumber);
                    string phoneNumber = request.PhoneNumber != null ? request.PhoneNumber : "";
                    if (request.PhoneNumber != null && request.PhoneNumber.Length > 9 && request.PhoneNumber.First() == '0')
                    {
                        phoneNumber = request.PhoneNumber.Remove(0, 1);
                    }
                    driver.Title = title;
                    driver.FirstName = request.FirstName != null ? request.FirstName : "";
                    driver.MiddleName = request.MiddleName != null ? request.MiddleName : "";
                    driver.LastName = request.LastName != null ? request.LastName : "";
                    driver.Name = request.Name != null ? request.Name : "";
                    driver.IdentityType = request.IdentityType != null ? request.IdentityType : 0;
                    driver.IdentityNumber = IdentityNumber;
                    driver.ContactPersonTitle = contactPersonTitle;
                    driver.ContactPersonFirstName = request.ContactPersonFirstName != null ? request.ContactPersonFirstName : "";
                    driver.ContactPersonMiddleName = request.ContactPersonMiddleName != null ? request.ContactPersonFirstName : "";
                    driver.ContactPersonLastName = request.ContactPersonLastName != null ? request.ContactPersonFirstName : "";
                    driver.Birthday = request.Birthday;
                    driver.Level = request.Level;
                    driver.PhoneNumber = phoneNumber;
                    driver.Email = request.Email != null ? request.Email : "";
                    driver.Facbook = request.Facbook != null ? request.Facbook : "";
                    driver.Line = request.Line != null ? request.Line : "";
                    driver.Twitter = request.Twitter != null ? request.Twitter : "";
                    driver.Whatapp = request.Whatapp != null ? request.Whatapp : "";
                    driver.VerifyStatus = request.VerifyStatus;
                    driver.Facbook = request.Facbook != null ? request.Facbook : "";
                    driver.Line = request.Line != null ? request.Line : "";
                    driver.Twitter = request.Twitter != null ? request.Twitter : "";
                    driver.Whatapp = request.Whatapp != null ? request.Whatapp : "";
                    driver.Wechat = request.Wechat != null ? request.Wechat : "";
                    driver.Status = request.Status;
                    driver.Modified = DateTime.Now;
                    driver.CreatedBy = "xxxxxxx";
                    driver.ModifiedBy = "xxxxxxx";

                    await _driverRepository.UpdateAsync(driver);
 

                    //string folderPath = request.ContentDirectory + "driver/" + request.Id.ToString();
                    //if (!Directory.Exists(folderPath))
                    //{
                    //    Directory.CreateDirectory(folderPath);
                    //}
                    //if (request.ProfilePicture != null )
                    //{
                     
                    //    var file = _driverFileRepository.FindByCondition(f => f.DocumentType.Equals("profilepicture") && f.DriverId.Equals(request.Id)).Result.FirstOrDefault();
                    //    var fileCount = _driverFileRepository.FindByCondition(f =>  f.DriverId.Equals(request.Id)).Result.Count();
                    //    if (file != null)
                    //    {

                    //        string filePath = Path.Combine(folderPath, request.ProfilePicture.FileName);
                    //        using (FileStream fileStream = new FileStream(filePath, FileMode.Create))
                    //        {
                    //            await request.ProfilePicture.CopyToAsync(fileStream);
                    //            FileInfo fi = new FileInfo(filePath);


                    //            file.DriverId = request.Id;
                    //            file.FileName = request.ProfilePicture.FileName;
                    //            file.ContentType = request.ProfilePicture.ContentType;
                    //            file.FilePath = "driver/" + request.Id.ToString() + "/" + request.ProfilePicture.FileName;
                    //            file.DirectoryPath = "driver/" + request.Id.ToString() + "/";
                    //            file.FileEXT = fi.Extension;
                    //            file.DocumentType = "profilepicture";
                    //            file.Sequence = file.Sequence;

                                
                    //            await _driverFileRepository.UpdateAsync(file);
                    //        }
                    //    }
                    //    else
                    //    {
                    //        string filePath = Path.Combine(folderPath, request.ProfilePicture.FileName);
                    //        using (FileStream fileStream = new FileStream(filePath, FileMode.Create))
                    //        {
                    //            await request.ProfilePicture.CopyToAsync(fileStream);
                    //            FileInfo fi = new FileInfo(filePath);

                    //            DriverFile driverFile = new DriverFile
                    //            {
                    //                DriverId = request.Id,
                    //                FileName = request.ProfilePicture.FileName,
                    //                ContentType = request.ProfilePicture.ContentType,
                    //                FilePath = "driver/" + request.Id.ToString() + "/" + request.ProfilePicture.FileName,
                    //                DirectoryPath = "driver/" + request.Id.ToString() + "/",
                    //                FileEXT = fi.Extension,
                    //                DocumentType = "profilepicture",
                    //                Sequence = fileCount + 1
                    //            };
                    //           await _driverFileRepository.AddAsync(driverFile);
                    //        }
                    //    }
                    //}
                    var driverAddress = (await _driverAddressRepository.FindByCondition(x => x.DriverId == request.Id && x.AddressType == "register").ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                    if (driverAddress == null)
                    {
                        DriverAddress address = new DriverAddress
                        {
                            DriverId = request.Id,
                            Country = country,
                            Province = province,
                            District = district,
                            Subdistrict = subdistrict,
                            PostCode = request.PostCode != null ? request.PostCode : "",
                            Road = request.Road != null ? request.Road : "",
                            Alley = request.Alley != null ? request.Alley : "",
                            Address = request.Address != null ? request.Address : "",
                            Branch = "",
                            AddressType = "register",
                            AddressName = "",
                            ContactPerson = "",
                            ContactPhoneNumber = "",
                            ContactEmail = "",
                            IsMainData = true,
                            Maps = "",
                            Sequence = 1,
                            Modified = DateTime.Now,
                            CreatedBy = "xxxxxxx",
                            ModifiedBy = "xxxxxxx"
                        };
                        var driverAddressObject = await _driverAddressRepository.AddAsync(address);
                    }
                    else
                    {
                        driverAddress.DriverId = request.Id;
                        driverAddress.Country = country;
                        driverAddress.Province = province;
                        driverAddress.District = district;
                        driverAddress.Subdistrict = subdistrict;
                        driverAddress.PostCode = request.PostCode != null ? request.PostCode : "";
                        driverAddress.Road = request.Road != null ? request.Road : "";
                        driverAddress.Alley = request.Alley != null ? request.Alley : "";
                        driverAddress.Address = request.Address != null ? request.Address : "";
                        driverAddress.IsMainData = true;
                        await _driverAddressRepository.UpdateAsync(driverAddress);
                    }
                    if (request.VerifyStatus == 2)
                    {
                        foreach (DriverFile customerFile in driver.Files)
                        {
                            if (customerFile.IsDelete && File.Exists(Path.Combine(request.ContentDirectory, customerFile.FilePath)))
                                File.Delete(Path.Combine(request.ContentDirectory, customerFile.FilePath));
                        }
                        (await _driverFileRepository.CreateSQLQuery("DELETE Driver_File where DriverId = '" + request.Id.ToString() + "' and IsDelete = 1").ConfigureAwait(false)).ExecuteUpdate();
                        (await _driverFileRepository.CreateSQLQuery("UPDATE Driver_File  SET IsApprove = 1 where DriverId = '" + request.Id.ToString() + "'").ConfigureAwait(false)).ExecuteUpdate();

                    }
                    if (request.VerifyStatus == 1)
                    {
                        #region add noti list
                        var oneSignalIds = (await _notificationUserRepository.FindByCondition(x => x.UserId.Equals(request.Id)).ConfigureAwait(false)).AsQueryable().Select(x => x.OneSignalId).ToList();
                        Domain.Notification notification = new Domain.Notification();

                        notification.OneSignalId = oneSignalIds.FirstOrDefault();
                        notification.UserId = request.Id;
                        notification.OwnerId = request.Id;
                        notification.Title = "Documents do not pass";
                        notification.Detail = "Documents do not pass";
                        notification.ServiceCode = "Reject Doc";
                        //notification.ReferenceContentKey = request.ReferenceContentKey;
                        //notification.Payload = Newtonsoft.Json.JsonConvert.SerializeObject(cloneOrdering);
                        notification.Created = DateTime.UtcNow;
                        notification.Modified = DateTime.UtcNow;
                        //notification.CreatedBy = ;
                        //notification.ModifiedBy = ;
                        await _notificationRepository.AddAsync(notification);
                        #endregion

                        #region send noti

                        if (oneSignalIds.Count > 0)
                        {
                            NotificationManager notiMgr = new NotificationManager(request.APP_ID, request.REST_API_KEY, request.AUTH_ID);
                            notiMgr.SendNotificationAsync(notification.Title, notification.Detail, oneSignalIds, notification.ServiceCode, "", "");
                        }

                        #endregion
                        List<Ordering> orderings = new List<Ordering>();
                        if (driver.DriverType == "corporate")
                        {
                            var drivers = (await _driverRepository.FindByCondition(x => x.Corparate == request.Id).ConfigureAwait(false)).AsQueryable().ToList();

                            foreach (Driver item in drivers)
                            {
                                var orders = (await _orderingRepository.FindByCondition(x => x.Driver.Id == item.Id && (x.Status == 1 || x.Status == 2 || x.Status == 201 || x.Status == 3)).ConfigureAwait(false)).AsQueryable().ToList();
                                orderings.AddRange(orders);
                            }

                        }
                        else
                        {
                            var orders = (await _orderingRepository.FindByCondition(x => x.Driver.Id == request.Id && (x.Status == 1 || x.Status == 2 || x.Status == 201 || x.Status == 3)).ConfigureAwait(false)).AsQueryable().ToList();
                            orderings.AddRange(orders);
                        }

                        var productTypes = (await _productTypeRepository.FindByCondition(x => x.Active.Equals(true)).ConfigureAwait(false)).AsQueryable().ToList();
                        var packagings = (await _productPackagingRepository.FindByCondition(x => x.Active.Equals(true)).ConfigureAwait(false)).AsQueryable().ToList();

                        var folderDetails = Path.Combine(Directory.GetCurrentDirectory(), $"Shared\\OrderingStatus.json");
                        var statusJson = System.IO.File.ReadAllText(folderDetails);
                        List<OrderingStatusViewModel> orderingStatus = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OrderingStatusViewModel>>(statusJson);

                        var notificationDetails = Path.Combine(Directory.GetCurrentDirectory(), $"Shared\\NotificationWord.json");
                        var notificationJson = System.IO.File.ReadAllText(notificationDetails);
                        List<NotificationWordStatusViewModel> notificationStatus = Newtonsoft.Json.JsonConvert.DeserializeObject<List<NotificationWordStatusViewModel>>(notificationJson);
                        string notiTitle = "";
                        string notiDetail = "";
                        var notificationObj = notificationStatus.Where(o => o.id == 21).FirstOrDefault();
                        if (notificationObj != null)
                        {
                            notiTitle = notificationObj.title.th;
                            notiDetail = notificationObj.detail.th;

                        }

                        foreach (Ordering orderingItem in orderings)
                        {
                            (await _orderingRepository.CreateSQLQuery("UPDATE Ordering set Status = 15  WHERE Id = '" + orderingItem.Id.ToString() + "'").ConfigureAwait(false)).ExecuteUpdate();
                            #region
                            //var cloneOrdering = _mapper.Map<OrderingViewModel>(data);
                            var cloneOrdering = new OrderingNotificationViewModel
                            {
                                id = orderingItem.Id,
                                status = orderingItem.Status,
                                trackingCode = orderingItem.TrackingCode,
                                orderNumber = orderingItem.TrackingCode,
                                products = _mapper.Map<List<OrderingProductNotificationViewModel>>(orderingItem.Products)
                            };

                            if (cloneOrdering.status != null)
                            {
                                cloneOrdering.statusObj = orderingStatus.Where(o => o.id == cloneOrdering.status).FirstOrDefault();
                            }
                            if (orderingItem.Cars.Count > 0)
                            {
                                cloneOrdering.carType = _mapper.Map<CarTypeViewModel>(orderingItem.Cars[0].CarType);
                                cloneOrdering.carList = _mapper.Map<CarListViewModel>(orderingItem.Cars[0].CarList);
                            }

                            foreach (OrderingProductNotificationViewModel productViewModel in cloneOrdering.products)
                            {
                                if (productViewModel.productType != null)
                                {
                                    string[] productTypeIds = productViewModel.productType.Split(",");
                                    productViewModel.productTypes = new List<ProductTypeViewModel>();
                                    foreach (string productId in productTypeIds)
                                    {
                                        int id = 0;
                                        if (int.TryParse(productId, out id))
                                        {
                                            var productType = productTypes.Where(p => p.Id == id).FirstOrDefault();
                                            if (productType != null)
                                                productViewModel.productTypes.Add(_mapper.Map<ProductTypeViewModel>(productType));
                                        }
                                    }
                                }
                                if (productViewModel.packaging != null)
                                {
                                    string[] packagingIds = productViewModel.packaging.Split(",");
                                    productViewModel.packagings = new List<ProductPackagingViewModel>();
                                    foreach (string packagingId in packagingIds)
                                    {
                                        int id = 0;
                                        if (int.TryParse(packagingId, out id))
                                        {
                                            var packaging = packagings.Where(p => p.Id == id).FirstOrDefault();
                                            if (packaging != null)
                                                productViewModel.packagings.Add(_mapper.Map<ProductPackagingViewModel>(packaging));
                                        }
                                    }
                                }

                            }
                            if (orderingItem.Addresses.Count > 0)
                            {
                                cloneOrdering.startPoint = _mapper.Map<OrderingAddressNotificationViewModel>(orderingItem.Addresses[0]);
                                cloneOrdering.endPoint = _mapper.Map<OrderingAddressNotificationViewModel>(orderingItem.Addresses[orderingItem.Addresses.Count - 1]);
                            }
                            #endregion

                            if (orderingItem.Customer != null)
                            {
                                #region add noti list
                                var customerSignalIds = (await _notificationUserRepository.FindByCondition(x => x.UserId.Equals(orderingItem.Customer.Id)).ConfigureAwait(false)).AsQueryable().Select(x => x.OneSignalId).ToList();
                                Domain.Notification customerNoti = new Domain.Notification();

                                customerNoti.OneSignalId = customerSignalIds.FirstOrDefault();
                                customerNoti.UserId = orderingItem.Customer.Id;
                                //customerNoti.OwnerId = request.Id;
                                customerNoti.Title = notiTitle;
                                customerNoti.Detail = notiDetail;
                                customerNoti.ServiceCode = "Ordering";
                                //notification.ReferenceContentKey = request.ReferenceContentKey;
                                notification.Payload = Newtonsoft.Json.JsonConvert.SerializeObject(cloneOrdering);
                                customerNoti.Created = DateTime.UtcNow;
                                customerNoti.Modified = DateTime.UtcNow;
                                //notification.CreatedBy = ;
                                //notification.ModifiedBy = ;
                                await _notificationRepository.AddAsync(customerNoti);
                                #endregion

                                #region send noti

                                if (customerSignalIds.Count > 0)
                                {
                                    NotificationManager notiMgr = new NotificationManager(request.APP_ID, request.REST_API_KEY, request.AUTH_ID);
                                    notiMgr.SendNotificationAsync(customerNoti.Title, customerNoti.Detail, customerSignalIds, customerNoti.ServiceCode, "", "{id:'" + cloneOrdering.id + "'}");
                                }
                                #endregion
                            }
                            if (orderingItem.Driver != null)
                            {
                                #region add noti list
                                var driverSignalIds = (await _notificationUserRepository.FindByCondition(x => x.UserId.Equals(orderingItem.Driver.Id)).ConfigureAwait(false)).AsQueryable().Select(x => x.OneSignalId).ToList();
                                Domain.Notification driverNoti = new Domain.Notification();

                                driverNoti.OneSignalId = driverSignalIds.FirstOrDefault();
                                driverNoti.UserId = orderingItem.Driver.Id;
                                //driverNoti.OwnerId = request.Id;
                                driverNoti.Title = notiTitle;
                                driverNoti.Detail = notiDetail;
                                driverNoti.ServiceCode = "Ordering";
                                //notification.ReferenceContentKey = request.ReferenceContentKey;
                                notification.Payload = Newtonsoft.Json.JsonConvert.SerializeObject(cloneOrdering);
                                driverNoti.Created = DateTime.UtcNow;
                                driverNoti.Modified = DateTime.UtcNow;
                                //notification.CreatedBy = ;
                                //notification.ModifiedBy = ;
                                await _notificationRepository.AddAsync(driverNoti);
                                #endregion

                                #region send noti

                                if (driverSignalIds.Count > 0)
                                {
                                    NotificationManager notiMgr = new NotificationManager(request.APP_ID, request.REST_API_KEY, request.AUTH_ID);
                                    notiMgr.SendNotificationAsync(driverNoti.Title, driverNoti.Detail, driverSignalIds, driverNoti.ServiceCode, "", "{id:'" + cloneOrdering.id + "'}");
                                }
                                #endregion
                            }

                        }
                    }
                    var log = new CreateAppLog(_applicationLogRepository);
                    log.CreateLog("Driver", "Driver", "Update", Newtonsoft.Json.JsonConvert.SerializeObject(request), request.UserId.ToString());
                    return new Response<Guid>(driver.Id);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
        }
    }






