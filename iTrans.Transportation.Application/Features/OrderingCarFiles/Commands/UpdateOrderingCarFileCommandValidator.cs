﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;


namespace iTrans.Transportation.Application.Features.OrderingCarFiles.Commands
{
    public class UpdateOrderingCarFileCommandValidator : AbstractValidator<UpdateOrderingCarFileCommand>
    {
        private readonly IOrderingCarFileRepositoryAsync orderingCarFileRepository;

        public UpdateOrderingCarFileCommandValidator(IOrderingCarFileRepositoryAsync orderingCarFileRepository)
        {
            this.orderingCarFileRepository = orderingCarFileRepository;

            RuleFor(p => p.OrderingCarId)
                .NotNull().WithMessage("{PropertyName} is required.");

            RuleFor(p => p.FileName)
                .NotEmpty().WithMessage("{PropertyName} is required.")
                .NotNull();

            RuleFor(p => p.ContentType)
               .NotEmpty().WithMessage("{PropertyName} is required.")
               .NotNull();

            RuleFor(p => p.FilePath)
               .NotEmpty().WithMessage("{PropertyName} is required.")
               .NotNull();

            RuleFor(p => p.FileEXT)
               .NotEmpty().WithMessage("{PropertyName} is required.")
               .NotNull();

            RuleFor(p => p.DocumentType)
               .NotEmpty().WithMessage("{PropertyName} is required.")
               .NotNull();
        }

        private async Task<bool> IsUnique(string value, CancellationToken cancellationToken)
        {
            var orderingCarFileObject = (await orderingCarFileRepository.FindByCondition(x => x.FileName.ToLower() == value.ToLower()).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            if (orderingCarFileObject != null)
            {
                return false;
            }
            return true;
        }
    }
}
