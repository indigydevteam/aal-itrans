﻿using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
namespace iTrans.Transportation.Application.Features.OrderingCarFiles.Commands
{
    public class DeleteOrderingCarFileByIdCommand : IRequest<Response<int>>
    {
        public int Id { get; set; }
        public class DeleteOrderingCarFileByIdCommandHandler : IRequestHandler<DeleteOrderingCarFileByIdCommand, Response<int>>
        {
            private readonly IOrderingCarFileRepositoryAsync _orderingCarFileRepository;
            public DeleteOrderingCarFileByIdCommandHandler(IOrderingCarFileRepositoryAsync orderingCarFileRepository)
            {
                _orderingCarFileRepository = orderingCarFileRepository;
            }
            public async Task<Response<int>> Handle(DeleteOrderingCarFileByIdCommand command, CancellationToken cancellationToken)
            {
                var orderingCarFile = (await _orderingCarFileRepository.FindByCondition(x => x.Id == command.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (orderingCarFile == null)
                {
                    throw new ApiException($"OrderingCarFile Not Found.");
                }
                else
                {
                    await _orderingCarFileRepository.DeleteAsync(orderingCarFile);
                    return new Response<int>(orderingCarFile.Id);
                }
            }
        }
    }
}
