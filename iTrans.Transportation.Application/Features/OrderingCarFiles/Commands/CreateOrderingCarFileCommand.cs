﻿using AutoMapper;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Interfaces.Services;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.OrderingCarFiles.Commands
{
    public partial class CreateOrderingCarFileCommand : IRequest<Response<int>>
    {
        public int Id { get; set; }
        public int OrderingCarId { get; set; }
        public string FileName { get; set; }
        public  string ContentType { get; set; }
        public  string FilePath { get; set; }
        public  string FileEXT { get; set; }
        public  string DocumentType { get; set; }
        public  int Sequence { get; set; }

    }
    public class CreateOrderingCarFileCommandHandler : IRequestHandler<CreateOrderingCarFileCommand, Response<int>>
    {
        private readonly IOrderingCarFileRepositoryAsync _orderingCarFileRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;
        public CreateOrderingCarFileCommandHandler(IOrderingCarFileRepositoryAsync orderingCarFileRepository,IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _orderingCarFileRepository = orderingCarFileRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(CreateOrderingCarFileCommand request, CancellationToken cancellationToken)
        {
            var orderingCarFile = _mapper.Map<OrderingCarFile>(request);
            orderingCarFile.Created = DateTime.UtcNow;
            orderingCarFile.Modified = DateTime.UtcNow;
            var orderingCarFileObject = await _orderingCarFileRepository.AddAsync(orderingCarFile);
            var log = new CreateAppLog(_applicationLogRepository);
            log.Create("Ordering", "OrderingCar File ", "Create", Newtonsoft.Json.JsonConvert.SerializeObject(orderingCarFile));
            return new Response<int>(orderingCarFileObject.Id);
        }
    }
}
