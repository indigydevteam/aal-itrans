﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
namespace iTrans.Transportation.Application.Features.OrderingCarFiles.Commands
{
    public class CreateOrderingCarFileCommandValidator : AbstractValidator<CreateOrderingCarFileCommand>
    {
        private readonly IOrderingCarFileRepositoryAsync orderingCarFileRepository;

        public CreateOrderingCarFileCommandValidator(IOrderingCarFileRepositoryAsync orderingCarFileRepository)
        {
            this.orderingCarFileRepository = orderingCarFileRepository;

            RuleFor(p => p.OrderingCarId)
                .NotNull().WithMessage("{PropertyName} is required.");

            RuleFor(p => p.FileName)
                .NotEmpty().WithMessage("{PropertyName} is required.")
                .NotNull();

            RuleFor(p => p.ContentType)
               .NotEmpty().WithMessage("{PropertyName} is required.")
               .NotNull();

            RuleFor(p => p.FilePath)
               .NotEmpty().WithMessage("{PropertyName} is required.")
               .NotNull();

            RuleFor(p => p.FileEXT)
               .NotEmpty().WithMessage("{PropertyName} is required.")
               .NotNull();

            RuleFor(p => p.DocumentType)
               .NotEmpty().WithMessage("{PropertyName} is required.")
               .NotNull();
        }
    }
}
