﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;

namespace iTrans.Transportation.Application.Features.OrderingCarFiles.Commands
{
    public class UpdateOrderingCarFileCommand : IRequest<Response<int>>
    {
        public int Id { get; set; }
        public int OrderingCarId { get; set; }
        public string FileName { get; set; }
        public string ContentType { get; set; }
        public string FilePath { get; set; }
        public string FileEXT { get; set; }
        public string DocumentType { get; set; }
        public int Sequence { get; set; }
    }

    public class UpdateOrderingCarFileCommandHandler : IRequestHandler<UpdateOrderingCarFileCommand, Response<int>>
    {
        private readonly IOrderingCarFileRepositoryAsync _orderingCarFileRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;

        public UpdateOrderingCarFileCommandHandler(IOrderingCarFileRepositoryAsync orderingCarFileRepository,IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _orderingCarFileRepository = orderingCarFileRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(UpdateOrderingCarFileCommand request, CancellationToken cancellationToken)
        {
            var orderingCarFile = (await _orderingCarFileRepository.FindByCondition(x => x.Id == request.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            if (orderingCarFile == null)
            {
                throw new ApiException($"OrderingCarFile Not Found.");
            }
            else
            {
                //orderingCarFile.OrderingCarId = request.OrderingCarId;
                orderingCarFile.FileName = request.FileName;
                orderingCarFile.ContentType = request.ContentType;
                orderingCarFile.FilePath = request.FilePath;
                orderingCarFile.FileEXT = request.FileEXT;
               // orderingCarFile.DocumentType = request.DocumentType;
                orderingCarFile.Sequence = request.Sequence;
                orderingCarFile.Modified = DateTime.UtcNow;
                await _orderingCarFileRepository.UpdateAsync(orderingCarFile);
                var log = new CreateAppLog(_applicationLogRepository);
                log.Create("Ordering", "OrderingCar File ", "Edit", Newtonsoft.Json.JsonConvert.SerializeObject(orderingCarFile));
                return new Response<int>(orderingCarFile.Id);
            }
        }
    }
}
