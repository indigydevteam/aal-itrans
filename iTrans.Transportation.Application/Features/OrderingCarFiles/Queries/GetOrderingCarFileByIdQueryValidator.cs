﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using iTrans.Transportation.Application.Interfaces.Repositories;
using FluentValidation;

namespace iTrans.Transportation.Application.Features.OrderingCarFiles.Queries
{
    public class GetOrderingCarFileByIdQueryValidator : AbstractValidator<GetOrderingCarFileByIdQuery>
    {
        private readonly IOrderingCarFileRepositoryAsync orderingCarFileRepository;

        public GetOrderingCarFileByIdQueryValidator(IOrderingCarFileRepositoryAsync orderingCarFileRepository)
        {
            this.orderingCarFileRepository = orderingCarFileRepository;
            RuleFor(p => p.Id)
               .NotEmpty().WithMessage("{PropertyName} is required.")
               .NotNull()
               .MustAsync(IsOrderingCarFileExists).WithMessage("{PropertyName} not exists.");
        }

        private async Task<bool> IsOrderingCarFileExists(int Id, CancellationToken cancellationToken)
        {
            var userObject = (await orderingCarFileRepository.FindByCondition(x => x.Id.Equals(Id)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            if (userObject != null)
            {
                return true;
            }
            return false;
        }
    }
}
