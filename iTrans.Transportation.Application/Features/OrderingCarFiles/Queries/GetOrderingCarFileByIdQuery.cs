﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.OrderingCarFile;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.OrderingCarFiles.Queries
{
   public class GetOrderingCarFileByIdQuery : IRequest<Response<OrderingCarFileViewModel>>
    {
        public int Id { get; set; }
    }
    public class GetOrderingCarFileByIdQueryHandler : IRequestHandler<GetOrderingCarFileByIdQuery, Response<OrderingCarFileViewModel>>
    {
        private readonly IOrderingCarFileRepositoryAsync _orderingCarFileRepository;
        private readonly IMapper _mapper;
        public GetOrderingCarFileByIdQueryHandler(IOrderingCarFileRepositoryAsync orderingCarFileRepository, IMapper mapper)
        {
            _orderingCarFileRepository = orderingCarFileRepository;
            _mapper = mapper;
        }
        public async Task<Response<OrderingCarFileViewModel>> Handle(GetOrderingCarFileByIdQuery request, CancellationToken cancellationToken)
        {
            var orderingCarFileObject = (await _orderingCarFileRepository.FindByCondition(x => x.Id.Equals(request.Id)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            return new Response<OrderingCarFileViewModel>(_mapper.Map<OrderingCarFileViewModel>(orderingCarFileObject));
        }
    }
}
