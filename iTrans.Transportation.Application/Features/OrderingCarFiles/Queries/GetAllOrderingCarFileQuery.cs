﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.OrderingCarFile;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.OrderingCarFiles.Queries
{
    public class GetAllOrderingCarFileQuery : IRequest<Response<IEnumerable<OrderingCarFileViewModel>>>
    {
         
    }
    public class GetAllOrderingCarFileQueryHandler : IRequestHandler<GetAllOrderingCarFileQuery, Response<IEnumerable<OrderingCarFileViewModel>>>
    {
        private readonly IOrderingCarFileRepositoryAsync _orderingCarFileRepository;
        private readonly IMapper _mapper;
        public GetAllOrderingCarFileQueryHandler(IOrderingCarFileRepositoryAsync orderingCarFileRepository, IMapper mapper)
        {
            _orderingCarFileRepository = orderingCarFileRepository;
            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<OrderingCarFileViewModel>>> Handle(GetAllOrderingCarFileQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var ordering = await _orderingCarFileRepository.GetAllAsync();
                var orderingCarFileViewModel = _mapper.Map<IEnumerable<OrderingCarFileViewModel>>(ordering);
                return new Response<IEnumerable<OrderingCarFileViewModel>>(orderingCarFileViewModel);
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
    }
}
