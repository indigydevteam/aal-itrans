﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.ProductPackaging;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.ProductPackagings.Queries
{
    public class GetAllProductPackaging : IRequest<Response<IEnumerable<ProductPackagingViewModel>>>
    {

    }
    public class GetAllProductPackagingHandler : IRequestHandler<GetAllProductPackaging, Response<IEnumerable<ProductPackagingViewModel>>>
    {
        private readonly IProductPackagingRepositoryAsync _productPackagingRepository;
        private readonly IMapper _mapper;
        public GetAllProductPackagingHandler(IProductPackagingRepositoryAsync productPackagingRepository, IMapper mapper)
        {
            _productPackagingRepository = productPackagingRepository;
            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<ProductPackagingViewModel>>> Handle(GetAllProductPackaging request, CancellationToken cancellationToken)
        {
            var productPackaging = (await _productPackagingRepository.FindByCondition(x => x.Active == true).ConfigureAwait(false)).AsQueryable().ToList();
            var productPackagingViewModel = _mapper.Map<IEnumerable<ProductPackagingViewModel>>(productPackaging);
            return new Response<IEnumerable<ProductPackagingViewModel>>(productPackagingViewModel);
        }
    }
}
