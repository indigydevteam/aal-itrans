﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.ProductPackaging;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.ProductPackagings.Queries
{
    public class GetProductPackagingById : IRequest<Response<ProductPackagingViewModel>>
    {
        public int Id { get; set; }
    }
    public class GetProductPackagingByIdHandler : IRequestHandler<GetProductPackagingById, Response<ProductPackagingViewModel>>
    {
        private readonly IProductPackagingRepositoryAsync _productPackagingRepository;
        private readonly IMapper _mapper;
        public GetProductPackagingByIdHandler(IProductPackagingRepositoryAsync productPackagingRepository, IMapper mapper)
        {
            _productPackagingRepository = productPackagingRepository;
            _mapper = mapper;
        }
        public async Task<Response<ProductPackagingViewModel>> Handle(GetProductPackagingById request, CancellationToken cancellationToken)
        {
            var productPackagingObject = (await _productPackagingRepository.FindByCondition(x => x.Id.Equals(request.Id)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            if (productPackagingObject == null)
            {
                throw new ApiException($"productPackaging Not Found.");
            }
            return new Response<ProductPackagingViewModel>(_mapper.Map<ProductPackagingViewModel>(productPackagingObject));
        }
    }
}
