﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using iTrans.Transportation.Application.Interfaces.Repositories;
using FluentValidation;

namespace iTrans.Transportation.Application.Features.ProductPackagings.Queries
{
    public class GetProductPackagingByIdValidator : AbstractValidator<GetProductPackagingById>
    {
        private readonly IProductPackagingRepositoryAsync productPackagingRepository;

        public GetProductPackagingByIdValidator(IProductPackagingRepositoryAsync productPackagingRepository)
        {
            this.productPackagingRepository = productPackagingRepository;
            RuleFor(p => p.Id)
               .NotEmpty().WithMessage("{PropertyName} is required.")
               .NotNull()
               .MustAsync(IsProductPackagingExists).WithMessage("{PropertyName} not exists.");
        }

        private async Task<bool> IsProductPackagingExists(int ProductPackagingId, CancellationToken cancellationToken)
        {
            var userObject = (await productPackagingRepository.FindByCondition(x => x.Id.Equals(ProductPackagingId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            if (userObject != null)
            {
                return true;
            }
            return false;
        }
    }
}
