﻿using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.ProductPackagings.Backoffice.Commands
{
    public class DeleteProductPackagingByIdCommand : IRequest<Response<int>>
    {
        public Guid? UserId { get; set; }
        public int Id { get; set; }
        public class DeleteProductPackagingByIdCommandHandler : IRequestHandler<DeleteProductPackagingByIdCommand, Response<int>>
        {
            private readonly IProductPackagingRepositoryAsync _productPackagingRepository;
            private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
            public DeleteProductPackagingByIdCommandHandler(IProductPackagingRepositoryAsync productPackagingRepository, IApplicationLogRepositoryAsync applicationLogRepository)
            {
                _productPackagingRepository = productPackagingRepository;
                _applicationLogRepository = applicationLogRepository;
            }
            public async Task<Response<int>> Handle(DeleteProductPackagingByIdCommand command, CancellationToken cancellationToken)
            {
                var productPackaging = (await _productPackagingRepository.FindByCondition(x => x.Id == command.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (productPackaging == null)
                {
                    throw new ApiException($"ProductPackaging Not Found.");
                }
                else
                {
                    productPackaging.Active = false;
                    productPackaging.IsDelete = true;
                    productPackaging.Modified = DateTime.UtcNow;
                    productPackaging.ModifiedBy = command.UserId != null ? command.UserId.GetValueOrDefault().ToString() : "";
                    await _productPackagingRepository.UpdateAsync(productPackaging);
                    var log = new CreateAppLog(_applicationLogRepository);
                    log.CreateLog("ProductPackaging", "ProductPackaging", "Delete", Newtonsoft.Json.JsonConvert.SerializeObject(command), command.UserId == null ? "-" : command.UserId.Value.ToString());
                    return new Response<int>(productPackaging.Id);
                }
            }
        }
    }
}
