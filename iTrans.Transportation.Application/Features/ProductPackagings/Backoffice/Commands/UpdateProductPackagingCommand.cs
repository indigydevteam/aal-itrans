﻿using System;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;
using Microsoft.AspNetCore.Http;

namespace iTrans.Transportation.Application.Features.ProductPackagings.Backoffice.Commands
{
    public class UpdateProductPackagingCommand : IRequest<Response<int>>
    {
        public Guid? UserId { get; set; }
        public int Id { get; set; }
        public string Name_TH { get; set; }
        public string Name_ENG { get; set; }
        public int Sequence { get; set; }
        public bool Active { get; set; }
        public bool Specified { get; set; }
        public IFormFile PackagingImage { get; set; }
        public string ContentDirectory { get; set; }

    }

    public class UpdateProductPackagingCommandHandler : IRequestHandler<UpdateProductPackagingCommand, Response<int>>
    {
        private readonly IProductPackagingRepositoryAsync _productPackagingRepository;
        private readonly IProductPackagingFileRepositoryAsync _productPackagingFileRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;

        public UpdateProductPackagingCommandHandler(IProductPackagingRepositoryAsync productPackagingRepository,IApplicationLogRepositoryAsync applicationLogRepository, IProductPackagingFileRepositoryAsync productPackagingFileRepository, IMapper mapper)
        {
            _productPackagingRepository = productPackagingRepository;
            _productPackagingFileRepository = productPackagingFileRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(UpdateProductPackagingCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var productPackaging = (await _productPackagingRepository.FindByCondition(x => x.Id == request.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (productPackaging == null)
                {
                    throw new ApiException($"ProductPackaging Not Found.");
                }
                else
                {
                    productPackaging.Name_TH = request.Name_TH;
                    productPackaging.Name_ENG = request.Name_ENG;
                    productPackaging.Sequence = request.Sequence;
                    productPackaging.Active = request.Active;
                    productPackaging.Specified = request.Specified;
                    productPackaging.Modified = DateTime.UtcNow;

                    await _productPackagingRepository.UpdateAsync(productPackaging);

                    string folderPath = request.ContentDirectory + "ProductPackagingFile/" + request.Name_TH.ToString();
                    if (!Directory.Exists(folderPath))
                    {
                        Directory.CreateDirectory(folderPath);
                    }
                    if (request.PackagingImage != null)
                    {
                        var file = _productPackagingFileRepository.FindByCondition(f => f.ProductPackaging.Id.Equals(request.Id)).Result.FirstOrDefault();
                        if (file != null)
                        {

                            string filePath = Path.Combine(folderPath, request.PackagingImage.FileName);
                            using (FileStream fileStream = new FileStream(filePath, FileMode.Create))
                            {
                                await request.PackagingImage.CopyToAsync(fileStream);
                                FileInfo fi = new FileInfo(filePath);

                                file.ProductPackaging = productPackaging;
                                file.FileName = request.PackagingImage.FileName;
                                file.ContentType = request.PackagingImage.ContentType;
                                file.FilePath = "ProductPackagingFile/" + request.Name_TH.ToString() + "/" + request.PackagingImage.FileName;
                                file.DirectoryPath = "ProductPackagingFile/" + request.Name_TH.ToString() + "/";
                                file.FileEXT = fi.Extension;


                                await _productPackagingFileRepository.UpdateAsync(file);
                            }
                        }
                        else
                        {
                            string filePath = Path.Combine(folderPath, request.PackagingImage.FileName);
                            using (FileStream fileStream = new FileStream(filePath, FileMode.Create))
                            {
                                await request.PackagingImage.CopyToAsync(fileStream);
                                FileInfo fi = new FileInfo(filePath);

                                ProductPackagingFile PackagingFile = new ProductPackagingFile
                                {
                                    ProductPackaging = productPackaging,
                                    FileName = request.PackagingImage.FileName,
                                    ContentType = request.PackagingImage.ContentType,
                                    FilePath = "ProductPackagingFile/" + request.Name_TH.ToString() + "/" + request.PackagingImage.FileName,
                                    DirectoryPath = "ProductPackagingFile/" + request.Name_TH.ToString() + "/",
                                    FileEXT = fi.Extension,

                                };
                                await _productPackagingFileRepository.AddAsync(PackagingFile);
                            }
                        }
                    }


                    var log = new CreateAppLog(_applicationLogRepository);
                    log.CreateLog("ProductPackaging", "Product Packaging","Edit", Newtonsoft.Json.JsonConvert.SerializeObject(request),request.UserId.ToString());
                    return new Response<int>(productPackaging.Id);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
