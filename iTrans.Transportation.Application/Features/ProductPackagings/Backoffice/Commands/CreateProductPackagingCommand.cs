﻿using AutoMapper;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Interfaces.Services;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.ProductPackagings.Backoffice.Commands
{
    public partial class CreateProductPackagingCommand : IRequest<Response<int>>
    {
        public Guid? UserId { get; set; }
        public string Name_TH { get; set; }
        public string Name_ENG { get; set; }
        public int Sequence { get; set; }
        public bool Active { get; set; }
        public  bool Specified { get; set; }
        public IFormFile PackagingImage { get; set; }
        public string ContentDirectory { get; set; }


    }
    public class CreateProductPackagingCommandHandler : IRequestHandler<CreateProductPackagingCommand, Response<int>>
    {
        private readonly IProductPackagingRepositoryAsync _productPackagingRepository;
        private readonly IProductPackagingFileRepositoryAsync _productPackagingFileRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;
        public CreateProductPackagingCommandHandler(IProductPackagingRepositoryAsync productPackagingRepository,IApplicationLogRepositoryAsync applicationLogRepository , IProductPackagingFileRepositoryAsync productPackagingFileRepository, IMapper mapper)
        {
            _productPackagingRepository = productPackagingRepository;
            _applicationLogRepository = applicationLogRepository;
            _productPackagingFileRepository = productPackagingFileRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(CreateProductPackagingCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var productPackaging = _mapper.Map<ProductPackaging>(request);
                productPackaging.Created = DateTime.UtcNow;
                productPackaging.Modified = DateTime.UtcNow;
                productPackaging.CreatedBy = request.UserId == null ? "" : request.UserId.Value.ToString();
                productPackaging.ModifiedBy = request.UserId == null ? "" : request.UserId.Value.ToString();

                var productPackagingObject = await _productPackagingRepository.AddAsync(productPackaging);

                if (request.PackagingImage != null)
                {
                    string folderPath = request.ContentDirectory + "ProductPackagingFile/" + request.Name_TH.ToString();
                    if (!Directory.Exists(folderPath))
                    {
                        Directory.CreateDirectory(folderPath);
                    }
                    string filePath = Path.Combine(folderPath, request.PackagingImage.FileName);
                    using (FileStream fileStream = new FileStream(filePath, FileMode.Create))
                    {
                        await request.PackagingImage.CopyToAsync(fileStream);
                        FileInfo fi = new FileInfo(filePath);

                        ProductPackagingFile PackagingFile = new ProductPackagingFile
                        {
                            ProductPackaging = productPackaging,
                            FileName = request.PackagingImage.FileName,
                            ContentType = request.PackagingImage.ContentType,
                            FilePath = "ProductPackagingFile/" + request.Name_TH.ToString() + "/" + request.PackagingImage.FileName,
                            DirectoryPath = "ProductPackagingFile/" + request.Name_TH.ToString() + "/",
                            FileEXT = fi.Extension,
                            Created = DateTime.Now,
                            CreatedBy = request.UserId != null ? request.UserId.Value.ToString() : "",
                            Modified = DateTime.Now,
                            ModifiedBy = request.UserId != null ? request.UserId.Value.ToString() : ""
                        };
                        await _productPackagingFileRepository.AddAsync(PackagingFile);
                    }
                }

                var log = new CreateAppLog(_applicationLogRepository);
                log.CreateLog("ProductPackaging", "Product Packaging","Create", Newtonsoft.Json.JsonConvert.SerializeObject(request),request.UserId.ToString());
                return new Response<int>(productPackagingObject.Id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
