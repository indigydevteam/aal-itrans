﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;

namespace iTrans.Transportation.Application.Features.DriverComplainAndRecommends.Commands
{
    public class UpdateDriverComplainAndRecommendCommand : IRequest<Response<int>>
    {
        public int Id { get; set; }
        public Guid DriverId { get; set; }
        public string Title { get; set; }
        public string Detail { get; set; }

        public class UpdateDriverComplainAndRecommendCommandHandler : IRequestHandler<UpdateDriverComplainAndRecommendCommand, Response<int>>
        {
            private readonly IDriverComplainAndRecommendRepositoryAsync _DriverComplainAndRecommendRepository;
            private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
            private readonly IMapper _mapper;

            public UpdateDriverComplainAndRecommendCommandHandler(IDriverComplainAndRecommendRepositoryAsync DriverComplainAndRecommendRepository, IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
            {
                _DriverComplainAndRecommendRepository = DriverComplainAndRecommendRepository;
                _applicationLogRepository = applicationLogRepository;
                _mapper = mapper;
            }

            public async Task<Response<int>> Handle(UpdateDriverComplainAndRecommendCommand request, CancellationToken cancellationToken)
            {
                var DriverComplainAndRecommend = (await _DriverComplainAndRecommendRepository.FindByCondition(x => x.Id == request.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (DriverComplainAndRecommend == null)
                {
                    throw new ApiException($"DriverComplainAndRecommend Not Found.");
                }
                else
                {
                    //DriverComplainAndRecommend.DriverId = request.DriverId;
                    DriverComplainAndRecommend.Title = request.Title;
                    DriverComplainAndRecommend.Detail = request.Detail;
                    DriverComplainAndRecommend.Modified = DateTime.UtcNow;
                    await _DriverComplainAndRecommendRepository.UpdateAsync(DriverComplainAndRecommend);
                    var log = new CreateAppLog(_applicationLogRepository);
                    log.Create("Driver", "Driver ComplainAndRecommend", "Edit", Newtonsoft.Json.JsonConvert.SerializeObject(request));
                    return new Response<int>(DriverComplainAndRecommend.Id);
                }
            }
        }
    }
}
