﻿using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Interfaces.Services;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using iTrans.Transportation.Domain.Entities;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.DriverComplainAndRecommends.Commands
{
    public partial class CreateDriverComplainAndRecommendCommand : IRequest<Response<int>>
    {
 
        public  Guid DriverId { get; set; }
        public  string Title { get; set; }
        public  string Detail { get; set; }
        public List<IFormFile> Files { get; set; }
    }
    public class CreateDriverComplainAndRecommendCommandHandler : IRequestHandler<CreateDriverComplainAndRecommendCommand, Response<int>>
    {
        private readonly IDriverRepositoryAsync _driverRepository;
        private readonly IDriverComplainAndRecommendRepositoryAsync _driverComplainAndRecommendRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IConfiguration _configuration;
        private readonly IMapper _mapper;
        public CreateDriverComplainAndRecommendCommandHandler(IDriverRepositoryAsync driverRepository, IDriverComplainAndRecommendRepositoryAsync driverComplainAndRecommendRepository, IApplicationLogRepositoryAsync applicationLogRepository
            , IMapper mapper, IConfiguration configuration)
        {
            _driverRepository = driverRepository;
            _driverComplainAndRecommendRepository = driverComplainAndRecommendRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
            _configuration = configuration;
        }

        public async Task<Response<int>> Handle(CreateDriverComplainAndRecommendCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var driverObject = (await _driverRepository.FindByCondition(x => x.Id.Equals(request.DriverId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (driverObject == null)
                {
                    throw new ApiException($"Driver Not Found.");
                }
                DriverComplainAndRecommend DriverComplainAndRecommend = new DriverComplainAndRecommend
                {
                    Driver = driverObject,
                    Title = request.Title,
                    Detail = request.Detail,
                    Files = new List<DriverComplainAndRecommendFile>()
                };

                if (request.Files != null)
                {
                    string currentTimeStr = DateTime.Now.ToString("yyyyMMddHHmmss");
                    var getContentPath = _configuration.GetSection("ContentPath");
                    var contentPath = getContentPath.Value;

                    string folderPath = contentPath + "driver/complainandrecommend/" + driverObject.Id.ToString() + "/" + currentTimeStr;
                    if (!Directory.Exists(folderPath))
                    {
                        Directory.CreateDirectory(folderPath);
                    }
                    int fileCount = 0;
                    foreach (IFormFile file in request.Files)
                    {
                        fileCount = fileCount + 1;
                        string filePath = Path.Combine(folderPath, file.FileName);
                        using (FileStream fileStream = new FileStream(filePath, FileMode.Create))
                        {
                            await file.CopyToAsync(fileStream);
                            FileInfo fi = new FileInfo(filePath);
                            DriverComplainAndRecommendFile DriverComplainAndRecommendFile = new DriverComplainAndRecommendFile
                            {
                                DriverComplainAndRecommend = DriverComplainAndRecommend,
                                FileName = file.FileName,
                                ContentType = file.ContentType,
                                FilePath = "driver/complainandrecommend/" + driverObject.Id.ToString() + "/" + currentTimeStr + "/" + file.FileName,
                                DirectoryPath = "driver/complainandrecommend/" + driverObject.Id.ToString() + "/" + currentTimeStr + "/",
                                FileEXT = fi.Extension,
                                Sequence = fileCount,
                            };
                            DriverComplainAndRecommend.Files.Add(DriverComplainAndRecommendFile);
                        }
                    }
                }

                DriverComplainAndRecommend.Created = DateTime.UtcNow;
                DriverComplainAndRecommend.Modified = DateTime.UtcNow;
                var DriverComplainAndRecommendObject = await _driverComplainAndRecommendRepository.AddAsync(DriverComplainAndRecommend);
                var log = new CreateAppLog(_applicationLogRepository);
                log.Create("Driver", "Driver ComplainAndRecommend", "Create", Newtonsoft.Json.JsonConvert.SerializeObject(request));
                return new Response<int>(DriverComplainAndRecommendObject.Id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
