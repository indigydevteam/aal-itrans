﻿using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.DriverComplainAndRecommendes.Commands
{
    public class DeleteDriverComplainAndRecommendeByIdCommand : IRequest<Response<int>>
    {
        public int Id { get; set; }
        public class DeleteDriverComplainAndRecommendeByIdCommandHandler : IRequestHandler<DeleteDriverComplainAndRecommendeByIdCommand, Response<int>>
        {
            private readonly IDriverComplainAndRecommendRepositoryAsync _DriverComplainAndRecommendeRepository;
            public DeleteDriverComplainAndRecommendeByIdCommandHandler(IDriverComplainAndRecommendRepositoryAsync DriverComplainAndRecommendeRepository)
            {
                _DriverComplainAndRecommendeRepository = DriverComplainAndRecommendeRepository;
            }
            public async Task<Response<int>> Handle(DeleteDriverComplainAndRecommendeByIdCommand command, CancellationToken cancellationToken)
            {
                var DriverComplainAndRecommende = (await _DriverComplainAndRecommendeRepository.FindByCondition(x => x.Id == command.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (DriverComplainAndRecommende == null)
                {
                    throw new ApiException($"DriverComplainAndRecommende Not Found.");
                }
                else
                {
                    await _DriverComplainAndRecommendeRepository.DeleteAsync(DriverComplainAndRecommende);
                    return new Response<int>(DriverComplainAndRecommende.Id);
                }
            }
        }
    }
}
