﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.DriverComplainAndRecommend;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.DriverComplainAndRecommends.Queries
{
    public class GetDriverComplainAndRecommendByDriverQuery : IRequest<Response<DriverComplainAndRecommendViewModel>>
    {
        public Guid DriverId { get; set; }
    }
    public class GetDriverComplainAndRecommendByDriverQueryHandler : IRequestHandler<GetDriverComplainAndRecommendByDriverQuery, Response<DriverComplainAndRecommendViewModel>>
    {
        private readonly IDriverComplainAndRecommendRepositoryAsync _DriverComplainAndRecommendRepository;
        private readonly IMapper _mapper;
        public GetDriverComplainAndRecommendByDriverQueryHandler(IDriverComplainAndRecommendRepositoryAsync DriverComplainAndRecommendRepository, IMapper mapper)
        {
            _DriverComplainAndRecommendRepository = DriverComplainAndRecommendRepository;
            _mapper = mapper;
        }
        public async Task<Response<DriverComplainAndRecommendViewModel>> Handle(GetDriverComplainAndRecommendByDriverQuery request, CancellationToken cancellationToken)
        {
            var driverObject = (await _DriverComplainAndRecommendRepository.FindByCondition(x => x.Driver.Id.Equals(request.DriverId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            return new Response<DriverComplainAndRecommendViewModel>(_mapper.Map<DriverComplainAndRecommendViewModel>(driverObject));
        }
    }
}
