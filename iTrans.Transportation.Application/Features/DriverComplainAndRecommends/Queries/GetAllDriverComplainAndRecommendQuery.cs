﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.DriverComplainAndRecommend;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.DriverComplainAndRecommends.Queries
{
    public class GetAllDriverComplainAndRecommendQuery : IRequest<Response<IEnumerable<DriverComplainAndRecommendViewModel>>>
    {

    }
    public class GetAllDriverComplainAndRecommendQueryHandler : IRequestHandler<GetAllDriverComplainAndRecommendQuery, Response<IEnumerable<DriverComplainAndRecommendViewModel>>>
    {
        private readonly IDriverComplainAndRecommendRepositoryAsync _driverComplainAndRecommendRepository;
        private readonly IMapper _mapper;
        public GetAllDriverComplainAndRecommendQueryHandler(IDriverComplainAndRecommendRepositoryAsync driverComplainAndRecommendRepository, IMapper mapper)
        {
            _driverComplainAndRecommendRepository = driverComplainAndRecommendRepository;
            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<DriverComplainAndRecommendViewModel>>> Handle(GetAllDriverComplainAndRecommendQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var validFilter = _mapper.Map<GetAllDriverComplainAndRecommendQuery>(request);
                var driver = await _driverComplainAndRecommendRepository.GetAllAsync();
                var driverComplainAndRecommendViewModel = _mapper.Map<IEnumerable<DriverComplainAndRecommendViewModel>>(driver);
                return new Response<IEnumerable<DriverComplainAndRecommendViewModel>>(driverComplainAndRecommendViewModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
