﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.DriverComplainAndRecommend;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using iTrans.Transportation.Domain.Entities;
using MediatR;

namespace iTrans.Transportation.Application.Features.DriverComplainAndRecommends.Queries
{
    public class GetAllDriverComplainAndRecommendParameter : IRequest<PagedResponse<IEnumerable<DriverComplainAndRecommendViewModel>>>
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
    }
    public class GetAllDriverComplainAndRecommendParameterHandler : IRequestHandler<GetAllDriverComplainAndRecommendParameter, PagedResponse<IEnumerable<DriverComplainAndRecommendViewModel>>>
    {
        private readonly IDriverComplainAndRecommendRepositoryAsync _DriverComplainAndRecommendRepository;
        private readonly IMapper _mapper;
        public GetAllDriverComplainAndRecommendParameterHandler(IDriverComplainAndRecommendRepositoryAsync DriverComplainAndRecommendRepository, IMapper mapper)
        {
            _DriverComplainAndRecommendRepository = DriverComplainAndRecommendRepository;
            _mapper = mapper;
        }

        public async Task<PagedResponse<IEnumerable<DriverComplainAndRecommendViewModel>>> Handle(GetAllDriverComplainAndRecommendParameter request, CancellationToken cancellationToken)
        {
            var validFilter = _mapper.Map<GetAllDriverComplainAndRecommendParameter>(request);
            Expression<Func<DriverComplainAndRecommend, bool>> expression = x => x.Driver.Id != null;
            var DriverComplainAndRecommend = await _DriverComplainAndRecommendRepository.FindByConditionWithPage(expression, validFilter.PageNumber, validFilter.PageSize);
            var DriverComplainAndRecommendViewModel = _mapper.Map<IEnumerable<DriverComplainAndRecommendViewModel>>(DriverComplainAndRecommend);
            return new PagedResponse<IEnumerable<DriverComplainAndRecommendViewModel>>(DriverComplainAndRecommendViewModel, validFilter.PageNumber, validFilter.PageSize);
        }
    }
}