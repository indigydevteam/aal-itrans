﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.DriverComplainAndRecommend;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.DriverComplainAndRecommends.Queries
{
    public class GetDriverComplainAndRecommendByIdQuery : IRequest<Response<DriverComplainAndRecommendViewModel>>
    {
        public int Id { get; set; }
    }
    public class GetDriverComplainAndRecommendByIdQueryHandler : IRequestHandler<GetDriverComplainAndRecommendByIdQuery, Response<DriverComplainAndRecommendViewModel>>
    {
        private readonly IDriverComplainAndRecommendRepositoryAsync _DriverComplainAndRecommendRepository;
        private readonly IMapper _mapper;
        public GetDriverComplainAndRecommendByIdQueryHandler(IDriverComplainAndRecommendRepositoryAsync DriverComplainAndRecommendRepository, IMapper mapper)
        {
            _DriverComplainAndRecommendRepository = DriverComplainAndRecommendRepository;
            _mapper = mapper;
        }
        public async Task<Response<DriverComplainAndRecommendViewModel>> Handle(GetDriverComplainAndRecommendByIdQuery request, CancellationToken cancellationToken)
        {
            var driverObject = (await _DriverComplainAndRecommendRepository.FindByCondition(x => x.Id.Equals(request.Id)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            return new Response<DriverComplainAndRecommendViewModel>(_mapper.Map<DriverComplainAndRecommendViewModel>(driverObject));
        }
    }
}
