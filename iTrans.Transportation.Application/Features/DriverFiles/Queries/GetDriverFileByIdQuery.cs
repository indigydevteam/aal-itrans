﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.DriverFile;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.DriverFiles.Queries
{
   public class GetDriverFileByIdQuery : IRequest<Response<DriverFileViewModel>>
    {
        public int Id { get; set; }
    }
    public class GetDriverFileByIdQueryHandler : IRequestHandler<GetDriverFileByIdQuery, Response<DriverFileViewModel>>
    {
        private readonly IDriverFileRepositoryAsync _driverFileRepository;
        private readonly IMapper _mapper;
        public GetDriverFileByIdQueryHandler(IDriverFileRepositoryAsync driverFileRepository, IMapper mapper)
        {
            _driverFileRepository = driverFileRepository;
            _mapper = mapper;
        }
        public async Task<Response<DriverFileViewModel>> Handle(GetDriverFileByIdQuery request, CancellationToken cancellationToken)
        {
            var driverFileObject = (await _driverFileRepository.FindByCondition(x => x.Id.Equals(request.Id)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            return new Response<DriverFileViewModel>(_mapper.Map<DriverFileViewModel>(driverFileObject));
        }
    }
}
