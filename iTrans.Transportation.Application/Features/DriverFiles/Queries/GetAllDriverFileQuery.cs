﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.DriverFile;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.DriverFiles.Queries
{
    public class GetAllDriverFileQuery : IRequest<Response<IEnumerable<DriverFileViewModel>>>
    {
         
    }
    public class GetAllDriverFileQueryHandler : IRequestHandler<GetAllDriverFileQuery, Response<IEnumerable<DriverFileViewModel>>>
    {
        private readonly IDriverFileRepositoryAsync _driverFileRepository;
        private readonly IMapper _mapper;
        public GetAllDriverFileQueryHandler(IDriverFileRepositoryAsync driverFileRepository, IMapper mapper)
        {
            _driverFileRepository = driverFileRepository;
            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<DriverFileViewModel>>> Handle(GetAllDriverFileQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var driver = await _driverFileRepository.GetAllAsync();
                var driverFileViewModel = _mapper.Map<IEnumerable<DriverFileViewModel>>(driver);
                return new Response<IEnumerable<DriverFileViewModel>>(driverFileViewModel);
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
    }
}
