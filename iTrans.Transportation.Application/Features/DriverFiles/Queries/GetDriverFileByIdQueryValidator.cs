﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using iTrans.Transportation.Application.Interfaces.Repositories;
using FluentValidation;

namespace iTrans.Transportation.Application.Features.DriverFiles.Queries
{
    public class GetDriverFileByIdQueryValidator : AbstractValidator<GetDriverFileByIdQuery>
    {
        private readonly IDriverFileRepositoryAsync driverFileRepository;

        public GetDriverFileByIdQueryValidator(IDriverFileRepositoryAsync driverFileRepository)
        {
            this.driverFileRepository = driverFileRepository;
            RuleFor(p => p.Id)
               .NotEmpty().WithMessage("{PropertyName} is required.")
               .NotNull()
               .MustAsync(IsDriverFileExists).WithMessage("{PropertyName} not exists.");
        }

        private async Task<bool> IsDriverFileExists(int Id, CancellationToken cancellationToken)
        {
            var userObject = (await driverFileRepository.FindByCondition(x => x.Id.Equals(Id)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            if (userObject != null)
            {
                return true;
            }
            return false;
        }
    }
}
