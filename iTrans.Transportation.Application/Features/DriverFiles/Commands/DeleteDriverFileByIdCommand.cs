﻿using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
namespace iTrans.Transportation.Application.Features.DriverFiles.Commands
{
    public class DeleteDriverFileByIdCommand : IRequest<Response<int>>
    {
        public int Id { get; set; }
        public class DeleteDriverFileByIdCommandHandler : IRequestHandler<DeleteDriverFileByIdCommand, Response<int>>
        {
            private readonly IDriverFileRepositoryAsync _driverFileRepository;
            public DeleteDriverFileByIdCommandHandler(IDriverFileRepositoryAsync driverFileRepository)
            {
                _driverFileRepository = driverFileRepository;
            }
            public async Task<Response<int>> Handle(DeleteDriverFileByIdCommand command, CancellationToken cancellationToken)
            {
                var driverFile = (await _driverFileRepository.FindByCondition(x => x.Id == command.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (driverFile == null)
                {
                    throw new ApiException($"DriverFile Not Found.");
                }
                else
                {
                    await _driverFileRepository.DeleteAsync(driverFile);
                    return new Response<int>(driverFile.Id);
                }
            }
        }
    }
}
