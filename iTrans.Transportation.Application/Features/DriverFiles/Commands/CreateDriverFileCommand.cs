﻿using AutoMapper;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Interfaces.Services;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.DriverFiles.Commands
{
    public partial class CreateDriverFileCommand : IRequest<Response<int>>
    {
        public int Id { get; set; }
        public Guid DriverId { get; set; }
        public string FileName { get; set; }
        public  string ContentType { get; set; }
        public  string FilePath { get; set; }
        public  string FileEXT { get; set; }
        public  string DocumentType { get; set; }
        public  int Sequence { get; set; }

    }
    public class CreateDriverFileCommandHandler : IRequestHandler<CreateDriverFileCommand, Response<int>>
    {
        private readonly IDriverFileRepositoryAsync _driverFileRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;
        public CreateDriverFileCommandHandler(IDriverFileRepositoryAsync driverFileRepository,IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _driverFileRepository = driverFileRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(CreateDriverFileCommand request, CancellationToken cancellationToken)
        {
            var driverFile = _mapper.Map<DriverFile>(request);
            driverFile.Created = DateTime.UtcNow;
            driverFile.Modified = DateTime.UtcNow;
            var driverFileObject = await _driverFileRepository.AddAsync(driverFile);
            var log = new CreateAppLog(_applicationLogRepository);
            log.Create("Driver", "Driver File", "Create", Newtonsoft.Json.JsonConvert.SerializeObject(driverFile));
            return new Response<int>(driverFileObject.Id);
        }
    }
}
