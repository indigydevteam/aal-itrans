﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;

namespace iTrans.Transportation.Application.Features.DriverFiles.Commands
{
    public class UpdateDriverFileCommand : IRequest<Response<int>>
    {
        public int Id { get; set; }
        public Guid DriverId { get; set; }
        public string FileName { get; set; }
        public string ContentType { get; set; }
        public string FilePath { get; set; }
        public string FileEXT { get; set; }
        public string DocumentType { get; set; }
        public int Sequence { get; set; }
    }

    public class UpdateDriverFileCommandHandler : IRequestHandler<UpdateDriverFileCommand, Response<int>>
    {
        private readonly IDriverFileRepositoryAsync _driverFileRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;

        public UpdateDriverFileCommandHandler(IDriverFileRepositoryAsync driverFileRepository, IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _driverFileRepository = driverFileRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(UpdateDriverFileCommand request, CancellationToken cancellationToken)
        {
            var driverFile = (await _driverFileRepository.FindByCondition(x => x.Id == request.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            if (driverFile == null)
            {
                throw new ApiException($"DriverFile Not Found.");
            }
            else
            {
                driverFile.DriverId = request.DriverId;
                driverFile.FileName = request.FileName;
                driverFile.ContentType = request.ContentType;
                driverFile.FilePath = request.FilePath;
                driverFile.FileEXT = request.FileEXT;
                driverFile.DocumentType = request.DocumentType;
                driverFile.Sequence = request.Sequence;
                driverFile.Modified = DateTime.UtcNow;
                await _driverFileRepository.UpdateAsync(driverFile);
                var log = new CreateAppLog(_applicationLogRepository);
                log.Create("Driver", "Driver File", "Edit", Newtonsoft.Json.JsonConvert.SerializeObject(driverFile));
                return new Response<int>(driverFile.Id);
            }
        }
    }
}
