﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;


namespace iTrans.Transportation.Application.Features.DriverFiles.Commands
{
    public class UpdateDriverFileCommandValidator : AbstractValidator<UpdateDriverFileCommand>
    {
        private readonly IDriverFileRepositoryAsync driverFileRepository;

        public UpdateDriverFileCommandValidator(IDriverFileRepositoryAsync driverFileRepository)
        {
            this.driverFileRepository = driverFileRepository;

            RuleFor(p => p.DriverId)
                .NotNull().WithMessage("{PropertyName} is required.");

            RuleFor(p => p.FileName)
                .NotEmpty().WithMessage("{PropertyName} is required.")
                .NotNull();

            RuleFor(p => p.ContentType)
               .NotEmpty().WithMessage("{PropertyName} is required.")
               .NotNull();

            RuleFor(p => p.FilePath)
               .NotEmpty().WithMessage("{PropertyName} is required.")
               .NotNull();

            RuleFor(p => p.FileEXT)
               .NotEmpty().WithMessage("{PropertyName} is required.")
               .NotNull();

            RuleFor(p => p.DocumentType)
               .NotEmpty().WithMessage("{PropertyName} is required.")
               .NotNull();
        }

        private async Task<bool> IsUnique(string value, CancellationToken cancellationToken)
        {
            var driverFileObject = (await driverFileRepository.FindByCondition(x => x.FileName.ToLower() == value.ToLower()).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            if (driverFileObject != null)
            {
                return false;
            }
            return true;
        }
    }
}
