﻿using AutoMapper;
using iTrans.Transportation.Application.DTOs.RegisterInformation;
using iTrans.Transportation.Application.DTOs.TermAndCondition;
using iTrans.Transportation.Application.DTOs.UserLevel;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.Customers.Queries
{
    public class CheckCustomerTermAndCondition : IRequest<Response<IEnumerable<UserTermAndConditionViewModel>>>
    {
        public Guid Id { set; get; }
    }
    public class CheckCustomerTermAndConditionHandler : IRequestHandler<CheckCustomerTermAndCondition, Response<IEnumerable<UserTermAndConditionViewModel>>>
    {
        private readonly ICustomerTermAndConditionRepositoryAsync _customerTermAndConditionRepository;
        private readonly ITermAndConditionRepositoryAsync _termAndConditionRepository;
        private readonly IMapper _mapper;
        public CheckCustomerTermAndConditionHandler(ITermAndConditionRepositoryAsync termAndConditionRepository, ICustomerTermAndConditionRepositoryAsync customerTermAndConditionRepository, IMapper mapper)
        {
            _customerTermAndConditionRepository = customerTermAndConditionRepository;
            _termAndConditionRepository = termAndConditionRepository;
            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<UserTermAndConditionViewModel>>> Handle(CheckCustomerTermAndCondition request, CancellationToken cancellationToken)
        {
            var data = (await _termAndConditionRepository.FindByCondition(x => x.Active == true && x.Module.Trim().ToLower() == "customer").ConfigureAwait(false)).AsQueryable().ToList();
            List<TermAndCondition> termAndConditions = new List<TermAndCondition>();
            foreach (TermAndCondition termAndCondition in data)
            {
                var existItem = termAndConditions.Where(x => x.Section == termAndCondition.Section).FirstOrDefault();
                if (existItem == null)
                {
                    termAndConditions.Add(termAndCondition);
                }
                else
                {
                    int n;
                    var version1 = new Version(int.TryParse(termAndCondition.Version, out n) ? termAndCondition.Version + ".0" : termAndCondition.Version);
                    var version2 = new Version(int.TryParse(existItem.Version, out n) ? existItem.Version + ".0" : existItem.Version);
                    var result = version1.CompareTo(version2);
                    if (result > 0)
                    {
                        termAndConditions.Remove(existItem);
                        termAndConditions.Add(termAndCondition);
                    }
                }
            }


            var customerTermAndCondition = (await _customerTermAndConditionRepository.FindByCondition(x => x.Customer.Id.Equals(request.Id)).ConfigureAwait(false)).AsQueryable().ToList();
            List<UserTermAndConditionViewModel> results = new List<UserTermAndConditionViewModel>();
            //var data = termAndCondition.Where(t => !driverTermAndCondition.Any(c => c.TermAndConditionId != t.Id && c.Version != t.Version)).ToList();

            foreach (TermAndCondition item in termAndConditions)
            {
                UserTermAndConditionViewModel termAndConditionViewModel = _mapper.Map<UserTermAndConditionViewModel>(item);
                var existTermAndCondition = customerTermAndCondition.Where(x => x.TermAndConditionId == item.Id && x.Version == item.Version).FirstOrDefault();
                if (existTermAndCondition == null)
                {
                    termAndConditionViewModel.isUserAccept = false;
                }
                else
                {
                    termAndConditionViewModel.isUserAccept = true;
                }
                results.Add(termAndConditionViewModel);
            }

            var TermAndConditionViewModel = _mapper.Map<IEnumerable<UserTermAndConditionViewModel>>(results.OrderBy(x => x.Section).OrderBy(x => x.Sequence));
            return new Response<IEnumerable<UserTermAndConditionViewModel>>(TermAndConditionViewModel);
        }
    }
}
