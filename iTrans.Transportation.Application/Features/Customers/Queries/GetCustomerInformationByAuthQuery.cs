﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.Customer;
using iTrans.Transportation.Application.DTOs.CustomerTermAndCondition;
using iTrans.Transportation.Application.DTOs.OrderingStatus;
using iTrans.Transportation.Application.DTOs.TermAndCondition;
using iTrans.Transportation.Application.DTOs.UserLevel;
using iTrans.Transportation.Application.DTOs.VerifyStatus;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using iTrans.Transportation.Domain.Entities;
using MediatR;

namespace iTrans.Transportation.Application.Features.Customers.Queries
{
    public class GetCustomerInformationByAuthQuery : IRequest<Response<CustomerInformationViewModel>>
    {
        public Guid Id { get; set; }
    }
    public class GetCustomerInformationByAuthQueryQueryHandler : IRequestHandler<GetCustomerInformationByAuthQuery, Response<CustomerInformationViewModel>>
    {
        private readonly ICustomerRepositoryAsync _customerRepository;
        private readonly IUserLevelRepositoryAsync _userLevelRepository;
        private readonly ICustomerTermAndConditionRepositoryAsync _customerTermAndConditionRepository;
        private readonly ITermAndConditionRepositoryAsync _termAndConditionRepository;
        private readonly IMapper _mapper;
        public GetCustomerInformationByAuthQueryQueryHandler(ICustomerRepositoryAsync customerRepository, IUserLevelRepositoryAsync userLevelRepository
            , ICustomerTermAndConditionRepositoryAsync customerTermAndConditionRepository, ITermAndConditionRepositoryAsync termAndConditionRepository, IMapper mapper)
        {
            _customerRepository = customerRepository;
            _userLevelRepository = userLevelRepository;
            _customerTermAndConditionRepository = customerTermAndConditionRepository;
            _termAndConditionRepository = termAndConditionRepository;
            _mapper = mapper;
        }
        public async Task<Response<CustomerInformationViewModel>> Handle(GetCustomerInformationByAuthQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var customerObject = (await _customerRepository.FindByCondition(x => x.Id == request.Id && !x.IsDelete).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                var userLevels = (await _userLevelRepository.FindByCondition(x => x.Module == "customer" && x.Active == true).ConfigureAwait(false)).AsQueryable().OrderBy(x => x.Level).ToList();
                CustomerInformationViewModel customerInformationObject = _mapper.Map<CustomerInformationViewModel>(customerObject);

                if (customerInformationObject != null)
                {
                    #region term and con
                    var data = (await _termAndConditionRepository.FindByCondition(x => x.Active == true && x.Module.Trim().ToLower() == "customer").ConfigureAwait(false)).AsQueryable().ToList().OrderBy(x => x.Sequence).OrderBy(x => x.Version).OrderBy(x => x.Modified);
                    List<TermAndCondition> termAndConditions = new List<TermAndCondition>();
                    foreach (TermAndCondition termAndCondition in data)
                    {
                        var existItem = termAndConditions.Where(x => x.Section == termAndCondition.Section).FirstOrDefault();
                        if (existItem == null)
                        {
                            termAndConditions.Add(termAndCondition);
                        }
                        else
                        {
                            int n;
                            var version1 = new Version(int.TryParse(termAndCondition.Version, out n) ? termAndCondition.Version + ".0" : termAndCondition.Version);
                            var version2 = new Version(int.TryParse(existItem.Version, out n) ? existItem.Version + ".0" : existItem.Version);
                            var result = version1.CompareTo(version2);
                            if (result > 0)
                            {
                                termAndConditions.Remove(existItem);
                                termAndConditions.Add(termAndCondition);
                            }
                        }
                    };

                    List<CustomerTermAndConditionViewModel> customerTermAndConditions = new List<CustomerTermAndConditionViewModel>();
                    foreach (TermAndCondition item in termAndConditions)
                    {
                        var customerTermAndCondition = (await _customerTermAndConditionRepository.FindByCondition(x => x.Customer.Id.Equals(request.Id) && x.Section == item.Section && x.Version == item.Version)
                            .ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                        if (customerTermAndCondition == null)
                        {
                            CustomerTermAndConditionViewModel itemTermAndCondition = new CustomerTermAndConditionViewModel()
                            {
                                id = 0,
                                termAndConditionId = item.Id,
                                name_TH = item.Name_TH,
                                name_ENG = item.Name_ENG,
                                section = item.Section,
                                version = item.Version,
                                isAccept = item.IsAccept,
                                isUserAccept = false,
                                isComplete = false
                            };
                            customerTermAndConditions.Add(itemTermAndCondition);
                        }
                        else
                        {
                            var itemTermAndCondition = _mapper.Map<CustomerTermAndConditionViewModel>(customerTermAndCondition);
                            itemTermAndCondition.isComplete = true;
                            customerTermAndConditions.Add(itemTermAndCondition);
                        }
                    }
                    customerInformationObject.termAndConditions = customerTermAndConditions;
                    #endregion #region term and con

                    if (customerInformationObject.files != null)
                    {
                        customerInformationObject.files.RemoveAll(f => f.IsDelete);
                    }
                    var folderDetails = Path.Combine(Directory.GetCurrentDirectory(), $"Shared\\VerifyStatus.json");
                    var statusJson = System.IO.File.ReadAllText(folderDetails);
                    List<VerifyStatusViewModel> verifyStatus = Newtonsoft.Json.JsonConvert.DeserializeObject<List<VerifyStatusViewModel>>(statusJson);

                    customerInformationObject.verifyStatusObj = verifyStatus.Where(o => o.id == customerInformationObject.verifyStatus).FirstOrDefault();

                    customerInformationObject.customerLevel = _mapper.Map<UserLevelViewModel>(userLevels.Where(x => x.Level.Equals(customerInformationObject.level)).FirstOrDefault());
                    customerInformationObject.nextLevel = _mapper.Map<UserLevelViewModel>(userLevels.Where(x => x.Level.Equals(customerInformationObject.level + 1)).FirstOrDefault());
                    if(customerInformationObject.nextLevel != null)
                    {
                        customerInformationObject.nextLevel.star = customerInformationObject.nextLevel.star - customerInformationObject.star;
                    }
                    if(customerInformationObject.phoneCode == "+66" && customerInformationObject.phoneNumber != null && customerInformationObject.phoneNumber.Trim() != "")
                    {
                        customerInformationObject.phoneNumber = "0" + customerInformationObject.phoneNumber;
                    }

                }
                return new Response<CustomerInformationViewModel>(customerInformationObject);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
