﻿using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.Customers.Queries
{
    public class CheckCustomerPhoneNumberQuery : IRequest<Response<bool>>
    {
        public Guid Id { get; set; }
        public string IdentityNumber { get; set; }
        public string PhoneCode { get; set; }
        public string PhoneNumber { get; set; }
    }
    public class CheckCustomerPhoneNumberQueryHandler : IRequestHandler<CheckCustomerPhoneNumberQuery, Response<bool>>
    {
        private readonly ICustomerRepositoryAsync _customerRepository;
        private readonly IMapper _mapper;

        public CheckCustomerPhoneNumberQueryHandler(ICustomerRepositoryAsync customerdRepository, IMapper mapper)
        {
            _customerRepository = customerdRepository;
            _mapper = mapper;
        }

        public async Task<Response<bool>> Handle(CheckCustomerPhoneNumberQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var customer = (await _customerRepository.FindByCondition(x => x.Id.Equals(request.Id) && x .IdentityNumber == request.IdentityNumber &&  x.PhoneNumber == request.PhoneNumber && x.PhoneCode == request.PhoneCode).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (customer == null)
                {
                    return new Response<bool>(false);
                }
                else
                {
                    return new Response<bool>(true);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
