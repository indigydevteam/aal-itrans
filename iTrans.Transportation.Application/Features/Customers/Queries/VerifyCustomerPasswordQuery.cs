﻿using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using iTrans.Transportation.Application.Helper;
using Microsoft.Extensions.Configuration;

namespace iTrans.Transportation.Application.Features.Customers.Queries
{
    public class VerifyCustomerPasswordQuery : IRequest<Response<bool>>
    {
        public Guid Id { get; set; }
        public string Password { get; set; }
    }
    public class VerifyCustomerPasswordQueryHandler : IRequestHandler<VerifyCustomerPasswordQuery, Response<bool>>
    {
        private readonly ICustomerRepositoryAsync _customerRepository;
        private readonly ILoginErrorLogRepositoryAsync _loginErrorLogRepository;
        private readonly IConfiguration _configuration;
        private readonly IMapper _mapper;

        public VerifyCustomerPasswordQueryHandler(ICustomerRepositoryAsync customerdRepository, ILoginErrorLogRepositoryAsync loginErrorLogRepository, IConfiguration configuration, IMapper mapper)
        {
            _customerRepository = customerdRepository;
            _loginErrorLogRepository = loginErrorLogRepository;
            _configuration = configuration;
            _mapper = mapper;
        }

        public async Task<Response<bool>> Handle(VerifyCustomerPasswordQuery request, CancellationToken cancellationToken)
        {
            try
            {
                bool result = false;
                string username = "";
                var customer = (await _customerRepository.FindByCondition(x => x.Id.Equals(request.Id)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (customer != null)
                {
                    username = customer.PhoneNumber;
                    if (request.Password != null && request.Password.Trim() != "" && customer.Password == request.Password.Trim())
                    {
                        result = true;
                    }
                }
                LoginErrorLogHelper loginErrorLog = new LoginErrorLogHelper();
                var IsAccountLocked = loginErrorLog.IsAccountLocked(username, _loginErrorLogRepository);
                IsAccountLocked.Wait();

                if (result && !IsAccountLocked.Result)
                {
                    loginErrorLog.ClearIncorrect(username, _loginErrorLogRepository);

                    return new Response<bool>(result);
                }
                else
                {
                    var msg = loginErrorLog.UpdateIncorrect(username, _loginErrorLogRepository, _configuration);
                    msg.Wait();
                    return new Response<bool>(result, msg.Result.Item1, msg.Result.Item2);
                }

            }
            catch (Exception ex)
            {
                return new Response<bool>(false, "รหัสผ่านไม่ถูกต้อง", "Incorrect password.");
            }
        }
    }
}
