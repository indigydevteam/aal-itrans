﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.Customer;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.Customers.Queries
{
    public class GetCustomerInformationByIdQuery : IRequest<Response<CustomerInformationViewModel>>
    {
        public Guid Id { get; set; }
    }
    public class GetCustomerInformationByIdQueryHandler : IRequestHandler<GetCustomerInformationByIdQuery, Response<CustomerInformationViewModel>>
    {
        private readonly ICustomerRepositoryAsync _customerRepository;
        private readonly IMapper _mapper;
        public GetCustomerInformationByIdQueryHandler(ICustomerRepositoryAsync customerRepository, IMapper mapper)
        {
            _customerRepository = customerRepository;
            _mapper = mapper;
        }
        public async Task<Response<CustomerInformationViewModel>> Handle(GetCustomerInformationByIdQuery request, CancellationToken cancellationToken)
        {
            var customerObject = (await _customerRepository.FindByCondition(x => x.Id.Equals(request.Id)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            return new Response<CustomerInformationViewModel>(_mapper.Map<CustomerInformationViewModel>(customerObject));
        }
    }
}
