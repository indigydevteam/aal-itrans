﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using iTrans.Transportation.Application.Interfaces.Repositories;
using FluentValidation;

namespace iTrans.Transportation.Application.Features.Customers.Queries
{
    public class GetCustomerInformationByAuthQueryValidator : AbstractValidator<GetCustomerInformationByAuthQuery>
    {
        private readonly ICustomerRepositoryAsync customerRepository;

        public GetCustomerInformationByAuthQueryValidator(ICustomerRepositoryAsync customerRepository)
        {
            this.customerRepository = customerRepository;
            RuleFor(p => p.Id)
               .NotEmpty().WithMessage("{PropertyName} is required.")
               .NotNull()
               .MustAsync(IsCustomerExists).WithMessage("{PropertyName} not exists.");
        }

        private async Task<bool> IsCustomerExists(Guid Id, CancellationToken cancellationToken)
        {
            var userObject = (await customerRepository.FindByCondition(x => x.Id.Equals(Id) && !x.IsDelete).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            if (userObject != null)
            {
                return true;
            }
            return false;
        }
    }
}
