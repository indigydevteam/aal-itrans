﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.Customer;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.Customers.Queries
{
    public class GetCustomerWithAddressByIdQuery : IRequest<Response<CustomerWithAddressViewModel>>
    {
        public Guid Id { get; set; }
    }
    public class GetCustomerWithAddressByIdQueryHandler : IRequestHandler<GetCustomerWithAddressByIdQuery, Response<CustomerWithAddressViewModel>>
    {
        private readonly ICustomerRepositoryAsync _customerRepository;
        private readonly ICustomerAddressRepositoryAsync _customerAddressRepository;
        private readonly IMapper _mapper;
        public GetCustomerWithAddressByIdQueryHandler(ICustomerRepositoryAsync customerRepository, ICustomerAddressRepositoryAsync customerAddressRepository, IMapper mapper)
        {
            _customerRepository = customerRepository;
            _customerAddressRepository = customerAddressRepository;
            _mapper = mapper;
        }
        public async Task<Response<CustomerWithAddressViewModel>> Handle(GetCustomerWithAddressByIdQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var customerObject = (await _customerRepository.FindByCondition(x => x.Id.Equals(request.Id)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                //if (customerObject != null)
                //    customerObject.Addresses = (await _customerAddressRepository.FindByCondition(x => x.CustomerId.Equals(customerObject.Id)).ConfigureAwait(false)).AsQueryable().ToList();
                return new Response<CustomerWithAddressViewModel>(_mapper.Map<CustomerWithAddressViewModel>(customerObject));
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
    }
}
