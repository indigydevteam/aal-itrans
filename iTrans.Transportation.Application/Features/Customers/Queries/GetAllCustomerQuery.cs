﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.Customer;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;


namespace iTrans.Transportation.Application.Features.Customers.Queries
{
    public class GetAllCustomerQuery : IRequest<Response<IEnumerable<CustomerViewModel>>>
    {
       
    }
    public class GetAllCustomerQueryHandler : IRequestHandler<GetAllCustomerQuery, Response<IEnumerable<CustomerViewModel>>>
    {
        private readonly ICustomerRepositoryAsync _customerRepository;
        private readonly IMapper _mapper;
        public GetAllCustomerQueryHandler(ICustomerRepositoryAsync customerRepository, IMapper mapper)
        {
            _customerRepository = customerRepository;
            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<CustomerViewModel>>> Handle(GetAllCustomerQuery request, CancellationToken cancellationToken)
        {
            var customer = await _customerRepository.GetAllAsync();
            var customerViewModel = _mapper.Map<IEnumerable<CustomerViewModel>>(customer);
            return new Response<IEnumerable<CustomerViewModel>>(customerViewModel);
        }
    }
}
