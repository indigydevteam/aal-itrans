﻿using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.Customers.Queries
{
    public class CheckCustomerIdentityNumberQuery : IRequest<Response<bool>>
    {
        public Guid Id { get; set; }
        public string IdentityNumber { get; set; }
    }
    public class CheckIdentityNumberQueryHandler : IRequestHandler<CheckCustomerIdentityNumberQuery, Response<bool>>
    {
        private readonly ICustomerRepositoryAsync _customerRepository;
        private readonly IMapper _mapper;

        public CheckIdentityNumberQueryHandler(ICustomerRepositoryAsync customerdRepository, IMapper mapper)
        {
            _customerRepository = customerdRepository;
            _mapper = mapper;
        }

        public async Task<Response<bool>> Handle(CheckCustomerIdentityNumberQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var customer = (await _customerRepository.FindByCondition(x => x.Id.Equals(request.Id) && x.IdentityNumber == request.IdentityNumber).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (customer == null)
                {
                    return new Response<bool>(false);
                }
                else
                {
                    return new Response<bool>(true);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
