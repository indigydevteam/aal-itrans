﻿using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.Customers.Queries
{
    public class VerifyCustomerPhoneNumberQuery : IRequest<Response<bool>>
    {
        public Guid Id   { get; set; }
        public string PhoneNumber { get; set; }
    }
    public class VerifyCustomerPhoneNumberQueryHandler : IRequestHandler<VerifyCustomerPhoneNumberQuery, Response<bool>>
    {
        private readonly ICustomerRepositoryAsync _customerRepository;
        private readonly IMapper _mapper;

        public VerifyCustomerPhoneNumberQueryHandler(ICustomerRepositoryAsync customerdRepository, IMapper mapper)
        {
            _customerRepository = customerdRepository;
            _mapper = mapper;
        }

        public async Task<Response<bool>> Handle(VerifyCustomerPhoneNumberQuery request, CancellationToken cancellationToken)
        {
            try
            {
                bool result = false;
                if (request.PhoneNumber != null && request.PhoneNumber.Trim() != "")
                {
                    if(request.PhoneNumber.Trim().Length >= 10 && request.PhoneNumber.Trim().StartsWith('0'))
                    {
                        request.PhoneNumber = request.PhoneNumber.Trim().Remove(0, 1);
                    }
                    var customer = (await _customerRepository.FindByCondition(x => x.Id == request.Id && x.PhoneNumber == request.PhoneNumber && x.IsDelete == false).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                    if (customer != null)
                    {
                        result = true;
                    }
                }
                if (result)
                {
                    return new Response<bool>(result);
                }
                else
                {
                    return new Response<bool>(result, "หมายเลขโทรศัพท์ไม่ถูกต้อง", "Invalid phone number.");
                }
            }
            catch (Exception ex)
            {
                return new Response<bool>(false, "หมายเลขโทรศัพท์ไม่ถูกต้อง", "Invalid phone number.");
            }
        }
    }
}
