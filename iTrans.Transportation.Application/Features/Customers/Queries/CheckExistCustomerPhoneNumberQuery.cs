﻿using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.Customers.Queries
{
    public class CheckExistCustomerPhoneNumberQuery : IRequest<Response<bool>>
    {
        public string PhoneNumber { get; set; }
    }
    public class CheckExistCustomerPhoneNumberQueryHandler : IRequestHandler<CheckExistCustomerPhoneNumberQuery, Response<bool>>
    {
        private readonly ICustomerRepositoryAsync _customerRepository;
        private readonly IMapper _mapper;

        public CheckExistCustomerPhoneNumberQueryHandler(ICustomerRepositoryAsync customerdRepository, IMapper mapper)
        {
            _customerRepository = customerdRepository;
            _mapper = mapper;
        }

        public async Task<Response<bool>> Handle(CheckExistCustomerPhoneNumberQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var customer = (await _customerRepository.FindByCondition(x =>  x.PhoneNumber == request.PhoneNumber).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (customer == null)
                {
                    return new Response<bool>(false);
                }
                else
                {
                    return new Response<bool>(true);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
