﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.Customer;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;

namespace iTrans.Transportation.Application.Features.Customers.Queries
{
    public class GetAllCustomerParameter : IRequest<PagedResponse<IEnumerable<CustomerInformationViewModel>>>
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public string Search { get; set; }
    }
    public class GetAllCustomerParameterHandler : IRequestHandler<GetAllCustomerParameter, PagedResponse<IEnumerable<CustomerInformationViewModel>>>
    {
        private readonly ICustomerRepositoryAsync _customerRepository;
        private readonly IMapper _mapper;
        private Expression<Func<Customer, bool>> expression;

        public GetAllCustomerParameterHandler(ICustomerRepositoryAsync customerRepository, IMapper mapper)
        {
            _customerRepository = customerRepository;
            _mapper = mapper;
        }

        public async Task<PagedResponse<IEnumerable<CustomerInformationViewModel>>> Handle(GetAllCustomerParameter request, CancellationToken cancellationToken)
        {
            var validFilter = _mapper.Map<GetAllCustomerParameter>(request);
            if (validFilter.Search != null && validFilter.Search.Trim() != "")
            {
                expression = x => (x.FirstName.Contains(validFilter.Search.Trim()) || x.LastName.Contains(validFilter.Search.Trim()) || x.Name.Contains(validFilter.Search.Trim()));
            }
            int itemCount = _customerRepository.GetItemCount(expression);

            //Expression<Func<Customer, bool>> expression = x => x.Name != "";
            var customer = await _customerRepository.FindByConditionWithPage(expression, validFilter.PageNumber, validFilter.PageSize);
            var CustomerInformationViewModel = _mapper.Map<IEnumerable<CustomerInformationViewModel>>(customer);
            return new PagedResponse<IEnumerable<CustomerInformationViewModel>>(CustomerInformationViewModel, validFilter.PageNumber, validFilter.PageSize, itemCount);
        }
    }
}
