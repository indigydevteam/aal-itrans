﻿using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using iTrans.Transportation.Application.Helper;
using Microsoft.Extensions.Configuration;

namespace iTrans.Transportation.Application.Features.Customers.Queries
{
    public class VerifyCustomerIdentityNumberQuery : IRequest<Response<bool>>
    {
        public Guid Id { get; set; }
        public string IdentityNumber { get; set; }
    }
    public class VerifyCustomerIdentityNumberQueryHandler : IRequestHandler<VerifyCustomerIdentityNumberQuery, Response<bool>>
    {
        private readonly ICustomerRepositoryAsync _customerRepository;
        private readonly IConfiguration _configuration;
        private readonly IMapper _mapper;

        public VerifyCustomerIdentityNumberQueryHandler(ICustomerRepositoryAsync customerdRepository, IMapper mapper, IConfiguration configuration)
        {
            _customerRepository = customerdRepository;
            _configuration = configuration;
            _mapper = mapper;
        }

        public async Task<Response<bool>> Handle(VerifyCustomerIdentityNumberQuery request, CancellationToken cancellationToken)
        {
            try
            {
                bool result = false;
                var isDevelopment = _configuration.GetSection("IsDevelopment");

                if (request.IdentityNumber != null && request.IdentityNumber != "")
                {
                   
                        string identityNumber = AESMgr.Decrypt(request.IdentityNumber);

                        Regex rexPersonal = new Regex(@"^[0-9]{13}$");
                        if (rexPersonal.IsMatch(identityNumber))
                        {
                            int sum = 0;

                            for (int i = 0; i < 12; i++)
                            {
                                sum += int.Parse(identityNumber[i].ToString()) * (13 - i);
                            }

                            result = (int.Parse(identityNumber[12].ToString()) == ((11 - (sum % 11)) % 10));
                        }
                    if (isDevelopment != null && isDevelopment.Value == "true")
                    {
                        result = true;
                    }
                    if (result)
                    {
                        var customer = (await _customerRepository.FindByCondition(x => x.IdentityNumber == request.IdentityNumber && x.Id.Equals(request.Id) && x.IsDelete == false).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                        if (customer == null)
                        {
                            result = false;
                        }
                    }
                }
                if (result)
                {
                    return new Response<bool>(result);
                }
                else
                {
                    return new Response<bool>(result, "เลขที่บัตรประชาชนไม่ถูกต้อง", "Invalid IDcard number.");
                }
            }
            catch (Exception ex)
            {
                return new Response<bool>(false, "เลขที่บัตรประชาชนไม่ถูกต้อง", "Invalid IDcard number.");
            }
        }
    }
}
