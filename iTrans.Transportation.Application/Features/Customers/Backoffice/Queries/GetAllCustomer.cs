﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.Customer.Backoffice;
using iTrans.Transportation.Application.DTOs.CustomerFile;
using iTrans.Transportation.Application.DTOs.UserLevel;
using iTrans.Transportation.Application.DTOs.VerifyStatus;
using iTrans.Transportation.Application.Helper;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;

namespace iTrans.Transportation.Application.Features.Customers.Backoffice.Queries
{
    public class GetAllCustomer : IRequest<PagedResponse<IEnumerable<CustomerViewModel>>>
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public string Search { get; set; }
        public string Customertype { get; set; }
        public int Active { set; get; }
        public string Column { set; get; }

    }
    public class GetAllCustomerHandler : IRequestHandler<GetAllCustomer, PagedResponse<IEnumerable<CustomerViewModel>>>
    {
        private readonly ICustomerRepositoryAsync _customerRepository;
        private readonly ICustomerFileRepositoryAsync _customerFileRepository;
        private readonly IUserLevelRepositoryAsync _userLevelRepository;
        private readonly IMapper _mapper;
        private Expression<Func<Customer, bool>> expression;

        public GetAllCustomerHandler(ICustomerRepositoryAsync customerRepository, IUserLevelRepositoryAsync userLevelRepository, ICustomerFileRepositoryAsync customerFileRepository, IMapper mapper )
        {
            _customerRepository = customerRepository;
            _userLevelRepository = userLevelRepository;
            _customerFileRepository = customerFileRepository;
            _mapper = mapper;
        }

        public async Task<PagedResponse<IEnumerable<CustomerViewModel>>> Handle(GetAllCustomer request, CancellationToken cancellationToken)
        {
            var folderDetails = Path.Combine(Directory.GetCurrentDirectory(), $"Shared\\VerifyStatus.json");
            var statusJson = System.IO.File.ReadAllText(folderDetails);
            List<VerifyStatusViewModel> verifyStatus = Newtonsoft.Json.JsonConvert.DeserializeObject<List<VerifyStatusViewModel>>(statusJson);

            var customerLevel = _mapper.Map<List<UserLevelViewModel>>((await _userLevelRepository.FindByCondition(x => x.Module == "customer" && x.Active == true).ConfigureAwait(false)).AsQueryable().ToList());
            var validFilter = _mapper.Map<GetAllCustomer>(request);
            var file = _mapper.Map<List<CustomerFileViewModel>>((await _customerFileRepository.GetAllAsync()));

            var result = from x in _customerRepository.GetAllAsync().Result.Where(x => x.CustomerType.Contains(validFilter.Customertype)).ToList()
                         join u in _userLevelRepository.GetAllAsync().Result.Where(x => x.Module == "customer" && x.Active == true) on x.Level equals u.Level
                         join v in verifyStatus on x.VerifyStatus equals v.id
                      
                         select new CustomerViewModel
                         {
                             Id = x.Id,
                             CustomerType = x.CustomerType != null ? x.CustomerType : "-",
                             Name = x.Name != null ? x.Name.Replace("  ", " ") : "-",
                             PhoneNumber = String.Format("{0}{1}", x.PhoneCode, x.PhoneNumber),
                             IdentityNumber = x.IdentityNumber != null ? AESMgr.Decrypt(x.IdentityNumber) : "-",
                             LevelName = u.Name_TH != null ? u.Name_TH : "-",
                             Created = x.Created.ToString("yyyy/MM/dd HH:MM"),
                             Files = file.Where(f => f.CustomerId.Equals(x.Id)).ToList(),
                             VerifyStatusName = v.th != null ? v.th : "-",
                             VerifyStatusColor = v.color != null ? v.color : "#000",
                             Status = x.Status

                         };
            if (validFilter.Search != null && validFilter.Search.Trim() != "")
            {
                result = result.Where(l => l.CustomerType.Trim().ToLower().Contains(validFilter.Search.Trim().ToLower()) || l.LevelName.ToLower().Contains(validFilter.Search.Trim().ToLower()) 
                    || l.Name.Trim().ToLower().Contains(validFilter.Search.Trim().ToLower())  || l.PhoneNumber.Trim().ToLower().Contains(validFilter.Search.Trim().ToLower()) || l.VerifyStatusName.ToLower().Contains(validFilter.Search.Trim().ToLower())
                    || l.IdentityNumber.Trim().ToLower().Contains(validFilter.Search.Trim().ToLower()) || l.Created.Trim().ToLower().Contains(validFilter.Search.Trim().ToLower())).ToList();
            }
            var itemCount = result.Count();
            result = result.Skip((validFilter.PageNumber - 1) * validFilter.PageSize).Take(validFilter.PageSize);

            if (request.Column == "name" && request.Active == 2)
            {
                result = result.OrderByDescending(x => x.Name);
            }
            if (request.Column == "name" && request.Active == 3)
            {
                result = result.OrderBy(x => x.Name);
            }
            if (request.Column == "identityNumber" && request.Active == 2)
            {
                result = result.OrderByDescending(x => x.IdentityNumber);
            }
            if (request.Column == "phoneNumber" && request.Active == 3)
            {
                result = result.OrderBy(x => x.PhoneNumber);
            }
            if (request.Column == "phoneNumber" && request.Active == 2)
            {
                result = result.OrderByDescending(x => x.PhoneNumber);
            }
            if (request.Column == "customerType" && request.Active == 3)
            {
                result = result.OrderBy(x => x.CustomerType);
            }
            if (request.Column == "customerType" && request.Active == 2)
            {
                result = result.OrderByDescending(x => x.CustomerType);
            }
            if (request.Column == "levelName" && request.Active == 3)
            {
                result = result.OrderBy(x => x.LevelName);
            }
            if (request.Column == "levelName" && request.Active == 2)
            {
                result = result.OrderByDescending(x => x.LevelName);
            }
            if (request.Column == "verifyStatusName" && request.Active == 3)
            {
                result = result.OrderBy(x => x.VerifyStatusName);
            }
            if (request.Column == "verifyStatusName" && request.Active == 2)
            {
                result = result.OrderByDescending(x => x.VerifyStatusName);
            }
            if (request.Column == "created" && request.Active == 3)
            {
                result = result.OrderBy(x => x.Created);
            }
            if (request.Column == "created" && request.Active == 2)
            {
                result = result.OrderByDescending(x => x.Created);
            }
          

            return new PagedResponse<IEnumerable<CustomerViewModel>>(result, validFilter.PageNumber, validFilter.PageSize, itemCount);
        }
    }
}
