﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.Customer.Backoffice;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.Customers.Backoffice.Queries
{
    public class GetCustomerTaxById : IRequest<Response<TaxCustomerAddressViewModel>>
    {
        public Guid customerId { get; set; }
    }
    public class GetCustomerTaxByIdHandler : IRequestHandler<GetCustomerTaxById, Response<TaxCustomerAddressViewModel>>
    {
        private readonly ICustomerAddressRepositoryAsync _customerAddressRepository;
        private readonly ICustomerRepositoryAsync _customerRepository;

        private readonly IMapper _mapper;
        public GetCustomerTaxByIdHandler(ICustomerAddressRepositoryAsync CustomerAddressRepository, ICustomerRepositoryAsync customerRepository, IMapper mapper)
        {
            _customerAddressRepository = CustomerAddressRepository;
            _customerRepository = customerRepository;
            _mapper = mapper;
        }
        public async Task<Response<TaxCustomerAddressViewModel>> Handle(GetCustomerTaxById request, CancellationToken cancellationToken)
        {


            var customer = _mapper.Map<CustomerViewModel>((await _customerRepository.FindByCondition(x => x.Id.Equals(request.customerId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault());
            var customerpersonal = _mapper.Map<CustomerViewModel>((await _customerRepository.FindByCondition(x => x.Id.Equals(request.customerId) && x.CustomerType == "personal ").ConfigureAwait(false)).AsQueryable().FirstOrDefault());
            var customercorporate = _mapper.Map<CustomerViewModel>((await _customerRepository.FindByCondition(x => x.Id.Equals(request.customerId) && x.CustomerType == "corporate").ConfigureAwait(false)).AsQueryable().FirstOrDefault());
            var result = new TaxCustomerAddressViewModel();

            if (customerpersonal != null) {

                result.customerId = customer.Id;
                result.customerType = customer.CustomerType;
                result.personal = customerpersonal.Addresses.Where(x => x.AddressType == "tax" && x.IsMainData == true).FirstOrDefault();
                if (result.personal == null)
                {
                    result.personal = customerpersonal.Addresses.Where(x => x.AddressType == "register" && x.IsMainData == true).FirstOrDefault();
                }

            }
            else if(customercorporate != null)
            {

                result.customerId = customer.Id;
                result.customerType = customer.CustomerType;
                result.corporate = customercorporate.Addresses.Where(x => x.AddressType == "tax" ).ToList();
                if (result.corporate.Count() ==0)
                {
                    result.corporate = customercorporate.Addresses.Where(x => x.AddressType == "register" && x.IsMainData == true).ToList();
                }

            }

            return new Response<TaxCustomerAddressViewModel>(_mapper.Map<TaxCustomerAddressViewModel>(result));
        }
    }
}
