﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.Customer;
using iTrans.Transportation.Application.DTOs.Customer.Backoffice;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.Customers.Backoffice.Queries
{
   public class GetCustomerByVerifyStatus : IRequest<Response<CustomerByVerifyStatusViewModel>>
    {

    }
    public class GetCustomerByVerifyStatusHandler : IRequestHandler<GetCustomerByVerifyStatus, Response<CustomerByVerifyStatusViewModel>>
    {
        private readonly ICustomerRepositoryAsync _customerRepository;
   
        private readonly IMapper _mapper;
        public GetCustomerByVerifyStatusHandler(ICustomerRepositoryAsync customerRepository, IMapper mapper)
        {
            _customerRepository = customerRepository;
            _mapper = mapper;
        }

        public async Task<Response<CustomerByVerifyStatusViewModel>> Handle(GetCustomerByVerifyStatus request, CancellationToken cancellationToken)
        {
            var customer = await _customerRepository.GetAllAsync();
            var result = new CustomerByVerifyStatusViewModel();

            result.presonalstatustrue = customer.Where(c => c.VerifyStatus.Equals(1) && c.CustomerType.Equals("personal")).Count();
            result.presonalstatusfalse = customer.Where(c => c.VerifyStatus.Equals(2) && c.CustomerType.Equals("personal")).Count();
            result.corporatestatustrue = customer.Where(c => c.VerifyStatus.Equals(1) && c.CustomerType.Equals("corporate")).Count();
            result.corporatestatusfalse = customer.Where(c => c.VerifyStatus.Equals(2) && c.CustomerType.Equals("corporate")).Count();

            return new Response<CustomerByVerifyStatusViewModel>(result);
        }
    }
}