﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.Customer.Backoffice;
using iTrans.Transportation.Application.DTOs.CustomerTermAndCondition;
using iTrans.Transportation.Application.DTOs.UserLevel;
using iTrans.Transportation.Application.DTOs.VerifyStatus;
using iTrans.Transportation.Application.Helper;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.Customers.Backoffice.Queries
{
    public class GetCustomerById : IRequest<Response<CustomerViewModel>>
    {
        public Guid Id { get; set; }
        public string ContentDirectory { get; set; }

    }
    public class GetCustomerByIdQueryHandler : IRequestHandler<GetCustomerById, Response<CustomerViewModel>>
    {
        private readonly ICustomerRepositoryAsync _customerRepository;
        private readonly IUserLevelRepositoryAsync _userLevelRepository;
        private readonly ICustomerTermAndConditionRepositoryAsync _customerTermAndConditionRepository;

        private readonly IMapper _mapper;
        public GetCustomerByIdQueryHandler(ICustomerRepositoryAsync customerRepository, ICustomerTermAndConditionRepositoryAsync customerTermAndConditionRepository, IUserLevelRepositoryAsync userLevelRepository, IMapper mapper)
        {
            _customerRepository = customerRepository;
            _customerTermAndConditionRepository = customerTermAndConditionRepository;
            _userLevelRepository = userLevelRepository;
            _mapper = mapper;
        }
        public async Task<Response<CustomerViewModel>> Handle(GetCustomerById request, CancellationToken cancellationToken)
        {
            var folderDetails = Path.Combine(Directory.GetCurrentDirectory(), $"Shared\\VerifyStatus.json");
            var statusJson = System.IO.File.ReadAllText(folderDetails);
            List<VerifyStatusViewModel> verifyStatus = Newtonsoft.Json.JsonConvert.DeserializeObject<List<VerifyStatusViewModel>>(statusJson);

            var customerObject = (await _customerRepository.FindByCondition(x => x.Id.Equals(request.Id)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();

            var customerterm = (await _customerTermAndConditionRepository.FindByCondition(x => x.Customer.Id.Equals(request.Id)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();

            var customer = _mapper.Map<CustomerViewModel>(customerObject);
            if (customer != null)
            {
                //if (customer.Files != null)
                //{
                //    customer.Files.RemoveAll(f => f.IsDelete);
                //}
                customer.IdentityNumber = AESMgr.Decrypt(customer.IdentityNumber);
                if (customer.Files != null) customer.Files = customer.Files.Where(x => x.IsDelete == false).ToList();
                customer.CustomerLevel = _mapper.Map<UserLevelViewModel>((await _userLevelRepository.FindByCondition(x => x.Level.Equals(customer.Level) && x.Active == true && x.Module == "customer").ConfigureAwait(false)).AsQueryable().FirstOrDefault());
                customer.Addresses = customer.Addresses.Where(x => x.AddressType == "register").ToList();
                customer.VerifyStatusObj = _mapper.Map<VerifyStatusViewModel>(verifyStatus.Where(f => f.id.Equals(customer.VerifyStatus)).FirstOrDefault());
                if (customer.PhoneCode == "+66" && customer.PhoneNumber != null && customer.PhoneNumber.Trim() != "")
                {
                    customer.PhoneNumber = "0" + customer.PhoneNumber;
                }
                //customer.PhoneNumber = "0" + customer.PhoneNumber;
                //if (customer.Files.Where(x => x.DocumentType.Equals("profilepicture")).Select(x => x.FilePath).FirstOrDefault() != null)
                //{
                //    customer.imageProfile = ConvertImage.ConvertImageURLToBase64(request.ContentDirectory + "/" + customer.Files.Where(x => x.DocumentType.Equals("profilepicture")).Select(x => x.FilePath).FirstOrDefault());
                //}
                //if (customer.Files.Where(x => x.DocumentType.Equals("IdCard")).Select(x => x.FilePath).FirstOrDefault() != null)
                //{
                //    if (customer.VerifyStatus == 0)
                //    {
                //        customer.idCard = ConvertImage.ConvertImageURLToBase64(request.ContentDirectory + "/" + customer.Files.Where(x => x.DocumentType.Equals("IdCard") && !x.IsApprove && !x.IsDelete).Select(x => x.FilePath).FirstOrDefault());
                //    }
                //    else
                //    {
                //        customer.idCard = ConvertImage.ConvertImageURLToBase64(request.ContentDirectory + "/" + customer.Files.Where(x => x.DocumentType.Equals("IdCard") && !x.IsDelete).Select(x => x.FilePath).FirstOrDefault());
                //    }
                //    //customer.idCard = ConvertImage.ConvertImageURLToBase64(request.ContentDirectory + "/" + customer.Files.Where(x => x.DocumentType.Equals("IdCard")).Select(x => x.FilePath).FirstOrDefault());
                //}
                //if (customer.Files.Where(x => x.DocumentType.Equals("CompanyCertificate")).Select(x => x.FilePath).FirstOrDefault() != null)
                //{
                //    if (customer.VerifyStatus == 0)
                //    {
                //        customer.companyCertificate = ConvertImage.ConvertImageURLToBase64(request.ContentDirectory + "/" + customer.Files.Where(x => x.DocumentType.Equals("CompanyCertificate") && !x.IsApprove && !x.IsDelete).Select(x => x.FilePath).FirstOrDefault());
                //    }
                //    else
                //    {
                //        customer.companyCertificate = ConvertImage.ConvertImageURLToBase64(request.ContentDirectory + "/" + customer.Files.Where(x => x.DocumentType.Equals("CompanyCertificate") && !x.IsDelete).Select(x => x.FilePath).FirstOrDefault());
                //    }
                //      //  customer.companyCertificate = ConvertImage.ConvertImageURLToBase64(request.ContentDirectory + "/" + customer.Files.Where(x => x.DocumentType.Equals("CompanyCertificate")).Select(x => x.FilePath).FirstOrDefault());
                //}
            }

            return new Response<CustomerViewModel>(customer);
        }
      
    }
}
