﻿using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Interfaces.Services;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using iTrans.Transportation.Application.Helper;

namespace iTrans.Transportation.Application.Features.Customers.Backoffice.Commands
{
    public partial class UpdateCustomerByIdCommand : IRequest<Response<Guid>>
    {
        public Guid? UserId { get; set; }
        public Guid Id { get; set; }
        public int CorporateTypeId { get; set; }
        public int TitleId { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public int IdentityType { get; set; }
        public string IdentityNumber { get; set; }
        public DateTime Birthday { get; set; }
        public string PhoneCode { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string Facbook { get; set; }
        public string Line { get; set; }
        public string Twitter { get; set; }
        public string Whatapp { get; set; }
        public string Wechat { get; set; }
        public int Level { get; set; }
        public int CountryId { get; set; }
        public int ProvinceId { get; set; }
        public int DistrictId { get; set; }
        public int SubdistrictId { get; set; }
        public string PostCode { get; set; }
        public string Road { get; set; }
        public string Alley { get; set; }
        public string Address { get; set; }
        public int ContactPersonTitleId { get; set; }
        public string ContactPersonFirstName { get; set; }
        public string ContactPersonLastName { get; set; }
        public string Name { get; set; }
        public string idCard { get; set; }
        public bool Status { get; set; }

        public int VerifyStatus { get; set; }

        public string APP_ID { get; set; }
        public string REST_API_KEY { get; set; }
        public string AUTH_ID { get; set; }
        public string ContentDirectory { get; set; }

        public IFormFile ProfilePicture { get; set; }


    }

    public class UpdateCustomerByIdCommandHandler : IRequestHandler<UpdateCustomerByIdCommand, Response<Guid>>
    {
        private readonly ICustomerRepositoryAsync _customerRepository;
        private readonly ICustomerAddressRepositoryAsync _customerAddressRepository;
        private readonly ICustomerFileRepositoryAsync _customerFileRepository;
        private readonly ICorporateTypeRepositoryAsync _corporateTypeRepository;
        private readonly ITitleRepositoryAsync _titleRepository;
        private readonly ICountryRepositoryAsync _countryRepository;
        private readonly IProvinceRepositoryAsync _provinceRepository;
        private readonly IDistrictRepositoryAsync _districtRepository;
        private readonly ISubdistrictRepositoryAsync _subdistrictRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;
        private readonly IConfiguration _configuration;
        private readonly INotificationRepositoryAsync _notificationRepository;
        private readonly INotificationUserRepositoryAsync _notificationUserRepository;
        public UpdateCustomerByIdCommandHandler(ICustomerRepositoryAsync customerRepository, ICustomerAddressRepositoryAsync customerAddressRepository, ICustomerFileRepositoryAsync customerFileRepository
            , ICorporateTypeRepositoryAsync corporateTypeRepository, ITitleRepositoryAsync titleRepository, ICountryRepositoryAsync countryRepository, IProvinceRepositoryAsync provinceRepository, IDistrictRepositoryAsync districtRepository
            , ISubdistrictRepositoryAsync subdistrictRepository, IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper, IConfiguration configuration
            , INotificationRepositoryAsync notificationRepository, INotificationUserRepositoryAsync notificationUserRepository)
        {
            _customerRepository = customerRepository;
            _customerAddressRepository = customerAddressRepository;
            _customerFileRepository = customerFileRepository;
            _corporateTypeRepository = corporateTypeRepository;
            _titleRepository = titleRepository;
            _countryRepository = countryRepository;
            _provinceRepository = provinceRepository;
            _districtRepository = districtRepository;
            _subdistrictRepository = subdistrictRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
            _configuration = configuration;
            _notificationRepository = notificationRepository;
            _notificationUserRepository = notificationUserRepository;
        }

        public async Task<Response<Guid>> Handle(UpdateCustomerByIdCommand request, CancellationToken cancellationToken)
        {

            try
            {
                var customer = (await _customerRepository.FindByCondition(x => x.Id == request.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (customer == null)
                {
                    throw new ApiException($"Customer Not Found.");
                }
                else
                {
                    CorporateType corporateType = (await _corporateTypeRepository.FindByCondition(x => x.Id.Equals(request.CorporateTypeId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                    Title title = (await _titleRepository.FindByCondition(x => x.Id.Equals(request.TitleId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                    Title contactPersonTitle = (await _titleRepository.FindByCondition(x => x.Id.Equals(request.ContactPersonTitleId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                    Country country = (await _countryRepository.FindByCondition(x => x.Id.Equals(request.CountryId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                    Province province = (await _provinceRepository.FindByCondition(x => x.Id.Equals(request.ProvinceId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                    District district = (await _districtRepository.FindByCondition(x => x.Id.Equals(request.DistrictId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                    Subdistrict subdistrict = (await _subdistrictRepository.FindByCondition(x => x.Id.Equals(request.SubdistrictId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();

                    var IdentityNumber = AESMgr.Encrypt(request.IdentityNumber);
                    string phoneNumber = request.PhoneNumber != null ? request.PhoneNumber : "";
                    if (request.PhoneNumber != null && request.PhoneNumber.Length > 9 && request.PhoneNumber.First() == '0')
                    {
                        phoneNumber = request.PhoneNumber.Remove(0, 1);
                    }
                    customer.CorporateType = corporateType;
                    customer.Title = title;
                    customer.FirstName = request.FirstName != null ? request.FirstName : "";
                    customer.MiddleName = request.MiddleName != null ? request.MiddleName : "";
                    customer.LastName = request.LastName != null ? request.LastName : "";
                    customer.IdentityType = request.IdentityType;
                    customer.IdentityNumber = request.IdentityNumber;
                    customer.ContactPersonTitle = contactPersonTitle;
                    customer.ContactPersonMiddleName = request.ContactPersonFirstName != null ? request.ContactPersonFirstName : "";
                    customer.ContactPersonFirstName = request.ContactPersonFirstName != null ? request.ContactPersonFirstName : "";
                    customer.Name = request.Name != null ? request.Name : "";
                    customer.ContactPersonLastName = request.ContactPersonLastName != null ? request.ContactPersonLastName : "";
                    customer.Birthday = request.Birthday;
                    customer.Level = request.Level;
                    customer.PhoneNumber = phoneNumber;
                    customer.Email = request.Email != null ? request.Email : "";
                    customer.Facbook = request.Facbook != null ? request.Facbook : "";
                    customer.Line = request.Line != null ? request.Line : "";
                    customer.Twitter = request.Twitter != null ? request.Twitter : "";
                    customer.Whatapp = request.Whatapp != null ? request.Whatapp : "";
                    customer.Wechat = request.Wechat != null ? request.Wechat : "";
                    customer.VerifyStatus = request.VerifyStatus;
                    customer.Status = request.Status;
                    customer.Modified = DateTime.UtcNow;
                    customer.CreatedBy = "xxxxxxx";
                    customer.ModifiedBy = "xxxxxxx";

                    await _customerRepository.UpdateAsync(customer);
                    string folderPath = request.ContentDirectory + "customer/" + request.Id.ToString();
                    if (!Directory.Exists(folderPath))
                    {
                        Directory.CreateDirectory(folderPath);
                    }
                    if (request.ProfilePicture != null)
                    {
                        var file = _customerFileRepository.FindByCondition(f => f.DocumentType.Equals("profilepicture") && f.CustomerId.Equals(request.Id)).Result.FirstOrDefault();
                        var fileCount = _customerFileRepository.FindByCondition(f => f.CustomerId.Equals(request.Id)).Result.Count();
                        if (file != null)
                        {

                            string filePath = Path.Combine(folderPath, request.ProfilePicture.FileName);
                            using (FileStream fileStream = new FileStream(filePath, FileMode.Create))
                            {
                                await request.ProfilePicture.CopyToAsync(fileStream);
                                FileInfo fi = new FileInfo(filePath);


                                file.CustomerId = request.Id;
                                file.FileName = request.ProfilePicture.FileName;
                                file.ContentType = request.ProfilePicture.ContentType;
                                file.FilePath = "customer/" + request.Id.ToString() + "/" + request.ProfilePicture.FileName;
                                file.DirectoryPath = "customer/" + request.Id.ToString() + "/";
                                file.FileEXT = fi.Extension;
                                file.DocumentType = "profilepicture";
                                file.Sequence = file.Sequence;


                                await _customerFileRepository.UpdateAsync(file);
                            }
                        }
                        else
                        {
                            string filePath = Path.Combine(folderPath, request.ProfilePicture.FileName);
                            using (FileStream fileStream = new FileStream(filePath, FileMode.Create))
                            {
                                await request.ProfilePicture.CopyToAsync(fileStream);
                                FileInfo fi = new FileInfo(filePath);

                                CustomerFile customerFile = new CustomerFile
                                {
                                    CustomerId = request.Id,
                                    FileName = request.ProfilePicture.FileName,
                                    ContentType = request.ProfilePicture.ContentType,
                                    FilePath = "customer/" + request.Id.ToString() + "/" + request.ProfilePicture.FileName,
                                    DirectoryPath = "customer/" + request.Id.ToString() + "/",
                                    FileEXT = fi.Extension,
                                    DocumentType = "profilepicture",
                                    Sequence = fileCount + 1
                                };
                                await _customerFileRepository.AddAsync(customerFile);
                            }
                        }
                    }
                    var customerAddress = (await _customerAddressRepository.FindByCondition(x => x.CustomerId == request.Id && x.AddressType == "register").ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                    if (customerAddress == null)
                    {
                        CustomerAddress address = new CustomerAddress
                        {
                            CustomerId = request.Id,
                            Country = country,
                            Province = province,
                            District = district,
                            Subdistrict = subdistrict,
                            PostCode = request.PostCode != null ? request.PostCode : "",
                            Road = request.Road != null ? request.Road : "",
                            Alley = request.Alley != null ? request.Alley : "",
                            Address = request.Address != null ? request.Address : "",
                            Branch = "",
                            AddressType = "register",
                            AddressName = "",
                            ContactPerson = "",
                            ContactPhoneNumber = "",
                            ContactEmail = "",
                            IsMainData = true,
                            Maps = "",
                            Sequence = 1,
                            Created = DateTime.UtcNow,
                            Modified = DateTime.UtcNow,
                            CreatedBy = "xxxxxxx",
                            ModifiedBy = "xxxxxxx"
                        };
                        var customerAddressObject = await _customerAddressRepository.AddAsync(address);
                    }
                    else
                    {
                        customerAddress.CustomerId = request.Id;
                        customerAddress.Country = country;
                        customerAddress.Province = province;
                        customerAddress.District = district;
                        customerAddress.Subdistrict = subdistrict;
                        customerAddress.PostCode = request.PostCode != null ? request.PostCode : "";
                        customerAddress.Road = request.Road != null ? request.Road : "";
                        customerAddress.Alley = request.Alley != null ? request.Alley : "";
                        customerAddress.Address = request.Address != null ? request.Address : "";
                        customerAddress.IsMainData = true;
                        await _customerAddressRepository.UpdateAsync(customerAddress);
                    }
                    if (request.VerifyStatus == 2)
                    {
                        foreach (CustomerFile customerFile in customer.Files)
                        {
                            if (customerFile.IsDelete && File.Exists(Path.Combine(request.ContentDirectory, customerFile.FilePath)))
                                 File.Delete(Path.Combine(request.ContentDirectory, customerFile.FilePath));
                        }
                        (await _customerFileRepository.CreateSQLQuery("DELETE Customer_File where CustomerId = '" + request.Id.ToString() + "' and IsDelete = 1").ConfigureAwait(false)).ExecuteUpdate();
                        (await _customerFileRepository.CreateSQLQuery("UPDATE Customer_File  SET IsApprove = 1 where CustomerId = '" + request.Id.ToString() + "'").ConfigureAwait(false)).ExecuteUpdate();
                        
                    }
                    if (request.VerifyStatus == 1)
                    {
                        #region add noti list
                        var oneSignalIds = (await _notificationUserRepository.FindByCondition(x => x.UserId.Equals(request.Id)).ConfigureAwait(false)).AsQueryable().Select(x => x.OneSignalId).ToList();
                        Domain.Notification notification = new Domain.Notification();

                        notification.OneSignalId = oneSignalIds.FirstOrDefault();
                        notification.UserId = request.Id;
                        notification.OwnerId = request.Id;
                        notification.Title = "Documents do not pass";
                        notification.Detail = "Documents do not pass";
                        notification.ServiceCode = "Reject Doc";
                        //notification.ReferenceContentKey = request.ReferenceContentKey;
                        //notification.Payload = Newtonsoft.Json.JsonConvert.SerializeObject(cloneOrdering);
                        notification.Created = DateTime.UtcNow;
                        notification.Modified = DateTime.UtcNow;
                        //notification.CreatedBy = ;
                        //notification.ModifiedBy = ;
                        await _notificationRepository.AddAsync(notification);
                        #endregion

                        #region send noti

                        if (oneSignalIds.Count > 0)
                        {
                            NotificationManager notiMgr = new NotificationManager(request.APP_ID, request.REST_API_KEY, request.AUTH_ID);
                            notiMgr.SendNotificationAsync(notification.Title, notification.Detail, oneSignalIds, notification.ServiceCode, "", "");
                        }

                        #endregion
                    }

                    var log = new CreateAppLog(_applicationLogRepository);
                    log.CreateLog("Customer", "Customer", "Update", Newtonsoft.Json.JsonConvert.SerializeObject(request), request.UserId.ToString());
                    return new Response<Guid>(customer.Id);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


    }
}
