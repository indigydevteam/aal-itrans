﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.Customer.Backoffice;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;

namespace iTrans.Transportation.Application.Features.Customers.Backoffice.Commands
{
    public class UpdateCustomerTaxById : IRequest<Response<int>>
    {
        public Guid? UserId { get; set; }
        public List<CustomerLocationViewModel> location { get; set; }
    }

    public class UpdateCustomerTaxByIdHandler : IRequestHandler<UpdateCustomerTaxById, Response<int>>
    {
        private readonly ICustomerAddressRepositoryAsync _customerAddressRepository;
        private readonly ICountryRepositoryAsync _countryRepository;
        private readonly IProvinceRepositoryAsync _provinceRepository;
        private readonly IDistrictRepositoryAsync _districtRepository;
        private readonly ISubdistrictRepositoryAsync _subdistrictRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;

        public UpdateCustomerTaxByIdHandler(ICustomerAddressRepositoryAsync CustomerAddressRepository, ICountryRepositoryAsync countryRepository, IProvinceRepositoryAsync provinceRepository, IDistrictRepositoryAsync districtRepository, ISubdistrictRepositoryAsync subdistrictRepository, IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _customerAddressRepository = CustomerAddressRepository;
            _countryRepository = countryRepository;
            _provinceRepository = provinceRepository;
            _districtRepository = districtRepository;
            _subdistrictRepository = subdistrictRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(UpdateCustomerTaxById request, CancellationToken cancellationToken)
        {
            foreach (var i in request.location)
            {

                var CustomerAddress = (await _customerAddressRepository.FindByCondition(x => x.Id == i.Id && x.CustomerId.Equals(i.CustomerId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();

                if (CustomerAddress == null)
                {
                    throw new ApiException($"CustomerAddress Not Found.");
                }
                else
                {

                    if (i.IsMainData)
                    {
                        (await _customerAddressRepository.CreateSQLQuery("UPDATE Customer_Address SET IsMainData  = 0 where CustomerId = '" + i.CustomerId.ToString() + "' and AddressType = '" + i.AddressType.Trim() + "' ").ConfigureAwait(false)).ExecuteUpdate();
                    }
                    Country country = (await _countryRepository.FindByCondition(x => x.Id.Equals(i.CountryId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                    Province province = (await _provinceRepository.FindByCondition(x => x.Id.Equals(i.ProvinceId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                    District district = (await _districtRepository.FindByCondition(x => x.Id.Equals(i.DistrictId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                    Subdistrict subdistrict = (await _subdistrictRepository.FindByCondition(x => x.Id.Equals(i.SubdistrictId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();



                    CustomerAddress.CustomerId = i.CustomerId;
                    CustomerAddress.Country = country;
                    CustomerAddress.Province = province;
                    CustomerAddress.District = district;
                    CustomerAddress.Subdistrict = subdistrict;
                    CustomerAddress.PostCode = i.PostCode;
                    CustomerAddress.Road = i.Road;
                    CustomerAddress.Alley = i.Alley;
                    CustomerAddress.Address = i.Address;
                    CustomerAddress.AddressType = i.AddressType;
                    CustomerAddress.IsMainData = i.IsMainData;
                    CustomerAddress.Sequence = i.Sequence;
                    CustomerAddress.Modified = DateTime.Now;

                    if (i.AddressType != "tax")
                    {

                        CustomerAddress address = new CustomerAddress
                        {
                            CustomerId = i.CustomerId,
                            Country = country,
                            Province = province,
                            District = district,
                            Subdistrict = subdistrict,
                            PostCode = i.PostCode,
                            Road = i.Road,
                            Alley = i.Alley,
                            Address = i.Address,
                            Branch = "",
                            AddressType = "tax",
                            AddressName = "",
                            ContactPerson = "",
                            ContactPhoneNumber = "",
                            ContactEmail = "",
                            IsMainData = true,
                            Maps = "",
                            Sequence = 1,
                            Created = DateTime.Now,
                            Modified = DateTime.Now,
                            CreatedBy = "xxxxxxx",
                            ModifiedBy = "xxxxxxx"
                        };

                        await _customerAddressRepository.AddAsync(address);
                    }
                    else
                    {
                        await _customerAddressRepository.UpdateAsync(CustomerAddress);
                    }
                }
            }
            var log = new CreateAppLog(_applicationLogRepository);
            log.CreateLog("Customer", "Customer", "Update", Newtonsoft.Json.JsonConvert.SerializeObject(request), request.UserId.ToString());
            return new Response<int>(request.location[0].Id);

        }
    }
}
