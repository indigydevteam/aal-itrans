﻿using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Interfaces.Services;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;

namespace iTrans.Transportation.Application.Features.Customers.Commands
{
    public partial class UpdateCustomerInformationByIdCommand : IRequest<Response<Guid>>
    {

        public Guid? UserId { get; set; }

        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public Guid Id { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public int CorporateTypeId { get; set; }
        public int TitleId { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string FirstName { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string MiddleName { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string LastName { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string Name { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public int IdentityType { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string IdentityNumber { get; set; }
        public int ContactPersonTitleId { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string ContactPersonFirstName { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string ContactPersonMiddleName { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string ContactPersonLastName { get; set; }
        public DateTime Birthday { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string PhoneCode { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string PhoneNumber { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string Email { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string Facbook { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string Line { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string Twitter { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string Whatapp { get; set; }
        public int CountryId { get; set; }
        public int ProvinceId { get; set; }
        public int DistrictId { get; set; }
        public int SubdistrictId { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string PostCode { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string Road { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string Alley { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string Address { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string idCard { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string drivingLicense { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string driverPicture { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string companyCertificate { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string NP20 { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string ProfilePicture { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string DeleteFiles { get; set; }
        public List<IFormFile> files { get; set; }
    }

    public class UpdateCustomerInformationByIdCommandHandler : IRequestHandler<UpdateCustomerInformationByIdCommand, Response<Guid>>
    {
        private readonly ICustomerRepositoryAsync _customerRepository;
        private readonly ICustomerAddressRepositoryAsync _customerAddressRepository;
        private readonly ICustomerFileRepositoryAsync _customerFileRepository;
        private readonly ICorporateTypeRepositoryAsync _corporateTypeRepository;
        private readonly ITitleRepositoryAsync _titleRepository;
        private readonly ICountryRepositoryAsync _countryRepository;
        private readonly IProvinceRepositoryAsync _provinceRepository;
        private readonly IDistrictRepositoryAsync _districtRepository;
        private readonly ISubdistrictRepositoryAsync _subdistrictRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;
        private readonly IConfiguration _configuration;
        public UpdateCustomerInformationByIdCommandHandler(ICustomerRepositoryAsync customerRepository, ICustomerAddressRepositoryAsync customerAddressRepository, ICustomerFileRepositoryAsync customerFileRepository
            , ICorporateTypeRepositoryAsync corporateTypeRepository, ITitleRepositoryAsync titleRepository, ICountryRepositoryAsync countryRepository, IProvinceRepositoryAsync provinceRepository, IDistrictRepositoryAsync districtRepository
            , ISubdistrictRepositoryAsync subdistrictRepository, IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper, IConfiguration configuration)
        {
            _customerRepository = customerRepository;
            _customerAddressRepository = customerAddressRepository;
            _customerFileRepository = customerFileRepository;
            _corporateTypeRepository = corporateTypeRepository;
            _titleRepository = titleRepository;
            _countryRepository = countryRepository;
            _provinceRepository = provinceRepository;
            _districtRepository = districtRepository;
            _subdistrictRepository = subdistrictRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
            _configuration = configuration;
        }

        public async Task<Response<Guid>> Handle(UpdateCustomerInformationByIdCommand request, CancellationToken cancellationToken)
        {
            var getContentPath = _configuration.GetSection("ContentPath");
            string contentPath = getContentPath.Value;
            try
            {
                var customer = (await _customerRepository.FindByCondition(x => x.Id == request.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (customer == null)
                {
                    throw new ApiException($"Customer Not Found.");
                }
                else
                {
                    CorporateType corporateType = (await _corporateTypeRepository.FindByCondition(x => x.Id.Equals(request.CorporateTypeId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                    Title title = (await _titleRepository.FindByCondition(x => x.Id.Equals(request.TitleId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                    Title contactPersonTitle = (await _titleRepository.FindByCondition(x => x.Id.Equals(request.ContactPersonTitleId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                    Country country = (await _countryRepository.FindByCondition(x => x.Id.Equals(request.CountryId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                    Province province = (await _provinceRepository.FindByCondition(x => x.Id.Equals(request.ProvinceId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                    District district = (await _districtRepository.FindByCondition(x => x.Id.Equals(request.DistrictId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                    Subdistrict subdistrict = (await _subdistrictRepository.FindByCondition(x => x.Id.Equals(request.SubdistrictId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();

                    string phoneNumber = request.PhoneNumber != null ? request.PhoneNumber : "";
                    if (request.PhoneCode == "+66" && request.PhoneNumber != null && request.PhoneNumber.Length > 9 && request.PhoneNumber.First() == '0')
                    {
                        phoneNumber = request.PhoneNumber.Remove(0, 1);
                    }

                    customer.CorporateType = corporateType;
                    customer.Title = title;
                    customer.FirstName = request.FirstName;
                    customer.MiddleName = request.MiddleName;
                    customer.LastName = request.LastName;
                    customer.Name = request.Name;
                    customer.IdentityType = request.IdentityType;
                    customer.IdentityNumber = request.IdentityNumber;
                    customer.ContactPersonTitle = contactPersonTitle;
                    customer.ContactPersonFirstName = request.ContactPersonFirstName;
                    customer.ContactPersonMiddleName = request.ContactPersonMiddleName;
                    customer.ContactPersonLastName = request.ContactPersonLastName;
                    customer.Birthday = request.Birthday;
                    customer.PhoneCode = request.PhoneCode;
                    customer.PhoneNumber = phoneNumber;
                    customer.Email = request.Email;
                    customer.Facbook = request.Facbook;
                    customer.Line = request.Line;
                    customer.Twitter = request.Twitter;
                    customer.Whatapp = request.Whatapp;
                    customer.Modified = DateTime.UtcNow;
                    customer.ModifiedBy = request.UserId != null ? request.UserId.Value.ToString() : "";
                    if (request.files != null)
                    {
                        var verifyStatus = customer.VerifyStatus;
                        foreach (IFormFile file in request.files)
                        {
                            string documentType = GetDocumentType(request, file.FileName);
                            if (documentType != "profilepicture")
                            {
                                verifyStatus = 0;
                            }
                        }
                        customer.VerifyStatus = verifyStatus;
                    }
                    await _customerRepository.UpdateAsync(customer);
                    #region register address
                    var customerAddress = (await _customerAddressRepository.FindByCondition(x => x.CustomerId == request.Id && x.AddressType == "register").ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                    if (customerAddress == null)
                    {
                        CustomerAddress address = new CustomerAddress
                        {
                            CustomerId = request.Id,
                            Country = country,
                            Province = province,
                            District = district,
                            Subdistrict = subdistrict,
                            PostCode = request.PostCode,
                            Road = request.Road,
                            Alley = request.Alley,
                            Address = request.Address,
                            Branch = "",
                            AddressType = "register",
                            AddressName = "",
                            ContactPerson = "",
                            ContactPhoneNumber = "",
                            ContactEmail = "",
                            IsMainData = true,
                            Maps = "",
                            Sequence = 1,
                            Created = DateTime.UtcNow,
                            Modified = DateTime.UtcNow,
                            CreatedBy = request.UserId != null ? request.UserId.Value.ToString() : "",
                            ModifiedBy = request.UserId != null ? request.UserId.Value.ToString() : "",
                        };
                        var customerAddressObject = await _customerAddressRepository.AddAsync(address);
                    }
                    else
                    {
                        customerAddress.CustomerId = request.Id;
                        customerAddress.Country = country;
                        customerAddress.Province = province;
                        customerAddress.District = district;
                        customerAddress.Subdistrict = subdistrict;
                        customerAddress.PostCode = request.PostCode;
                        customerAddress.Road = request.Road;
                        customerAddress.Alley = request.Alley;
                        customerAddress.Address = request.Address;
                        customerAddress.IsMainData = true;
                        customerAddress.Modified = DateTime.UtcNow;
                        customerAddress.ModifiedBy = request.UserId != null ? request.UserId.Value.ToString() : "";
                        await _customerAddressRepository.UpdateAsync(customerAddress);
                    }
                    #endregion
                    #region tax address
                    var customerTaxAddress = (await _customerAddressRepository.FindByCondition(x => x.CustomerId == request.Id && x.AddressType == "register-tax").ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                    if (customerTaxAddress == null)
                    {
                        CustomerAddress address = new CustomerAddress
                        {
                            CustomerId = request.Id,
                            Country = country,
                            Province = province,
                            District = district,
                            Subdistrict = subdistrict,
                            PostCode = request.PostCode,
                            Road = request.Road,
                            Alley = request.Alley,
                            Address = request.Address,
                            Branch = "",
                            AddressType = "register-tax",
                            AddressName = "",
                            ContactPerson = "",
                            ContactPhoneNumber = "",
                            ContactEmail = "",
                            IsMainData = true,
                            Maps = "",
                            Sequence = 1,
                            Created = DateTime.UtcNow,
                            Modified = DateTime.UtcNow,
                            CreatedBy = request.UserId != null ? request.UserId.Value.ToString() : "",
                            ModifiedBy = request.UserId != null ? request.UserId.Value.ToString() : "",
                        };
                        var customerAddressObject = await _customerAddressRepository.AddAsync(address);
                    }
                    else
                    {
                        customerTaxAddress.CustomerId = request.Id;
                        customerTaxAddress.Country = country;
                        customerTaxAddress.Province = province;
                        customerTaxAddress.District = district;
                        customerTaxAddress.Subdistrict = subdistrict;
                        customerTaxAddress.PostCode = request.PostCode;
                        customerTaxAddress.Road = request.Road;
                        customerTaxAddress.Alley = request.Alley;
                        customerTaxAddress.Address = request.Address;
                        customerTaxAddress.IsMainData = true;
                        customerTaxAddress.Modified = DateTime.UtcNow;
                        customerTaxAddress.ModifiedBy = request.UserId != null ? request.UserId.Value.ToString() : "";
                        await _customerAddressRepository.UpdateAsync(customerTaxAddress);
                    }
                    #endregion

                    string folderPath = contentPath + "customer/" + request.Id.ToString();
                    if (!Directory.Exists(folderPath))
                    {
                        Directory.CreateDirectory(folderPath);
                    }
                    if (request.DeleteFiles != null && request.DeleteFiles.Trim() != "")
                    {
                        string[] deleteFiles = request.DeleteFiles.Split(",");
                        foreach (string delFile in deleteFiles)
                        {
                            var customerFile = (await _customerFileRepository.FindByCondition(x => x.FileName.Trim().ToLower() == delFile.Trim().ToLower() && x.CustomerId.Equals(request.Id)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                            if (customerFile != null)
                            {
                                if (customerFile.IsApprove == false)
                                {
                                    if (File.Exists(Path.Combine(contentPath, customerFile.FilePath)))
                                        File.Delete(Path.Combine(contentPath, customerFile.FilePath));
                                    await _customerFileRepository.DeleteAsync(customerFile);
                                }
                                else
                                {
                                    customerFile.IsDelete = true;
                                    await _customerFileRepository.UpdateAsync(customerFile);
                                }
                                    //if (File.Exists(Path.Combine(contentPath, customerFile.FilePath)))
                                    //    File.Delete(Path.Combine(contentPath, customerFile.FilePath));
                                    //await _customerFileRepository.DeleteAsync(customerFile);
                                }
                            }
                    }
                    if (request.ProfilePicture != null && request.ProfilePicture.Trim() != "")
                    {
                        var customerProfilePictures = customer.Files.Where(x => x.DocumentType == "profilepicture").ToList();
                        foreach (CustomerFile customerProfilePicture in customerProfilePictures)
                        {
                            if (File.Exists(Path.Combine(contentPath, customerProfilePicture.FilePath)))
                                File.Delete(Path.Combine(contentPath, customerProfilePicture.FilePath));
                        }
                        (await _customerFileRepository.CreateSQLQuery("DELETE Customer_File where DocumentType = 'profilepicture' and CustomerId = '" + request.Id.ToString() + "'").ConfigureAwait(false)).ExecuteUpdate();
                    }
                    int fileCount = 0;
                    if (request.files != null)
                    {
                        foreach (IFormFile file in request.files)
                        {
                            fileCount = fileCount + 1;
                            string filePath = Path.Combine(folderPath, file.FileName);
                            using (FileStream fileStream = new FileStream(filePath, FileMode.Create))
                            {
                                await file.CopyToAsync(fileStream);
                                FileInfo fi = new FileInfo(filePath);
                                string documentType = GetDocumentType(request, file.FileName);
                                if (customer.CustomerType == "corporate" && documentType == "IdCard")
                                {
                                    documentType = "CompanyCertificate";
                                }
                                CustomerFile customerFile = new CustomerFile
                                {
                                    CustomerId = request.Id,
                                    FileName = file.FileName,
                                    ContentType = file.ContentType,
                                    FilePath = "customer/" + request.Id.ToString() + "/" + file.FileName,
                                    DirectoryPath = "customer/" + request.Id.ToString() + "/",
                                    FileEXT = fi.Extension,
                                    DocumentType = documentType,
                                    Sequence = fileCount,
                                    IsApprove = documentType == "profilepicture" ? true : false,
                                    Created = DateTime.UtcNow,
                                    CreatedBy = request.UserId != null ? request.UserId.Value.ToString() : "",
                                    Modified = DateTime.UtcNow,
                                    ModifiedBy = request.UserId != null ? request.UserId.Value.ToString() : "",
                                IsDelete = false
                                };
                                var customerFileObject = await _customerFileRepository.AddAsync(customerFile);
                            }
                        }
                    }
                    var log = new CreateAppLog(_applicationLogRepository);
                    log.CreateLog("Customer", "Customer", "Create", Newtonsoft.Json.JsonConvert.SerializeObject(request), request.UserId.ToString());
                    return new Response<Guid>(customer.Id);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GetDocumentType(UpdateCustomerInformationByIdCommand request, string fileName)
        {
            string result = "";
            if (request.idCard != null)
            {
                string[] idCard = request.idCard.Split(",");
                var existIdCard = idCard.Where(x => x == fileName).FirstOrDefault();
                if (existIdCard != null)
                {
                    result = "IdCard";
                    return result;
                }
            }
            if (request.companyCertificate != null)
            {
                string[] companyCertificate = request.companyCertificate.Split(",");
                var existCompanyCertificate = companyCertificate.Where(x => x == fileName).FirstOrDefault();
                if (existCompanyCertificate != null)
                {
                    result = "CompanyCertificate";
                    return result;
                }
            }
            if (request.drivingLicense != null)
            {
                string[] drivingLicense = request.drivingLicense.Split(",");
                var existDrivingLicensee = drivingLicense.Where(x => x == fileName).FirstOrDefault();
                if (existDrivingLicensee != null)
                {
                    result = "DrivingLicense";
                    return result;
                }
            }
            if (request.driverPicture != null)
            {
                string[] driverPicture = request.driverPicture.Split(",");
                var existDriverPicture = driverPicture.Where(x => x == fileName).FirstOrDefault();
                if (existDriverPicture != null)
                {
                    result = "DriverPicture";
                    return result;
                }
            }
            if (request.NP20 != null)
            {
                string[] NP20 = request.NP20.Split(",");
                var existNP20 = NP20.Where(x => x == fileName).FirstOrDefault();
                if (existNP20 != null)
                {
                    result = "NP20";
                    return result;
                }
            }
            if (request.ProfilePicture != null)
            {
                string[] ProfilePicture = request.ProfilePicture.Split(",");
                var existProfilePicture = ProfilePicture.Where(x => x == fileName).FirstOrDefault();
                if (existProfilePicture != null)
                {
                    result = "profilepicture";
                    return result;
                }
            }
            return result;
        }
    }
}
