﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;

namespace iTrans.Transportation.Application.Features.Customers.Commands
{
    public class UpdateCustomerStarByIdCommand : IRequest<Response<Guid>>
    {
        public Guid? UserId { get; set; }
        public Guid Id { get; set; }
        public Guid OrderingId { get; set; }
        public int Star { get; set; }

    }

    public class UpdateCustomerStarByIdCommandHandler : IRequestHandler<UpdateCustomerStarByIdCommand, Response<Guid>>
    {
        private readonly ICustomerRepositoryAsync _customerRepository;
        private readonly ICustomerActivityRepositoryAsync _customerActivityRepository;
        private readonly IOrderingRepositoryAsync _orderingRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;

        public UpdateCustomerStarByIdCommandHandler(ICustomerRepositoryAsync CustomerdRepository, IOrderingRepositoryAsync orderingRepository
            , IApplicationLogRepositoryAsync applicationLogRepository, ICustomerActivityRepositoryAsync customerActivityRepository, IMapper mapper
            )
        {
            _customerRepository = CustomerdRepository;
            _customerActivityRepository = customerActivityRepository;
            _applicationLogRepository = applicationLogRepository;
            _orderingRepository = orderingRepository;
            _mapper = mapper;
        }

        public async Task<Response<Guid>> Handle(UpdateCustomerStarByIdCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var customerStar = (await _customerRepository.FindByCondition(x => x.Id == request.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (customerStar == null)
                {
                    throw new ApiException($"Data Not Found.");
                }
                else
                {
                    var orderingCount = (await _orderingRepository.FindByCondition(x => x.Customer.Id.Equals(request.Id) && (x.Status == 15 || x.Status == 16)).ConfigureAwait(false)).AsQueryable().Count();
                    var ordering = (await _orderingRepository.FindByCondition(x => x.Id == request.OrderingId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                    if (ordering != null)
                    {
                        ordering.CustomerRanking = request.Star;
                        await _orderingRepository.UpdateAsync(ordering);
                    }
                    customerStar.Star = customerStar.Star + request.Star;
                    if (orderingCount > 0)
                    {
                        float rating = 0;
                        rating = customerStar.Star / orderingCount;

                        if (rating >= 5)
                        {
                            customerStar.Grade = "A";
                            customerStar.Rating = "5";
                        }
                        else if (rating > 4 && rating < 5)
                        {
                            customerStar.Grade = "B";
                            customerStar.Rating = "4";
                        }
                        else if (rating > 3 && rating < 4)
                        {
                            customerStar.Grade = "C";
                            customerStar.Rating = "3";
                        }
                        else if (rating > 2 && rating < 3)
                        {
                            customerStar.Grade = "D";
                            customerStar.Rating = "2";
                        }
                        else if (rating < 2)
                        {
                            customerStar.Grade = "E";
                            customerStar.Rating = "1";
                        }
                    }
                    await _customerRepository.UpdateAsync(customerStar);

                    var customerActivity = (await _customerActivityRepository.FindByCondition(x => x.CustomerId.Equals(request.Id) && x.Date == DateTime.Now.ToString("yyyy'-'MM'-'dd")).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                    if (customerActivity == null)
                    {
                        CustomerActivity newCustomerActivity = new CustomerActivity
                        {
                            CustomerId = request.Id,
                            Date = DateTime.Now.ToString("yyyy'-'MM'-'dd"),
                            Star = request.Star,
                            RequestCar = 0,
                            Cancel = 0,
                            OrderingValue = 0,
                            Created = DateTime.Now,
                            CreatedBy = request.UserId != null ? request.UserId.Value.ToString() : "-",
                            Modified = DateTime.Now,
                            ModifiedBy = request.UserId != null ? request.UserId.Value.ToString() : "-",
                        };
                        await _customerActivityRepository.AddAsync(newCustomerActivity);
                    }
                    else
                    {
                        customerActivity.Star = customerActivity.Star + request.Star;
                        await _customerActivityRepository.UpdateAsync(customerActivity);
                    }
                    var log = new CreateAppLog(_applicationLogRepository);
                    log.CreateLog("Customer", "Customer update star", "Edit", Newtonsoft.Json.JsonConvert.SerializeObject(request),request.UserId.ToString());
                    return new Response<Guid>(customerStar.Id);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}