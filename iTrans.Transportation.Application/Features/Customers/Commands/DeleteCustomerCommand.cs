﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;

namespace iTrans.Transportation.Application.Features.Customers.Commands
{
    public class DeleteCustomerCommand : IRequest<Response<bool>>
    {
        public Guid? Id { get; set; }
    }

    public class DeleteCustomerCommandHandler : IRequestHandler<DeleteCustomerCommand, Response<bool>>
    {
        private readonly ICustomerRepositoryAsync _customerRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;

        public DeleteCustomerCommandHandler(ICustomerRepositoryAsync customerRepository, IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _customerRepository = customerRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
        }

        public async Task<Response<bool>> Handle(DeleteCustomerCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var customer = (await _customerRepository.FindByCondition(x => x.Id == request.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (customer == null)
                {
                    throw new ApiException($"Customer Not Found.");
                }
                else
                {
                    customer.IsDelete = true;
                    
                    await _customerRepository.UpdateAsync(customer);
                    var log = new CreateAppLog(_applicationLogRepository);
                    log.CreateLog("Customer", "Customer", "Edit", Newtonsoft.Json.JsonConvert.SerializeObject(request),request.Id.ToString());
                    return new Response<bool>(true);
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
    }
}