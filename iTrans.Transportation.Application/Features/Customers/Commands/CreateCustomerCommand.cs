﻿using AutoMapper;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Interfaces.Services;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.Customers.Commands
{
    public partial class CreateCustomerCommand : IRequest<Response<Guid>>
    {
        public Guid? UserId{ get; set; }
        public string Password { get; set; }
        public string CustomerType { get; set; }
        public int RoleId { get; set; }
        public int CorporateTypeId { get; set; }
        public int Title { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Name { get; set; }
        public string IdentityNumber { get; set; }
        public int ContactPersonTitle { get; set; }
        public string ContactPersonFirstName { get; set; }
        public string ContactPersonMiddleName { get; set; }
        public string ContactPersonLastName { get; set; }
        public DateTime Birthday { get; set; }
        public string PhoneCode { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public  string Facbook { get; set; }
        public string Line { get; set; }
        public string Twitter { get; set; }
        public string Whatapp { get; set; }

    }
    public class CreateCustomerCommandHandler : IRequestHandler<CreateCustomerCommand, Response<Guid>>
    {
        private readonly ICustomerRepositoryAsync _customerRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;
        public CreateCustomerCommandHandler(ICustomerRepositoryAsync customerRepository, IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _customerRepository = customerRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
        }

        public async Task<Response<Guid>> Handle(CreateCustomerCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var customer = _mapper.Map<Customer>(request);
                customer.Created = DateTime.UtcNow;
                customer.Modified = DateTime.UtcNow;
                //customer.CreatedBy = "xxxxxxx";
                //customer.ModifiedBy = "xxxxxxx";
                var customerObject = await _customerRepository.AddAsync(customer);
                var log = new CreateAppLog(_applicationLogRepository);
                log.CreateLog("Customer", "Customer", "Create", Newtonsoft.Json.JsonConvert.SerializeObject(customer),request.UserId.ToString());
                return new Response<Guid>(customerObject.Id);
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
    }
}
