﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;

namespace iTrans.Transportation.Application.Features.Customers.Commands
{
    public class VerifyCustomerStatusCommand : IRequest<Response<Guid>>
    {
        public Guid Id { get; set; }
    }

    public class VerifyCustomerStatusCommandHandler : IRequestHandler<VerifyCustomerStatusCommand, Response<Guid>>
    {
        private readonly ICustomerRepositoryAsync _customerRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        public VerifyCustomerStatusCommandHandler(ICustomerRepositoryAsync customerRepository, IApplicationLogRepositoryAsync applicationLogRepository)
        {
            _customerRepository = customerRepository;
            _applicationLogRepository = applicationLogRepository;
        }

        public async Task<Response<Guid>> Handle(VerifyCustomerStatusCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var customer = (await _customerRepository.FindByCondition(x => x.Id == request.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (customer == null)
                {
                    throw new ApiException($"Customer Not Found.");
                }
                else
                {
                    customer.VerifyStatus = 0;
                    await _customerRepository.UpdateAsync(customer);
                    var log = new CreateAppLog(_applicationLogRepository);
                    log.Create("Customer", "Customer Verify Status", "Edit", Newtonsoft.Json.JsonConvert.SerializeObject(request));

                    return new Response<Guid>(customer.Id);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
