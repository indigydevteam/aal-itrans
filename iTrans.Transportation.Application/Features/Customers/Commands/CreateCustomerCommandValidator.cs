﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.Customers.Commands
{
    public class CreateCustomerCommandValidator : AbstractValidator<CreateCustomerCommand>
    {
        private readonly ICustomerRepositoryAsync customerRepository;

        public CreateCustomerCommandValidator(ICustomerRepositoryAsync customerRepository)
        {
            this.customerRepository = customerRepository;

            RuleFor(p => p.PhoneNumber)
                .NotEmpty().WithMessage("{PropertyName} is required.")
                .NotNull()
                .MaximumLength(50).WithMessage("{PropertyName} must not exceed 50 characters.");
                

            RuleFor(p => p.Name)
                .NotEmpty().WithMessage("{PropertyName} is required.")
                .NotNull()
                .MaximumLength(50).WithMessage("{PropertyName} must not exceed 50 characters.");

            RuleFor(p => p.IdentityNumber)
               .NotEmpty().WithMessage("{PropertyName} is required.")
               .NotNull()
               .MaximumLength(50).WithMessage("{PropertyName} must not exceed 50 characters.")
               .MustAsync(IsUniqueIdentityNumber).WithMessage("{PropertyName} already exists.");
        }

        private async Task<bool> IsUniqueIdentityNumber(string identityNumber, CancellationToken cancellationToken)
        {
            var customerObject = (await customerRepository.FindByCondition(x => x.IdentityNumber.ToLower() == identityNumber.ToLower() && x.IsDelete == false).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            if (customerObject != null)
            {
                return false;
            }
            return true;
        }
    }
}
