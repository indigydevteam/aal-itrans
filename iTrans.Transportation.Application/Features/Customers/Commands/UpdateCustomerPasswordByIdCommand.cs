﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;

namespace iTrans.Transportation.Application.Features.CustomerPassWords.Commands
{
    public class UpdateCustomerPasswordByIdCommand : IRequest<Response<Guid>>
    {
        public Guid? Id { get; set; }
        public string Password { get; set; }
       
    }

    public class UpdateCustomerPasswordByIdCommandHandler : IRequestHandler<UpdateCustomerPasswordByIdCommand, Response<Guid>>
    {
        private readonly ICustomerRepositoryAsync _customerRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;

        public UpdateCustomerPasswordByIdCommandHandler(ICustomerRepositoryAsync CustomerdRepository, IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _customerRepository = CustomerdRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
        }

        public async Task<Response<Guid>> Handle(UpdateCustomerPasswordByIdCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var CustomerPassWord = (await _customerRepository.FindByCondition(x => x.Id == request.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (CustomerPassWord == null)
                {
                    throw new ApiException($"CustomerPassWord Not Found.");
                }
                else
                {
                    CustomerPassWord.Password = request.Password;
                    await _customerRepository.UpdateAsync(CustomerPassWord);
                    var log = new CreateAppLog(_applicationLogRepository);
                    log.CreateLog("Customer", "Customer PassWord", "Edit", Newtonsoft.Json.JsonConvert.SerializeObject(request),request.Id.ToString());
                    return new Response<Guid>(CustomerPassWord.Id);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}