﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;

namespace iTrans.Transportation.Application.Features.Customers.Commands
{
    public class UpdateCustomerCommand : IRequest<Response<Guid>>
    {
        public Guid? UserId { get; set; }
        public Guid Id { get; set; }
        public string Password { get; set; }
        public string CustomerType { get; set; }
        public int RoleId { get; set; }
        public int CorporateTypeId { get; set; }
        public int Title { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Name { get; set; }
        public string IdentityNumber { get; set; }
        public int ContactPersonTitle { get; set; }
        public string ContactPersonFirstName { get; set; }
        public string ContactPersonMiddleName { get; set; }
        public string ContactPersonLastName { get; set; }
        public DateTime Birthday { get; set; }
        public string PhoneCode { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public int Level { get; set; }
        public string Facbook { get; set; }
        public string Line { get; set; }
        public string Twitter { get; set; }
        public string Whatapp { get; set; }
    }

    public class UpdateCustomerCommandHandler : IRequestHandler<UpdateCustomerCommand, Response<Guid>>
    {
        private readonly ICustomerRepositoryAsync _customerRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;

        public UpdateCustomerCommandHandler(ICustomerRepositoryAsync customerRepository, IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _customerRepository = customerRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
        }

        public async Task<Response<Guid>> Handle(UpdateCustomerCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var customer = (await _customerRepository.FindByCondition(x => x.Id == request.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (customer == null)
                {
                    throw new ApiException($"Customer Not Found.");
                }
                else
                {
                    customer.Password = request.Password;
                    customer.CustomerType = request.CustomerType;
                    //customer.RoleId = request.RoleId;
                    //customer.CorporateTypeId = request.CorporateTypeId;
                   //customer.Title = request.Title;
                    customer.FirstName = request.FirstName;
                    customer.MiddleName = request.MiddleName;
                    customer.LastName = request.LastName;
                    customer.Name = request.Name;
                    customer.IdentityNumber = request.IdentityNumber;
                    //customer.ContactPersonTitle = request.ContactPersonTitle;
                    customer.ContactPersonFirstName = request.ContactPersonFirstName;
                    customer.ContactPersonMiddleName = request.ContactPersonMiddleName;
                    customer.ContactPersonLastName = request.ContactPersonLastName;
                    customer.Birthday = request.Birthday;
                    customer.PhoneCode = request.PhoneCode;
                    customer.PhoneNumber = request.PhoneNumber;
                    customer.Email = request.Email;
                    await _customerRepository.UpdateAsync(customer);
                    var log = new CreateAppLog(_applicationLogRepository);
                    log.CreateLog("Customer", "Customer", "Edit", Newtonsoft.Json.JsonConvert.SerializeObject(customer),request.UserId.ToString());
                    return new Response<Guid>(customer.Id);
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
    }
}