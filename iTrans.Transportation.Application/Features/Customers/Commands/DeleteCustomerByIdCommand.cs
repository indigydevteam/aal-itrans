﻿using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.Customers.Commands
{
    public class DeleteCustomerByIdCommand : IRequest<Response<Guid>>
    {
        public Guid Id { get; set; }
        public class DeleteCustomerByIdCommandHandler : IRequestHandler<DeleteCustomerByIdCommand, Response<Guid>>
        {
            private readonly ICustomerRepositoryAsync _customerRepository;
            public DeleteCustomerByIdCommandHandler(ICustomerRepositoryAsync customerRepository)
            {
                _customerRepository = customerRepository;
            }
            public async Task<Response<Guid>> Handle(DeleteCustomerByIdCommand command, CancellationToken cancellationToken)
            {
                var customer = (await _customerRepository.FindByCondition(x => x.Id == command.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (customer == null)
                {
                    throw new ApiException($"Customer Not Found.");
                }
                else
                {
                    await _customerRepository.DeleteAsync(customer);
                    return new Response<Guid>(customer.Id);
                }
            }
        }
    }
}
