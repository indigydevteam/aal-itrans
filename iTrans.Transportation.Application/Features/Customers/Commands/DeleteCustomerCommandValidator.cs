﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.Customers.Commands
{
    public class DeleteCustomerCommandValidator : AbstractValidator<DeleteCustomerCommand>
    {
        private readonly ICustomerRepositoryAsync customerRepository;
        private readonly IOrderingRepositoryAsync orderingRepository;

        public DeleteCustomerCommandValidator(ICustomerRepositoryAsync customerRepository, IOrderingRepositoryAsync orderingRepository)
        {
            this.customerRepository = customerRepository;
            this.orderingRepository = orderingRepository;
            RuleFor(p => p.Id)
               .NotEmpty().WithMessage("{PropertyName} is required.")
               .NotNull()
               .MustAsync(IsExistUser).WithMessage("{PropertyName} is not exists.")
               .MustAsync(IsOrderingInProcess).WithMessage("Your have ordering in process.");
        }

        private async Task<bool> IsExistUser(Guid? userId, CancellationToken cancellationToken)
        {
            if (userId == null) return false;
            var customerObject = (await customerRepository.FindByCondition(x => x.Id == userId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            if (customerObject == null)
            {
                return false;
            }
            return true;
        }

        private async Task<bool> IsOrderingInProcess(Guid? userId, CancellationToken cancellationToken)
        {
            int corderingCount = orderingRepository.GetItemCount(x => x.Customer.Id == userId && x.Status != 16 && x.Status !=15);
            if (corderingCount == 0)
            {
                return true;
            }
            return false;
        }
    }
}
