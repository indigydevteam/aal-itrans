﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;

namespace iTrans.Transportation.Application.Features.Customers.Commands
{
    public class UpdateCustomerLevelCommand : IRequest<Response<Guid>>
    {
        public Guid? UserId { get; set; }
        public Guid Id { get; set; }
        public string DocumentType { get; set; }
        public List<IFormFile> Files { get; set; }

    }

    public class UpdateCustomerLevelCommandHandler : IRequestHandler<UpdateCustomerLevelCommand, Response<Guid>>
    {
        private readonly ICustomerRepositoryAsync _customerRepository;
        private readonly ICustomerFileRepositoryAsync _customerFileRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;
        private readonly IConfiguration _configuration;
        public UpdateCustomerLevelCommandHandler(ICustomerRepositoryAsync customerRepository, ICustomerFileRepositoryAsync customerFileRepository,IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper, IConfiguration configuration)
        {
            _customerRepository = customerRepository;
            _customerFileRepository = customerFileRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
            _configuration = configuration;
        }

        public async Task<Response<Guid>> Handle(UpdateCustomerLevelCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var customer = (await _customerRepository.FindByCondition(x => x.Id == request.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (customer == null)
                {
                    throw new ApiException($"Data Not Found.");
                }
                else
                {
                    string currentTimeStr = DateTime.Now.ToString("yyyyMMddHHmmss");
                    if (request.Files != null && request.Files.Count > 0)
                    {
                        var getContentPath = _configuration.GetSection("ContentPath");
                        var contentPath = getContentPath.Value;

                        string folderPath = contentPath + "customer/" + customer.Id.ToString() +"/" + currentTimeStr;
                        if (!Directory.Exists(folderPath))
                        {
                            Directory.CreateDirectory(folderPath);
                        }
                        int fileCount = 0;

                        foreach (IFormFile file in request.Files)
                        {
                            fileCount = fileCount + 1;
                            string filePath = Path.Combine(folderPath, file.FileName);
                            using (FileStream fileStream = new FileStream(filePath, FileMode.Create))
                            {
                                await file.CopyToAsync(fileStream);
                                FileInfo fi = new FileInfo(filePath);

                                CustomerFile customerFile = new CustomerFile
                                {
                                    CustomerId = customer.Id,
                                    FileName = file.FileName,
                                    ContentType = file.ContentType,
                                    FilePath = "customer/" + customer.Id.ToString() + "/" + currentTimeStr + "/" + file.FileName,
                                    DirectoryPath = "customer/" + customer.Id.ToString() + "/" + currentTimeStr + "/",
                                    FileEXT = fi.Extension,
                                    DocumentType = request.DocumentType,
                                    Sequence = fileCount,
                                };
                                var customerFileObject = await _customerFileRepository.AddAsync(customerFile);
                            }
                        }

                        await _customerRepository.UpdateAsync(customer);
                        var log = new CreateAppLog(_applicationLogRepository);
                        log.CreateLog("Customer", "Customer Level", "Edit", Newtonsoft.Json.JsonConvert.SerializeObject(request),request.UserId.ToString());
                    }
                    return new Response<Guid>(customer.Id);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
