﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.Customers.Commands
{
    public class UpdateCustomerCommandValidator : AbstractValidator<UpdateCustomerCommand>
    {
        private readonly ICustomerRepositoryAsync customerRepository;
        public UpdateCustomerCommandValidator(ICustomerRepositoryAsync customerRepository)
        {
            this.customerRepository = customerRepository;

            RuleFor(p => p.IdentityNumber)
                .NotEmpty().WithMessage("{PropertyName} is required.")
                .NotNull()
                .MaximumLength(50).WithMessage("{PropertyName} must not exceed 50 characters.");

            RuleFor(p => p.PhoneNumber)
                .NotEmpty().WithMessage("{PropertyName} is required.")
                .NotNull()
                .MaximumLength(50).WithMessage("{PropertyName} must not exceed 50 characters.");

            RuleFor(p => p.IdentityNumber)
                .MustAsync(async (p, s, cancellation) =>
                {
                    return await IsUniqueIdentityNumber(p.Id,p.IdentityNumber).ConfigureAwait(false);
                }).WithMessage("{PropertyName} already exists.");
        }

        private async Task<bool> IsUniqueIdentityNumber(Guid id,string IdentityNumber)
        {
            var customerObject = (await customerRepository.FindByCondition(x => x.IdentityNumber.ToLower() == IdentityNumber.ToLower() && x.Id != id && x.IsDelete == false).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            if (customerObject != null)
            {
                return false;
            }
            return true;
        }

        
    }
}
