﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.CustomerLevelCharacteristic;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.CustomerLevelCharacteristics.Queries
{
    public class GetCustomerLevelCharacteristicByIdQuery : IRequest<Response<CustomerLevelCharacteristicViewModel>>
    {
        public int Id { get; set; }

    }
    public class GetCustomerLevelCharacteristicQueryHandler : IRequestHandler<GetCustomerLevelCharacteristicByIdQuery, Response<CustomerLevelCharacteristicViewModel>>
    {
        private readonly ICustomerLevelCharacteristicRepositoryAsync _customerLevelCharacteristicRepository;
        private readonly IMapper _mapper;
        public GetCustomerLevelCharacteristicQueryHandler(ICustomerLevelCharacteristicRepositoryAsync customerLevelCharacteristicRepository, IMapper mapper)
        {
            _customerLevelCharacteristicRepository = customerLevelCharacteristicRepository;
            _mapper = mapper;
        }
        public async Task<Response<CustomerLevelCharacteristicViewModel>> Handle(GetCustomerLevelCharacteristicByIdQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var customerObject = (await _customerLevelCharacteristicRepository.FindByCondition(x => x.Id.Equals(request.Id)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                return new Response<CustomerLevelCharacteristicViewModel>(_mapper.Map<CustomerLevelCharacteristicViewModel>(customerObject));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
