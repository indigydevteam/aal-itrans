﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.Backoffice.CustomerLevelCharacteristic;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using LinqKit;
using MediatR;

namespace iTrans.Transportation.Application.Features.CustomerLevelCharacteristics.Queries
{
    public class GetAllCustomerLevelCharacteristicQuery : IRequest<PagedResponse<IEnumerable<CustomerLevelCharacteristicViewModel>>>
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public string Search { get; set; }
        public int Active { set; get; }
        public string Column { set; get; }
    }
    public class GetAllCustomerLevelCharacteristicQueryHandler : IRequestHandler<GetAllCustomerLevelCharacteristicQuery, PagedResponse<IEnumerable<CustomerLevelCharacteristicViewModel>>>
    {
        private readonly ICustomerLevelCharacteristicRepositoryAsync _customerLevelCharacteristicRepository;
        private readonly IMapper _mapper;
        public GetAllCustomerLevelCharacteristicQueryHandler(ICustomerLevelCharacteristicRepositoryAsync customerLevelCharacteristicRepository, IMapper mapper)
        {
            _customerLevelCharacteristicRepository = customerLevelCharacteristicRepository;
            _mapper = mapper;
        }

        public async Task<PagedResponse<IEnumerable<CustomerLevelCharacteristicViewModel>>> Handle(GetAllCustomerLevelCharacteristicQuery request, CancellationToken cancellationToken)
        {
            var validFilter = _mapper.Map<GetAllCustomerLevelCharacteristicQuery>(request);

            var result = from x in _customerLevelCharacteristicRepository.GetAllAsync().Result.ToList().OrderByDescending(x => x.Modified)
                         select new CustomerLevelCharacteristicViewModel
                         {
                             id = x.Id,
                             level = x.Level,
                             star = x.Star,
                             requestCarPerWeek = x.RequestCarPerWeek,
                             requestCarPerMonth = x.RequestCarPerMonth,
                             requestCarPerYear = x.RequestCarPerYear,
                             cancelPerWeek = x.CancelPerWeek,
                             cancelPerMonth = x.CancelPerMonth,
                             cancelPerYear = x.CancelPerYear,
                             orderingValuePerWeek = x.OrderingValuePerWeek,
                             orderingValuePerMonth = x.OrderingValuePerMonth,
                             orderingValuePerYear = x.OrderingValuePerYear,
                             discount = x.Discount,
                             fine = x.Fine,
                             created = x.Created,
                             modified = x.Modified
                         };

            if (validFilter.Search != null && validFilter.Search.Trim() != "")
            {
                result = result.Where(x => x.level.Equals(validFilter.Search.Trim()) || x.star.Equals(validFilter.Search.Trim())
                         || x.requestCarPerWeek.Equals(validFilter.Search.Trim()) || x.requestCarPerMonth.Equals(validFilter.Search.Trim()) || x.requestCarPerYear.Equals(validFilter.Search.Trim())
                         || x.cancelPerWeek.Equals(validFilter.Search.Trim()) || x.cancelPerWeek.Equals(validFilter.Search.Trim()) || x.cancelPerWeek.Equals(validFilter.Search.Trim())
                         || x.orderingValuePerWeek.Equals(validFilter.Search.Trim()) || x.orderingValuePerMonth.Equals(validFilter.Search.Trim()) || x.orderingValuePerYear.Equals(validFilter.Search.Trim())).ToList();
            }
            if (request.Column == "level" && request.Active == 3)
            {
                result = result.OrderBy(x => x.level);
            }
            if (request.Column == "level" && request.Active == 2)
            {
                result = result.OrderByDescending(x => x.level);
            }
            if (request.Column == "star" && request.Active == 3)
            {
                result = result.OrderBy(x => x.star);
            }
            if (request.Column == "star" && request.Active == 2)
            {
                result = result.OrderByDescending(x => x.star);
            }
            if (request.Column == "requestCarPerWeek" && request.Active == 3)
            {
                result = result.OrderBy(x => x.requestCarPerWeek);
            }
            if (request.Column == "requestCarPerWeek" && request.Active == 2)
            {
                result = result.OrderByDescending(x => x.requestCarPerWeek);
            }
            if (request.Column == "requestCarPerMonth" && request.Active == 3)
            {
                result = result.OrderBy(x => x.requestCarPerMonth);
            }
            if (request.Column == "requestCarPerMonth" && request.Active == 2)
            {
                result = result.OrderByDescending(x => x.requestCarPerMonth);
            }
            if (request.Column == "requestCarPerYear" && request.Active == 3)
            {
                result = result.OrderBy(x => x.requestCarPerYear);
            }
            if (request.Column == "requestCarPerYear" && request.Active == 2)
            {
                result = result.OrderByDescending(x => x.requestCarPerYear);
            }

            if (request.Column == "cancelPerWeek" && request.Active == 3)
            {
                result = result.OrderBy(x => x.cancelPerWeek);
            }
            if (request.Column == "cancelPerWeek" && request.Active == 2)
            {
                result = result.OrderByDescending(x => x.cancelPerWeek);
            }
            if (request.Column == "cancelPerMonth" && request.Active == 3)
            {
                result = result.OrderBy(x => x.cancelPerMonth);
            }
            if (request.Column == "cancelPerMonth" && request.Active == 2)
            {
                result = result.OrderByDescending(x => x.cancelPerMonth);
            }
            if (request.Column == "cancelPerYear" && request.Active == 3)
            {
                result = result.OrderBy(x => x.cancelPerYear);
            }
            if (request.Column == "cancelPerYear" && request.Active == 2)
            {
                result = result.OrderByDescending(x => x.cancelPerYear);
            }

            if (request.Column == "orderingValuePerWeek" && request.Active == 3)
            {
                result = result.OrderBy(x => x.orderingValuePerWeek);
            }
            if (request.Column == "orderingValuePerWeek" && request.Active == 2)
            {
                result = result.OrderByDescending(x => x.orderingValuePerWeek);
            }
            if (request.Column == "orderingValuePerMonth" && request.Active == 3)
            {
                result = result.OrderBy(x => x.orderingValuePerMonth);
            }
            if (request.Column == "orderingValuePerMonth" && request.Active == 2)
            {
                result = result.OrderByDescending(x => x.orderingValuePerMonth);
            }
            if (request.Column == "orderingValuePerYear" && request.Active == 3)
            {
                result = result.OrderBy(x => x.orderingValuePerYear);
            }
            if (request.Column == "orderingValuePerYear" && request.Active == 2)
            {
                result = result.OrderByDescending(x => x.orderingValuePerYear);
            }

            var itemCount = result.Count();
            result = result.Skip((validFilter.PageNumber - 1) * validFilter.PageSize).Take(validFilter.PageSize);
            return new PagedResponse<IEnumerable<CustomerLevelCharacteristicViewModel>>(result, request.PageNumber, request.PageSize);
        }
    }
}
