﻿using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Interfaces.Services;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using iTrans.Transportation.Domain.Entities;
using MediatR;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.CustomerLevelCharacteristics.Commands
{
    public partial class CreateCustomerLevelCharacteristicCommand : IRequest<Response<int>>
    {
        public Guid? UserId { get; set; }
        public int Level { get; set; }
        public int RequestCar { get; set; }
        public int Star { get; set; }
        public int CancelAmount { get; set; }
        public decimal OrderValue { get; set; }

    }
    public class CreateCustomerLevelCharacteristicCommandHandler : IRequestHandler<CreateCustomerLevelCharacteristicCommand, Response<int>>
    {
        private readonly ICustomerLevelCharacteristicRepositoryAsync _customerLevelCharacteristicRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;
        public CreateCustomerLevelCharacteristicCommandHandler(ICustomerLevelCharacteristicRepositoryAsync customerLevelCharacteristicRepository, IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _customerLevelCharacteristicRepository = customerLevelCharacteristicRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(CreateCustomerLevelCharacteristicCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var customerLevelCharacteristic = _mapper.Map<CustomerLevelCharacteristic>(request);
                customerLevelCharacteristic.Created = DateTime.UtcNow;
                customerLevelCharacteristic.Modified = DateTime.UtcNow;
                var customerLevelCharacteristicObject = await _customerLevelCharacteristicRepository.AddAsync(customerLevelCharacteristic);
                var log = new CreateAppLog(_applicationLogRepository);
                log.CreateLog("CustomerLevelCharacteristic", "Customer LevelCharacteristic", "Create", Newtonsoft.Json.JsonConvert.SerializeObject(request),request.UserId.ToString());
                return new Response<int>(customerLevelCharacteristicObject.Id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
