﻿using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
namespace iTrans.Transportation.Application.Features.CustomerLevelCharacteristics.Commands
{
    public class DeleteCustomerLevelCharacteristicCommand : IRequest<Response<int>>
    {
        public int Id { get; set; }
        public class DeleteCustomerLevelCharacteristicCommandHandler : IRequestHandler<DeleteCustomerLevelCharacteristicCommand, Response<int>>
        {
            private readonly ICustomerLevelCharacteristicRepositoryAsync _customerLevelCharacteristicRepository;
            public DeleteCustomerLevelCharacteristicCommandHandler(ICustomerLevelCharacteristicRepositoryAsync customerLevelCharacteristicRepository)
            {
                _customerLevelCharacteristicRepository = customerLevelCharacteristicRepository;
            }
            public async Task<Response<int>> Handle(DeleteCustomerLevelCharacteristicCommand command, CancellationToken cancellationToken)
            {
                var customerLevelCharacteristic = (await _customerLevelCharacteristicRepository.FindByCondition(x => x.Id == command.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (customerLevelCharacteristic == null)
                {
                    throw new ApiException($"Customer LevelCharacteristic Not Found.");
                }
                else
                {
                    await _customerLevelCharacteristicRepository.DeleteAsync(customerLevelCharacteristic);
                    return new Response<int>(customerLevelCharacteristic.Id);
                }
            }
        }
    }
}
