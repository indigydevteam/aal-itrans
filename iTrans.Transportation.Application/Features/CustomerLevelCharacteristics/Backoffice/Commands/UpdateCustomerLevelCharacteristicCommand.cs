﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;

namespace iTrans.Transportation.Application.Features.CustomerLevelCharacteristics.Commands
{
    public class UpdateCustomerLevelCharacteristicCommand : IRequest<Response<int>>
    {
        public Guid? UserId { get; set; }
        public int Id { get; set; }
        public int Level { get; set; }
        public int Star { get; set; }
        public int RequestCarPerWeek { get; set; }

        public int RequestCarPerMonth { get; set; }
        public int RequestCarPerYear { get; set; }
        public int CancelPerWeek { get; set; }
        public int CancelPerMonth { get; set; }
        public int CancelPerYear { get; set; }
        public decimal OrderingValuePerWeek { get; set; }
        public decimal OrderingValuePerMonth { get; set; }
        public decimal OrderingValuePerYear { get; set; }
        public decimal Discount { get; set; }
        public decimal Fine { get; set; }
    }
    public class UpdateCustomerLevelCharacteristicCommandHandler : IRequestHandler<UpdateCustomerLevelCharacteristicCommand, Response<int>>
    {
        private readonly ICustomerLevelCharacteristicRepositoryAsync _customerLevelCharacteristicRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;

        public UpdateCustomerLevelCharacteristicCommandHandler(ICustomerLevelCharacteristicRepositoryAsync customerLevelCharacteristicRepository, IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _customerLevelCharacteristicRepository = customerLevelCharacteristicRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(UpdateCustomerLevelCharacteristicCommand request, CancellationToken cancellationToken)
        {
            var customerLevelCharacteristic = (await _customerLevelCharacteristicRepository.FindByCondition(x => x.Id == request.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            if (customerLevelCharacteristic == null)
            {
                throw new ApiException($"Customer LevelCharacteristic Not Found.");
            }
            else
            {
                customerLevelCharacteristic.Level = request.Level;
                customerLevelCharacteristic.Star = request.Star;
                customerLevelCharacteristic.RequestCarPerWeek = request.RequestCarPerWeek;
                customerLevelCharacteristic.RequestCarPerMonth = request.RequestCarPerMonth;
                customerLevelCharacteristic.RequestCarPerYear = request.RequestCarPerYear;
                customerLevelCharacteristic.CancelPerWeek = request.CancelPerWeek;
                customerLevelCharacteristic.CancelPerMonth = request.CancelPerMonth;
                customerLevelCharacteristic.CancelPerYear = request.CancelPerYear;
                customerLevelCharacteristic.OrderingValuePerWeek = request.OrderingValuePerWeek;
                customerLevelCharacteristic.OrderingValuePerMonth = request.OrderingValuePerMonth;
                customerLevelCharacteristic.OrderingValuePerYear = request.OrderingValuePerYear;
                customerLevelCharacteristic.Discount = request.Discount;
                customerLevelCharacteristic.Fine = request.Fine;
                customerLevelCharacteristic.Modified = DateTime.UtcNow;
                await _customerLevelCharacteristicRepository.UpdateAsync(customerLevelCharacteristic);
                var log = new CreateAppLog(_applicationLogRepository);
                log.CreateLog("CustomerLevelCharacteristic", "Customer LevelCharacteristic", "Edit", Newtonsoft.Json.JsonConvert.SerializeObject(request), request.UserId.ToString());
                return new Response<int>(customerLevelCharacteristic.Id);
            }
        }
    }
}
