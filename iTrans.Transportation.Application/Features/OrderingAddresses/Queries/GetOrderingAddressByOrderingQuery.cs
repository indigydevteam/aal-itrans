﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.Ordering;
using iTrans.Transportation.Application.DTOs.OrderingAddress;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.OrderingAddresses.Queries
{
   public class GetOrderingAddressByOrderingQuery : IRequest<Response<IEnumerable<OrderingAddressViewModel>>>
    {
        public int OrderingId { set; get; }
    }
    public class GetOrderingAddressByOrderingQueryHandler : IRequestHandler<GetOrderingAddressByOrderingQuery, Response<IEnumerable<OrderingAddressViewModel>>>
    {
        private readonly IOrderingAddressRepositoryAsync _orderingAddressRepository;
        private readonly IMapper _mapper;
        public GetOrderingAddressByOrderingQueryHandler(IOrderingAddressRepositoryAsync orderingAddressRepository, IMapper mapper)
        {
            _orderingAddressRepository = orderingAddressRepository;
            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<OrderingAddressViewModel>>> Handle(GetOrderingAddressByOrderingQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var OrderingAddress = (await _orderingAddressRepository.FindByCondition(x => x.Ordering.Id.Equals(request.OrderingId)).ConfigureAwait(false)).AsQueryable().ToList();
                var OrderingAddressViewModel = _mapper.Map<IEnumerable<OrderingAddressViewModel>>(OrderingAddress);
                return new Response<IEnumerable<OrderingAddressViewModel>>(OrderingAddressViewModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
