﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.OrderingAddress;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
namespace iTrans.Transportation.Application.Features.OrderingAddresses.Queries
{
    public class GetAllOrderingAddressQuery : IRequest<Response<IEnumerable<OrderingAddressViewModel>>>
    {

    }
    public class GetAllOrderingAddressQueryHandler : IRequestHandler<GetAllOrderingAddressQuery, Response<IEnumerable<OrderingAddressViewModel>>>
    {
        private readonly IOrderingAddressRepositoryAsync _orderingAddressRepository;
        private readonly IMapper _mapper;
        public GetAllOrderingAddressQueryHandler(IOrderingAddressRepositoryAsync OrderingAddressRepository, IMapper mapper)
        {
            _orderingAddressRepository = OrderingAddressRepository;
            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<OrderingAddressViewModel>>> Handle(GetAllOrderingAddressQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var OrderingAddress = await _orderingAddressRepository.GetAllAsync();
                var OrderingAddressViewModel = _mapper.Map<IEnumerable<OrderingAddressViewModel>>(OrderingAddress);
                return new Response<IEnumerable<OrderingAddressViewModel>>(OrderingAddressViewModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
