﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.OrderingAddress;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;

namespace iTrans.Transportation.Application.Features.OrderingAddresss.Queries
{
    public class GetAllOrderingAddressParameter : IRequest<PagedResponse<IEnumerable<OrderingAddressViewModel>>>
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
    }
    public class GetAllOrderingAddressParameterHandler : IRequestHandler<GetAllOrderingAddressParameter, PagedResponse<IEnumerable<OrderingAddressViewModel>>>
    {
        private readonly IOrderingAddressRepositoryAsync _OrderingAddressRepository;
        private readonly IMapper _mapper;
        public GetAllOrderingAddressParameterHandler(IOrderingAddressRepositoryAsync OrderingAddressRepository, IMapper mapper)
        {
            _OrderingAddressRepository = OrderingAddressRepository;
            _mapper = mapper;
        }

        public async Task<PagedResponse<IEnumerable<OrderingAddressViewModel>>> Handle(GetAllOrderingAddressParameter request, CancellationToken cancellationToken)
        {
            var validFilter = _mapper.Map<GetAllOrderingAddressParameter>(request);
            Expression<Func<OrderingAddress, bool>> expression = x => x.PostCode != "";
            var OrderingAddress = await _OrderingAddressRepository.FindByConditionWithPage(expression, validFilter.PageNumber, validFilter.PageSize);
            var OrderingAddressViewModel = _mapper.Map<IEnumerable<OrderingAddressViewModel>>(OrderingAddress);
            return new PagedResponse<IEnumerable<OrderingAddressViewModel>>(OrderingAddressViewModel, validFilter.PageNumber, validFilter.PageSize);
        }
    }
}
