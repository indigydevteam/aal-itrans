﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.OrderingAddress;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;


namespace iTrans.Transportation.Application.Features.OrderingAddressAddresses.Queries
{
   public class GetOrderingAddressByIdQuery : IRequest<Response<OrderingAddressViewModel>>
    {
        public int Id { get; set; }
    }
    public class GetOrderingAddressByIdQueryHandler : IRequestHandler<GetOrderingAddressByIdQuery, Response<OrderingAddressViewModel>>
    {
        private readonly IOrderingAddressRepositoryAsync _orderingAddressRepository;
        private readonly IMapper _mapper;
        public GetOrderingAddressByIdQueryHandler(IOrderingAddressRepositoryAsync orderingAddressRepository, IMapper mapper)
        {
            _orderingAddressRepository = orderingAddressRepository;
            _mapper = mapper;
        }
        public async Task<Response<OrderingAddressViewModel>> Handle(GetOrderingAddressByIdQuery request, CancellationToken cancellationToken)
        {
            var DataObject = (await _orderingAddressRepository.FindByCondition(x => x.Id.Equals(request.Id)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            return new Response<OrderingAddressViewModel>(_mapper.Map<OrderingAddressViewModel>(DataObject));
        }
    }
}
