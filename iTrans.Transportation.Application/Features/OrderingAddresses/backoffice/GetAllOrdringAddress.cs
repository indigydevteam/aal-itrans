﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.OrderingAddress;
using iTrans.Transportation.Application.DTOs.OrderingStatus;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;

namespace iTrans.Transportation.Application.Features.OrderingAddresses.backoffice
{
    public class GetAllOrderingAddress : IRequest<PagedResponse<IEnumerable<OrderingAddressViewModel>>>
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public Guid orderingId { get; set; }
        public string Search { set; get; }

    }
    public class GetAllOrderingAddressHandler : IRequestHandler<GetAllOrderingAddress, PagedResponse<IEnumerable<OrderingAddressViewModel>>>
    {
        private readonly IOrderingAddressRepositoryAsync _OrderingAddressRepository;
        private readonly IMapper _mapper;
        private Expression<Func<OrderingAddress, bool>> expression;

        public GetAllOrderingAddressHandler(IOrderingAddressRepositoryAsync OrderingAddressRepository, IMapper mapper)
        {
            _OrderingAddressRepository = OrderingAddressRepository;
            _mapper = mapper;
        }

        public async Task<PagedResponse<IEnumerable<OrderingAddressViewModel>>> Handle(GetAllOrderingAddress request, CancellationToken cancellationToken)
        {
            var orderingStatusPathFile = Path.Combine(Directory.GetCurrentDirectory(), $"Shared\\OrderingStatus.json");
            var statusJson = System.IO.File.ReadAllText(orderingStatusPathFile);
            List<OrderingStatusViewModel> orderingStatus = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OrderingStatusViewModel>>(statusJson);


            var validFilter = _mapper.Map<GetAllOrderingAddress>(request);
            if (validFilter.Search != null && validFilter.Search.Trim() != "" )
            {
                expression = x => (x.TrackingCode.Contains(validFilter.Search.Trim()));
            }
            int itemCount = _OrderingAddressRepository.GetItemCount(expression);
            var OrderingAddress = await _OrderingAddressRepository.FindByConditionWithPage(expression, validFilter.PageNumber, validFilter.PageSize);


            var result = _mapper.Map<IEnumerable<OrderingAddressViewModel>>(OrderingAddress).Select(p => new OrderingAddressViewModel
            {
                id = p.id,
                trackingCode = p.trackingCode,
                orderingId = p.orderingId,
                personalName = p.personalName,
                phoneCode = p.phoneCode,
                phoneNumber = p.phoneNumber,
                email = p.email,
                country = p.country,
                province = p.province,
                district = p.district,
                subdistrict = p.subdistrict,
                status = p.status,
                statusObj = orderingStatus.Where(o => o.id == p.status).FirstOrDefault(),
                statusReason = p.statusReason,
                postCode = p.postCode,
                road = p.road,
                alley = p.alley,
                address = p.address,
                branch = p.branch,
                addressType = p.addressType,
                addressName = p.addressName,
                date = p.date,
                maps = p.maps,
                sequence = p.sequence,
                note = p.note,
                driverWaitingToPickUpDate = p.driverWaitingToPickUpDate,
                driverWaitingToDeliveryDate = p.driverWaitingToDeliveryDate,
                uploadNote = p.uploadNote,
                products = p.products,
                files = p.files,
    
            });


            return new PagedResponse<IEnumerable<OrderingAddressViewModel>>(result, validFilter.PageNumber, validFilter.PageSize, itemCount);
        }
    }
}
