﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.OrderingAddress.BackOffice;
using iTrans.Transportation.Application.DTOs.OrderingAddressFile;
using iTrans.Transportation.Application.DTOs.OrderingStatus;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;

namespace iTrans.Transportation.Application.Features.OrderingAddresses.backoffice
{
    public class GetAllOrderingAddressByOrderingId : IRequest<PagedResponse<IEnumerable<OrderingAddressViewModel>>>
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public Guid  OrderingId { get; set; }
        public string Search { set; get; }

    }
    public class GetAllOrderingAddressByOrderingIdHandler : IRequestHandler<GetAllOrderingAddressByOrderingId, PagedResponse<IEnumerable<OrderingAddressViewModel>>>
    {
        private readonly IOrderingAddressRepositoryAsync _orderingAddressRepository;
        private readonly IOrderingAddressFileRepositoryAsync _orderingAddressFileRepository;
        private readonly IMapper _mapper;
        private Expression<Func<OrderingAddress, bool>> expression;

        public GetAllOrderingAddressByOrderingIdHandler(IOrderingAddressRepositoryAsync orderingAddressRepository, IOrderingAddressFileRepositoryAsync orderingAddressFileRepository, IMapper mapper)
        {
            _orderingAddressRepository = orderingAddressRepository;
            _orderingAddressFileRepository = orderingAddressFileRepository;
            _mapper = mapper;
        }

        public async Task<PagedResponse<IEnumerable<OrderingAddressViewModel>>> Handle(GetAllOrderingAddressByOrderingId request, CancellationToken cancellationToken)
        {
            var orderingStatusPathFile = Path.Combine(Directory.GetCurrentDirectory(), $"Shared\\OrderingStatus.json");
            var statusJson = System.IO.File.ReadAllText(orderingStatusPathFile);
            List<OrderingStatusViewModel> orderingStatus = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OrderingStatusViewModel>>(statusJson);

            var orderingAddressFile = _mapper.Map<List<OrderingAddressFileViewModel>>(await _orderingAddressFileRepository.GetAllAsync());



            var validFilter = _mapper.Map<GetAllOrderingAddressByOrderingId>(request);
            
         
            var OrderingAddress = (await _orderingAddressRepository.FindByCondition(x => x.Ordering.Id.Equals(request.OrderingId)).ConfigureAwait(false)).AsQueryable().ToList();


            var result = _mapper.Map<IEnumerable<OrderingAddressViewModel>>(OrderingAddress).Select(p => new OrderingAddressViewModel
            {
                id = p.id,
                trackingCode = p.trackingCode,
                orderingId = p.orderingId,
                personalName = p.personalName,
                phoneCode = p.phoneCode,
                phoneNumber = p.phoneNumber,
                email = p.email,
                country = p.country,
                province = p.province,
                district = p.district,
                subdistrict = p.subdistrict,
                status = p.status,
                statusObj = orderingStatus.Where(o => o.id == p.status).FirstOrDefault(),
                statusReason = p.statusReason,
                postCode = p.postCode,
                road = p.road,
                alley = p.alley,
                address = p.address,
                branch = p.branch,
                addressType = p.addressType,
                addressName = p.addressName,
                date = p.date,
                maps = p.maps,
                sequence = p.sequence,
                note = p.note,
                driverWaitingToPickUpDate = p.driverWaitingToPickUpDate,
                driverWaitingToDeliveryDate = p.driverWaitingToDeliveryDate,
                uploadNote = p.uploadNote,
                products = p.products,
                files = p.files,
                compensation = p.compensation
                

            }).Where(x=>x.orderingId == validFilter.OrderingId);

           int itemCount = result.Count();
            result = result.Where(c => c.statusObj.th.Contains(validFilter.Search) || c.compensation.ToString().Contains(validFilter.Search) || c.actualreceive.ToString().Contains(validFilter.Search) || c.personalName.ToString().Contains(validFilter.Search)).ToList();

          result = result.Skip((validFilter.PageNumber - 1) * validFilter.PageSize).Take(validFilter.PageSize);


            return new PagedResponse<IEnumerable<OrderingAddressViewModel>>(result, validFilter.PageNumber, validFilter.PageSize, itemCount);
        }
    }
}
