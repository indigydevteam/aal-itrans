﻿using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;

namespace iTrans.Transportation.Application.Features.OrderingAddresses.Commands
{
    public class DeleteOrderingAddressCommand : IRequest<Response<int>>
    {
        public int Id { get; set; }
        public Guid OrderingId { get; set; }
        public class DeleteOrderingAddressCommandHandler : IRequestHandler<DeleteOrderingAddressCommand, Response<int>>
        {
            private readonly IOrderingAddressRepositoryAsync _OrderingAddressRepository;
            private readonly IOrderingAddressProductRepositoryAsync _orderingAddressProductRepository;
            private readonly IConfiguration _configuration;
            public DeleteOrderingAddressCommandHandler(IOrderingAddressRepositoryAsync OrderingAddressRepository, IOrderingAddressProductRepositoryAsync orderingAddressProductRepository, IConfiguration configuration)//, IOrderingAddressFileRepositoryAsync orderingAddressFileRepository)
            {
                _OrderingAddressRepository = OrderingAddressRepository;
                _orderingAddressProductRepository = orderingAddressProductRepository;
                _configuration = configuration;
                // _orderingAddressFileRepository = orderingAddressFileRepository;
            }
            public async Task<Response<int>> Handle(DeleteOrderingAddressCommand command, CancellationToken cancellationToken)
            {
                try
                {
                    var getContentPath = _configuration.GetSection("ContentPath");
                    string contentPath = getContentPath.Value;
                    var OrderingAddress = (await _OrderingAddressRepository.FindByCondition(x => x.Id == command.Id && x.Ordering.Id.Equals(command.OrderingId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                    if (OrderingAddress == null)
                    {
                        throw new ApiException($"OrderingAddress Not Found.");
                    }
                    else
                    {
                        (await _orderingAddressProductRepository.CreateSQLQuery("DELETE Ordering_AddressProduct where OrderingAddressId = '" + command.Id.ToString() + "'").ConfigureAwait(false)).ExecuteUpdate();
                        //(await _orderingAddressFileRepository.CreateSQLQuery("DELETE Ordering_AddressFile where OrderingAddressId = '" + command.Id.ToString() + "'").ConfigureAwait(false)).ExecuteUpdate();
                        //var orderingAddressFiles = (await _orderingAddressFileRepository.FindByCondition(x => x.OrderingAddress.Id == command.Id && x.OrderingAddress.Ordering.Id.Equals(command.OrderingId)).ConfigureAwait(false)).AsQueryable().ToList();
                        //foreach (OrderingAddressFile orderingAddressFile in orderingAddressFiles)
                        //{
                        //    File.Delete(Path.Combine(contentPath, orderingAddressFile.FilePath));
                        //    await _orderingAddressFileRepository.DeleteAsync(orderingAddressFile);
                        //}
                        await _OrderingAddressRepository.DeleteAsync(OrderingAddress);
                        return new Response<int>(OrderingAddress.Id);
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
    }
}
