﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.OrderingAddressProduct;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;


namespace iTrans.Transportation.Application.Features.OrderingAddresses.Commands
{
    public class UpdateOrderingAddressCommand : IRequest<Response<int>>
    {
        public virtual int Id { get; set; }
        public virtual Guid OrderingId { get; set; }
        public virtual string PersonalName { get; set; }
        public virtual string PhoneCode { get; set; }
        public virtual string PhoneNumber { get; set; }
        public virtual string Email { get; set; }
        public virtual string AddressType { get; set; }
        public virtual int CountryId { get; set; }
        public virtual int ProvinceId { get; set; }
        public virtual int DistrictId { get; set; }
        public virtual int SubdistrictId { get; set; }
        public virtual string PostCode { get; set; }
        public virtual string Road { get; set; }
        public virtual string Alley { get; set; }
        public virtual string Address { get; set; }
        public virtual DateTime Date { get; set; }
        public virtual string Maps { get; set; }
        public virtual int Sequence { get; set; }
        public virtual string Note { get; set; }
        public virtual List<CreateAddressProductViewModel> RefProducts { get; set; }
    }

    public class UpdateOrderingAddressCommandHandler : IRequestHandler<UpdateOrderingAddressCommand, Response<int>>
    {
        private readonly IOrderingAddressRepositoryAsync _orderingAddressRepository;
        private readonly IOrderingAddressProductRepositoryAsync _orderingAddressProductRepository;
        private readonly ICountryRepositoryAsync _countryRepository;
        private readonly IProvinceRepositoryAsync _provinceRepository;
        private readonly IDistrictRepositoryAsync _districtRepository;
        private readonly ISubdistrictRepositoryAsync _subdistrictRepository;
        private readonly IProductTypeRepositoryAsync _productTypeRepository;
        private readonly IProductPackagingRepositoryAsync _productPackagingRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;

        public UpdateOrderingAddressCommandHandler(IOrderingAddressRepositoryAsync orderingAddressRepository, IOrderingAddressProductRepositoryAsync orderingAddressProductRepository, ICountryRepositoryAsync countryRepository
            , IProvinceRepositoryAsync provinceRepository, IDistrictRepositoryAsync districtRepository, ISubdistrictRepositoryAsync subdistrictRepository, IProductTypeRepositoryAsync productTypeRepository
            , IProductPackagingRepositoryAsync productPackagingRepository,IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _orderingAddressRepository = orderingAddressRepository;
            _orderingAddressProductRepository = orderingAddressProductRepository;
            _countryRepository = countryRepository;
            _provinceRepository = provinceRepository;
            _districtRepository = districtRepository;
            _subdistrictRepository = subdistrictRepository;
            _productTypeRepository = productTypeRepository;
            _productPackagingRepository = productPackagingRepository;
            _applicationLogRepository = applicationLogRepository; 
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(UpdateOrderingAddressCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var OrderingAddress = (await _orderingAddressRepository.FindByCondition(x => x.Id == request.Id && x.Ordering.Id.Equals(request.OrderingId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (OrderingAddress == null)
                {
                    throw new ApiException($"OrderingAddress Not Found.");
                }
                else
                {
                    List<ProductType> productTypeList = (await _productTypeRepository.FindByCondition(x => x.Active.Equals(true)).ConfigureAwait(false)).AsQueryable().ToList();
                    List<ProductPackaging> packagingList = (await _productPackagingRepository.FindByCondition(x => x.Active.Equals(true)).ConfigureAwait(false)).AsQueryable().ToList();

                    OrderingAddress.PersonalName = request.PersonalName;
                    OrderingAddress.PhoneCode = request.PhoneCode;
                    if (OrderingAddress.PhoneNumber != null && OrderingAddress.PhoneNumber.Length == 10 && OrderingAddress.PhoneNumber.First() == '0')
                    {
                        OrderingAddress.PhoneNumber = OrderingAddress.PhoneNumber.Remove(0, 1);
                    }
                    OrderingAddress.Email = request.Email;
                    OrderingAddress.AddressType = request.AddressType;
                    OrderingAddress.Country = (await _countryRepository.FindByCondition(x => x.Id.Equals(request.CountryId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                    OrderingAddress.Province = (await _provinceRepository.FindByCondition(x => x.Id.Equals(request.ProvinceId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                    OrderingAddress.District = (await _districtRepository.FindByCondition(x => x.Id.Equals(request.DistrictId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                    OrderingAddress.Subdistrict = (await _subdistrictRepository.FindByCondition(x => x.Id.Equals(request.SubdistrictId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                    OrderingAddress.PostCode = request.PostCode;
                    OrderingAddress.Road = request.Road;
                    OrderingAddress.Alley = request.Alley;
                    OrderingAddress.Address = request.Address;
                    OrderingAddress.AddressType = request.AddressType;
                    OrderingAddress.Date = request.Date;
                    OrderingAddress.Maps = request.Maps;
                    OrderingAddress.Sequence = request.Sequence;
                    OrderingAddress.Modified = DateTime.UtcNow;

                    (await _orderingAddressProductRepository.CreateSQLQuery("DELETE Ordering_AddressProduct where OrderingAddressId = '" + request.Id.ToString() + "'").ConfigureAwait(false)).ExecuteUpdate();

                    List<OrderingAddressProduct> products = new List<OrderingAddressProduct>();
                    foreach (CreateAddressProductViewModel refProduct in request.RefProducts)
                    {
                        var product = _mapper.Map<OrderingAddressProduct>(refProduct);
                        product.OrderingAddress = OrderingAddress;
                        //product.ProductType = productTypeList.Where(p => p.Id.Equals(refProduct.ProductTypeId)).FirstOrDefault();
                        //product.Packaging = packagingList.Where(p => p.Id.Equals(refProduct.PackagingId)).FirstOrDefault();
                        products.Add(product);
                    }
                    OrderingAddress.AddressProducts = products;

                    await _orderingAddressRepository.UpdateAsync(OrderingAddress);
                    var log = new CreateAppLog(_applicationLogRepository);
                    log.Create("Ordering", "Ordering Adrress", "Edit", Newtonsoft.Json.JsonConvert.SerializeObject(OrderingAddress));
                    return new Response<int>(OrderingAddress.Id);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}