﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.OrderingAddressProduct;
using iTrans.Transportation.Application.Enums;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;


namespace iTrans.Transportation.Application.Features.OrderingAddresses.Commands
{
    public class UpdateOrderingAddressStatusCommand : IRequest<Response<int>>
    {
        public  int Id { get; set; }
        public  Guid OrderingId { get; set; }
        public int Status { get; set; }
    }

    public class UpdateOrderingAddressStatusCommandHandler : IRequestHandler<UpdateOrderingAddressStatusCommand, Response<int>>
    {
        private readonly IOrderingAddressRepositoryAsync _orderingAddressRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;

        public UpdateOrderingAddressStatusCommandHandler(IOrderingAddressRepositoryAsync orderingAddressRepository, IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _orderingAddressRepository = orderingAddressRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(UpdateOrderingAddressStatusCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var OrderingAddress = (await _orderingAddressRepository.FindByCondition(x => x.Id == request.Id && x.Ordering.Id.Equals(request.OrderingId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (OrderingAddress == null)
                {
                    throw new ApiException($"OrderingAddress Not Found.");
                }
                else
                {

                    OrderingAddress.Status = request.Status;// Enum.GetName(typeof(OrderingAddressStatus), request.Status);
                    OrderingAddress.Modified = DateTime.UtcNow;

                    await _orderingAddressRepository.UpdateAsync(OrderingAddress);
                    var log = new CreateAppLog(_applicationLogRepository);
                    log.Create("ordering", "orderingadrress ", this.ToString(), Newtonsoft.Json.JsonConvert.SerializeObject(request));
                    return new Response<int>(OrderingAddress.Id);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}