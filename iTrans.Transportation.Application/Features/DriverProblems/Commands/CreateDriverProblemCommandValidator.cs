﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
namespace iTrans.Transportation.Application.Features.DriverProblems.Commands
{
    public class CreateDriverProblemCommandValidator : AbstractValidator<CreateDriverProblemCommand>
    {
        private readonly IDriverProblemRepositoryAsync driverProblemRepository;

        public CreateDriverProblemCommandValidator(IDriverProblemRepositoryAsync driverProblemRepository)
        {
            this.driverProblemRepository = driverProblemRepository;

            RuleFor(p => p.ProblemTopicId)
                .NotNull().WithMessage("{PropertyName} is required.");

            RuleFor(p => p.Message)
                .NotEmpty().WithMessage("{PropertyName} is required.")
                .NotNull()
                .MustAsync(IsUnique).WithMessage("{PropertyName} already exists.");

            RuleFor(p => p.Email)
               .NotEmpty().WithMessage("{PropertyName} is required.")
               .NotNull();
        }

        private async Task<bool> IsUnique(string value, CancellationToken cancellationToken)
        {
            var driverProblemObject = (await driverProblemRepository.FindByCondition(x => x.Message.ToLower() == value.ToLower()).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            if (driverProblemObject != null)
            {
                return false;
            }
            return true;
        }
    }
}
