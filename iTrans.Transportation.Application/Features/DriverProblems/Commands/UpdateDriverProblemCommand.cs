﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;

namespace iTrans.Transportation.Application.Features.DriverProblems.Commands
{
    public class UpdateDriverProblemCommand : IRequest<Response<int>>
    {
        public int Id { get; set; }
        public Guid ProblemTopicId { get; set; }
        public string Message { get; set; }
        public string Email { get; set; }
    }

    public class UpdateDriverProblemCommandHandler : IRequestHandler<UpdateDriverProblemCommand, Response<int>>
    {
        private readonly IDriverProblemRepositoryAsync _driverProblemRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;

        public UpdateDriverProblemCommandHandler(IDriverProblemRepositoryAsync driverProblemRepository, IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _driverProblemRepository = driverProblemRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(UpdateDriverProblemCommand request, CancellationToken cancellationToken)
        {
            var driverProblem = (await _driverProblemRepository.FindByCondition(x => x.Id == request.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            if (driverProblem == null)
            {
                throw new ApiException($"DriverProblem Not Found.");
            }
            else
            {
                //driverProblem.ProblemTopicId = request.ProblemTopicId;
                driverProblem.Message = request.Message;
                driverProblem.Email = request.Email;
                driverProblem.Modified = DateTime.UtcNow;
                await _driverProblemRepository.UpdateAsync(driverProblem);
                var log = new CreateAppLog(_applicationLogRepository);
                log.Create("Driver", "Driver Problem", "Edit", Newtonsoft.Json.JsonConvert.SerializeObject(request));
                return new Response<int>(driverProblem.Id);
            }
        }
    }
}
