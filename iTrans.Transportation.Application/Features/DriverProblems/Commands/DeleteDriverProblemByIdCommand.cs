﻿using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
namespace iTrans.Transportation.Application.Features.DriverProblems.Commands
{
    public class DeleteDriverProblemByIdCommand : IRequest<Response<int>>
    {
        public int Id { get; set; }
        public class DeleteDriverProblemByIdCommandHandler : IRequestHandler<DeleteDriverProblemByIdCommand, Response<int>>
        {
            private readonly IDriverProblemRepositoryAsync _DriverProblemRepository;
            public DeleteDriverProblemByIdCommandHandler(IDriverProblemRepositoryAsync DriverProblemRepository)
            {
                _DriverProblemRepository = DriverProblemRepository;
            }
            public async Task<Response<int>> Handle(DeleteDriverProblemByIdCommand command, CancellationToken cancellationToken)
            {
                var DriverProblem = (await _DriverProblemRepository.FindByCondition(x => x.Id == command.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (DriverProblem == null)
                {
                    throw new ApiException($"DriverProblem Not Found.");
                }
                else
                {
                    await _DriverProblemRepository.DeleteAsync(DriverProblem);
                    return new Response<int>(DriverProblem.Id);
                }
            }
        }
    }
}
