﻿using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Interfaces.Services;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.DriverProblems.Commands
{
    public partial class CreateDriverProblemCommand : IRequest<Response<int>>
    {
        public int ProblemTopicId { get; set; }
        public Guid DriverId { get; set; }
        public string Message { get; set; }
        public  string Email { get; set; }
        public List<IFormFile> Files { get; set; }

    }
    public class CreateDriverProblemCommandHandler : IRequestHandler<CreateDriverProblemCommand, Response<int>>
    {
        private readonly IDriverRepositoryAsync _driverRepository;
        private readonly IProblemTopicRepositoryAsync _problemTopicRepository;
        private readonly IDriverProblemRepositoryAsync _driverProblemRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IConfiguration _configuration;
        private readonly IMapper _mapper;
        public CreateDriverProblemCommandHandler(IDriverRepositoryAsync driverRepository, IProblemTopicRepositoryAsync problemTopicRepository, 
            IDriverProblemRepositoryAsync driverProblemRepository,IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper, IConfiguration configuration)
        {
            _driverRepository = driverRepository;
            _driverProblemRepository = driverProblemRepository;
            _problemTopicRepository = problemTopicRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
            _configuration = configuration;
        }

        public async Task<Response<int>> Handle(CreateDriverProblemCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var driverObject = (await _driverRepository.FindByCondition(x => x.Id.Equals(request.DriverId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (driverObject == null)
                {
                    throw new ApiException($"Driver Not Found.");
                }
                var problemTopic = (await _problemTopicRepository.FindByCondition(x => x.Id.Equals(request.ProblemTopicId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                DriverProblem problem = new DriverProblem
                {
                    Driver = driverObject,
                    ProblemTopic = problemTopic,
                    Message = request.Message,
                    Email = request.Email,
                    Files = new List<DriverProblemFile>()
                };

                if (request.Files != null)
                {
                    string currentTimeStr = DateTime.Now.ToString("yyyyMMddHHmmss");
                    var getContentPath = _configuration.GetSection("ContentPath");
                    string contentPath = getContentPath.Value;

                    int fileCount = 0;

                    string folderPath = contentPath + "driver/problem/" + driverObject.Id.ToString() + "/" + currentTimeStr;
                    if (!Directory.Exists(folderPath))
                    {
                        Directory.CreateDirectory(folderPath);
                    }
                    foreach (IFormFile file in request.Files)
                    {
                        fileCount = fileCount + 1;
                        string filePath = Path.Combine(folderPath, file.FileName);
                        using (FileStream fileStream = new FileStream(filePath, FileMode.Create))
                        {
                            await file.CopyToAsync(fileStream);
                            FileInfo fi = new FileInfo(filePath);

                            DriverProblemFile problemFile = new DriverProblemFile
                            {
                                DriverProblem = problem,
                                FileName = file.FileName,
                                ContentType = file.ContentType,
                                FilePath = "driver/problem/" + driverObject.Id.ToString() + "/" + currentTimeStr + "/" + file.FileName,
                                DirectoryPath = "driver/problem/" + driverObject.Id.ToString() + "/" + currentTimeStr + "/",
                                FileEXT = fi.Extension,
                                Sequence = fileCount,
                            };
                            problem.Files.Add(problemFile);
                        }
                    }
                }

                problem.Created = DateTime.UtcNow;
                problem.Modified = DateTime.UtcNow;

                var problemObject = await _driverProblemRepository.AddAsync(problem);
                var log = new CreateAppLog(_applicationLogRepository);
                log.Create("Driver", "Driver Problem", "Create", Newtonsoft.Json.JsonConvert.SerializeObject(request));
                return new Response<int>(problemObject.Id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
