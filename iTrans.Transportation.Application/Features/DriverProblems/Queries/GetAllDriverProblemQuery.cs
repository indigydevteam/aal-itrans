﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.DriverProblem;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.DriverProblems.Queries
{
    public class GetAllDriverProblemQuery : IRequest<Response<IEnumerable<DriverProblemViewModel>>>
    {
         
    }
    public class GetAllDriverProblemQueryHandler : IRequestHandler<GetAllDriverProblemQuery, Response<IEnumerable<DriverProblemViewModel>>>
    {
        private readonly IDriverProblemRepositoryAsync _driverProblemRepository;
        private readonly IMapper _mapper;
        public GetAllDriverProblemQueryHandler(IDriverProblemRepositoryAsync driverProblemRepository, IMapper mapper)
        {
            _driverProblemRepository = driverProblemRepository;
            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<DriverProblemViewModel>>> Handle(GetAllDriverProblemQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var driver = await _driverProblemRepository.GetAllAsync();
                var driverProblemViewModel = _mapper.Map<IEnumerable<DriverProblemViewModel>>(driver);
                return new Response<IEnumerable<DriverProblemViewModel>>(driverProblemViewModel);
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
    }
}
