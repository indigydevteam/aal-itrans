﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.DriverProblem;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.DriverProblems.Queries
{
   public class GetDriverProblemByIdQuery : IRequest<Response<DriverProblemViewModel>>
    {
        public int Id { get; set; }
    }
    public class GetDriverProblemByIdQueryHandler : IRequestHandler<GetDriverProblemByIdQuery, Response<DriverProblemViewModel>>
    {
        private readonly IDriverProblemRepositoryAsync _driverProblemRepository;
        private readonly IMapper _mapper;
        public GetDriverProblemByIdQueryHandler(IDriverProblemRepositoryAsync driverProblemRepository, IMapper mapper)
        {
            _driverProblemRepository = driverProblemRepository;
            _mapper = mapper;
        }
        public async Task<Response<DriverProblemViewModel>> Handle(GetDriverProblemByIdQuery request, CancellationToken cancellationToken)
        {
            var driverProblemObject = (await _driverProblemRepository.FindByCondition(x => x.Id.Equals(request.Id)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            return new Response<DriverProblemViewModel>(_mapper.Map<DriverProblemViewModel>(driverProblemObject));
        }
    }
}
