﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using iTrans.Transportation.Application.Interfaces.Repositories;
using FluentValidation;

namespace iTrans.Transportation.Application.Features.DriverProblems.Queries
{
    public class GetDriverProblemByIdQueryValidator : AbstractValidator<GetDriverProblemByIdQuery>
    {
        private readonly IDriverProblemRepositoryAsync driverProblemRepository;

        public GetDriverProblemByIdQueryValidator(IDriverProblemRepositoryAsync driverProblemRepository)
        {
            this.driverProblemRepository = driverProblemRepository;
            RuleFor(p => p.Id)
               .NotEmpty().WithMessage("{PropertyName} is required.")
               .NotNull()
               .MustAsync(IsDriverProblemExists).WithMessage("{PropertyName} not exists.");
        }

        private async Task<bool> IsDriverProblemExists(int Id, CancellationToken cancellationToken)
        {
            var userObject = (await driverProblemRepository.FindByCondition(x => x.Id.Equals(Id)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            if (userObject != null)
            {
                return true;
            }
            return false;
        }
    }
}
