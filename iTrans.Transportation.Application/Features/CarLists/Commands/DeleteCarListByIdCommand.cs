﻿using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.CarLists.Commands
{
    public class DeleteCarListByIdCommand : IRequest<Response<int>>
    {
        public int Id { get; set; }
        public class DeleteCarListByIdCommandHandler : IRequestHandler<DeleteCarListByIdCommand, Response<int>>
        {
            private readonly ICarListRepositoryAsync _carListRepository;
            public DeleteCarListByIdCommandHandler(ICarListRepositoryAsync carListRepository)
            {
                _carListRepository = carListRepository;
            }
            public async Task<Response<int>> Handle(DeleteCarListByIdCommand command, CancellationToken cancellationToken)
            {
                var carList = (await _carListRepository.FindByCondition(x => x.Id == command.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (carList == null)
                {
                    throw new ApiException($"CarList Not Found.");
                }
                else
                {
                    await _carListRepository.DeleteAsync(carList);
                    return new Response<int>(carList.Id);
                }
            }
        }
    }
}
