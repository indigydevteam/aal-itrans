﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;

namespace iTrans.Transportation.Application.Features.CarLists.Commands
{
    public class UpdateCarListCommand : IRequest<Response<int>>
    {
        public Guid? UserId { get; set; }
        public int Id { get; set; }
        public int CarTypeId { get; set; }
        public string Name_TH { get; set; }
        public string Name_ENG { get; set; }
        public int Sequence { get; set; }
        public bool Active { get; set; }
    }

    public class UpdateCarListCommandHandler : IRequestHandler<UpdateCarListCommand, Response<int>>
    {
        private readonly ICarListRepositoryAsync _carListRepository;
        private readonly ICarTypeRepositoryAsync _carTypeRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;

        public UpdateCarListCommandHandler(ICarListRepositoryAsync carListRepository, ICarTypeRepositoryAsync carTypeRepository, IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _carTypeRepository = carTypeRepository;
            _carListRepository = carListRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(UpdateCarListCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var carType = (await _carTypeRepository.FindByCondition(x => x.Id == request.CarTypeId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (carType == null)
                {
                    throw new ApiException($"CarType Not Found.");
                }
                var carList = (await _carListRepository.FindByCondition(x => x.Id == request.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (carList == null)
                {
                    throw new ApiException($"CarList Not Found.");
                }
                else
                {
                    carList.CarType = carType;
                    carList.Name_TH = request.Name_TH;
                    carList.Name_ENG = request.Name_ENG;
                    carList.CarType = carType;
                    carList.Sequence = request.Sequence;
                    carList.Active = request.Active;

                    await _carListRepository.UpdateAsync(carList);
                    var log = new CreateAppLog(_applicationLogRepository);
                    log.CreateLog("Car", "Car List", "Edit", Newtonsoft.Json.JsonConvert.SerializeObject(carList),request.UserId.ToString());
                    return new Response<int>(carList.Id);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
