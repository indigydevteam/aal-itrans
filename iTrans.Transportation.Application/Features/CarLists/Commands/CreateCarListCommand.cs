﻿using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Interfaces.Services;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.CarLists.Commands
{
    public partial class CreateCarListCommand : IRequest<Response<int>>
    {
        public Guid? UserId { get; set; }
        public int CarTypeId { get; set; }
        public string Name_TH { get; set; }
        public string Name_ENG { get; set; }
        public int Sequence { get; set; }
        public bool Active { get; set; }

    }
    public class CreateCarListCommandHandler : IRequestHandler<CreateCarListCommand, Response<int>>
    {
        private readonly ICarTypeRepositoryAsync _carTypeRepository;
        private readonly ICarListRepositoryAsync _carListRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;
        public CreateCarListCommandHandler(ICarListRepositoryAsync carListRepository, ICarTypeRepositoryAsync carTypeRepository, IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _carListRepository = carListRepository;
            _carTypeRepository = carTypeRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(CreateCarListCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var carTypeObject = (await _carTypeRepository.FindByCondition(x => x.Id.Equals(request.CarTypeId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (carTypeObject == null)
                {
                    throw new ApiException($"CarType Not Found.");
                }
                var carList = _mapper.Map<CarList>(request);
                carList.CarType = carTypeObject;
                carList.Created = DateTime.UtcNow;
                carList.Modified = DateTime.UtcNow;
                //title.CreatedBy = "xxxxxxx";
                //title.ModifiedBy = "xxxxxxx";
                var carListObject = await _carListRepository.AddAsync(carList);
                var log = new CreateAppLog(_applicationLogRepository);
                log.CreateLog("Car", "Car List", "Create", Newtonsoft.Json.JsonConvert.SerializeObject(carList),request.UserId.ToString());
                return new Response<int>(carListObject.Id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
