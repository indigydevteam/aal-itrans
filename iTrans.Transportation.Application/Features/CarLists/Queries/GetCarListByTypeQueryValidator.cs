﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using iTrans.Transportation.Application.Interfaces.Repositories;
using FluentValidation;

namespace iTrans.Transportation.Application.Features.CarLists.Queries
{
    public class GetCarListByTypeQueryValidator : AbstractValidator<GetCarListByTypeQuery>
    {
        private readonly ICarListRepositoryAsync carListRepository;

        public GetCarListByTypeQueryValidator(ICarListRepositoryAsync carListRepository)
        {
            this.carListRepository = carListRepository;
            RuleFor(p => p.TypeId)
               .NotEmpty().WithMessage("{PropertyName} is required.")
               .NotNull()
               .MustAsync(IsCarListExists).WithMessage("{PropertyName} not exists.");
        }
         
        private async Task<bool> IsCarListExists(int TypeId, CancellationToken cancellationToken)
        {
            var userObject = (await carListRepository.FindByCondition(x => x.CarType.Id.Equals(TypeId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            if (userObject != null)
            {
                return true;
            }
            return false;
        }
    }
}
