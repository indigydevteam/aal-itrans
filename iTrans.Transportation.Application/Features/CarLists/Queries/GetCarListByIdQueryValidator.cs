﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using iTrans.Transportation.Application.Interfaces.Repositories;
using FluentValidation;

namespace iTrans.Transportation.Application.Features.CarLists.Queries
{
    public class GetCarListByIdQueryValidator : AbstractValidator<GetCarListByIdQuery>
    {
        private readonly ICarListRepositoryAsync carListRepository;

        public GetCarListByIdQueryValidator(ICarListRepositoryAsync carListRepository)
        {
            this.carListRepository = carListRepository;
            RuleFor(p => p.Id)
               .NotEmpty().WithMessage("{PropertyName} is required.")
               .NotNull()
               .MustAsync(IsCarListExists).WithMessage("{PropertyName} not exists.");
        }

        private async Task<bool> IsCarListExists(int CarListId, CancellationToken cancellationToken)
        {
            var userObject = (await carListRepository.FindByCondition(x => x.Id.Equals(CarListId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            if (userObject != null)
            {
                return true;
            }
            return false;
        }
    }
}
