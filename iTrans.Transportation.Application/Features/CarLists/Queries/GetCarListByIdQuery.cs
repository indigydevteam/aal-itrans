﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.CarList;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.CarLists.Queries
{
    public class GetCarListByIdQuery : IRequest<Response<CarListViewModel>>
    {
        public int Id { get; set; }
    }
    public class GetCarListByIdQueryHandler : IRequestHandler<GetCarListByIdQuery, Response<CarListViewModel>>
    {
        private readonly ICarListRepositoryAsync _carListRepository;
        private readonly IMapper _mapper;
        public GetCarListByIdQueryHandler(ICarListRepositoryAsync carListRepository, IMapper mapper)
        {
            _carListRepository = carListRepository;
            _mapper = mapper;
        }
        public async Task<Response<CarListViewModel>> Handle(GetCarListByIdQuery request, CancellationToken cancellationToken)
        {
            var carListObject = (await _carListRepository.FindByCondition(x => x.Id.Equals(request.Id)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            return new Response<CarListViewModel>(_mapper.Map<CarListViewModel>(carListObject));
        }
    }
}
