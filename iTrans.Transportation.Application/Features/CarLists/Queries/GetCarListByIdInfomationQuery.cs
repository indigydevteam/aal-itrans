﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.CarList;
using iTrans.Transportation.Application.DTOs.CarType;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.CarLists.Queries
{
    public class GetCarListByIdInfomationQuery : IRequest<Response<CarListInfomationViewModel>>
    {
        public int Id { get; set; }
    }
    public class GetCarListByIdInfomationQueryHandler : IRequestHandler<GetCarListByIdInfomationQuery, Response<CarListInfomationViewModel>>
    {

        private readonly ICarListRepositoryAsync _carListRepository;
        private readonly ICarTypeRepositoryAsync _carTypeRepository;

        private readonly IMapper _mapper;
        public GetCarListByIdInfomationQueryHandler(ICarListRepositoryAsync carListRepository, ICarTypeRepositoryAsync carTypeRepository, IMapper mapper)
        {
            _carListRepository = carListRepository;
            _carTypeRepository = carTypeRepository;
            _mapper = mapper;
        }
        public async Task<Response<CarListInfomationViewModel>> Handle(GetCarListByIdInfomationQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var carListObject = (await _carListRepository.FindByCondition(x => x.Id.Equals(request.Id)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                CarListInfomationViewModel carListObjectViewModel = _mapper.Map<CarListInfomationViewModel>(carListObject);
                //if (carListObjectViewModel != null)
                //    carListObjectViewModel.CarType = _mapper.Map<CarTypeViewModel>((await _carTypeRepository.FindByCondition(x => x.Id == carListObject.CarTypeId).ConfigureAwait(false)).AsQueryable().FirstOrDefault());


                return new Response<CarListInfomationViewModel>(carListObjectViewModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
