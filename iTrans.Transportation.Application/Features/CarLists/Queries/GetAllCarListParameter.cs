﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.CarList;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;

namespace iTrans.Transportation.Application.Features.CarLists.Queries
{
    public class GetAllCarListParameter : IRequest<PagedResponse<IEnumerable<CarListInfomationViewModel>>>
    {
        public string Search { set; get; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
    }
    public class GetAllCarListParameterHandler : IRequestHandler<GetAllCarListParameter, PagedResponse<IEnumerable<CarListInfomationViewModel>>>
    {
        private readonly ICarListRepositoryAsync _carListRepository;
        private readonly IMapper _mapper;
        private Expression<Func<CarList, bool>> expression;
        public GetAllCarListParameterHandler(ICarListRepositoryAsync carListRepository, IMapper mapper)
        {
            _carListRepository = carListRepository;
            _mapper = mapper;
        }

        public async Task<PagedResponse<IEnumerable<CarListInfomationViewModel>>> Handle(GetAllCarListParameter request, CancellationToken cancellationToken)
        {
            var validFilter = _mapper.Map<GetAllCarListParameter>(request);
            if (validFilter.Search != null && validFilter.Search.Trim() != "")
            {
                expression = x => (x.Name_TH.Contains(validFilter.Search.Trim()) || x.Name_ENG.Contains(validFilter.Search.Trim()) || x.CarType.Name_TH.Contains(validFilter.Search.Trim()) || x.CarType.Name_ENG.Contains(validFilter.Search.Trim()) || x.Sequence.ToString().Contains(validFilter.Search.Trim()));
            }
            int itemCount = _carListRepository.GetItemCount(expression);
            //Expression<Func<CarList, bool>> expression = x => x.Name_TH != "";
            var carList = await _carListRepository.FindByConditionWithPage(expression, validFilter.PageNumber, validFilter.PageSize);
            var CarListInfomationViewModel = _mapper.Map<IEnumerable<CarListInfomationViewModel>>(carList);
            return new PagedResponse<IEnumerable<CarListInfomationViewModel>>(CarListInfomationViewModel, validFilter.PageNumber, validFilter.PageSize, itemCount);
        }
    }
}
