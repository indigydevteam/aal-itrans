﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.CarList;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.CarLists.Queries
{
    public class GetAllCarListQuery : IRequest<Response<IEnumerable<CarListViewModel>>>
    {

    }
    public class GetAllCarListQueryHandler : IRequestHandler<GetAllCarListQuery, Response<IEnumerable<CarListViewModel>>>
    {
        private readonly ICarListRepositoryAsync _carListRepository;
        private readonly IMapper _mapper;
        public GetAllCarListQueryHandler(ICarListRepositoryAsync carListRepository, IMapper mapper)
        {
            _carListRepository = carListRepository;
            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<CarListViewModel>>> Handle(GetAllCarListQuery request, CancellationToken cancellationToken)
        {
            var carList = (await _carListRepository.FindByCondition(x => x.Active == true).ConfigureAwait(false)).OrderBy(x => x.Sequence).AsQueryable().ToList();
            var carListViewModel = _mapper.Map<IEnumerable<CarListViewModel>>(carList);
            return new Response<IEnumerable<CarListViewModel>>(carListViewModel);
        }
    }
}
