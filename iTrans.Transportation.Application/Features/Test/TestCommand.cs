﻿using AutoMapper;
using Microsoft.Extensions.Configuration;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Helper;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Interfaces.Services;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.Test
{
    public partial class TestCommand : IRequest<Response<int>>
    {
        public string startMap { get; set; }
        public string endMap { get; set; }
    }
    public class CreateSubdistrictCommandHandler : IRequestHandler<TestCommand, Response<int>>
    {
        private readonly IConfiguration _configuration;
        public CreateSubdistrictCommandHandler( IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public async Task<Response<int>> Handle(TestCommand request, CancellationToken cancellationToken)
        {
            int detectArea = Convert.ToInt32(_configuration.GetSection("DetectArea").Value);
            string api_key = _configuration.GetSection("GoogleAPIKEY").Value;

            var locationDistince = LocationHelper.GetDistanceAndEstimateTime(detectArea, api_key, request.startMap, request.endMap);
            var TotalDistance = locationDistince.Distance;
            var TotalEstimateTime = locationDistince.EstimateTime;
            var Distance = locationDistince.Distance;
            var EstimateTime = locationDistince.EstimateTime;
            return new Response<int>(1);

        }
    }
}
