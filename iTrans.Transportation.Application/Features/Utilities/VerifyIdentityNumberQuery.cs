﻿using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using iTrans.Transportation.Application.Helper;

namespace iTrans.Transportation.Application.Features.Utilities
{
    public class VerifyIdentityNumberQuery : IRequest<Response<bool>>
    {
        public string Module { get; set; }
        public string IdentityNumber { get; set; }
    }
    public class VerifyIdentityNumberQueryHandler : IRequestHandler<VerifyIdentityNumberQuery, Response<bool>>
    {
        private readonly IMapper _mapper;

        public VerifyIdentityNumberQueryHandler(IMapper mapper)
        {
            _mapper = mapper;
        }

        public async Task<Response<bool>> Handle(VerifyIdentityNumberQuery request, CancellationToken cancellationToken)
        {
            try
            {

                if (request.IdentityNumber == null || request.IdentityNumber == "")
                {
                    return new Response<bool>(false);
                }

                string identityNumber = AESMgr.Decrypt(request.IdentityNumber);
                
                Regex rexPersonal = new Regex(@"^[0-9]{13}$");
                if (rexPersonal.IsMatch(identityNumber))
                {
                    int sum = 0;

                    for (int i = 0; i < 12; i++)
                    {
                        sum += int.Parse(identityNumber[i].ToString()) * (13 - i);
                    }

                    return new Response<bool>(int.Parse(identityNumber[12].ToString()) == ((11 - (sum % 11)) % 10));
                }
                return new Response<bool>(false);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
