﻿using AutoMapper;
using iTrans.Transportation.Application.DTOs;
using iTrans.Transportation.Application.DTOs.PaymentType;
using iTrans.Transportation.Application.DTOs.UserLevel;
using iTrans.Transportation.Application.DTOs.UserLevelCondition;
using iTrans.Transportation.Application.DTOs.WelcomeInformation;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.PaymentTypes.Queries
{
    public class GetAllPaymentTypeQuery : IRequest<Response<IEnumerable<PaymentTypeViewModel>>>
    {

    }
    public class GetAllOrdingQueryHandler : IRequestHandler<GetAllPaymentTypeQuery, Response<IEnumerable<PaymentTypeViewModel>>>
    {
        private readonly IPaymentTypeRepositoryAsync _paymentTypeRepository;
        private readonly IMapper _mapper;
        public GetAllOrdingQueryHandler(IPaymentTypeRepositoryAsync paymentTypeRepository, IMapper mapper)
        {
            _paymentTypeRepository = paymentTypeRepository;
            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<PaymentTypeViewModel>>> Handle(GetAllPaymentTypeQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var paymentTypes = (await _paymentTypeRepository.FindByCondition(x => x.Active.Equals(true)).ConfigureAwait(false)).AsQueryable().ToList();
                var paymentType = _mapper.Map<IEnumerable<PaymentTypeViewModel>>(paymentTypes);
                return new Response<IEnumerable<PaymentTypeViewModel>>(paymentType);
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
    }
}
