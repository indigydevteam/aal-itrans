﻿using AutoMapper;
using iTrans.Transportation.Application.DTOs.Register_Document;
using iTrans.Transportation.Application.DTOs.UserLevel;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.RegisterDocuments.Queries
{
   public class GetAllRegisterDocumentQuery : IRequest<Response<IEnumerable<RegisterDocumentViweModel>>>
    {

    }
    public class GetAllUserLevelQueryHandler : IRequestHandler<GetAllRegisterDocumentQuery, Response<IEnumerable<RegisterDocumentViweModel>>>
    {
        private readonly IRegisterDocumentRepositoryAsync _registerDocumentRepository;
        private readonly IMapper _mapper;
        public GetAllUserLevelQueryHandler(IRegisterDocumentRepositoryAsync registerDocumentRepository, IMapper mapper)
        {
            _registerDocumentRepository = registerDocumentRepository;
            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<RegisterDocumentViweModel>>> Handle(GetAllRegisterDocumentQuery request, CancellationToken cancellationToken)
        {
            var data = await _registerDocumentRepository.GetAllAsync();
            var RegisterDocumentViewModel = _mapper.Map<IEnumerable<RegisterDocumentViweModel>>(data);
            return new Response<IEnumerable<RegisterDocumentViweModel>>(RegisterDocumentViewModel);
        }
    }
}
