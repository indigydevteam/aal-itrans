﻿using AutoMapper;
using iTrans.Transportation.Application.DTOs.CarList;
using iTrans.Transportation.Application.DTOs.Register_Document;
using iTrans.Transportation.Application.DTOs.UserLevel;
using iTrans.Transportation.Application.Features.CarLists.Queries;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using iTrans.Transportation.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.RegisterDocuments.Queries
{
    public class GetAllRegisterDocumentParameter : IRequest<PagedResponse<IEnumerable<RegisterDocumentViweModel>>>
    {
        public string Search { set; get; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
    }
    public class GetAllRegisterDocumentParameterHandler : IRequestHandler<GetAllRegisterDocumentParameter, PagedResponse<IEnumerable<RegisterDocumentViweModel>>>
    {
        private readonly IRegisterDocumentRepositoryAsync _registerDocumentRepository;
        private readonly IMapper _mapper;
        private Expression<Func<RegisterDocument, bool>> expression;

        public GetAllRegisterDocumentParameterHandler(IRegisterDocumentRepositoryAsync registerDocumentRepository, IMapper mapper)
        {
            _registerDocumentRepository = registerDocumentRepository;
            _mapper = mapper;
        }

        public async Task<PagedResponse<IEnumerable<RegisterDocumentViweModel>>> Handle(GetAllRegisterDocumentParameter request, CancellationToken cancellationToken)
        {
            var validFilter = _mapper.Map<GetAllRegisterDocumentParameter>(request);
            if (validFilter.Search != null && validFilter.Search.Trim() != "")
            {
                expression = x => (x.Information_TH.Contains(validFilter.Search.Trim()) || x.Information_ENG.Contains(validFilter.Search.Trim()));
            }
            int itemCount = _registerDocumentRepository.GetItemCount(expression);
            //Expression<Func<RegisterDocument, bool>> expression = x => x.Information_TH != "";
            var data = await _registerDocumentRepository.FindByConditionWithPage(expression, validFilter.PageNumber, validFilter.PageSize);
            var RegisterDocumentViewModel = _mapper.Map<IEnumerable<RegisterDocumentViweModel>>(data);
            return new PagedResponse<IEnumerable<RegisterDocumentViweModel>>(RegisterDocumentViewModel, validFilter.PageNumber, validFilter.PageSize);
        }
    }
}
