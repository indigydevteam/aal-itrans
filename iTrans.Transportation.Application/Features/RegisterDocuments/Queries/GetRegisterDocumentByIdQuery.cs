﻿using AutoMapper;
using iTrans.Transportation.Application.DTOs.Register_Document;
using iTrans.Transportation.Application.DTOs.UserLevel;
using iTrans.Transportation.Application.Features.CarLists.Queries;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.RegisterDocuments.Queries
{
   public class GetRegisterDocumentByIdQuery : IRequest<Response<RegisterDocumentViweModel>>
    {
        public int Id { get; set; }
    }
    public class GetRegisterDocumentByIdQueryHandler : IRequestHandler<GetRegisterDocumentByIdQuery, Response<RegisterDocumentViweModel>>
    {
        private readonly IRegisterDocumentRepositoryAsync _registerDocumentRepository;
        private readonly IMapper _mapper;
        public GetRegisterDocumentByIdQueryHandler(IRegisterDocumentRepositoryAsync registerDocumentRepository, IMapper mapper)
        {
            _registerDocumentRepository = registerDocumentRepository;
            _mapper = mapper;
        }
        public async Task<Response<RegisterDocumentViweModel>> Handle(GetRegisterDocumentByIdQuery request, CancellationToken cancellationToken)
        {
            var DataObject = (await _registerDocumentRepository.FindByCondition(x => x.Id.Equals(request.Id)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            return new Response<RegisterDocumentViweModel>(_mapper.Map<RegisterDocumentViweModel>(DataObject));
        }
    }
}
