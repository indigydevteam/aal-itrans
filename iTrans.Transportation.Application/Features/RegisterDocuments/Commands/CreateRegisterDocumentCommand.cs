﻿using AutoMapper;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using iTrans.Transportation.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
namespace iTrans.Transportation.Application.Features.RegisterDocuments.Commands
{
   public partial class CreateRegisterDocumentCommand : IRequest<Response<int>>
    {

   
        public virtual string Module { get; set; }
        public virtual string Information_TH { get; set; }
        public virtual string Information_ENG { get; set; }
        public virtual int Sequence { get; set; }
        public virtual bool Active { get; set; }
    }
    public class CreateRegisterDocumentCommandHandler : IRequestHandler<CreateRegisterDocumentCommand, Response<int>>
    {
        private readonly IRegisterDocumentRepositoryAsync _registerDocumentRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;
        public CreateRegisterDocumentCommandHandler(IRegisterDocumentRepositoryAsync registerDocumentRepository,IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _registerDocumentRepository = registerDocumentRepository;
            _applicationLogRepository = applicationLogRepository; 
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(CreateRegisterDocumentCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var data = _mapper.Map<RegisterDocument>(request);
                data.Created = DateTime.UtcNow;
                data.Modified = DateTime.UtcNow;
                //title.CreatedBy = "xxxxxxx";
                //title.ModifiedBy = "xxxxxxx";
                var dataListObject = await _registerDocumentRepository.AddAsync(data);
                var log = new CreateAppLog(_applicationLogRepository);
                log.Create("RegisterDocument", "Register Document", "Create", Newtonsoft.Json.JsonConvert.SerializeObject(data));
                return new Response<int>(dataListObject.Id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}