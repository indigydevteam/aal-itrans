﻿using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.RegisterDocuments.Commands
{
   public class UpdateRegisterDocumentCommand : IRequest<Response<int>>
    {
        public int Id { get; set; }
        public virtual string Module { get; set; }
        public virtual string Information_TH { get; set; }
        public virtual string Information_ENG { get; set; }
        public virtual int Sequence { get; set; }
        public virtual bool Active { get; set; }
    }
    public class UpdateRegisterDocumentCommandHandler : IRequestHandler<UpdateRegisterDocumentCommand, Response<int>>
    {
        private readonly IRegisterDocumentRepositoryAsync _registerDocumentRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;

        public UpdateRegisterDocumentCommandHandler(IRegisterDocumentRepositoryAsync registerDocumentRepository,IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _registerDocumentRepository = registerDocumentRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(UpdateRegisterDocumentCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var data = (await _registerDocumentRepository.FindByCondition(x => x.Id == request.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (data == null)
                {
                    throw new ApiException($"RegisterDocument Not Found.");
                }
                else
                {
                    data.Module = request.Module;
                    data.Information_TH = request.Information_TH;
                    data.Information_ENG = request.Information_ENG;
                    data.Active = request.Active;
                    data.Sequence = request.Sequence;
                    data.Modified = DateTime.UtcNow;

                    await _registerDocumentRepository.UpdateAsync(data);
                    var log = new CreateAppLog(_applicationLogRepository);
                    log.Create("RegisterDocument", "Register Document","Edit", Newtonsoft.Json.JsonConvert.SerializeObject(data));
                    return new Response<int>(data.Id);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
