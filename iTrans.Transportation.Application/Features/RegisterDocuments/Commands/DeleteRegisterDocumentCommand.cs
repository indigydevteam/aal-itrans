﻿using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.RegisterDocuments.Commands
{
   public class DeleteRegisterDocumentCommand : IRequest<Response<int>>
    {
        public int Id { get; set; }
        public class DeleteRegisterDocumentByIdCommandHandler : IRequestHandler<DeleteRegisterDocumentCommand, Response<int>>
        {
            private readonly IRegisterDocumentRepositoryAsync _registerDocumentRepository;
            public DeleteRegisterDocumentByIdCommandHandler(IRegisterDocumentRepositoryAsync registerDocumentRepository)
            {
                _registerDocumentRepository = registerDocumentRepository;
            }
            public async Task<Response<int>> Handle(DeleteRegisterDocumentCommand command, CancellationToken cancellationToken)
            {
                var data = (await _registerDocumentRepository.FindByCondition(x => x.Id == command.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (data == null)
                {
                    throw new ApiException($"RegisterDocument Not Found.");
                }
                else
                {
                    await _registerDocumentRepository.DeleteAsync(data);
                    return new Response<int>(data.Id);
                }
            }
        }
    }
}
