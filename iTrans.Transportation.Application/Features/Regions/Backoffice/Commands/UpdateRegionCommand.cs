﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;

namespace iTrans.Transportation.Application.Features.Regions.Backoffice.Commands
{
    public class UpdateRegionCommand : IRequest<Response<int>>
    {
        public int Id { get; set; }
        public int CountryId { get; set; }
        public string Name_TH { get; set; }
        public string Name_ENG { get; set; }
        public int Sequence { get; set; }
        public bool Active { get; set; }
    }

    public class UpdateRegionCommandHandler : IRequestHandler<UpdateRegionCommand, Response<int>>
    {
        private readonly IRegionRepositoryAsync _regionRepository;
        private readonly ICountryRepositoryAsync _countryRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;

        public UpdateRegionCommandHandler(IRegionRepositoryAsync regionRepository, ICountryRepositoryAsync countryRepository, IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _regionRepository = regionRepository;
            _countryRepository = countryRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(UpdateRegionCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var region = (await _regionRepository.FindByCondition(x => x.Id == request.Id ).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (region == null)
                {
                    throw new ApiException($"Region Not Found.");
                }
                else
                {
                    var country = (await _countryRepository.FindByCondition(x => x.Id == request.CountryId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                    region.Country = country;
                    region.Name_TH = request.Name_TH;
                    region.Name_ENG = request.Name_ENG;
                    region.Country.Id = request.CountryId;
                    region.Sequence = request.Sequence;
                    region.Active = request.Active;
                    region.Modified = DateTime.UtcNow;

                    await _regionRepository.UpdateAsync(region);
                    var log = new CreateAppLog(_applicationLogRepository);
                    log.Create("Region", "Region", "Edit", Newtonsoft.Json.JsonConvert.SerializeObject(region));
                    return new Response<int>(region.Id);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
