﻿using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Interfaces.Services;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.Regions.Backoffice.Commands
{
    public partial class CreateRegionCommand : IRequest<Response<int>>
    { 
        public int CountryId { get; set; }
        public string Name_TH { get; set; }
        public string Name_ENG { get; set; }
        public int Sequence { get; set; }
        public bool Active { get; set; }

    }
    public class CreateRegionCommandHandler : IRequestHandler<CreateRegionCommand, Response<int>>
    {
        private readonly IRegionRepositoryAsync _regionRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly ICountryRepositoryAsync _countryRepository;
        private readonly IMapper _mapper;
        public CreateRegionCommandHandler(IRegionRepositoryAsync regionRepository, ICountryRepositoryAsync countryRepository, IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _regionRepository = regionRepository;
            _countryRepository = countryRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(CreateRegionCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var country = (await _countryRepository.FindByCondition(x => x.Id == request.CountryId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (country == null)
                {
                    throw new ApiException($"Country Not Found.");
                }
                else
                {
                    var region = _mapper.Map<Region>(request);
                    region.Country = country;
                    region.Created = DateTime.UtcNow;
                    region.Modified = DateTime.UtcNow;
                    //country.CreatedBy = "xxxxxxx";
                    //country.ModifiedBy = "xxxxxxx";
                    var regionObject = await _regionRepository.AddAsync(region);
                    var log = new CreateAppLog(_applicationLogRepository);
                    log.Create("Region", "Region", "Create", Newtonsoft.Json.JsonConvert.SerializeObject(region));
                    return new Response<int>(regionObject.Id);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
