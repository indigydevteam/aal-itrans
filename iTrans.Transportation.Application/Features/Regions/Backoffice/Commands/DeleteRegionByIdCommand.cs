﻿using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.Regions.Backoffice.Commands
{
    public class DeleteRegionByIdCommand : IRequest<Response<int>>
    {
        public int Id { get; set; }
        public class DeleteRegionByIdCommandHandler : IRequestHandler<DeleteRegionByIdCommand, Response<int>>
        {
            private readonly IRegionRepositoryAsync _regionRepository;
            public DeleteRegionByIdCommandHandler(IRegionRepositoryAsync regionRepository)
            {
                _regionRepository = regionRepository;
            }
            public async Task<Response<int>> Handle(DeleteRegionByIdCommand command, CancellationToken cancellationToken)
            {
                var region = (await _regionRepository.FindByCondition(x => x.Id == command.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (region == null)
                {
                    throw new ApiException($"Region Not Found.");
                }
                else
                {
                    await _regionRepository.DeleteAsync(region);
                    return new Response<int>(region.Id);
                }
            }
        }
    }
}
