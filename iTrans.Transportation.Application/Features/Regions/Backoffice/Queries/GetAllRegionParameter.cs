﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.Region.Backoffice;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;

namespace iTrans.Transportation.Application.Features.Regions.Backoffice.Queries
{
    public class GetAllRegionParameter : IRequest<PagedResponse<IEnumerable<RegionViewModel>>>
    {
        public string Search { set; get; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
    }
    public class GetAllRegionParameterHandler : IRequestHandler<GetAllRegionParameter, PagedResponse<IEnumerable<RegionViewModel>>>
    {
        private readonly IRegionRepositoryAsync _regionRepository;
        private readonly IMapper _mapper;
        private Expression<Func<Region, bool>> expression;
        public GetAllRegionParameterHandler(IRegionRepositoryAsync regionRepository, IMapper mapper)
        {
            _regionRepository = regionRepository;
            _mapper = mapper;
        }

        public async Task<PagedResponse<IEnumerable<RegionViewModel>>> Handle(GetAllRegionParameter request, CancellationToken cancellationToken)
        {
            var validFilter = _mapper.Map<GetAllRegionParameter>(request);
            if (validFilter.Search != null && validFilter.Search.Trim() != "")
            {
                expression = x => (x.Name_TH.Contains(validFilter.Search.Trim()) || x.Name_ENG.Contains(validFilter.Search.Trim()));
            }
            int itemCount = _regionRepository.GetItemCount(expression);
            //Expression<Func<Region, bool>> expression = x => x.Name_TH != "";
            var region = await _regionRepository.FindByConditionWithPage(expression, validFilter.PageNumber, validFilter.PageSize);
            var regionViewModel = _mapper.Map<IEnumerable<RegionViewModel>>(region);
            return new PagedResponse<IEnumerable<RegionViewModel>>(regionViewModel, validFilter.PageNumber, validFilter.PageSize);
        }
    }
}
