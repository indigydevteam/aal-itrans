﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.Region.Backoffice;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.Regions.Backoffice.Queries
{
    public class BackofficeGetRegionByIdQuery : IRequest<Response<RegionViewModel>>
    {
        public int Id { get; set; }
    }
    public class BackofficeGetRegionByIdQueryHandler : IRequestHandler<BackofficeGetRegionByIdQuery, Response<RegionViewModel>>
    {
        private readonly IRegionRepositoryAsync _regionRepository;
        private readonly IMapper _mapper;
        public BackofficeGetRegionByIdQueryHandler(IRegionRepositoryAsync regionRepository, IMapper mapper)
        {
            _regionRepository = regionRepository;
            _mapper = mapper;
        }
        public async Task<Response<RegionViewModel>> Handle(BackofficeGetRegionByIdQuery request, CancellationToken cancellationToken)
        {
            var regionObject = (await _regionRepository.FindByCondition(x => x.Id.Equals(request.Id)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            return new Response<RegionViewModel>(_mapper.Map<RegionViewModel>(regionObject));
        }
    }
}
