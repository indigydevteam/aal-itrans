﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.Region;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.Regions.Queries
{
    public class GetRegionByCountryQuery : IRequest<Response<IEnumerable<RegionViewModel>>>
    {
        public int CountryId { get; set; }
    }
    public class GetRegionByCountryQueryHandler : IRequestHandler<GetRegionByCountryQuery, Response<IEnumerable<RegionViewModel>>>
    {
        private readonly IRegionRepositoryAsync _regionRepository;
        private readonly IMapper _mapper;
        public GetRegionByCountryQueryHandler(IRegionRepositoryAsync regionRepository, IMapper mapper)
        {
            _regionRepository = regionRepository;
            _mapper = mapper;
        }
        public async Task<Response<IEnumerable<RegionViewModel>>> Handle(GetRegionByCountryQuery request, CancellationToken cancellationToken)
        {
            var region = (await _regionRepository.FindByCondition(x => x.Country.Id.Equals(request.CountryId)).ConfigureAwait(false)).AsQueryable().ToList();
            var regionViewModel = _mapper.Map<IEnumerable<RegionViewModel>>(region);
            RegionViewModel allRegion = new RegionViewModel
            {
                Id = 0,
                CountryId = 0,
                Name_TH = "ทั้งหมด",
                Name_ENG = "all",
                Sequence = 0
            };
            regionViewModel = regionViewModel.Append(allRegion);
            return new Response<IEnumerable<RegionViewModel>>(regionViewModel.OrderBy(x => x.Sequence));
        }
    }
}
