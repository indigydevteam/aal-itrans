﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using iTrans.Transportation.Application.Interfaces.Repositories;
using FluentValidation;

namespace iTrans.Transportation.Application.Features.Regions.Queries
{
    public class GetRegionByIdQueryValidator : AbstractValidator<GetRegionByIdQuery>
    {
        private readonly IRegionRepositoryAsync regionRepository;

        public GetRegionByIdQueryValidator(IRegionRepositoryAsync regionRepository)
        {
            this.regionRepository = regionRepository;
            RuleFor(p => p.Id)
               .NotEmpty().WithMessage("{PropertyName} is required.")
               .NotNull()
               .MustAsync(IsRegionExists).WithMessage("{PropertyName} not exists.");
        }

        private async Task<bool> IsRegionExists(int regionId, CancellationToken cancellationToken)
        {
            var regionObject = (await regionRepository.FindByCondition(x => x.Id.Equals(regionId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            if (regionObject != null)
            {
                return true;
            }
            return false;
        }
    }
}
