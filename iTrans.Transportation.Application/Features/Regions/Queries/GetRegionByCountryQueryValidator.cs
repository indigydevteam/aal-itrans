﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using iTrans.Transportation.Application.Interfaces.Repositories;
using FluentValidation;

namespace iTrans.Transportation.Application.Features.Regions.Queries
{
    public class GetRegionByCountryQueryValidator : AbstractValidator<GetRegionByCountryQuery>
    {
        private readonly IRegionRepositoryAsync regionRepository;

        public GetRegionByCountryQueryValidator(IRegionRepositoryAsync regionRepository)
        {
            this.regionRepository = regionRepository;
            RuleFor(p => p.CountryId)
               .NotEmpty().WithMessage("{PropertyName} is required.")
               .NotNull()
               .MustAsync(IsCountryExists).WithMessage("{PropertyName} not exists.");
        }

        private async Task<bool> IsCountryExists(int countryId, CancellationToken cancellationToken)
        {
            var regionObject = (await regionRepository.FindByCondition(x => x.Country.Id.Equals(countryId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            if (regionObject != null)
            {
                return true;
            }
            return false;
        }
    }
}
