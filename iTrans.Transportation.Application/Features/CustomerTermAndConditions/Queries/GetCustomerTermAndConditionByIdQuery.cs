﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.CustomerTermAndCondition;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.CustomerTermAndConditions.Queries
{
    public class GetCustomerTermAndConditionByIdQuery : IRequest<Response<CustomerTermAndConditionViewModel>>
    {
        public Guid CustomerId { get; set; }
        public int Id { get; set; }
    }
    public class GetCustomerTermAndConditionByIdQueryHandler : IRequestHandler<GetCustomerTermAndConditionByIdQuery, Response<CustomerTermAndConditionViewModel>>
    {
        private readonly ICustomerTermAndConditionRepositoryAsync _CustomerTermAndConditionRepository;
        private readonly IMapper _mapper;
        public GetCustomerTermAndConditionByIdQueryHandler(ICustomerTermAndConditionRepositoryAsync CustomerTermAndConditionRepository, IMapper mapper)
        {
            _CustomerTermAndConditionRepository = CustomerTermAndConditionRepository;
            _mapper = mapper;
        }
        public async Task<Response<CustomerTermAndConditionViewModel>> Handle(GetCustomerTermAndConditionByIdQuery request, CancellationToken cancellationToken)
        {
            var CustomerObject = (await _CustomerTermAndConditionRepository.FindByCondition(x => x.Id.Equals(request.Id) && x.Customer.Id == request.CustomerId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            return new Response<CustomerTermAndConditionViewModel>(_mapper.Map<CustomerTermAndConditionViewModel>(CustomerObject));
        }
    }
}
