﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.CustomerTermAndCondition;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.CustomerTermAndConditions.Queries
{
    public class GetAllCustomerTermAndConditionQuery : IRequest<Response<IEnumerable<CustomerTermAndConditionViewModel>>>
    {

    }
    public class GetAllCustomerTermAndConditionQueryHandler : IRequestHandler<GetAllCustomerTermAndConditionQuery, Response<IEnumerable<CustomerTermAndConditionViewModel>>>
    {
        private readonly ICustomerTermAndConditionRepositoryAsync _CustomerTermAndConditionRepository;
        private readonly IMapper _mapper;
        public GetAllCustomerTermAndConditionQueryHandler(ICustomerTermAndConditionRepositoryAsync CustomerTermAndConditionRepository, IMapper mapper)
        {
            _CustomerTermAndConditionRepository = CustomerTermAndConditionRepository;
            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<CustomerTermAndConditionViewModel>>> Handle(GetAllCustomerTermAndConditionQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var validFilter = _mapper.Map<GetAllCustomerTermAndConditionQuery>(request);
                var Customer = await _CustomerTermAndConditionRepository.GetAllAsync();
                var CustomerTermAndConditionViewModel = _mapper.Map<IEnumerable<CustomerTermAndConditionViewModel>>(Customer);
                return new Response<IEnumerable<CustomerTermAndConditionViewModel>>(CustomerTermAndConditionViewModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
