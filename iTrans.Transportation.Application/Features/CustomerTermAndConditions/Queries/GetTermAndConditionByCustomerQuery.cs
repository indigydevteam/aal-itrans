﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.CustomerTermAndCondition;
using iTrans.Transportation.Application.DTOs.TermAndCondition;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using iTrans.Transportation.Domain.Entities;
using MediatR;

namespace iTrans.Transportation.Application.Features.CustomerTermAndConditions.Queries
{
    public class GetTermAndConditionByCustomerQuery : IRequest<Response<IEnumerable<CustomerTermAndConditionViewModel>>>
    {
        public Guid CustomerId { get; set; }
    }
    public class GetTermAndConditionByCustomerQueryHandler : IRequestHandler<GetTermAndConditionByCustomerQuery, Response<IEnumerable<CustomerTermAndConditionViewModel>>>
    {
        private readonly ICustomerTermAndConditionRepositoryAsync _customerTermAndConditionRepository;
        private readonly ITermAndConditionRepositoryAsync _termAndConditionRepository;
        private readonly IMapper _mapper;
        public GetTermAndConditionByCustomerQueryHandler(ICustomerTermAndConditionRepositoryAsync customerTermAndConditionRepository, ITermAndConditionRepositoryAsync termAndConditionRepository, IMapper mapper)
        {
            _customerTermAndConditionRepository = customerTermAndConditionRepository;
            _termAndConditionRepository = termAndConditionRepository;
            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<CustomerTermAndConditionViewModel>>> Handle(GetTermAndConditionByCustomerQuery request, CancellationToken cancellationToken)
        {
            try
            {

                //var data = (await _termAndConditionRepository.FindByCondition(x => x.Active == true && x.Module.Trim().ToLower() == "customer").ConfigureAwait(false)).AsQueryable().ToList().OrderBy(x => x.Sequence).OrderBy(x => x.Version).OrderBy(x => x.Modified);
                //List<TermAndCondition> termAndConditions = new List<TermAndCondition>();
                //foreach (TermAndCondition termAndCondition in data)
                //{
                //    var existItem = termAndConditions.Where(x => x.Section == termAndCondition.Section).FirstOrDefault();
                //    if (existItem == null)
                //    {
                //        termAndConditions.Add(termAndCondition);
                //    }
                //    else
                //    {
                //        int n;
                //        var version1 = new Version(int.TryParse(termAndCondition.Version, out n) ? termAndCondition.Version + ".0" : termAndCondition.Version);
                //        var version2 = new Version(int.TryParse(existItem.Version, out n) ? existItem.Version + ".0" : existItem.Version);
                //        var result = version1.CompareTo(version2);
                //        if (result > 0)
                //        {
                //            termAndConditions.Remove(existItem);
                //            termAndConditions.Add(termAndCondition);
                //        }
                //    }
                //};
                //List<TermAndCondition> nonAcceptAermAndConditions = new List<TermAndCondition>();
                //List<CustomerTermAndCondition> customerTermAndConditions = new List<CustomerTermAndCondition>();
                //foreach (TermAndCondition item in termAndConditions)
                //{
                //    var customerTermAndCondition = (await _customerTermAndConditionRepository.FindByCondition(x => x.Customer.Id.Equals(request.CustomerId) && x.Section == item.Section, x => x.Created, true)
                //        .ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                //    if (customerTermAndCondition != null)
                //    {
                //        customerTermAndConditions.Add(customerTermAndCondition);
                //        if (!customerTermAndCondition.Version.Contains("."))
                //        {
                //            customerTermAndCondition.Version = customerTermAndCondition.Version + ".0";
                //        }
                //        if (!item.Version.Contains("."))
                //        {
                //            item.Version = item.Version + ".0";
                //        }
                //        Version customerTermAndConditionVersion = new Version(customerTermAndCondition.Version);
                //        Version TermAndConditionVersion = new Version(item.Version);
                //        if (new Version(customerTermAndCondition.Version) < new Version(item.Version))
                //        {
                //            nonAcceptAermAndConditions.Add(item);
                //        }
                //        else if (new Version(customerTermAndCondition.Version) == new Version(item.Version) && !customerTermAndCondition.IsUserAccept)
                //        {
                //            nonAcceptAermAndConditions.Add(item);
                //        }
                //    }
                //    else
                //    {
                //        nonAcceptAermAndConditions.Add(item);
                //    }
                //}
                //AcceptTermAndConditionViewModel resultTermAndCondition = new AcceptTermAndConditionViewModel();
                //resultTermAndCondition.termAndCondition = _mapper.Map<List<CustomerTermAndConditionViewModel>>(customerTermAndConditions);
                //resultTermAndCondition.nonAcceptTermAndCondition = _mapper.Map<List<TermAndConditionViewModel>>(nonAcceptAermAndConditions);
                //var customerTermAndConditionViewModel = _mapper.Map<IEnumerable<CustomerTermAndConditionViewModel>>(customerTermAndConditions);
                //return new Response<AcceptTermAndConditionViewModel>(resultTermAndCondition);

                var data = (await _termAndConditionRepository.FindByCondition(x => x.Active == true && x.Module.Trim().ToLower() == "customer").ConfigureAwait(false)).AsQueryable().ToList().OrderBy(x => x.Sequence).OrderBy(x => x.Version).OrderBy(x => x.Modified);
                List<TermAndCondition> termAndConditions = new List<TermAndCondition>();
                foreach (TermAndCondition termAndCondition in data)
                {
                    var existItem = termAndConditions.Where(x => x.Section == termAndCondition.Section).FirstOrDefault();
                    if (existItem == null)
                    {
                        termAndConditions.Add(termAndCondition);
                    }
                    else
                    {
                        int n;
                        var version1 = new Version(int.TryParse(termAndCondition.Version, out n) ? termAndCondition.Version + ".0" : termAndCondition.Version);
                        var version2 = new Version(int.TryParse(existItem.Version, out n) ? existItem.Version + ".0" : existItem.Version);
                        var result = version1.CompareTo(version2);
                        if (result > 0)
                        {
                            termAndConditions.Remove(existItem);
                            termAndConditions.Add(termAndCondition);
                        }
                    }
                };

                List<CustomerTermAndConditionViewModel> customerTermAndConditions = new List<CustomerTermAndConditionViewModel>();
                foreach (TermAndCondition item in termAndConditions)
                {
                    var customerTermAndCondition = (await _customerTermAndConditionRepository.FindByCondition(x => x.Customer.Id.Equals(request.CustomerId) && x.Section == item.Section && x.Version == item.Version)
                        .ConfigureAwait(false)).OrderByDescending(x => x.Created).AsQueryable().FirstOrDefault();
                    if(customerTermAndCondition == null)
                    {
                        CustomerTermAndConditionViewModel itemTermAndCondition = new CustomerTermAndConditionViewModel()
                        {
                            id =  0,
                            termAndConditionId = item.Id,
                            name_TH = item.Name_TH,
                            name_ENG = item.Name_ENG,
                            section = item.Section,
                            version = item.Version,
                            isAccept = item.IsAccept,
                            isUserAccept = false,
                            isComplete = false
                        };
                        customerTermAndConditions.Add(itemTermAndCondition);
                    }
                    else
                    {
                        var itemTermAndCondition = _mapper.Map<CustomerTermAndConditionViewModel>(customerTermAndCondition);
                        itemTermAndCondition.isComplete = true;
                        customerTermAndConditions.Add(itemTermAndCondition);
                    }
                }
                return new Response<IEnumerable<CustomerTermAndConditionViewModel>>(customerTermAndConditions);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
