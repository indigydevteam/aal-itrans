﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.CustomerTermAndCondition;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using iTrans.Transportation.Domain.Entities;
using MediatR;

namespace iTrans.Transportation.Application.Features.CustomerTermAndConditions.Queries
{
    public class GetAllCustomerTermAndConditionParameter : IRequest<PagedResponse<IEnumerable<CustomerTermAndConditionViewModel>>>
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
    }
    public class GetAllCustomerTermAndConditionParameterHandler : IRequestHandler<GetAllCustomerTermAndConditionParameter, PagedResponse<IEnumerable<CustomerTermAndConditionViewModel>>>
    {
        private readonly ICustomerTermAndConditionRepositoryAsync _CustomerTermAndConditionRepository;
        private readonly IMapper _mapper;
        public GetAllCustomerTermAndConditionParameterHandler(ICustomerTermAndConditionRepositoryAsync CustomerTermAndConditionRepository, IMapper mapper)
        {
            _CustomerTermAndConditionRepository = CustomerTermAndConditionRepository;
            _mapper = mapper;
        }

        public async Task<PagedResponse<IEnumerable<CustomerTermAndConditionViewModel>>> Handle(GetAllCustomerTermAndConditionParameter request, CancellationToken cancellationToken)
        {
            var validFilter = _mapper.Map<GetAllCustomerTermAndConditionParameter>(request);
            Expression<Func<CustomerTermAndCondition, bool>> expression = x => x.Customer.Id != null;
            var CustomerTermAndCondition = await _CustomerTermAndConditionRepository.FindByConditionWithPage(expression, validFilter.PageNumber, validFilter.PageSize);
            var CustomerTermAndConditionViewModel = _mapper.Map<IEnumerable<CustomerTermAndConditionViewModel>>(CustomerTermAndCondition);
            return new PagedResponse<IEnumerable<CustomerTermAndConditionViewModel>>(CustomerTermAndConditionViewModel, validFilter.PageNumber, validFilter.PageSize);
        }
    }
}
