﻿using AutoMapper;
using iTrans.Transportation.Application.DTOs.CustomerTermAndCondition;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using iTrans.Transportation.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using iTrans.Transportation.Application.DTOs.Registers;

namespace iTrans.Transportation.Application.Features.CustomerTermAndConditions.Commands
{
    public partial class CreateCustomerTermAndConditionCommand : IRequest<Response<Guid>>
    {
        public Guid CustomerId { get; set; }
        public List<CreateTermAndConditionViewModel> TermAndConditions { get; set; }
    }
    public class CreateCustomerTermAndConditionCommandHandler : IRequestHandler<CreateCustomerTermAndConditionCommand, Response<Guid>>
    {
        private readonly ICustomerRepositoryAsync _customerRepository;
        private readonly ICustomerTermAndConditionRepositoryAsync _customerTermAndConditionRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;
        public CreateCustomerTermAndConditionCommandHandler(ICustomerRepositoryAsync customerRepository, ICustomerTermAndConditionRepositoryAsync customerTermAndConditionRepository, IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _customerTermAndConditionRepository = customerTermAndConditionRepository;
            _customerRepository = customerRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
        }

        public async Task<Response<Guid>> Handle(CreateCustomerTermAndConditionCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var customerObject = (await _customerRepository.FindByCondition(x => x.Id.Equals(request.CustomerId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (customerObject == null)
                {
                    throw new ApiException($"Customer Not Found.");
                }
                if (request.TermAndConditions != null)
                {
                    foreach (CreateTermAndConditionViewModel termAndConditionViewModel in request.TermAndConditions)
                    {
                        CustomerTermAndCondition termAndCondition = new CustomerTermAndCondition
                        {
                            Customer = customerObject,
                            TermAndConditionId = termAndConditionViewModel.TermAndConditionId > 0 ? termAndConditionViewModel.TermAndConditionId : termAndConditionViewModel.Id,
                            Name_TH = termAndConditionViewModel.Name_TH,
                            Name_ENG = termAndConditionViewModel.Name_ENG,
                            Section = termAndConditionViewModel.Section,
                            Version = termAndConditionViewModel.Version,
                            IsAccept = termAndConditionViewModel.IsAccept,
                            IsUserAccept = termAndConditionViewModel.IsUserAccept,
                            CreatedBy = request.CustomerId != null ? request.CustomerId.ToString() : "-",
                            Created = DateTime.Now,
                            ModifiedBy = request.CustomerId != null ? request.CustomerId.ToString() : "-",
                            Modified = DateTime.Now
                        };
                     var  dataListObject =  await _customerTermAndConditionRepository.AddAsync(termAndCondition);

                    }
                    var log = new CreateAppLog(_applicationLogRepository);
                    log.Create("Customer", "Customer TermAndConditions", "Create", Newtonsoft.Json.JsonConvert.SerializeObject(request));
                    return new Response<Guid>(customerObject.Id);
                }
                else
                {
                    throw new ApiException($"TermAndConditions Not null.");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}