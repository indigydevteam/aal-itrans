﻿using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.CustomerTermAndConditions.Commands
{
    public class CancelCustomerTermAndConditionCommand : IRequest<Response<bool>>
    {
        public Guid CustomerId { get; set; }
        public string Section { get; set; }
        public string Version { get; set; }

        public class CancelCustomerTermAndConditionCommandHandler : IRequestHandler<CancelCustomerTermAndConditionCommand, Response<bool>>
        {
            private readonly ICustomerTermAndConditionRepositoryAsync _customerTermAndConditionRepository;
            public CancelCustomerTermAndConditionCommandHandler(ICustomerTermAndConditionRepositoryAsync customerTermAndConditionRepository)
            {
                _customerTermAndConditionRepository = customerTermAndConditionRepository;
            }
            public async Task<Response<bool>> Handle(CancelCustomerTermAndConditionCommand command, CancellationToken cancellationToken)
            {

                (await _customerTermAndConditionRepository.CreateSQLQuery("DELETE Customer_TermAndCondition where CustomerId = '" + command.CustomerId.ToString() + "' and Section = '"+ command.Section + "' and Version = '" + command.Version + "'").ConfigureAwait(false)).ExecuteUpdate(); ;
                return new Response<bool>(true);

            }
        }
    }
}
