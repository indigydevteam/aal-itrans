﻿using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.CustomerTermAndConditions.Commands
{
    public class DeleteCustomerTermAndConditionCommand : IRequest<Response<int>>
    {
        public int Id { get; set; }

        public class DeleteCustomerTermAndConditionCommandHandler : IRequestHandler<DeleteCustomerTermAndConditionCommand, Response<int>>
        {
            private readonly ICustomerTermAndConditionRepositoryAsync _CustomerTermAndConditionRepository;
            public DeleteCustomerTermAndConditionCommandHandler(ICustomerTermAndConditionRepositoryAsync CustomerTermAndConditionRepository)
            {
                _CustomerTermAndConditionRepository = CustomerTermAndConditionRepository;
            }
            public async Task<Response<int>> Handle(DeleteCustomerTermAndConditionCommand command, CancellationToken cancellationToken)
            {
                var CustomerTermAndCondition = (await _CustomerTermAndConditionRepository.FindByCondition(x => x.Id == command.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (CustomerTermAndCondition == null)
                {
                    throw new ApiException($"CustomerTermAndCondition Not Found.");
                }
                else
                {
                    await _CustomerTermAndConditionRepository.DeleteAsync(CustomerTermAndCondition);
                    return new Response<int>(CustomerTermAndCondition.Id);
                }
            }
        }
    }
}
