﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;

namespace iTrans.Transportation.Application.Features.CustomerTermAndConditions.Commands
{
    public class UpdateCustomerTermAndConditionCommand : IRequest<Response<int>>
    {
        public int Id { get; set; }
        public Guid CustomerId { get; set; }
        
    }

    public class UpdateCustomerTermAndConditionCommandHandler : IRequestHandler<UpdateCustomerTermAndConditionCommand, Response<int>>
    {
        private readonly ICustomerTermAndConditionRepositoryAsync _CustomerTermAndConditionRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;

        public UpdateCustomerTermAndConditionCommandHandler(ICustomerTermAndConditionRepositoryAsync CustomerTermAndConditionRepository, IMapper mapper)
        {
            _CustomerTermAndConditionRepository = CustomerTermAndConditionRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(UpdateCustomerTermAndConditionCommand request, CancellationToken cancellationToken)
        {
            var CustomerTermAndCondition = (await _CustomerTermAndConditionRepository.FindByCondition(x => x.Id == request.Id && x.Customer.Id.Equals(request.CustomerId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            if (CustomerTermAndCondition == null)
            {
                throw new ApiException($"CustomerTermAndCondition Not Found.");
            }
            else
            {

                //CustomerTermAndCondition.CustomerId = request.CustomerId;
                //CustomerTermAndCondition.TermAndCondition = request.TermAndCondition;
                CustomerTermAndCondition.Modified = DateTime.UtcNow;

                await _CustomerTermAndConditionRepository.UpdateAsync(CustomerTermAndCondition);
                var log = new CreateAppLog(_applicationLogRepository);
                log.Create("Customer", "Customer TermAndConditions", "Edit", Newtonsoft.Json.JsonConvert.SerializeObject(request));
                return new Response<int>(CustomerTermAndCondition.Id);
            }
        }
    }
}
