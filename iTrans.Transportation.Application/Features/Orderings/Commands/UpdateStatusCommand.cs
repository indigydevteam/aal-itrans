﻿using AutoMapper;
using iTrans.Transportation.Application.DTOs.CarList;
using iTrans.Transportation.Application.DTOs.CarType;
using iTrans.Transportation.Application.DTOs.Ordering;
using iTrans.Transportation.Application.DTOs.OrderingAddress;
using iTrans.Transportation.Application.DTOs.OrderingAddressProduct;
using iTrans.Transportation.Application.DTOs.OrderingProduct;
using iTrans.Transportation.Application.DTOs.OrderingStatus;
using iTrans.Transportation.Application.DTOs.ProductPackaging;
using iTrans.Transportation.Application.DTOs.ProductType;
using iTrans.Transportation.Application.Enums;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Helper;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;
using Microsoft.Extensions.Configuration;
using NHibernate.Engine;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;


namespace iTrans.Transportation.Application.Features.Orderings.Commands
{
    public class UpdateStatusCommand : IRequest<Response<Guid>>
    {
        public Guid UserId { get; set; }
        public Guid OrderingId { get; set; }
        public int AddressId { get; set; }
        public Guid CustomerId { get; set; }
        public Guid DriverId { get; set; }
        public int Status { get; set; }
        public int CancelStatus { get; set; }
        public decimal Compensation { get; set; }
        public string APP_ID { get; set; }
        public string REST_API_KEY { get; set; }
        public string AUTH_ID { get; set; }
        public string DRIVER_APP_ID { get; set; }
        public string DRIVER_REST_API_KEY { get; set; }
        public string DRIVER_AUTH_ID { get; set; }
        public string UserRole { get; set; }
    }
    public class UpdateStatusCommandHandler : IRequestHandler<UpdateStatusCommand, Response<Guid>>
    {
        private readonly IOrderingRepositoryAsync _orderingRepository;
        private readonly IOrderingPaymentRepositoryAsync _orderingPaymentRepository;
        private readonly ICustomerRepositoryAsync _customerRepository;
        private readonly IDriverRepositoryAsync _driverRepository;
        private readonly ICustomerActivityRepositoryAsync _customerActivityRepository;
        private readonly IDriverActivityRepositoryAsync _driverActivityRepository;
        private readonly IOrderingAddressRepositoryAsync _orderingAddressRepository;
        private readonly IDriverAnnouncementRepositoryAsync _driverAnnouncementRepository;
        private readonly IOrderingCancelStatusRepositoryAsync _orderingCancelStatusRepository;
        private readonly INotificationRepositoryAsync _notificationRepository;
        private readonly INotificationUserRepositoryAsync _notificationUserRepository;
        private readonly IProductTypeRepositoryAsync _productTypeRepository;
        private readonly IProductPackagingRepositoryAsync _productPackagingRepository;
        private readonly IOrderingHistoryRepositoryAsync _orderingHistoryRepository;
        private readonly ICustomerInboxRepositoryAsync _customerInboxRepository;
        private readonly IDriverInboxRepositoryAsync _driverInboxRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly ICustomerPaymentRepositoryAsync _customerPaymentRepository;
        private readonly IDriverPaymentRepositoryAsync _driverPaymentRepository;
        private readonly ILoginErrorLogRepositoryAsync _loginErrorLogRepository;
        private readonly IConfiguration _configuration;

        private readonly IMapper _mapper;

        public UpdateStatusCommandHandler(IOrderingRepositoryAsync orderingRepository, IOrderingCancelStatusRepositoryAsync orderingCancelStatusRepository
            , ICustomerRepositoryAsync customerRepository, IDriverRepositoryAsync driverRepository
            , INotificationRepositoryAsync notificationRepository, INotificationUserRepositoryAsync notificationUserRepository
            , IProductTypeRepositoryAsync productTypeRepository, IProductPackagingRepositoryAsync productPackagingRepository
            , IDriverAnnouncementRepositoryAsync driverAnnouncementRepository, IOrderingAddressRepositoryAsync orderingAddressRepository
            , IOrderingHistoryRepositoryAsync orderingHistoryRepository, ICustomerPaymentRepositoryAsync customerPaymentRepository, IDriverPaymentRepositoryAsync driverPaymentRepository
            , ICustomerInboxRepositoryAsync customerInboxRepository, IDriverInboxRepositoryAsync driverInboxRepository, IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper
            , ICustomerActivityRepositoryAsync customerActivityRepository, IDriverActivityRepositoryAsync driverActivityRepository
            , ILoginErrorLogRepositoryAsync loginErrorLogRepository, IConfiguration configuration, IOrderingPaymentRepositoryAsync orderingPaymentRepository
            )
        {
            _orderingRepository = orderingRepository;
            _orderingAddressRepository = orderingAddressRepository;
            _orderingCancelStatusRepository = orderingCancelStatusRepository;
            _driverAnnouncementRepository = driverAnnouncementRepository;
            _notificationRepository = notificationRepository;
            _notificationUserRepository = notificationUserRepository;
            _productTypeRepository = productTypeRepository;
            _productPackagingRepository = productPackagingRepository;
            _orderingHistoryRepository = orderingHistoryRepository;
            _customerRepository = customerRepository;
            _driverRepository = driverRepository;
            _customerActivityRepository = customerActivityRepository;
            _driverActivityRepository = driverActivityRepository;
            _customerInboxRepository = customerInboxRepository;
            _driverInboxRepository = driverInboxRepository;
            _applicationLogRepository = applicationLogRepository;
            _customerPaymentRepository = customerPaymentRepository;
            _driverPaymentRepository = driverPaymentRepository;
            _loginErrorLogRepository = loginErrorLogRepository;
            _orderingPaymentRepository = orderingPaymentRepository;
            _configuration = configuration;
            _mapper = mapper;
        }

        public async Task<Response<Guid>> Handle(UpdateStatusCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var data = (await _orderingRepository.FindByCondition(x => x.Id == request.OrderingId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                //var address = (await _orderingAddressRepository.FindByCondition(x => x.Id == request.AddressId && x.Ordering.Id == request.OrderingId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (data == null)
                {
                    throw new ApiException($"Ordering Not Found.");
                }
                else
                {

                    var existDriver = (await _driverRepository.FindByCondition(x => x.Id == request.DriverId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                    if (existDriver != null)
                    {
                        if (existDriver.Level == 1 && request.Status == 2)
                        {
                            var driverActivityRepository = (await _driverActivityRepository.FindByCondition(x => x.DriverId == data.Driver.Id && x.Date.Contains("yyyy'-'MM'")).ConfigureAwait(false)).AsQueryable().ToList();
                            int acceptjobCount = 0;

                            foreach (DriverActivity item in driverActivityRepository)
                            {
                                acceptjobCount = acceptjobCount + item.AcceptJob + item.Cancel;
                            }
                            if (data.OrderingDesiredPrice > 2000)
                            {
                                throw new ApiException("You can't get a job priced above 2000.");
                            }
                            if (acceptjobCount > 4)
                            {
                                throw new ApiException("You can't get more than 4 jobs per month.");
                            }
                        }
                    }



                    var address = data.Addresses.Where(x => x.Id == request.AddressId).FirstOrDefault();

                    string orderStatusPath = $"Shared\\orderingstatus.json";
                    var orderingStatusPathFile = Path.Combine(Directory.GetCurrentDirectory(), orderStatusPath);

                    var statusJson = System.IO.File.ReadAllText(orderingStatusPathFile);
                    List<OrderingStatusViewModel> orderingStatus = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OrderingStatusViewModel>>(statusJson);

                    OrderingHistory orderingHistory = new OrderingHistory();

                    if (request.CustomerId != null && request.CustomerId != new Guid() && data.Customer.Id != request.CustomerId)
                        throw new ApiException($"Ordering Not Found.");
                    if (request.DriverId != null && request.DriverId != new Guid() && data.Driver.Id != request.DriverId)
                        throw new ApiException($"Ordering Not Found.");

                    var customerOrderingStatusFile = Path.Combine(Directory.GetCurrentDirectory(), $"Shared\\customer_orderingStatus.json");
                    var customerOrderingStatusJson = System.IO.File.ReadAllText(customerOrderingStatusFile);
                    List<OrderingStatusViewModel> customerOrderingStatus = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OrderingStatusViewModel>>(customerOrderingStatusJson);

                    var driverOrderingStatusFile = Path.Combine(Directory.GetCurrentDirectory(), $"Shared\\driver_orderingStatus.json");
                    var driverOrderingStatusJson = System.IO.File.ReadAllText(customerOrderingStatusFile);
                    List<OrderingStatusViewModel> driverOrderingStatus = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OrderingStatusViewModel>>(driverOrderingStatusJson);

                    var userId = request.CustomerId;
                    var ownerId = request.CustomerId;

                    var APP_ID = request.APP_ID;
                    var REST_API_KEY = request.REST_API_KEY;
                    var AUTH_ID = request.AUTH_ID;
                    if (request.CustomerId != null && request.CustomerId != new Guid())
                    {
                        userId = data.Driver.Id;
                        ownerId = request.CustomerId;
                        APP_ID = request.DRIVER_APP_ID;
                        REST_API_KEY = request.DRIVER_REST_API_KEY;
                        AUTH_ID = request.DRIVER_AUTH_ID;
                    }

                    var productTypes = (await _productTypeRepository.FindByCondition(x => x.Active.Equals(true)).ConfigureAwait(false)).AsQueryable().ToList();
                    var packagings = (await _productPackagingRepository.FindByCondition(x => x.Active.Equals(true)).ConfigureAwait(false)).AsQueryable().ToList();

                    int previousStatus = data.Status;
                    data.Status = request.Status;
                    data.Modified = DateTime.Now;
                    data.ModifiedBy = request.UserId != null ? request.UserId.ToString() : "-";
                    if (request.Status >= 4 && request.Status <= 12)
                    {
                        data.Status = 0;
                        //foreach (OrderingAddress obj in data.Addresses)
                        //{
                        //    if(obj.Status == 3)
                        //    {
                        //        obj.Status = 0;
                        //    }
                        //}
                        if (address != null)
                        {
                            if (request.Status == 6)
                            {
                                address.DriverWaitingToPickUpDate = DateTime.Now;
                            }
                            if (request.Status == 11)
                            {
                                address.DriverWaitingToDeliveryDate = DateTime.Now;
                            }
                            if ((address.Status == 6 && request.Status == 7) || (address.Status == 11 && request.Status == 12))
                            {
                                //    TimeSpan? periodTime = DateTime.Now - address.Modified;
                                //    var driver = data.Drivers.Where(x => x.DriverId.Equals(request.DriverId)).FirstOrDefault();
                                //    bool isCharter = true;
                                //    address.Compensation = 0;
                                //    address.PeriodTime = periodTime.ToString();
                                //    if (driver != null)
                                //    {
                                //        var car = data.Cars.Where(x => x.OrderingDriver.Id.Equals(driver.Id)).FirstOrDefault();
                                //        if (car != null)
                                //        {
                                //            isCharter = car.IsCharter;
                                //        }
                                //    }
                                //    double totalMinutes = periodTime.Value.TotalMinutes;
                                //    if (data.TransportType == 1)
                                //    {
                                //        if (totalMinutes > 60 && totalMinutes <= 300)
                                //        {
                                //            address.Compensation = data.OrderingPrice * 0.05m;
                                //        }
                                //        else if (totalMinutes > 300)
                                //        {
                                //            address.Compensation = data.OrderingPrice * 0.1m;
                                //        }
                                //    }
                                //    else
                                //    {
                                //        if (totalMinutes > 15 && totalMinutes <= 30)
                                //        {
                                //            address.Compensation = data.OrderingPrice * 0.05m;
                                //        }
                                //        else if (totalMinutes > 15 && totalMinutes <= 45)
                                //        {
                                //            address.Compensation = data.OrderingPrice * 0.05m;
                                //            address.Compensation = address.Compensation + (data.OrderingPrice * 0.05m);
                                //        }
                                //        else if (totalMinutes > 15 && totalMinutes <= 60)
                                //        {
                                //            address.Compensation = data.OrderingPrice * 0.05m;
                                //            address.Compensation = address.Compensation + (data.OrderingPrice * 0.05m);
                                //            address.Compensation = address.Compensation + (data.OrderingPrice * 0.05m);
                                //        }
                                //        else if (totalMinutes > 60)
                                //        {
                                //            address.Compensation = data.OrderingPrice * 0.05m;
                                //            address.Compensation = address.Compensation + (data.OrderingPrice * 0.05m);
                                //            address.Compensation = address.Compensation + (data.OrderingPrice * 0.05m);
                                //            int hourTime = (int)Math.Round((totalMinutes - 60) / 60);
                                //            for (int i = 0; i < hourTime; i++)
                                //            {
                                //                address.Compensation = address.Compensation + (data.OrderingPrice * 0.1m);
                                //            }
                                //        }
                                //    }
                            }

                            address.Status = request.Status;
                            address.Modified = DateTime.Now;
                            address.ModifiedBy = request.UserId != null ? request.UserId.ToString() : "-";
                        }
                    }
                    if (request.Status == 13)
                    {
                        data.Status = 0;
                        var successAddress = data.Addresses.Where(x => x.Status != 13 && x.AddressType == "deliverypoint" && x.Id != request.AddressId).ToList();
                        if (address != null)
                        {
                            address.Status = request.Status;
                            if (successAddress.Count == 0)
                            {
                                data.Status = 13;
                            }
                        }
                    }
                    var cancelStatus = (await _orderingCancelStatusRepository.FindByCondition(x => x.Id.Equals(request.CancelStatus)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                    data.CancelStatus = cancelStatus;

                    //if(data.Status == 15 || data.Status == 16)
                    //{
                    //    data.Customer.RequestCarPerYear = data.Customer.RequestCarPerYear + 1;
                    //    data.Customer.RequestCarPerMonth = data.Customer.RequestCarPerMonth + 1;


                    //    var customerActivity = (await _customerActivityRepository.FindByCondition(x => x.CustomerId.Equals(request.CustomerId) && x.Date == DateTime.Now.ToString("yyyy'-'MM'-'dd")).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                    //    if (customerActivity == null)
                    //    {
                    //        CustomerActivity newCustomerActivity = new CustomerActivity
                    //        {
                    //            CustomerId = request.CustomerId,
                    //            Date = DateTime.Now.ToString("yyyy'-'MM'-'dd"),
                    //            Star = 0,
                    //            RequestCar = 1,
                    //            Cancel = 0,
                    //            OrderingValue = 0,
                    //            Created = DateTime.Now,
                    //            CreatedBy = request.CustomerId != null ? request.CustomerId.ToString() : "-",
                    //            Modified = DateTime.Now,
                    //            ModifiedBy = request.CustomerId != null ? request.CustomerId.ToString() : "-",
                    //        };
                    //        await _customerActivityRepository.AddAsync(newCustomerActivity);
                    //    }
                    //    else
                    //    {
                    //        customerActivity.RequestCar = customerActivity.RequestCar + 1;
                    //        await _customerActivityRepository.UpdateAsync(customerActivity);
                    //    }
                    //}

                    var orderingStatusObject = orderingStatus.Where(o => o.id == data.Status).FirstOrDefault();
                    data.StatusName_Str = orderingStatusObject != null ? orderingStatusObject.th : "-";

                    data.Modified = DateTime.Now;
                    data.ModifiedBy = request.UserId != null ? request.UserId.ToString() : "-";

                    await _orderingRepository.UpdateAsync(data);
                    if (data.Status == 13)
                    {
                        if (data.Customer != null && data.Customer.Id != new Guid())
                        {
                            var customer = (await _customerRepository.FindByCondition(x => x.Id.Equals(data.Customer.Id)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                            customer.RequestCarPerYear = customer.RequestCarPerYear + 1;
                            customer.RequestCarPerMonth = customer.RequestCarPerMonth + 1;
                            customer.OrderValuePerYear = data.Customer.OrderValuePerYear + data.OrderingPrice;
                            customer.OrderValuePerMonth = data.Customer.OrderValuePerMonth + data.OrderingPrice;

                            var customerActivity = (await _customerActivityRepository.FindByCondition(x => x.CustomerId.Equals(data.Customer.Id) && x.Date == DateTime.Now.ToString("yyyy'-'MM'-'dd")).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                            if (customerActivity == null)
                            {
                                CustomerActivity newCustomerActivity = new CustomerActivity
                                {
                                    CustomerId = data.Customer.Id,
                                    Date = DateTime.Now.ToString("yyyy'-'MM'-'dd"),
                                    Star = 0,
                                    RequestCar = 1,
                                    Cancel = 0,
                                    OrderingValue = data.OrderingPrice,
                                    Created = DateTime.Now,
                                    CreatedBy = request.UserId != null ? request.UserId.ToString() : "-",
                                    Modified = DateTime.Now,
                                    ModifiedBy = request.UserId != null ? request.UserId.ToString() : "-",
                                };
                                await _customerActivityRepository.AddAsync(newCustomerActivity);
                            }
                            else
                            {
                                customerActivity.RequestCar = customerActivity.RequestCar + 1;
                                customerActivity.OrderingValue = customerActivity.OrderingValue + data.OrderingPrice;
                                await _customerActivityRepository.UpdateAsync(customerActivity);
                            }
                        }
                        if (data.Driver != null && data.Driver.Id != new Guid())
                        {
                            var driver = (await _driverRepository.FindByCondition(x => x.Id == data.Driver.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                            driver.AcceptJobPerMonth = driver.AcceptJobPerMonth + 1;
                            driver.AcceptJobPerYear = driver.AcceptJobPerYear + 1;
                            var driverActivity = (await _driverActivityRepository.FindByCondition(x => x.DriverId.Equals(data.Driver.Id) && x.Date == DateTime.Now.ToString("yyyy'-'MM'-'dd")).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                            if (driverActivity == null)
                            {
                                DriverActivity newDriverActivity = new DriverActivity
                                {
                                    DriverId = data.Driver.Id,
                                    Date = DateTime.Now.ToString("yyyy'-'MM'-'dd"),
                                    Star = 0,
                                    AcceptJob = 1,
                                    Complaint = 0,
                                    Reject = 0,
                                    Cancel = 0,
                                    InsuranceValue = 0,
                                    Created = DateTime.Now,
                                    CreatedBy = request.UserId != null ? request.UserId.ToString() : "-",
                                    Modified = DateTime.Now,
                                    ModifiedBy = request.UserId != null ? request.UserId.ToString() : "-",
                                };
                                await _driverActivityRepository.AddAsync(newDriverActivity);
                            }
                            else
                            {
                                driverActivity.AcceptJob = driverActivity.AcceptJob + 1;
                                await _driverActivityRepository.UpdateAsync(driverActivity);
                            }
                        }
                    }
                    else if (data.Status == 15)
                    {
                        LoginErrorLogHelper loginErrorLog = new LoginErrorLogHelper();
                        if (request.CustomerId != null && request.CustomerId != new Guid())
                        {
                            var customer = (await _customerRepository.FindByCondition(x => x.Id.Equals(request.CustomerId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                            if (customer != null)
                            {
                                customer.CancelAmountPerYear = customer.CancelAmountPerYear + 1;
                                customer.CancelAmountPerMonth = customer.CancelAmountPerMonth + 1;
                                await _customerRepository.UpdateAsync(customer);
                            }

                            var customerActivity = (await _customerActivityRepository.FindByCondition(x => x.CustomerId.Equals(request.CustomerId) && x.Date == DateTime.Now.ToString("yyyy'-'MM'-'dd")).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                            if (customerActivity == null)
                            {
                                CustomerActivity newCustomerActivity = new CustomerActivity
                                {
                                    CustomerId = request.CustomerId,
                                    Date = DateTime.Now.ToString("yyyy'-'MM'-'dd"),
                                    Star = 0,
                                    RequestCar = 1,
                                    Cancel = 1,
                                    OrderingValue = 0,
                                    Created = DateTime.Now,
                                    CreatedBy = request.UserId != null ? request.UserId.ToString() : "-",
                                    Modified = DateTime.Now,
                                    ModifiedBy = request.UserId != null ? request.UserId.ToString() : "-",
                                };
                                await _customerActivityRepository.AddAsync(newCustomerActivity);
                            }
                            else
                            {
                                customerActivity.RequestCar = customerActivity.RequestCar + 1;
                                customerActivity.Cancel = customerActivity.Cancel + 1;
                                await _customerActivityRepository.UpdateAsync(customerActivity);
                            }
                            #region payment
                            var customerPayments = (await _customerPaymentRepository.FindByCondition(x => x.Customer.Id.Equals(request.CustomerId)).ConfigureAwait(false)).AsQueryable().ToList();
                            #region fine
                            if (previousStatus != 1 && previousStatus != 2 && previousStatus != 201 && previousStatus != 13 && previousStatus != 14 && previousStatus != 15 && previousStatus != 16)
                            {
                                if (previousStatus == 3)
                                {
                                    var fisrtAddress = data.Addresses.OrderBy(x => x.Sequence).FirstOrDefault();
                                    var orderPayment = data.Payments.Where(p => p.Module == "customer" && p.UserId == customer.Id).FirstOrDefault();

                                    if ((fisrtAddress.Date - DateTime.Now).TotalHours < 24)
                                    {
                                        if (customer.CancelAmountPerMonth > 1)
                                        {
                                            if (customerPayments != null && orderPayment != null)
                                            {
                                                var customerPayment = customerPayments.Where(p => p.Type.Id == 1).FirstOrDefault();
                                                if (customerPayment != null)
                                                {
                                                    customerPayment.Amount = (customerPayment.Amount + orderPayment.Amount) - 50;
                                                    await _customerPaymentRepository.UpdateAsync(customerPayment);
                                                    //var msg = loginErrorLog.LockUser(customer.PhoneNumber, _loginErrorLogRepository, 24);
                                                    //msg.Wait();
                                                }
                                            }
                                        }
                                        else
                                        {
                                            if (customerPayments != null && orderPayment != null)
                                            {
                                                var customerPayment = customerPayments.Where(p => p.Type.Id == 1).FirstOrDefault();
                                                if (customerPayment != null)
                                                {
                                                    customerPayment.Amount = (customerPayment.Amount + orderPayment.Amount);
                                                    await _customerPaymentRepository.UpdateAsync(customerPayment);
                                                    //var msg = loginErrorLog.LockUser(customer.PhoneNumber, _loginErrorLogRepository, 24);
                                                    //msg.Wait();
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (customer.CancelAmountPerMonth > 1)
                                        {
                                            if (customerPayments != null && orderPayment != null)
                                            {
                                                var customerPayment = customerPayments.Where(p => p.Type.Id == 1).FirstOrDefault();
                                                if (customerPayment != null)
                                                {
                                                    customerPayment.Amount = (customerPayment.Amount + orderPayment.Amount) - 100;
                                                    await _customerPaymentRepository.UpdateAsync(customerPayment);
                                                    //var msg = loginErrorLog.LockUser(customer.PhoneNumber, _loginErrorLogRepository,48);
                                                    // msg.Wait();
                                                }
                                            }
                                        }
                                        else
                                        {
                                            if (customerPayments != null && orderPayment != null)
                                            {
                                                var customerPayment = customerPayments.Where(p => p.Type.Id == 1).FirstOrDefault();
                                                if (customerPayment != null)
                                                {
                                                    customerPayment.Amount = (customerPayment.Amount + orderPayment.Amount);
                                                    await _customerPaymentRepository.UpdateAsync(customerPayment);
                                                    //var msg = loginErrorLog.LockUser(customer.PhoneNumber, _loginErrorLogRepository, 24);
                                                    //msg.Wait();
                                                }
                                            }
                                        }
                                    }
                                    var driverPayment = (await _driverPaymentRepository.FindByCondition(x => x.Driver.Id == data.Driver.Id && x.Type.Id == 1).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                                    var orderDriverPayment = (await _orderingPaymentRepository.FindByCondition(x => x.Ordering.Id == data.Id && x.Module == "driver" && x.UserId == data.Driver.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                                    if (driverPayment != null && orderDriverPayment != null)
                                    {
                                        driverPayment.Amount = driverPayment.Amount + orderDriverPayment.Amount;
                                    }
                                }
                            }
                            #endregion

                            #endregion payment
                        }
                        else if (request.DriverId != null && request.DriverId != new Guid())
                        {
                            var driverPayments = (await _driverPaymentRepository.FindByCondition(x => x.Driver.Id == request.DriverId).ConfigureAwait(false)).AsQueryable().ToList();
                            var driver = (await _driverRepository.FindByCondition(x => x.Id == request.DriverId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                            if (driver != null)
                            {
                                driver.CancelPerYear = driver.CancelPerYear + 1;
                                driver.CancelPerMonth = driver.CancelPerMonth + 1;
                                await _driverRepository.UpdateAsync(driver);
                            }


                            var driverActivity = (await _driverActivityRepository.FindByCondition(x => x.DriverId.Equals(request.DriverId) && x.Date == DateTime.Now.ToString("yyyy'-'MM'-'dd")).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                            if (driverActivity == null)
                            {
                                DriverActivity newDriverActivity = new DriverActivity
                                {
                                    DriverId = request.DriverId,
                                    Date = DateTime.Now.ToString("yyyy'-'MM'-'dd"),
                                    Star = 0,
                                    AcceptJob = 1,
                                    Complaint = 0,
                                    Reject = 0,
                                    Cancel = 1,
                                    InsuranceValue = 0,
                                    Created = DateTime.Now,
                                    CreatedBy = request.UserId != null ? request.UserId.ToString() : "-",
                                    Modified = DateTime.Now,
                                    ModifiedBy = request.UserId != null ? request.UserId.ToString() : "-",
                                };
                                await _driverActivityRepository.AddAsync(newDriverActivity);
                            }
                            else
                            {
                                driverActivity.AcceptJob = driverActivity.AcceptJob + 1;
                                driverActivity.Cancel = driverActivity.Cancel + 1;
                                await _driverActivityRepository.UpdateAsync(driverActivity);
                            }

                            #region fine
                            if (previousStatus != 1 && previousStatus != 2 && previousStatus != 201 && previousStatus != 13 && previousStatus != 14 && previousStatus != 15 && previousStatus != 16)
                            {
                                if (previousStatus == 3)
                                {
                                    var fisrtAddress = data.Addresses.OrderBy(x => x.Sequence).FirstOrDefault();
                                    var orderPayment = data.Payments.Where(p => p.Module == "driver" && p.UserId == driver.Id).FirstOrDefault();
                                    if ((fisrtAddress.Date - DateTime.Now).TotalHours < 24)
                                    {
                                        if (driver.CancelPerMonth > 1)
                                        {
                                            if (driverPayments != null && orderPayment != null)
                                            {
                                                var driverPayment = driverPayments.Where(p => p.Type.Id == 1).FirstOrDefault();
                                                if (driverPayment != null)
                                                {
                                                    driverPayment.Amount = driverPayment.Amount + orderPayment.Amount - 30;
                                                    await _driverPaymentRepository.UpdateAsync(driverPayment);
                                                    var msg = loginErrorLog.LockUser(driver.PhoneNumber, _loginErrorLogRepository, 24);
                                                    msg.Wait();
                                                }
                                            }
                                        }
                                        else
                                        {
                                            if (driverPayments != null && orderPayment != null)
                                            {
                                                var driverPayment = driverPayments.Where(p => p.Type.Id == 1).FirstOrDefault();
                                                if (driverPayment != null)
                                                {
                                                    driverPayment.Amount = (driverPayment.Amount + orderPayment.Amount);
                                                    await _driverPaymentRepository.UpdateAsync(driverPayment);
                                                    var msg = loginErrorLog.LockUser(driver.PhoneNumber, _loginErrorLogRepository, 24);
                                                    msg.Wait();
                                                }
                                            }
                                        }

                                        var cancleOrderings = (await _orderingRepository.FindByCondition(x => x.Driver.Id == driver.Id && x.Status == 3).ConfigureAwait(false)).AsQueryable().ToList();
                                        //cancal all ordering
                                        foreach (Ordering cancleItem in cancleOrderings)
                                        {
                                            var cancleItemAddress = cancleItem.Addresses.OrderBy(x => x.Sequence).FirstOrDefault();
                                            if ((cancleItemAddress.Date - DateTime.Now).TotalHours < 24)
                                            {
                                                cancleItem.Status = 15;

                                                var customerPayment = (await _customerPaymentRepository.FindByCondition(x => x.Customer.Id == cancleItem.Customer.Id && x.Type.Id == 1).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                                                var orderCustomerPayment = (await _orderingPaymentRepository.FindByCondition(x => x.Ordering.Id == data.Id && x.Module == "customer" && x.UserId == cancleItem.Customer.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                                                if (customerPayment != null && orderCustomerPayment != null)
                                                {
                                                    customerPayment.Amount = customerPayment.Amount + orderCustomerPayment.Amount;
                                                }


                                                await _orderingRepository.UpdateAsync(cancleItem);
                                                #region send noti

                                                if (userId != null)
                                                {
                                                    //var cloneOrdering = _mapper.Map<OrderingViewModel>(data);
                                                    var cloneOrdering = new OrderingNotificationViewModel
                                                    {
                                                        id = cancleItem.Id,
                                                        status = cancleItem.Status == 0 && address != null ? address.Status : cancleItem.Status,
                                                        trackingCode = cancleItem.TrackingCode,
                                                        orderNumber = cancleItem.TrackingCode,
                                                        products = _mapper.Map<List<OrderingProductNotificationViewModel>>(cancleItem.Products)
                                                    };
                                                    if (cancleItem.Cars.Count > 0)
                                                    {
                                                        cloneOrdering.carType = _mapper.Map<CarTypeViewModel>(cancleItem.Cars[0].CarType);
                                                        cloneOrdering.carList = _mapper.Map<CarListViewModel>(cancleItem.Cars[0].CarList);
                                                    }
                                                    if (cloneOrdering.status != null)
                                                    {
                                                        cloneOrdering.statusObj = driverOrderingStatus.Where(o => o.id == cloneOrdering.status).FirstOrDefault();
                                                        if (request.DriverId != null && request.DriverId != new Guid())
                                                        {
                                                            cloneOrdering.statusObj = customerOrderingStatus.Where(o => o.id == cloneOrdering.status).FirstOrDefault();
                                                        }
                                                    }
                                                    if (cancleItem.Addresses.Count > 0)
                                                    {
                                                        cloneOrdering.startPoint = _mapper.Map<OrderingAddressNotificationViewModel>(cancleItem.Addresses[0]);
                                                        cloneOrdering.endPoint = _mapper.Map<OrderingAddressNotificationViewModel>(cancleItem.Addresses[cancleItem.Addresses.Count - 1]);
                                                    }
                                                    foreach (OrderingProductNotificationViewModel productViewModel in cloneOrdering.products)
                                                    {
                                                        if (productViewModel.productType != null)
                                                        {
                                                            string[] productTypeIds = productViewModel.productType.Split(",");
                                                            productViewModel.productTypes = new List<ProductTypeViewModel>();
                                                            foreach (string productId in productTypeIds)
                                                            {
                                                                int id = 0;
                                                                if (int.TryParse(productId, out id))
                                                                {
                                                                    var productType = productTypes.Where(p => p.Id == id).FirstOrDefault();
                                                                    if (productType != null)
                                                                        productViewModel.productTypes.Add(_mapper.Map<ProductTypeViewModel>(productType));
                                                                }
                                                            }
                                                        }
                                                        if (productViewModel.packaging != null)
                                                        {
                                                            string[] packagingIds = productViewModel.packaging.Split(",");
                                                            productViewModel.packagings = new List<ProductPackagingViewModel>();
                                                            foreach (string packagingId in packagingIds)
                                                            {
                                                                int id = 0;
                                                                if (int.TryParse(packagingId, out id))
                                                                {
                                                                    var packaging = packagings.Where(p => p.Id == id).FirstOrDefault();
                                                                    if (packaging != null)
                                                                        productViewModel.packagings.Add(_mapper.Map<ProductPackagingViewModel>(packaging));
                                                                }
                                                            }
                                                        }

                                                    }
                                                    //if (cancleItem.Addresses.Count > 0)
                                                    //{
                                                    //    cloneOrdering.startPoint = _mapper.Map<OrderingAddressNotificationViewModel>(cancleItem.Addresses[0]);
                                                    //    cloneOrdering.endPoint = _mapper.Map<OrderingAddressNotificationViewModel>(cancleItem.Addresses[cancleItem.Addresses.Count - 1]);
                                                    //}
                                                    var oneSignalIds = (await _notificationUserRepository.FindByCondition(x => x.UserId.Equals(userId)).ConfigureAwait(false)).AsQueryable().Select(x => x.OneSignalId).ToList();
                                                    var notificationDetails = Path.Combine(Directory.GetCurrentDirectory(), $"Shared\\NotificationWord.json");
                                                    var notificationJson = System.IO.File.ReadAllText(notificationDetails);
                                                    List<NotificationWordStatusViewModel> notificationStatus = Newtonsoft.Json.JsonConvert.DeserializeObject<List<NotificationWordStatusViewModel>>(notificationJson);

                                                    string title = "";
                                                    string detail = "";
                                                    int notiId = 9;

                                                    var notificationObj = notificationStatus.Where(o => o.id == notiId).FirstOrDefault();

                                                    if (notificationObj != null)
                                                    {
                                                        title = notificationObj.title.th;
                                                        if (cancleItem.Customer != null)
                                                        {
                                                            detail = notificationObj.detail.th.Replace("#customertitle#", cancleItem.Customer.Title != null ? cancleItem.Customer.Title.Name_TH : "")
                                                                     .Replace("#customername#", cancleItem.Customer.Name)
                                                                     .Replace("#orderprice#", cancleItem.OrderingDesiredPrice != null || cancleItem.OrderingDesiredPrice != 0 ? cancleItem.OrderingDesiredPrice.ToString() : "-");

                                                            //if (cancelStatus != null)
                                                            //{
                                                            //    detail = detail.Replace("#cancelstatus#", cancelStatus.Name_TH);
                                                            //}
                                                        }
                                                    }
                                                    //if (cloneOrdering.products.Count > 2)
                                                    //{
                                                    //    cloneOrdering.products.RemoveRange(2, cloneOrdering.products.Count - 2);
                                                    //}
                                                    Domain.Notification notification = new Domain.Notification();

                                                    notification.OneSignalId = oneSignalIds.FirstOrDefault();
                                                    notification.UserId = userId;
                                                    notification.OwnerId = ownerId;
                                                    notification.Title = title;
                                                    notification.Detail = detail;
                                                    notification.ServiceCode = "Ordering";
                                                    //notification.ReferenceContentKey = request.ReferenceContentKey;
                                                    notification.Payload = Newtonsoft.Json.JsonConvert.SerializeObject(cloneOrdering);
                                                    notification.Created = DateTime.UtcNow;
                                                    notification.Modified = DateTime.UtcNow;
                                                    notification.CreatedBy = request.DriverId.ToString();
                                                    notification.ModifiedBy = request.DriverId.ToString();
                                                    await _notificationRepository.AddAsync(notification);

                                                    oneSignalIds = oneSignalIds.Where(x => x.Trim() != "").ToList();
                                                    if (oneSignalIds.Count > 0)
                                                    {
                                                        NotificationManager notiMgr = new NotificationManager(APP_ID, REST_API_KEY, AUTH_ID);
                                                        notiMgr.SendNotificationAsync(title, detail, oneSignalIds, notification.ServiceCode, "", "{id:'" + cloneOrdering.id + "'}");
                                                    }
                                                }
                                                #endregion
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (driver.CancelPerMonth > 1)
                                        {
                                            if (driverPayments != null && orderPayment != null)
                                            {
                                                var driverPayment = driverPayments.Where(p => p.Type.Id.ToString() == orderPayment.Channel).FirstOrDefault();
                                                if (driverPayment != null)
                                                {
                                                    driverPayment.Amount = driverPayment.Amount + orderPayment.Amount - 100;
                                                    await _driverPaymentRepository.UpdateAsync(driverPayment);
                                                    var msg = loginErrorLog.LockUser(driver.PhoneNumber, _loginErrorLogRepository, 48);
                                                    msg.Wait();
                                                }
                                            }
                                        }
                                        else
                                        {
                                            if (driverPayments != null && orderPayment != null)
                                            {
                                                var driverPayment = driverPayments.Where(p => p.Type.Id == 1).FirstOrDefault();
                                                if (driverPayment != null)
                                                {
                                                    driverPayment.Amount = (driverPayment.Amount + orderPayment.Amount);
                                                    await _driverPaymentRepository.UpdateAsync(driverPayment);
                                                    var msg = loginErrorLog.LockUser(driver.PhoneNumber, _loginErrorLogRepository, 48);
                                                    msg.Wait();
                                                }
                                            }
                                        }
                                        var cancleOrderings = (await _orderingRepository.FindByCondition(x => x.Driver.Id == driver.Id && x.Status == 3).ConfigureAwait(false)).AsQueryable().ToList();
                                        //cancal all ordering
                                        foreach (Ordering cancleItem in cancleOrderings)
                                        {
                                            var cancleItemAddress = cancleItem.Addresses.OrderBy(x => x.Sequence).FirstOrDefault();
                                            if ((cancleItemAddress.Date - DateTime.Now).TotalHours < 48)
                                            {
                                                cancleItem.Status = 15;



                                                await _orderingRepository.UpdateAsync(cancleItem);
                                                #region send noti

                                                if (userId != null)
                                                {
                                                    //var cloneOrdering = _mapper.Map<OrderingViewModel>(data);
                                                    var cloneOrdering = new OrderingNotificationViewModel
                                                    {
                                                        id = cancleItem.Id,
                                                        status = cancleItem.Status == 0 && address != null ? address.Status : cancleItem.Status,
                                                        trackingCode = cancleItem.TrackingCode,
                                                        orderNumber = cancleItem.TrackingCode,
                                                        products = _mapper.Map<List<OrderingProductNotificationViewModel>>(cancleItem.Products)
                                                    };
                                                    if (cancleItem.Cars.Count > 0)
                                                    {
                                                        cloneOrdering.carType = _mapper.Map<CarTypeViewModel>(cancleItem.Cars[0].CarType);
                                                        cloneOrdering.carList = _mapper.Map<CarListViewModel>(cancleItem.Cars[0].CarList);
                                                    }
                                                    if (cloneOrdering.status != null)
                                                    {
                                                        cloneOrdering.statusObj = driverOrderingStatus.Where(o => o.id == cloneOrdering.status).FirstOrDefault();
                                                        if (request.DriverId != null && request.DriverId != new Guid())
                                                        {
                                                            cloneOrdering.statusObj = customerOrderingStatus.Where(o => o.id == cloneOrdering.status).FirstOrDefault();
                                                        }
                                                    }
                                                    if (cancleItem.Addresses.Count > 0)
                                                    {
                                                        cloneOrdering.startPoint = _mapper.Map<OrderingAddressNotificationViewModel>(cancleItem.Addresses[0]);
                                                        cloneOrdering.endPoint = _mapper.Map<OrderingAddressNotificationViewModel>(cancleItem.Addresses[cancleItem.Addresses.Count - 1]);
                                                    }
                                                    foreach (OrderingProductNotificationViewModel productViewModel in cloneOrdering.products)
                                                    {
                                                        if (productViewModel.productType != null)
                                                        {
                                                            string[] productTypeIds = productViewModel.productType.Split(",");
                                                            productViewModel.productTypes = new List<ProductTypeViewModel>();
                                                            foreach (string productId in productTypeIds)
                                                            {
                                                                int id = 0;
                                                                if (int.TryParse(productId, out id))
                                                                {
                                                                    var productType = productTypes.Where(p => p.Id == id).FirstOrDefault();
                                                                    if (productType != null)
                                                                        productViewModel.productTypes.Add(_mapper.Map<ProductTypeViewModel>(productType));
                                                                }
                                                            }
                                                        }
                                                        if (productViewModel.packaging != null)
                                                        {
                                                            string[] packagingIds = productViewModel.packaging.Split(",");
                                                            productViewModel.packagings = new List<ProductPackagingViewModel>();
                                                            foreach (string packagingId in packagingIds)
                                                            {
                                                                int id = 0;
                                                                if (int.TryParse(packagingId, out id))
                                                                {
                                                                    var packaging = packagings.Where(p => p.Id == id).FirstOrDefault();
                                                                    if (packaging != null)
                                                                        productViewModel.packagings.Add(_mapper.Map<ProductPackagingViewModel>(packaging));
                                                                }
                                                            }
                                                        }

                                                    }
                                                    //if (cancleItem.Addresses.Count > 0)
                                                    //{
                                                    //    cloneOrdering.startPoint = _mapper.Map<OrderingAddressNotificationViewModel>(cancleItem.Addresses[0]);
                                                    //    cloneOrdering.endPoint = _mapper.Map<OrderingAddressNotificationViewModel>(cancleItem.Addresses[cancleItem.Addresses.Count - 1]);
                                                    //}
                                                    var oneSignalIds = (await _notificationUserRepository.FindByCondition(x => x.UserId.Equals(userId)).ConfigureAwait(false)).AsQueryable().Select(x => x.OneSignalId).ToList();
                                                    var notificationDetails = Path.Combine(Directory.GetCurrentDirectory(), $"Shared\\NotificationWord.json");
                                                    var notificationJson = System.IO.File.ReadAllText(notificationDetails);
                                                    List<NotificationWordStatusViewModel> notificationStatus = Newtonsoft.Json.JsonConvert.DeserializeObject<List<NotificationWordStatusViewModel>>(notificationJson);

                                                    string title = "";
                                                    string detail = "";
                                                    int notiId = 9;

                                                    var notificationObj = notificationStatus.Where(o => o.id == notiId).FirstOrDefault();

                                                    if (notificationObj != null)
                                                    {
                                                        title = notificationObj.title.th;
                                                        if (cancleItem.Customer != null)
                                                        {
                                                            detail = notificationObj.detail.th.Replace("#customertitle#", cancleItem.Customer.Title != null ? cancleItem.Customer.Title.Name_TH : "")
                                                                     .Replace("#customername#", cancleItem.Customer.Name)
                                                                     .Replace("#orderprice#", cancleItem.OrderingDesiredPrice != null || cancleItem.OrderingDesiredPrice != 0 ? cancleItem.OrderingDesiredPrice.ToString() : "-");

                                                            //if (cancelStatus != null)
                                                            //{
                                                            //    detail = detail.Replace("#cancelstatus#", cancelStatus.Name_TH);
                                                            //}
                                                        }
                                                    }
                                                    //if (cloneOrdering.products.Count > 2)
                                                    //{
                                                    //    cloneOrdering.products.RemoveRange(2, cloneOrdering.products.Count - 2);
                                                    //}
                                                    Domain.Notification notification = new Domain.Notification();

                                                    notification.OneSignalId = oneSignalIds.FirstOrDefault();
                                                    notification.UserId = userId;
                                                    notification.OwnerId = ownerId;
                                                    notification.Title = title;
                                                    notification.Detail = detail;
                                                    notification.ServiceCode = "Ordering";
                                                    //notification.ReferenceContentKey = request.ReferenceContentKey;
                                                    notification.Payload = Newtonsoft.Json.JsonConvert.SerializeObject(cloneOrdering);
                                                    notification.Created = DateTime.UtcNow;
                                                    notification.Modified = DateTime.UtcNow;
                                                    notification.CreatedBy = request.DriverId.ToString();
                                                    notification.ModifiedBy = request.DriverId.ToString();
                                                    await _notificationRepository.AddAsync(notification);

                                                    oneSignalIds = oneSignalIds.Where(x => x.Trim() != "").ToList();
                                                    if (oneSignalIds.Count > 0)
                                                    {
                                                        NotificationManager notiMgr = new NotificationManager(APP_ID, REST_API_KEY, AUTH_ID);
                                                        notiMgr.SendNotificationAsync(title, detail, oneSignalIds, notification.ServiceCode, "", "{id:'" + cloneOrdering.id + "'}");
                                                    }
                                                }
                                                #endregion
                                            }
                                        }
                                    }
                                }
                            }
                            #endregion
                        }
                    }

                    orderingHistory.TrackingCode = data.TrackingCode;
                    orderingHistory.OrderingId = data.Id;
                    orderingHistory.Driver = data.Driver;
                    orderingHistory.Customer = data.Customer;
                    orderingHistory.ProductCBM = data.ProductCBM;
                    orderingHistory.ProductTotalWeight = data.ProductTotalWeight;
                    orderingHistory.OrderingPrice = data.OrderingPrice;
                    orderingHistory.OrderingStatus = data.Status;
                    orderingHistory.AdditionalDetail = data.AdditionalDetail;
                    orderingHistory.Note = data.Note;
                    orderingHistory.OrderNumber = data.TrackingCode;
                    orderingHistory.OrderingDesiredPrice = data.OrderingDesiredPrice;
                    orderingHistory.OrderingDriverOffering = data.OrderingDriverOffering;
                    orderingHistory.IsOrderingDriverOffer = data.IsOrderingDriverOffer;
                    orderingHistory.CancelStatus = data.CancelStatus;
                    orderingHistory.CustomerRanking = data.CustomerRanking;
                    orderingHistory.DriverRanking = data.DriverRanking;
                    orderingHistory.PinnedDate = data.PinnedDate;
                    orderingHistory.Distance = data.Distance;
                    orderingHistory.EstimateTime = data.EstimateTime;
                    orderingHistory.IsDriverPay = data.IsDriverPay;
                    orderingHistory.CurrentLocation = data.CurrentLocation;
                    orderingHistory.GPValue = data.GPValue;
                    orderingHistory.DriverPayValue = data.DriverPayValue;
                    orderingHistory.CustomerCancelValue = data.CustomerCancelValue;
                    orderingHistory.DriverCancelValue = data.DriverCancelValue;
                    orderingHistory.CustomerCashBack = data.CustomerCashBack;
                    orderingHistory.DriverCashBack = data.DriverCashBack;
                    orderingHistory.DriverAnnouncements = data.DriverAnnouncements;
                    orderingHistory.Created = DateTime.UtcNow;
                    orderingHistory.Modified = DateTime.UtcNow;
                    orderingHistory.CreatedBy = request.UserId != null ? request.UserId.ToString() : "";
                    orderingHistory.ModifiedBy = request.UserId != null ? request.UserId.ToString() : "";

                    await _orderingHistoryRepository.AddAsync(orderingHistory);


                    #region send noti

                    //var customerOrderingStatusFile = Path.Combine(Directory.GetCurrentDirectory(), $"Shared\\customer_orderingStatus.json");
                    //var customerOrderingStatusJson = System.IO.File.ReadAllText(customerOrderingStatusFile);
                    //List<OrderingStatusViewModel> customerOrderingStatus = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OrderingStatusViewModel>>(customerOrderingStatusJson);

                    //var driverOrderingStatusFile = Path.Combine(Directory.GetCurrentDirectory(), $"Shared\\driver_orderingStatus.json");
                    //var driverOrderingStatusJson = System.IO.File.ReadAllText(customerOrderingStatusFile);
                    //List<OrderingStatusViewModel> driverOrderingStatus = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OrderingStatusViewModel>>(driverOrderingStatusJson);


                    //var userId = request.CustomerId;
                    //var ownerId = request.CustomerId;

                    //var APP_ID = request.APP_ID;
                    //var REST_API_KEY = request.REST_API_KEY;
                    //var AUTH_ID = request.AUTH_ID;

                    //if (request.CustomerId != null && request.CustomerId != new Guid())
                    //{
                    //    userId = data.Driver.Id;
                    //    ownerId = request.CustomerId;
                    //    APP_ID = request.DRIVER_APP_ID;
                    //    REST_API_KEY = request.DRIVER_REST_API_KEY;
                    //    AUTH_ID = request.DRIVER_AUTH_ID;
                    //}
                    if (request.DriverId != null && request.DriverId != new Guid())
                    {
                        userId = data.Customer.Id;
                        ownerId = request.DriverId;
                    }
                    if (userId != null)
                    {
                        //var cloneOrdering = _mapper.Map<OrderingViewModel>(data);
                        var cloneOrdering = new OrderingNotificationViewModel
                        {
                            id = data.Id,
                            status = data.Status == 0 && address != null ? address.Status : data.Status,
                            trackingCode = data.TrackingCode,
                            orderNumber = data.TrackingCode,
                            products = _mapper.Map<List<OrderingProductNotificationViewModel>>(data.Products)
                        };
                        if (data.Cars.Count > 0)
                        {
                            cloneOrdering.carType = _mapper.Map<CarTypeViewModel>(data.Cars[0].CarType);
                            cloneOrdering.carList = _mapper.Map<CarListViewModel>(data.Cars[0].CarList);
                        }
                        if (cloneOrdering.status != null)
                        {
                            cloneOrdering.statusObj = driverOrderingStatus.Where(o => o.id == cloneOrdering.status).FirstOrDefault();
                            if (request.DriverId != null && request.DriverId != new Guid())
                            {
                                cloneOrdering.statusObj = customerOrderingStatus.Where(o => o.id == cloneOrdering.status).FirstOrDefault();
                            }
                        }
                        if (data.Addresses.Count > 0)
                        {
                            cloneOrdering.startPoint = _mapper.Map<OrderingAddressNotificationViewModel>(data.Addresses[0]);
                            cloneOrdering.endPoint = _mapper.Map<OrderingAddressNotificationViewModel>(data.Addresses[data.Addresses.Count - 1]);
                        }
                        foreach (OrderingProductNotificationViewModel productViewModel in cloneOrdering.products)
                        {
                            if (productViewModel.productType != null)
                            {
                                string[] productTypeIds = productViewModel.productType.Split(",");
                                productViewModel.productTypes = new List<ProductTypeViewModel>();
                                foreach (string productId in productTypeIds)
                                {
                                    int id = 0;
                                    if (int.TryParse(productId, out id))
                                    {
                                        var productType = productTypes.Where(p => p.Id == id).FirstOrDefault();
                                        if (productType != null)
                                            productViewModel.productTypes.Add(_mapper.Map<ProductTypeViewModel>(productType));
                                    }
                                }
                            }
                            if (productViewModel.packaging != null)
                            {
                                string[] packagingIds = productViewModel.packaging.Split(",");
                                productViewModel.packagings = new List<ProductPackagingViewModel>();
                                foreach (string packagingId in packagingIds)
                                {
                                    int id = 0;
                                    if (int.TryParse(packagingId, out id))
                                    {
                                        var packaging = packagings.Where(p => p.Id == id).FirstOrDefault();
                                        if (packaging != null)
                                            productViewModel.packagings.Add(_mapper.Map<ProductPackagingViewModel>(packaging));
                                    }
                                }
                            }

                        }
                        //if (data.Addresses.Count > 0)
                        //{
                        //    cloneOrdering.startPoint = _mapper.Map<OrderingAddressNotificationViewModel>(data.Addresses[0]);
                        //    cloneOrdering.endPoint = _mapper.Map<OrderingAddressNotificationViewModel>(data.Addresses[data.Addresses.Count - 1]);
                        //}
                        var oneSignalIds = (await _notificationUserRepository.FindByCondition(x => x.UserId.Equals(userId)).ConfigureAwait(false)).AsQueryable().Select(x => x.OneSignalId).ToList();
                        var notificationDetails = Path.Combine(Directory.GetCurrentDirectory(), $"Shared\\NotificationWord.json");
                        var notificationJson = System.IO.File.ReadAllText(notificationDetails);
                        List<NotificationWordStatusViewModel> notificationStatus = Newtonsoft.Json.JsonConvert.DeserializeObject<List<NotificationWordStatusViewModel>>(notificationJson);

                        string title = "";
                        string detail = "";
                        int notiId = 1;
                        if (request.Status == 15 && request.CustomerId != null && request.CustomerId != new Guid())
                        {
                            notiId = 8;
                        }
                        else if (request.Status == 15 && request.DriverId != null && request.DriverId != new Guid())
                        {
                            notiId = 9;
                        }
                        else if (request.Status == 4)
                        {
                            notiId = 10;
                        }
                        else if (request.Status == 5)
                        {
                            notiId = 11;
                        }
                        else if (request.Status == 6)
                        {
                            notiId = 12;
                        }
                        else if (request.Status == 7)
                        {
                            notiId = 120;
                        }
                        else if (request.Status == 8)
                        {
                            notiId = 13;
                        }
                        else if (request.Status == 9)
                        {
                            notiId = 15;
                        }
                        else if (request.Status == 10)
                        {
                            notiId = 16;
                        }
                        else if (request.Status == 11)
                        {
                            notiId = 17;
                        }
                        else if (request.Status == 12)
                        {
                            notiId = 18;
                        }
                        else if (request.Status == 13)
                        {
                            notiId = 19;
                        }
                        var notificationObj = notificationStatus.Where(o => o.id == notiId).FirstOrDefault();

                        if (notificationObj != null)
                        {
                            title = notificationObj.title.th;
                            if (data.Customer != null)
                            {
                                detail = notificationObj.detail.th.Replace("#customertitle#", data.Customer.Title != null ? data.Customer.Title.Name_TH : "")
                                         .Replace("#customername#", data.Customer.Name)
                                         .Replace("#orderprice#", data.OrderingDesiredPrice != null || data.OrderingDesiredPrice != 0 ? data.OrderingDesiredPrice.ToString() : "-");

                                if (cancelStatus != null)
                                {
                                    detail = detail.Replace("#cancelstatus#", cancelStatus.Name_TH);
                                }
                            }
                        }
                        //if (cloneOrdering.products.Count > 2)
                        //{
                        //    cloneOrdering.products.RemoveRange(2, cloneOrdering.products.Count - 2);
                        //}
                        Domain.Notification notification = new Domain.Notification();

                        notification.OneSignalId = oneSignalIds.FirstOrDefault();
                        notification.UserId = userId;
                        notification.OwnerId = ownerId;
                        notification.Title = title;
                        notification.Detail = detail;
                        notification.ServiceCode = "Ordering";
                        //notification.ReferenceContentKey = request.ReferenceContentKey;
                        notification.Payload = Newtonsoft.Json.JsonConvert.SerializeObject(cloneOrdering);
                        notification.Created = DateTime.UtcNow;
                        notification.Modified = DateTime.UtcNow;
                        notification.CreatedBy = request.DriverId.ToString();
                        notification.ModifiedBy = request.DriverId.ToString();
                        await _notificationRepository.AddAsync(notification);

                        //if (request.CustomerId != null && request.CustomerId != new Guid())
                        //{
                        //    #region insert to Inbox
                        //    DriverInbox driverInbox = new DriverInbox()
                        //    {
                        //        DriverId = data.Driver.Id,
                        //        Title = "-",
                        //        Title_ENG = "-",
                        //        Module = "การแจ้งเตือน",
                        //        Module_ENG = "Notification",
                        //        Content = Newtonsoft.Json.JsonConvert.SerializeObject(cloneOrdering),
                        //        FromUser = data.Customer.FirstName + data.Customer.LastName,
                        //        IsDelete = false,
                        //        Created = DateTime.Now
                        //    };
                        //    await _driverInboxRepository.AddAsync(driverInbox);
                        //    #endregion
                        //}
                        //if (request.DriverId != null && request.DriverId != new Guid())
                        //{
                        //    #region insert to Inbox
                        //    CustomerInbox customerInbox = new CustomerInbox()
                        //    {
                        //        CustomerId = data.Customer.Id,
                        //        Title = "-",
                        //        Title_ENG = "-",
                        //        Module = "การแจ้งเตือน",
                        //        Module_ENG = "Notification",
                        //        Content = Newtonsoft.Json.JsonConvert.SerializeObject(cloneOrdering),
                        //        FromUser = data.Driver.FirstName + data.Driver.LastName,
                        //        IsDelete = false,
                        //        Created = DateTime.Now
                        //    };
                        //    await _customerInboxRepository.AddAsync(customerInbox);
                        //    #endregion
                        //}
                        oneSignalIds = oneSignalIds.Where(x => x.Trim() != "").ToList();
                        if (oneSignalIds.Count > 0)
                        {
                            NotificationManager notiMgr = new NotificationManager(APP_ID, REST_API_KEY, AUTH_ID);
                            notiMgr.SendNotificationAsync(title, detail, oneSignalIds, notification.ServiceCode, "", "{id:'" + cloneOrdering.id + "'}");
                        }
                    }
                    #endregion

                    var log = new CreateAppLog(_applicationLogRepository);
                    log.CreateLog("Ordering", "DriverAcceptOrdering", "Edit", Newtonsoft.Json.JsonConvert.SerializeObject(request), request.UserId.ToString());
                    return new Response<Guid>(data.Id);

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}