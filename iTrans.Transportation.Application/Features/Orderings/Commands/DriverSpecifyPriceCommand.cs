﻿using AutoMapper;
using iTrans.Transportation.Application.DTOs.CarList;
using iTrans.Transportation.Application.DTOs.CarType;
using iTrans.Transportation.Application.DTOs.Ordering;
using iTrans.Transportation.Application.DTOs.OrderingAddress;
using iTrans.Transportation.Application.DTOs.OrderingAddressProduct;
using iTrans.Transportation.Application.DTOs.OrderingProduct;
using iTrans.Transportation.Application.DTOs.OrderingStatus;
using iTrans.Transportation.Application.DTOs.ProductPackaging;
using iTrans.Transportation.Application.DTOs.ProductType;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Features.Locations;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;


namespace iTrans.Transportation.Application.Features.Orderings.Commands
{
    public class DriverSpecifyPriceCommand : IRequest<Response<Guid>>
    {       
        public virtual Guid? UserId { get; set; }
        public virtual int DriverReserveId { get; set; }
        public virtual Guid OrderingId { get; set; }
        public virtual Guid DriverId { get; set; }
        public virtual decimal DriverOffering { get; set; }
        public string APP_ID { get; set; }
        public string REST_API_KEY { get; set; }
        public string AUTH_ID { get; set; }
    }
    public class DriverSpecifyPriceCommandHandler : IRequestHandler<DriverSpecifyPriceCommand, Response<Guid>>
    {
        private readonly IOrderingRepositoryAsync _orderingRepository;
        private readonly IDriverRepositoryAsync _driverRepository;
        private readonly IDriverActivityRepositoryAsync _driverActivityRepository;
        private readonly IOrderingDriverReserveRepositoryAsync _orderingDriverReserveRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IProductTypeRepositoryAsync _productTypeRepository;
        private readonly IProductPackagingRepositoryAsync _productPackagingRepository;
        private readonly INotificationRepositoryAsync _notificationRepository;
        private readonly INotificationUserRepositoryAsync _notificationUserRepository;
        private readonly IOrderingHistoryRepositoryAsync _orderingHistoryRepository;
        private readonly ICustomerInboxRepositoryAsync _customerInboxRepository;
        private readonly IMapper _mapper;

        public DriverSpecifyPriceCommandHandler(IDriverRepositoryAsync driverRepository, IOrderingRepositoryAsync orderingRepository, IApplicationLogRepositoryAsync applicationLogRepository
            , IProductTypeRepositoryAsync productTypeRepository, IProductPackagingRepositoryAsync productPackagingRepository, IOrderingDriverReserveRepositoryAsync orderingDriverReserveRepository
            , INotificationRepositoryAsync notificationRepository, INotificationUserRepositoryAsync notificationUserRepository, IOrderingHistoryRepositoryAsync orderingHistoryRepository
            , ICustomerInboxRepositoryAsync customerInboxRepository, IDriverActivityRepositoryAsync driverActivityRepository, IMapper mapper)
        {
            _orderingRepository = orderingRepository;
            _driverRepository = driverRepository;
            _applicationLogRepository = applicationLogRepository;
            _productTypeRepository = productTypeRepository;
            _productPackagingRepository = productPackagingRepository;
            _orderingDriverReserveRepository = orderingDriverReserveRepository;
            _notificationRepository = notificationRepository;
            _notificationUserRepository = notificationUserRepository;
            _orderingHistoryRepository = orderingHistoryRepository;
            _customerInboxRepository = customerInboxRepository;
            _driverActivityRepository = driverActivityRepository;
            _mapper = mapper;
        }

        public async Task<Response<Guid>> Handle(DriverSpecifyPriceCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var ordering = (await _orderingRepository.FindByCondition(x => x.Id == request.OrderingId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (ordering == null)
                {
                    throw new ApiException($"Ordering Not Found.");
                }
                var driver = (await _driverRepository.FindByCondition(x => x.Id == request.DriverId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (ordering == null)
                {
                    throw new ApiException($"Driver Not Found.");
                }

                
                if (driver != null)
                {
                    if (driver.Level == 1)
                    {
                        var driverActivityRepository = (await _driverActivityRepository.FindByCondition(x => x.DriverId == request.DriverId && x.Date.Contains("yyyy'-'MM'")).ConfigureAwait(false)).AsQueryable().ToList();
                        int acceptjobCount = 0;

                        foreach (DriverActivity item in driverActivityRepository)
                        {
                            acceptjobCount = acceptjobCount + item.AcceptJob + item.Cancel;
                        }
                        if (request.DriverOffering > 2000)
                        {
                            throw new ApiException("You can't get a job priced above 2000.");
                        }
                        if (acceptjobCount > 4)
                        {
                            throw new ApiException("You can't get more than 4 jobs per month.");
                        }
                    }
                }

                OrderingHistory orderingHistory = new OrderingHistory();


                var productTypes = (await _productTypeRepository.FindByCondition(x => x.Active.Equals(true)).ConfigureAwait(false)).AsQueryable().ToList();
                var packagings = (await _productPackagingRepository.FindByCondition(x => x.Active.Equals(true)).ConfigureAwait(false)).AsQueryable().ToList();

                var orderingDriverReserve = (await _orderingDriverReserveRepository.FindByCondition(x => x.Id == request.DriverReserveId && x.Ordering.Id == request.OrderingId && x.Driver.Id == request.DriverId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (orderingDriverReserve == null)
                {
                    OrderingDriverReserve driverReserve = new OrderingDriverReserve();

                    driverReserve.Ordering = ordering;
                    driverReserve.Driver = driver;
                    driverReserve.DriverOfferingPrice = request.DriverOffering;
                    driverReserve.Status = 2;
                    driverReserve.Created = DateTime.Now;
                    driverReserve.Modified = DateTime.Now;
                    driverReserve.CreatedBy = request.DriverId != null ? request.DriverId.ToString() : "";
                    driverReserve.ModifiedBy = request.DriverId != null ? request.DriverId.ToString() : "";
                    await _orderingDriverReserveRepository.AddAsync(driverReserve);
                }
                else
                {
                    orderingDriverReserve.DriverOfferingPrice = request.DriverOffering;
                    orderingDriverReserve.Status = 2;
                    orderingDriverReserve.Modified = DateTime.Now;
                    orderingDriverReserve.ModifiedBy = request.DriverId != null ? request.DriverId.ToString() : "";
                    await _orderingDriverReserveRepository.UpdateAsync(orderingDriverReserve);
                }

                #region
                var folderDetails = Path.Combine(Directory.GetCurrentDirectory(), $"Shared\\OrderingStatus.json");
                var statusJson = System.IO.File.ReadAllText(folderDetails);
                List<OrderingStatusViewModel> orderingStatus = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OrderingStatusViewModel>>(statusJson);

                //var cloneOrdering = _mapper.Map<OrderingViewModel>(ordering);
                var cloneOrdering = new OrderingNotificationViewModel
                {
                    id = ordering.Id,
                    status = ordering.Status,
                    trackingCode = ordering.TrackingCode,
                    orderNumber = ordering.TrackingCode,
                    products = _mapper.Map<List<OrderingProductNotificationViewModel>>(ordering.Products)
                };
                if (ordering.Cars.Count > 0)
                {
                    cloneOrdering.carType = _mapper.Map<CarTypeViewModel>(ordering.Cars[0].CarType);
                    cloneOrdering.carList = _mapper.Map<CarListViewModel>(ordering.Cars[0].CarList);
                }
                if (cloneOrdering.status != null )
                {
                    cloneOrdering.statusObj = orderingStatus.Where(o => o.id == cloneOrdering.status).FirstOrDefault();
                }
                foreach (OrderingProductNotificationViewModel productViewModel in cloneOrdering.products)
                {
                    if (productViewModel.productType != null)
                    {
                        string[] productTypeIds = productViewModel.productType.Split(",");
                        productViewModel.productTypes = new List<ProductTypeViewModel>();
                        foreach (string productId in productTypeIds)
                        {
                            int id = 0;
                            if (int.TryParse(productId, out id))
                            {
                                var productType = productTypes.Where(p => p.Id == id).FirstOrDefault();
                                if (productType != null)
                                    productViewModel.productTypes.Add(_mapper.Map<ProductTypeViewModel>(productType));
                            }
                        }
                    }
                    if (productViewModel.packaging != null)
                    {
                        string[] packagingIds = productViewModel.packaging.Split(",");
                        productViewModel.packagings = new List<ProductPackagingViewModel>();
                        foreach (string packagingId in packagingIds)
                        {
                            int id = 0;
                            if (int.TryParse(packagingId, out id))
                            {
                                var packaging = packagings.Where(p => p.Id == id).FirstOrDefault();
                                if (packaging != null)
                                    productViewModel.packagings.Add(_mapper.Map<ProductPackagingViewModel>(packaging));
                            }
                        }
                    }

                }

                if (ordering.Addresses.Count > 0)
                {
                    cloneOrdering.startPoint = _mapper.Map<OrderingAddressNotificationViewModel>(ordering.Addresses[0]);
                    cloneOrdering.endPoint = _mapper.Map<OrderingAddressNotificationViewModel>(ordering.Addresses[ordering.Addresses.Count - 1]);
                }



                orderingHistory.TrackingCode = ordering.TrackingCode;
                orderingHistory.OrderingId = ordering.Id;
                orderingHistory.Driver = ordering.Driver;
                orderingHistory.Customer = ordering.Customer;
                orderingHistory.ProductCBM = ordering.ProductCBM;
                orderingHistory.ProductTotalWeight = ordering.ProductTotalWeight;
                orderingHistory.OrderingPrice = ordering.OrderingPrice;
                orderingHistory.OrderingStatus = ordering.Status;
                orderingHistory.AdditionalDetail = ordering.AdditionalDetail;
                orderingHistory.Note = ordering.Note;
                orderingHistory.OrderNumber = ordering.TrackingCode;
                orderingHistory.OrderingDesiredPrice = ordering.OrderingDesiredPrice;
                orderingHistory.OrderingDriverOffering = ordering.OrderingDriverOffering;
                orderingHistory.IsOrderingDriverOffer = ordering.IsOrderingDriverOffer;
                orderingHistory.CancelStatus = ordering.CancelStatus;
                orderingHistory.CustomerRanking = ordering.CustomerRanking;
                orderingHistory.DriverRanking = ordering.DriverRanking;
                orderingHistory.PinnedDate = ordering.PinnedDate;
                orderingHistory.Distance = ordering.Distance;
                orderingHistory.EstimateTime = ordering.EstimateTime;
                orderingHistory.IsDriverPay = ordering.IsDriverPay;
                orderingHistory.CurrentLocation = ordering.CurrentLocation;
                orderingHistory.GPValue = ordering.GPValue;
                orderingHistory.DriverPayValue = ordering.DriverPayValue;
                orderingHistory.CustomerCancelValue = ordering.CustomerCancelValue;
                orderingHistory.DriverCancelValue = ordering.DriverCancelValue;
                orderingHistory.CustomerCashBack = ordering.CustomerCashBack;
                orderingHistory.DriverCashBack = ordering.DriverCashBack;
                orderingHistory.DriverAnnouncements = ordering.DriverAnnouncements;
                orderingHistory.Created = DateTime.UtcNow;
                orderingHistory.Modified = DateTime.UtcNow;


                await _orderingHistoryRepository.AddAsync(orderingHistory);

                #endregion
                #region send noti
                var oneSignalIds = (await _notificationUserRepository.FindByCondition(x => x.UserId.Equals(ordering.Customer.Id)).ConfigureAwait(false)).AsQueryable().Select(x => x.OneSignalId).ToList();
                var notificationDetails = Path.Combine(Directory.GetCurrentDirectory(), $"Shared\\NotificationWord.json");
                var notificationJson = System.IO.File.ReadAllText(notificationDetails);
                List<NotificationWordStatusViewModel> notificationStatus = Newtonsoft.Json.JsonConvert.DeserializeObject<List<NotificationWordStatusViewModel>>(notificationJson);

                string title = "";
                string detail = "";
                var notificationObj = notificationStatus.Where(o => o.id == 2).FirstOrDefault();
                if (notificationObj != null)
                {
                    title = notificationObj.title.th;
                    if (ordering.Customer != null)
                    {
                        detail = notificationObj.detail.th.Replace("#drivertitle#", driver.Title != null ? driver.Title.Name_TH : "")
                                 .Replace("#drivername#", driver.Name)
                                 .Replace("#orderprice#", request.DriverOffering != null || request.DriverOffering != 0 ? request.DriverOffering.ToString() : "-");
                    }
                }
                //if (cloneOrdering.products.Count > 2)
                //{
                //    cloneOrdering.products.RemoveRange(2, cloneOrdering.products.Count - 2);
                //}
                Domain.Notification notification = new Domain.Notification();

                notification.OneSignalId = oneSignalIds.FirstOrDefault();
                notification.UserId = ordering.Customer.Id;
                notification.OwnerId = request.DriverId;
                notification.Title = title;
                notification.Detail = detail;
                notification.ServiceCode = "Ordering";
                //notification.ReferenceContentKey = request.ReferenceContentKey;
                notification.Payload = Newtonsoft.Json.JsonConvert.SerializeObject(cloneOrdering);
                notification.Created = DateTime.UtcNow;
                notification.Modified = DateTime.UtcNow;
                notification.CreatedBy = request.DriverId.ToString();
                notification.ModifiedBy = request.DriverId.ToString();
                await _notificationRepository.AddAsync(notification);
                //#region insert to Inbox
                //CustomerInbox customerInbox = new CustomerInbox()
                //{
                //    CustomerId = ordering.Customer.Id,
                //    Title = "-",
                //    Title_ENG = "-",
                //    Module = "การแจ้งเตือน",
                //    Module_ENG = "Notification",
                //    Content = Newtonsoft.Json.JsonConvert.SerializeObject(cloneOrdering),
                //    FromUser = driver.FirstName + driver.LastName,
                //    IsDelete = false,
                //    Created = DateTime.Now
                //};
                //await _customerInboxRepository.AddAsync(customerInbox);
                //#endregion
                oneSignalIds = oneSignalIds.Where(x => x.Trim() != "").ToList();
                if (oneSignalIds.Count > 0)
                {
                    NotificationManager notiMgr = new NotificationManager(request.APP_ID, request.REST_API_KEY, request.AUTH_ID);
                    notiMgr.SendNotificationAsync(title, detail,  oneSignalIds, notification.ServiceCode, "", "{id:'" + cloneOrdering.id + "'}");
                }
                #endregion
                var log = new CreateAppLog(_applicationLogRepository);
                log.CreateLog("Ordering", "DriverSpecifyPrice", "Create", Newtonsoft.Json.JsonConvert.SerializeObject(request),request.UserId.ToString());
                return new Response<Guid>(ordering.Id);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
