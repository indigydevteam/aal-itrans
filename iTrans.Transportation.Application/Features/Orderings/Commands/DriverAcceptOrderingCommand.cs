﻿using AutoMapper;
using iTrans.Transportation.Application.DTOs.Ordering;
using iTrans.Transportation.Application.DTOs.OrderingAddress;
using iTrans.Transportation.Application.DTOs.OrderingAddressProduct;
using iTrans.Transportation.Application.DTOs.OrderingProduct;
using iTrans.Transportation.Application.DTOs.OrderingStatus;
using iTrans.Transportation.Application.DTOs.ProductPackaging;
using iTrans.Transportation.Application.DTOs.ProductType;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Features.Locations;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;


namespace iTrans.Transportation.Application.Features.Orderings.Commands
{
    public class DriverAcceptOrderingCommand : IRequest<Response<Guid>>
    {
        public virtual Guid? UserId { get; set; }
        public virtual int DriverReserveId { get; set; }
        public virtual Guid OrderingId { get; set; }
        public virtual Guid DriverId { get; set; }
        public int DriverAnnouncementId { get; set; }
        public string APP_ID { get; set; }
        public string REST_API_KEY { get; set; }
        public string AUTH_ID { get; set; }
    }
    public class DriverAcceptOrderingCommandHandler : IRequestHandler<DriverAcceptOrderingCommand, Response<Guid>>
    {
        private readonly IOrderingRepositoryAsync _orderingRepository;
        private readonly IDriverRepositoryAsync _driverRepository;
        private readonly IDriverActivityRepositoryAsync _driverActivityRepository;
        private readonly IOrderingDriverReserveRepositoryAsync _orderingDriverReserveRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IProductTypeRepositoryAsync _productTypeRepository;
        private readonly IProductPackagingRepositoryAsync _productPackagingRepository;
        private readonly IDriverAnnouncementRepositoryAsync _driverAnnouncementRepository;
        private readonly INotificationRepositoryAsync _notificationRepository;
        private readonly INotificationUserRepositoryAsync _notificationUserRepository;
        private readonly IOrderingHistoryRepositoryAsync _orderingHistoryRepository;
        private readonly ICustomerInboxRepositoryAsync _customerInboxRepository;
        private readonly IMapper _mapper;

        public DriverAcceptOrderingCommandHandler(IDriverRepositoryAsync driverRepository, IOrderingRepositoryAsync orderingRepository, IApplicationLogRepositoryAsync applicationLogRepository
            , IProductTypeRepositoryAsync productTypeRepository, IProductPackagingRepositoryAsync productPackagingRepository, IOrderingDriverReserveRepositoryAsync orderingDriverReserveRepository, IOrderingHistoryRepositoryAsync orderingHistoryRepository
            , INotificationRepositoryAsync notificationRepository, INotificationUserRepositoryAsync notificationUserRepository, IDriverAnnouncementRepositoryAsync driverAnnouncementRepository
            , ICustomerInboxRepositoryAsync customerInboxRepository, IDriverActivityRepositoryAsync driverActivityRepository
            , IMapper mapper)
        {
            _orderingRepository = orderingRepository;
            _driverRepository = driverRepository;
            _driverActivityRepository = driverActivityRepository;
            _applicationLogRepository = applicationLogRepository;
            _productTypeRepository = productTypeRepository;
            _productPackagingRepository = productPackagingRepository;
            _orderingDriverReserveRepository = orderingDriverReserveRepository;
            _notificationRepository = notificationRepository;
            _notificationUserRepository = notificationUserRepository;
            _driverAnnouncementRepository = driverAnnouncementRepository;
            _orderingHistoryRepository = orderingHistoryRepository;
            _customerInboxRepository = customerInboxRepository;
            _mapper = mapper;
        }

        public async Task<Response<Guid>> Handle(DriverAcceptOrderingCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var ordering = (await _orderingRepository.FindByCondition(x => x.Id == request.OrderingId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (ordering == null)
                {
                    throw new ApiException($"Ordering Not Found.");
                }
                var driver = (await _driverRepository.FindByCondition(x => x.Id == request.DriverId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (driver == null)
                {
                    throw new ApiException($"Driver Not Found.");
                }
                var existDriver = (await _driverRepository.FindByCondition(x => x.Id == request.DriverId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (existDriver != null)
                {
                    if (existDriver.Level == 1 )
                    {
                        var driverActivityRepository = (await _driverActivityRepository.FindByCondition(x => x.DriverId == ordering.Driver.Id && x.Date.Contains("yyyy'-'MM'")).ConfigureAwait(false)).AsQueryable().ToList();
                        int acceptjobCount = 0;

                        foreach (DriverActivity item in driverActivityRepository)
                        {
                            acceptjobCount = acceptjobCount + item.AcceptJob + item.Cancel;
                        }
                        if (ordering.OrderingDesiredPrice > 2000)
                        {
                            throw new ApiException("You can't get a job priced above 2000.");
                        }
                        if (acceptjobCount > 4)
                        {
                            throw new ApiException("You can't get more than 4 jobs per month.");
                        }
                    }
                }

                OrderingHistory orderingHistory = new OrderingHistory();

                var productTypes = (await _productTypeRepository.FindByCondition(x => x.Active.Equals(true)).ConfigureAwait(false)).AsQueryable().ToList();
                var packagings = (await _productPackagingRepository.FindByCondition(x => x.Active.Equals(true)).ConfigureAwait(false)).AsQueryable().ToList();

                var orderingDriverReserve = (await _orderingDriverReserveRepository.FindByCondition(x => x.Ordering.Id == request.OrderingId && x.Driver.Id == request.DriverId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                
                string orderStatusPath = $"Shared\\orderingstatus.json";
                var orderingStatusPathFile = Path.Combine(Directory.GetCurrentDirectory(), orderStatusPath);

                var statusJson = System.IO.File.ReadAllText(orderingStatusPathFile);
                List<OrderingStatusViewModel> orderingStatus = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OrderingStatusViewModel>>(statusJson);

                if (orderingDriverReserve == null)
                {
                    //throw new ApiException($"Ordering Driver Reserve Not Found.");
                    orderingDriverReserve = new OrderingDriverReserve();

                    orderingDriverReserve.Ordering = ordering;
                    orderingDriverReserve.Driver = driver;
                    orderingDriverReserve.Status = 4;
                    if (ordering.DriverReserve == null)
                    {
                        ordering.DriverReserve = new List<OrderingDriverReserve>();
                    }
                    ordering.DriverReserve.Add(orderingDriverReserve);
                    //if (orderingHistory.DriverReserve == null)
                    //{
                    //    orderingHistory.DriverReserve = new List<OrderingDriverReserve>();
                    //}
                    //orderingHistory.DriverReserve.Add(orderingDriverReserve);
                }
                if (orderingDriverReserve.DriverAnnouncement != null)
                {
                    var driverAnnouncement = (await _driverAnnouncementRepository.FindByCondition(x => x.Id.Equals(orderingDriverReserve.DriverAnnouncement.Id) && x.Driver.Id.Equals(request.DriverId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                    if (driverAnnouncement == null)
                    {

                    }
                    else
                    {
                        driverAnnouncement.Status = 2; //"gotjob";
                        driverAnnouncement.Order = ordering;
                        await _driverAnnouncementRepository.UpdateAsync(driverAnnouncement);

                    }
                }
                ordering.Status = 2;
                var orderingStatusObject = orderingStatus.Where(o => o.id == ordering.Status).FirstOrDefault();
                ordering.StatusName_Str = orderingStatusObject != null ? orderingStatusObject.th : "-";

                foreach (OrderingAddress address in ordering.Addresses)
                {
                    address.Status = 2;
                }

                await _orderingRepository.UpdateAsync(ordering);
                //driver.AcceptJobPerMonth = driver.AcceptJobPerMonth + 1;
                //driver.AcceptJobPerYear = driver.AcceptJobPerYear + 1;
                await _driverRepository.UpdateAsync(driver);

                //var driverActivity = (await _driverActivityRepository.FindByCondition(x => x.DriverId.Equals(request.DriverId) && x.Date == DateTime.Now.ToString("yyyy'-'MM'-'dd")).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                //if (driverActivity == null)
                //{
                //    DriverActivity newDriverActivity = new DriverActivity
                //    {
                //        DriverId = request.DriverId,
                //        Date = DateTime.Now.ToString("yyyy'-'MM'-'dd"),
                //        Star = 0,
                //        AcceptJob = 1,
                //        Complaint = 0,
                //        Reject = 0,
                //        Cancel = 0,
                //        InsuranceValue = 0,
                //        Created = DateTime.Now,
                //        CreatedBy = request.UserId != null ? request.UserId.Value.ToString() : "-",
                //        Modified = DateTime.Now,
                //        ModifiedBy = request.UserId != null ? request.UserId.Value.ToString() : "-",
                //    };
                //    await _driverActivityRepository.AddAsync(newDriverActivity);
                //}
                //else
                //{
                //    driverActivity.AcceptJob = driverActivity.AcceptJob + 1;
                //    await _driverActivityRepository.UpdateAsync(driverActivity);
                //}

                orderingDriverReserve.Status = 4;
                orderingDriverReserve.Modified = DateTime.Now;
                orderingDriverReserve.ModifiedBy = request.UserId != null ? request.UserId.Value.ToString() : "";
                await _orderingDriverReserveRepository.UpdateAsync(orderingDriverReserve);



                orderingHistory.TrackingCode = ordering.TrackingCode;
                orderingHistory.OrderingId = ordering.Id;
                orderingHistory.Driver = ordering.Driver;
                orderingHistory.Customer = ordering.Customer;
                orderingHistory.ProductCBM = ordering.ProductCBM;
                orderingHistory.ProductTotalWeight = ordering.ProductTotalWeight;
                orderingHistory.OrderingPrice = ordering.OrderingPrice;
                orderingHistory.OrderingStatus = ordering.Status;
                orderingHistory.AdditionalDetail = ordering.AdditionalDetail;
                orderingHistory.Note = ordering.Note;
                orderingHistory.OrderNumber = ordering.TrackingCode;
                orderingHistory.OrderingDesiredPrice = ordering.OrderingDesiredPrice;
                orderingHistory.OrderingDriverOffering = ordering.OrderingDriverOffering;
                orderingHistory.IsOrderingDriverOffer = ordering.IsOrderingDriverOffer;
                orderingHistory.CancelStatus = ordering.CancelStatus;
                orderingHistory.CustomerRanking = ordering.CustomerRanking;
                orderingHistory.DriverRanking = ordering.DriverRanking;
                orderingHistory.PinnedDate = ordering.PinnedDate;
                orderingHistory.Distance = ordering.Distance;
                orderingHistory.EstimateTime = ordering.EstimateTime;
                orderingHistory.IsDriverPay = ordering.IsDriverPay;
                orderingHistory.CurrentLocation = ordering.CurrentLocation;
                orderingHistory.GPValue = ordering.GPValue;
                orderingHistory.DriverPayValue = ordering.DriverPayValue;
                orderingHistory.CustomerCancelValue = ordering.CustomerCancelValue;
                orderingHistory.DriverCancelValue = ordering.DriverCancelValue;
                orderingHistory.CustomerCashBack = ordering.CustomerCashBack;
                orderingHistory.DriverCashBack = ordering.DriverCashBack;
                orderingHistory.DriverAnnouncements = ordering.DriverAnnouncements;
                orderingHistory.IsMutipleRoutes = ordering.IsMutipleRoutes;
                orderingHistory.Created = DateTime.UtcNow;
                orderingHistory.Modified = DateTime.UtcNow;
                orderingHistory.CreatedBy = request.UserId != null ? request.UserId.Value.ToString() : "";
                orderingHistory.ModifiedBy = request.UserId != null ? request.UserId.Value.ToString() : "";

                await _orderingHistoryRepository.AddAsync(orderingHistory);



                #region
                //var folderDetails = Path.Combine(Directory.GetCurrentDirectory(), $"Shared\\customer_orderingStatus.json");
                //var statusJson = System.IO.File.ReadAllText(folderDetails);
                //List<OrderingStatusViewModel> orderingStatus = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OrderingStatusViewModel>>(statusJson);

                //var cloneOrdering = _mapper.Map<OrderingViewModel>(ordering);
                var cloneOrdering = new OrderingNotificationViewModel
                {
                    id = ordering.Id,
                    status = ordering.Status,
                    trackingCode = ordering.TrackingCode,
                    orderNumber = ordering.TrackingCode,
                    products = _mapper.Map<List<OrderingProductNotificationViewModel>>(ordering.Products)
                };

                if (cloneOrdering.status != null)
                {
                    cloneOrdering.statusObj = orderingStatus.Where(o => o.id == cloneOrdering.status).FirstOrDefault();
                }
                foreach (OrderingProductNotificationViewModel productViewModel in cloneOrdering.products)
                {
                    if (productViewModel.productType != null)
                    {
                        string[] productTypeIds = productViewModel.productType.Split(",");
                        productViewModel.productTypes = new List<ProductTypeViewModel>();
                        foreach (string productId in productTypeIds)
                        {
                            int id = 0;
                            if (int.TryParse(productId, out id))
                            {
                                var productType = productTypes.Where(p => p.Id == id).FirstOrDefault();
                                if (productType != null)
                                    productViewModel.productTypes.Add(_mapper.Map<ProductTypeViewModel>(productType));
                            }
                        }
                    }
                    if (productViewModel.packaging != null)
                    {
                        string[] packagingIds = productViewModel.packaging.Split(",");
                        productViewModel.packagings = new List<ProductPackagingViewModel>();
                        foreach (string packagingId in packagingIds)
                        {
                            int id = 0;
                            if (int.TryParse(packagingId, out id))
                            {
                                var packaging = packagings.Where(p => p.Id == id).FirstOrDefault();
                                if (packaging != null)
                                    productViewModel.packagings.Add(_mapper.Map<ProductPackagingViewModel>(packaging));
                            }
                        }
                    }

                }
                if (ordering.Addresses.Count > 0)
                {
                    cloneOrdering.startPoint = _mapper.Map<OrderingAddressNotificationViewModel>(ordering.Addresses[0]);
                    cloneOrdering.endPoint = _mapper.Map<OrderingAddressNotificationViewModel>(ordering.Addresses[ordering.Addresses.Count - 1]);
                }

                #endregion
                #region send noti
                var oneSignalIds = (await _notificationUserRepository.FindByCondition(x => x.UserId.Equals(ordering.Customer.Id)).ConfigureAwait(false)).AsQueryable().Select(x => x.OneSignalId).ToList();
                var notificationDetails = Path.Combine(Directory.GetCurrentDirectory(), $"Shared\\NotificationWord.json");
                var notificationJson = System.IO.File.ReadAllText(notificationDetails);
                List<NotificationWordStatusViewModel> notificationStatus = Newtonsoft.Json.JsonConvert.DeserializeObject<List<NotificationWordStatusViewModel>>(notificationJson);

                string title = "";
                string detail = "";
                var notificationObj = notificationStatus.Where(o => o.id == 3).FirstOrDefault();
                if (notificationObj != null)
                {
                    title = notificationObj.title.th;
                    if (ordering.Customer != null)
                    {
                        detail = notificationObj.detail.th.Replace("#carregistration#", driver.Cars.Count > 0 ? driver.Cars[0].CarRegistration : "")
                                 .Replace("#driverstar#", driver.Star.ToString())
                                 .Replace("#driverlevel#", driver.Level.ToString());
                        if (orderingDriverReserve.DriverOfferingPrice > 0)
                        {
                            detail = detail.Replace("#orderprice#", orderingDriverReserve.DriverOfferingPrice.ToString());
                        }
                        else
                        {
                            detail = detail.Replace("#orderprice#", ordering.OrderingDesiredPrice.ToString());
                        }
                    }
                }
                //if (cloneOrdering.products.Count > 2)
                //{
                //    cloneOrdering.products.RemoveRange(2, cloneOrdering.products.Count - 2);
                //}
                Domain.Notification notification = new Domain.Notification();

                notification.OneSignalId = oneSignalIds.FirstOrDefault();
                notification.UserId = ordering.Customer.Id;
                notification.OwnerId = request.DriverId;
                notification.Title = title;
                notification.Detail = detail;
                notification.ServiceCode = "Ordering";
                //notification.ReferenceContentKey = request.ReferenceContentKey;
                notification.Payload = Newtonsoft.Json.JsonConvert.SerializeObject(cloneOrdering);
                notification.Created = DateTime.UtcNow;
                notification.Modified = DateTime.UtcNow;
                notification.CreatedBy = request.DriverId.ToString();
                notification.ModifiedBy = request.DriverId.ToString();
                await _notificationRepository.AddAsync(notification);
                #region insert to Inbox
                //CustomerInbox customerInbox = new CustomerInbox()
                //{
                //    CustomerId = ordering.Customer.Id,
                //    Title = "-",
                //    Title_ENG = "-",
                //    Module = "การแจ้งเตือน",
                //    Module_ENG = "Notification",
                //    Content = Newtonsoft.Json.JsonConvert.SerializeObject(cloneOrdering),
                //    FromUser = driver.FirstName + driver.LastName,
                //    IsDelete = false,
                //    Created = DateTime.Now
                //};
                //await _customerInboxRepository.AddAsync(customerInbox);
                #endregion
                oneSignalIds = oneSignalIds.Where(x => x.Trim() != "").ToList();
                if (oneSignalIds.Count > 0)
                {
                    NotificationManager notiMgr = new NotificationManager(request.APP_ID, request.REST_API_KEY, request.AUTH_ID);
                    notiMgr.SendNotificationAsync(title, detail, oneSignalIds, notification.ServiceCode, "", "{id:'" + cloneOrdering.id + "'}");
                }
                #endregion
                var log = new CreateAppLog(_applicationLogRepository);
                log.CreateLog("Ordering", "DriverAcceptOrdering", "Edit", Newtonsoft.Json.JsonConvert.SerializeObject(request), request.UserId.ToString());
                return new Response<Guid>(ordering.Id);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
