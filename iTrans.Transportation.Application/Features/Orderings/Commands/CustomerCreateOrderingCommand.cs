﻿using AutoMapper;
using iTrans.Transportation.Application.DTOs.OrderingAddress;
using iTrans.Transportation.Application.DTOs.OrderingAddressProduct;
using iTrans.Transportation.Application.DTOs.OrderingCar;
using iTrans.Transportation.Application.DTOs.OrderingContainer;
//using iTrans.Transportation.Application.DTOs.OrderingContainerAddress;
using iTrans.Transportation.Application.DTOs.OrderingProduct;
using iTrans.Transportation.Application.Enums;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using Microsoft.Extensions.Configuration;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using iTrans.Transportation.Application.DTOs.Ordering;
using iTrans.Transportation.Application.DTOs.OrderingStatus;
using iTrans.Transportation.Application.DTOs.ProductType;
using iTrans.Transportation.Application.DTOs.ProductPackaging;
using iTrans.Transportation.Application.DTOs.CarType;
using iTrans.Transportation.Application.DTOs.CarList;
using iTrans.Transportation.Application.Helper;
using NHibernate.Driver;
using iTrans.Transportation.Application.Features.Locations;

namespace iTrans.Transportation.Application.Features.Orderings.Commands
{
    public partial class CustomerCreateOrderingCommand : IRequest<Response<Guid>>
    {
        public Guid CustomerId { get; set; }
        public Guid DriverId { get; set; }
        public int DriverAnnouncementId { get; set; }
        //public bool IsRequestTax { get; set; }
        //public int TransportType { get; set; }
        public CreateCarViweModel Cars { get; set; }
        public List<CreateProductViewModel> Products { get; set; }
        public int ProductCBM { get; set; }
        public float ProductTotalWeight { get; set; }
        public string ProductAdditionalDetail { get; set; }
        public string ProductNote { get; set; }
        public decimal OrderingDesiredPrice { get; set; }
        public bool IsOrderingDriverOffer { get; set; }
        public bool IsMutipleRoutes { get; set; }
        public List<CreateAddressViewModel> Addresses { get; set; }
        public List<CreateContainerViewModel> Containers { get; set; }
        public string DRIVER_APP_ID { get; set; }
        public string DRIVER_REST_API_KEY { get; set; }
        public string DRIVER_AUTH_ID { get; set; }
    }
    public class CustomerCreateOrderingCommandHandler : IRequestHandler<CustomerCreateOrderingCommand, Response<Guid>>
    {
        private readonly ICustomerRepositoryAsync _customerRepository;
        //private readonly ICustomerLevelRepositoryAsync _customerLevelRepository;
        private readonly IDriverRepositoryAsync _driverRepository;
        private readonly IDriverAnnouncementRepositoryAsync _driverAnnouncementRepository;
        private readonly ICustomerActivityRepositoryAsync _customerActivityRepository;
        private readonly ICustomerProductRepositoryAsync _customerProductRepository;
        private readonly IOrderingRepositoryAsync _orderingRepository;
        private readonly IOrderingProductRepositoryAsync _orderingProductRepository;
        private readonly IOrderingCarRepositoryAsync _orderingCarRepository;
        private readonly IOrderingAddressRepositoryAsync _orderingAddressRepository;
        private readonly IProductTypeRepositoryAsync _productTypeRepository;
        private readonly IProductPackagingRepositoryAsync _productPackagingRepository;

        private readonly IContainerTypeRepositoryAsync _containerTypeRepository;
        private readonly IContainerSpecificationRepositoryAsync _containerSpecificationRepository;
        private readonly IEnergySavingDeviceRepositoryAsync _energySavingDeviceRepository;

        private readonly ICarTypeRepositoryAsync _carTypeRepository;
        private readonly ICarListRepositoryAsync _carListRepository;
        private readonly ICarDescriptionRepositoryAsync _carDescriptionRepository;
        private readonly ICarSpecificationRepositoryAsync _carSpecificationRepository;
        private readonly ICarFeatureRepositoryAsync _carFeatureRepository;
        private readonly ICountryRepositoryAsync _countryRepository;
        private readonly IProvinceRepositoryAsync _provinceRepository;
        private readonly IDistrictRepositoryAsync _districtRepository;
        private readonly ISubdistrictRepositoryAsync _subdistrictRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;

        private readonly INotificationRepositoryAsync _notificationRepository;
        private readonly INotificationUserRepositoryAsync _notificationUserRepository;
        private readonly IOrderingHistoryRepositoryAsync _orderingHistoryRepository;
        private readonly IDriverInboxRepositoryAsync _driverInboxRepository;

        private readonly IMapper _mapper;
        private readonly IConfiguration _configuration;
        public CustomerCreateOrderingCommandHandler(ICustomerRepositoryAsync customerRepository, IDriverRepositoryAsync driverRepository, ICustomerProductRepositoryAsync customerProductRepository, IOrderingRepositoryAsync orderingRepository, IOrderingProductRepositoryAsync orderingProductRepository, IOrderingCarRepositoryAsync orderingCarRepository
            , IOrderingAddressRepositoryAsync orderingAddressRepository, IProductTypeRepositoryAsync productTypeRepository, IProductPackagingRepositoryAsync productPackagingRepository
            , ICarTypeRepositoryAsync carTypeRepository, ICarListRepositoryAsync carListRepository, ICarDescriptionRepositoryAsync carDescriptionRepository, ICarSpecificationRepositoryAsync carSpecificationRepository
            , ICarFeatureRepositoryAsync carFeatureRepository, ICountryRepositoryAsync countryRepository, IProvinceRepositoryAsync provinceRepository, IDistrictRepositoryAsync districtRepository
            , ISubdistrictRepositoryAsync subdistrictRepository, IApplicationLogRepositoryAsync applicationLogRepository
            , INotificationRepositoryAsync notificationRepository, INotificationUserRepositoryAsync notificationUserRepository, IContainerTypeRepositoryAsync containerTypeRepository, IContainerSpecificationRepositoryAsync containerSpecificationRepository, IEnergySavingDeviceRepositoryAsync energySavingDeviceRepository
            , IMapper mapper, IConfiguration configuration, IDriverAnnouncementRepositoryAsync driverAnnouncementRepository, IOrderingHistoryRepositoryAsync orderingHistoryRepository, IDriverInboxRepositoryAsync driverInboxRepository
            , ICustomerActivityRepositoryAsync customerActivityRepository
            )
        {
            _customerRepository = customerRepository;
            //_customerLevelRepository = customerLevelRepository;
            _driverRepository = driverRepository;
            _driverAnnouncementRepository = driverAnnouncementRepository;
            _customerProductRepository = customerProductRepository;
            _orderingRepository = orderingRepository;
            _orderingProductRepository = orderingProductRepository;
            _orderingAddressRepository = orderingAddressRepository;
            _orderingCarRepository = orderingCarRepository;
            _productTypeRepository = productTypeRepository;
            _productPackagingRepository = productPackagingRepository;
            _carTypeRepository = carTypeRepository;
            _carListRepository = carListRepository;
            _carDescriptionRepository = carDescriptionRepository;
            _carSpecificationRepository = carSpecificationRepository;
            _carFeatureRepository = carFeatureRepository;
            _countryRepository = countryRepository;
            _provinceRepository = provinceRepository;
            _districtRepository = districtRepository;
            _subdistrictRepository = subdistrictRepository;
            _applicationLogRepository = applicationLogRepository;
            _containerTypeRepository = containerTypeRepository;
            _containerSpecificationRepository = containerSpecificationRepository;
            _energySavingDeviceRepository = energySavingDeviceRepository;
            _notificationRepository = notificationRepository;
            _notificationUserRepository = notificationUserRepository;
            _orderingHistoryRepository = orderingHistoryRepository;
            _driverInboxRepository = driverInboxRepository;
            _customerActivityRepository = customerActivityRepository;
            _mapper = mapper;
            _configuration = configuration;

        }

        public async Task<Response<Guid>> Handle(CustomerCreateOrderingCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var customerObject = (await _customerRepository.FindByCondition(x => x.Id.Equals(request.CustomerId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (customerObject == null)
                {
                    throw new ApiException($"Customer Not Found.");
                }
                string currentTimeStr = DateTime.Now.ToString("yyyyMMddHHmmss");
                int detectArea = Convert.ToInt32(_configuration.GetSection("DetectArea").Value);
                string api_key = _configuration.GetSection("GoogleAPIKEY").Value;

                var carTypes = (await _carTypeRepository.FindByCondition(x => x.Active.Equals(true)).ConfigureAwait(false)).AsQueryable().ToList();
                var carLists = (await _carListRepository.FindByCondition(x => x.Active.Equals(true)).ConfigureAwait(false)).AsQueryable().ToList();
                var carDescriptions = (await _carDescriptionRepository.FindByCondition(x => x.Active.Equals(true)).ConfigureAwait(false)).ToList().ToList();
                var carSpecifications = (await _carSpecificationRepository.FindByCondition(x => x.Active.Equals(true)).ConfigureAwait(false)).AsQueryable().ToList();
                var carFeatures = (await _carFeatureRepository.FindByCondition(x => x.Active.Equals(true)).ConfigureAwait(false)).AsQueryable().ToList();
                var productTypes = (await _productTypeRepository.FindByCondition(x => x.Active.Equals(true)).ConfigureAwait(false)).AsQueryable().ToList();
                var packagings = (await _productPackagingRepository.FindByCondition(x => x.Active.Equals(true)).ConfigureAwait(false)).AsQueryable().ToList();
                var containerTypes = (await _containerTypeRepository.FindByCondition(x => x.Active.Equals(true)).ConfigureAwait(false)).AsQueryable().ToList();
                var containerSpecifications = (await _containerSpecificationRepository.FindByCondition(x => x.Active.Equals(true)).ConfigureAwait(false)).AsQueryable().ToList();
                var energySavingDevices = (await _energySavingDeviceRepository.FindByCondition(x => x.Active.Equals(true)).ConfigureAwait(false)).AsQueryable().ToList();

                string orderStatusPath = $"Shared\\orderingstatus.json";
                var orderingStatusPathFile = Path.Combine(Directory.GetCurrentDirectory(), orderStatusPath);

                var statusJson = System.IO.File.ReadAllText(orderingStatusPathFile);
                List<OrderingStatusViewModel> orderingStatus = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OrderingStatusViewModel>>(statusJson);

                var getContentPath = _configuration.GetSection("ContentPath");
                string contentPath = getContentPath.Value; //"D://iTrans/content/";

                Ordering ordering = new Ordering();
                OrderingHistory orderingHistory = new OrderingHistory();

                ordering.Customer = customerObject;
                ordering.CustomerName_Str = customerObject.Name;
                //ordering.IsRequestTax = request.IsRequestTax;
                //ordering.TransportType = request.TransportType;
                ordering.ProductCBM = request.ProductCBM;
                ordering.ProductTotalWeight = request.ProductTotalWeight;
                ordering.AdditionalDetail = request.ProductAdditionalDetail;
                ordering.OrderingDesiredPrice = request.OrderingDesiredPrice;
                ordering.IsOrderingDriverOffer = request.IsOrderingDriverOffer;
                ordering.IsMutipleRoutes = request.IsMutipleRoutes;
                ordering.Note = request.ProductNote;
                ordering.Created = DateTime.Now;
                ordering.Modified = DateTime.Now;
                ordering.CreatedBy = request.CustomerId != null ? request.CustomerId.ToString() : "";
                ordering.ModifiedBy = request.CustomerId != null ? request.CustomerId.ToString() : "";

                ordering.Status = 1;
                var orderingStatusObject = orderingStatus.Where(o => o.id == ordering.Status).FirstOrDefault();
                ordering.StatusName_Str = orderingStatusObject != null ? orderingStatusObject.th : "-";

                if (request.Cars != null)
                {
                    List<OrderingCar> cars = new List<OrderingCar>();
                    var car = _mapper.Map<OrderingCar>(request.Cars);
                    car.Ordering = ordering;
                    car.CarType = carTypes.Where(c => c.Id.Equals(request.Cars.CarTypeId)).FirstOrDefault();
                    car.CarList = carLists.Where(c => c.Id.Equals(request.Cars.CarListId)).FirstOrDefault();
                    car.CarDescription = carDescriptions.Where(c => c.Id.Equals(request.Cars.CarDescriptionId)).FirstOrDefault();
                    car.CarSpecification = carSpecifications.Where(c => c.Id.Equals(request.Cars.CarSpecificationId)).FirstOrDefault();
                    car.CarFeature = carFeatures.Where(c => c.Id.Equals(request.Cars.CarFeatureId)).FirstOrDefault();
                    car.EnergySavingDevice = energySavingDevices.Where(c => c.Id == request.Cars.EnergySavingDeviceId).FirstOrDefault();
                    ordering.IsRequestTax = car.IsRequestTax;
                    ordering.TransportType = car.TransportType;
                    car.Created = DateTime.Now;
                    car.Modified = DateTime.Now;
                    car.CreatedBy = request.CustomerId != null ? request.CustomerId.ToString() : "";
                    car.ModifiedBy = request.CustomerId != null ? request.CustomerId.ToString() : "";
                    cars.Add(car);
                    ordering.Cars = cars;
                    orderingHistory.Cars = cars;
                }

                if (request.Products != null)
                {
                    int productCount = 0;
                    List<OrderingProduct> products = new List<OrderingProduct>();
                    foreach (CreateProductViewModel productView in request.Products)
                    {
                        productCount = productCount + 1;
                        var product = _mapper.Map<OrderingProduct>(productView);
                        product.Ordering = ordering;
                        product.ProductType = "";
                        foreach (int productId in productView.ProductType)
                        {
                            var productType = productTypes.Where(p => p.Id == productId).FirstOrDefault();
                            if (productType != null)
                                product.ProductType = product.ProductType + productId + ",";
                        }
                        if (product.ProductType != "")
                            product.ProductType = product.ProductType.Remove(product.ProductType.Length - 1);
                        product.Packaging = "";
                        foreach (int packagingId in productView.Packaging)
                        {
                            var packaging = packagings.Where(p => p.Id == packagingId).FirstOrDefault();
                            if (packaging != null)
                                product.Packaging = product.Packaging + packagingId + ",";
                        }
                        if (product.Packaging != "")
                            product.Packaging = product.Packaging.Remove(product.Packaging.Length - 1);
                        //product.ProductType = productTypes.Where(p => p.Id.Equals(productView.ProductTypeId)).FirstOrDefault();
                        //product.Packaging = packagings.Where(p => p.Id.Equals(productView.PackagingId)).FirstOrDefault();
                        string folderPath = contentPath + "ordering/" + request.CustomerId.ToString() + "/" + currentTimeStr + "/product/" + productCount + "/";
                        if (!Directory.Exists(folderPath))
                        {
                            Directory.CreateDirectory(folderPath);
                        }
                        int fileCount = 0;
                        List<OrderingProductFile> productFiles = new List<OrderingProductFile>();
                        if (productView.Files != null)
                        {
                            foreach (IFormFile file in productView.Files)
                            {
                                fileCount = fileCount + 1;
                                string fileName = file.FileName;
                                string contentType = file.ContentType;
                                if (fileName.Trim() == "" || fileName.ToLower().Trim() == "blob")
                                {
                                    fileName = DateTime.Now.ToString("ddMMyyyyHHmmss") + fileCount.ToString("D" + 4) + ".jpg";
                                    contentType = "image/jpeg";
                                }
                                string filePath = Path.Combine(folderPath, fileName);
                                if (File.Exists(filePath))
                                    File.Delete(filePath);

                                using (FileStream fileStream = new FileStream(filePath, FileMode.Create))
                                {
                                    await file.CopyToAsync(fileStream);
                                    FileInfo fi = new FileInfo(filePath);

                                    OrderingProductFile orderingProductFile = new OrderingProductFile
                                    {
                                        OrderingProduct = product,
                                        FileName = fileName,
                                        ContentType = contentType,
                                        FilePath = "ordering/" + request.CustomerId.ToString() + "/" + currentTimeStr + "/product/" + productCount + "/" + fileName,
                                        DirectoryPath = "ordering/" + request.CustomerId.ToString() + "/" + currentTimeStr + "/product/" + productCount + "/",
                                        FileEXT = fi.Extension,
                                        Created = DateTime.Now,
                                        Modified = DateTime.Now,
                                        CreatedBy = request.CustomerId != null ? request.CustomerId.ToString() : "",
                                        ModifiedBy = request.CustomerId != null ? request.CustomerId.ToString() : "",
                                    };
                                    productFiles.Add(orderingProductFile);
                                }
                            }
                        }
                        if (productView.CustomerProductId != null && productView.CustomerProductId != 0)
                        {
                            var customerProductObject = (await _customerProductRepository.FindByCondition(x => x.Id.Equals(productView.CustomerProductId) && x.CustomerId.Equals(request.CustomerId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                            if (customerProductObject != null)
                            {
                                foreach (CustomerProductFile productFile in customerProductObject.ProductFiles)
                                {
                                    if (File.Exists(Path.Combine(contentPath, productFile.FilePath)))
                                    {
                                        string fileName = productFile.FileName;
                                        string contentType = productFile.ContentType;
                                        if (fileName.Trim() == "" || fileName.ToLower().Trim() == "blob")
                                        {
                                            fileName = DateTime.Now.ToString("ddMMyyyyHHmmss") + fileCount.ToString("D" + 4) + ".jpg";
                                            contentType = "image/jpeg";
                                        }
                                        string filePath = Path.Combine(folderPath, fileName);
                                        if (File.Exists(filePath))
                                            File.Delete(filePath);
                                        File.Copy(Path.Combine(contentPath, productFile.FilePath), filePath);
                                        OrderingProductFile orderingProductFile = new OrderingProductFile
                                        {
                                            OrderingProduct = product,
                                            FileName = fileName,
                                            ContentType = contentType,
                                            FilePath = "ordering/" + request.CustomerId.ToString() + "/" + currentTimeStr + "/product/" + productCount + "/" + fileName,
                                            DirectoryPath = "ordering/" + request.CustomerId.ToString() + "/" + currentTimeStr + "/product/" + productCount + "/",
                                            FileEXT = productFile.FileEXT,
                                            Created = DateTime.Now,
                                            Modified = DateTime.Now,
                                            CreatedBy = request.CustomerId != null ? request.CustomerId.ToString() : "",
                                            ModifiedBy = request.CustomerId != null ? request.CustomerId.ToString() : "",
                                        };
                                        productFiles.Add(orderingProductFile);
                                    }
                                }
                            }
                        }
                        product.ProductFiles = productFiles;
                        products.Add(product);
                    }
                    ordering.Products = products;
                    orderingHistory.Products = products;
                }

                if (request.Addresses != null)
                {
                    List<OrderingAddress> addresses = new List<OrderingAddress>();
                    int addressCount = 0;
                    foreach (CreateAddressViewModel addressView in request.Addresses)
                    {
                        addressCount = addressCount + 1;
                        var address = _mapper.Map<OrderingAddress>(addressView);
                        Country country = (await _countryRepository.FindByCondition(x => x.Id.Equals(addressView.CountryId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                        Province province = (await _provinceRepository.FindByCondition(x => x.Id.Equals(addressView.ProvinceId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                        District district = (await _districtRepository.FindByCondition(x => x.Id.Equals(addressView.DistrictId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                        Subdistrict subdistrict = (await _subdistrictRepository.FindByCondition(x => x.Id.Equals(addressView.SubdistrictId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();

                        string phoneNumber = addressView.PhoneNumber != null ? addressView.PhoneNumber.Trim() : "";
                        if (addressView.PhoneCode == "+66" && addressView.PhoneNumber != null && addressView.PhoneNumber.Length > 9 && addressView.PhoneNumber.First() == '0')
                        {
                            phoneNumber = addressView.PhoneNumber.Remove(0, 1);
                        }
                        addressView.PhoneNumber = phoneNumber;

                        List<OrderingAddressProduct> addressProducts = new List<OrderingAddressProduct>();
                        if (addressView.Products != null)
                        {
                            int productCount = 0;
                            foreach (CreateProductFormViewModel viewProduct in addressView.Products)
                            {
                                var selProduct = request.Products.Where(p => p.Sequence.Equals(viewProduct.Product)).FirstOrDefault();
                                if (selProduct != null)
                                {
                                    productCount = productCount + 1;
                                    var addressProduct = _mapper.Map<OrderingAddressProduct>(selProduct);

                                    addressProduct.ProductType = "";
                                    foreach (int productId in selProduct.ProductType)
                                    {
                                        var productType = productTypes.Where(p => p.Id == productId).FirstOrDefault();
                                        if (productType != null)
                                            addressProduct.ProductType = addressProduct.ProductType + productId + ",";
                                    }
                                    if (addressProduct.ProductType != "")
                                        addressProduct.ProductType = addressProduct.ProductType.Remove(addressProduct.ProductType.Length - 1);
                                    addressProduct.Packaging = "";
                                    foreach (int packagingId in selProduct.Packaging)
                                    {
                                        var packaging = packagings.Where(p => p.Id == packagingId).FirstOrDefault();
                                        if (packaging != null)
                                            addressProduct.Packaging = addressProduct.Packaging + packagingId + ",";
                                    }
                                    if (addressProduct.Packaging != "")
                                        addressProduct.Packaging = addressProduct.Packaging.Remove(addressProduct.Packaging.Length - 1);

                                    addressProduct.OrderingProduct = ordering.Products.Where(p => p.Sequence.Equals(viewProduct.Product)).FirstOrDefault();
                                    addressProduct.Quantity = viewProduct.Quantity;
                                    addressProduct.Sequence = viewProduct.Sequence;
                                    addressProduct.OrderingAddress = address;
                                    //addressProduct.ProductType = productTypes.Where(p => p.Id.Equals(selProduct.ProductTypeId)).FirstOrDefault();
                                    //addressProduct.Packaging = packagings.Where(p => p.Id.Equals(selProduct.PackagingId)).FirstOrDefault();
                                    addressProducts.Add(addressProduct);
                                    string folderPath = contentPath + "ordering/" + request.CustomerId.ToString() + "/" + currentTimeStr + "/addressproduct/" + addressCount + "/" + productCount + "/";
                                    if (!Directory.Exists(folderPath))
                                    {
                                        Directory.CreateDirectory(folderPath);
                                    }
                                    int fileCount = 0;
                                    List<OrderingAddressProductFile> AddressProductFiles = new List<OrderingAddressProductFile>();
                                    if (selProduct.Files != null)
                                    {

                                        foreach (IFormFile file in selProduct.Files)
                                        {
                                            fileCount = fileCount + 1;
                                            string fileName = file.FileName;
                                            string contentType = file.ContentType;
                                            if (fileName.Trim() == "" || fileName.ToLower().Trim() == "blob")
                                            {
                                                fileName = DateTime.Now.ToString("ddMMyyyyHHmmss") + fileCount.ToString("D" + 4) + ".jpg";
                                                contentType = "image/jpeg";
                                            }
                                            string filePath = Path.Combine(folderPath, fileName);
                                            using (FileStream fileStream = new FileStream(filePath, FileMode.Create))
                                            {
                                                await file.CopyToAsync(fileStream);
                                                FileInfo fi = new FileInfo(filePath);

                                                OrderingAddressProductFile addressProductFile = new OrderingAddressProductFile
                                                {
                                                    OrderingAddressProduct = addressProduct,
                                                    FileName = fileName,
                                                    ContentType = contentType,
                                                    FilePath = "ordering/" + request.CustomerId.ToString() + "/" + currentTimeStr + "/addressproduct/" + addressCount + "/" + productCount + "/" + fileName,
                                                    DirectoryPath = "ordering/" + request.CustomerId.ToString() + "/" + currentTimeStr + "/addressproduct/" + addressCount + "/" + productCount + "/",
                                                    FileEXT = fi.Extension,
                                                    Type = "fromordering",
                                                    Created = DateTime.Now,
                                                    Modified = DateTime.Now,
                                                    CreatedBy = request.CustomerId != null ? request.CustomerId.ToString() : "",
                                                    ModifiedBy = request.CustomerId != null ? request.CustomerId.ToString() : "",
                                                };
                                                AddressProductFiles.Add(addressProductFile);
                                            }
                                        }
                                    }
                                    if (selProduct.CustomerProductId != null && selProduct.CustomerProductId != 0)
                                    {
                                        var customerProductObject = (await _customerProductRepository.FindByCondition(x => x.Id.Equals(selProduct.CustomerProductId) && x.CustomerId.Equals(request.CustomerId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                                        if (customerProductObject != null)
                                        {
                                            fileCount = 0;

                                            foreach (CustomerProductFile productFile in customerProductObject.ProductFiles)
                                            {
                                                if (File.Exists(Path.Combine(contentPath, productFile.FilePath)))
                                                {
                                                    string fileName = productFile.FileName;
                                                    string contentType = productFile.ContentType;
                                                    if (fileName.Trim() == "" || fileName.ToLower().Trim() == "blob")
                                                    {
                                                        fileName = DateTime.Now.ToString("ddMMyyyyHHmmss") + fileCount.ToString("D" + 4) + ".jpg";
                                                        contentType = "image/jpeg";
                                                    }
                                                    string filePath = Path.Combine(folderPath, fileName);
                                                    File.Copy(Path.Combine(contentPath, productFile.FilePath), filePath);
                                                    OrderingAddressProductFile addressProductFile = new OrderingAddressProductFile
                                                    {
                                                        OrderingAddressProduct = addressProduct,
                                                        FileName = fileName,
                                                        ContentType = contentType,
                                                        FilePath = "ordering/" + request.CustomerId.ToString() + "/" + currentTimeStr + "/addressproduct/" + addressCount + "/" + productCount + "/" + fileName,
                                                        DirectoryPath = "ordering/" + request.CustomerId.ToString() + "/" + currentTimeStr + "/addressproduct/" + addressCount + "/" + productCount + "/",
                                                        FileEXT = productFile.FileEXT,
                                                        Type = "fromordering",
                                                        Created = DateTime.Now,
                                                        Modified = DateTime.Now,
                                                        CreatedBy = request.CustomerId != null ? request.CustomerId.ToString() : "",
                                                        ModifiedBy = request.CustomerId != null ? request.CustomerId.ToString() : "",
                                                    };
                                                    AddressProductFiles.Add(addressProductFile);
                                                }
                                            }
                                        }
                                    }
                                    addressProduct.OrderingAddressProductFiles = AddressProductFiles;
                                }

                            }
                            address.AddressProducts = addressProducts;
                        }
                        address.Ordering = ordering;
                        address.Country = country;
                        address.Province = province;
                        address.District = district;
                        address.Subdistrict = subdistrict;
                        address.Status = 1;
                        address.Created = DateTime.Now;
                        address.CreatedBy = request.CustomerId != null ? request.CustomerId.ToString() : "-";
                        address.Modified = DateTime.Now;
                        address.ModifiedBy = request.CustomerId != null ? request.CustomerId.ToString() : "-";
                        address.NotiAmount = 0;
                        addresses.Add(address);
                    }
                    if (addresses.Count() > 1)
                    {
                        var locationDistince = LocationHelper.GetDistanceAndEstimateTime(detectArea, api_key, addresses[0].Maps, addresses[addresses.Count() - 1].Maps);
                        ordering.TotalDistance = locationDistince.Distance;
                        ordering.TotalEstimateTime = locationDistince.EstimateTime;
                        ordering.Distance = locationDistince.Distance;
                        ordering.EstimateTime = locationDistince.EstimateTime;
                    }
                    ordering.Addresses = addresses;
                    var firstPickupPoint = addresses.Where(a => a.AddressType == "pickuppoint").OrderBy(a => a.Sequence).FirstOrDefault();
                    var lastDeliveryPoint = addresses.Where(a => a.AddressType == "deliverypoint").OrderByDescending(a => a.Sequence).FirstOrDefault();
                    if (firstPickupPoint != null)
                    {
                        ordering.PickupPoint_Str = firstPickupPoint.Province != null ? firstPickupPoint.Province.Name_TH : "-";
                        ordering.PickupPointDate_Str = firstPickupPoint.Date;
                    }
                    if (lastDeliveryPoint != null)
                    {
                        ordering.RecipientName_Str = lastDeliveryPoint.PersonalName;
                        ordering.PickupPoint_Str = lastDeliveryPoint.Province != null ? lastDeliveryPoint.Province.Name_TH : "-";
                        ordering.PickupPointDate_Str = lastDeliveryPoint.Date;
                    }
                    orderingHistory.Addresses = addresses;
                }

                if (request.Containers != null)
                {
                    List<OrderingContainer> containers = new List<OrderingContainer>();
                    foreach (CreateContainerViewModel containerObj in request.Containers)
                    {
                        var container = _mapper.Map<OrderingContainer>(containerObj);
                        container.Ordering = ordering;
                        container.ContainerType = containerTypes.Where(c => c.Id == containerObj.ContainerTypeId).FirstOrDefault();
                        container.ContainerSpecification = containerSpecifications.Where(c => c.Id == containerObj.ContainerSpecificationId).FirstOrDefault();
                        container.EnergySavingDevice = energySavingDevices.Where(c => c.Id == containerObj.EnergySavingDeviceId).FirstOrDefault();
                        container.PickupPointCountry = (await _countryRepository.FindByCondition(x => x.Id == containerObj.PickupPointCountryId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                        container.PickupPointProvince = (await _provinceRepository.FindByCondition(x => x.Id == containerObj.PickupPointProvinceId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                        container.PickupPointDistrict = (await _districtRepository.FindByCondition(x => x.Id == containerObj.PickupPointDistrictId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                        container.PickupPointSubdistrict = (await _subdistrictRepository.FindByCondition(x => x.Id == containerObj.PickupPointSubdistrictId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                        container.ReturnPointCountry = (await _countryRepository.FindByCondition(x => x.Id == containerObj.ReturnPointCountryId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                        container.ReturnPointProvince = (await _provinceRepository.FindByCondition(x => x.Id == containerObj.ReturnPointProvinceId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                        container.ReturnPointDistrict = (await _districtRepository.FindByCondition(x => x.Id == containerObj.ReturnPointDistrictId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                        container.ReturnPointSubdistrict = (await _subdistrictRepository.FindByCondition(x => x.Id == containerObj.ReturnPointSubdistrictId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                        container.Created = DateTime.Now;
                        container.Modified = DateTime.Now;
                        container.CreatedBy = request.CustomerId != null ? request.CustomerId.ToString() : "";
                        container.ModifiedBy = request.CustomerId != null ? request.CustomerId.ToString() : "";
                        string phoneNumber = containerObj.ShippingContactPhoneNumber != null ? containerObj.ShippingContactPhoneNumber : "";
                        if (containerObj.ShippingContactPhoneCode == "+66" && containerObj.ShippingContactPhoneNumber != null && containerObj.ShippingContactPhoneNumber.Length > 9 && containerObj.ShippingContactPhoneNumber.First() == '0')
                        {
                            phoneNumber = containerObj.ShippingContactPhoneNumber.Remove(0, 1);
                        }
                        containerObj.ShippingContactPhoneNumber = phoneNumber;

                        phoneNumber = containerObj.YardContactPhoneNumber != null ? containerObj.YardContactPhoneNumber : "";
                        if (containerObj.YardContactPhoneCode == "+66" && containerObj.YardContactPhoneNumber != null && containerObj.YardContactPhoneNumber.Length > 9 && containerObj.YardContactPhoneNumber.First() == '0')
                        {
                            phoneNumber = containerObj.YardContactPhoneNumber.Remove(0, 1);
                        }
                        containerObj.YardContactPhoneNumber = phoneNumber;

                        phoneNumber = containerObj.LinerContactPhoneNumber != null ? containerObj.LinerContactPhoneNumber : "";
                        if (containerObj.LinerContactPhoneCode == "+66" && containerObj.LinerContactPhoneNumber != null && containerObj.LinerContactPhoneNumber.Length > 9 && containerObj.LinerContactPhoneNumber.First() == '0')
                        {
                            phoneNumber = containerObj.LinerContactPhoneNumber.Remove(0, 1);
                        }
                        containerObj.LinerContactPhoneNumber = phoneNumber;

                        if (containerObj.AttachedFiles != null && containerObj.AttachedFiles.Count > 0)
                        {
                            if (container.Files == null)
                            {
                                container.Files = new List<OrderingContainerFile>();
                            }
                            string folderPath = contentPath + "ordering/" + request.CustomerId.ToString() + "/" + currentTimeStr + "/container/";
                            if (!Directory.Exists(folderPath))
                            {
                                Directory.CreateDirectory(folderPath);
                            }
                            int fileCount = 0;
                            foreach (IFormFile file in containerObj.AttachedFiles)
                            {
                                fileCount = fileCount + 1;
                                string fileName = file.FileName;
                                string contentType = file.ContentType;
                                if (fileName.Trim() == "" || fileName.ToLower().Trim() == "blob")
                                {
                                    fileName = DateTime.Now.ToString("ddMMyyyyHHmmss") + fileCount.ToString("D" + 4) + ".jpg";
                                    contentType = "image/jpeg";
                                }
                                string filePath = Path.Combine(folderPath, fileName);
                                using (FileStream fileStream = new FileStream(filePath, FileMode.Create))
                                {
                                    await file.CopyToAsync(fileStream);
                                    FileInfo fi = new FileInfo(filePath);

                                    OrderingContainerFile orderingContainerFile = new OrderingContainerFile
                                    {
                                        OrderingContainer = container,
                                        FileName = fileName,
                                        ContentType = contentType,
                                        FilePath = "ordering/" + request.CustomerId.ToString() + "/" + currentTimeStr + "/container/" + fileName,
                                        DirectoryPath = "ordering/" + request.CustomerId.ToString() + "/" + currentTimeStr + "/container/",
                                        FileEXT = fi.Extension,
                                        Created = DateTime.Now,
                                        Modified = DateTime.Now,
                                        CreatedBy = request.CustomerId != null ? request.CustomerId.ToString() : "",
                                        ModifiedBy = request.CustomerId != null ? request.CustomerId.ToString() : "",
                                    };
                                    container.Files.Add(orderingContainerFile);
                                }
                            }
                        }
                        foreach (OrderingAddress addrss in  ordering.Addresses)
                        {
                            foreach(OrderingAddressProduct product in addrss.AddressProducts)
                            {
                                if(product.Name == "#container#")
                                {
                                    product.OrderingContainer = container;
                                }
                            }
                        } 
                        containers.Add(container);
                    }
                    ordering.Containers = containers;
                    orderingHistory.Containers = containers;
                }

                var driverObject = (await _driverRepository.FindByCondition(x => x.Id.Equals(request.DriverId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if(driverObject != null)
                {
                    OrderingDriverReserve driverReserve = new OrderingDriverReserve();

                    driverReserve.Ordering = ordering;
                    driverReserve.Driver = driverObject;
                    driverReserve.Status = 1;
                    driverReserve.Created = DateTime.Now;
                    driverReserve.Modified = DateTime.Now;
                    driverReserve.CreatedBy = request.CustomerId != null ? request.CustomerId.ToString() : "";
                    driverReserve.ModifiedBy = request.CustomerId != null ? request.CustomerId.ToString() : "";
                    if (ordering.DriverReserve == null)
                    {
                        ordering.DriverReserve = new List<OrderingDriverReserve>();
                    }
                    ordering.DriverReserve.Add(driverReserve);
                }

                var dataObject = await _orderingRepository.AddAsync(ordering);
                //if (dataObject != null)
                //{
                //    customerObject.RequestCarPerYear = customerObject.RequestCarPerYear + 1;
                //    customerObject.RequestCarPerMonth = customerObject.RequestCarPerMonth + 1;


                //    var customerActivity = (await _customerActivityRepository.FindByCondition(x => x.CustomerId.Equals(request.CustomerId) && x.Date == DateTime.Now.ToString("yyyy'-'MM'-'dd")).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                //    if (customerActivity == null)
                //    {
                //        CustomerActivity newCustomerActivity = new CustomerActivity
                //        {
                //            CustomerId = request.CustomerId,
                //            Date = DateTime.Now.ToString("yyyy'-'MM'-'dd"),
                //            Star = 0,
                //            RequestCar = 1,
                //            Cancel = 0,
                //            OrderingValue = 0,
                //            Created = DateTime.Now,
                //            CreatedBy = request.CustomerId != null ? request.CustomerId.ToString() : "-",
                //            Modified = DateTime.Now,
                //            ModifiedBy = request.CustomerId != null ? request.CustomerId.ToString() : "-",
                //        };
                //        await _customerActivityRepository.AddAsync(newCustomerActivity);
                //    }
                //    else
                //    {
                //        customerActivity.RequestCar = customerActivity.RequestCar + 1;
                //        await _customerActivityRepository.UpdateAsync(customerActivity);
                //    }
                //}

                orderingHistory.TrackingCode = ordering.TrackingCode;
                orderingHistory.OrderingId = ordering.Id;
                orderingHistory.Driver = ordering.Driver;
                orderingHistory.Customer = ordering.Customer;
                orderingHistory.ProductCBM = ordering.ProductCBM;
                orderingHistory.ProductTotalWeight = ordering.ProductTotalWeight;
                orderingHistory.OrderingPrice = ordering.OrderingPrice;
                orderingHistory.OrderingStatus = ordering.Status;
                orderingHistory.AdditionalDetail = ordering.AdditionalDetail;
                orderingHistory.Note = ordering.Note;
                orderingHistory.OrderNumber = ordering.TrackingCode;
                orderingHistory.OrderingDesiredPrice = ordering.OrderingDesiredPrice;
                orderingHistory.OrderingDriverOffering = ordering.OrderingDriverOffering;
                orderingHistory.IsOrderingDriverOffer = ordering.IsOrderingDriverOffer;
                orderingHistory.IsMutipleRoutes = ordering.IsMutipleRoutes;
                orderingHistory.CancelStatus = ordering.CancelStatus;
                orderingHistory.CustomerRanking = ordering.CustomerRanking;
                orderingHistory.DriverRanking = ordering.DriverRanking;
                orderingHistory.PinnedDate = ordering.PinnedDate;
                orderingHistory.Distance = ordering.Distance;
                orderingHistory.EstimateTime = ordering.EstimateTime;
                orderingHistory.IsDriverPay = ordering.IsDriverPay;
                orderingHistory.CurrentLocation = ordering.CurrentLocation;
                orderingHistory.GPValue = ordering.GPValue;
                orderingHistory.DriverPayValue = ordering.DriverPayValue;
                orderingHistory.CustomerCancelValue = ordering.CustomerCancelValue;
                orderingHistory.DriverCancelValue = ordering.DriverCancelValue;
                orderingHistory.CustomerCashBack = ordering.CustomerCashBack;
                orderingHistory.DriverCashBack = ordering.DriverCashBack;
                orderingHistory.Insurances = ordering.Insurances;
                orderingHistory.Payments = ordering.Payments;
                orderingHistory.DriverAnnouncements = ordering.DriverAnnouncements;
                orderingHistory.Created = DateTime.UtcNow;
                orderingHistory.Modified = DateTime.UtcNow;
                orderingHistory.CreatedBy = request.CustomerId != null ? request.CustomerId.ToString() : "";
                orderingHistory.ModifiedBy = request.CustomerId != null ? request.CustomerId.ToString() : "";

                await _orderingHistoryRepository.AddAsync(orderingHistory);

                if (driverObject != null)
                {
                    #region
                    var driverOrderingStatusFile = Path.Combine(Directory.GetCurrentDirectory(), $"Shared\\driver_orderingStatus.json");
                    var driverOrderingStatusJson = System.IO.File.ReadAllText(driverOrderingStatusFile);
                    List<OrderingStatusViewModel> driverOrderingStatus = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OrderingStatusViewModel>>(driverOrderingStatusJson);

                    //var cloneOrdering = _mapper.Map<OrderingViewModel>(dataObject);
                    var cloneOrdering = new OrderingNotificationViewModel
                    {
                        id = dataObject.Id,
                        trackingCode = dataObject.TrackingCode,
                        orderNumber = dataObject.TrackingCode,
                        status = dataObject.Status,
                        products = _mapper.Map<List<OrderingProductNotificationViewModel>>(dataObject.Products)
                    };
                    if (dataObject.Cars != null && dataObject.Cars.Count > 0)
                    {
                        cloneOrdering.carType = _mapper.Map<CarTypeViewModel>(dataObject.Cars[0].CarType);
                        cloneOrdering.carList = _mapper.Map<CarListViewModel>(dataObject.Cars[0].CarList);
                    }
                    if (cloneOrdering.status != null)
                    {
                        cloneOrdering.statusObj = orderingStatus.Where(o => o.id == cloneOrdering.status).FirstOrDefault();
                    }
                    foreach (OrderingProductNotificationViewModel productViewModel in cloneOrdering.products)
                    {
                        if (productViewModel.productType != null)
                        {
                            string[] productTypeIds = productViewModel.productType.Split(",");
                            productViewModel.productTypes = new List<ProductTypeViewModel>();
                            foreach (string productId in productTypeIds)
                            {
                                int id = 0;
                                if (int.TryParse(productId, out id))
                                {
                                    var productType = productTypes.Where(p => p.Id == id).FirstOrDefault();
                                    if (productType != null)
                                        productViewModel.productTypes.Add(_mapper.Map<ProductTypeViewModel>(productType));
                                }
                            }
                        }
                        if (productViewModel.packaging != null)
                        {
                            string[] packagingIds = productViewModel.packaging.Split(",");
                            productViewModel.packagings = new List<ProductPackagingViewModel>();
                            foreach (string packagingId in packagingIds)
                            {
                                int id = 0;
                                if (int.TryParse(packagingId, out id))
                                {
                                    var packaging = packagings.Where(p => p.Id == id).FirstOrDefault();
                                    if (packaging != null)
                                        productViewModel.packagings.Add(_mapper.Map<ProductPackagingViewModel>(packaging));
                                }
                            }
                        }

                    }
                    if (dataObject.Addresses != null && dataObject.Addresses.Count > 0)
                    {
                        cloneOrdering.startPoint = _mapper.Map<OrderingAddressNotificationViewModel>(dataObject.Addresses[0]);
                        cloneOrdering.endPoint = _mapper.Map<OrderingAddressNotificationViewModel>(dataObject.Addresses[dataObject.Addresses.Count - 1]);
                    }

                    #endregion
                    //#region add noti list
                    {
                        if (cloneOrdering.status != null)
                        {
                            cloneOrdering.statusObj = driverOrderingStatus.Where(o => o.id == cloneOrdering.status).FirstOrDefault();
                        }
                        var driverSignalIds = (await _notificationUserRepository.FindByCondition(x => x.UserId.Equals(driverObject.Id)).ConfigureAwait(false)).AsQueryable().Select(x => x.OneSignalId).ToList();
                        var notificationDetails = Path.Combine(Directory.GetCurrentDirectory(), $"Shared\\NotificationWord.json");
                        var notificationJson = System.IO.File.ReadAllText(notificationDetails);
                        List<NotificationWordStatusViewModel> notificationStatus = Newtonsoft.Json.JsonConvert.DeserializeObject<List<NotificationWordStatusViewModel>>(notificationJson);

                        string title = "";
                        string detail = "";
                        var notificationObj = notificationStatus.Where(o => o.id == 1).FirstOrDefault();
                        if (notificationObj != null)
                        {
                            title = notificationObj.title.th;
                            if (ordering.Customer != null)
                            {
                                detail = notificationObj.detail.th.Replace("#customertitle#", ordering.Customer.Title != null ? ordering.Customer.Title.Name_TH : "")
                                         .Replace("#customername#", ordering.Customer.Name)
                                         .Replace("#orderprice#", ordering.OrderingDesiredPrice != null || ordering.OrderingDesiredPrice != 0 ? ordering.OrderingDesiredPrice.ToString() : "-");
                            }
                        }
                        //if (cloneOrdering.products.Count > 2)
                        //{
                        //    cloneOrdering.products.RemoveRange(2, cloneOrdering.products.Count - 2);
                        //}
                        Domain.Notification notification = new Domain.Notification();

                        notification.OneSignalId = driverSignalIds.FirstOrDefault();
                        notification.UserId = driverObject.Id;
                        notification.OwnerId = request.CustomerId;
                        notification.Title = title;
                        notification.Detail = detail;
                        notification.ServiceCode = "Ordering";
                        //notification.ReferenceContentKey = request.ReferenceContentKey;
                        notification.Payload = Newtonsoft.Json.JsonConvert.SerializeObject(cloneOrdering);
                        notification.Created = DateTime.UtcNow;
                        notification.Modified = DateTime.UtcNow;
                        notification.CreatedBy = request.CustomerId.ToString();
                        notification.ModifiedBy = request.CustomerId.ToString();
                        await _notificationRepository.AddAsync(notification);
                        #region insert to Inbox
                        DriverInbox driverInbox = new DriverInbox()
                        {
                            DriverId = driverObject.Id,
                            Title = "-",
                            Title_ENG = "-",
                            Module = "การแจ้งเตือน",
                            Module_ENG = "Notification",
                            Content = Newtonsoft.Json.JsonConvert.SerializeObject(cloneOrdering),
                            FromUser = customerObject.FirstName + customerObject.LastName,
                            IsDelete = false,
                            Created = DateTime.Now
                        };
                        await _driverInboxRepository.AddAsync(driverInbox);
                        #endregion
                        driverSignalIds = driverSignalIds.Where(x => x.Trim() != "").ToList();
                        if (driverSignalIds.Count > 0)
                        {
                            NotificationManager notiMgr = new NotificationManager(request.DRIVER_APP_ID, request.DRIVER_REST_API_KEY, request.DRIVER_AUTH_ID);
                            notiMgr.SendNotificationAsync(title, detail, driverSignalIds, notification.ServiceCode, "", "{id:'" + cloneOrdering.id + "'}");
                        }
                    }
                    //#endregion
                }
                var log = new CreateAppLog(_applicationLogRepository);
                log.Create("Ordering", "Ordering", "Create", Newtonsoft.Json.JsonConvert.SerializeObject(request));
                return new Response<Guid>(dataObject.Id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}

