﻿using AutoMapper;
using iTrans.Transportation.Application.DTOs.OrderingAddress;
using iTrans.Transportation.Application.DTOs.OrderingAddressProduct;
using iTrans.Transportation.Application.DTOs.OrderingCar;
using iTrans.Transportation.Application.DTOs.OrderingContainer;
//using iTrans.Transportation.Application.DTOs.OrderingContainerAddress;
using iTrans.Transportation.Application.DTOs.OrderingProduct;
using iTrans.Transportation.Application.Enums;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using Microsoft.Extensions.Configuration;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using iTrans.Transportation.Application.DTOs.Ordering;
using iTrans.Transportation.Application.DTOs.OrderingStatus;
using iTrans.Transportation.Application.DTOs.ProductType;
using iTrans.Transportation.Application.DTOs.ProductPackaging;
using iTrans.Transportation.Application.DTOs.CarType;
using iTrans.Transportation.Application.DTOs.CarList;
using iTrans.Transportation.Application.Helper;

namespace iTrans.Transportation.Application.Features.Orderings.Commands
{
    public partial class UpdateOrderingCommand_NotUse : IRequest<Response<Guid>>
    {
        public Guid? UserId { get; set; }
        public Guid OrderingId { get; set; }

        public OrderingUpdateViewModel Ordering { get; set; }
        public UpdateCarViweModel Car { get; set; }
        public List<int> DeleteProductId { get; set; }
        public List<UpdateProductViewModel> Products { get; set; }
        public List<int> DeleteAddressesId { get; set; }
        public List<UpdateAddressViewModel> Addresses { get; set; }
        public List<int> DelContainerId { get; set; }
        public List<UpdateContainerViewModel> Containers { get; set; }
        public string APP_ID { get; set; }
        public string REST_API_KEY { get; set; }
        public string AUTH_ID { get; set; }
    }
    public class UpdateOrderingCommand_NotUseHandler : IRequestHandler<UpdateOrderingCommand_NotUse, Response<Guid>>
    {
        private readonly ICustomerRepositoryAsync _customerRepository;
        private readonly IDriverRepositoryAsync _driverRepository;
        private readonly IDriverAnnouncementRepositoryAsync _driverAnnouncementRepository;
        private readonly ICustomerActivityRepositoryAsync _customerActivityRepository;
        private readonly ICustomerProductRepositoryAsync _customerProductRepository;
        private readonly IOrderingRepositoryAsync _orderingRepository;
        private readonly IOrderingProductRepositoryAsync _orderingProductRepository;
        private readonly IOrderingProductFileRepositoryAsync _orderingProductFileRepository;
        private readonly IOrderingCarRepositoryAsync _orderingCarRepository;
        private readonly IOrderingAddressRepositoryAsync _orderingAddressRepository;
        private readonly IOrderingAddressProductRepositoryAsync _orderingAddressProductRepository;
        private readonly IOrderingAddressProductFileRepositoryAsync _orderingAddressProductFileRepository;
        private readonly IProductTypeRepositoryAsync _productTypeRepository;
        private readonly IProductPackagingRepositoryAsync _productPackagingRepository;

        private readonly IContainerTypeRepositoryAsync _containerTypeRepository;
        private readonly IContainerSpecificationRepositoryAsync _containerSpecificationRepository;
        private readonly IEnergySavingDeviceRepositoryAsync _energySavingDeviceRepository;

        private readonly ICarTypeRepositoryAsync _carTypeRepository;
        private readonly ICarListRepositoryAsync _carListRepository;
        private readonly ICarDescriptionRepositoryAsync _carDescriptionRepository;
        private readonly ICarSpecificationRepositoryAsync _carSpecificationRepository;
        private readonly ICarFeatureRepositoryAsync _carFeatureRepository;
        private readonly ICountryRepositoryAsync _countryRepository;
        private readonly IProvinceRepositoryAsync _provinceRepository;
        private readonly IDistrictRepositoryAsync _districtRepository;
        private readonly ISubdistrictRepositoryAsync _subdistrictRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;

        private readonly INotificationRepositoryAsync _notificationRepository;
        private readonly INotificationUserRepositoryAsync _notificationUserRepository;
        private readonly IOrderingHistoryRepositoryAsync _orderingHistoryRepository;
        private readonly IDriverInboxRepositoryAsync _driverInboxRepository;

        private readonly IOrderingContainerRepositoryAsync _orderingContainerRepository;
        private readonly IOrderingContainerFileRepositoryAsync _orderingContainerFileRepository;

        private readonly IMapper _mapper;
        private readonly IConfiguration _configuration;
        public UpdateOrderingCommand_NotUseHandler(ICustomerRepositoryAsync customerRepository, IDriverRepositoryAsync driverRepository, ICustomerProductRepositoryAsync customerProductRepository, IOrderingRepositoryAsync orderingRepository, IOrderingProductRepositoryAsync orderingProductRepository, IOrderingCarRepositoryAsync orderingCarRepository
            , IOrderingAddressRepositoryAsync orderingAddressRepository, IProductTypeRepositoryAsync productTypeRepository, IProductPackagingRepositoryAsync productPackagingRepository
            , ICarTypeRepositoryAsync carTypeRepository, ICarListRepositoryAsync carListRepository, ICarDescriptionRepositoryAsync carDescriptionRepository, ICarSpecificationRepositoryAsync carSpecificationRepository
            , ICarFeatureRepositoryAsync carFeatureRepository, ICountryRepositoryAsync countryRepository, IProvinceRepositoryAsync provinceRepository, IDistrictRepositoryAsync districtRepository
            , ISubdistrictRepositoryAsync subdistrictRepository, IApplicationLogRepositoryAsync applicationLogRepository
            , INotificationRepositoryAsync notificationRepository, INotificationUserRepositoryAsync notificationUserRepository, IContainerTypeRepositoryAsync containerTypeRepository, IContainerSpecificationRepositoryAsync containerSpecificationRepository, IEnergySavingDeviceRepositoryAsync energySavingDeviceRepository
            , IMapper mapper, IConfiguration configuration, IDriverAnnouncementRepositoryAsync driverAnnouncementRepository, IOrderingHistoryRepositoryAsync orderingHistoryRepository, IDriverInboxRepositoryAsync driverInboxRepository
            , ICustomerActivityRepositoryAsync customerActivityRepository, IOrderingProductFileRepositoryAsync orderingProductFileRepository, IOrderingAddressProductFileRepositoryAsync orderingAddressProductFileRepository
            , IOrderingAddressProductRepositoryAsync orderingAddressProductRepository, IOrderingContainerFileRepositoryAsync orderingContainerFileRepository, IOrderingContainerRepositoryAsync orderingContainerRepository
            )
        {
            _customerRepository = customerRepository;
            _driverRepository = driverRepository;
            _driverAnnouncementRepository = driverAnnouncementRepository;
            _customerProductRepository = customerProductRepository;
            _orderingRepository = orderingRepository;
            _orderingProductRepository = orderingProductRepository;
            _orderingAddressRepository = orderingAddressRepository;
            _orderingCarRepository = orderingCarRepository;
            _productTypeRepository = productTypeRepository;
            _productPackagingRepository = productPackagingRepository;
            _carTypeRepository = carTypeRepository;
            _carListRepository = carListRepository;
            _carDescriptionRepository = carDescriptionRepository;
            _carSpecificationRepository = carSpecificationRepository;
            _carFeatureRepository = carFeatureRepository;
            _countryRepository = countryRepository;
            _provinceRepository = provinceRepository;
            _districtRepository = districtRepository;
            _subdistrictRepository = subdistrictRepository;
            _applicationLogRepository = applicationLogRepository;
            _containerTypeRepository = containerTypeRepository;
            _containerSpecificationRepository = containerSpecificationRepository;
            _energySavingDeviceRepository = energySavingDeviceRepository;
            _notificationRepository = notificationRepository;
            _notificationUserRepository = notificationUserRepository;
            _orderingHistoryRepository = orderingHistoryRepository;
            _driverInboxRepository = driverInboxRepository;
            _customerActivityRepository = customerActivityRepository;
            _orderingProductFileRepository = orderingProductFileRepository;
            _orderingAddressProductFileRepository = orderingAddressProductFileRepository;
            _orderingAddressProductRepository = orderingAddressProductRepository;
            _orderingContainerRepository = orderingContainerRepository;
            _orderingContainerFileRepository = orderingContainerFileRepository;
            _mapper = mapper;
            _configuration = configuration;

        }

        public async Task<Response<Guid>> Handle(UpdateOrderingCommand_NotUse request, CancellationToken cancellationToken)
        {
            try
            {
                var ordering = (await _orderingRepository.FindByCondition(x => x.Id == request.OrderingId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (ordering == null)
                {
                    throw new ApiException($"Ordering Not Found.");
                }

                var getContentPath = _configuration.GetSection("ContentPath");
                string contentPath = getContentPath.Value;

                string currentTimeStr = DateTime.Now.ToString("yyyyMMddHHmmss");
                int detectArea = Convert.ToInt32(_configuration.GetSection("DetectArea").Value);
                string api_key = _configuration.GetSection("GoogleAPIKEY").Value;

                if (request.Ordering != null)
                {
                    ordering.TransportType = request.Ordering.transportType;
                    ordering.ProductTotalWeight = request.Ordering.productTotalWeight;
                    ordering.ProductCBM = request.Ordering.productCBM;
                    ordering.OrderingDesiredPrice = request.Ordering.orderingDesiredPrice;
                    ordering.OrderingDriverOffering = request.Ordering.orderingDriverOffering;
                    ordering.IsOrderingDriverOffer = request.Ordering.isOrderingDriverOffer;
                    ordering.AdditionalDetail = request.Ordering.additionalDetail;
                    ordering.Note = request.Ordering.note;
                    ordering.Modified = DateTime.Now;
                    ordering.ModifiedBy = request.UserId != null ? request.UserId.ToString() : "";
                }

                if (request.Car != null)
                {
                    var car = ordering.Cars.Where(x => x.Id == request.Car.Id).FirstOrDefault();
                    if (car != null)
                    {
                        var carType = (await _carTypeRepository.FindByCondition(x => x.Active.Equals(true) && x.Id == request.Car.CarTypeId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                        var carSpecification = (await _carSpecificationRepository.FindByCondition(x => x.Active.Equals(true) && x.Id == request.Car.CarSpecificationId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                        var energySavingDevice = (await _energySavingDeviceRepository.FindByCondition(x => x.Active.Equals(true) && x.Id == request.Car.EnergySavingDeviceId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                        car.CarType = carType;
                        car.CarSpecification = carSpecification;
                        car.EnergySavingDevice = energySavingDevice;
                        car.Width = request.Car.Width;
                        car.Length = request.Car.Length;
                        car.Height = request.Car.Height;
                        car.Temperature = request.Car.Temperature;
                        car.ProductInsurance = request.Car.ProductInsurance;
                        car.ProductInsuranceAmount = request.Car.ProductInsuranceAmount;
                        car.Note = request.Car.Note;
                        car.IsRequestTax = request.Car.IsRequestTax;
                        car.TransportType = request.Car.TransportType;
                        car.Modified = DateTime.Now;
                        car.ModifiedBy = request.UserId != null ? request.UserId.ToString() : "";

                        ordering.IsRequestTax = request.Car.IsRequestTax;
                        ordering.TransportType = request.Car.TransportType;
                    }
                    else
                    {
                        List<OrderingCar> cars = new List<OrderingCar>();
                        var carType = (await _carTypeRepository.FindByCondition(x => x.Active.Equals(true) && x.Id == request.Car.CarTypeId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                        var carSpecification = (await _carSpecificationRepository.FindByCondition(x => x.Active.Equals(true) && x.Id == request.Car.CarSpecificationId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                        var energySavingDevice = (await _energySavingDeviceRepository.FindByCondition(x => x.Active.Equals(true) && x.Id == request.Car.EnergySavingDeviceId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                        car.Ordering = ordering;
                        car.CarType = carType;
                        car.CarSpecification = carSpecification;
                        car.EnergySavingDevice = energySavingDevice;
                        car.Width = request.Car.Width;
                        car.Length = request.Car.Length;
                        car.Height = request.Car.Height;
                        car.Temperature = request.Car.Temperature;
                        car.ProductInsurance = request.Car.ProductInsurance;
                        car.ProductInsuranceAmount = request.Car.ProductInsuranceAmount;
                        car.Note = request.Car.Note;
                        car.IsRequestTax = request.Car.IsRequestTax;
                        car.TransportType = request.Car.TransportType;
                        car.Created = DateTime.Now;
                        car.CreatedBy = request.UserId != null ? request.UserId.ToString() : "";
                        car.Modified = DateTime.Now;
                        car.ModifiedBy = request.UserId != null ? request.UserId.ToString() : "";
                        cars.Add(car);
                        ordering.Cars = cars;
                    }
                }

                if (request.DeleteProductId != null && request.DeleteProductId.Count > 0)
                {
                    foreach (int delProductId in request.DeleteProductId)
                    {
                        var deleteProduct = ordering.Products.Where(x => x.Id == delProductId).FirstOrDefault();
                        if (deleteProduct != null)
                        {
                            if (deleteProduct.ProductFiles != null)
                            {
                                foreach (OrderingProductFile productFile in deleteProduct.ProductFiles)
                                {
                                    if (File.Exists(Path.Combine(contentPath, productFile.FilePath)))
                                        File.Delete(Path.Combine(contentPath, productFile.FilePath));
                                }
                                (await _orderingProductFileRepository.CreateSQLQuery("DELETE Ordering_ProductFile where OrderingProductId = " + deleteProduct.Id.ToString()).ConfigureAwait(false)).ExecuteUpdate();
                            }
                            ordering.Products.Remove(deleteProduct);
                        }
                    }
                }

                if (request.Products != null)
                {
                    if (ordering.Products == null)
                    {
                        ordering.Products = new List<OrderingProduct>();
                    }
                    var productTypes = (await _productTypeRepository.FindByCondition(x => x.Active.Equals(true)).ConfigureAwait(false)).AsQueryable().ToList();
                    var packagings = (await _productPackagingRepository.FindByCondition(x => x.Active.Equals(true)).ConfigureAwait(false)).AsQueryable().ToList();
                    int productCount = ordering.Products.Count();
                    foreach (UpdateProductViewModel product in request.Products)
                    {
                        productCount = productCount + 1;
                        var orderingProduct = ordering.Products.Where(x => x.Id == product.Id).FirstOrDefault();
                        var customerProduct = (await _customerProductRepository.FindByCondition(x => x.Id == product.CustomerProductId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                        if (orderingProduct != null)
                        {
                            orderingProduct.Name = product.Name;
                            orderingProduct.Width = product.Width;
                            orderingProduct.Length = product.Length;
                            orderingProduct.Height = product.Height;
                            orderingProduct.Weight = product.Weight;
                            orderingProduct.Quantity = product.Quantity;
                            orderingProduct.Sequence = product.Sequence;
                            orderingProduct.ProductType = "";
                            foreach (int productId in product.ProductType)
                            {
                                var productType = productTypes.Where(p => p.Id == productId).FirstOrDefault();
                                if (productType != null)
                                    orderingProduct.ProductType = orderingProduct.ProductType + productId + ",";
                            }
                            if (orderingProduct.ProductType != "")
                                orderingProduct.ProductType = orderingProduct.ProductType.Remove(orderingProduct.ProductType.Length - 1);

                            orderingProduct.Packaging = "";
                            foreach (int packagingId in product.Packaging)
                            {
                                var packaging = packagings.Where(p => p.Id == packagingId).FirstOrDefault();
                                if (packaging != null)
                                    orderingProduct.Packaging = orderingProduct.Packaging + packagingId + ",";
                            }
                            if (orderingProduct.Packaging != "")
                                orderingProduct.Packaging = orderingProduct.Packaging.Remove(product.Packaging.Length - 1);

                            if (product.DeleteProductFileId != null && product.DeleteProductFileId.Count > 0)
                            {
                                foreach (int delProductId in product.DeleteProductFileId)
                                {
                                    var deleteProduct = orderingProduct.ProductFiles.Where(x => x.Id == delProductId).FirstOrDefault();
                                    if (deleteProduct != null)
                                    {
                                        foreach (OrderingProductFile productFile in orderingProduct.ProductFiles)
                                        {
                                            if (File.Exists(Path.Combine(contentPath, productFile.FilePath)))
                                                File.Delete(Path.Combine(contentPath, productFile.FilePath));
                                        }
                                        (await _orderingProductFileRepository.CreateSQLQuery("DELETE Ordering_ProductFile where OrderingProductId = " + deleteProduct.Id.ToString()).ConfigureAwait(false)).ExecuteUpdate();
                                    }
                                    orderingProduct.ProductFiles.Remove(deleteProduct);
                                }
                            }

                            string folderPath = contentPath + "ordering/" + ordering.Id.ToString() + "/" + currentTimeStr + "/product/" + productCount + "/";
                            if (!Directory.Exists(folderPath))
                            {
                                Directory.CreateDirectory(folderPath);
                            }
                            int fileCount = 0;
                            if (orderingProduct.ProductFiles == null)
                            {
                                orderingProduct.ProductFiles = new List<OrderingProductFile>();
                            }
                            if (product.Files != null && product.Files.Count > 0)
                            {
                                foreach (IFormFile file in product.Files)
                                {
                                    fileCount = fileCount + 1;
                                    string fileName = file.FileName;
                                    string contentType = file.ContentType;
                                    if (fileName.Trim() == "" || fileName.ToLower().Trim() == "blob")
                                    {
                                        fileName = DateTime.Now.ToString("ddMMyyyyHHmmss") + fileCount.ToString("D" + 4) + ".jpg";
                                        contentType = "image/jpeg";
                                    }
                                    string filePath = Path.Combine(folderPath, fileName);
                                    using (FileStream fileStream = new FileStream(filePath, FileMode.Create))
                                    {
                                        await file.CopyToAsync(fileStream);
                                        FileInfo fi = new FileInfo(filePath);

                                        OrderingProductFile orderingProductFile = new OrderingProductFile
                                        {
                                            OrderingProduct = orderingProduct,
                                            FileName = fileName,
                                            ContentType = contentType,
                                            FilePath = "ordering/" + ordering.Id.ToString() + "/" + currentTimeStr + "/product/" + productCount + "/" + fileName,
                                            DirectoryPath = "ordering/" + ordering.Id.ToString() + "/" + currentTimeStr + "/product/" + productCount + "/",
                                            FileEXT = fi.Extension,
                                            Created = DateTime.Now,
                                            Modified = DateTime.Now,
                                            CreatedBy = request.UserId != null ? request.UserId.ToString() : "",
                                            ModifiedBy = request.UserId != null ? request.UserId.ToString() : "",
                                        };
                                        orderingProduct.ProductFiles.Add(orderingProductFile);
                                    }
                                }
                            }

                            if (customerProduct != null)
                            {
                                if (customerProduct.ProductFiles != null && customerProduct.ProductFiles.Count() > 0)
                                {
                                    foreach (CustomerProductFile productFile in customerProduct.ProductFiles)
                                    {
                                        if (File.Exists(Path.Combine(contentPath, productFile.FilePath)))
                                        {
                                            string fileName = productFile.FileName;
                                            string contentType = productFile.ContentType;
                                            if (fileName.Trim() == "" || fileName.ToLower().Trim() == "blob")
                                            {
                                                fileName = DateTime.Now.ToString("ddMMyyyyHHmmss") + fileCount.ToString("D" + 4) + ".jpg";
                                                contentType = "image/jpeg";
                                            }
                                            string filePath = Path.Combine(folderPath, fileName);
                                            File.Copy(Path.Combine(contentPath, productFile.FilePath), filePath);
                                            OrderingProductFile orderingProductFile = new OrderingProductFile
                                            {
                                                OrderingProduct = orderingProduct,
                                                FileName = fileName,
                                                ContentType = contentType,
                                                FilePath = "ordering/" + ordering.Id.ToString() + "/" + currentTimeStr + "/product/" + productCount + "/" + fileName,
                                                DirectoryPath = "ordering/" + ordering.Id.ToString() + "/" + currentTimeStr + "/product/" + productCount + "/",
                                                FileEXT = productFile.FileEXT,
                                                Created = DateTime.Now,
                                                Modified = DateTime.Now,
                                                CreatedBy = ordering.Id != null ? ordering.Id.ToString() : "",
                                                ModifiedBy = ordering.Id != null ? ordering.Id.ToString() : "",
                                            };
                                            orderingProduct.ProductFiles.Add(orderingProductFile);
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            orderingProduct = new OrderingProduct()
                            {
                                Ordering = ordering,
                                ProductType = "",
                                Packaging = "",
                                Width = product.Width,
                                Length = product.Length,
                                Height = product.Height,
                                Weight = product.Weight,
                                Quantity = product.Quantity,
                                Sequence = product.Sequence,
                            };

                            foreach (int productId in product.ProductType)
                            {
                                var productType = productTypes.Where(p => p.Id == productId).FirstOrDefault();
                                if (productType != null)
                                    orderingProduct.ProductType = orderingProduct.ProductType + productId + ",";
                            }
                            if (orderingProduct.ProductType != "")
                                orderingProduct.ProductType = orderingProduct.ProductType.Remove(orderingProduct.ProductType.Length - 1);

                            foreach (int packagingId in product.Packaging)
                            {
                                var packaging = packagings.Where(p => p.Id == packagingId).FirstOrDefault();
                                if (packaging != null)
                                    orderingProduct.Packaging = orderingProduct.Packaging + packagingId + ",";
                            }
                            if (orderingProduct.Packaging != "")
                                orderingProduct.Packaging = orderingProduct.Packaging.Remove(product.Packaging.Length - 1);

                            string folderPath = contentPath + "ordering/" + ordering.Id.ToString() + "/" + currentTimeStr + "/product/" + productCount + "/";
                            if (!Directory.Exists(folderPath))
                            {
                                Directory.CreateDirectory(folderPath);
                            }
                            int fileCount = 0;
                            List<OrderingProductFile> productFiles = new List<OrderingProductFile>();
                            if (product.Files != null)
                            {
                                foreach (IFormFile file in product.Files)
                                {
                                    fileCount = fileCount + 1;
                                    string fileName = file.FileName;
                                    string contentType = file.ContentType;
                                    if (fileName.Trim() == "" || fileName.ToLower().Trim() == "blob")
                                    {
                                        fileName = DateTime.Now.ToString("ddMMyyyyHHmmss") + fileCount.ToString("D" + 4) + ".jpg";
                                        contentType = "image/jpeg";
                                    }
                                    string filePath = Path.Combine(folderPath, fileName);
                                    using (FileStream fileStream = new FileStream(filePath, FileMode.Create))
                                    {
                                        await file.CopyToAsync(fileStream);
                                        FileInfo fi = new FileInfo(filePath);

                                        OrderingProductFile orderingProductFile = new OrderingProductFile
                                        {
                                            OrderingProduct = orderingProduct,
                                            FileName = fileName,
                                            ContentType = contentType,
                                            FilePath = "ordering/" + ordering.Id.ToString() + "/" + currentTimeStr + "/product/" + productCount + "/" + fileName,
                                            DirectoryPath = "ordering/" + ordering.Id.ToString() + "/" + currentTimeStr + "/product/" + productCount + "/",
                                            FileEXT = fi.Extension,
                                            Created = DateTime.Now,
                                            Modified = DateTime.Now,
                                            CreatedBy = request.UserId != null ? request.UserId.ToString() : "",
                                            ModifiedBy = request.UserId != null ? request.UserId.ToString() : "",
                                        };
                                        productFiles.Add(orderingProductFile);
                                    }
                                }
                            }

                            if (customerProduct != null)
                            {
                                foreach (CustomerProductFile productFile in customerProduct.ProductFiles)
                                {
                                    if (File.Exists(Path.Combine(contentPath, productFile.FilePath)))
                                    {
                                        string fileName = productFile.FileName;
                                        string contentType = productFile.ContentType;
                                        if (fileName.Trim() == "" || fileName.ToLower().Trim() == "blob")
                                        {
                                            fileName = DateTime.Now.ToString("ddMMyyyyHHmmss") + fileCount.ToString("D" + 4) + ".jpg";
                                            contentType = "image/jpeg";
                                        }
                                        string filePath = Path.Combine(folderPath, fileName);
                                        File.Copy(Path.Combine(contentPath, productFile.FilePath), filePath);
                                        OrderingProductFile orderingProductFile = new OrderingProductFile
                                        {
                                            OrderingProduct = orderingProduct,
                                            FileName = fileName,
                                            ContentType = contentType,
                                            FilePath = "ordering/" + ordering.Id.ToString() + "/" + currentTimeStr + "/product/" + productCount + "/" + fileName,
                                            DirectoryPath = "ordering/" + ordering.Id.ToString() + "/" + currentTimeStr + "/product/" + productCount + "/",
                                            FileEXT = productFile.FileEXT,
                                            Created = DateTime.Now,
                                            Modified = DateTime.Now,
                                            CreatedBy = request.UserId != null ? request.UserId.ToString() : "",
                                            ModifiedBy = request.UserId != null ? request.UserId.ToString() : "",
                                        };
                                        productFiles.Add(orderingProductFile);
                                    }
                                }
                            }
                            orderingProduct.ProductFiles = productFiles;
                            ordering.Products.Add(orderingProduct);
                        }
                    }
                }

                if (request.DeleteAddressesId != null && request.DeleteAddressesId.Count > 0)
                {
                    foreach (int delId in request.DeleteAddressesId)
                    {
                        var delAddress = ordering.Addresses.Where(x => x.Id == delId).FirstOrDefault();
                        if (delAddress != null)
                        {
                            if (delAddress.Files != null)
                            {
                                foreach (OrderingAddressFile dleFile in delAddress.Files)
                                {
                                    if (File.Exists(Path.Combine(contentPath, dleFile.FilePath)))
                                        File.Delete(Path.Combine(contentPath, dleFile.FilePath));
                                }
                                 (await _orderingProductFileRepository.CreateSQLQuery("DELETE Ordering_AddressFile where OrderingAddressId = " + delAddress.Id.ToString()).ConfigureAwait(false)).ExecuteUpdate();
                            }
                            if (delAddress.AddressProducts != null)
                            {
                                foreach (OrderingAddressProduct dleProduct in delAddress.AddressProducts)
                                {
                                    if (dleProduct.OrderingAddressProductFiles != null)
                                    {
                                        foreach (OrderingAddressProductFile dleFile in dleProduct.OrderingAddressProductFiles)
                                        {
                                            if (File.Exists(Path.Combine(contentPath, dleFile.FilePath)))
                                                File.Delete(Path.Combine(contentPath, dleFile.FilePath));
                                        }
                                         (await _orderingProductFileRepository.CreateSQLQuery("DELETE Ordering_AddressProductFile where OrderingAddressProductId = " + dleProduct.Id.ToString()).ConfigureAwait(false)).ExecuteUpdate();
                                    }
                                }
                                (await _orderingProductFileRepository.CreateSQLQuery("DELETE Ordering_AddressProduct where OrderingAddressId = " + delAddress.Id.ToString()).ConfigureAwait(false)).ExecuteUpdate();
                            }
                            (await _orderingProductFileRepository.CreateSQLQuery("DELETE Ordering_Address where Id = " + delAddress.Id.ToString()).ConfigureAwait(false)).ExecuteUpdate();
                            ordering.Addresses.Remove(delAddress);
                        }
                    }
                }

                if (request.Addresses != null && request.Addresses.Count > 0)
                {

                    if (ordering.Addresses == null)
                    {
                        ordering.Addresses = new List<OrderingAddress>();
                    }
                    foreach (UpdateAddressViewModel address in request.Addresses)
                    {
                        Country country = (await _countryRepository.FindByCondition(x => x.Id.Equals(address.CountryId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                        Province province = (await _provinceRepository.FindByCondition(x => x.Id.Equals(address.ProvinceId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                        District district = (await _districtRepository.FindByCondition(x => x.Id.Equals(address.DistrictId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                        Subdistrict subdistrict = (await _subdistrictRepository.FindByCondition(x => x.Id.Equals(address.SubdistrictId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                        string phoneNumber = address.PhoneNumber != null ? address.PhoneNumber.Trim() : "";
                        if (address.PhoneCode == "+66" && address.PhoneNumber != null && address.PhoneNumber.Length > 9 && address.PhoneNumber.First() == '0')
                        {
                            phoneNumber = address.PhoneNumber.Remove(0, 1);
                        }

                        var orderingAddress = ordering.Addresses.Where(x => x.Id == address.Id).FirstOrDefault();
                        if (orderingAddress.AddressProducts == null)
                        {
                            orderingAddress.AddressProducts = new List<OrderingAddressProduct>();
                        }
                        if (orderingAddress != null)
                        {
                            if (address.DeleteProductId != null && address.DeleteProductId.Count > 0)
                            {
                                foreach (int delProductId in address.DeleteProductId)
                                {
                                    var delProduct = orderingAddress.AddressProducts.Where(x => x.Id == delProductId).FirstOrDefault();
                                    if (delProduct != null)
                                    {
                                        foreach (OrderingAddressProductFile dleFile in delProduct.OrderingAddressProductFiles)
                                        {
                                            if (File.Exists(Path.Combine(contentPath, dleFile.FilePath)))
                                                File.Delete(Path.Combine(contentPath, dleFile.FilePath));
                                        }
                                        (await _orderingAddressProductFileRepository.CreateSQLQuery("DELETE Ordering_AddressProductFile where OrderingAddressProductId = " + delProduct.Id.ToString()).ConfigureAwait(false)).ExecuteUpdate();
                                        (await _orderingAddressProductRepository.CreateSQLQuery("DELETE Ordering_AddressProduct where Id = " + delProduct.Id.ToString()).ConfigureAwait(false)).ExecuteUpdate();
                                        orderingAddress.AddressProducts.Remove(delProduct);
                                    }
                                }
                            }
                            orderingAddress.PhoneNumber = phoneNumber;
                            orderingAddress.AddressType = address.AddressType;
                            orderingAddress.PersonalName = address.PersonalName;
                            orderingAddress.PhoneCode = address.PhoneCode;
                            orderingAddress.Email = address.Email;
                            orderingAddress.Country = country;
                            orderingAddress.Province = province;
                            orderingAddress.District = district;
                            orderingAddress.Subdistrict = subdistrict;
                            orderingAddress.PostCode = address.PostCode;
                            orderingAddress.Road = address.Road;
                            orderingAddress.Alley = address.Alley;
                            orderingAddress.Address = address.Address;
                            orderingAddress.Maps = address.Maps;
                            orderingAddress.Date = address.Date;
                            orderingAddress.Sequence = address.Sequence;
                            orderingAddress.Note = address.Note;
                            orderingAddress.Modified = DateTime.Now;
                            orderingAddress.ModifiedBy = request.UserId != null ? request.UserId.ToString() : "-";
                            if (address.Products != null && address.Products.Count > 0)
                            {
                                var productTypes = (await _productTypeRepository.FindByCondition(x => x.Active.Equals(true)).ConfigureAwait(false)).AsQueryable().ToList();
                                var packagings = (await _productPackagingRepository.FindByCondition(x => x.Active.Equals(true)).ConfigureAwait(false)).AsQueryable().ToList();

                                int productCount = address.Products.Count;
                                foreach (UpdateProductFormViewModel product in address.Products)
                                {
                                    var addressProduct = orderingAddress.AddressProducts.Where(x => x.Id == product.Id ).FirstOrDefault();
                                    if (addressProduct != null)
                                    {
                                        addressProduct.Quantity = product.Quantity;
                                        addressProduct.Sequence = product.Sequence;
                                    }
                                    else
                                    {
                                        var orderingProduct = ordering.Products.Where(x => x.Id == product.ProductId).FirstOrDefault();
                                        if (orderingProduct != null)
                                        {
                                            OrderingAddressProduct _product = new OrderingAddressProduct()
                                            {

                                            };
                                        }
                                        else 
                                        if (request.Products != null)
                                        {
                                            var selProduct = request.Products.Where(p => p.Sequence == product.Product ).FirstOrDefault();
                                            if (selProduct != null)
                                            {
                                                productCount = productCount + 1;
                                                var _product = _mapper.Map<OrderingAddressProduct>(selProduct);
                                                _product.ProductType = "";
                                                foreach (int productTypeId in selProduct.ProductType)
                                                {
                                                    var productType = productTypes.Where(p => p.Id == productTypeId).FirstOrDefault();
                                                    if (productType != null)
                                                        _product.ProductType = _product.ProductType + productTypeId + ",";
                                                }
                                                if (_product.ProductType != "")
                                                    _product.ProductType = _product.ProductType.Remove(_product.ProductType.Length - 1);
                                                _product.Packaging = "";
                                                foreach (int packagingId in selProduct.Packaging)
                                                {
                                                    var packaging = packagings.Where(p => p.Id == packagingId).FirstOrDefault();
                                                    if (packaging != null)
                                                        _product.Packaging = _product.Packaging + packagingId + ",";
                                                }
                                                if (_product.Packaging != "")
                                                    _product.Packaging = _product.Packaging.Remove(_product.Packaging.Length - 1);

                                                _product.OrderingProduct = ordering.Products.Where(p => p.Sequence.Equals(product.Product)).FirstOrDefault();
                                                _product.Quantity = product.Quantity;
                                                _product.Sequence = product.Sequence;
                                                _product.OrderingAddress = orderingAddress;
                                                orderingAddress.AddressProducts.Add(_product);
                                                string folderPath = contentPath + "ordering/" + request.OrderingId.ToString() + "/" + currentTimeStr + "/addressproduct/" + address.Id.ToString() + "/" + productCount + "/";
                                                if (!Directory.Exists(folderPath))
                                                {
                                                    Directory.CreateDirectory(folderPath);
                                                }
                                                int fileCount = 0;
                                                if (_product.OrderingAddressProductFiles == null)
                                                {
                                                    _product.OrderingAddressProductFiles = new List<OrderingAddressProductFile>();
                                                }
                                                if (selProduct.Files != null)
                                                {
                                                    foreach (IFormFile file in selProduct.Files)
                                                    {
                                                        fileCount = fileCount + 1;
                                                        string fileName = file.FileName;
                                                        string contentType = file.ContentType;
                                                        if (fileName.Trim() == "" || fileName.ToLower().Trim() == "blob")
                                                        {
                                                            fileName = DateTime.Now.ToString("ddMMyyyyHHmmss") + fileCount.ToString("D" + 4) + ".jpg";
                                                            contentType = "image/jpeg";
                                                        }
                                                        string filePath = Path.Combine(folderPath, fileName);
                                                        using (FileStream fileStream = new FileStream(filePath, FileMode.Create))
                                                        {
                                                            await file.CopyToAsync(fileStream);
                                                            FileInfo fi = new FileInfo(filePath);

                                                            OrderingAddressProductFile addressProductFile = new OrderingAddressProductFile
                                                            {
                                                                OrderingAddressProduct = addressProduct,
                                                                FileName = fileName,
                                                                ContentType = contentType,
                                                                FilePath = "ordering/" + request.OrderingId.ToString() + "/" + currentTimeStr + "/addressproduct/" + address.Id + "/" + productCount + "/" + fileName,
                                                                DirectoryPath = "ordering/" + request.OrderingId.ToString() + "/" + currentTimeStr + "/addressproduct/" + address.Id + "/" + productCount + "/",
                                                                FileEXT = fi.Extension,
                                                                Type = "fromordering",
                                                                Created = DateTime.Now,
                                                                Modified = DateTime.Now,
                                                                CreatedBy = request.UserId != null ? request.UserId.ToString() : "",
                                                                ModifiedBy = request.UserId != null ? request.UserId.ToString() : "",
                                                            };
                                                            _product.OrderingAddressProductFiles.Add(addressProductFile);
                                                        }
                                                    }
                                                }
                                                if (selProduct.CustomerProductId != null && selProduct.CustomerProductId != 0)
                                                {
                                                    var customerProductObject = (await _customerProductRepository.FindByCondition(x => x.Id == selProduct.CustomerProductId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                                                    if (customerProductObject != null)
                                                    {
                                                        fileCount = 0;

                                                        foreach (CustomerProductFile productFile in customerProductObject.ProductFiles)
                                                        {
                                                            if (File.Exists(Path.Combine(contentPath, productFile.FilePath)))
                                                            {
                                                                string fileName = productFile.FileName;
                                                                string contentType = productFile.ContentType;
                                                                if (fileName.Trim() == "" || fileName.ToLower().Trim() == "blob")
                                                                {
                                                                    fileName = DateTime.Now.ToString("ddMMyyyyHHmmss") + fileCount.ToString("D" + 4) + ".jpg";
                                                                    contentType = "image/jpeg";
                                                                }
                                                                string filePath = Path.Combine(folderPath, fileName);
                                                                File.Copy(Path.Combine(contentPath, productFile.FilePath), filePath);
                                                                OrderingAddressProductFile addressProductFile = new OrderingAddressProductFile
                                                                {
                                                                    OrderingAddressProduct = addressProduct,
                                                                    FileName = fileName,
                                                                    ContentType = contentType,
                                                                    FilePath = "ordering/" + request.OrderingId.ToString() + "/" + currentTimeStr + "/addressproduct/" + address.Id + "/" + productCount + "/" + fileName,
                                                                    DirectoryPath = "ordering/" + request.OrderingId.ToString() + "/" + currentTimeStr + "/addressproduct/" + address.Id + "/" + productCount + "/",
                                                                    FileEXT = productFile.FileEXT,
                                                                    Type = "fromordering",
                                                                    Created = DateTime.Now,
                                                                    Modified = DateTime.Now,
                                                                    CreatedBy = request.UserId != null ? request.UserId.ToString() : "",
                                                                    ModifiedBy = request.UserId != null ? request.UserId.ToString() : "",
                                                                };
                                                                _product.OrderingAddressProductFiles.Add(addressProductFile);
                                                            }
                                                        }
                                                    }
                                                }
                                                orderingAddress.AddressProducts.Add(_product);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {

                        }
                    }
                }

                if(request.DelContainerId != null && request.DelContainerId.Count > 0)
                {
                    foreach (int delContainerId in request.DelContainerId)
                    {
                        var delContainer = ordering.Containers.Where(x => x.Id == delContainerId).FirstOrDefault();
                        if(delContainer != null)
                        {
                            if (delContainer.Files != null)
                            {
                                foreach (OrderingContainerFile containerFile in delContainer.Files)
                                {
                                    if (File.Exists(Path.Combine(contentPath, containerFile.FilePath)))
                                        File.Delete(Path.Combine(contentPath, containerFile.FilePath));
                                }
                                (await _orderingContainerFileRepository.CreateSQLQuery("DELETE Ordering_ContainerFile where OrderingContainerId = " + delContainer.Id.ToString()).ConfigureAwait(false)).ExecuteUpdate();

                            }

                            var containerProduct = ordering.Products.Where(x => x.Name == "#container#").ToList();
                            foreach (OrderingProduct delProduct in containerProduct)
                            {
                                foreach (OrderingProductFile delProductFile in delProduct.ProductFiles)
                                {
                                    if (File.Exists(Path.Combine(contentPath, delProductFile.FilePath)))
                                        File.Delete(Path.Combine(contentPath, delProductFile.FilePath));
                                }
                                (await _orderingProductFileRepository.CreateSQLQuery("DELETE Ordering_ProductFile where OrderingProductId = " + delProduct.Id.ToString()).ConfigureAwait(false)).ExecuteUpdate();
                                (await _orderingProductRepository.CreateSQLQuery("DELETE Ordering_Product where Id = " + delProduct.Id.ToString()).ConfigureAwait(false)).ExecuteUpdate();
                                ordering.Products.Remove(delProduct);
                            }
                             (await _orderingContainerRepository.CreateSQLQuery("DELETE Ordering_Container where Id = " + delContainer.Id.ToString()).ConfigureAwait(false)).ExecuteUpdate();
                            ordering.Containers.Remove(delContainer);
                        }
                    }
                }
                if (request.Containers != null && request.Containers.Count > 0)
                {
                    var containerTypes = (await _containerTypeRepository.FindByCondition(x => x.Active.Equals(true)).ConfigureAwait(false)).AsQueryable().ToList();
                    var containerSpecifications = (await _containerSpecificationRepository.FindByCondition(x => x.Active.Equals(true)).ConfigureAwait(false)).AsQueryable().ToList();
                    var energySavingDevices = (await _energySavingDeviceRepository.FindByCondition(x => x.Active.Equals(true)).ConfigureAwait(false)).AsQueryable().ToList();

                    foreach (UpdateContainerViewModel updateContainer in request.Containers)
                    {
                        var container = ordering.Containers.Where(x => x.Id == updateContainer.Id).FirstOrDefault();
                        if (container != null)
                        {
                            container.Section = updateContainer.Section;
                            container.ContainerBooking = updateContainer.ContainerBooking;
                            container.ContainerType = containerTypes.Where(c => c.Id == updateContainer.ContainerTypeId).FirstOrDefault();
                            container.ContainerSpecification = containerSpecifications.Where(c => c.Id == updateContainer.ContainerSpecificationId).FirstOrDefault();
                            container.EnergySavingDevice = energySavingDevices.Where(c => c.Id == updateContainer.EnergySavingDeviceId).FirstOrDefault();
                            container.Temperature = updateContainer.Temperature;
                            container.Moisture = updateContainer.Moisture;
                            container.Ventilation = updateContainer.Ventilation;
                            container.SpecialSpecify = updateContainer.SpecialSpecify;
                            container.Weight = updateContainer.Weight;
                            container.Commodity = updateContainer.Commodity;
                            container.DateOfBooking = updateContainer.DateOfBooking;
                            container.PickupPointCountry = (await _countryRepository.FindByCondition(x => x.Id == updateContainer.PickupPointCountryId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                            container.PickupPointProvince = (await _provinceRepository.FindByCondition(x => x.Id == updateContainer.PickupPointProvinceId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                            container.PickupPointDistrict = (await _districtRepository.FindByCondition(x => x.Id == updateContainer.PickupPointDistrictId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                            container.PickupPointSubdistrict = (await _subdistrictRepository.FindByCondition(x => x.Id == updateContainer.PickupPointSubdistrictId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                            container.PickupPointPostCode = updateContainer.PickupPointPostCode;
                            container.PickupPointRoad = updateContainer.PickupPointRoad;
                            container.PickupPointAlley = updateContainer.PickupPointAlley;
                            container.PickupPointAddress = updateContainer.PickupPointAddress;
                            container.PickupPointMaps = updateContainer.PickupPointMaps;
                            if (updateContainer.PickupPointDate != null) container.PickupPointDate = updateContainer.PickupPointDate.Value;
                             
                            container.ReturnPointCountry = (await _countryRepository.FindByCondition(x => x.Id == updateContainer.ReturnPointCountryId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                            container.ReturnPointProvince = (await _provinceRepository.FindByCondition(x => x.Id == updateContainer.ReturnPointProvinceId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                            container.ReturnPointDistrict = (await _districtRepository.FindByCondition(x => x.Id == updateContainer.ReturnPointDistrictId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                            container.ReturnPointSubdistrict = (await _subdistrictRepository.FindByCondition(x => x.Id == updateContainer.ReturnPointSubdistrictId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                            container.ReturnPointPostCode = updateContainer.ReturnPointPostCode;
                            container.ReturnPointRoad = updateContainer.ReturnPointRoad;
                            container.ReturnPointAlley = updateContainer.ReturnPointAlley;
                            container.ReturnPointAddress = updateContainer.ReturnPointAddress;
                            container.ReturnPointMaps = updateContainer.ReturnPointMaps;
                            if (updateContainer.ReturnPointDate != null) container.ReturnPointDate = updateContainer.ReturnPointDate.Value;
                            container.CutOffDate = updateContainer.CutOffDate;
                            container.ShippingContact = updateContainer.ShippingContact;
                            container.ShippingContactPhoneCode = updateContainer.ShippingContactPhoneCode;
                            container.ShippingContactPhoneNumber = updateContainer.ShippingContactPhoneNumber;
                            container.YardContact = updateContainer.YardContact;
                            container.YardContactPhoneCode = updateContainer.YardContactPhoneCode;
                            container.YardContactPhoneNumber = updateContainer.YardContactPhoneNumber;
                            container.LinerContact = updateContainer.LinerContact;
                            container.LinerContactPhoneCode = updateContainer.LinerContactPhoneCode;
                            container.LinerContactPhoneNumber = updateContainer.LinerContactPhoneNumber;
                            container.DeliveryOrderNumber = updateContainer.DeliveryOrderNumber;
                            container.BookingNumber = updateContainer.BookingNumber;
                            container.ContainerAmount = updateContainer.ContainerAmount;
                            container.SpecialOrder = updateContainer.SpecialOrder;
                            container.Modified = DateTime.Now;
                            container.ModifiedBy = request.UserId != null ? request.UserId.ToString() : "";
                            string phoneNumber = container.ShippingContactPhoneNumber != null ? container.ShippingContactPhoneNumber : "";
                            if (container.ShippingContactPhoneCode == "+66" && container.ShippingContactPhoneNumber != null && container.ShippingContactPhoneNumber.Length > 9 && container.ShippingContactPhoneNumber.First() == '0')
                            {
                                phoneNumber = container.ShippingContactPhoneNumber.Remove(0, 1);
                            }
                            container.ShippingContactPhoneNumber = phoneNumber;

                            phoneNumber = container.YardContactPhoneNumber != null ? container.YardContactPhoneNumber : "";
                            if (container.YardContactPhoneCode == "+66" && container.YardContactPhoneNumber != null && container.YardContactPhoneNumber.Length > 9 && container.YardContactPhoneNumber.First() == '0')
                            {
                                phoneNumber = container.YardContactPhoneNumber.Remove(0, 1);
                            }
                            container.YardContactPhoneNumber = phoneNumber;

                            phoneNumber = container.LinerContactPhoneNumber != null ? container.LinerContactPhoneNumber : "";
                            if (container.LinerContactPhoneCode == "+66" && container.LinerContactPhoneNumber != null && container.LinerContactPhoneNumber.Length > 9 && container.LinerContactPhoneNumber.First() == '0')
                            {
                                phoneNumber = container.LinerContactPhoneNumber.Remove(0, 1);
                            }
                            container.LinerContactPhoneNumber = phoneNumber;
                            if (updateContainer.DeleteContainerFileId != null && updateContainer.DeleteContainerFileId.Count > 0)
                            {
                                foreach (int delItem in updateContainer.DeleteContainerFileId)
                                {
                                    var deleteContainerFile = container.Files.Where(x => x.Id == delItem).FirstOrDefault();
                                    if (deleteContainerFile != null)
                                    {

                                        if (File.Exists(Path.Combine(contentPath, deleteContainerFile.FilePath)))
                                            File.Delete(Path.Combine(contentPath, deleteContainerFile.FilePath));

                                        (await _orderingContainerFileRepository.CreateSQLQuery("DELETE Ordering_ContainerFile where Id = " + deleteContainerFile.Id.ToString()).ConfigureAwait(false)).ExecuteUpdate();
                                    }
                                    container.Files.Remove(deleteContainerFile);
                                }
                            }
                            if (updateContainer.AttachedFiles != null && updateContainer.AttachedFiles.Count > 0)
                            {
                                if (container.Files == null)
                                {
                                    container.Files = new List<OrderingContainerFile>();
                                }
                                string folderPath = contentPath + "ordering/" + ordering.Id.ToString() + "/" + currentTimeStr + "/container/";
                                if (!Directory.Exists(folderPath))
                                {
                                    Directory.CreateDirectory(folderPath);
                                }
                                int fileCount = 0;
                                foreach (IFormFile file in updateContainer.AttachedFiles)
                                {
                                    fileCount = fileCount + 1;
                                    string fileName = file.FileName;
                                    string contentType = file.ContentType;
                                    if (fileName.Trim() == "" || fileName.ToLower().Trim() == "blob")
                                    {
                                        fileName = DateTime.Now.ToString("ddMMyyyyHHmmss") + fileCount.ToString("D" + 4) + ".jpg";
                                        contentType = "image/jpeg";
                                    }
                                    string filePath = Path.Combine(folderPath, fileName);
                                    using (FileStream fileStream = new FileStream(filePath, FileMode.Create))
                                    {
                                        await file.CopyToAsync(fileStream);
                                        FileInfo fi = new FileInfo(filePath);

                                        OrderingContainerFile orderingContainerFile = new OrderingContainerFile
                                        {
                                            OrderingContainer = container,
                                            FileName = fileName,
                                            ContentType = contentType,
                                            FilePath = "ordering/" + ordering.Id.ToString() + "/" + currentTimeStr + "/container/" + fileName,
                                            DirectoryPath = "ordering/" + ordering.Id.ToString() + "/" + currentTimeStr + "/container/",
                                            FileEXT = fi.Extension,
                                            Created = DateTime.Now,
                                            Modified = DateTime.Now,
                                            CreatedBy = request.UserId != null ? request.UserId.ToString() : "",
                                            ModifiedBy = request.UserId != null ? request.UserId.ToString() : "",
                                        };
                                        container.Files.Add(orderingContainerFile);
                                    }
                                }
                            }
                        }
                        else
                        {
                            container = new OrderingContainer();
                            container.Ordering = ordering;
                            container.Section = updateContainer.Section;
                            container.ContainerBooking = updateContainer.ContainerBooking;
                            container.ContainerType = containerTypes.Where(c => c.Id == updateContainer.ContainerTypeId).FirstOrDefault();
                            container.ContainerSpecification = containerSpecifications.Where(c => c.Id == updateContainer.ContainerSpecificationId).FirstOrDefault();
                            container.EnergySavingDevice = energySavingDevices.Where(c => c.Id == updateContainer.EnergySavingDeviceId).FirstOrDefault();
                            container.Temperature = updateContainer.Temperature;
                            container.Moisture = updateContainer.Moisture;
                            container.Ventilation = updateContainer.Ventilation;
                            container.SpecialSpecify = updateContainer.SpecialSpecify;
                            container.Weight = updateContainer.Weight;
                            container.Commodity = updateContainer.Commodity;
                            container.DateOfBooking = updateContainer.DateOfBooking;
                            container.PickupPointCountry = (await _countryRepository.FindByCondition(x => x.Id == updateContainer.PickupPointCountryId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                            container.PickupPointProvince = (await _provinceRepository.FindByCondition(x => x.Id == updateContainer.PickupPointProvinceId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                            container.PickupPointDistrict = (await _districtRepository.FindByCondition(x => x.Id == updateContainer.PickupPointDistrictId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                            container.PickupPointSubdistrict = (await _subdistrictRepository.FindByCondition(x => x.Id == updateContainer.PickupPointSubdistrictId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                            container.PickupPointPostCode = updateContainer.PickupPointPostCode;
                            container.PickupPointRoad = updateContainer.PickupPointRoad;
                            container.PickupPointAlley = updateContainer.PickupPointAlley;
                            container.PickupPointAddress = updateContainer.PickupPointAddress;
                            container.PickupPointMaps = updateContainer.PickupPointMaps;
                           if(updateContainer.PickupPointDate != null) container.PickupPointDate = updateContainer.PickupPointDate.Value ;
                            container.ReturnPointCountry = (await _countryRepository.FindByCondition(x => x.Id == updateContainer.ReturnPointCountryId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                            container.ReturnPointProvince = (await _provinceRepository.FindByCondition(x => x.Id == updateContainer.ReturnPointProvinceId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                            container.ReturnPointDistrict = (await _districtRepository.FindByCondition(x => x.Id == updateContainer.ReturnPointDistrictId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                            container.ReturnPointSubdistrict = (await _subdistrictRepository.FindByCondition(x => x.Id == updateContainer.ReturnPointSubdistrictId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                            container.ReturnPointPostCode = updateContainer.ReturnPointPostCode;
                            container.ReturnPointRoad = updateContainer.ReturnPointRoad;
                            container.ReturnPointAlley = updateContainer.ReturnPointAlley;
                            container.ReturnPointAddress = updateContainer.ReturnPointAddress;
                            container.ReturnPointMaps = updateContainer.ReturnPointMaps;
                            if(updateContainer.ReturnPointDate != null) container.ReturnPointDate = updateContainer.ReturnPointDate.Value;
                            container.CutOffDate = updateContainer.CutOffDate;
                            container.ShippingContact = updateContainer.ShippingContact;
                            container.ShippingContactPhoneCode = updateContainer.ShippingContactPhoneCode;
                            container.ShippingContactPhoneNumber = updateContainer.ShippingContactPhoneNumber;
                            container.YardContact = updateContainer.YardContact;
                            container.YardContactPhoneCode = updateContainer.YardContactPhoneCode;
                            container.YardContactPhoneNumber = updateContainer.YardContactPhoneNumber;
                            container.LinerContact = updateContainer.LinerContact;
                            container.LinerContactPhoneCode = updateContainer.LinerContactPhoneCode;
                            container.LinerContactPhoneNumber = updateContainer.LinerContactPhoneNumber;
                            container.DeliveryOrderNumber = updateContainer.DeliveryOrderNumber;
                            container.BookingNumber = updateContainer.BookingNumber;
                            container.ContainerAmount = updateContainer.ContainerAmount;
                            container.SpecialOrder = updateContainer.SpecialOrder;
                            container.Modified = DateTime.Now;
                            container.ModifiedBy = request.UserId != null ? request.UserId.ToString() : "";
                            string phoneNumber = container.ShippingContactPhoneNumber != null ? container.ShippingContactPhoneNumber : "";
                            if (container.ShippingContactPhoneCode == "+66" && container.ShippingContactPhoneNumber != null && container.ShippingContactPhoneNumber.Length > 9 && container.ShippingContactPhoneNumber.First() == '0')
                            {
                                phoneNumber = container.ShippingContactPhoneNumber.Remove(0, 1);
                            }
                            container.ShippingContactPhoneNumber = phoneNumber;

                            phoneNumber = container.YardContactPhoneNumber != null ? container.YardContactPhoneNumber : "";
                            if (container.YardContactPhoneCode == "+66" && container.YardContactPhoneNumber != null && container.YardContactPhoneNumber.Length > 9 && container.YardContactPhoneNumber.First() == '0')
                            {
                                phoneNumber = container.YardContactPhoneNumber.Remove(0, 1);
                            }
                            container.YardContactPhoneNumber = phoneNumber;

                            phoneNumber = container.LinerContactPhoneNumber != null ? container.LinerContactPhoneNumber : "";
                            if (container.LinerContactPhoneCode == "+66" && container.LinerContactPhoneNumber != null && container.LinerContactPhoneNumber.Length > 9 && container.LinerContactPhoneNumber.First() == '0')
                            {
                                phoneNumber = container.LinerContactPhoneNumber.Remove(0, 1);
                            }
                            container.LinerContactPhoneNumber = phoneNumber;
                            if (ordering.Containers == null)
                            {
                                ordering.Containers = new List<OrderingContainer>();
                            }
                            if (updateContainer.AttachedFiles != null && updateContainer.AttachedFiles.Count > 0)
                            {
                                if (container.Files == null)
                                {
                                    container.Files = new List<OrderingContainerFile>();
                                }
                                string folderPath = contentPath + "ordering/" + ordering.Id.ToString() + "/" + currentTimeStr + "/container/";
                                if (!Directory.Exists(folderPath))
                                {
                                    Directory.CreateDirectory(folderPath);
                                }
                                int fileCount = 0;
                                foreach (IFormFile file in updateContainer.AttachedFiles)
                                {
                                    fileCount = fileCount + 1;
                                    string fileName = file.FileName;
                                    string contentType = file.ContentType;
                                    if (fileName.Trim() == "" || fileName.ToLower().Trim() == "blob")
                                    {
                                        fileName = DateTime.Now.ToString("ddMMyyyyHHmmss") + fileCount.ToString("D" + 4) + ".jpg";
                                        contentType = "image/jpeg";
                                    }
                                    string filePath = Path.Combine(folderPath, fileName);
                                    using (FileStream fileStream = new FileStream(filePath, FileMode.Create))
                                    {
                                        await file.CopyToAsync(fileStream);
                                        FileInfo fi = new FileInfo(filePath);

                                        OrderingContainerFile orderingContainerFile = new OrderingContainerFile
                                        {
                                            OrderingContainer = container,
                                            FileName = fileName,
                                            ContentType = contentType,
                                            FilePath = "ordering/" + ordering.Id.ToString() + "/" + currentTimeStr + "/container/" + fileName,
                                            DirectoryPath = "ordering/" + ordering.Id.ToString() + "/" + currentTimeStr + "/container/",
                                            FileEXT = fi.Extension,
                                            Created = DateTime.Now,
                                            Modified = DateTime.Now,
                                            CreatedBy = request.UserId != null ? request.UserId.ToString() : "",
                                            ModifiedBy = request.UserId != null ? request.UserId.ToString() : "",
                                        };
                                        container.Files.Add(orderingContainerFile);
                                    }
                                }
                            }
                            ordering.Containers.Add(container);
                        }
                    }
                }

                await _orderingRepository.UpdateAsync(ordering);
                return new Response<Guid>(ordering.Id);
                #region
                //var customerObject = (await _customerRepository.FindByCondition(x => x.Id.Equals(request.CustomerId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                //if (customerObject == null)
                //{
                //    throw new ApiException($"Customer Not Found.");
                //}
                //string currentTimeStr = DateTime.Now.ToString("yyyyMMddHHmmss");
                //int detectArea = Convert.ToInt32(_configuration.GetSection("DetectArea").Value);
                //string api_key = _configuration.GetSection("GoogleAPIKEY").Value;

                //var carTypes = (await _carTypeRepository.FindByCondition(x => x.Active.Equals(true)).ConfigureAwait(false)).AsQueryable().ToList();
                //var carLists = (await _carListRepository.FindByCondition(x => x.Active.Equals(true)).ConfigureAwait(false)).AsQueryable().ToList();
                //var carDescriptions = (await _carDescriptionRepository.FindByCondition(x => x.Active.Equals(true)).ConfigureAwait(false)).ToList().ToList();
                //var carSpecifications = (await _carSpecificationRepository.FindByCondition(x => x.Active.Equals(true)).ConfigureAwait(false)).AsQueryable().ToList();
                //var carFeatures = (await _carFeatureRepository.FindByCondition(x => x.Active.Equals(true)).ConfigureAwait(false)).AsQueryable().ToList();
                //var productTypes = (await _productTypeRepository.FindByCondition(x => x.Active.Equals(true)).ConfigureAwait(false)).AsQueryable().ToList();
                //var packagings = (await _productPackagingRepository.FindByCondition(x => x.Active.Equals(true)).ConfigureAwait(false)).AsQueryable().ToList();
                //var containerTypes = (await _containerTypeRepository.FindByCondition(x => x.Active.Equals(true)).ConfigureAwait(false)).AsQueryable().ToList();
                //var containerSpecifications = (await _containerSpecificationRepository.FindByCondition(x => x.Active.Equals(true)).ConfigureAwait(false)).AsQueryable().ToList();
                //var energySavingDevices = (await _energySavingDeviceRepository.FindByCondition(x => x.Active.Equals(true)).ConfigureAwait(false)).AsQueryable().ToList();

                //string orderStatusPath = $"Shared\\orderingstatus.json";
                //var orderingStatusPathFile = Path.Combine(Directory.GetCurrentDirectory(), orderStatusPath);

                //var statusJson = System.IO.File.ReadAllText(orderingStatusPathFile);
                //List<OrderingStatusViewModel> orderingStatus = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OrderingStatusViewModel>>(statusJson);

                //var getContentPath = _configuration.GetSection("ContentPath");
                //string contentPath = getContentPath.Value; //"D://iTrans/content/";

                //Ordering ordering = new Ordering();
                //OrderingHistory orderingHistory = new OrderingHistory();

                //ordering.Customer = customerObject;
                //ordering.CustomerName = customerObject.Name;
                ////ordering.IsRequestTax = request.IsRequestTax;
                ////ordering.TransportType = request.TransportType;
                //ordering.ProductCBM = request.ProductCBM;
                //ordering.ProductTotalWeight = request.ProductTotalWeight;
                //ordering.AdditionalDetail = request.ProductAdditionalDetail;
                //ordering.OrderingDesiredPrice = request.OrderingDesiredPrice;
                //ordering.IsOrderingDriverOffer = request.IsOrderingDriverOffer;
                //ordering.IsMutipleRoutes = request.IsMutipleRoutes;
                //ordering.Note = request.ProductNote;
                //ordering.Created = DateTime.Now;
                //ordering.Modified = DateTime.Now;
                //ordering.CreatedBy = request.CustomerId != null ? request.CustomerId.ToString() : "";
                //ordering.ModifiedBy = request.CustomerId != null ? request.CustomerId.ToString() : "";

                //ordering.Status = 1;
                //var orderingStatusObject = orderingStatus.Where(o => o.id == ordering.Status).FirstOrDefault();
                //ordering.StatusName = orderingStatusObject != null ? orderingStatusObject.th : "-";

                //if (request.Cars != null)
                //{
                //    List<OrderingCar> cars = new List<OrderingCar>();
                //    var car = _mapper.Map<OrderingCar>(request.Cars);
                //    car.Ordering = ordering;
                //    car.CarType = carTypes.Where(c => c.Id.Equals(request.Cars.CarTypeId)).FirstOrDefault();
                //    car.CarList = carLists.Where(c => c.Id.Equals(request.Cars.CarListId)).FirstOrDefault();
                //    car.CarDescription = carDescriptions.Where(c => c.Id.Equals(request.Cars.CarDescriptionId)).FirstOrDefault();
                //    car.CarSpecification = carSpecifications.Where(c => c.Id.Equals(request.Cars.CarSpecificationId)).FirstOrDefault();
                //    car.CarFeature = carFeatures.Where(c => c.Id.Equals(request.Cars.CarFeatureId)).FirstOrDefault();
                //    car.EnergySavingDevice = energySavingDevices.Where(c => c.Id == request.Cars.EnergySavingDeviceId).FirstOrDefault();
                //    ordering.IsRequestTax = car.IsRequestTax;
                //    ordering.TransportType = car.TransportType;
                //    car.Created = DateTime.Now;
                //    car.Modified = DateTime.Now;
                //    car.CreatedBy = request.CustomerId != null ? request.CustomerId.ToString() : "";
                //    car.ModifiedBy = request.CustomerId != null ? request.CustomerId.ToString() : "";
                //    cars.Add(car);
                //    ordering.Cars = cars;
                //    orderingHistory.Cars = cars;
                //}

                //if (request.Products != null)
                //{
                //    int productCount = 0;
                //    List<OrderingProduct> products = new List<OrderingProduct>();
                //    foreach (CreateProductViewModel productView in request.Products)
                //    {
                //        productCount = productCount + 1;
                //        var product = _mapper.Map<OrderingProduct>(productView);
                //        product.Ordering = ordering;
                //        product.ProductType = "";
                //        foreach (int productId in productView.ProductType)
                //        {
                //            var productType = productTypes.Where(p => p.Id == productId).FirstOrDefault();
                //            if (productType != null)
                //                product.ProductType = product.ProductType + productId + ",";
                //        }
                //        if (product.ProductType != "")
                //            product.ProductType = product.ProductType.Remove(product.ProductType.Length - 1);
                //        product.Packaging = "";
                //        foreach (int packagingId in productView.Packaging)
                //        {
                //            var packaging = packagings.Where(p => p.Id == packagingId).FirstOrDefault();
                //            if (packaging != null)
                //                product.Packaging = product.Packaging + packagingId + ",";
                //        }
                //        if (product.Packaging != "")
                //            product.Packaging = product.Packaging.Remove(product.Packaging.Length - 1);
                //        //product.ProductType = productTypes.Where(p => p.Id.Equals(productView.ProductTypeId)).FirstOrDefault();
                //        //product.Packaging = packagings.Where(p => p.Id.Equals(productView.PackagingId)).FirstOrDefault();
                //        string folderPath = contentPath + "ordering/" + request.CustomerId.ToString() + "/" + currentTimeStr + "/product/" + productCount + "/";
                //        if (!Directory.Exists(folderPath))
                //        {
                //            Directory.CreateDirectory(folderPath);
                //        }
                //        int fileCount = 0;
                //        List<OrderingProductFile> productFiles = new List<OrderingProductFile>();
                //        if (productView.Files != null)
                //        {
                //            foreach (IFormFile file in productView.Files)
                //            {
                //                fileCount = fileCount + 1;
                //                string fileName = file.FileName;
                //                string contentType = file.ContentType;
                //                if (fileName.Trim() == "" || fileName.ToLower().Trim() == "blob")
                //                {
                //                    fileName = DateTime.Now.ToString("ddMMyyyyHHmmss") + fileCount.ToString("D" + 4) + ".jpg";
                //                    contentType = "image/jpeg";
                //                }
                //                string filePath = Path.Combine(folderPath, fileName);
                //                using (FileStream fileStream = new FileStream(filePath, FileMode.Create))
                //                {
                //                    await file.CopyToAsync(fileStream);
                //                    FileInfo fi = new FileInfo(filePath);

                //                    OrderingProductFile orderingProductFile = new OrderingProductFile
                //                    {
                //                        OrderingProduct = product,
                //                        FileName = fileName,
                //                        ContentType = contentType,
                //                        FilePath = "ordering/" + request.CustomerId.ToString() + "/" + currentTimeStr + "/product/" + productCount + "/" + fileName,
                //                        DirectoryPath = "ordering/" + request.CustomerId.ToString() + "/" + currentTimeStr + "/product/" + productCount + "/",
                //                        FileEXT = fi.Extension,
                //                        Created = DateTime.Now,
                //                        Modified = DateTime.Now,
                //                        CreatedBy = request.CustomerId != null ? request.CustomerId.ToString() : "",
                //                        ModifiedBy = request.CustomerId != null ? request.CustomerId.ToString() : "",
                //                    };
                //                    productFiles.Add(orderingProductFile);
                //                }
                //            }
                //        }
                //        if (productView.CustomerProductId != null && productView.CustomerProductId != 0)
                //        {
                //            var customerProductObject = (await _customerProductRepository.FindByCondition(x => x.Id.Equals(productView.CustomerProductId) && x.CustomerId.Equals(request.CustomerId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                //            if (customerProductObject != null)
                //            {
                //                fileCount = 0;

                //                foreach (CustomerProductFile productFile in customerProductObject.ProductFiles)
                //                {
                //                    if (File.Exists(Path.Combine(contentPath, productFile.FilePath)))
                //                    {
                //                        string fileName = productFile.FileName;
                //                        string contentType = productFile.ContentType;
                //                        if (fileName.Trim() == "" || fileName.ToLower().Trim() == "blob")
                //                        {
                //                            fileName = DateTime.Now.ToString("ddMMyyyyHHmmss") + fileCount.ToString("D" + 4) + ".jpg";
                //                            contentType = "image/jpeg";
                //                        }
                //                        string filePath = Path.Combine(folderPath, fileName);
                //                        File.Copy(Path.Combine(contentPath, productFile.FilePath), filePath);
                //                        OrderingProductFile orderingProductFile = new OrderingProductFile
                //                        {
                //                            OrderingProduct = product,
                //                            FileName = fileName,
                //                            ContentType = contentType,
                //                            FilePath = "ordering/" + request.CustomerId.ToString() + "/" + currentTimeStr + "/product/" + productCount + "/" + fileName,
                //                            DirectoryPath = "ordering/" + request.CustomerId.ToString() + "/" + currentTimeStr + "/product/" + productCount + "/",
                //                            FileEXT = productFile.FileEXT,
                //                            Created = DateTime.Now,
                //                            Modified = DateTime.Now,
                //                            CreatedBy = request.CustomerId != null ? request.CustomerId.ToString() : "",
                //                            ModifiedBy = request.CustomerId != null ? request.CustomerId.ToString() : "",
                //                        };
                //                        productFiles.Add(orderingProductFile);
                //                    }
                //                }
                //            }
                //        }
                //        product.ProductFiles = productFiles;
                //        products.Add(product);
                //    }
                //    ordering.Products = products;
                //    orderingHistory.Products = products;
                //}

                //if (request.Addresses != null)
                //{
                //    List<OrderingAddress> addresses = new List<OrderingAddress>();
                //    int addressCount = 0;
                //    foreach (CreateAddressViewModel addressView in request.Addresses)
                //    {
                //        addressCount = addressCount + 1;
                //        var address = _mapper.Map<OrderingAddress>(addressView);
                //        Country country = (await _countryRepository.FindByCondition(x => x.Id.Equals(addressView.CountryId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                //        Province province = (await _provinceRepository.FindByCondition(x => x.Id.Equals(addressView.ProvinceId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                //        District district = (await _districtRepository.FindByCondition(x => x.Id.Equals(addressView.DistrictId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                //        Subdistrict subdistrict = (await _subdistrictRepository.FindByCondition(x => x.Id.Equals(addressView.SubdistrictId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();

                //        string phoneNumber = addressView.PhoneNumber != null ? addressView.PhoneNumber.Trim() : "";
                //        if (addressView.PhoneCode == "+66" && addressView.PhoneNumber != null && addressView.PhoneNumber.Length > 9 && addressView.PhoneNumber.First() == '0')
                //        {
                //            phoneNumber = addressView.PhoneNumber.Remove(0, 1);
                //        }
                //        addressView.PhoneNumber = phoneNumber;

                //        List<OrderingAddressProduct> addressProducts = new List<OrderingAddressProduct>();
                //        if (addressView.Products != null)
                //        {
                //            int productCount = 0;
                //            foreach (CreateProductFormViewModel viewProduct in addressView.Products)
                //            {
                //                var selProduct = request.Products.Where(p => p.Sequence.Equals(viewProduct.Product)).FirstOrDefault();
                //                if (selProduct != null)
                //                {
                //                    productCount = productCount + 1;
                //                    var addressProduct = _mapper.Map<OrderingAddressProduct>(selProduct);

                //                    addressProduct.ProductType = "";
                //                    foreach (int productId in selProduct.ProductType)
                //                    {
                //                        var productType = productTypes.Where(p => p.Id == productId).FirstOrDefault();
                //                        if (productType != null)
                //                            addressProduct.ProductType = addressProduct.ProductType + productId + ",";
                //                    }
                //                    if (addressProduct.ProductType != "")
                //                        addressProduct.ProductType = addressProduct.ProductType.Remove(addressProduct.ProductType.Length - 1);
                //                    addressProduct.Packaging = "";
                //                    foreach (int packagingId in selProduct.Packaging)
                //                    {
                //                        var packaging = packagings.Where(p => p.Id == packagingId).FirstOrDefault();
                //                        if (packaging != null)
                //                            addressProduct.Packaging = addressProduct.Packaging + packagingId + ",";
                //                    }
                //                    if (addressProduct.Packaging != "")
                //                        addressProduct.Packaging = addressProduct.Packaging.Remove(addressProduct.Packaging.Length - 1);

                //                    addressProduct.OrderingProduct = ordering.Products.Where(p => p.Sequence.Equals(viewProduct.Product)).FirstOrDefault();
                //                    addressProduct.Quantity = viewProduct.Quantity;
                //                    addressProduct.Sequence = viewProduct.Sequence;
                //                    addressProduct.OrderingAddress = address;
                //                    //addressProduct.ProductType = productTypes.Where(p => p.Id.Equals(selProduct.ProductTypeId)).FirstOrDefault();
                //                    //addressProduct.Packaging = packagings.Where(p => p.Id.Equals(selProduct.PackagingId)).FirstOrDefault();
                //                    addressProducts.Add(addressProduct);
                //                    string folderPath = contentPath + "ordering/" + request.CustomerId.ToString() + "/" + currentTimeStr + "/addressproduct/" + addressCount + "/" + productCount + "/";
                //                    if (!Directory.Exists(folderPath))
                //                    {
                //                        Directory.CreateDirectory(folderPath);
                //                    }
                //                    int fileCount = 0;
                //                    List<OrderingAddressProductFile> AddressProductFiles = new List<OrderingAddressProductFile>();
                //                    if (selProduct.Files != null)
                //                    {

                //                        foreach (IFormFile file in selProduct.Files)
                //                        {
                //                            fileCount = fileCount + 1;
                //                            string fileName = file.FileName;
                //                            string contentType = file.ContentType;
                //                            if (fileName.Trim() == "" || fileName.ToLower().Trim() == "blob")
                //                            {
                //                                fileName = DateTime.Now.ToString("ddMMyyyyHHmmss") + fileCount.ToString("D" + 4) + ".jpg";
                //                                contentType = "image/jpeg";
                //                            }
                //                            string filePath = Path.Combine(folderPath, fileName);
                //                            using (FileStream fileStream = new FileStream(filePath, FileMode.Create))
                //                            {
                //                                await file.CopyToAsync(fileStream);
                //                                FileInfo fi = new FileInfo(filePath);

                //                                OrderingAddressProductFile addressProductFile = new OrderingAddressProductFile
                //                                {
                //                                    OrderingAddressProduct = addressProduct,
                //                                    FileName = fileName,
                //                                    ContentType = contentType,
                //                                    FilePath = "ordering/" + request.CustomerId.ToString() + "/" + currentTimeStr + "/addressproduct/" + addressCount + "/" + productCount + "/" + fileName,
                //                                    DirectoryPath = "ordering/" + request.CustomerId.ToString() + "/" + currentTimeStr + "/addressproduct/" + addressCount + "/" + productCount + "/",
                //                                    FileEXT = fi.Extension,
                //                                    Type = "fromordering",
                //                                    Created = DateTime.Now,
                //                                    Modified = DateTime.Now,
                //                                    CreatedBy = request.CustomerId != null ? request.CustomerId.ToString() : "",
                //                                    ModifiedBy = request.CustomerId != null ? request.CustomerId.ToString() : "",
                //                                };
                //                                AddressProductFiles.Add(addressProductFile);
                //                            }
                //                        }
                //                    }
                //                    if (selProduct.CustomerProductId != null && selProduct.CustomerProductId != 0)
                //                    {
                //                        var customerProductObject = (await _customerProductRepository.FindByCondition(x => x.Id.Equals(selProduct.CustomerProductId) && x.CustomerId.Equals(request.CustomerId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                //                        if (customerProductObject != null)
                //                        {
                //                            fileCount = 0;

                //                            foreach (CustomerProductFile productFile in customerProductObject.ProductFiles)
                //                            {
                //                                if (File.Exists(Path.Combine(contentPath, productFile.FilePath)))
                //                                {
                //                                    string fileName = productFile.FileName;
                //                                    string contentType = productFile.ContentType;
                //                                    if (fileName.Trim() == "" || fileName.ToLower().Trim() == "blob")
                //                                    {
                //                                        fileName = DateTime.Now.ToString("ddMMyyyyHHmmss") + fileCount.ToString("D" + 4) + ".jpg";
                //                                        contentType = "image/jpeg";
                //                                    }
                //                                    string filePath = Path.Combine(folderPath, fileName);
                //                                    File.Copy(Path.Combine(contentPath, productFile.FilePath), filePath);
                //                                    OrderingAddressProductFile addressProductFile = new OrderingAddressProductFile
                //                                    {
                //                                        OrderingAddressProduct = addressProduct,
                //                                        FileName = fileName,
                //                                        ContentType = contentType,
                //                                        FilePath = "ordering/" + request.CustomerId.ToString() + "/" + currentTimeStr + "/addressproduct/" + addressCount + "/" + productCount + "/" + fileName,
                //                                        DirectoryPath = "ordering/" + request.CustomerId.ToString() + "/" + currentTimeStr + "/addressproduct/" + addressCount + "/" + productCount + "/",
                //                                        FileEXT = productFile.FileEXT,
                //                                        Type = "fromordering",
                //                                        Created = DateTime.Now,
                //                                        Modified = DateTime.Now,
                //                                        CreatedBy = request.CustomerId != null ? request.CustomerId.ToString() : "",
                //                                        ModifiedBy = request.CustomerId != null ? request.CustomerId.ToString() : "",
                //                                    };
                //                                    AddressProductFiles.Add(addressProductFile);
                //                                }
                //                            }
                //                        }
                //                    }
                //                    addressProduct.OrderingAddressProductFiles = AddressProductFiles;
                //                }

                //            }
                //            address.AddressProducts = addressProducts;
                //        }
                //        address.Ordering = ordering;
                //        address.Country = country;
                //        address.Province = province;
                //        address.District = district;
                //        address.Subdistrict = subdistrict;
                //        address.Status = 1;
                //        address.Created = DateTime.Now;
                //        address.CreatedBy = request.CustomerId != null ? request.CustomerId.ToString() : "-";
                //        address.Modified = DateTime.Now;
                //        address.ModifiedBy = request.CustomerId != null ? request.CustomerId.ToString() : "-";
                //        addresses.Add(address);
                //    }
                //    if (addresses.Count() > 1)
                //    {
                //        var locationDistince = LocationHelper.GetDistanceAndEstimateTime(detectArea, api_key, addresses[0].Maps, addresses[addresses.Count() - 1].Maps);
                //        ordering.TotalDistance = locationDistince.Distance;
                //        ordering.TotalEstimateTime = locationDistince.EstimateTime;
                //        ordering.Distance = locationDistince.Distance;
                //        ordering.EstimateTime = locationDistince.EstimateTime;
                //    }
                //    ordering.Addresses = addresses;
                //    var firstPickupPoint = addresses.Where(a => a.AddressType == "pickuppoint").OrderBy(a => a.Sequence).FirstOrDefault();
                //    var lastDeliveryPoint = addresses.Where(a => a.AddressType == "deliverypoint").OrderByDescending(a => a.Sequence).FirstOrDefault();
                //    if(firstPickupPoint != null)
                //    {
                //        ordering.PickupPoint = firstPickupPoint.Province.Name_TH;
                //        ordering.PickupPointDate = firstPickupPoint.Date;
                //    }
                //    if (lastDeliveryPoint != null)
                //    {
                //        ordering.RecipientName = lastDeliveryPoint.PersonalName;
                //        ordering.PickupPoint = lastDeliveryPoint.Province.Name_TH;
                //        ordering.PickupPointDate = lastDeliveryPoint.Date;
                //    }
                //    orderingHistory.Addresses = addresses;
                //}

                //if (request.Containers != null)
                //{
                //    List<OrderingContainer> containers = new List<OrderingContainer>();
                //    foreach (CreateContainerViewModel containerObj in request.Containers)
                //    {
                //        var container = _mapper.Map<OrderingContainer>(containerObj);
                //        container.Ordering = ordering;
                //        container.ContainerType = containerTypes.Where(c => c.Id == containerObj.ContainerTypeId).FirstOrDefault();
                //        container.ContainerSpecification = containerSpecifications.Where(c => c.Id == containerObj.ContainerSpecificationId).FirstOrDefault();
                //        container.EnergySavingDevice = energySavingDevices.Where(c => c.Id == containerObj.EnergySavingDeviceId).FirstOrDefault();
                //        container.PickupPointCountry = (await _countryRepository.FindByCondition(x => x.Id.Equals(containerObj.PickupPointCountryId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                //        container.PickupPointProvince = (await _provinceRepository.FindByCondition(x => x.Id.Equals(containerObj.PickupPointProvinceId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                //        container.PickupPointDistrict = (await _districtRepository.FindByCondition(x => x.Id.Equals(containerObj.PickupPointDistrictId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                //        container.PickupPointSubdistrict = (await _subdistrictRepository.FindByCondition(x => x.Id.Equals(containerObj.PickupPointSubdistrictId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                //        container.ReturnPointCountry = (await _countryRepository.FindByCondition(x => x.Id.Equals(containerObj.ReturnPointCountryId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                //        container.ReturnPointProvince = (await _provinceRepository.FindByCondition(x => x.Id.Equals(containerObj.ReturnPointProvinceId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                //        container.ReturnPointDistrict = (await _districtRepository.FindByCondition(x => x.Id.Equals(containerObj.ReturnPointDistrictId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                //        container.ReturnPointSubdistrict = (await _subdistrictRepository.FindByCondition(x => x.Id.Equals(containerObj.ReturnPointSubdistrictId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                //        container.Created = DateTime.Now;
                //        container.Modified = DateTime.Now;
                //        container.CreatedBy = request.CustomerId != null ? request.CustomerId.ToString() : "";
                //        container.ModifiedBy = request.CustomerId != null ? request.CustomerId.ToString() : "";
                //        string phoneNumber = containerObj.ShippingContactPhoneNumber != null ? containerObj.ShippingContactPhoneNumber : "";
                //        if (containerObj.ShippingContactPhoneCode == "+66" && containerObj.ShippingContactPhoneNumber != null && containerObj.ShippingContactPhoneNumber.Length > 9 && containerObj.ShippingContactPhoneNumber.First() == '0')
                //        {
                //            phoneNumber = containerObj.ShippingContactPhoneNumber.Remove(0, 1);
                //        }
                //        containerObj.ShippingContactPhoneNumber = phoneNumber;

                //        phoneNumber = containerObj.YardContactPhoneNumber != null ? containerObj.YardContactPhoneNumber : "";
                //        if (containerObj.YardContactPhoneCode == "+66" && containerObj.YardContactPhoneNumber != null && containerObj.YardContactPhoneNumber.Length > 9 && containerObj.YardContactPhoneNumber.First() == '0')
                //        {
                //            phoneNumber = containerObj.YardContactPhoneNumber.Remove(0, 1);
                //        }
                //        containerObj.YardContactPhoneNumber = phoneNumber;

                //        phoneNumber = containerObj.LinerContactPhoneNumber != null ? containerObj.LinerContactPhoneNumber : "";
                //        if (containerObj.LinerContactPhoneCode == "+66" && containerObj.LinerContactPhoneNumber != null && containerObj.LinerContactPhoneNumber.Length > 9 && containerObj.LinerContactPhoneNumber.First() == '0')
                //        {
                //            phoneNumber = containerObj.LinerContactPhoneNumber.Remove(0, 1);
                //        }
                //        containerObj.LinerContactPhoneNumber = phoneNumber;

                //        containers.Add(container);
                //    }
                //    ordering.Containers = containers;
                //    orderingHistory.Containers = containers;
                //}

                //var driverObject = (await _driverRepository.FindByCondition(x => x.Id.Equals(request.DriverId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();

                //var dataObject = await _orderingRepository.AddAsync(ordering);
                ////if (dataObject != null)
                ////{
                ////    customerObject.RequestCarPerYear = customerObject.RequestCarPerYear + 1;
                ////    customerObject.RequestCarPerMonth = customerObject.RequestCarPerMonth + 1;


                ////    var customerActivity = (await _customerActivityRepository.FindByCondition(x => x.CustomerId.Equals(request.CustomerId) && x.Date == DateTime.Now.ToString("yyyy'-'MM'-'dd")).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                ////    if (customerActivity == null)
                ////    {
                ////        CustomerActivity newCustomerActivity = new CustomerActivity
                ////        {
                ////            CustomerId = request.CustomerId,
                ////            Date = DateTime.Now.ToString("yyyy'-'MM'-'dd"),
                ////            Star = 0,
                ////            RequestCar = 1,
                ////            Cancel = 0,
                ////            OrderingValue = 0,
                ////            Created = DateTime.Now,
                ////            CreatedBy = request.CustomerId != null ? request.CustomerId.ToString() : "-",
                ////            Modified = DateTime.Now,
                ////            ModifiedBy = request.CustomerId != null ? request.CustomerId.ToString() : "-",
                ////        };
                ////        await _customerActivityRepository.AddAsync(newCustomerActivity);
                ////    }
                ////    else
                ////    {
                ////        customerActivity.RequestCar = customerActivity.RequestCar + 1;
                ////        await _customerActivityRepository.UpdateAsync(customerActivity);
                ////    }
                ////}

                //orderingHistory.TrackingCode = ordering.TrackingCode;
                //orderingHistory.OrderingId = ordering.Id;
                //orderingHistory.Driver = ordering.Driver;
                //orderingHistory.Customer = ordering.Customer;
                //orderingHistory.ProductCBM = ordering.ProductCBM;
                //orderingHistory.ProductTotalWeight = ordering.ProductTotalWeight;
                //orderingHistory.OrderingPrice = ordering.OrderingPrice;
                //orderingHistory.OrderingStatus = ordering.Status;
                //orderingHistory.AdditionalDetail = ordering.AdditionalDetail;
                //orderingHistory.Note = ordering.Note;
                //orderingHistory.OrderNumber = ordering.TrackingCode;
                //orderingHistory.OrderingDesiredPrice = ordering.OrderingDesiredPrice;
                //orderingHistory.OrderingDriverOffering = ordering.OrderingDriverOffering;
                //orderingHistory.IsOrderingDriverOffer = ordering.IsOrderingDriverOffer;
                //orderingHistory.IsMutipleRoutes = ordering.IsMutipleRoutes;
                //orderingHistory.CancelStatus = ordering.CancelStatus;
                //orderingHistory.CustomerRanking = ordering.CustomerRanking;
                //orderingHistory.DriverRanking = ordering.DriverRanking;
                //orderingHistory.PinnedDate = ordering.PinnedDate;
                //orderingHistory.Distance = ordering.Distance;
                //orderingHistory.EstimateTime = ordering.EstimateTime;
                //orderingHistory.IsDriverPay = ordering.IsDriverPay;
                //orderingHistory.CurrentLocation = ordering.CurrentLocation;
                //orderingHistory.GPValue = ordering.GPValue;
                //orderingHistory.DriverPayValue = ordering.DriverPayValue;
                //orderingHistory.CustomerCancelValue = ordering.CustomerCancelValue;
                //orderingHistory.DriverCancelValue = ordering.DriverCancelValue;
                //orderingHistory.CustomerCashBack = ordering.CustomerCashBack;
                //orderingHistory.DriverCashBack = ordering.DriverCashBack;
                //orderingHistory.Insurances = ordering.Insurances;
                //orderingHistory.Payments = ordering.Payments;
                //orderingHistory.DriverAnnouncements = ordering.DriverAnnouncements;
                //orderingHistory.Created = DateTime.UtcNow;
                //orderingHistory.Modified = DateTime.UtcNow;
                //orderingHistory.CreatedBy = request.CustomerId != null ? request.CustomerId.ToString() : "";
                //orderingHistory.ModifiedBy = request.CustomerId != null ? request.CustomerId.ToString() : "";

                //await _orderingHistoryRepository.AddAsync(orderingHistory);

                //if (driverObject != null)
                //{
                //    #region
                //    //var orderingStatus = Path.Combine(Directory.GetCurrentDirectory(), $"Shared\\driver_orderingstatus.json");
                //    //var orderingStatusJson = System.IO.File.ReadAllText(orderingStatus);
                //    //List<OrderingStatusViewModel> orderingStatusModel = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OrderingStatusViewModel>>(orderingStatusJson);

                //    //var cloneOrdering = _mapper.Map<OrderingViewModel>(dataObject);
                //    var cloneOrdering = new OrderingNotificationViewModel
                //    {
                //        id = dataObject.Id,
                //        trackingCode = dataObject.TrackingCode,
                //        orderNumber = dataObject.TrackingCode,
                //        status = dataObject.Status,
                //        products = _mapper.Map<List<OrderingProductNotificationViewModel>>(dataObject.Products)
                //    };
                //    if (dataObject.Cars.Count > 0)
                //    {
                //        cloneOrdering.carType = _mapper.Map<CarTypeViewModel>(dataObject.Cars[0].CarType);
                //        cloneOrdering.carList = _mapper.Map<CarListViewModel>(dataObject.Cars[0].CarList);
                //    }
                //    if (cloneOrdering.status != null)
                //    {
                //        cloneOrdering.statusObj = orderingStatus.Where(o => o.id == cloneOrdering.status).FirstOrDefault();
                //    }
                //    foreach (OrderingProductNotificationViewModel productViewModel in cloneOrdering.products)
                //    {
                //        if (productViewModel.productType != null)
                //        {
                //            string[] productTypeIds = productViewModel.productType.Split(",");
                //            productViewModel.productTypes = new List<ProductTypeViewModel>();
                //            foreach (string productId in productTypeIds)
                //            {
                //                int id = 0;
                //                if (int.TryParse(productId, out id))
                //                {
                //                    var productType = productTypes.Where(p => p.Id == id).FirstOrDefault();
                //                    if (productType != null)
                //                        productViewModel.productTypes.Add(_mapper.Map<ProductTypeViewModel>(productType));
                //                }
                //            }
                //        }
                //        if (productViewModel.packaging != null)
                //        {
                //            string[] packagingIds = productViewModel.packaging.Split(",");
                //            productViewModel.packagings = new List<ProductPackagingViewModel>();
                //            foreach (string packagingId in packagingIds)
                //            {
                //                int id = 0;
                //                if (int.TryParse(packagingId, out id))
                //                {
                //                    var packaging = packagings.Where(p => p.Id == id).FirstOrDefault();
                //                    if (packaging != null)
                //                        productViewModel.packagings.Add(_mapper.Map<ProductPackagingViewModel>(packaging));
                //                }
                //            }
                //        }

                //    }
                //    if (dataObject.Addresses.Count > 0)
                //    {
                //        cloneOrdering.startPoint = _mapper.Map<OrderingAddressNotificationViewModel>(dataObject.Addresses[0]);
                //        cloneOrdering.endPoint = _mapper.Map<OrderingAddressNotificationViewModel>(dataObject.Addresses[dataObject.Addresses.Count - 1]);
                //    }

                //    //foreach (OrderingAddressViewModel orderingAddressViewModel in cloneOrdering.addresses)
                //    //{
                //    //    foreach (OrderingAddressProductViewModel orderingAddressProductViewModel in orderingAddressViewModel.products)
                //    //    {
                //    //        if (orderingAddressProductViewModel.productType != null)
                //    //        {
                //    //            string[] productTypeIds = orderingAddressProductViewModel.productType.Split(",");
                //    //            orderingAddressProductViewModel.productTypes = new List<ProductTypeViewModel>();
                //    //            foreach (string productId in productTypeIds)
                //    //            {
                //    //                int id = 0;
                //    //                if (int.TryParse(productId, out id))
                //    //                {
                //    //                    var productType = productTypes.Where(p => p.Id == id).FirstOrDefault();
                //    //                    if (productType != null)
                //    //                        orderingAddressProductViewModel.productTypes.Add(_mapper.Map<ProductTypeViewModel>(productType));
                //    //                }
                //    //            }
                //    //        }
                //    //        if (orderingAddressProductViewModel.packaging != null)
                //    //        {
                //    //            string[] packagingIds = orderingAddressProductViewModel.packaging.Split(",");
                //    //            orderingAddressProductViewModel.packagings = new List<ProductPackagingViewModel>();
                //    //            foreach (string packagingId in packagingIds)
                //    //            {
                //    //                int id = 0;
                //    //                if (int.TryParse(packagingId, out id))
                //    //                {
                //    //                    var packaging = packagings.Where(p => p.Id == id).FirstOrDefault();
                //    //                    if (packaging != null)
                //    //                        orderingAddressProductViewModel.packagings.Add(_mapper.Map<ProductPackagingViewModel>(packaging));
                //    //                }
                //    //            }
                //    //        }
                //    //    }
                //    //}

                //    #endregion
                //    //#region add noti list
                //    //var oneSignalIds = (await _notificationUserRepository.FindByCondition(x => x.UserId.Equals(request.DriverId)).ConfigureAwait(false)).AsQueryable().Select(x => x.OneSignalId).ToList();

                //    //var notificationDetails = Path.Combine(Directory.GetCurrentDirectory(), $"Shared\\NotificationWord.json");
                //    //var notificationJson = System.IO.File.ReadAllText(notificationDetails);
                //    //List<NotificationWordStatusViewModel> notificationStatus = Newtonsoft.Json.JsonConvert.DeserializeObject<List<NotificationWordStatusViewModel>>(notificationJson);

                //    //string title = "";
                //    //string detail = "";
                //    //var notificationObj = notificationStatus.Where(o => o.id == 1).FirstOrDefault();
                //    //if (notificationObj != null)
                //    //{
                //    //    title = notificationObj.title.th;
                //    //    if (dataObject.Customer != null)
                //    //    {
                //    //        detail = notificationObj.detail.th.Replace("#customertitle#", dataObject.Customer.Title != null ? dataObject.Customer.Title.Name_TH : "")
                //    //                 .Replace("#customername#", dataObject.Customer.Name)
                //    //                 .Replace("#orderprice#", dataObject.OrderingDesiredPrice != null || dataObject.OrderingDesiredPrice != 0 ? dataObject.OrderingDesiredPrice.ToString() : "-");
                //    //    }
                //    //}
                //    ////if (cloneOrdering.products.Count > 2)
                //    ////{
                //    ////    cloneOrdering.products.RemoveRange(2, cloneOrdering.products.Count - 2);
                //    ////}
                //    //Domain.Notification notification = new Domain.Notification();

                //    //notification.OneSignalId = oneSignalIds.FirstOrDefault();
                //    //notification.UserId = request.DriverId;
                //    //notification.OwnerId = request.CustomerId;
                //    //notification.Title = title;
                //    //notification.Detail = detail;
                //    //notification.ServiceCode = "Ordering";
                //    ////notification.ReferenceContentKey = request.ReferenceContentKey;
                //    //notification.Payload = Newtonsoft.Json.JsonConvert.SerializeObject(cloneOrdering);
                //    //notification.Created = DateTime.UtcNow;
                //    //notification.Modified = DateTime.UtcNow;
                //    //notification.CreatedBy = request.CustomerId.ToString();
                //    //notification.ModifiedBy = request.CustomerId.ToString();

                //    //await _notificationRepository.AddAsync(notification);
                //    //#endregion

                //    //#region send noti

                //    //if (oneSignalIds.Count > 0)
                //    //{
                //    //    NotificationManager notiMgr = new NotificationManager(request.APP_ID, request.REST_API_KEY, request.AUTH_ID);
                //    //    notiMgr.SendNotificationAsync(title, detail, oneSignalIds, notification.ServiceCode, "", "{id:'" + cloneOrdering.id + "'}");
                //    //}

                //    //#endregion
                //}
                //var log = new CreateAppLog(_applicationLogRepository);
                //log.Create("Ordering", "Ordering", "Create", Newtonsoft.Json.JsonConvert.SerializeObject(request));
                //return new Response<Guid>(dataObject.Id);
                #endregion
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}

