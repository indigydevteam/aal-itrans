﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.Orderings.Commands
{
    public class CustomerCreateOrderingCommandValidator : AbstractValidator<CustomerCreateOrderingCommand>
    {
        private readonly ICustomerRepositoryAsync customerRepository;

        public CustomerCreateOrderingCommandValidator(ICustomerRepositoryAsync customerRepository)
        {
            this.customerRepository = customerRepository;

            RuleFor(p => p.CustomerId)
                .NotEmpty().WithMessage("{PropertyName} is required.")
                .NotNull()
                .MustAsync(IsExistCustomer).WithMessage("{PropertyName} is not exists.");

        }

        private async Task<bool> IsExistCustomer(Guid customerId, CancellationToken cancellationToken)
        {
            var customerObject = (await customerRepository.FindByCondition(x => x.Id.Equals(customerId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            if (customerObject == null)
            {
                return false;
            }
            return true;
        }
    }
}
