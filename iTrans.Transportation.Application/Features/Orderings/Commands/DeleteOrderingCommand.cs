﻿using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;


namespace iTrans.Transportation.Application.Features.Orderings.Commands
{
    public class DeleteOrderingCommand : IRequest<Response<Guid>>
    {
        public Guid Id { get; set; }
        public Guid CustomerId { get; set; }
        public class DeleteOrderingCommandHandler : IRequestHandler<DeleteOrderingCommand, Response<Guid>>
        {
            private readonly IOrderingRepositoryAsync _orderingRepository;
            private readonly IOrderingAddressRepositoryAsync _orderingAddressRepository;
            private readonly IOrderingAddressFileRepositoryAsync _orderingAddressFileRepository;
            private readonly IOrderingAddressProductRepositoryAsync _orderingAddressProductRepository;
            private readonly IOrderingAddressProductFileRepositoryAsync _orderingAddressProductFileRepository;
            private readonly IOrderingCarRepositoryAsync _orderingCarRepository;
            private readonly IOrderingCarFileRepositoryAsync _orderingCarFileRepository;
            private readonly IOrderingContainerRepositoryAsync _orderingContainerRepository;
            private readonly IOrderingDriverRepositoryAsync _orderingDriverRepository;
            private readonly IOrderingDriverFileRepositoryAsync _orderingDriverFileRepository;
            private readonly IOrderingInsuranceRepositoryAsync _orderingInsuranceRepository;
            private readonly IOrderingProductRepositoryAsync _orderingProductRepository;
            private readonly IOrderingPaymentRepositoryAsync _orderingPaymentRepository;
            private readonly IOrderingProductFileRepositoryAsync _orderingProductFileRepository;
            private readonly IConfiguration _configuration;
            public DeleteOrderingCommandHandler(IOrderingRepositoryAsync orderingRepository, IOrderingAddressFileRepositoryAsync orderingAddressFileRepository
                , IOrderingAddressProductFileRepositoryAsync orderingAddressProductFileRepository, IOrderingAddressRepositoryAsync orderingAddressRepository
                , IOrderingAddressProductRepositoryAsync orderingAddressProductRepository, IOrderingCarRepositoryAsync orderingCarRepository
                , IOrderingDriverRepositoryAsync orderingDriverRepository, IOrderingInsuranceRepositoryAsync orderingInsuranceRepository
                , IOrderingProductRepositoryAsync orderingProductRepository, IOrderingProductFileRepositoryAsync orderingProductFileRepository, IOrderingPaymentRepositoryAsync orderingPaymentRepository
                , IOrderingDriverFileRepositoryAsync orderingDriverFileRepository, IOrderingCarFileRepositoryAsync orderingCarFileRepository, IOrderingContainerRepositoryAsync orderingContainerRepository, IConfiguration configuration)
            {
                _orderingAddressFileRepository = orderingAddressFileRepository;
                _orderingAddressRepository = orderingAddressRepository;
                _orderingAddressProductRepository = orderingAddressProductRepository;
                _orderingAddressProductFileRepository = orderingAddressProductFileRepository;
                _orderingCarRepository = orderingCarRepository;
                _orderingCarFileRepository = orderingCarFileRepository;
                _orderingDriverRepository = orderingDriverRepository;
                _orderingDriverFileRepository = orderingDriverFileRepository;
                _orderingProductRepository = orderingProductRepository;
                _orderingProductFileRepository = orderingProductFileRepository;
                _orderingRepository = orderingRepository;
                _orderingContainerRepository = orderingContainerRepository;
                _orderingInsuranceRepository = orderingInsuranceRepository;
                _orderingPaymentRepository = orderingPaymentRepository;
                _configuration = configuration;
            }
            public async Task<Response<Guid>> Handle(DeleteOrderingCommand command, CancellationToken cancellationToken)
            {
                var data = (await _orderingRepository.FindByCondition(x => x.Id == command.Id && x.Customer.Id.Equals(command.CustomerId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (data == null)
                {
                    throw new ApiException($"Ordering Not Found.");

                }
                if (data.TrackingCode != null)
                {
                    throw new ApiException($"Ordering can not delete.");

                }
                try
                {
                    var getContentPath = _configuration.GetSection("ContentPath");
                    string contentPath = getContentPath.Value;

                    foreach (OrderingAddress orderingAddress in data.Addresses)
                    {
                        foreach (OrderingAddressFile orderingAddressFile in orderingAddress.Files)
                        {
                            if (File.Exists(Path.Combine(contentPath, orderingAddressFile.FilePath)))
                            {
                                File.Delete(Path.Combine(contentPath, orderingAddressFile.FilePath));
                            }
                        }
                        (await _orderingAddressFileRepository.CreateSQLQuery("DELETE Ordering_AddressFile where OrderingAddressId = '" + orderingAddress.Id.ToString() + "'").ConfigureAwait(false)).ExecuteUpdate();
                        orderingAddress.Files.Clear();
                        foreach (OrderingAddressProduct orderingAddressProduct in orderingAddress.AddressProducts)
                        {
                            foreach (OrderingAddressProductFile orderingAddressProductFile in orderingAddressProduct.OrderingAddressProductFiles)
                            {
                                if (File.Exists(Path.Combine(contentPath, orderingAddressProductFile.FilePath)))
                                {
                                    File.Delete(Path.Combine(contentPath, orderingAddressProductFile.FilePath));
                                }
                            }
                            (await _orderingAddressProductFileRepository.CreateSQLQuery("DELETE Ordering_AddressProductFile where OrderingAddressProductId = '" + orderingAddressProduct.Id.ToString() + "'").ConfigureAwait(false)).ExecuteUpdate();
                            orderingAddressProduct.OrderingAddressProductFiles.Clear();
                        }
                        (await _orderingAddressProductRepository.CreateSQLQuery("DELETE Ordering_AddressProduct where OrderingAddressId = '" + orderingAddress.Id.ToString() + "'").ConfigureAwait(false)).ExecuteUpdate();
                        orderingAddress.AddressProducts.Clear();
                    }
                     (await _orderingAddressRepository.CreateSQLQuery("DELETE Ordering_Address where OrderingId = '" + data.Id.ToString() + "'").ConfigureAwait(false)).ExecuteUpdate();

                    foreach (OrderingCar orderingCar in data.Cars)
                    {
                        foreach (OrderingCarFile orderingCarFile in orderingCar.CarFiles)
                        {
                            if (File.Exists(Path.Combine(contentPath, orderingCarFile.FilePath)))
                            {
                                File.Delete(Path.Combine(contentPath, orderingCarFile.FilePath));
                            }
                        }
                        (await _orderingCarFileRepository.CreateSQLQuery("DELETE Ordering_CarFile where OrderingCarId = '" + orderingCar.Id.ToString() + "'").ConfigureAwait(false)).ExecuteUpdate();
                        orderingCar.CarFiles.Clear();
                    }
                    (await _orderingCarRepository.CreateSQLQuery("DELETE Ordering_Car where OrderingId = '" + data.Id.ToString() + "'").ConfigureAwait(false)).ExecuteUpdate();
                    (await _orderingContainerRepository.CreateSQLQuery("DELETE Ordering_Container where OrderingId = '" + data.Id.ToString() + "'").ConfigureAwait(false)).ExecuteUpdate();

                    foreach (OrderingDriver orderingDriver in data.Drivers)
                    {
                        foreach (OrderingDriverFile orderingDriverFile in orderingDriver.DriverFiles)
                        {
                            if (File.Exists(Path.Combine(contentPath, orderingDriverFile.FilePath)))
                            {
                                File.Delete(Path.Combine(contentPath, orderingDriverFile.FilePath));
                            }
                        }
                        (await _orderingDriverFileRepository.CreateSQLQuery("DELETE Ordering_DriverFile where OrderingDriverId = '" + orderingDriver.Id.ToString() + "'").ConfigureAwait(false)).ExecuteUpdate();
                        orderingDriver.DriverFiles.Clear();
                    }
                    (await _orderingDriverRepository.CreateSQLQuery("DELETE Ordering_Driver where OrderingId = '" + data.Id.ToString() + "'").ConfigureAwait(false)).ExecuteUpdate();
                    (await _orderingInsuranceRepository.CreateSQLQuery("DELETE Ordering_Insurance where OrderingId = '" + data.Id.ToString() + "'").ConfigureAwait(false)).ExecuteUpdate();

                    foreach (OrderingProduct orderingProduct in data.Products)
                    {
                        foreach (OrderingProductFile orderingProductFile in orderingProduct.ProductFiles)
                        {
                            if (File.Exists(Path.Combine(contentPath, orderingProductFile.FilePath)))
                            {
                                File.Delete(Path.Combine(contentPath, orderingProductFile.FilePath));
                            }
                        }
                        (await _orderingProductFileRepository.CreateSQLQuery("DELETE Ordering_ProductFile where OrderingProductId = '" + orderingProduct.Id.ToString() + "'").ConfigureAwait(false)).ExecuteUpdate();
                        orderingProduct.ProductFiles.Clear();
                    }
                    (await _orderingProductRepository.CreateSQLQuery("DELETE Ordering_Product where OrderingId = '" + data.Id.ToString() + "'").ConfigureAwait(false)).ExecuteUpdate();
                    (await _orderingPaymentRepository.CreateSQLQuery("DELETE Ordering_Payment where OrderingId = '" + data.Id.ToString() + "'").ConfigureAwait(false)).ExecuteUpdate();

                    data.Addresses.Clear();
                    data.Cars.Clear();
                    data.Containers.Clear();
                    data.Drivers.Clear();
                    data.Insurances.Clear();
                    data.Products.Clear();
                    data.Payments.Clear();
                    (await _orderingRepository.CreateSQLQuery("DELETE Ordering where Id = '" + data.Id.ToString() + "'").ConfigureAwait(false)).ExecuteUpdate();
                    //await _orderingRepository.DeleteAsync(data);
                    return new Response<Guid>(data.Id);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
    }
}
