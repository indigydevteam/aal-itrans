﻿using AutoMapper;
using iTrans.Transportation.Application.DTOs.CarList;
using iTrans.Transportation.Application.DTOs.CarType;
using iTrans.Transportation.Application.DTOs.Ordering;
using iTrans.Transportation.Application.DTOs.OrderingAddress;
using iTrans.Transportation.Application.DTOs.OrderingAddressProduct;
using iTrans.Transportation.Application.DTOs.OrderingProduct;
using iTrans.Transportation.Application.DTOs.OrderingStatus;
using iTrans.Transportation.Application.DTOs.ProductPackaging;
using iTrans.Transportation.Application.DTOs.ProductType;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.Orderings.Commands
{
    public class DriverCancelReserveCommand : IRequest<Response<Guid>>
    {
        public virtual Guid? UserId { get; set; }
        public virtual Guid OrderingId { get; set; }
        public virtual Guid DriverId { get; set; }

        public string APP_ID { get; set; }
        public string REST_API_KEY { get; set; }
        public string AUTH_ID { get; set; }

    }
    public class DriverCancelReserveCommandHandler : IRequestHandler<DriverCancelReserveCommand, Response<Guid>>
    {
        private readonly IOrderingRepositoryAsync _orderingRepository;
        private readonly IOrderingDriverReserveRepositoryAsync _orderingDriverReserveRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IProductTypeRepositoryAsync _productTypeRepository;
        private readonly IProductPackagingRepositoryAsync _productPackagingRepository;
        private readonly INotificationRepositoryAsync _notificationRepository;
        private readonly INotificationUserRepositoryAsync _notificationUserRepository;
        private readonly IDriverActivityRepositoryAsync _driverActivityRepository;
        private readonly IDriverRepositoryAsync _driverRepository;
        private readonly ICustomerInboxRepositoryAsync _customerInboxRepository;
        private readonly IMapper _mapper;

        public DriverCancelReserveCommandHandler(IDriverRepositoryAsync driverRepository, IOrderingRepositoryAsync orderingRepository, IApplicationLogRepositoryAsync applicationLogRepository
            , IProductTypeRepositoryAsync productTypeRepository, IProductPackagingRepositoryAsync productPackagingRepository
            , IOrderingDriverReserveRepositoryAsync orderingDriverReserveRepository, IDriverActivityRepositoryAsync driverActivityRepository
            , INotificationRepositoryAsync notificationRepository, INotificationUserRepositoryAsync notificationUserRepository, ICustomerInboxRepositoryAsync customerInboxRepository, IMapper mapper)
        {
            _orderingRepository = orderingRepository;
            _applicationLogRepository = applicationLogRepository;
            _productTypeRepository = productTypeRepository;
            _productPackagingRepository = productPackagingRepository;
            _orderingDriverReserveRepository = orderingDriverReserveRepository;
            _notificationRepository = notificationRepository;
            _notificationUserRepository = notificationUserRepository;
            _driverActivityRepository = driverActivityRepository;
            _customerInboxRepository = customerInboxRepository;
            _driverRepository = driverRepository;
            _mapper = mapper;
        }

        public async Task<Response<Guid>> Handle(DriverCancelReserveCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var ordering = (await _orderingRepository.FindByCondition(x => x.Id == request.OrderingId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                var driverObject = (await _driverRepository.FindByCondition(x => x.Id.Equals(request.DriverId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (ordering == null || driverObject == null)
                {
                    throw new ApiException($"Ordering Not Found.");
                }
                else
                {
                    //var productTypes = (await _productTypeRepository.FindByCondition(x => x.Active.Equals(true)).ConfigureAwait(false)).AsQueryable().ToList();
                    //var packagings = (await _productPackagingRepository.FindByCondition(x => x.Active.Equals(true)).ConfigureAwait(false)).AsQueryable().ToList();
                    
                    (await _orderingDriverReserveRepository.CreateSQLQuery("DELETE Ordering_DriverReserve where OrderingId = '" + request.OrderingId.ToString() + "' and DriverId = '" + request.DriverId.ToString() + "'").ConfigureAwait(false)).ExecuteUpdate();


                    driverObject.RejectPerMonth = driverObject.RejectPerMonth + 1;
                    driverObject.RejectPerYear = driverObject.RejectPerYear + 1;
                    await _driverRepository.UpdateAsync(driverObject);

                    var driverActivity = (await _driverActivityRepository.FindByCondition(x => x.DriverId.Equals(request.DriverId) && x.Date == DateTime.Now.ToString("yyyy'-'MM'-'dd")).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                    if (driverActivity == null)
                    {
                        DriverActivity newDriverActivity = new DriverActivity
                        {
                            DriverId = request.DriverId,
                            Date = DateTime.Now.ToString("yyyy'-'MM'-'dd"),
                            Star = 0,
                            AcceptJob = 0,
                            Complaint = 0,
                            Reject = 1,
                            Cancel = 0,
                            InsuranceValue = 0,
                            Created = DateTime.Now,
                            CreatedBy = request.UserId != null ? request.UserId.Value.ToString() : "-",
                            Modified = DateTime.Now,
                            ModifiedBy = request.UserId != null ? request.UserId.Value.ToString() : "-",
                        };
                        await _driverActivityRepository.AddAsync(newDriverActivity);
                    }
                    else
                    {
                        driverActivity.Reject = driverActivity.Reject + 1;
                        await _driverActivityRepository.UpdateAsync(driverActivity);
                    }

                    if (ordering.Status > 0 && ordering.Status < 3)
                    {
                        //ordering.Status = 1;
                        //ordering.Modified = DateTime.Now;
                        //await _orderingRepository.UpdateAsync(ordering);

                        #region send noti
                        var productTypes = (await _productTypeRepository.FindByCondition(x => x.Active.Equals(true)).ConfigureAwait(false)).AsQueryable().ToList();
                        var packagings = (await _productPackagingRepository.FindByCondition(x => x.Active.Equals(true)).ConfigureAwait(false)).AsQueryable().ToList();

                        var folderDetails = Path.Combine(Directory.GetCurrentDirectory(), $"Shared\\customer_orderingStatus.json");
                        var statusJson = System.IO.File.ReadAllText(folderDetails);
                        List<OrderingStatusViewModel> orderingStatus = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OrderingStatusViewModel>>(statusJson);

                        var APP_ID = request.APP_ID;
                        var REST_API_KEY = request.REST_API_KEY;
                        var AUTH_ID = request.AUTH_ID;
                        if (ordering.Customer != null)
                        {
                            var userId = ordering.Customer.Id;
                            var ownerId = request.DriverId;

                            var cloneOrdering = new OrderingNotificationViewModel
                            {
                                id = ordering.Id,
                                status = ordering.Status,
                                trackingCode = ordering.TrackingCode,
                                orderNumber = ordering.TrackingCode,
                                products = _mapper.Map<List<OrderingProductNotificationViewModel>>(ordering.Products)
                            };
                            if (ordering.Cars.Count > 0)
                            {
                                cloneOrdering.carType = _mapper.Map<CarTypeViewModel>(ordering.Cars[0].CarType);
                                cloneOrdering.carList = _mapper.Map<CarListViewModel>(ordering.Cars[0].CarList);
                            }
                            if (cloneOrdering.status != null)
                            {
                                cloneOrdering.statusObj = orderingStatus.Where(o => o.id == cloneOrdering.status).FirstOrDefault();
                            }
                            foreach (OrderingProductNotificationViewModel productViewModel in cloneOrdering.products)
                            {
                                if (productViewModel.productType != null)
                                {
                                    string[] productTypeIds = productViewModel.productType.Split(",");
                                    productViewModel.productTypes = new List<ProductTypeViewModel>();
                                    foreach (string productId in productTypeIds)
                                    {
                                        int id = 0;
                                        if (int.TryParse(productId, out id))
                                        {
                                            var productType = productTypes.Where(p => p.Id == id).FirstOrDefault();
                                            if (productType != null)
                                                productViewModel.productTypes.Add(_mapper.Map<ProductTypeViewModel>(productType));
                                        }
                                    }
                                }
                                if (productViewModel.packaging != null)
                                {
                                    string[] packagingIds = productViewModel.packaging.Split(",");
                                    productViewModel.packagings = new List<ProductPackagingViewModel>();
                                    foreach (string packagingId in packagingIds)
                                    {
                                        int id = 0;
                                        if (int.TryParse(packagingId, out id))
                                        {
                                            var packaging = packagings.Where(p => p.Id == id).FirstOrDefault();
                                            if (packaging != null)
                                                productViewModel.packagings.Add(_mapper.Map<ProductPackagingViewModel>(packaging));
                                        }
                                    }
                                }

                            }
                            if (ordering.Addresses.Count > 0)
                            {
                                cloneOrdering.startPoint = _mapper.Map<OrderingAddressNotificationViewModel>(ordering.Addresses[0]);
                                cloneOrdering.endPoint = _mapper.Map<OrderingAddressNotificationViewModel>(ordering.Addresses[ordering.Addresses.Count - 1]);
                            }

                            var oneSignalIds = (await _notificationUserRepository.FindByCondition(x => x.UserId.Equals(userId)).ConfigureAwait(false)).AsQueryable().Select(x => x.OneSignalId).ToList();
                            var notificationDetails = Path.Combine(Directory.GetCurrentDirectory(), $"Shared\\NotificationWord.json");
                            var notificationJson = System.IO.File.ReadAllText(notificationDetails);
                            List<NotificationWordStatusViewModel> notificationStatus = Newtonsoft.Json.JsonConvert.DeserializeObject<List<NotificationWordStatusViewModel>>(notificationJson);

                            string title = "";
                            string detail = "";
                            int notiId = 6;
                            var notificationObj = notificationStatus.Where(o => o.id == notiId).FirstOrDefault();
                            if (notificationObj != null)
                            {
                                title = notificationObj.title.th;
                                if (ordering.Customer != null)
                                {
                                    detail = notificationObj.detail.th.Replace("#customertitle#", ordering.Customer.Title != null ? ordering.Customer.Title.Name_TH : "")
                                             .Replace("#customername#", ordering.Customer.Name)
                                             .Replace("#orderprice#", ordering.OrderingDesiredPrice != null || ordering.OrderingDesiredPrice != 0 ? ordering.OrderingDesiredPrice.ToString() : "-");
                                }
                            }
                            Domain.Notification notification = new Domain.Notification();

                            notification.OneSignalId = oneSignalIds.FirstOrDefault();
                            notification.UserId = userId;
                            notification.OwnerId = ownerId;
                            notification.Title = title;
                            notification.Detail = detail;
                            notification.ServiceCode = "Ordering";
                            //notification.ReferenceContentKey = request.ReferenceContentKey;
                            notification.Payload = Newtonsoft.Json.JsonConvert.SerializeObject(cloneOrdering);
                            notification.Created = DateTime.UtcNow;
                            notification.Modified = DateTime.UtcNow;
                            notification.CreatedBy = ownerId.ToString();
                            notification.ModifiedBy = ownerId.ToString();
                            await _notificationRepository.AddAsync(notification);
                            //#region insert to Inbox
                            //CustomerInbox customerInbox = new CustomerInbox()
                            //{
                            //    CustomerId = ordering.Customer.Id,
                            //    Title = "-",
                            //    Title_ENG = "-",
                            //    Module = "การแจ้งเตือน",
                            //    Module_ENG = "Notification",
                            //    Content = Newtonsoft.Json.JsonConvert.SerializeObject(cloneOrdering),
                            //    FromUser = (driverObject != null) ? driverObject.FirstName + driverObject.LastName : "",
                            //    IsDelete = false,
                            //    Created = DateTime.Now
                            //};
                            //await _customerInboxRepository.AddAsync(customerInbox);
                            //#endregion
                            oneSignalIds = oneSignalIds.Where(x => x.Trim() != "").ToList();
                            if (oneSignalIds.Count > 0)
                            {
                                NotificationManager notiMgr = new NotificationManager(request.APP_ID, request.REST_API_KEY, request.AUTH_ID);
                                notiMgr.SendNotificationAsync(title, detail, oneSignalIds, notification.ServiceCode, "", "{id:'" + cloneOrdering.id + "'}");
                            }

                            var log = new CreateAppLog(_applicationLogRepository);
                            log.CreateLog("Ordering", "OrderingReserveDriver", "Create", Newtonsoft.Json.JsonConvert.SerializeObject(request), request.UserId.ToString());
                            #endregion
                        }
                    }
                    return new Response<Guid>(ordering.Id);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
