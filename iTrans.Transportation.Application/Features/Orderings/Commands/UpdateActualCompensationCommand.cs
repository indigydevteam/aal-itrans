﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;


namespace iTrans.Transportation.Application.Features.Orderings.Commands
{
    public class UpdateActualCompensationCommand : IRequest<Response<int>>
    {
        public virtual Guid? UserId{ get; set; }
        public virtual int OrderingAddressId { get; set; }
        public virtual Guid OrderingId { get; set; }
        public virtual decimal ActualReceive { get; set; }
    }

    public class UpdateActualCompensationCommandHandler : IRequestHandler<UpdateActualCompensationCommand, Response<int>>
    {
        private readonly IOrderingAddressRepositoryAsync _orderingAddressRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;

        public UpdateActualCompensationCommandHandler(IOrderingAddressRepositoryAsync orderingAddressRepository,IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _orderingAddressRepository = orderingAddressRepository;
            _applicationLogRepository = applicationLogRepository; 
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(UpdateActualCompensationCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var OrderingAddress = (await _orderingAddressRepository.FindByCondition(x => x.Id == request.OrderingAddressId && x.Ordering.Id.Equals(request.OrderingId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (OrderingAddress == null)
                {
                    throw new ApiException($"OrderingAddress Not Found.");
                }
                else
                {
                    OrderingAddress.ActualReceive = request.ActualReceive;
                    OrderingAddress.Modified = DateTime.UtcNow;
                    OrderingAddress.ModifiedBy = request.UserId != null ? request.UserId.Value.ToString() : "";
                    await _orderingAddressRepository.UpdateAsync(OrderingAddress);
                    var log = new CreateAppLog(_applicationLogRepository);
                    log.CreateLog("Ordering", "OrderingAdrress ","Edit", Newtonsoft.Json.JsonConvert.SerializeObject(request),request.UserId.ToString());
                    return new Response<int>(OrderingAddress.Id);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}