﻿using AutoMapper;
using iTrans.Transportation.Application.DTOs.Driver;
using iTrans.Transportation.Application.DTOs.DriverCar;
using iTrans.Transportation.Application.DTOs.OrderingCar;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using iTrans.Transportation.Application.DTOs.OrderingStatus;
using iTrans.Transportation.Application.DTOs.Ordering;
using iTrans.Transportation.Application.DTOs.OrderingProduct;
using iTrans.Transportation.Application.DTOs.ProductType;
using iTrans.Transportation.Application.DTOs.ProductPackaging;
using iTrans.Transportation.Application.DTOs.OrderingAddress;
using iTrans.Transportation.Application.DTOs.OrderingAddressProduct;

namespace iTrans.Transportation.Application.Features.Orderings.Commands
{
    public class DriverAcceptCommand : IRequest<Response<Guid>>
    {
        public Guid DriverId { get; set; }
        public Guid OrderingId { get; set; }
        public int DriverAnnouncementId { get; set; }

        public string APP_ID { get; set; }
        public string REST_API_KEY { get; set; }
        public string AUTH_ID { get; set; }
    }
    public class DriverAcceptCommandHandler : IRequestHandler<DriverAcceptCommand, Response<Guid>>
    {
        private readonly IOrderingRepositoryAsync _orderingRepository;
        private readonly IDriverRepositoryAsync _driverRepository;
        private readonly IOrderingCarRepositoryAsync _orderingCarRepository;
        private readonly IOrderingCarFileRepositoryAsync _orderingCarFileRepository;
        private readonly IOrderingDriverRepositoryAsync _orderingDriverRepository;
        private readonly IOrderingDriverFileRepositoryAsync _orderingDriverFileRepository;
        private readonly IDriverAnnouncementRepositoryAsync _driverAnnouncementRepository;
        private readonly IOrderingTrackingCodeRepositoryAsync _orderingTrackingCodeRepository;
        private readonly INotificationRepositoryAsync _notificationRepository;
        private readonly INotificationUserRepositoryAsync _notificationUserRepository;
        private readonly IProductTypeRepositoryAsync _productTypeRepository;
        private readonly IProductPackagingRepositoryAsync _productPackagingRepository;
        private readonly IOrderingHistoryRepositoryAsync _orderingHistoryRepository;
        private readonly IMapper _mapper;
        private readonly IConfiguration _configuration;
        private readonly ICustomerInboxRepositoryAsync _customerInboxRepository;

        public DriverAcceptCommandHandler(IOrderingRepositoryAsync orderingRepository, IOrderingCarFileRepositoryAsync orderingCarFileRepository, IDriverRepositoryAsync driverRepository, IOrderingCarRepositoryAsync orderingCarRepository,
            IOrderingDriverRepositoryAsync orderingDriverRepository, IOrderingDriverFileRepositoryAsync orderingDriverFileRepository, IDriverAnnouncementRepositoryAsync driverAnnouncementRepository, IOrderingTrackingCodeRepositoryAsync orderingTrackingCodeRepository
            , IProductTypeRepositoryAsync productTypeRepository, IProductPackagingRepositoryAsync productPackagingRepository
            , INotificationRepositoryAsync notificationRepository, INotificationUserRepositoryAsync notificationUserRepository, IOrderingHistoryRepositoryAsync orderingHistoryRepository, IMapper mapper, IConfiguration configuration
            , ICustomerInboxRepositoryAsync customerInboxRepository )//, IOrderingCarDriverRepositoryAsync orderingCarDriverRepository,)
        {
            _orderingRepository = orderingRepository;
            _driverRepository = driverRepository;
            _orderingCarRepository = orderingCarRepository;
            _orderingCarFileRepository = orderingCarFileRepository;
            _orderingDriverRepository = orderingDriverRepository;
            _orderingDriverFileRepository = orderingDriverFileRepository;
            _driverAnnouncementRepository = driverAnnouncementRepository;
            _orderingTrackingCodeRepository = orderingTrackingCodeRepository;
            _notificationRepository = notificationRepository;
            _notificationUserRepository = notificationUserRepository;
            _productTypeRepository = productTypeRepository;
            _productPackagingRepository = productPackagingRepository;
            _orderingHistoryRepository = orderingHistoryRepository;
            _mapper = mapper;
            _configuration = configuration;
            _customerInboxRepository = customerInboxRepository;
        }

        public async Task<Response<Guid>> Handle(DriverAcceptCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var data = (await _orderingRepository.FindByCondition(x => x.Id == request.OrderingId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (data == null)
                {
                    throw new ApiException($"Ordering Not Found.");
                }
                if (data.Status != 1)
                {
                    throw new ApiException($"Ordering Not allow accept.");
                }
                else
                {
                    OrderingHistory orderingHistory = new OrderingHistory();

                    var getContentPath = _configuration.GetSection("ContentPath");
                    string contentPath = getContentPath.Value;
                    string currentTimeStr = DateTime.Now.ToString("yyyyMMddHHmmss");

                    var driverObject = (await _driverRepository.FindByCondition(x => x.Id.Equals(request.DriverId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                    if (driverObject == null)
                    {
                        throw new ApiException($"Driver Not Found.");
                    }
                    var productTypes = (await _productTypeRepository.FindByCondition(x => x.Active.Equals(true)).ConfigureAwait(false)).AsQueryable().ToList();
                    var packagings = (await _productPackagingRepository.FindByCondition(x => x.Active.Equals(true)).ConfigureAwait(false)).AsQueryable().ToList();

                    var folderDetails = Path.Combine(Directory.GetCurrentDirectory(), $"Shared\\OrderingStatus.json");
                    var statusJson = System.IO.File.ReadAllText(folderDetails);
                    List<OrderingStatusViewModel> orderingStatus = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OrderingStatusViewModel>>(statusJson);

                    data.Driver = driverObject;
                    var driverAnnouncement = (await _driverAnnouncementRepository.FindByCondition(x => x.Id.Equals(request.DriverAnnouncementId) && x.Driver.Id.Equals(request.DriverId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                    if (driverAnnouncement == null)
                    {
                        //throw new ApiException($"DriverAnnouncement Not Found.");
                    }
                    else
                    {
                        driverAnnouncement.Status = 2; //"gotjob";
                        driverAnnouncement.Order = data;
                        await _driverAnnouncementRepository.UpdateAsync(driverAnnouncement);

                    }
                    IList<OrderingDriver> orderingDrivers = new List<OrderingDriver>();
                    if (data.Drivers.Count() > 0)
                    {
                        foreach (OrderingDriver driver in data.Drivers)
                        {

                            foreach (OrderingDriverFile orderingDriverFile in driver.DriverFiles)
                            {
                                if (File.Exists(Path.Combine(contentPath, orderingDriverFile.FilePath)))
                                {
                                    File.Delete(Path.Combine(contentPath, orderingDriverFile.FilePath));
                                }
                                (await _orderingDriverFileRepository.CreateSQLQuery("DELETE Ordering_DriverFile where Id = '" + orderingDriverFile.Id.ToString() + "'").ConfigureAwait(false)).ExecuteUpdate();
                                //await _orderingDriverFileRepository.DeleteAsync(orderingDriverFile);
                            }
                            driver.DriverFiles.Clear();
                            (await _orderingDriverRepository.CreateSQLQuery("DELETE Ordering_Driver where Id = '" + driver.Id.ToString() + "'").ConfigureAwait(false)).ExecuteUpdate();
                            //await _orderingDriverRepository.DeleteAsync(driver);
                        }
                        data.Drivers.Clear();
                    }

                    OrderingDriver orderingDriver = new OrderingDriver
                    {
                        Ordering = data,
                        DriverId = driverObject.Id,
                        DriverType = driverObject.DriverType,
                        Title = driverObject.Title,
                        FirstName = driverObject.FirstName,
                        MiddleName = driverObject.MiddleName,
                        LastName = driverObject.LastName,
                        Name = driverObject.Name,
                        IdentityNumber = driverObject.IdentityNumber,
                        PhoneCode = driverObject.PhoneCode,
                        PhoneNumber = driverObject.PhoneNumber,
                        Email = driverObject.Email,
                    };
                    string folderPath = contentPath + "ordering/" + request.DriverId.ToString() + "/" + currentTimeStr + "/orderingdriver/";
                    if (!Directory.Exists(folderPath))
                    {
                        Directory.CreateDirectory(folderPath);
                    }
                    int fileCount = 0;
                    if (driverObject.Files != null)
                    {
                        List<OrderingDriverFile> orderingDriverFiles = new List<OrderingDriverFile>();
                        foreach (DriverFile driverFile in driverObject.Files)
                        {
                            if (driverFile.DocumentType == "DriverPicture" || driverFile.DocumentType == "profilepicture")
                            {
                                fileCount = fileCount + 1;

                                string desPath = Path.Combine(folderPath, driverFile.FileName);
                                if (File.Exists(Path.Combine(contentPath, driverFile.FilePath)))
                                {
                                    File.Copy(Path.Combine(contentPath, driverFile.FilePath), desPath);
                                    OrderingDriverFile orderingDriverFile = new OrderingDriverFile
                                    {
                                        OrderingDriver = orderingDriver,
                                        FileName = driverFile.FileName,
                                        ContentType = driverFile.ContentType,
                                        FilePath = "ordering/" + request.DriverId.ToString() + "/" + currentTimeStr + "/orderingdriver/" + driverFile.FileName,
                                        DirectoryPath = "ordering/" + request.DriverId.ToString() + "/" + currentTimeStr + "/orderingdriver/",
                                        FileEXT = driverFile.FileEXT

                                    };
                                    orderingDriverFiles.Add(orderingDriverFile);
                                }
                            }
                        }
                        orderingDriver.DriverFiles = orderingDriverFiles;
                    }
                    orderingDrivers.Add(orderingDriver);
                    data.Drivers = orderingDrivers;
                    orderingHistory.Drivers = orderingDrivers;


                    var orderingcar = (await _orderingCarRepository.FindByCondition(x => x.Ordering.Id.Equals(request.OrderingId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                    var driverCars = _mapper.Map<IList<DriverCar>>(driverObject.Cars);
                    IList<OrderingCar> orderingCars = new List<OrderingCar>();// _mapper.Map<IList<OrderingCar>>(data.Cars);
                    if (data.Cars.Count() > 0)
                    {
                        foreach (OrderingCar orderingCar in data.Cars)
                        {
                            foreach (OrderingCarFile orderingCarFile in orderingCar.CarFiles)
                            {
                                if (File.Exists(Path.Combine(contentPath, orderingCarFile.FilePath)))
                                {
                                    File.Delete(Path.Combine(contentPath, orderingCarFile.FilePath));
                                }
                                (await _orderingCarFileRepository.CreateSQLQuery("DELETE Ordering_CarFile where Id = '" + orderingCarFile.Id.ToString() + "'").ConfigureAwait(false)).ExecuteUpdate();
                                //await _orderingCarFileRepository.DeleteAsync(orderingCarFile);
                            }
                            (await _orderingCarRepository.CreateSQLQuery("DELETE Ordering_Car where Id = '" + orderingCar.Id.ToString() + "'").ConfigureAwait(false)).ExecuteUpdate();
                            //await _orderingCarRepository.DeleteAsync(orderingCar);
                        }
                    }

                    int index = 0;
                    foreach (var driverCar in driverCars)
                    {
                        index = index + 1;
                        OrderingCar car = new OrderingCar();
                        car.Ordering = data;
                        car.CarType = driverCar.CarType;
                        car.ActExpiry = driverCar.ActExpiry;
                        car.CarRegistration = driverCar.CarRegistration;
                        car.CarDescriptionDetail = driverCar.CarDescriptionDetail;
                        car.CarType = driverCar.CarType;
                        car.CarList = driverCar.CarList;
                        car.CarDescription = driverCar.CarDescription;
                        car.CarDescriptionDetail = driverCar.CarDescriptionDetail;
                        car.CarFeature = driverCar.CarFeature;
                        car.CarFeatureDetail = driverCar.CarFeatureDetail;
                        car.CarSpecification = driverCar.CarSpecification;
                        car.CarSpecificationDetail = driverCar.CarSpecificationDetail;
                        car.Width = driverCar.Width;
                        car.Length = driverCar.Length;
                        car.Height = driverCar.Height;
                        car.DriverName = driverCar.DriverName;
                        car.DriverPhoneCode = driverCar.DriverPhoneCode;
                        car.DriverPhoneNumber = driverCar.DriverPhoneNumber;
                        car.ProductInsurance = driverCar.ProductInsurance;
                        car.ProductInsuranceAmount = driverCar.ProductInsuranceAmount;
                        car.Note = driverCar.Note;

                        folderPath = contentPath + "ordering/" + data.Customer.Id.ToString() + "/" + currentTimeStr + "/orderingcar/" + index + "/";
                        if (!Directory.Exists(folderPath))
                        {
                            Directory.CreateDirectory(folderPath);
                        }
                        fileCount = 0;
                        if (driverCar.CarFiles != null)
                        {
                            List<OrderingCarFile> orderingCarFiles = new List<OrderingCarFile>();
                            foreach (DriverCarFile driverCarFile in driverCar.CarFiles)
                            {
                                fileCount = fileCount + 1;

                                string desPath = Path.Combine(folderPath, driverCarFile.FileName);
                                if (File.Exists(Path.Combine(contentPath, driverCarFile.FilePath)))
                                {
                                    File.Copy(Path.Combine(contentPath, driverCarFile.FilePath), desPath);
                                    OrderingCarFile orderingCarFile = new OrderingCarFile
                                    {
                                        OrderingCar = car,
                                        FileName = driverCarFile.FileName,
                                        ContentType = driverCarFile.ContentType,
                                        FilePath = "ordering/" + data.Customer.Id.ToString() + "/" + currentTimeStr + "/orderingcar/" + index + "/" + driverCarFile.FileName,
                                        DirectoryPath = "ordering/" + data.Customer.Id.ToString() + "/" + currentTimeStr + "/orderingcar/" + index + "/",
                                        FileEXT = driverCarFile.FileEXT

                                    };
                                    orderingCarFiles.Add(orderingCarFile);
                                }
                            }
                            car.CarFiles = orderingCarFiles;
                        }
                        orderingCars.Add(car);
                    };
                    data.Cars = orderingCars;
                    orderingHistory.Cars = orderingCars;

                    data.Status = 2;

                    
                    await _orderingRepository.UpdateAsync(data);



                    orderingHistory.TrackingCode = data.TrackingCode;
                    orderingHistory.OrderingId = data.Id;
                    orderingHistory.Driver = data.Driver;
                    orderingHistory.Customer = data.Customer;
                    orderingHistory.ProductCBM = data.ProductCBM;
                    orderingHistory.ProductTotalWeight = data.ProductTotalWeight;
                    orderingHistory.OrderingPrice = data.OrderingPrice;
                    orderingHistory.OrderingStatus = data.Status;
                    orderingHistory.AdditionalDetail = data.AdditionalDetail;
                    orderingHistory.Note = data.Note;
                    orderingHistory.OrderNumber = data.TrackingCode;
                    orderingHistory.OrderingDesiredPrice = data.OrderingDesiredPrice;
                    orderingHistory.OrderingDriverOffering = data.OrderingDriverOffering;
                    orderingHistory.IsOrderingDriverOffer = data.IsOrderingDriverOffer;
                    orderingHistory.CancelStatus = data.CancelStatus;
                    orderingHistory.CustomerRanking = data.CustomerRanking;
                    orderingHistory.DriverRanking = data.DriverRanking;
                    orderingHistory.PinnedDate = data.PinnedDate;
                    orderingHistory.Distance = data.Distance;
                    orderingHistory.EstimateTime = data.EstimateTime;
                    orderingHistory.IsDriverPay = data.IsDriverPay;
                    orderingHistory.CurrentLocation = data.CurrentLocation;
                    orderingHistory.GPValue = data.GPValue;
                    orderingHistory.DriverPayValue = data.DriverPayValue;
                    orderingHistory.CustomerCancelValue = data.CustomerCancelValue;
                    orderingHistory.DriverCancelValue = data.DriverCancelValue;
                    orderingHistory.CustomerCashBack = data.CustomerCashBack;
                    orderingHistory.DriverCashBack = data.DriverCashBack;
                    orderingHistory.DriverAnnouncements = data.DriverAnnouncements;
                    orderingHistory.Created = DateTime.UtcNow;
                    orderingHistory.Modified = DateTime.UtcNow;


                    await _orderingHistoryRepository.AddAsync(orderingHistory);


                    #region
                    var cloneOrdering = _mapper.Map<OrderingViewModel>(data);

                    if (cloneOrdering.status != null && cloneOrdering.status != 0)
                    {
                        cloneOrdering.statusObj = orderingStatus.Where(o => o.id == cloneOrdering.status).FirstOrDefault();
                    }
                    foreach (OrderingProductViewModel productViewModel in cloneOrdering.products)
                    {
                        if (productViewModel.productType != null)
                        {
                            string[] productTypeIds = productViewModel.productType.Split(",");
                            productViewModel.productTypes = new List<ProductTypeViewModel>();
                            foreach (string productId in productTypeIds)
                            {
                                int id = 0;
                                if (int.TryParse(productId, out id))
                                {
                                    var productType = productTypes.Where(p => p.Id == id).FirstOrDefault();
                                    if (productType != null)
                                        productViewModel.productTypes.Add(_mapper.Map<ProductTypeViewModel>(productType));
                                }
                            }
                        }
                        if (productViewModel.packaging != null)
                        {
                            string[] packagingIds = productViewModel.packaging.Split(",");
                            productViewModel.packagings = new List<ProductPackagingViewModel>();
                            foreach (string packagingId in packagingIds)
                            {
                                int id = 0;
                                if (int.TryParse(packagingId, out id))
                                {
                                    var packaging = packagings.Where(p => p.Id == id).FirstOrDefault();
                                    if (packaging != null)
                                        productViewModel.packagings.Add(_mapper.Map<ProductPackagingViewModel>(packaging));
                                }
                            }
                        }
                    }

                    foreach (OrderingAddressViewModel orderingAddressViewModel in cloneOrdering.addresses)
                    {
                        foreach (OrderingAddressProductViewModel orderingAddressProductViewModel in orderingAddressViewModel.products)
                        {
                            if (orderingAddressProductViewModel.productType != null)
                            {
                                string[] productTypeIds = orderingAddressProductViewModel.productType.Split(",");
                                orderingAddressProductViewModel.productTypes = new List<ProductTypeViewModel>();
                                foreach (string productId in productTypeIds)
                                {
                                    int id = 0;
                                    if (int.TryParse(productId, out id))
                                    {
                                        var productType = productTypes.Where(p => p.Id == id).FirstOrDefault();
                                        if (productType != null)
                                            orderingAddressProductViewModel.productTypes.Add(_mapper.Map<ProductTypeViewModel>(productType));
                                    }
                                }
                            }
                            if (orderingAddressProductViewModel.packaging != null)
                            {
                                string[] packagingIds = orderingAddressProductViewModel.packaging.Split(",");
                                orderingAddressProductViewModel.packagings = new List<ProductPackagingViewModel>();
                                foreach (string packagingId in packagingIds)
                                {
                                    int id = 0;
                                    if (int.TryParse(packagingId, out id))
                                    {
                                        var packaging = packagings.Where(p => p.Id == id).FirstOrDefault();
                                        if (packaging != null)
                                            orderingAddressProductViewModel.packagings.Add(_mapper.Map<ProductPackagingViewModel>(packaging));
                                    }
                                }
                            }
                        }
                    }

                    #endregion

                    #region send noti
                    var oneSignalIds = (await _notificationUserRepository.FindByCondition(x => x.UserId.Equals(data.Customer.Id)).ConfigureAwait(false)).AsQueryable().Select(x => x.OneSignalId).ToList();
                    if (oneSignalIds.Count > 0)
                    {
                        Domain.Notification notification = new Domain.Notification();

                        notification.OneSignalId = oneSignalIds.FirstOrDefault();
                        notification.UserId = data.Customer.Id;
                        notification.OwnerId = request.DriverId;
                        notification.Title = "Driver accept job";
                        notification.Detail = "The driver confirmed the receipt of the job.";
                        notification.ServiceCode = "Ordering";
                        //notification.ReferenceContentKey = request.ReferenceContentKey;
                        notification.Payload = Newtonsoft.Json.JsonConvert.SerializeObject(cloneOrdering);
                        notification.Created = DateTime.UtcNow;
                        notification.Modified = DateTime.UtcNow;
                        notification.CreatedBy = request.DriverId.ToString();
                        notification.ModifiedBy = request.DriverId.ToString();
                        await _notificationRepository.AddAsync(notification);

                        NotificationManager notiMgr = new NotificationManager(request.APP_ID, request.REST_API_KEY, request.AUTH_ID);
                        notiMgr.SendNotificationAsync("Driver accept job", "The driver confirmed the receipt of the job.", oneSignalIds, notification.ServiceCode, "", Newtonsoft.Json.JsonConvert.SerializeObject(cloneOrdering));
                    }
                    #endregion
                }


                return new Response<Guid>(data.Id);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
