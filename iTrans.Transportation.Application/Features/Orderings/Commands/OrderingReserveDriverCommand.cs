﻿using AutoMapper;
using iTrans.Transportation.Application.DTOs.CarList;
using iTrans.Transportation.Application.DTOs.CarType;
using iTrans.Transportation.Application.DTOs.Ordering;
using iTrans.Transportation.Application.DTOs.OrderingAddress;
using iTrans.Transportation.Application.DTOs.OrderingAddressProduct;
using iTrans.Transportation.Application.DTOs.OrderingProduct;
using iTrans.Transportation.Application.DTOs.OrderingStatus;
using iTrans.Transportation.Application.DTOs.ProductPackaging;
using iTrans.Transportation.Application.DTOs.ProductType;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
namespace iTrans.Transportation.Application.Features.Orderings.Commands
{
    public class OrderingReserveDriverCommand : IRequest<Response<Guid>>
    {
        public virtual Guid? UserId { get; set; }
        public virtual Guid OrderingId { get; set; }
        public virtual Guid CustomerId { get; set; }
        public virtual Guid DriverId { get; set; }
        public virtual int DriverAnnouncementId { get; set; }
        //public string APP_ID { get; set; }
        //public string REST_API_KEY { get; set; }
        //public string AUTH_ID { get; set; }

        public string DRIVER_APP_ID { get; set; }
        public string DRIVER_REST_API_KEY { get; set; }
        public string DRIVER_AUTH_ID { get; set; }

    }
    public class OrderingReserveDriverCommandHandler : IRequestHandler<OrderingReserveDriverCommand, Response<Guid>>
    {
        private readonly IOrderingRepositoryAsync _orderingRepository;
        private readonly IDriverRepositoryAsync _driverRepository;
        private readonly IDriverAnnouncementRepositoryAsync _driverAnnouncementRepository;
        private readonly IOrderingDriverReserveRepositoryAsync _orderingDriverReserveRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IProductTypeRepositoryAsync _productTypeRepository;
        private readonly IProductPackagingRepositoryAsync _productPackagingRepository;
        private readonly INotificationRepositoryAsync _notificationRepository;
        private readonly INotificationUserRepositoryAsync _notificationUserRepository;
        private readonly IDriverInboxRepositoryAsync _driverInboxRepository;
        private readonly IMapper _mapper;

        public OrderingReserveDriverCommandHandler(IDriverRepositoryAsync driverRepository,IOrderingRepositoryAsync orderingRepository, IApplicationLogRepositoryAsync applicationLogRepository
            , IProductTypeRepositoryAsync productTypeRepository, IProductPackagingRepositoryAsync productPackagingRepository, IOrderingDriverReserveRepositoryAsync orderingDriverReserveRepository
            , INotificationRepositoryAsync notificationRepository, INotificationUserRepositoryAsync notificationUserRepository, IDriverAnnouncementRepositoryAsync driverAnnouncementRepository
            , IDriverInboxRepositoryAsync driverInboxRepository, IMapper mapper)
        {
            _orderingRepository = orderingRepository;
            _driverRepository = driverRepository;
            _driverAnnouncementRepository = driverAnnouncementRepository;
            _applicationLogRepository = applicationLogRepository;
            _productTypeRepository = productTypeRepository;
            _productPackagingRepository = productPackagingRepository;
            _orderingDriverReserveRepository = orderingDriverReserveRepository;
            _notificationRepository = notificationRepository;
            _notificationUserRepository = notificationUserRepository;
            _driverInboxRepository = driverInboxRepository;
            _mapper = mapper;
        }

        public async Task<Response<Guid>> Handle(OrderingReserveDriverCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var ordering = (await _orderingRepository.FindByCondition(x => x.Id == request.OrderingId && x.Customer.Id == request.CustomerId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (ordering == null)
                {
                    throw new ApiException($"Ordering Not Found.");
                }
                else
                {
                    var driver = (await _driverRepository.FindByCondition(x => x.Id == request.DriverId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                    if (ordering == null)
                    {
                        throw new ApiException($"Driver Not Found.");
                    }
                    var productTypes = (await _productTypeRepository.FindByCondition(x => x.Active.Equals(true)).ConfigureAwait(false)).AsQueryable().ToList();
                    var packagings = (await _productPackagingRepository.FindByCondition(x => x.Active.Equals(true)).ConfigureAwait(false)).AsQueryable().ToList();

                    var driverAnnouncement = (await _driverAnnouncementRepository.FindByCondition(x => x.Id == request.DriverAnnouncementId && x.Driver.Id == request.DriverId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                    
                    OrderingDriverReserve driverReserve = new OrderingDriverReserve();

                    driverReserve.Ordering = ordering;
                    driverReserve.Driver = driver;
                    driverReserve.DriverAnnouncement = driverAnnouncement;
                    driverReserve.Status = 1;
                    driverReserve.Created = DateTime.Now;
                    driverReserve.Modified = DateTime.Now;
                    driverReserve.CreatedBy = request.CustomerId != null ? request.CustomerId.ToString() : "";
                    driverReserve.ModifiedBy = request.CustomerId != null ? request.CustomerId.ToString() : "";
                    if (ordering.DriverReserve == null)
                    {
                        ordering.DriverReserve = new List<OrderingDriverReserve>();
                    }
                    ordering.DriverReserve.Add(driverReserve);

                    await _orderingDriverReserveRepository.AddAsync(driverReserve);
                    
                    #region
                    var folderDetails = Path.Combine(Directory.GetCurrentDirectory(), $"Shared\\driver_orderingStatus.json");
                    var statusJson = System.IO.File.ReadAllText(folderDetails);
                    List<OrderingStatusViewModel> orderingStatus = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OrderingStatusViewModel>>(statusJson);

                    //var cloneOrdering = _mapper.Map<OrderingViewModel>(ordering);
                    var cloneOrdering = new OrderingNotificationViewModel
                    {
                        id = ordering.Id,
                        status = ordering.Status,
                        trackingCode = ordering.TrackingCode,
                        orderNumber = ordering.TrackingCode,
                        products = _mapper.Map<List<OrderingProductNotificationViewModel>>(ordering.Products)
                    };
                    if (ordering.Cars.Count > 0)
                    {
                        cloneOrdering.carType = _mapper.Map<CarTypeViewModel>(ordering.Cars[0].CarType);
                        cloneOrdering.carList = _mapper.Map<CarListViewModel>(ordering.Cars[0].CarList);
                    }
                    if (cloneOrdering.status != null )
                    {
                        cloneOrdering.statusObj = orderingStatus.Where(o => o.id == cloneOrdering.status).FirstOrDefault();
                    }
                    foreach (OrderingProductNotificationViewModel productViewModel in cloneOrdering.products)
                    {
                        if (productViewModel.productType != null)
                        {
                            string[] productTypeIds = productViewModel.productType.Split(",");
                            productViewModel.productTypes = new List<ProductTypeViewModel>();
                            foreach (string productId in productTypeIds)
                            {
                                int id = 0;
                                if (int.TryParse(productId, out id))
                                {
                                    var productType = productTypes.Where(p => p.Id == id).FirstOrDefault();
                                    if (productType != null)
                                        productViewModel.productTypes.Add(_mapper.Map<ProductTypeViewModel>(productType));
                                }
                            }
                        }
                        if (productViewModel.packaging != null)
                        {
                            string[] packagingIds = productViewModel.packaging.Split(",");
                            productViewModel.packagings = new List<ProductPackagingViewModel>();
                            foreach (string packagingId in packagingIds)
                            {
                                int id = 0;
                                if (int.TryParse(packagingId, out id))
                                {
                                    var packaging = packagings.Where(p => p.Id == id).FirstOrDefault();
                                    if (packaging != null)
                                        productViewModel.packagings.Add(_mapper.Map<ProductPackagingViewModel>(packaging));
                                }
                            }
                        }

                    }

                    if (ordering.Addresses.Count > 0)
                    {
                        cloneOrdering.startPoint = _mapper.Map<OrderingAddressNotificationViewModel>(ordering.Addresses[0]);
                        cloneOrdering.endPoint = _mapper.Map<OrderingAddressNotificationViewModel>(ordering.Addresses[ordering.Addresses.Count - 1]);
                    }

                    #endregion
                    #region send noti customer
                    var oneSignalIds = (await _notificationUserRepository.FindByCondition(x => x.UserId.Equals(request.DriverId)).ConfigureAwait(false)).AsQueryable().Select(x => x.OneSignalId).ToList();
                    var notificationDetails = Path.Combine(Directory.GetCurrentDirectory(), $"Shared\\NotificationWord.json");
                    var notificationJson = System.IO.File.ReadAllText(notificationDetails);
                    List<NotificationWordStatusViewModel> notificationStatus = Newtonsoft.Json.JsonConvert.DeserializeObject<List<NotificationWordStatusViewModel>>(notificationJson);
                    string title = "";
                    string detail = "";
                    var notificationObj = notificationStatus.Where(o => o.id == 1).FirstOrDefault();
                    if (notificationObj != null)
                    {
                        title = notificationObj.title.th;
                        if (ordering.Customer != null)
                        {
                            detail = notificationObj.detail.th.Replace("#customertitle#", ordering.Customer.Title != null ? ordering.Customer.Title.Name_TH : "")
                                     .Replace("#customername#", ordering.Customer.Name)
                                     .Replace("#orderprice#", ordering.OrderingDesiredPrice != null || ordering.OrderingDesiredPrice != 0 ? ordering.OrderingDesiredPrice.ToString() : "-");
                        }
                    }
                    //if (cloneOrdering.products.Count > 2)
                    //{
                    //    cloneOrdering.products.RemoveRange(2, cloneOrdering.products.Count - 2);
                    //}

                    Domain.Notification notification = new Domain.Notification();

                    notification.OneSignalId = oneSignalIds.FirstOrDefault();
                    notification.UserId = request.DriverId;
                    notification.OwnerId = request.CustomerId;
                    notification.Title = title;
                    notification.Detail = detail;
                    notification.ServiceCode = "Ordering";
                    //notification.ReferenceContentKey = request.ReferenceContentKey;
                    notification.Payload = Newtonsoft.Json.JsonConvert.SerializeObject(cloneOrdering);
                    notification.Created = DateTime.UtcNow;
                    notification.Modified = DateTime.UtcNow;
                    notification.CreatedBy = request.CustomerId.ToString();
                    notification.ModifiedBy = request.CustomerId.ToString();
                    await _notificationRepository.AddAsync(notification);
                    #region insert to Inbox
                    DriverInbox driverInbox = new DriverInbox()
                    {
                        DriverId = request.DriverId,
                        Title = "-",
                        Title_ENG = "-",
                        Module = "การแจ้งเตือน",
                        Module_ENG = "Notification",
                        Content = Newtonsoft.Json.JsonConvert.SerializeObject(cloneOrdering),
                        FromUser = ordering.Customer.FirstName + ordering.Customer.LastName,
                        IsDelete = false,
                        Created = DateTime.Now
                    };
                    await _driverInboxRepository.AddAsync(driverInbox);
                    #endregion
                    oneSignalIds = oneSignalIds.Where(x => x.Trim() != "").ToList();
                    if (oneSignalIds.Count > 0)
                    {
                        NotificationManager notiMgr = new NotificationManager(request.DRIVER_APP_ID, request.DRIVER_REST_API_KEY, request.DRIVER_AUTH_ID);
                        notiMgr.SendNotificationAsync(title, detail, oneSignalIds, notification.ServiceCode, "", "{id:'" + cloneOrdering.id + "'}");
                    }
                    #endregion
                    var log = new CreateAppLog(_applicationLogRepository);
                    log.CreateLog("Ordering", "OrderingReserveDriver", "Create", Newtonsoft.Json.JsonConvert.SerializeObject(request),request.UserId.ToString());
                    return new Response<Guid>(ordering.Id);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
