﻿ using AutoMapper;
using iTrans.Transportation.Application.DTOs.Ordering;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.Ordering;
using iTrans.Transportation.Application.DTOs.OrderingAddress;
using iTrans.Transportation.Application.DTOs.OrderingAddressProduct;
using iTrans.Transportation.Application.DTOs.OrderingProduct;
using iTrans.Transportation.Application.DTOs.OrderingStatus;
using iTrans.Transportation.Application.DTOs.ProductPackaging;
using iTrans.Transportation.Application.DTOs.ProductType;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.Orderings.Commands
{
    public class CustomerComfirmPriceCommand : IRequest<Response<Guid>>
    {
        public Guid? UserId { get; set; }
        public int DriverReserveId { get; set; }
        public Guid OrderingId { get; set; }
        public Guid CustomerId { get; set; }
        public Guid DriverId { get; set; }
        public string APP_ID { get; set; }
        public string REST_API_KEY { get; set; }
        public string AUTH_ID { get; set; }
    }
    public class CustomerComfirmPriceCommandHandler : IRequestHandler<CustomerComfirmPriceCommand, Response<Guid>>
    {
        private readonly IOrderingRepositoryAsync _orderingRepository;
        private readonly IDriverRepositoryAsync _driverRepository;
        private readonly IOrderingDriverReserveRepositoryAsync _orderingDriverReserveRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IProductTypeRepositoryAsync _productTypeRepository;
        private readonly IProductPackagingRepositoryAsync _productPackagingRepository;
        private readonly INotificationRepositoryAsync _notificationRepository;
        private readonly INotificationUserRepositoryAsync _notificationUserRepository;
        private readonly IMapper _mapper;

        public CustomerComfirmPriceCommandHandler(IDriverRepositoryAsync driverRepository, IOrderingRepositoryAsync orderingRepository, IApplicationLogRepositoryAsync applicationLogRepository
            , IProductTypeRepositoryAsync productTypeRepository, IProductPackagingRepositoryAsync productPackagingRepository, IOrderingDriverReserveRepositoryAsync orderingDriverReserveRepository
            , INotificationRepositoryAsync notificationRepository, INotificationUserRepositoryAsync notificationUserRepository, IMapper mapper)
        {
            _orderingRepository = orderingRepository;
            _driverRepository = driverRepository;
            _applicationLogRepository = applicationLogRepository;
            _productTypeRepository = productTypeRepository;
            _productPackagingRepository = productPackagingRepository;
            _orderingDriverReserveRepository = orderingDriverReserveRepository;
            _notificationRepository = notificationRepository;
            _notificationUserRepository = notificationUserRepository;
            _mapper = mapper;
        }

        public async Task<Response<Guid>> Handle(CustomerComfirmPriceCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var ordering = (await _orderingRepository.FindByCondition(x => x.Id == request.OrderingId && x.Customer.Id == request.CustomerId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (ordering == null)
                {
                    throw new ApiException($"Ordering Not Found.");
                }
               
                var orderingDriverReserve = (await _orderingDriverReserveRepository.FindByCondition(x => x.Id == request.DriverReserveId && x.Ordering.Id == request.OrderingId && x.Driver.Id == request.DriverId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (orderingDriverReserve == null)
                {
                    throw new ApiException($"Ordering Driver Reserve Not Found.");
                }

                orderingDriverReserve.Status = 3;
                orderingDriverReserve.Modified = DateTime.Now;
                orderingDriverReserve.ModifiedBy = request.UserId != null ? request.UserId.Value.ToString() : "";
                await _orderingDriverReserveRepository.UpdateAsync(orderingDriverReserve);

                var productTypes = (await _productTypeRepository.FindByCondition(x => x.Active.Equals(true)).ConfigureAwait(false)).AsQueryable().ToList();
                var packagings = (await _productPackagingRepository.FindByCondition(x => x.Active.Equals(true)).ConfigureAwait(false)).AsQueryable().ToList();
                #region
                var folderDetails = Path.Combine(Directory.GetCurrentDirectory(), $"Shared\\OrderingStatus.json");
                var statusJson = System.IO.File.ReadAllText(folderDetails);
                List<OrderingStatusViewModel> orderingStatus = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OrderingStatusViewModel>>(statusJson);

                var cloneOrdering = _mapper.Map<OrderingViewModel>(ordering);

                if (cloneOrdering.status != null && cloneOrdering.status != 0)
                {
                    cloneOrdering.statusObj = orderingStatus.Where(o => o.id == cloneOrdering.status).FirstOrDefault();
                }
                foreach (OrderingProductViewModel productViewModel in cloneOrdering.products)
                {
                    if (productViewModel.productType != null)
                    {
                        string[] productTypeIds = productViewModel.productType.Split(",");
                        productViewModel.productTypes = new List<ProductTypeViewModel>();
                        foreach (string productId in productTypeIds)
                        {
                            int id = 0;
                            if (int.TryParse(productId, out id))
                            {
                                var productType = productTypes.Where(p => p.Id == id).FirstOrDefault();
                                if (productType != null)
                                    productViewModel.productTypes.Add(_mapper.Map<ProductTypeViewModel>(productType));
                            }
                        }
                    }
                    if (productViewModel.packaging != null)
                    {
                        string[] packagingIds = productViewModel.packaging.Split(",");
                        productViewModel.packagings = new List<ProductPackagingViewModel>();
                        foreach (string packagingId in packagingIds)
                        {
                            int id = 0;
                            if (int.TryParse(packagingId, out id))
                            {
                                var packaging = packagings.Where(p => p.Id == id).FirstOrDefault();
                                if (packaging != null)
                                    productViewModel.packagings.Add(_mapper.Map<ProductPackagingViewModel>(packaging));
                            }
                        }
                    }
                }

                foreach (OrderingAddressViewModel orderingAddressViewModel in cloneOrdering.addresses)
                {
                    foreach (OrderingAddressProductViewModel orderingAddressProductViewModel in orderingAddressViewModel.products)
                    {
                        if (orderingAddressProductViewModel.productType != null)
                        {
                            string[] productTypeIds = orderingAddressProductViewModel.productType.Split(",");
                            orderingAddressProductViewModel.productTypes = new List<ProductTypeViewModel>();
                            foreach (string productId in productTypeIds)
                            {
                                int id = 0;
                                if (int.TryParse(productId, out id))
                                {
                                    var productType = productTypes.Where(p => p.Id == id).FirstOrDefault();
                                    if (productType != null)
                                        orderingAddressProductViewModel.productTypes.Add(_mapper.Map<ProductTypeViewModel>(productType));
                                }
                            }
                        }
                        if (orderingAddressProductViewModel.packaging != null)
                        {
                            string[] packagingIds = orderingAddressProductViewModel.packaging.Split(",");
                            orderingAddressProductViewModel.packagings = new List<ProductPackagingViewModel>();
                            foreach (string packagingId in packagingIds)
                            {
                                int id = 0;
                                if (int.TryParse(packagingId, out id))
                                {
                                    var packaging = packagings.Where(p => p.Id == id).FirstOrDefault();
                                    if (packaging != null)
                                        orderingAddressProductViewModel.packagings.Add(_mapper.Map<ProductPackagingViewModel>(packaging));
                                }
                            }
                        }
                    }
                }

                #endregion
                #region send noti
                var oneSignalIds = (await _notificationUserRepository.FindByCondition(x => x.UserId.Equals(request.DriverId)).ConfigureAwait(false)).AsQueryable().Select(x => x.OneSignalId).ToList();
                Domain.Notification notification = new Domain.Notification();

                notification.OneSignalId = oneSignalIds.FirstOrDefault();
                notification.UserId = request.DriverId;
                notification.OwnerId = request.CustomerId;
                notification.Title = "Customer confirm price";
                notification.Detail = "The customer confirm offer price.";
                notification.ServiceCode = "Ordering";
                //notification.ReferenceContentKey = request.ReferenceContentKey;
                notification.Payload = Newtonsoft.Json.JsonConvert.SerializeObject(cloneOrdering);
                notification.Created = DateTime.UtcNow;
                notification.Modified = DateTime.UtcNow;
                notification.CreatedBy = request.CustomerId.ToString();
                notification.ModifiedBy = request.CustomerId.ToString();
                await _notificationRepository.AddAsync(notification);
                oneSignalIds = oneSignalIds.Where(x => x.Trim() != "").ToList();
                if (oneSignalIds.Count > 0)
                {
                    NotificationManager notiMgr = new NotificationManager(request.APP_ID, request.REST_API_KEY, request.AUTH_ID);
                    notiMgr.SendNotificationAsync("Customer confirm price", "The customer confirm offer price.", oneSignalIds, notification.ServiceCode, "", "{id:'" + cloneOrdering.id + "'}");
                }
                #endregion
                var log = new CreateAppLog(_applicationLogRepository);
                log.CreateLog("Ordering", "OrderingReserveDriver", "Create", Newtonsoft.Json.JsonConvert.SerializeObject(request), request.UserId.ToString());
                return new Response<Guid>(ordering.Id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
