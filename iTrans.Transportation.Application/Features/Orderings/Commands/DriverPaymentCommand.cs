﻿using AutoMapper;
using iTrans.Transportation.Application.DTOs.CarList;
using iTrans.Transportation.Application.DTOs.CarType;
using iTrans.Transportation.Application.DTOs.Ordering;
using iTrans.Transportation.Application.DTOs.OrderingAddress;
using iTrans.Transportation.Application.DTOs.OrderingAddressProduct;
using iTrans.Transportation.Application.DTOs.OrderingInsurance;
using iTrans.Transportation.Application.DTOs.OrderingProduct;
using iTrans.Transportation.Application.DTOs.OrderingStatus;
using iTrans.Transportation.Application.DTOs.ProductPackaging;
using iTrans.Transportation.Application.DTOs.ProductType;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.Orderings.Commands
{
    public class DriverPaymentCommand : IRequest<Response<OrderingDriverPaymentViewModel>>
    {
        public Guid? UserId { get; set; }
        public Guid DriverId { get; set; }
        public Guid OrderingId { get; set; }
        public int PaymentType { get; set; }
        public string APP_ID { get; set; }
        public string REST_API_KEY { get; set; }
        public string AUTH_ID { get; set; }

        public string DRIVER_APP_ID { get; set; }
        public string DRIVER_REST_API_KEY { get; set; }
        public string DRIVER_AUTH_ID { get; set; }
    }
    public class DriverPaymentCommandHandler : IRequestHandler<DriverPaymentCommand, Response<OrderingDriverPaymentViewModel>>
    {
        private readonly IOrderingRepositoryAsync _orderingRepository;
        private readonly IOrderingDriverRepositoryAsync _orderingDriverRepository;
        private readonly IOrderingDriverFileRepositoryAsync _orderingDriverFileRepository;
        private readonly IOrderingCarRepositoryAsync _orderingCarRepository;
        private readonly IOrderingCarFileRepositoryAsync _orderingCarFileRepository;

        private readonly IOrderingDriverReserveRepositoryAsync _orderingDriverReserveRepository;
        private readonly INotificationRepositoryAsync _notificationRepository;
        private readonly INotificationUserRepositoryAsync _notificationUserRepository;
        private readonly IOrderingTrackingCodeRepositoryAsync _orderingTrackingCodeRepository;
        private readonly IProductTypeRepositoryAsync _productTypeRepository;
        private readonly IProductPackagingRepositoryAsync _productPackagingRepository;
        private readonly IDriverRepositoryAsync _driverRepository;
        //private readonly IDriverLevelRepositoryAsync _driverLevelRepository;
        private readonly IDriverAnnouncementRepositoryAsync _driverAnnouncementRepository;
        private readonly IMapper _mapper;
        private readonly IConfiguration _configuration;
        private readonly IDriverPaymentRepositoryAsync _driverPaymentRepository;
        private readonly IOrderingHistoryRepositoryAsync _orderingHistoryRepository;
        private readonly ICustomerInboxRepositoryAsync _customerInboxRepository;
        private readonly IDriverInboxRepositoryAsync _driverInboxRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IDriverLevelCharacteristicRepositoryAsync _driverLevelCharacteristic;

        public DriverPaymentCommandHandler(IOrderingRepositoryAsync orderingRepository, IOrderingDriverRepositoryAsync orderingDriverRepository, IOrderingDriverFileRepositoryAsync orderingDriverFileRepository
            , IOrderingCarRepositoryAsync orderingCarRepository, IOrderingCarFileRepositoryAsync orderingCarFileRepository, IOrderingDriverReserveRepositoryAsync orderingDriverReserveRepository
            , INotificationRepositoryAsync notificationRepository, INotificationUserRepositoryAsync notificationUserRepository, IOrderingTrackingCodeRepositoryAsync orderingTrackingCodeRepository
            , IProductTypeRepositoryAsync productTypeRepository, IProductPackagingRepositoryAsync productPackagingRepository, IDriverRepositoryAsync driverRepository, IDriverAnnouncementRepositoryAsync driverAnnouncementRepository
            , IMapper mapper, IConfiguration configuration, IDriverPaymentRepositoryAsync driverPaymentRepository, IOrderingHistoryRepositoryAsync orderingHistoryRepository //, IDriverLevelRepositoryAsync driverLevelRepository
            , ICustomerInboxRepositoryAsync customerInboxRepository, IDriverInboxRepositoryAsync driverInboxRepository, IApplicationLogRepositoryAsync applicationLogRepository
            , IDriverLevelCharacteristicRepositoryAsync driverLevelCharacteristic
            )
        {
            _orderingRepository = orderingRepository;
            _orderingDriverRepository = orderingDriverRepository;
            _orderingDriverFileRepository = orderingDriverFileRepository;
            _orderingCarRepository = orderingCarRepository;
            _orderingCarFileRepository = orderingCarFileRepository;
            _orderingDriverReserveRepository = orderingDriverReserveRepository;
            _notificationRepository = notificationRepository;
            _notificationUserRepository = notificationUserRepository;
            _orderingTrackingCodeRepository = orderingTrackingCodeRepository;
            _productTypeRepository = productTypeRepository;
            _productPackagingRepository = productPackagingRepository;
            _driverRepository = driverRepository;
            _driverAnnouncementRepository = driverAnnouncementRepository;
            _orderingHistoryRepository = orderingHistoryRepository;
            _mapper = mapper;
            _configuration = configuration;
            _driverPaymentRepository = driverPaymentRepository;
            //_driverLevelRepository = driverLevelRepository;
            _customerInboxRepository = customerInboxRepository;
            _driverInboxRepository = driverInboxRepository;
            _applicationLogRepository = applicationLogRepository;
            _driverLevelCharacteristic = driverLevelCharacteristic;
        }

        public async Task<Response<OrderingDriverPaymentViewModel>> Handle(DriverPaymentCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var data = (await _orderingRepository.FindByCondition(x => x.Id.Equals(request.OrderingId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (data == null)
                {
                    throw new ApiException($"Ordering Not Found.");
                }
                var driver = (await _driverRepository.FindByCondition(x => x.Id == request.DriverId && x.IsDelete == false).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (driver == null)
                {
                    throw new ApiException($"Driver Not Found.");
                }
                var productTypes = (await _productTypeRepository.FindByCondition(x => x.Active.Equals(true)).ConfigureAwait(false)).AsQueryable().ToList();
                var packagings = (await _productPackagingRepository.FindByCondition(x => x.Active.Equals(true)).ConfigureAwait(false)).AsQueryable().ToList();

                string orderStatusPath = $"Shared\\orderingstatus.json";
                var orderingStatusPathFile = Path.Combine(Directory.GetCurrentDirectory(), orderStatusPath);

                var statusJson = System.IO.File.ReadAllText(orderingStatusPathFile);
                List<OrderingStatusViewModel> orderingStatus = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OrderingStatusViewModel>>(statusJson);

                OrderingHistory orderingHistory = new OrderingHistory();
                decimal commission = 10;
                var driverLevelCharacteristic = (await _driverLevelCharacteristic.FindByCondition(x => x.Level == driver.Level).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (driverLevelCharacteristic != null)
                {
                    commission = driverLevelCharacteristic.Commission;
                }
                decimal price = 0;
                price = data.OrderingPrice * (commission / 100);
                int paymentType = 1;
                if (request.PaymentType > 0) paymentType = request.PaymentType;
                var driverPayment = (await _driverPaymentRepository.FindByCondition(x => x.Driver.Id == request.DriverId && x.Type.Id == paymentType).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (driverPayment == null || driverPayment.Amount < price)
                {
                    throw new ApiException($"Your wallet don't have enough.");
                }
                driverPayment.Amount = driverPayment.Amount - price;
                driverPayment.ModifiedBy = request.DriverId != null ? request.DriverId.ToString() : "";
                driverPayment.Modified = DateTime.Now;
                await _driverPaymentRepository.UpdateAsync(driverPayment);

                OrderingPayment payment = new OrderingPayment
                {
                    Ordering = data,
                    Module = "driver",
                    UserId = request.DriverId,
                    Channel = paymentType.ToString(),
                    Amount = price,
                    Created = DateTime.UtcNow,
                    Modified = DateTime.UtcNow,
                    CreatedBy = request.DriverId != null ? request.DriverId.ToString() : "",
                    ModifiedBy = request.DriverId != null ? request.DriverId.ToString() : ""
                };
                if (data.Payments == null)
                {
                    data.Payments = new List<OrderingPayment>();
                }
                if (orderingHistory.Payments == null)
                {
                    orderingHistory.Payments = new List<OrderingPayment>();
                }

                data.Payments.Add(payment);

                data.Status = 3;

                var orderingStatusObject = orderingStatus.Where(o => o.id == data.Status).FirstOrDefault();
                data.StatusName_Str = orderingStatusObject != null ? orderingStatusObject.th : "-";

                data.IsDriverPay = true;
                data.DriverPayValue = price;
                data.ModifiedBy = request.DriverId != null ? request.DriverId.ToString() : "";
                data.Modified = DateTime.Now;
                foreach (OrderingAddress address in data.Addresses)
                {
                    address.Status = 3;
                    address.ModifiedBy = request.DriverId != null ? request.DriverId.ToString() : "";
                    address.Modified = DateTime.Now;
                }
                await _orderingRepository.UpdateAsync(data);

                driver.AcceptJobPerYear = driver.AcceptJobPerYear + 1;
                driver.AcceptJobPerMonth = driver.AcceptJobPerMonth + 1;
                await _driverRepository.UpdateAsync(driver);

                //var driverLevelObject = (await _driverLevelRepository.FindByCondition(x => x.DriverId.Equals(request.DriverId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                //if (driverLevelObject == null)
                //{
                //    DriverLevel driverLevel = new DriverLevel
                //    {
                //        DriverId = request.DriverId,
                //        Level = 1,
                //        Star = 0,
                //        AcceptJob = 1,
                //        ComplaintPerMonth = 0,
                //        RejectPerMonth = 0,
                //        CancelPerMonth = 0,
                //        ComplaintPerYear = 0,
                //        RejectPerYear = 0,
                //        CancelPerYear = 0,
                //        InsuranceValue = 0,
                //        Created = DateTime.Now,
                //        Modified = DateTime.Now,
                //        CreatedBy = request.DriverId != null ? request.DriverId.ToString() : "",
                //        ModifiedBy = request.DriverId != null ? request.DriverId.ToString() : "",
                //    };
                //    var driverLevelObj = await _driverLevelRepository.AddAsync(driverLevel);
                //}
                //else
                //{
                //    driverLevelObject.AcceptJob = driverLevelObject.AcceptJob + 1;
                //    driverLevelObject.Modified = DateTime.Now;
                //    driverLevelObject.ModifiedBy = request.DriverId != null ? request.DriverId.ToString() : "";
                //    await _driverLevelRepository.UpdateAsync(driverLevelObject);
                //}

                orderingHistory.TrackingCode = data.TrackingCode;
                orderingHistory.OrderingId = data.Id;
                orderingHistory.Driver = data.Driver;
                orderingHistory.Customer = data.Customer;
                orderingHistory.ProductCBM = data.ProductCBM;
                orderingHistory.ProductTotalWeight = data.ProductTotalWeight;
                orderingHistory.OrderingPrice = data.OrderingPrice;
                orderingHistory.OrderingStatus = data.Status;
                orderingHistory.AdditionalDetail = data.AdditionalDetail;
                orderingHistory.Note = data.Note;
                orderingHistory.OrderNumber = data.TrackingCode;
                orderingHistory.OrderingDesiredPrice = data.OrderingDesiredPrice;
                orderingHistory.OrderingDriverOffering = data.OrderingDriverOffering;
                orderingHistory.IsOrderingDriverOffer = data.IsOrderingDriverOffer;
                orderingHistory.CancelStatus = data.CancelStatus;
                orderingHistory.CustomerRanking = data.CustomerRanking;
                orderingHistory.DriverRanking = data.DriverRanking;
                orderingHistory.PinnedDate = data.PinnedDate;
                orderingHistory.Distance = data.Distance;
                orderingHistory.EstimateTime = data.EstimateTime;
                orderingHistory.IsDriverPay = data.IsDriverPay;
                orderingHistory.CurrentLocation = data.CurrentLocation;
                orderingHistory.GPValue = data.GPValue;
                orderingHistory.DriverPayValue = data.DriverPayValue;
                orderingHistory.IsMutipleRoutes = data.IsMutipleRoutes;
                //orderingHistory.Drivers = data.Drivers;
                //orderingHistory.Cars = data.Cars;
                //orderingHistory.DriverReserve = data.DriverReserve;
                orderingHistory.CustomerCancelValue = data.CustomerCancelValue;
                orderingHistory.DriverCancelValue = data.DriverCancelValue;
                orderingHistory.CustomerCashBack = data.CustomerCashBack;
                orderingHistory.DriverCashBack = data.DriverCashBack;
                orderingHistory.DriverAnnouncements = data.DriverAnnouncements;
                orderingHistory.Created = DateTime.UtcNow;
                orderingHistory.Modified = DateTime.UtcNow;
                orderingHistory.CreatedBy = request.DriverId != null ? request.DriverId.ToString() : "";
                orderingHistory.ModifiedBy = request.DriverId != null ? request.DriverId.ToString() : "";

                await _orderingHistoryRepository.AddAsync(orderingHistory);

                var customerOrderingStatusFile = Path.Combine(Directory.GetCurrentDirectory(), $"Shared\\customer_orderingStatus.json");
                var customerOrderingStatusJson = System.IO.File.ReadAllText(customerOrderingStatusFile);
                List<OrderingStatusViewModel> customerOrderingStatus = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OrderingStatusViewModel>>(customerOrderingStatusJson);

                var driverOrderingStatusFile = Path.Combine(Directory.GetCurrentDirectory(), $"Shared\\driver_orderingStatus.json");
                var driverOrderingStatusJson = System.IO.File.ReadAllText(driverOrderingStatusFile);
                List<OrderingStatusViewModel> driverOrderingStatus = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OrderingStatusViewModel>>(driverOrderingStatusJson);
                var cloneOrdering = new OrderingNotificationViewModel
                {
                    id = data.Id,
                    status = data.Status,
                    trackingCode = data.TrackingCode,
                    orderNumber = data.TrackingCode,
                    products = _mapper.Map<List<OrderingProductNotificationViewModel>>(data.Products)
                };

                if (cloneOrdering.status != null)
                {
                    cloneOrdering.statusObj = driverOrderingStatus.Where(o => o.id == cloneOrdering.status).FirstOrDefault();
                }

                if (data.Cars.Count > 0)
                {
                    cloneOrdering.carType = _mapper.Map<CarTypeViewModel>(data.Cars[0].CarType);
                    cloneOrdering.carList = _mapper.Map<CarListViewModel>(data.Cars[0].CarList);
                }

                foreach (OrderingProductNotificationViewModel productViewModel in cloneOrdering.products)
                {
                    if (productViewModel.productType != null)
                    {
                        string[] productTypeIds = productViewModel.productType.Split(",");
                        productViewModel.productTypes = new List<ProductTypeViewModel>();
                        foreach (string productId in productTypeIds)
                        {
                            int id = 0;
                            if (int.TryParse(productId, out id))
                            {
                                var productType = productTypes.Where(p => p.Id == id).FirstOrDefault();
                                if (productType != null)
                                    productViewModel.productTypes.Add(_mapper.Map<ProductTypeViewModel>(productType));
                            }
                        }
                    }
                    if (productViewModel.packaging != null)
                    {
                        string[] packagingIds = productViewModel.packaging.Split(",");
                        productViewModel.packagings = new List<ProductPackagingViewModel>();
                        foreach (string packagingId in packagingIds)
                        {
                            int id = 0;
                            if (int.TryParse(packagingId, out id))
                            {
                                var packaging = packagings.Where(p => p.Id == id).FirstOrDefault();
                                if (packaging != null)
                                    productViewModel.packagings.Add(_mapper.Map<ProductPackagingViewModel>(packaging));
                            }
                        }
                    }

                }
                if (data.Addresses.Count > 0)
                {
                    cloneOrdering.startPoint = _mapper.Map<OrderingAddressNotificationViewModel>(data.Addresses[0]);
                    cloneOrdering.endPoint = _mapper.Map<OrderingAddressNotificationViewModel>(data.Addresses[data.Addresses.Count - 1]);
                }
                // noti driver
                {
                    if (cloneOrdering.status != null)
                    {
                        cloneOrdering.statusObj = driverOrderingStatus.Where(o => o.id == cloneOrdering.status).FirstOrDefault();
                    }
                    var driverSignalIds = (await _notificationUserRepository.FindByCondition(x => x.UserId.Equals(data.Driver.Id)).ConfigureAwait(false)).AsQueryable().Select(x => x.OneSignalId).ToList();
                    var notificationDetails = Path.Combine(Directory.GetCurrentDirectory(), $"Shared\\NotificationWord.json");
                    var notificationJson = System.IO.File.ReadAllText(notificationDetails);
                    List<NotificationWordStatusViewModel> notificationStatus = Newtonsoft.Json.JsonConvert.DeserializeObject<List<NotificationWordStatusViewModel>>(notificationJson);

                    string title = "";
                    string detail = "";
                    var notificationObj = notificationStatus.Where(o => o.id == 5).FirstOrDefault();
                    if (notificationObj != null)
                    {
                        title = notificationObj.title.th;
                        if (data.Customer != null)
                        {
                            detail = notificationObj.detail.th;
                        }
                    }

                    Domain.Notification notification = new Domain.Notification();

                    notification.OneSignalId = driverSignalIds.FirstOrDefault();
                    notification.UserId = data.Driver.Id;
                    notification.OwnerId = data.Driver.Id;
                    notification.Title = title;
                    notification.Detail = detail;
                    notification.ServiceCode = "Ordering";
                    //notification.ReferenceContentKey = request.ReferenceContentKey;
                    notification.Payload = Newtonsoft.Json.JsonConvert.SerializeObject(cloneOrdering);
                    notification.Created = DateTime.UtcNow;
                    notification.Modified = DateTime.UtcNow;
                    notification.CreatedBy = request.DriverId.ToString();
                    notification.ModifiedBy = request.DriverId.ToString();
                    await _notificationRepository.AddAsync(notification);
                    //#region insert to Inbox
                    //DriverInbox driverInbox = new DriverInbox()
                    //{
                    //    DriverId = data.Driver.Id,
                    //    Title = "-",
                    //    Title_ENG = "-",
                    //    Module = "การแจ้งเตือน",
                    //    Module_ENG = "Notification",
                    //    Content = Newtonsoft.Json.JsonConvert.SerializeObject(cloneOrdering),
                    //    FromUser = data.Driver.FirstName + data.Driver.LastName,
                    //    IsDelete = false,
                    //    Created = DateTime.Now
                    //};
                    //await _driverInboxRepository.AddAsync(driverInbox);
                    //#endregion
                    driverSignalIds = driverSignalIds.Where(x => x.Trim() != "").ToList();
                    if (driverSignalIds.Count > 0)
                    {
                        NotificationManager notiMgr = new NotificationManager(request.DRIVER_APP_ID, request.DRIVER_REST_API_KEY, request.DRIVER_AUTH_ID);
                        notiMgr.SendNotificationAsync(title, detail, driverSignalIds, notification.ServiceCode, "", "{id:'" + cloneOrdering.id + "'}");
                    }
                }
                // noti customer
                {
                    if (cloneOrdering.status != null)
                    {
                        cloneOrdering.statusObj = customerOrderingStatus.Where(o => o.id == cloneOrdering.status).FirstOrDefault();
                    }
                    var customerSignalIds = (await _notificationUserRepository.FindByCondition(x => x.UserId.Equals(data.Customer.Id)).ConfigureAwait(false)).AsQueryable().Select(x => x.OneSignalId).ToList();
                    var notificationDetails = Path.Combine(Directory.GetCurrentDirectory(), $"Shared\\NotificationWord.json");
                    var notificationJson = System.IO.File.ReadAllText(notificationDetails);
                    List<NotificationWordStatusViewModel> notificationStatus = Newtonsoft.Json.JsonConvert.DeserializeObject<List<NotificationWordStatusViewModel>>(notificationJson);

                    string title = "";
                    string detail = "";
                    var notificationObj = notificationStatus.Where(o => o.id == 401).FirstOrDefault();
                    if (notificationObj != null)
                    {
                        title = notificationObj.title.th;
                        if (data.Customer != null)
                        {
                            detail = notificationObj.detail.th;
                        }
                    }
                    if (cloneOrdering.products.Count > 2)
                    {
                        cloneOrdering.products.RemoveRange(2, cloneOrdering.products.Count - 2);
                    }
                    Domain.Notification notification = new Domain.Notification();

                    notification.OneSignalId = customerSignalIds.FirstOrDefault();
                    notification.UserId = data.Customer.Id;
                    notification.OwnerId = data.Driver.Id;
                    notification.Title = title;
                    notification.Detail = detail;
                    notification.ServiceCode = "Ordering";
                    //notification.ReferenceContentKey = request.ReferenceContentKey;
                    notification.Payload = Newtonsoft.Json.JsonConvert.SerializeObject(cloneOrdering);
                    notification.Created = DateTime.UtcNow;
                    notification.Modified = DateTime.UtcNow;
                    notification.CreatedBy = data.Driver.ToString();
                    notification.ModifiedBy = data.Driver.ToString();
                    await _notificationRepository.AddAsync(notification);
                    #region insert to Inbox
                    CustomerInbox customerInbox = new CustomerInbox()
                    {
                        CustomerId = data.Customer.Id,
                        Title = "-",
                        Title_ENG = "-",
                        Module = "การแจ้งเตือน",
                        Module_ENG = "Notification",
                        Content = Newtonsoft.Json.JsonConvert.SerializeObject(cloneOrdering),
                        FromUser = data.Customer.FirstName + data.Customer.LastName,
                        IsDelete = false,
                        Created = DateTime.Now
                    };
                    await _customerInboxRepository.AddAsync(customerInbox);
                    #endregion
                    customerSignalIds = customerSignalIds.Where(x => x.Trim() != "").ToList();
                    if (customerSignalIds.Count > 0)
                    {

                        NotificationManager notiMgr = new NotificationManager(request.APP_ID, request.REST_API_KEY, request.AUTH_ID);
                        notiMgr.SendNotificationAsync(title, detail, customerSignalIds, notification.ServiceCode, "", "{id:'" + cloneOrdering.id + "'}");
                    }
                }
                OrderingDriverPaymentViewModel results = new OrderingDriverPaymentViewModel()
                {
                    orderingId = data.Id,
                    debitAmount = price,
                    balanceAmount = driverPayment.Amount,
                };

                var log = new CreateAppLog(_applicationLogRepository);
                log.CreateLog("Ordering", "Ordering  ", "Edit", Newtonsoft.Json.JsonConvert.SerializeObject(request), request.UserId.ToString());
                return new Response<OrderingDriverPaymentViewModel>(results);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
