﻿using AutoMapper;
using iTrans.Transportation.Application.DTOs.OrderingStatus;
using iTrans.Transportation.Application.Enums;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using iTrans.Transportation.Domain;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.DTOs.Ordering;
using iTrans.Transportation.Application.DTOs.OrderingProduct;
using iTrans.Transportation.Application.DTOs.ProductType;
using iTrans.Transportation.Application.DTOs.ProductPackaging;
using iTrans.Transportation.Application.DTOs.OrderingAddress;
using iTrans.Transportation.Application.DTOs.OrderingAddressProduct;
using iTrans.Transportation.Application.DTOs.CarType;
using iTrans.Transportation.Application.DTOs.CarList;

namespace iTrans.Transportation.Application.Features.Orderings.Commands
{
    public class UploadFileCommand_BK20220602 : IRequest<Response<Guid>>
    {
        public Guid? UserId { get; set; }
        public Guid OrderingId { get; set; }
        public int AddressId { get; set; }
        public Guid CustomerId { get; set; }
        public  Guid DriverId { get; set; }
        public int Status { get; set; }
        public string Note { get; set; }
        public List<IFormFile> files { get; set; }
        public string APP_ID { get; set; }
        public string REST_API_KEY { get; set; }
        public string AUTH_ID { get; set; }
    }
    public class UploadFileCommand_BK20220602Handler : IRequestHandler<UploadFileCommand_BK20220602, Response<Guid>>
    {
        private readonly IOrderingRepositoryAsync _orderingRepository;
        private readonly IOrderingAddressRepositoryAsync _orderingAddressRepository;
        private readonly IOrderingAddressFileRepositoryAsync _orderingAddressFileRepository;
        private readonly IDriverAnnouncementRepositoryAsync _driverAnnouncementRepository;
        private readonly IOrderingCancelStatusRepositoryAsync _orderingCancelStatusRepository;
        private readonly INotificationRepositoryAsync _notificationRepository;
        private readonly INotificationUserRepositoryAsync _notificationUserRepository;
        private readonly IProductTypeRepositoryAsync _productTypeRepository;
        private readonly IProductPackagingRepositoryAsync _productPackagingRepository;
        private readonly IOrderingHistoryRepositoryAsync _orderingHistoryRepository;
        private readonly IMapper _mapper;
        private readonly IConfiguration _configuration;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly ICustomerInboxRepositoryAsync _customerInboxRepository;
        private readonly IDriverInboxRepositoryAsync _driverInboxRepository;

        public UploadFileCommand_BK20220602Handler(IOrderingRepositoryAsync orderingRepository, IOrderingAddressFileRepositoryAsync orderingAddressFileRepository, IOrderingCancelStatusRepositoryAsync orderingCancelStatusRepository
            , INotificationRepositoryAsync notificationRepository, INotificationUserRepositoryAsync notificationUserRepository, IConfiguration configuration
            , IProductTypeRepositoryAsync productTypeRepository, IProductPackagingRepositoryAsync productPackagingRepository, ICustomerInboxRepositoryAsync customerInboxRepository, IDriverInboxRepositoryAsync driverInboxRepository
            , IDriverAnnouncementRepositoryAsync driverAnnouncementRepository, IOrderingAddressRepositoryAsync orderingAddressRepository, IApplicationLogRepositoryAsync applicationLogRepository, IOrderingHistoryRepositoryAsync orderingHistoryRepository, IMapper mapper)
        {
            _orderingRepository = orderingRepository;
            _orderingAddressRepository = orderingAddressRepository;
            _orderingAddressFileRepository = orderingAddressFileRepository;
            _orderingCancelStatusRepository = orderingCancelStatusRepository;
            _driverAnnouncementRepository = driverAnnouncementRepository;
            _notificationRepository = notificationRepository;
            _notificationUserRepository = notificationUserRepository;
            _applicationLogRepository = applicationLogRepository;
            _productTypeRepository = productTypeRepository;
            _productPackagingRepository = productPackagingRepository;
            _orderingHistoryRepository = orderingHistoryRepository;
            _customerInboxRepository = customerInboxRepository;
            _driverInboxRepository = driverInboxRepository;
            _mapper = mapper;
            _configuration = configuration;
        }

        public async Task<Response<Guid>> Handle(UploadFileCommand_BK20220602 request, CancellationToken cancellationToken)
        {
            try
            {
                var getContentPath = _configuration.GetSection("ContentPath");
                string contentPath = getContentPath.Value;
                var data = (await _orderingRepository.FindByCondition(x => x.Id == request.OrderingId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                //var address = (await _orderingAddressRepository.FindByCondition(x => x.Id == request.AddressId && x.Ordering.Id == request.OrderingId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                //var orderingAddress = (await _orderingAddressRepository.FindByCondition(x => x.Id == request.AddressId && x.Ordering.Id.Equals(request.OrderingId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                
                if (data == null  )
                {
                    throw new ApiException($"Ordering or address Not Found.");
                }
                else
                {
                    var orderingAddress = data.Addresses.Where(x => x.Id == request.AddressId).FirstOrDefault();
                    if (orderingAddress == null)
                    {
                        throw new ApiException($"Ordering Not Found.");
                    }
                    OrderingHistory orderingHistory = new OrderingHistory();

                    var productTypes = (await _productTypeRepository.FindByCondition(x => x.Active.Equals(true)).ConfigureAwait(false)).AsQueryable().ToList();
                    var packagings = (await _productPackagingRepository.FindByCondition(x => x.Active.Equals(true)).ConfigureAwait(false)).AsQueryable().ToList();

                    //var folderDetails = Path.Combine(Directory.GetCurrentDirectory(), $"Shared\\OrderingStatus.json");
                    //var statusJson = System.IO.File.ReadAllText(folderDetails);
                    //List<OrderingStatusViewModel> orderingStatus = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OrderingStatusViewModel>>(statusJson);
                    //var orderingStatusObj = orderingStatus.Where(o => o.id == request.Status).FirstOrDefault();

                    data.Status = request.Status;
                    string uploadNote = orderingAddress.UploadNote == null ? "" : orderingAddress.UploadNote.Trim();
                    orderingAddress.UploadNote = uploadNote + request.Note;
                    if (request.Status >= 4 && request.Status <= 12)
                    {
                        data.Status = 0;
                        if (orderingAddress != null)
                        {
                            if (request.Status == 6)
                            {
                                orderingAddress.DriverWaitingToPickUpDate = DateTime.Now;
                            }
                            if (request.Status == 11)
                            {
                                orderingAddress.DriverWaitingToDeliveryDate = DateTime.Now;
                            }
                            if ((orderingAddress.Status == 6 && request.Status == 7) || (orderingAddress.Status == 11 && request.Status == 12))
                            {
                                TimeSpan? periodTime = DateTime.Now - orderingAddress.Modified;
                                var driver = data.Drivers.Where(x => x.DriverId.Equals(request.DriverId)).FirstOrDefault();
                                bool isCharter = true;
                                orderingAddress.Compensation = 0;
                                orderingAddress.PeriodTime = periodTime.ToString();
                                if (driver != null)
                                {
                                    var car = data.Cars.Where(x => x.OrderingDriver.Id.Equals(driver.Id)).FirstOrDefault();
                                    if (car != null)
                                    {
                                        isCharter = car.IsCharter;
                                    }
                                }
                                double totalMinutes = periodTime.Value.TotalMinutes;
                                if (isCharter)
                                {
                                    if (totalMinutes > 60 && totalMinutes <= 300)
                                    {
                                        orderingAddress.Compensation = data.OrderingPrice * 0.05m;
                                    }
                                    else if (totalMinutes > 300)
                                    {
                                        orderingAddress.Compensation = data.OrderingPrice * 0.1m;
                                    }
                                }
                                else
                                {
                                    if (totalMinutes > 15 && totalMinutes <= 30)
                                    {
                                        orderingAddress.Compensation = data.OrderingPrice * 0.05m;
                                    }
                                    else if (totalMinutes > 15 && totalMinutes <= 45)
                                    {
                                        orderingAddress.Compensation = data.OrderingPrice * 0.05m;
                                        orderingAddress.Compensation = orderingAddress.Compensation + (data.OrderingPrice * 0.05m);
                                    }
                                    else if (totalMinutes > 15 && totalMinutes <= 60)
                                    {
                                        orderingAddress.Compensation = data.OrderingPrice * 0.05m;
                                        orderingAddress.Compensation = orderingAddress.Compensation + (data.OrderingPrice * 0.05m);
                                        orderingAddress.Compensation = orderingAddress.Compensation + (data.OrderingPrice * 0.05m);
                                    }
                                    else if (totalMinutes > 60)
                                    {
                                        orderingAddress.Compensation = data.OrderingPrice * 0.05m;
                                        orderingAddress.Compensation = orderingAddress.Compensation + (data.OrderingPrice * 0.05m);
                                        orderingAddress.Compensation = orderingAddress.Compensation + (data.OrderingPrice * 0.05m);
                                        orderingAddress.Compensation = orderingAddress.Compensation + (data.OrderingPrice * 0.1m);
                                    }
                                }
                                //address.Compensation = (decimal)periodTime.Value.TotalMinutes;
                                //address.PeriodTime = periodTime.ToString();
                            }
                            orderingAddress.Status = request.Status;
                            orderingAddress.Modified = DateTime.Now;
                        }
                    }
                    if (request.Status == 13)
                    {
                        data.Status = 0;
                        var lastAddress = data.Addresses.Max(x => x.Sequence);
                        if (orderingAddress != null)
                        {
                            orderingAddress.Status = request.Status;
                            if (orderingAddress.Sequence == lastAddress)
                            {
                                data.Status = 16;
                            }
                        }
                    }

                    if (request.files != null)
                    {
                        string currentTimeStr = DateTime.Now.ToString("yyyyMMddHHmmss");
                        string folderPath = contentPath + "ordering/" + orderingAddress.Ordering.Id.ToString() + "/" + currentTimeStr + "/orderingAddress/" + orderingAddress.Id;
                        if (!Directory.Exists(folderPath))
                        {
                            Directory.CreateDirectory(folderPath);
                        }

                        int fileCount = 0;
                        foreach (IFormFile file in request.files)
                        {
                            fileCount = fileCount + 1;
                            string filePath = Path.Combine(folderPath, file.FileName);
                            using (FileStream fileStream = new FileStream(filePath, FileMode.Create))
                            {
                                await file.CopyToAsync(fileStream);
                                FileInfo fi = new FileInfo(filePath);

                                OrderingAddressFile orderingAddressFile = new OrderingAddressFile
                                {
                                    OrderingAddress = orderingAddress,
                                    FileName = file.FileName,
                                    ContentType = file.ContentType,
                                    FilePath = "ordering/" + orderingAddress.Ordering.Id.ToString() + "/" + currentTimeStr + "/orderingAddress/" + orderingAddress.Id + "/" + file.FileName,
                                    DirectoryPath = "ordering/" + orderingAddress.Ordering.Id.ToString() + "/" + currentTimeStr + "/orderingAddress/" + orderingAddress.Id,
                                    FileEXT = fi.Extension
                                };
                                orderingAddressFile.Created = DateTime.UtcNow;
                                orderingAddressFile.Modified = DateTime.UtcNow;
                                var driverFileObject = await _orderingAddressFileRepository.AddAsync(orderingAddressFile);
                                //var log = new CreateAppLog(_applicationLogRepository);
                                //log.Create("ordering", "orderingadrressfile ", this.ToString(), Newtonsoft.Json.JsonConvert.SerializeObject(request));
                            }
                        }
                    }
                    #region
                    //if (address != null)
                    //{
                    //    if (request.Status == 8 && address.Status == 7)
                    //    {
                    //        if (address != null)
                    //        {
                    //            TimeSpan? periodTime = DateTime.Now - address.Modified;
                    //            address.Compensation = (decimal)periodTime.Value.TotalMinutes;
                    //            address.PeriodTime = periodTime.ToString();
                    //        }

                    //    }

                    //    if (request.Status == 12 && address.Status == 11)
                    //    {
                    //        if (address != null)
                    //        {
                    //            TimeSpan? periodTime = DateTime.Now - address.Modified;
                    //            address.Compensation = (decimal)periodTime.Value.TotalMinutes;
                    //            address.PeriodTime = periodTime.ToString();
                    //        }

                    //    }

                    //    address.Status = request.Status;
                    //    address.Modified = DateTime.Now;
                    //    await _orderingAddressRepository.UpdateAsync(address);
                    //}
                    #endregion

                    await _orderingRepository.UpdateAsync(data);


                    orderingHistory.TrackingCode = data.TrackingCode;
                    orderingHistory.OrderingId = data.Id;
                    orderingHistory.Driver = data.Driver;
                    orderingHistory.Customer = data.Customer;
                    orderingHistory.ProductCBM = data.ProductCBM;
                    orderingHistory.ProductTotalWeight = data.ProductTotalWeight;
                    orderingHistory.OrderingPrice = data.OrderingPrice;
                    orderingHistory.OrderingStatus = data.Status;
                    orderingHistory.AdditionalDetail = data.AdditionalDetail;
                    orderingHistory.Note = data.Note;
                    orderingHistory.OrderNumber = data.TrackingCode;
                    orderingHistory.OrderingDesiredPrice = data.OrderingDesiredPrice;
                    orderingHistory.OrderingDriverOffering = data.OrderingDriverOffering;
                    orderingHistory.IsOrderingDriverOffer = data.IsOrderingDriverOffer;
                    orderingHistory.CancelStatus = data.CancelStatus;
                    orderingHistory.CustomerRanking = data.CustomerRanking;
                    orderingHistory.DriverRanking = data.DriverRanking;
                    orderingHistory.PinnedDate = data.PinnedDate;
                    orderingHistory.Distance = data.Distance;
                    orderingHistory.EstimateTime = data.EstimateTime;
                    orderingHistory.IsDriverPay = data.IsDriverPay;
                    orderingHistory.CurrentLocation = data.CurrentLocation;
                    orderingHistory.GPValue = data.GPValue;
                    orderingHistory.DriverPayValue = data.DriverPayValue;
                    orderingHistory.CustomerCancelValue = data.CustomerCancelValue;
                    orderingHistory.DriverCancelValue = data.DriverCancelValue;
                    orderingHistory.CustomerCashBack = data.CustomerCashBack;
                    orderingHistory.DriverCashBack = data.DriverCashBack;
                    orderingHistory.DriverAnnouncements = data.DriverAnnouncements;
                    orderingHistory.Created = DateTime.UtcNow;
                    orderingHistory.Modified = DateTime.UtcNow;


                    await _orderingHistoryRepository.AddAsync(orderingHistory);





                    #region
                    #region
                    //var cloneOrdering = _mapper.Map<OrderingViewModel>(data);

                    //if (cloneOrdering.status != null && cloneOrdering.status != 0)
                    //{
                    //    cloneOrdering.statusObj = orderingStatus.Where(o => o.id == cloneOrdering.status).FirstOrDefault();
                    //}
                    //foreach (OrderingProductViewModel productViewModel in cloneOrdering.products)
                    //{
                    //    if (productViewModel.productType != null)
                    //    {
                    //        string[] productTypeIds = productViewModel.productType.Split(",");
                    //        productViewModel.productTypes = new List<ProductTypeViewModel>();
                    //        foreach (string productId in productTypeIds)
                    //        {
                    //            int id = 0;
                    //            if (int.TryParse(productId, out id))
                    //            {
                    //                var productType = productTypes.Where(p => p.Id == id).FirstOrDefault();
                    //                if (productType != null)
                    //                    productViewModel.productTypes.Add(_mapper.Map<ProductTypeViewModel>(productType));
                    //            }
                    //        }
                    //    }
                    //    if (productViewModel.packaging != null)
                    //    {
                    //        string[] packagingIds = productViewModel.packaging.Split(",");
                    //        productViewModel.packagings = new List<ProductPackagingViewModel>();
                    //        foreach (string packagingId in packagingIds)
                    //        {
                    //            int id = 0;
                    //            if (int.TryParse(packagingId, out id))
                    //            {
                    //                var packaging = packagings.Where(p => p.Id == id).FirstOrDefault();
                    //                if (packaging != null)
                    //                    productViewModel.packagings.Add(_mapper.Map<ProductPackagingViewModel>(packaging));
                    //            }
                    //        }
                    //    }
                    //}

                    //foreach (OrderingAddressViewModel orderingAddressViewModel in cloneOrdering.addresses)
                    //{
                    //    foreach (OrderingAddressProductViewModel orderingAddressProductViewModel in orderingAddressViewModel.products)
                    //    {
                    //        if (orderingAddressProductViewModel.productType != null)
                    //        {
                    //            string[] productTypeIds = orderingAddressProductViewModel.productType.Split(",");
                    //            orderingAddressProductViewModel.productTypes = new List<ProductTypeViewModel>();
                    //            foreach (string productId in productTypeIds)
                    //            {
                    //                int id = 0;
                    //                if (int.TryParse(productId, out id))
                    //                {
                    //                    var productType = productTypes.Where(p => p.Id == id).FirstOrDefault();
                    //                    if (productType != null)
                    //                        orderingAddressProductViewModel.productTypes.Add(_mapper.Map<ProductTypeViewModel>(productType));
                    //                }
                    //            }
                    //        }
                    //        if (orderingAddressProductViewModel.packaging != null)
                    //        {
                    //            string[] packagingIds = orderingAddressProductViewModel.packaging.Split(",");
                    //            orderingAddressProductViewModel.packagings = new List<ProductPackagingViewModel>();
                    //            foreach (string packagingId in packagingIds)
                    //            {
                    //                int id = 0;
                    //                if (int.TryParse(packagingId, out id))
                    //                {
                    //                    var packaging = packagings.Where(p => p.Id == id).FirstOrDefault();
                    //                    if (packaging != null)
                    //                        orderingAddressProductViewModel.packagings.Add(_mapper.Map<ProductPackagingViewModel>(packaging));
                    //                }
                    //            }
                    //        }
                    //    }
                    //}
                    #endregion
                    #endregion
                    var customerOrderingStatusFile = Path.Combine(Directory.GetCurrentDirectory(), $"Shared\\customer_orderingStatus.json");
                    var customerOrderingStatusJson = System.IO.File.ReadAllText(customerOrderingStatusFile);
                    List<OrderingStatusViewModel> customerOrderingStatus = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OrderingStatusViewModel>>(customerOrderingStatusJson);

                    var driverOrderingStatusFile = Path.Combine(Directory.GetCurrentDirectory(), $"Shared\\driver_orderingStatus.json");
                    var driverOrderingStatusJson = System.IO.File.ReadAllText(customerOrderingStatusFile);
                    List<OrderingStatusViewModel> driverOrderingStatus = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OrderingStatusViewModel>>(driverOrderingStatusJson);

                    var cloneOrdering = new OrderingNotificationViewModel
                    {
                        id = data.Id,
                        status = data.Status == 0 && orderingAddress != null ? orderingAddress.Status : data.Status,
                        trackingCode = data.TrackingCode,
                        orderNumber = data.TrackingCode,
                        products = _mapper.Map<List<OrderingProductNotificationViewModel>>(data.Products)
                    };
                    if (data.Cars.Count > 0)
                    {
                        cloneOrdering.carType = _mapper.Map<CarTypeViewModel>(data.Cars[0].CarType);
                        cloneOrdering.carList = _mapper.Map<CarListViewModel>(data.Cars[0].CarList);
                    }
                    if (cloneOrdering.status != null)
                    {
                        cloneOrdering.statusObj = driverOrderingStatus.Where(o => o.id == cloneOrdering.status).FirstOrDefault();
                        if (request.DriverId != null && request.DriverId != new Guid())
                        {
                            cloneOrdering.statusObj = customerOrderingStatus.Where(o => o.id == cloneOrdering.status).FirstOrDefault();
                        }
                    }
                    foreach (OrderingProductNotificationViewModel productViewModel in cloneOrdering.products)
                    {
                        if (productViewModel.productType != null)
                        {
                            string[] productTypeIds = productViewModel.productType.Split(",");
                            productViewModel.productTypes = new List<ProductTypeViewModel>();
                            foreach (string productId in productTypeIds)
                            {
                                int id = 0;
                                if (int.TryParse(productId, out id))
                                {
                                    var productType = productTypes.Where(p => p.Id == id).FirstOrDefault();
                                    if (productType != null)
                                        productViewModel.productTypes.Add(_mapper.Map<ProductTypeViewModel>(productType));
                                }
                            }
                        }
                        if (productViewModel.packaging != null)
                        {
                            string[] packagingIds = productViewModel.packaging.Split(",");
                            productViewModel.packagings = new List<ProductPackagingViewModel>();
                            foreach (string packagingId in packagingIds)
                            {
                                int id = 0;
                                if (int.TryParse(packagingId, out id))
                                {
                                    var packaging = packagings.Where(p => p.Id == id).FirstOrDefault();
                                    if (packaging != null)
                                        productViewModel.packagings.Add(_mapper.Map<ProductPackagingViewModel>(packaging));
                                }
                            }
                        }

                    }
                    if (data.Addresses.Count > 0)
                    {
                        cloneOrdering.startPoint = _mapper.Map<OrderingAddressNotificationViewModel>(data.Addresses[0]);
                        cloneOrdering.endPoint = _mapper.Map<OrderingAddressNotificationViewModel>(data.Addresses[data.Addresses.Count - 1]);
                    }
                    #region send noti
                    var userId = request.CustomerId;
                    var ownerId = request.CustomerId;
                    if (request.CustomerId == data.Customer.Id)
                    {
                        userId = data.Driver.Id;
                        ownerId = request.CustomerId;
                    }
                    if (request.DriverId == data.Driver.Id)
                    {
                        userId = data.Customer.Id;
                        ownerId = request.DriverId;
                    }
                    if (userId != null)
                    {
                        string trackingCode = "";
                        if (data.TrackingCode != null)
                        {
                            trackingCode = data.TrackingCode;
                        }
                        var oneSignalIds = (await _notificationUserRepository.FindByCondition(x => x.UserId.Equals(userId)).ConfigureAwait(false)).AsQueryable().Select(x => x.OneSignalId).ToList();
                        var notificationDetails = Path.Combine(Directory.GetCurrentDirectory(), $"Shared\\NotificationWord.json");
                        var notificationJson = System.IO.File.ReadAllText(notificationDetails);
                        List<NotificationWordStatusViewModel> notificationStatus = Newtonsoft.Json.JsonConvert.DeserializeObject<List<NotificationWordStatusViewModel>>(notificationJson);

                        string title = "";
                        string detail = "";
                        int notiId = 1;
                        if (request.Status == 15 && request.CustomerId != null && request.CustomerId != new Guid())
                        {
                            notiId = 8;
                        }
                        else if (request.Status == 15 && request.DriverId != null && request.DriverId != new Guid())
                        {
                            notiId = 9;
                        }
                        else if (request.Status == 4)
                        {
                            notiId = 10;
                        }
                        else if (request.Status == 5)
                        {
                            notiId = 11;
                        }
                        else if (request.Status == 6)
                        {
                            notiId = 12;
                        }
                        else if (request.Status == 7)
                        {
                            notiId = 12;
                        }
                        else if (request.Status == 8)
                        {
                            notiId = 13;
                        }
                        else if (request.Status == 9)
                        {
                            notiId = 15;
                        }
                        else if (request.Status == 10)
                        {
                            notiId = 16;
                        }
                        else if (request.Status == 11)
                        {
                            notiId = 17;
                        }
                        else if (request.Status == 12)
                        {
                            notiId = 18;
                        }
                        else if (request.Status == 13)
                        {
                            notiId = 19;
                        }
                        var notificationObj = notificationStatus.Where(o => o.id == notiId).FirstOrDefault();
                        if (notificationObj != null)
                        {
                            title = notificationObj.title.th;
                            if (data.Customer != null)
                            {
                                detail = notificationObj.detail.th.Replace("#customertitle#", data.Customer.Title != null ? data.Customer.Title.Name_TH : "")
                                         .Replace("#customername#", data.Customer.Name)
                                         .Replace("#orderprice#", data.OrderingDesiredPrice != null || data.OrderingDesiredPrice != 0 ? data.OrderingDesiredPrice.ToString() : "-");
                            }
                        }
                        //if (cloneOrdering.products.Count > 2)
                        //{
                        //    cloneOrdering.products.RemoveRange(2, cloneOrdering.products.Count - 2);
                        //}
                        Domain.Notification notification = new Domain.Notification();

                        notification.OneSignalId = oneSignalIds.FirstOrDefault();
                        notification.UserId = userId;
                        notification.OwnerId = ownerId;
                        notification.Title = title;
                        notification.Detail = detail;
                        notification.ServiceCode = "Ordering";
                        //notification.ReferenceContentKey = request.ReferenceContentKey;
                        notification.Payload = Newtonsoft.Json.JsonConvert.SerializeObject(cloneOrdering);
                        notification.Created = DateTime.UtcNow;
                        notification.Modified = DateTime.UtcNow;
                        notification.CreatedBy = request.DriverId.ToString();
                        notification.ModifiedBy = request.DriverId.ToString();
                        await _notificationRepository.AddAsync(notification);
                        if (request.CustomerId != null && request.CustomerId != new Guid())
                        {
                            #region insert to Inbox
                            DriverInbox driverInbox = new DriverInbox()
                            {
                                DriverId = data.Driver.Id,
                                Title = "-",
                                Title_ENG = "-",
                                Module = "การแจ้งเตือน",
                                Module_ENG = "Notification",
                                Content = Newtonsoft.Json.JsonConvert.SerializeObject(cloneOrdering),
                                FromUser = data.Customer.FirstName + data.Customer.LastName,
                                IsDelete = false,
                                Created = DateTime.Now
                            };
                            await _driverInboxRepository.AddAsync(driverInbox);
                            #endregion
                        }
                        if (request.DriverId != null && request.DriverId != new Guid())
                        {
                            #region insert to Inbox
                            CustomerInbox customerInbox = new CustomerInbox()
                            {
                                CustomerId = data.Customer.Id,
                                Title = "-",
                                Title_ENG = "-",
                                Module = "การแจ้งเตือน",
                                Module_ENG = "Notification",
                                Content = Newtonsoft.Json.JsonConvert.SerializeObject(cloneOrdering),
                                FromUser = data.Driver.FirstName + data.Driver.LastName,
                                IsDelete = false,
                                Created = DateTime.Now
                            };
                            await _customerInboxRepository.AddAsync(customerInbox);
                            #endregion
                        }
                        if (oneSignalIds.Count > 0)
                        {

                            NotificationManager notiMgr = new NotificationManager(request.APP_ID, request.REST_API_KEY, request.AUTH_ID);
                            notiMgr.SendNotificationAsync(title, detail, oneSignalIds, notification.ServiceCode, "", "{id:'" + cloneOrdering.id + "'}");
                        }
                    }
                    #endregion


                    var log = new CreateAppLog(_applicationLogRepository);
                    log.CreateLog("Ordering", "DriverAcceptOrdering", "Edit", Newtonsoft.Json.JsonConvert.SerializeObject(request), request.UserId.ToString());

                    return new Response<Guid>(data.Id);

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
