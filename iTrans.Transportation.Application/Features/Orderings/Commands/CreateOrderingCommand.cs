﻿using AutoMapper;
using iTrans.Transportation.Application.DTOs.OrderingAddress;
using iTrans.Transportation.Application.DTOs.OrderingAddressProduct;
using iTrans.Transportation.Application.DTOs.OrderingCar;
using iTrans.Transportation.Application.DTOs.OrderingContainer;
//using iTrans.Transportation.Application.DTOs.OrderingContainerAddress;
using iTrans.Transportation.Application.DTOs.OrderingProduct;
using iTrans.Transportation.Application.Enums;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using iTrans.Transportation.Domain.Entities;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;

namespace iTrans.Transportation.Application.Features.Orderings.Commands
{
    public partial class CreateOrderingCommand : IRequest<Response<Guid>>
    {
        public Guid? UserId { get; set; }
        public Guid DriverId { get; set; }
        public Guid CustomerId { get; set; }
        public float ProductTotalWeight { get; set; }
        public int ProductCBM { get; set; }
        public decimal OrderingPrice { get; set; }
        public decimal OrderingDesiredPrice { get; set; }
        public string OrderingStatus { get; set; }
        public string AdditionalDetail { get; set; }
        public string Note { get; set; }
        public List<CreateCarViweModel> Cars { get; set; }
        public List<CreateProductViewModel> Products { get; set; }
        public List<CreateAddressViewModel> Addresses { get; set; }
        public CreateContainerViewModel Container { get; set; }
        //public CreateContainerAddressViewModel ContainerAddress { get; set; }
    }
    public class CreateOrderingCommandHandler : IRequestHandler<CreateOrderingCommand, Response<Guid>>
    {
        private readonly ICustomerRepositoryAsync _customerRepository;
        private readonly ICustomerProductRepositoryAsync _customerProductRepository;
        private readonly IDriverRepositoryAsync _driverRepository;
        private readonly IOrderingRepositoryAsync _orderingRepository;
        private readonly ICarTypeRepositoryAsync _carTypeRepository;
        private readonly ICarListRepositoryAsync _carListRepository;
        private readonly ICarDescriptionRepositoryAsync _carDescriptionRepository;
        private readonly ICarSpecificationRepositoryAsync _carSpecificationRepository;
        private readonly ICarFeatureRepositoryAsync _carFeatureRepository;
        private readonly IProductTypeRepositoryAsync _productTypeRepository;
        private readonly IProductPackagingRepositoryAsync _productPackagingRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly ICountryRepositoryAsync _countryRepository;
        private readonly IProvinceRepositoryAsync _provinceRepository;
        private readonly IDistrictRepositoryAsync _districtRepository;
        private readonly ISubdistrictRepositoryAsync _subdistrictRepository;
        private readonly IMapper _mapper;
        private readonly IConfiguration _configuration;
        public CreateOrderingCommandHandler(IOrderingRepositoryAsync orderingRepository, IApplicationLogRepositoryAsync applicationLogRepository, ICustomerRepositoryAsync customerRepository, ICustomerProductRepositoryAsync customerProductRepository
            , IDriverRepositoryAsync driverRepository, ICarTypeRepositoryAsync carTypeRepository, ICarListRepositoryAsync carListRepository, ICarDescriptionRepositoryAsync carDescriptionRepository, ICarSpecificationRepositoryAsync carSpecificationRepository
            , IProductTypeRepositoryAsync productTypeRepository, IProductPackagingRepositoryAsync productPackagingRepository, ICarFeatureRepositoryAsync carFeatureRepository
            , ICountryRepositoryAsync countryRepository, IProvinceRepositoryAsync provinceRepository, IDistrictRepositoryAsync districtRepository
            , ISubdistrictRepositoryAsync subdistrictRepository, IMapper mapper, IConfiguration configuration)
        {
            _customerRepository = customerRepository;
            _customerProductRepository = customerProductRepository;
            _driverRepository = driverRepository;
            _orderingRepository = orderingRepository;
            _applicationLogRepository = applicationLogRepository;
            _carTypeRepository = carTypeRepository;
            _carListRepository = carListRepository;
            _carDescriptionRepository = carDescriptionRepository;
            _carSpecificationRepository = carSpecificationRepository;
            _carFeatureRepository = carFeatureRepository;
            _productTypeRepository = productTypeRepository;
            _productPackagingRepository = productPackagingRepository;
            _countryRepository = countryRepository;
            _provinceRepository = provinceRepository;
            _districtRepository = districtRepository;
            _subdistrictRepository = subdistrictRepository;
            _mapper = mapper;
            _configuration = configuration;
        }

        public async Task<Response<Guid>> Handle(CreateOrderingCommand request, CancellationToken cancellationToken)
        {
            try
            {
                string currentTimeStr = DateTime.Now.ToString("yyyyMMddHHmmss");
                var getContentPath = _configuration.GetSection("ContentPath");
                string contentPath = getContentPath.Value;
                var customerObject = (await _customerRepository.FindByCondition(x => x.Id.Equals(request.CustomerId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (customerObject == null)
                {
                    throw new ApiException($"Customer Not Found.");
                }
                var driverObject = (await _driverRepository.FindByCondition(x => x.Id.Equals(request.DriverId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                //if (driverObject == null)
                //{
                //    throw new ApiException($"Driver Not Found.");
                //}
                var carTypes = (await _carTypeRepository.FindByCondition(x => x.Active.Equals(true)).ConfigureAwait(false)).AsQueryable().ToList();
                var carLists = (await _carListRepository.FindByCondition(x => x.Active.Equals(true)).ConfigureAwait(false)).AsQueryable().ToList();
                var carDescriptions = (await _carDescriptionRepository.FindByCondition(x => x.Active.Equals(true)).ConfigureAwait(false)).ToList().ToList();
                var carSpecifications = (await _carSpecificationRepository.FindByCondition(x => x.Active.Equals(true)).ConfigureAwait(false)).AsQueryable().ToList();
                var carFeatures = (await _carFeatureRepository.FindByCondition(x => x.Active.Equals(true)).ConfigureAwait(false)).AsQueryable().ToList();
                var productTypes = (await _productTypeRepository.FindByCondition(x => x.Active.Equals(true)).ConfigureAwait(false)).AsQueryable().ToList();
                var packagings = (await _productPackagingRepository.FindByCondition(x => x.Active.Equals(true)).ConfigureAwait(false)).AsQueryable().ToList();
                var ordering = _mapper.Map<Ordering>(request);
                ordering.Customer = customerObject;
                ordering.Created = DateTime.UtcNow;
                ordering.Modified = DateTime.UtcNow;

                if (driverObject != null)
                {
                    ordering.Driver = driverObject;
                    
                }

                if (request.Cars != null)
                {
                    List<OrderingCar> cars = new List<OrderingCar>();
                    foreach (CreateCarViweModel carView in request.Cars)
                    {
                        var car = _mapper.Map<OrderingCar>(carView);
                        car.Ordering = ordering;
                        car.CarType = carTypes.Where(c => c.Id.Equals(carView.CarTypeId)).FirstOrDefault();
                        car.CarList = carLists.Where(c => c.Id.Equals(carView.CarListId)).FirstOrDefault();
                        car.CarDescription = carDescriptions.Where(c => c.Id.Equals(carView.CarDescriptionId)).FirstOrDefault();
                        car.CarSpecification = carSpecifications.Where(c => c.Id.Equals(carView.CarSpecificationId)).FirstOrDefault();
                        car.CarFeature = carFeatures.Where(c => c.Id.Equals(carView.CarFeatureId)).FirstOrDefault();
                        if (carView.files != null)
                        {
                            string folderPath = contentPath + "ordering/" + request.CustomerId.ToString() + "/" + currentTimeStr + "/car/";
                            if (!Directory.Exists(folderPath))
                            {
                                Directory.CreateDirectory(folderPath);
                            }
                            int fileCount = 0;
                            List<OrderingCarFile> carFiles = new List<OrderingCarFile>();
                            foreach (IFormFile file in carView.files)
                            {
                                fileCount = fileCount + 1;
                                string filePath = Path.Combine(folderPath, file.FileName);
                                using (FileStream fileStream = new FileStream(filePath, FileMode.Create))
                                {
                                    await file.CopyToAsync(fileStream);
                                    FileInfo fi = new FileInfo(filePath);

                                    OrderingCarFile orderingCarFile = new OrderingCarFile
                                    {
                                        OrderingCar = car,
                                        FileName = file.FileName,
                                        ContentType = file.ContentType,
                                        FilePath = "ordering/" + ordering.Customer.Id.ToString() + "/" + currentTimeStr + "/car/" + file.FileName,
                                        DirectoryPath = "ordering/" + ordering.Customer.Id.ToString() + "/" + currentTimeStr + "/car/",
                                        FileEXT = fi.Extension
                                    };
                                    carFiles.Add(orderingCarFile);
                                }
                            }
                            car.CarFiles = carFiles;
                        }
                        cars.Add(car);
                    }

                    ordering.Cars = cars;
                }

                if (request.Products != null)
                {
                    List<OrderingProduct> products = new List<OrderingProduct>();
                    foreach (CreateProductViewModel productView in request.Products)
                    {
                        var product = _mapper.Map<OrderingProduct>(productView);
                        product.Ordering = ordering;
                        //product.ProductType = productTypes.Where(p => p.Id.Equals(productView.ProductTypeId)).FirstOrDefault();
                        //product.Packaging = packagings.Where(p => p.Id.Equals(productView.PackagingId)).FirstOrDefault();
                        string folderPath = contentPath + "ordering/" + request.CustomerId.ToString() + "/" + currentTimeStr + "/product/";
                        if (!Directory.Exists(folderPath))
                        {
                            Directory.CreateDirectory(folderPath);
                        }
                        int fileCount = 0;
                        List<OrderingProductFile> productFiles = new List<OrderingProductFile>();
                        if (productView.Files != null)
                        {
                            
                            foreach (IFormFile file in productView.Files)
                            {
                                fileCount = fileCount + 1;
                                string filePath = Path.Combine(folderPath, file.FileName);
                                using (FileStream fileStream = new FileStream(filePath, FileMode.Create))
                                {
                                    await file.CopyToAsync(fileStream);
                                    FileInfo fi = new FileInfo(filePath);

                                    OrderingProductFile orderingProductFile = new OrderingProductFile
                                    {
                                        OrderingProduct = product,
                                        FileName = file.FileName,
                                        ContentType = file.ContentType,
                                        FilePath = "ordering/" + request.CustomerId.ToString() + "/" + currentTimeStr + "/product/" + file.FileName,
                                        DirectoryPath = "ordering/" + request.CustomerId.ToString() + "/" + currentTimeStr + "/product/",
                                        FileEXT = fi.Extension
                                    };
                                    productFiles.Add(orderingProductFile);
                                }
                            }
                           
                        }
                        if(productView.CustomerProductId != null && productView.CustomerProductId != 0)
                        {
                            var customerProductObject = (await _customerProductRepository.FindByCondition(x => x.Id.Equals(productView.CustomerProductId) && x.CustomerId.Equals(request.CustomerId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                            if (customerProductObject != null)
                            {
                                fileCount = 0;
                               
                                foreach (CustomerProductFile productFile in customerProductObject.ProductFiles)
                                {
                                    if (File.Exists(Path.Combine(contentPath, productFile.FilePath)))
                                    {
                                        string filePath = Path.Combine(folderPath, productFile.FileName);
                                        File.Copy(Path.Combine(contentPath, productFile.FilePath), filePath);
                                        OrderingProductFile orderingProductFile = new OrderingProductFile
                                        {
                                            OrderingProduct = product,
                                            FileName = productFile.FileName,
                                            ContentType = productFile.ContentType,
                                            FilePath = "ordering/" + request.CustomerId.ToString() + "/" + currentTimeStr + "/product/" + productFile.FileName,
                                            DirectoryPath = "ordering/" + request.CustomerId.ToString() + "/" + currentTimeStr + "/product/",
                                            FileEXT = productFile.FileEXT
                                        };
                                        productFiles.Add(orderingProductFile);
                                    }
                                }
                            }
                        }
                        product.ProductFiles = productFiles;
                        products.Add(product);
                    }
                    ordering.Products = products;
                }

                if (request.Addresses != null)
                {
                    List<OrderingAddress> addresses = new List<OrderingAddress>();
                    foreach (CreateAddressViewModel addressView in request.Addresses)
                    {
                        var address = _mapper.Map<OrderingAddress>(addressView);
                        Country country = (await _countryRepository.FindByCondition(x => x.Id.Equals(addressView.CountryId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                        Province province = (await _provinceRepository.FindByCondition(x => x.Id.Equals(addressView.ProvinceId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                        District district = (await _districtRepository.FindByCondition(x => x.Id.Equals(addressView.DistrictId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                        Subdistrict subdistrict = (await _subdistrictRepository.FindByCondition(x => x.Id.Equals(addressView.SubdistrictId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                        List<OrderingAddressProduct> addressProducts = new List<OrderingAddressProduct>();
                        if (addressView.Products != null)
                        {
                            //foreach (CreateAddressProductViewModel viewProduct in addressView.Products)
                            //{
                            //    var addressProduct = _mapper.Map<OrderingAddressProduct>(viewProduct);
                            //    addressProduct.OrderingAddress = address;
                            //    addressProduct.ProductType = productTypes.Where(p => p.Id.Equals(viewProduct.ProductTypeId)).FirstOrDefault();
                            //    addressProduct.Packaging = packagings.Where(p => p.Id.Equals(viewProduct.PackagingId)).FirstOrDefault();
                            //    addressProducts.Add(addressProduct);
                            //    string folderPath = contentPath + "ordering/" + request.CustomerId.ToString() + "/" + currentTimeStr + "/addressproduct/";
                            //    if (!Directory.Exists(folderPath))
                            //    {
                            //        Directory.CreateDirectory(folderPath);
                            //    }
                            //    int fileCount = 0;
                            //    if (viewProduct.Files != null)
                            //    {
                            //        List<OrderingAddressProductFile> AddressProductFiles = new List<OrderingAddressProductFile>();
                            //        foreach (IFormFile file in viewProduct.Files)
                            //        {
                            //            fileCount = fileCount + 1;
                            //            string filePath = Path.Combine(folderPath, file.FileName);
                            //            using (FileStream fileStream = new FileStream(filePath, FileMode.Create))
                            //            {
                            //                await file.CopyToAsync(fileStream);
                            //                FileInfo fi = new FileInfo(filePath);

                            //                OrderingAddressProductFile addressProductFile = new OrderingAddressProductFile
                            //                {
                            //                    OrderingAddressProduct = addressProduct,
                            //                    FileName = file.FileName,
                            //                    ContentType = file.ContentType,
                            //                    FilePath = "ordering/" + request.CustomerId.ToString() + "/" + currentTimeStr + "/addressproduct/" + file.FileName,
                            //                    DirectoryPath = "ordering/" + request.CustomerId.ToString() + "/" + currentTimeStr + "/addressproduct/",
                            //                    FileEXT = fi.Extension
                            //                };
                            //                AddressProductFiles.Add(addressProductFile);
                            //            }
                            //        }
                            //        addressProduct.ProductFiles = AddressProductFiles;
                            //    }
                            //}
                            address.AddressProducts = addressProducts;
                        }
                        address.Ordering = ordering;
                        address.Country = country;
                        address.Province = province;
                        address.District = district;
                        address.Subdistrict = subdistrict;

                        addresses.Add(address);
                    }
                    ordering.Addresses = addresses;
                }

                if (request.Container != null)
                {
                    List<OrderingContainer> containers = new List<OrderingContainer>();
                    var container = _mapper.Map<OrderingContainer>(request.Container);
                    //if (request.Container.Files != null)
                    //{
                    //    List<OrderingContainerFile> containerFiles = new List<OrderingContainerFile>();
                    //    string folderPath = contentPath + "ordering/" + request.CustomerId.ToString() + "/" + currentTimeStr + "/container/";
                    //    if (!Directory.Exists(folderPath))
                    //    {
                    //        Directory.CreateDirectory(folderPath);
                    //    }
                    //    int fileCount = 0;
                    //    foreach (IFormFile file in request.Container.Files)
                    //    {
                    //        fileCount = fileCount + 1;
                    //        string filePath = Path.Combine(folderPath, file.FileName);
                    //        using (FileStream fileStream = new FileStream(filePath, FileMode.Create))
                    //        {
                    //            await file.CopyToAsync(fileStream);
                    //            FileInfo fi = new FileInfo(filePath);

                    //            OrderingContainerFile containerFile = new OrderingContainerFile
                    //            {
                    //                OrderingContainer = container,
                    //                FileName = file.FileName,
                    //                ContentType = file.ContentType,
                    //                FilePath = "ordering/" + request.CustomerId.ToString() + "/" + currentTimeStr + "/product/" + file.FileName,
                    //                DirectoryPath = "ordering/" + request.CustomerId.ToString() + "/" + currentTimeStr + "/product/",
                    //                FileEXT = fi.Extension
                    //            };
                    //            containerFiles.Add(containerFile);
                    //        }
                    //    }
                    //    container.ContainerFiles = containerFiles;
                    //}
                    //if (request.ContainerAddress != null)
                    //{
                    //    List<OrderingContainerAddress> containerAddresses = new List<OrderingContainerAddress>();
                    //    var containerAddress = _mapper.Map<OrderingContainerAddress>(request.ContainerAddress);
                    //    if (containerAddress != null)
                    //    {
                    //        Country loadingCountry = (await _countryRepository.FindByCondition(x => x.Id.Equals(request.ContainerAddress.LoadingCountryId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                    //        Province loadingProvince = (await _provinceRepository.FindByCondition(x => x.Id.Equals(request.ContainerAddress.LoadingProvinceId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                    //        District loadingDistrict = (await _districtRepository.FindByCondition(x => x.Id.Equals(request.ContainerAddress.LoadingDistrictId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                    //        Subdistrict loadingSubdistrict = (await _subdistrictRepository.FindByCondition(x => x.Id.Equals(request.ContainerAddress.LoadingSubdistrictId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                    //        Country destinationCountry = (await _countryRepository.FindByCondition(x => x.Id.Equals(request.ContainerAddress.DestinationCountryId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                    //        Province destinationProvince = (await _provinceRepository.FindByCondition(x => x.Id.Equals(request.ContainerAddress.DestinationProvinceId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                    //        District destinationDistrict = (await _districtRepository.FindByCondition(x => x.Id.Equals(request.ContainerAddress.DestinationDistrictId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                    //        Subdistrict destinationSubdistrict = (await _subdistrictRepository.FindByCondition(x => x.Id.Equals(request.ContainerAddress.DestinationSubdistrictId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();

                    //        containerAddress.OrderingContainer = container;
                    //        containerAddress.LoadingCountry = loadingCountry;
                    //        containerAddress.LoadingProvince = loadingProvince;
                    //        containerAddress.LoadingDistrict = loadingDistrict;
                    //        containerAddress.LoadingSubdistrict = loadingSubdistrict;
                    //        containerAddress.DestinationCountry = destinationCountry;
                    //        containerAddress.DestinationProvince = destinationProvince;
                    //        containerAddress.DestinationDistrict = destinationDistrict;
                    //        containerAddress.DestinationSubdistrict = destinationSubdistrict;
                    //        containerAddresses.Add(containerAddress);
                    //    }

                    //   // container.Addresses = containerAddresses;
                    //}
                    container.Ordering = ordering;
                    containers.Add(container);
                    ordering.Containers = containers;
                }


                var dataListObject = await _orderingRepository.AddAsync(ordering);
                var log = new CreateAppLog(_applicationLogRepository);
                log.CreateLog("Ordering", "Ordering", "Create", Newtonsoft.Json.JsonConvert.SerializeObject(request),request.UserId.ToString());
                return new Response<Guid>(dataListObject.Id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}