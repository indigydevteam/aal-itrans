﻿using AutoMapper;
using iTrans.Transportation.Application.DTOs.CarList;
using iTrans.Transportation.Application.DTOs.CarType;
using iTrans.Transportation.Application.DTOs.Ordering;
using iTrans.Transportation.Application.DTOs.OrderingAddress;
using iTrans.Transportation.Application.DTOs.OrderingAddressProduct;
using iTrans.Transportation.Application.DTOs.OrderingInsurance;
using iTrans.Transportation.Application.DTOs.OrderingProduct;
using iTrans.Transportation.Application.DTOs.OrderingStatus;
using iTrans.Transportation.Application.DTOs.ProductPackaging;
using iTrans.Transportation.Application.DTOs.ProductType;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.Orderings.Commands
{
    public class CustomerPaymentCommand : IRequest<Response<Guid>>
    {
        public Guid? UserId { get; set; }
        public Guid CustomerId { get; set; }
        public Guid DriverId { get; set; }
        public Guid OrderingId { get; set; }
        public int DriverReserveId { get; set; }
        public bool IsRequestTax { get; set; }

        //public int PaymentType { get; set; }
        public int Channel { get; set; }
        public decimal Amount { get; set; }
        //public CreateInsuranceViewModel Insurance { get; set; }
        public int Status { get; set; }
        public string APP_ID { get; set; }
        public string REST_API_KEY { get; set; }
        public string AUTH_ID { get; set; }

        public string DRIVER_APP_ID { get; set; }
        public string DRIVER_REST_API_KEY { get; set; }
        public string DRIVER_AUTH_ID { get; set; }
    }
    public class CustomerPaymentCommandHandler : IRequestHandler<CustomerPaymentCommand, Response<Guid>>
    {
        private readonly IOrderingRepositoryAsync _orderingRepository;
        private readonly ICustomerActivityRepositoryAsync _customerActivityRepository;
        private readonly IOrderingDriverRepositoryAsync _orderingDriverRepository;
        private readonly IOrderingDriverFileRepositoryAsync _orderingDriverFileRepository;
        private readonly IOrderingCarRepositoryAsync _orderingCarRepository;
        private readonly IOrderingCarFileRepositoryAsync _orderingCarFileRepository;
        private readonly IOrderingDriverReserveRepositoryAsync _orderingDriverReserveRepository;
        private readonly INotificationRepositoryAsync _notificationRepository;
        private readonly INotificationUserRepositoryAsync _notificationUserRepository;
        private readonly IOrderingTrackingCodeRepositoryAsync _orderingTrackingCodeRepository;
        private readonly IProductTypeRepositoryAsync _productTypeRepository;
        private readonly IProductPackagingRepositoryAsync _productPackagingRepository;
        private readonly IDriverRepositoryAsync _driverRepository;
        private readonly IDriverAnnouncementRepositoryAsync _driverAnnouncementRepository;
        private readonly IOrderingHistoryRepositoryAsync _orderingHistoryRepository;
        private readonly IMapper _mapper;
        private readonly IConfiguration _configuration;
        private readonly ICustomerPaymentRepositoryAsync _customerPaymentRepository;
        private readonly ICustomerInboxRepositoryAsync _customerInboxRepository;
        private readonly IDriverInboxRepositoryAsync _driverInboxRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;


        public CustomerPaymentCommandHandler(IOrderingRepositoryAsync orderingRepository, IOrderingDriverRepositoryAsync orderingDriverRepository, IOrderingDriverFileRepositoryAsync orderingDriverFileRepository
            , IOrderingCarRepositoryAsync orderingCarRepository, IOrderingCarFileRepositoryAsync orderingCarFileRepository, IOrderingDriverReserveRepositoryAsync orderingDriverReserveRepository
            , INotificationRepositoryAsync notificationRepository, INotificationUserRepositoryAsync notificationUserRepository, IOrderingTrackingCodeRepositoryAsync orderingTrackingCodeRepository
            , IProductTypeRepositoryAsync productTypeRepository, IProductPackagingRepositoryAsync productPackagingRepository, IDriverRepositoryAsync driverRepository, IDriverAnnouncementRepositoryAsync driverAnnouncementRepository
            , IMapper mapper, IConfiguration configuration, ICustomerPaymentRepositoryAsync customerPaymentRepository, IOrderingHistoryRepositoryAsync orderingHistoryRepository
            , ICustomerInboxRepositoryAsync customerInboxRepository, IDriverInboxRepositoryAsync driverInboxRepository, IApplicationLogRepositoryAsync applicationLogRepository
            , ICustomerActivityRepositoryAsync customerActivityRepository
            )
        {
            _orderingRepository = orderingRepository;
            _orderingDriverRepository = orderingDriverRepository;
            _orderingDriverFileRepository = orderingDriverFileRepository;
            _orderingCarRepository = orderingCarRepository;
            _orderingCarFileRepository = orderingCarFileRepository;
            _orderingDriverReserveRepository = orderingDriverReserveRepository;
            _notificationRepository = notificationRepository;
            _notificationUserRepository = notificationUserRepository;
            _orderingTrackingCodeRepository = orderingTrackingCodeRepository;
            _productTypeRepository = productTypeRepository;
            _productPackagingRepository = productPackagingRepository;
            _driverRepository = driverRepository;
            _driverAnnouncementRepository = driverAnnouncementRepository;
            _orderingHistoryRepository = orderingHistoryRepository;
            _mapper = mapper;
            _configuration = configuration;
            _customerPaymentRepository = customerPaymentRepository;
            _customerActivityRepository = customerActivityRepository;
            _customerInboxRepository = customerInboxRepository;
            _driverInboxRepository = driverInboxRepository;
            _applicationLogRepository = applicationLogRepository;
        }

        public async Task<Response<Guid>> Handle(CustomerPaymentCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var data = (await _orderingRepository.FindByCondition(x => x.Id == request.OrderingId && x.Customer.Id == request.CustomerId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (data == null)
                {
                    throw new ApiException($"Ordering Not Found.");
                }
                if (data.Status != 2)
                {
                    throw new ApiException($"Ordering Not allow accept.");
                }
                int channel = 2;
                if (request.Channel > 0) channel = request.Channel;
                var customerPayment = (await _customerPaymentRepository.FindByCondition(x => x.Customer.Id == request.CustomerId && x.Type.Id == channel).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (customerPayment == null || customerPayment.Amount < request.Amount)
                {
                    throw new ApiException($"Your wallet don't have enough.");
                }

                OrderingHistory orderingHistory = new OrderingHistory();


                var getContentPath = _configuration.GetSection("ContentPath");
                string contentPath = getContentPath.Value;
                string currentTimeStr = DateTime.Now.ToString("yyyyMMddHHmmss");
                var driverObject = (await _driverRepository.FindByCondition(x => x.Id.Equals(request.DriverId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (driverObject == null)
                {
                    throw new ApiException($"Driver Not Found.");
                }
                
                if (request.Status != 0)
                {
                    //var driverAnnouncement = (await _driverAnnouncementRepository.FindByCondition(x => x.Id == request.DriverReserveId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                    //if (driverAnnouncement != null)
                    //{
                    //
                    //    driverAnnouncement.Status = request.Status;
                    //    driverAnnouncement.Order = data;
                    //    data.OrderingPrice = driverAnnouncement.DesiredPrice;
                    //    await _driverAnnouncementRepository.UpdateAsync(driverAnnouncement);
                    //
                    //}
                    data.Status = request.Status;
                }

                string orderStatusPath = $"Shared\\orderingstatus.json";
                var orderingStatusPathFile = Path.Combine(Directory.GetCurrentDirectory(), orderStatusPath);

                var statusJson = System.IO.File.ReadAllText(orderingStatusPathFile);
                List<OrderingStatusViewModel> orderingStatus = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OrderingStatusViewModel>>(statusJson);

                var productTypes = (await _productTypeRepository.FindByCondition(x => x.Active.Equals(true)).ConfigureAwait(false)).AsQueryable().ToList();
                var packagings = (await _productPackagingRepository.FindByCondition(x => x.Active.Equals(true)).ConfigureAwait(false)).AsQueryable().ToList();

                if (request.Amount != null && request.Amount != 0)
                {
                    data.OrderingPrice = request.Amount;
                }
                else
                {
                    data.OrderingPrice = data.OrderingDesiredPrice;
                    var orderingDriverReserve = (await _orderingDriverReserveRepository.FindByCondition(x => x.Id == request.DriverReserveId && x.Ordering.Id == request.OrderingId && x.Driver.Id == request.DriverId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                    if (orderingDriverReserve != null && data.IsOrderingDriverOffer == true && orderingDriverReserve.DriverOfferingPrice != 0)
                    {
                        data.OrderingPrice = orderingDriverReserve.DriverOfferingPrice;
                    }

                    if (orderingDriverReserve != null)
                    {
                        var driverAnnouncement = (await _driverAnnouncementRepository.FindByCondition(x => x.Id == request.DriverReserveId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                        if (driverAnnouncement != null)
                        {
                            if(driverAnnouncement.AllRent)
                            {
                                data.TransportType = 1;
                            }
                            else
                            {
                                data.TransportType = 2;
                            }
                            //driverAnnouncement.Status = request.Status;
                            //driverAnnouncement.Order = data;
                            //data.OrderingPrice = driverAnnouncement.DesiredPrice;
                            //await _driverAnnouncementRepository.UpdateAsync(driverAnnouncement);
                        }
                    }
                }
                data.Driver = driverObject;
                data.DriverName_Str = driverObject != null ? driverObject.Name : "-";

                //var orderingDriverReserve = (await _orderingDriverReserveRepository.FindByCondition(x => x.Id == request.DriverReserveId && x.Ordering.Id == request.OrderingId && x.Driver.Id == request.DriverId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                //if (orderingDriverReserve != null && data.IsOrderingDriverOffer == true)
                //{
                //    data.OrderingPrice = orderingDriverReserve.DriverOfferingPrice;
                //}
                IList <OrderingDriver> orderingDrivers = new List<OrderingDriver>();
                if (data.Drivers.Count() > 0)
                {
                    foreach (OrderingDriver driver in data.Drivers)
                    {

                        foreach (OrderingDriverFile orderingDriverFile in driver.DriverFiles)
                        {
                            if (File.Exists(Path.Combine(contentPath, orderingDriverFile.FilePath)))
                            {
                                File.Delete(Path.Combine(contentPath, orderingDriverFile.FilePath));
                            }
                            (await _orderingDriverFileRepository.CreateSQLQuery("DELETE Ordering_DriverFile where Id = '" + orderingDriverFile.Id.ToString() + "'").ConfigureAwait(false)).ExecuteUpdate();
                            //await _orderingDriverFileRepository.DeleteAsync(orderingDriverFile);
                        }
                        driver.DriverFiles.Clear();
                        (await _orderingDriverRepository.CreateSQLQuery("DELETE Ordering_Driver where Id = '" + driver.Id.ToString() + "'").ConfigureAwait(false)).ExecuteUpdate();
                        //await _orderingDriverRepository.DeleteAsync(driver);
                    }
                    data.Drivers.Clear();
                }


                OrderingDriver orderingDriver = new OrderingDriver
                {
                    Ordering = data,
                    DriverId = driverObject.Id,
                    DriverType = driverObject.DriverType,
                    Title = driverObject.Title,
                    FirstName = driverObject.FirstName,
                    MiddleName = driverObject.MiddleName,
                    LastName = driverObject.LastName,
                    Name = driverObject.Name,
                    IdentityNumber = driverObject.IdentityNumber,
                    PhoneCode = driverObject.PhoneCode,
                    PhoneNumber = driverObject.PhoneNumber,
                    Email = driverObject.Email,
                    Facbook = driverObject.Facbook,
                    Line = driverObject.Line,
                    Twitter = driverObject.Twitter,
                    Whatapp = driverObject.Whatapp,
                    Level = driverObject.Level,
                    Rating = driverObject.Rating,
                    Corparate = driverObject.Corparate,
                    Star = driverObject.Star,
                    Grade = driverObject.Grade,
                    Created = DateTime.Now,
                    Modified = DateTime.Now,
                    CreatedBy = request.CustomerId != null ? request.CustomerId.ToString() : "",
                    ModifiedBy = request.CustomerId != null ? request.CustomerId.ToString() : "",
                };
                string folderPath = contentPath + "ordering/" + request.DriverId.ToString() + "/" + currentTimeStr + "/orderingdriver/";
                if (!Directory.Exists(folderPath))
                {
                    Directory.CreateDirectory(folderPath);
                }
                int fileCount = 0;
                if (driverObject.Files != null)
                {
                    List<OrderingDriverFile> orderingDriverFiles = new List<OrderingDriverFile>();
                    foreach (DriverFile driverFile in driverObject.Files)
                    {
                        //if (driverFile.DocumentType == "DriverPicture" || driverFile.DocumentType == "profilepicture")
                        //{
                        fileCount = fileCount + 1;

                        string desPath = Path.Combine(folderPath, driverFile.FileName);
                        if (File.Exists(Path.Combine(contentPath, driverFile.FilePath)))
                        {
                            File.Copy(Path.Combine(contentPath, driverFile.FilePath), desPath);
                            OrderingDriverFile orderingDriverFile = new OrderingDriverFile
                            {
                                OrderingDriver = orderingDriver,
                                FileName = driverFile.FileName,
                                ContentType = driverFile.ContentType,
                                DocumentType = driverFile.DocumentType,
                                FilePath = "ordering/" + request.DriverId.ToString() + "/" + currentTimeStr + "/orderingdriver/" + driverFile.FileName,
                                DirectoryPath = "ordering/" + request.DriverId.ToString() + "/" + currentTimeStr + "/orderingdriver/",
                                FileEXT = driverFile.FileEXT,
                                Created = DateTime.Now,
                                Modified = DateTime.Now,
                                CreatedBy = request.CustomerId != null ? request.CustomerId.ToString() : "",
                                ModifiedBy = request.CustomerId != null ? request.CustomerId.ToString() : "",

                            };
                            orderingDriverFiles.Add(orderingDriverFile);
                        }
                        //}
                    }
                    orderingDriver.DriverFiles = orderingDriverFiles;
                }
                orderingDrivers.Add(orderingDriver);
                data.Drivers = orderingDrivers;
                orderingHistory.Drivers = orderingDrivers;

                var orderingcar = (await _orderingCarRepository.FindByCondition(x => x.Ordering.Id.Equals(request.OrderingId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                var driverCars = _mapper.Map<IList<DriverCar>>(driverObject.Cars);
                IList<OrderingCar> orderingCars = new List<OrderingCar>();// _mapper.Map<IList<OrderingCar>>(data.Cars);
                if (data.Cars.Count() > 0)
                {
                    foreach (OrderingCar orderingCar in data.Cars)
                    {
                        foreach (OrderingCarFile orderingCarFile in orderingCar.CarFiles)
                        {
                            if (File.Exists(Path.Combine(contentPath, orderingCarFile.FilePath)))
                            {
                                File.Delete(Path.Combine(contentPath, orderingCarFile.FilePath));
                            }
                            (await _orderingCarFileRepository.CreateSQLQuery("DELETE Ordering_CarFile where Id = '" + orderingCarFile.Id.ToString() + "'").ConfigureAwait(false)).ExecuteUpdate();
                            //await _orderingCarFileRepository.DeleteAsync(orderingCarFile);
                        }
                        (await _orderingCarRepository.CreateSQLQuery("DELETE Ordering_Car where Id = '" + orderingCar.Id.ToString() + "'").ConfigureAwait(false)).ExecuteUpdate();
                        //await _orderingCarRepository.DeleteAsync(orderingCar);
                    }
                }

                int index = 0;
                foreach (var driverCar in driverCars)
                {
                    index = index + 1;
                    OrderingCar car = new OrderingCar();
                    car.Ordering = data;
                    car.OrderingDriver = orderingDriver;
                    car.DriverCarId = driverCar.Id;
                    car.CarType = driverCar.CarType;
                    car.ActExpiry = driverCar.ActExpiry;
                    car.CarRegistration = driverCar.CarRegistration;
                    car.CarDescriptionDetail = driverCar.CarDescriptionDetail;
                    car.CarType = driverCar.CarType;
                    car.CarList = driverCar.CarList;
                    car.CarDescription = driverCar.CarDescription;
                    car.CarDescriptionDetail = driverCar.CarDescriptionDetail;
                    car.CarFeature = driverCar.CarFeature;
                    car.CarFeatureDetail = driverCar.CarFeatureDetail;
                    car.CarSpecification = driverCar.CarSpecification;
                    car.CarSpecificationDetail = driverCar.CarSpecificationDetail;
                    car.Width = driverCar.Width;
                    car.Length = driverCar.Length;
                    car.Height = driverCar.Height;
                    car.Temperature = driverCar.Temperature;
                    car.EnergySavingDevice = driverCar.EnergySavingDevice;
                    car.DriverName = driverCar.DriverName;
                    car.DriverPhoneCode = driverCar.DriverPhoneCode;
                    car.DriverPhoneNumber = driverCar.DriverPhoneNumber;
                    car.ProductInsurance = driverCar.ProductInsurance;
                    car.ProductInsuranceAmount = driverCar.ProductInsuranceAmount;
                    car.Note = driverCar.Note;
                    car.Created = DateTime.Now;
                    car.Modified = DateTime.Now;
                    car.CreatedBy = request.CustomerId != null ? request.CustomerId.ToString() : "";
                    car.ModifiedBy = request.CustomerId != null ? request.CustomerId.ToString() : "";

                    folderPath = contentPath + "ordering/" + data.Customer.Id.ToString() + "/" + currentTimeStr + "/orderingcar/" + index + "/";
                    if (!Directory.Exists(folderPath))
                    {
                        Directory.CreateDirectory(folderPath);
                    }
                    fileCount = 0;
                    if (driverCar.CarFiles != null)
                    {
                        List<OrderingCarFile> orderingCarFiles = new List<OrderingCarFile>();
                        foreach (DriverCarFile driverCarFile in driverCar.CarFiles)
                        {
                            fileCount = fileCount + 1;

                            string desPath = Path.Combine(folderPath, driverCarFile.FileName);
                            if (File.Exists(Path.Combine(contentPath, driverCarFile.FilePath)))
                            {
                                File.Copy(Path.Combine(contentPath, driverCarFile.FilePath), desPath);
                                OrderingCarFile orderingCarFile = new OrderingCarFile
                                {
                                    OrderingCar = car,
                                    FileName = driverCarFile.FileName,
                                    ContentType = driverCarFile.ContentType,
                                    DocumentType = driverCarFile.DocumentType,
                                    FilePath = "ordering/" + data.Customer.Id.ToString() + "/" + currentTimeStr + "/orderingcar/" + index + "/" + driverCarFile.FileName,
                                    DirectoryPath = "ordering/" + data.Customer.Id.ToString() + "/" + currentTimeStr + "/orderingcar/" + index + "/",
                                    FileEXT = driverCarFile.FileEXT,
                                    Created = DateTime.Now,
                                    Modified = DateTime.Now,
                                    CreatedBy = request.CustomerId != null ? request.CustomerId.ToString() : "",
                                    ModifiedBy = request.CustomerId != null ? request.CustomerId.ToString() : "",

                                };
                                orderingCarFiles.Add(orderingCarFile);
                            }
                        }
                        car.CarFiles = orderingCarFiles;
                    }
                    data.CarRegistration_Str = car.CarRegistration;
                    orderingCars.Add(car);
                };
                data.Cars = orderingCars;
                data.Status = 201;

                var orderingStatusObject = orderingStatus.Where(o => o.id == data.Status).FirstOrDefault();
                data.StatusName_Str = orderingStatusObject != null ? orderingStatusObject.th : "-";

                orderingHistory.Cars = orderingCars;

                //if (request.Insurance != null)
                //{
                //    var insurance = _mapper.Map<OrderingInsurance>(request.Insurance);
                //    insurance.Ordering = data;
                //    insurance.ProductType = productTypes.Where(p => p.Id == request.Insurance.ProductTypeId).FirstOrDefault();
                //    if (data.Insurances == null)
                //    {
                //        data.Insurances = new List<OrderingInsurance>();
                //    }
                //    data.Insurances.Add(insurance);
                //}

                OrderingPayment payment = new OrderingPayment
                {
                    Ordering = data,
                    Module = "customer",
                    UserId = request.CustomerId,
                    Channel = request.Channel != null ? request.Channel.ToString() : "",
                    Amount = request.Amount,
                    Created = DateTime.UtcNow,
                    Modified = DateTime.UtcNow,
                    CreatedBy = request.CustomerId != null ? request.CustomerId.ToString() : "",
                    ModifiedBy = request.CustomerId != null ? request.CustomerId.ToString() : ""
                };
                if (data.Payments == null)
                {
                    data.Payments = new List<OrderingPayment>();
                }
                if (orderingHistory.Payments == null)
                {
                    orderingHistory.Payments = new List<OrderingPayment>();
                }
                data.Payments.Add(payment);

                orderingHistory.Payments.Add(payment);

                #region
                string trackingCode = "";
                while (trackingCode == "")
                {
                    trackingCode = Guid.NewGuid().ToString().Replace("-", "").ToUpper().Remove(13);
                    var objordering = (await _orderingTrackingCodeRepository.FindByCondition(x => x.TrackingCode.ToUpper() == trackingCode).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                    if (objordering != null)
                        trackingCode = "";
                }
                data.TrackingCode = trackingCode;
                (await _orderingTrackingCodeRepository.CreateSQLQuery("INSERT INTO Ordering_TrackingCode (TrackingCode,Section) VALUES ('" + trackingCode + "','ordering')").ConfigureAwait(false)).ExecuteUpdate();

                foreach (OrderingAddress address in data.Addresses)
                {
                    trackingCode = "";
                    while (trackingCode == "")
                    {
                        trackingCode = Guid.NewGuid().ToString().Replace("-", "").ToUpper().Remove(13);
                        var objordering = (await _orderingTrackingCodeRepository.FindByCondition(x => x.TrackingCode.ToUpper() == trackingCode).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                        if (objordering != null)
                            trackingCode = "";
                    }
                    address.TrackingCode = trackingCode;
                    (await _orderingTrackingCodeRepository.CreateSQLQuery("INSERT INTO Ordering_TrackingCode (TrackingCode,Section) VALUES ('" + trackingCode + "','address')").ConfigureAwait(false)).ExecuteUpdate();
                    address.Status = 201;
                }
                #endregion
                customerPayment.Amount = customerPayment.Amount - request.Amount;
                await _customerPaymentRepository.UpdateAsync(customerPayment);
                data.Status = 201;//request.Status;
                data.IsRequestTax = request.IsRequestTax;
                data.Modified = DateTime.UtcNow;
                data.ModifiedBy = request.CustomerId != null ? request.CustomerId.ToString() : "";

                //data.Customer.OrderValuePerYear = data.Customer.OrderValuePerYear + data.OrderingPrice;
                //data.Customer.OrderValuePerMonth = data.Customer.OrderValuePerMonth + data.OrderingPrice;

                await _orderingRepository.UpdateAsync(data);

                //var customerActivity = (await _customerActivityRepository.FindByCondition(x => x.CustomerId.Equals(request.CustomerId) && x.Date == DateTime.Now.ToString("yyyy'-'MM'-'dd")).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                //if (customerActivity == null)
                //{
                //    CustomerActivity newCustomerActivity = new CustomerActivity
                //    {
                //        CustomerId = request.CustomerId,
                //        Date = DateTime.Now.ToString("yyyy'-'MM'-'dd"),
                //        Star = 0,
                //        RequestCar = 0,
                //        Cancel = 0,
                //        OrderingValue = data.OrderingPrice,
                //        Created = DateTime.Now,
                //        CreatedBy = request.UserId != null ? request.UserId.Value.ToString() : "-",
                //        Modified = DateTime.Now,
                //        ModifiedBy = request.UserId != null ? request.UserId.Value.ToString() : "-",
                //    };
                //    await _customerActivityRepository.AddAsync(newCustomerActivity);
                //}
                //else
                //{
                //    customerActivity.OrderingValue = customerActivity.OrderingValue + data.OrderingPrice;
                //    await _customerActivityRepository.UpdateAsync(customerActivity);
                //}

                //var customerLevelObject = (await _customerLevelRepository.FindByCondition(x => x.CustomerId.Equals(data.Customer.Id)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                //if (customerLevelObject == null)
                //{
                //    CustomerLevel customerLevel = new CustomerLevel
                //    {
                //        CustomerId = request.CustomerId,
                //        Level = 1,
                //        RequestCar = 1,
                //        Star = 0,
                //        CancelAmount = 0,
                //        OrderValue = data.OrderingPrice,
                //        Created = DateTime.Now,
                //        Modified = DateTime.Now,
                //        CreatedBy = request.CustomerId != null ? request.CustomerId.ToString() : "",
                //        ModifiedBy = request.CustomerId != null ? request.CustomerId.ToString() : ""
                //    };
                //    await _customerLevelRepository.AddAsync(customerLevel);
                //}
                //else
                //{
                //    customerLevelObject.OrderValue = customerLevelObject.OrderValue + data.OrderingPrice;
                //    await _customerLevelRepository.UpdateAsync(customerLevelObject);
                //}

                orderingHistory.TrackingCode = data.TrackingCode;
                orderingHistory.OrderingId = data.Id;
                orderingHistory.Driver = data.Driver;
                orderingHistory.Customer = data.Customer;
                orderingHistory.ProductCBM = data.ProductCBM;
                orderingHistory.ProductTotalWeight = data.ProductTotalWeight;
                orderingHistory.OrderingPrice = data.OrderingPrice;
                orderingHistory.OrderingStatus = data.Status;
                orderingHistory.AdditionalDetail = data.AdditionalDetail;
                orderingHistory.Note = data.Note;
                orderingHistory.OrderNumber = data.TrackingCode;
                orderingHistory.OrderingDesiredPrice = data.OrderingDesiredPrice;
                orderingHistory.OrderingDriverOffering = data.OrderingDriverOffering;
                orderingHistory.IsOrderingDriverOffer = data.IsOrderingDriverOffer;
                orderingHistory.CancelStatus = data.CancelStatus;
                orderingHistory.CustomerRanking = data.CustomerRanking;
                orderingHistory.DriverRanking = data.DriverRanking;
                orderingHistory.PinnedDate = data.PinnedDate;
                orderingHistory.Distance = data.Distance;
                orderingHistory.EstimateTime = data.EstimateTime;
                orderingHistory.IsDriverPay = data.IsDriverPay;
                orderingHistory.CurrentLocation = data.CurrentLocation;
                orderingHistory.IsMutipleRoutes = data.IsMutipleRoutes;
                orderingHistory.GPValue = data.GPValue;
                orderingHistory.DriverPayValue = data.DriverPayValue;
                orderingHistory.CustomerCancelValue = data.CustomerCancelValue;
                orderingHistory.DriverCancelValue = data.DriverCancelValue;
                orderingHistory.CustomerCashBack = data.CustomerCashBack;
                orderingHistory.DriverCashBack = data.DriverCashBack;
                orderingHistory.DriverAnnouncements = data.DriverAnnouncements;
                orderingHistory.Created = DateTime.UtcNow;
                orderingHistory.Modified = DateTime.UtcNow;
                orderingHistory.CreatedBy = request.CustomerId != null ? request.CustomerId.ToString() : "";
                orderingHistory.ModifiedBy = request.CustomerId != null ? request.CustomerId.ToString() : "";
                orderingHistory.Created = DateTime.UtcNow;
                orderingHistory.Modified = DateTime.UtcNow;
                orderingHistory.CreatedBy = request.CustomerId != null ? request.CustomerId.ToString() : "";
                orderingHistory.ModifiedBy = request.CustomerId != null ? request.CustomerId.ToString() : "";
                await _orderingHistoryRepository.AddAsync(orderingHistory);




                (await _orderingDriverReserveRepository.CreateSQLQuery("DELETE FROM Ordering_DriverReserve  WHERE OrderingId = '" + data.Id.ToString() + "'").ConfigureAwait(false)).ExecuteUpdate();

                //if (request.Status == 3)
                //{

                #region
                var customerOrderingStatusFile = Path.Combine(Directory.GetCurrentDirectory(), $"Shared\\customer_orderingStatus.json");
                var customerOrderingStatusJson = System.IO.File.ReadAllText(customerOrderingStatusFile);
                List<OrderingStatusViewModel> customerOrderingStatus = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OrderingStatusViewModel>>(customerOrderingStatusJson);

                var driverOrderingStatusFile = Path.Combine(Directory.GetCurrentDirectory(), $"Shared\\driver_orderingStatus.json");
                var driverOrderingStatusJson = System.IO.File.ReadAllText(driverOrderingStatusFile);
                List<OrderingStatusViewModel> driverOrderingStatus = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OrderingStatusViewModel>>(driverOrderingStatusJson);
                //var cloneOrdering = _mapper.Map<OrderingViewModel>(data);
                var cloneOrdering = new OrderingNotificationViewModel
                {
                    id = data.Id,
                    status = data.Status,
                    trackingCode = data.TrackingCode,
                    orderNumber = data.TrackingCode,
                    products = _mapper.Map<List<OrderingProductNotificationViewModel>>(data.Products)
                };


                if (data.Cars.Count > 0)
                {
                    cloneOrdering.carType = _mapper.Map<CarTypeViewModel>(data.Cars[0].CarType);
                    cloneOrdering.carList = _mapper.Map<CarListViewModel>(data.Cars[0].CarList);
                }

                foreach (OrderingProductNotificationViewModel productViewModel in cloneOrdering.products)
                {
                    if (productViewModel.productType != null)
                    {
                        string[] productTypeIds = productViewModel.productType.Split(",");
                        productViewModel.productTypes = new List<ProductTypeViewModel>();
                        foreach (string productId in productTypeIds)
                        {
                            int id = 0;
                            if (int.TryParse(productId, out id))
                            {
                                var productType = productTypes.Where(p => p.Id == id).FirstOrDefault();
                                if (productType != null)
                                    productViewModel.productTypes.Add(_mapper.Map<ProductTypeViewModel>(productType));
                            }
                        }
                    }
                    if (productViewModel.packaging != null)
                    {
                        string[] packagingIds = productViewModel.packaging.Split(",");
                        productViewModel.packagings = new List<ProductPackagingViewModel>();
                        foreach (string packagingId in packagingIds)
                        {
                            int id = 0;
                            if (int.TryParse(packagingId, out id))
                            {
                                var packaging = packagings.Where(p => p.Id == id).FirstOrDefault();
                                if (packaging != null)
                                    productViewModel.packagings.Add(_mapper.Map<ProductPackagingViewModel>(packaging));
                            }
                        }
                    }

                }
                if (data.Addresses.Count > 0)
                {
                    cloneOrdering.startPoint = _mapper.Map<OrderingAddressNotificationViewModel>(data.Addresses[0]);
                    cloneOrdering.endPoint = _mapper.Map<OrderingAddressNotificationViewModel>(data.Addresses[data.Addresses.Count - 1]);
                }
                #endregion
                // noti driver
                {
                    if (cloneOrdering.status != null)
                    {
                        cloneOrdering.statusObj = driverOrderingStatus.Where(o => o.id == cloneOrdering.status).FirstOrDefault();
                    }
                    var driverSignalIds = (await _notificationUserRepository.FindByCondition(x => x.UserId.Equals(data.Driver.Id)).ConfigureAwait(false)).AsQueryable().Select(x => x.OneSignalId).ToList();
                    var notificationDetails = Path.Combine(Directory.GetCurrentDirectory(), $"Shared\\NotificationWord.json");
                    var notificationJson = System.IO.File.ReadAllText(notificationDetails);
                    List<NotificationWordStatusViewModel> notificationStatus = Newtonsoft.Json.JsonConvert.DeserializeObject<List<NotificationWordStatusViewModel>>(notificationJson);

                    string title = "";
                    string detail = "";
                    var notificationObj = notificationStatus.Where(o => o.id == 402).FirstOrDefault();
                    if (notificationObj != null)
                    {
                        title = notificationObj.title.th;
                        if (data.Customer != null)
                        {
                            detail = notificationObj.detail.th;
                        }
                    }
                    //if (cloneOrdering.products.Count > 2)
                    //{
                    //    cloneOrdering.products.RemoveRange(2, cloneOrdering.products.Count - 2);
                    //}
                    Domain.Notification notification = new Domain.Notification();

                    notification.OneSignalId = driverSignalIds.FirstOrDefault();
                    notification.UserId = data.Driver.Id;
                    notification.OwnerId = request.CustomerId;
                    notification.Title = title;
                    notification.Detail = detail;
                    notification.ServiceCode = "Ordering";
                    //notification.ReferenceContentKey = request.ReferenceContentKey;
                    notification.Payload = Newtonsoft.Json.JsonConvert.SerializeObject(cloneOrdering);
                    notification.Created = DateTime.UtcNow;
                    notification.Modified = DateTime.UtcNow;
                    notification.CreatedBy = request.CustomerId.ToString();
                    notification.ModifiedBy = request.CustomerId.ToString();
                    await _notificationRepository.AddAsync(notification);
                    #region insert to Inbox
                    DriverInbox driverInbox = new DriverInbox()
                    {
                        DriverId = data.Driver.Id,
                        Title = "-",
                        Title_ENG = "-",
                        Module = "การแจ้งเตือน",
                        Module_ENG = "Notification",
                        Content = Newtonsoft.Json.JsonConvert.SerializeObject(cloneOrdering),
                        FromUser = data.Customer.FirstName + data.Customer.LastName,
                        IsDelete = false,
                        Created = DateTime.Now
                    };
                    await _driverInboxRepository.AddAsync(driverInbox);
                    #endregion
                    driverSignalIds = driverSignalIds.Where(x => x.Trim() != "").ToList();
                    if (driverSignalIds.Count > 0)
                    {
                        NotificationManager notiMgr = new NotificationManager(request.DRIVER_APP_ID, request.DRIVER_REST_API_KEY, request.DRIVER_AUTH_ID);
                        notiMgr.SendNotificationAsync(title, detail, driverSignalIds, notification.ServiceCode, "", "{id:'" + cloneOrdering.id + "'}");
                    }
                }
                // noti customer
                {
                    if (cloneOrdering.status != null)
                    {
                        cloneOrdering.statusObj = customerOrderingStatus.Where(o => o.id == cloneOrdering.status).FirstOrDefault();
                    }
                    var customerSignalIds = (await _notificationUserRepository.FindByCondition(x => x.UserId.Equals(data.Customer.Id)).ConfigureAwait(false)).AsQueryable().Select(x => x.OneSignalId).ToList();
                    var notificationDetails = Path.Combine(Directory.GetCurrentDirectory(), $"Shared\\NotificationWord.json");
                    var notificationJson = System.IO.File.ReadAllText(notificationDetails);
                    List<NotificationWordStatusViewModel> notificationStatus = Newtonsoft.Json.JsonConvert.DeserializeObject<List<NotificationWordStatusViewModel>>(notificationJson);

                    string title = "";
                    string detail = "";
                    var notificationObj = notificationStatus.Where(o => o.id == 4).FirstOrDefault();
                    if (notificationObj != null)
                    {
                        title = notificationObj.title.th;
                        if (data.Customer != null)
                        {
                            detail = notificationObj.detail.th;
                        }
                    }
                    if (cloneOrdering.products.Count > 2)
                    {
                        cloneOrdering.products.RemoveRange(2, cloneOrdering.products.Count - 2);
                    }
                    Domain.Notification notification = new Domain.Notification();

                    notification.OneSignalId = customerSignalIds.FirstOrDefault();
                    notification.UserId = data.Customer.Id;
                    notification.OwnerId = data.Customer.Id;
                    notification.Title = title;
                    notification.Detail = detail;
                    notification.ServiceCode = "Ordering";
                    //notification.ReferenceContentKey = request.ReferenceContentKey;
                    notification.Payload = Newtonsoft.Json.JsonConvert.SerializeObject(cloneOrdering);
                    notification.Created = DateTime.UtcNow;
                    notification.Modified = DateTime.UtcNow;
                    notification.CreatedBy = request.CustomerId.ToString();
                    notification.ModifiedBy = request.CustomerId.ToString();
                    await _notificationRepository.AddAsync(notification);
                    //#region insert to Inbox
                    //CustomerInbox customerInbox = new CustomerInbox()
                    //{
                    //    CustomerId = data.Customer.Id,
                    //    Title = "-",
                    //    Title_ENG = "-",
                    //    Module = "การแจ้งเตือน",
                    //    Module_ENG = "Notification",
                    //    Content = Newtonsoft.Json.JsonConvert.SerializeObject(cloneOrdering),
                    //    FromUser = data.Customer.FirstName + data.Customer.LastName,
                    //    IsDelete = false,
                    //    Created = DateTime.Now
                    //};
                    //await _customerInboxRepository.AddAsync(customerInbox);
                    //#endregion
                    customerSignalIds = customerSignalIds.Where(x => x.Trim() != "").ToList();
                    if (customerSignalIds.Count > 0)
                    {

                        NotificationManager notiMgr = new NotificationManager(request.APP_ID, request.REST_API_KEY, request.AUTH_ID);
                        notiMgr.SendNotificationAsync(title, detail, customerSignalIds, notification.ServiceCode, "", "{id:'" + cloneOrdering.id + "'}");
                    }
                }
                //}
                var log = new CreateAppLog(_applicationLogRepository);
                log.CreateLog("Ordering", "DriverAcceptOrdering", "Edit", Newtonsoft.Json.JsonConvert.SerializeObject(request), request.UserId.ToString());
                return new Response<Guid>(data.Id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
