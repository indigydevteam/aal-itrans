﻿using AutoMapper;
using iTrans.Transportation.Application.DTOs.CarList;
using iTrans.Transportation.Application.DTOs.CarType;
using iTrans.Transportation.Application.DTOs.Ordering;
using iTrans.Transportation.Application.DTOs.OrderingAddress;
using iTrans.Transportation.Application.DTOs.OrderingAddressProduct;
using iTrans.Transportation.Application.DTOs.OrderingProduct;
using iTrans.Transportation.Application.DTOs.OrderingStatus;
using iTrans.Transportation.Application.DTOs.ProductPackaging;
using iTrans.Transportation.Application.DTOs.ProductType;
using iTrans.Transportation.Application.Enums;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Helper;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;


namespace iTrans.Transportation.Application.Features.Orderings.Commands
{
    public class UpdateCurrentLocationCommand : IRequest<Response<Guid>>
    {
        public virtual Guid? UserId { get; set; }
        public virtual Guid OrderingId { get; set; }
        public virtual int OrderingAddressId { get; set; }
        public virtual Guid DriverId { get; set; }
        public virtual string Location { get; set; }
        public virtual string Distance { get; set; }
        public virtual string EstimateTime { get; set; }
        public string APP_ID { get; set; }
        public string REST_API_KEY { get; set; }
        public string AUTH_ID { get; set; }
        //public string DRIVER_APP_ID { get; set; }
        //public string DRIVER_REST_API_KEY { get; set; }
        //public string DRIVER_AUTH_ID { get; set; }
    }
    public class UpdateCurrentLocationCommandHandler : IRequestHandler<UpdateCurrentLocationCommand, Response<Guid>>
    {
        private readonly IOrderingRepositoryAsync _orderingRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IProductTypeRepositoryAsync _productTypeRepository;
        private readonly IProductPackagingRepositoryAsync _productPackagingRepository;
        private readonly INotificationRepositoryAsync _notificationRepository;
        private readonly INotificationUserRepositoryAsync _notificationUserRepository;
        private readonly IMapper _mapper;
        private readonly IConfiguration _configuration;

        public UpdateCurrentLocationCommandHandler(IOrderingRepositoryAsync orderingRepository, IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper, IConfiguration configuration
            , IProductTypeRepositoryAsync productTypeRepository, IProductPackagingRepositoryAsync productPackagingRepository, INotificationRepositoryAsync notificationRepository, INotificationUserRepositoryAsync notificationUserRepository)
        {
            _orderingRepository = orderingRepository;
            _applicationLogRepository = applicationLogRepository;
            _productTypeRepository = productTypeRepository;
            _productPackagingRepository =  productPackagingRepository;
            _notificationRepository = notificationRepository;
            _notificationUserRepository = notificationUserRepository;
            _mapper = mapper;
            _configuration = configuration;
        }

        public async Task<Response<Guid>> Handle(UpdateCurrentLocationCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var ordering = (await _orderingRepository.FindByCondition(x => x.Id == request.OrderingId && x.Drivers.Where(d => d.DriverId.Equals(request.DriverId)).Count() > 0).ConfigureAwait(false)).AsQueryable().FirstOrDefault();

                if (ordering == null)
                {
                    throw new ApiException($"Ordering Not Found.");
                }
                else
                {
                    int detectArea = Convert.ToInt32(_configuration.GetSection("DetectArea").Value);
                    string api_key = _configuration.GetSection("GoogleAPIKEY").Value;

                    double noti_Distance = 10;
                    double noti_Time = 20;
                    int noti_Limit = 0;
                    int noti_Amount = 0;

                    noti_Distance = Convert.ToDouble(_configuration.GetSection("Noti_Distance").Value);
                    noti_Time = Convert.ToDouble(_configuration.GetSection("Noti_Time").Value);
                    noti_Limit = int.Parse(_configuration.GetSection("Noti_Limit").Value);

                    ordering.Modified = DateTime.Now;
                    ordering.ModifiedBy = request.UserId != null ? request.UserId.Value.ToString() : "";
                    ordering.CurrentLocation = request.Location;
                    var orderinfAddress = ordering.Addresses.Where(x => x.Id == request.OrderingAddressId).FirstOrDefault();
                    if (orderinfAddress != null)
                    {
                        var locationDistince = LocationHelper.GetDistanceAndEstimateTime(detectArea, api_key, request.Location, orderinfAddress.Maps);
                        ordering.Distance = locationDistince.Distance;
                        ordering.EstimateTime = locationDistince.EstimateTime;
                        noti_Amount = orderinfAddress.NotiAmount != null ? orderinfAddress.NotiAmount : 0;
                    }
                    else
                    {
                        ordering.Distance = request.Distance;
                        ordering.EstimateTime = request.EstimateTime;
                    }
                    
                    //await _orderingRepository.UpdateAsync(ordering);
                    double distance = Convert.ToDouble(ordering.Distance) / 1000;
                    double time = Convert.ToDouble(ordering.EstimateTime);
                    if (ordering.Status == 0 && orderinfAddress != null && (orderinfAddress.Status == 4 || orderinfAddress.Status == 9))
                    {
                        if ((distance <= noti_Distance || time <= noti_Time) && noti_Amount < noti_Limit)
                        {
                            #region send noti
                            var productTypes = (await _productTypeRepository.FindByCondition(x => x.Active.Equals(true)).ConfigureAwait(false)).AsQueryable().ToList();
                            var packagings = (await _productPackagingRepository.FindByCondition(x => x.Active.Equals(true)).ConfigureAwait(false)).AsQueryable().ToList();

                            var customerOrderingStatusFile = Path.Combine(Directory.GetCurrentDirectory(), $"Shared\\customer_orderingStatus.json");
                            var customerOrderingStatusJson = System.IO.File.ReadAllText(customerOrderingStatusFile);
                            List<OrderingStatusViewModel> customerOrderingStatus = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OrderingStatusViewModel>>(customerOrderingStatusJson);

                            var driverOrderingStatusFile = Path.Combine(Directory.GetCurrentDirectory(), $"Shared\\driver_orderingStatus.json");
                            var driverOrderingStatusJson = System.IO.File.ReadAllText(customerOrderingStatusFile);
                            List<OrderingStatusViewModel> driverOrderingStatus = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OrderingStatusViewModel>>(driverOrderingStatusJson);


                            var userId = ordering.Customer.Id;
                            var ownerId = request.DriverId;

                            var APP_ID = request.APP_ID;
                            var REST_API_KEY = request.REST_API_KEY;
                            var AUTH_ID = request.AUTH_ID;

                            if (userId != null)
                            {
                                //var cloneOrdering = _mapper.Map<OrderingViewModel>(data);
                                var cloneOrdering = new OrderingNotificationViewModel
                                {
                                    id = ordering.Id,
                                    status = ordering.Status == 0 && orderinfAddress != null ? orderinfAddress.Status : ordering.Status,
                                    trackingCode = ordering.TrackingCode,
                                    orderNumber = ordering.TrackingCode,
                                    products = _mapper.Map<List<OrderingProductNotificationViewModel>>(ordering.Products)
                                };
                                if (ordering.Cars.Count > 0)
                                {
                                    cloneOrdering.carType = _mapper.Map<CarTypeViewModel>(ordering.Cars[0].CarType);
                                    cloneOrdering.carList = _mapper.Map<CarListViewModel>(ordering.Cars[0].CarList);
                                }
                                if (cloneOrdering.status != null)
                                {
                                    cloneOrdering.statusObj = driverOrderingStatus.Where(o => o.id == cloneOrdering.status).FirstOrDefault();
                                    if (request.DriverId != null && request.DriverId != new Guid())
                                    {
                                        cloneOrdering.statusObj = customerOrderingStatus.Where(o => o.id == cloneOrdering.status).FirstOrDefault();
                                    }
                                }
                                if (ordering.Addresses.Count > 0)
                                {
                                    cloneOrdering.startPoint = _mapper.Map<OrderingAddressNotificationViewModel>(ordering.Addresses[0]);
                                    cloneOrdering.endPoint = _mapper.Map<OrderingAddressNotificationViewModel>(ordering.Addresses[ordering.Addresses.Count - 1]);
                                }
                                foreach (OrderingProductNotificationViewModel productViewModel in cloneOrdering.products)
                                {
                                    if (productViewModel.productType != null)
                                    {
                                        string[] productTypeIds = productViewModel.productType.Split(",");
                                        productViewModel.productTypes = new List<ProductTypeViewModel>();
                                        foreach (string productId in productTypeIds)
                                        {
                                            int id = 0;
                                            if (int.TryParse(productId, out id))
                                            {
                                                var productType = productTypes.Where(p => p.Id == id).FirstOrDefault();
                                                if (productType != null)
                                                    productViewModel.productTypes.Add(_mapper.Map<ProductTypeViewModel>(productType));
                                            }
                                        }
                                    }
                                    if (productViewModel.packaging != null)
                                    {
                                        string[] packagingIds = productViewModel.packaging.Split(",");
                                        productViewModel.packagings = new List<ProductPackagingViewModel>();
                                        foreach (string packagingId in packagingIds)
                                        {
                                            int id = 0;
                                            if (int.TryParse(packagingId, out id))
                                            {
                                                var packaging = packagings.Where(p => p.Id == id).FirstOrDefault();
                                                if (packaging != null)
                                                    productViewModel.packagings.Add(_mapper.Map<ProductPackagingViewModel>(packaging));
                                            }
                                        }
                                    }

                                }
                                //if (data.Addresses.Count > 0)
                                //{
                                //    cloneOrdering.startPoint = _mapper.Map<OrderingAddressNotificationViewModel>(data.Addresses[0]);
                                //    cloneOrdering.endPoint = _mapper.Map<OrderingAddressNotificationViewModel>(data.Addresses[data.Addresses.Count - 1]);
                                //}
                                var oneSignalIds = (await _notificationUserRepository.FindByCondition(x => x.UserId.Equals(userId)).ConfigureAwait(false)).AsQueryable().Select(x => x.OneSignalId).ToList();
                                var notificationDetails = Path.Combine(Directory.GetCurrentDirectory(), $"Shared\\NotificationWord.json");
                                var notificationJson = System.IO.File.ReadAllText(notificationDetails);
                                List<NotificationWordStatusViewModel> notificationStatus = Newtonsoft.Json.JsonConvert.DeserializeObject<List<NotificationWordStatusViewModel>>(notificationJson);

                                string title = "คนขับใกล้ถึงจุดส่งของ";
                                string detail = "คนขับใกล้ถึงจุดส่งของ ระยะทาง " + distance + " ก.ม. ระยะเวลา " + time + " นาที";

                                Domain.Notification notification = new Domain.Notification();

                                notification.OneSignalId = oneSignalIds.FirstOrDefault();
                                notification.UserId = userId;
                                notification.OwnerId = ownerId;
                                notification.Title = title;
                                notification.Detail = detail;
                                notification.ServiceCode = "Ordering";
                                //notification.ReferenceContentKey = request.ReferenceContentKey;
                                notification.Payload = Newtonsoft.Json.JsonConvert.SerializeObject(cloneOrdering);
                                notification.Created = DateTime.UtcNow;
                                notification.Modified = DateTime.UtcNow;
                                notification.CreatedBy = request.DriverId.ToString();
                                notification.ModifiedBy = request.DriverId.ToString();
                                await _notificationRepository.AddAsync(notification);

                                oneSignalIds = oneSignalIds.Where(x => x.Trim() != "").ToList();
                                if (oneSignalIds.Count > 0)
                                {
                                    NotificationManager notiMgr = new NotificationManager(APP_ID, REST_API_KEY, AUTH_ID);
                                    notiMgr.SendNotificationAsync(title, detail, oneSignalIds, notification.ServiceCode, "", "{id:'" + cloneOrdering.id + "'}");
                                }
                            }
                            #endregion
                            if (orderinfAddress != null)
                            {
                                orderinfAddress.NotiAmount = orderinfAddress.NotiAmount == null ? 1: orderinfAddress.NotiAmount + 1;
                            }
                        }
                    }
                    await _orderingRepository.UpdateAsync(ordering);
                    var log = new CreateAppLog(_applicationLogRepository);
                    log.CreateLog("Ordering", "Update CurrentLocation", "Edit", Newtonsoft.Json.JsonConvert.SerializeObject(request),request.UserId.ToString());

                    return new Response<Guid>(ordering.Id);

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}