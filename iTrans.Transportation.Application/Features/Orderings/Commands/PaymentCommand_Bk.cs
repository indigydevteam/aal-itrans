﻿using AutoMapper;
using iTrans.Transportation.Application.DTOs.Ordering;
using iTrans.Transportation.Application.DTOs.OrderingAddress;
using iTrans.Transportation.Application.DTOs.OrderingAddressProduct;
using iTrans.Transportation.Application.DTOs.OrderingInsurance;
using iTrans.Transportation.Application.DTOs.OrderingProduct;
using iTrans.Transportation.Application.DTOs.OrderingStatus;
using iTrans.Transportation.Application.DTOs.ProductPackaging;
using iTrans.Transportation.Application.DTOs.ProductType;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.Orderings.Commands
{
    public class PaymentCommand : IRequest<Response<Guid>>
    {
        public Guid CustomerId { get; set; }
        public Guid DriverId { get; set; }
        public Guid OrderingId { get; set; }
        public string Channel { get; set; }
        public decimal Amount { get; set; }
        public CreateInsuranceViewModel Insurance { get; set; }
        public int Status { get; set; }
        public string APP_ID { get; set; }
        public string REST_API_KEY { get; set; }
        public string AUTH_ID { get; set; }
    }
    public class PaymentCommandHandler : IRequestHandler<PaymentCommand, Response<Guid>>
    {
        private readonly IOrderingRepositoryAsync _orderingRepository;
        private readonly IProductTypeRepositoryAsync _productTypeRepository;
        private readonly IDriverAnnouncementRepositoryAsync _driverAnnouncementRepository;
        private readonly IOrderingDriverReserveRepositoryAsync _orderingDriverReserveRepository;
        private readonly INotificationRepositoryAsync _notificationRepository;
        private readonly INotificationUserRepositoryAsync _notificationUserRepository;
        private readonly IOrderingTrackingCodeRepositoryAsync _orderingTrackingCodeRepository;
        private readonly IProductPackagingRepositoryAsync _productPackagingRepository;
        private readonly IMapper _mapper;

        public PaymentCommandHandler(IOrderingRepositoryAsync orderingRepository, IOrderingTrackingCodeRepositoryAsync orderingTrackingCodeRepository, IProductTypeRepositoryAsync productTypeRepository
            , INotificationRepositoryAsync notificationRepository, IDriverAnnouncementRepositoryAsync driverAnnouncementRepository , IProductPackagingRepositoryAsync productPackagingRepository
            ,IOrderingDriverReserveRepositoryAsync orderingDriverReserveRepository, INotificationUserRepositoryAsync notificationUserRepository, IMapper mapper)
        {
            _orderingRepository = orderingRepository;
            _productTypeRepository = productTypeRepository;
            _productPackagingRepository = productPackagingRepository;
            _orderingTrackingCodeRepository = orderingTrackingCodeRepository;
            _driverAnnouncementRepository = driverAnnouncementRepository;
            _orderingDriverReserveRepository = orderingDriverReserveRepository;
            _notificationRepository = notificationRepository;
            _notificationUserRepository = notificationUserRepository;
            _mapper = mapper;
        }

        public async Task<Response<Guid>> Handle(PaymentCommand request, CancellationToken cancellationToken)
        {
            try
            {
                

                var data = (await _orderingRepository.FindByCondition(x => x.Id == request.OrderingId && x.Customer.Id == request.CustomerId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (data == null)
                {
                    throw new ApiException($"Ordering Not Found.");
                }
                var driverAnnouncement = (await _driverAnnouncementRepository.FindByCondition(x => x.Order.Id.Equals(request.OrderingId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                var productTypes = (await _productTypeRepository.FindByCondition(x => x.Active.Equals(true)).ConfigureAwait(false)).AsQueryable().ToList();
                var packagings = (await _productPackagingRepository.FindByCondition(x => x.Active.Equals(true)).ConfigureAwait(false)).AsQueryable().ToList();
                var folderDetails = Path.Combine(Directory.GetCurrentDirectory(), $"Shared\\OrderingStatus.json");
                var statusJson = System.IO.File.ReadAllText(folderDetails);
                List<OrderingStatusViewModel> orderingStatus = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OrderingStatusViewModel>>(statusJson);

                if (request.Status != 0)
                {
                    if (driverAnnouncement != null)
                    {

                        driverAnnouncement.Status = request.Status;
                        await _driverAnnouncementRepository.UpdateAsync(driverAnnouncement);

                    }
                    data.Status = request.Status;
                    foreach (OrderingAddress address in data.Addresses)
                    {
                        address.Status = request.Status;
                    }
                }
                if (request.Insurance != null)
                {
                    var insurance = _mapper.Map<OrderingInsurance>(request.Insurance);
                    insurance.Ordering = data;
                    insurance.ProductType = productTypes.Where(p => p.Id == request.Insurance.ProductTypeId).FirstOrDefault();
                    if (data.Insurances == null)
                    {
                        data.Insurances = new List<OrderingInsurance>();
                    }
                    data.Insurances.Add(insurance);
                }
                OrderingPayment payment = new OrderingPayment
                {
                    Ordering = data,
                    Channel = request.Channel,
                    Amount = request.Amount,
                    Created = DateTime.UtcNow,
                    Modified = DateTime.UtcNow,
                    CreatedBy = request.CustomerId.ToString(),
                    ModifiedBy = request.CustomerId.ToString()
                };
                if (data.Payments == null)
                {
                    data.Payments = new List<OrderingPayment>();
                }
                data.Payments.Add(payment);

                await _orderingRepository.UpdateAsync(data);
                (await _orderingDriverReserveRepository.CreateSQLQuery("DELETE FROM Ordering_DriverReserve  WHERE OrderingId = '"+ data.Id.ToString() + "'").ConfigureAwait(false)).ExecuteUpdate();
                if (request.Status == 3)
                {
                    #region
                    string trackingCode = "";
                    while (trackingCode == "")
                    {
                        trackingCode = Guid.NewGuid().ToString().Replace("-", "").ToUpper().Remove(13);
                        var objordering = (await _orderingTrackingCodeRepository.FindByCondition(x => x.TrackingCode.ToUpper() == trackingCode).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                        if (objordering != null)
                            trackingCode = "";
                    }
                    data.TrackingCode = trackingCode;
                    (await _orderingTrackingCodeRepository.CreateSQLQuery("INSERT INTO Ordering_TrackingCode (TrackingCode,Section) VALUES ('" + trackingCode + "','ordering')").ConfigureAwait(false)).ExecuteUpdate();
                    var isContainer = data.Addresses.Where(a => a.AddressName == "#container#").ToList();
                    string containerTrackingCode = "";
                    if (isContainer.Count > 0)
                    {
                        while (containerTrackingCode == "")
                        {
                            containerTrackingCode = Guid.NewGuid().ToString().Replace("-", "").ToUpper().Remove(13);
                            var objordering = (await _orderingTrackingCodeRepository.FindByCondition(x => x.TrackingCode.ToUpper() == containerTrackingCode).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                            if (objordering != null)
                                containerTrackingCode = "";
                        }
                         (await _orderingTrackingCodeRepository.CreateSQLQuery("INSERT INTO Ordering_TrackingCode (TrackingCode,Section) VALUES ('" + containerTrackingCode + "','ordering')").ConfigureAwait(false)).ExecuteUpdate();
                    }

                    foreach (OrderingProduct orderingProduct in data.Products)
                    {
                        string trackingCode_deliver = "";
                        trackingCode = "";
                        while (trackingCode == "")
                        {
                            Guid tempTrackingCode = Guid.NewGuid();
                            trackingCode = tempTrackingCode.ToString().Replace("-", "").ToUpper().Remove(13);
                            trackingCode_deliver = tempTrackingCode.ToString().Replace("-", "").ToUpper().Remove(15);
                            var objordering = (await _orderingTrackingCodeRepository.FindByCondition(x => x.TrackingCode.ToUpper() == trackingCode).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                            if (objordering != null)
                                trackingCode = "";
                        }
                        int runNumber = 0;
                        foreach (OrderingAddress orderingAddress in data.Addresses)
                        {
                            runNumber = runNumber + 1;
                            foreach (OrderingAddressProduct addressProduct in orderingAddress.AddressProducts)
                            {
                                if (addressProduct.OrderingProduct == null)
                                {
                                    if (addressProduct.Name == "#container#")
                                    {
                                        addressProduct.TrackingCode = containerTrackingCode;
                                    }
                                    string section = "";
                                    if (orderingAddress.AddressType == "pickuppoint")
                                    {
                                        section = "pickuppoint-product";
                                    }
                                    else
                                    {
                                        section = "deliverypoint-product";
                                    }
                                    (await _orderingTrackingCodeRepository.CreateSQLQuery("INSERT INTO Ordering_TrackingCode (TrackingCode,Section) VALUES ('" + containerTrackingCode + "','" + section + "')").ConfigureAwait(false)).ExecuteUpdate();
                                }
                                else
                                {
                                    if (addressProduct.OrderingProduct.Id == orderingProduct.Id)
                                    {
                                        string section = "";
                                        if (orderingAddress.AddressType == "pickuppoint")
                                        {
                                            section = "pickuppoint-product";
                                            addressProduct.TrackingCode = trackingCode;
                                            (await _orderingTrackingCodeRepository.CreateSQLQuery("INSERT INTO Ordering_TrackingCode (TrackingCode,Section) VALUES ('" + trackingCode + "','" + section + "')").ConfigureAwait(false)).ExecuteUpdate();
                                        }
                                        else
                                        {
                                            section = "deliverypoint-product";
                                            addressProduct.TrackingCode = trackingCode_deliver + runNumber;
                                            (await _orderingTrackingCodeRepository.CreateSQLQuery("INSERT INTO Ordering_TrackingCode (TrackingCode,Section) VALUES ('" + trackingCode_deliver + runNumber + "','" + section + "')").ConfigureAwait(false)).ExecuteUpdate();
                                        }
                                    }
                                }
                            }
                        }
                    }
                    #endregion
                    #region
                    var cloneOrdering = _mapper.Map<OrderingViewModel>(data);

                    if (cloneOrdering.status != null && cloneOrdering.status != 0)
                    {
                        cloneOrdering.statusObj = orderingStatus.Where(o => o.id == cloneOrdering.status).FirstOrDefault();
                    }
                    foreach (OrderingProductViewModel productViewModel in cloneOrdering.products)
                    {
                        if (productViewModel.productType != null)
                        {
                            string[] productTypeIds = productViewModel.productType.Split(",");
                            productViewModel.productTypes = new List<ProductTypeViewModel>();
                            foreach (string productId in productTypeIds)
                            {
                                int id = 0;
                                if (int.TryParse(productId, out id))
                                {
                                    var productType = productTypes.Where(p => p.Id == id).FirstOrDefault();
                                    if (productType != null)
                                        productViewModel.productTypes.Add(_mapper.Map<ProductTypeViewModel>(productType));
                                }
                            }
                        }
                        if (productViewModel.packaging != null)
                        {
                            string[] packagingIds = productViewModel.packaging.Split(",");
                            productViewModel.packagings = new List<ProductPackagingViewModel>();
                            foreach (string packagingId in packagingIds)
                            {
                                int id = 0;
                                if (int.TryParse(packagingId, out id))
                                {
                                    var packaging = packagings.Where(p => p.Id == id).FirstOrDefault();
                                    if (packaging != null)
                                        productViewModel.packagings.Add(_mapper.Map<ProductPackagingViewModel>(packaging));
                                }
                            }
                        }
                    }

                    foreach (OrderingAddressViewModel orderingAddressViewModel in cloneOrdering.addresses)
                    {
                        foreach (OrderingAddressProductViewModel orderingAddressProductViewModel in orderingAddressViewModel.products)
                        {
                            if (orderingAddressProductViewModel.productType != null)
                            {
                                string[] productTypeIds = orderingAddressProductViewModel.productType.Split(",");
                                orderingAddressProductViewModel.productTypes = new List<ProductTypeViewModel>();
                                foreach (string productId in productTypeIds)
                                {
                                    int id = 0;
                                    if (int.TryParse(productId, out id))
                                    {
                                        var productType = productTypes.Where(p => p.Id == id).FirstOrDefault();
                                        if (productType != null)
                                            orderingAddressProductViewModel.productTypes.Add(_mapper.Map<ProductTypeViewModel>(productType));
                                    }
                                }
                            }
                            if (orderingAddressProductViewModel.packaging != null)
                            {
                                string[] packagingIds = orderingAddressProductViewModel.packaging.Split(",");
                                orderingAddressProductViewModel.packagings = new List<ProductPackagingViewModel>();
                                foreach (string packagingId in packagingIds)
                                {
                                    int id = 0;
                                    if (int.TryParse(packagingId, out id))
                                    {
                                        var packaging = packagings.Where(p => p.Id == id).FirstOrDefault();
                                        if (packaging != null)
                                            orderingAddressProductViewModel.packagings.Add(_mapper.Map<ProductPackagingViewModel>(packaging));
                                    }
                                }
                            }
                        }
                    }
                    #endregion
                    var oneSignalIds = (await _notificationUserRepository.FindByCondition(x => x.UserId.Equals(data.Driver.Id)).ConfigureAwait(false)).AsQueryable().Select(x => x.OneSignalId).ToList();
                    if (oneSignalIds.Count > 0)
                    {
                        Domain.Notification notification = new Domain.Notification();

                        notification.OneSignalId = oneSignalIds.FirstOrDefault();
                        notification.UserId = data.Driver.Id;
                        notification.OwnerId = request.CustomerId;
                        notification.Title = "Customer payment";
                        notification.Detail = "The customer the payment success.";
                        notification.ServiceCode = "Ordering";
                        //notification.ReferenceContentKey = request.ReferenceContentKey;
                        notification.Payload = Newtonsoft.Json.JsonConvert.SerializeObject(cloneOrdering);
                        notification.Created = DateTime.UtcNow;
                        notification.Modified = DateTime.UtcNow;
                        notification.CreatedBy = request.CustomerId.ToString();
                        notification.ModifiedBy = request.CustomerId.ToString();
                        await _notificationRepository.AddAsync(notification);


                        NotificationManager notiMgr = new NotificationManager(request.APP_ID, request.REST_API_KEY, request.AUTH_ID);
                        notiMgr.SendNotificationAsync("Customer payment", "The customer the payment success.", oneSignalIds, notification.ServiceCode,"", Newtonsoft.Json.JsonConvert.SerializeObject(cloneOrdering));
                    }
                }
                return new Response<Guid>(data.Id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
