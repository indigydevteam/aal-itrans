﻿using AutoMapper;
using iTrans.Transportation.Application.DTOs.OrderingAddress;
using iTrans.Transportation.Application.DTOs.OrderingAddressProduct;
using iTrans.Transportation.Application.DTOs.OrderingCar;
using iTrans.Transportation.Application.DTOs.OrderingContainer;
//using iTrans.Transportation.Application.DTOs.OrderingContainerAddress;
using iTrans.Transportation.Application.DTOs.OrderingProduct;
using iTrans.Transportation.Application.Enums;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using Microsoft.Extensions.Configuration;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using iTrans.Transportation.Application.DTOs.Ordering;
using iTrans.Transportation.Application.DTOs.OrderingStatus;
using iTrans.Transportation.Application.DTOs.ProductType;
using iTrans.Transportation.Application.DTOs.ProductPackaging;
using iTrans.Transportation.Application.DTOs.CarType;
using iTrans.Transportation.Application.DTOs.CarList;
using iTrans.Transportation.Application.Helper;
using NHibernate.Driver;
using iTrans.Transportation.Application.Features.Locations;
using System.Net;
using System.ComponentModel;
using Remotion.Linq.Clauses;

namespace iTrans.Transportation.Application.Features.Orderings.Commands
{
    public partial class UpdateOrderingCommand_Bk20220826 : IRequest<Response<Guid>>
    {
        public Guid? UserId { get; set; }
        public Guid Id { get; set; }
        public Guid CustomerId { get; set; }
        //public Guid DriverId { get; set; }
        public int DriverAnnouncementId { get; set; }
        ////public bool IsRequestTax { get; set; }
        ////public int TransportType { get; set; }
        //public CreateCarViweModel Cars { get; set; }
        public UpdateCarViweModel Cars { get; set; }
        public List<UpdateProductViewModel> Products { get; set; }
        public int ProductCBM { get; set; }
        public float ProductTotalWeight { get; set; }
        public string ProductAdditionalDetail { get; set; }
        public string ProductNote { get; set; }
        public decimal OrderingDesiredPrice { get; set; }
        public bool IsOrderingDriverOffer { get; set; }
        public bool IsMutipleRoutes { get; set; }
        public List<UpdateAddressViewModel> Addresses { get; set; }
        public List<UpdateContainerViewModel> Containers { get; set; }
        public string DRIVER_APP_ID { get; set; }
        public string DRIVER_REST_API_KEY { get; set; }
        public string DRIVER_AUTH_ID { get; set; }
    }
    public class UpdateOrderingCommand_Bk20220826Handler : IRequestHandler<UpdateOrderingCommand_Bk20220826, Response<Guid>>
    {
        private readonly ICustomerRepositoryAsync _customerRepository;
        //private readonly ICustomerLevelRepositoryAsync _customerLevelRepository;
        //private readonly IDriverRepositoryAsync _driverRepository;
        //private readonly IDriverAnnouncementRepositoryAsync _driverAnnouncementRepository;
        //private readonly ICustomerActivityRepositoryAsync _customerActivityRepository;
        private readonly ICustomerProductRepositoryAsync _customerProductRepository;
        private readonly IOrderingRepositoryAsync _orderingRepository;
        private readonly IOrderingProductRepositoryAsync _orderingProductRepository;
        private readonly IOrderingProductFileRepositoryAsync _orderingProductFileRepository;
        private readonly IOrderingCarRepositoryAsync _orderingCarRepository;
        private readonly IOrderingAddressRepositoryAsync _orderingAddressRepository;
        private readonly IOrderingAddressFileRepositoryAsync _orderingAddressFileRepository;
        private readonly IOrderingAddressProductRepositoryAsync _orderingAddressProductRepository;
        private readonly IOrderingAddressProductFileRepositoryAsync _orderingAddressProductFileRepository;
        private readonly IOrderingContainerRepositoryAsync _orderingContainerRepository;
        private readonly IOrderingContainerFileRepositoryAsync _orderingContainerFileRepository;

        private readonly IProductTypeRepositoryAsync _productTypeRepository;
        private readonly IProductPackagingRepositoryAsync _productPackagingRepository;

        private readonly IContainerTypeRepositoryAsync _containerTypeRepository;
        private readonly IContainerSpecificationRepositoryAsync _containerSpecificationRepository;
        private readonly IEnergySavingDeviceRepositoryAsync _energySavingDeviceRepository;

        private readonly ICarTypeRepositoryAsync _carTypeRepository;
        private readonly ICarListRepositoryAsync _carListRepository;
        private readonly ICarDescriptionRepositoryAsync _carDescriptionRepository;
        private readonly ICarSpecificationRepositoryAsync _carSpecificationRepository;
        private readonly ICarFeatureRepositoryAsync _carFeatureRepository;
        private readonly ICountryRepositoryAsync _countryRepository;
        private readonly IProvinceRepositoryAsync _provinceRepository;
        private readonly IDistrictRepositoryAsync _districtRepository;
        private readonly ISubdistrictRepositoryAsync _subdistrictRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;

        private readonly INotificationRepositoryAsync _notificationRepository;
        private readonly INotificationUserRepositoryAsync _notificationUserRepository;
        private readonly IOrderingHistoryRepositoryAsync _orderingHistoryRepository;
        private readonly IDriverInboxRepositoryAsync _driverInboxRepository;

        private readonly IMapper _mapper;
        private readonly IConfiguration _configuration;
        public UpdateOrderingCommand_Bk20220826Handler(ICustomerRepositoryAsync customerRepository, ICustomerProductRepositoryAsync customerProductRepository, IOrderingRepositoryAsync orderingRepository, IOrderingProductRepositoryAsync orderingProductRepository, IOrderingCarRepositoryAsync orderingCarRepository
            , IOrderingAddressRepositoryAsync orderingAddressRepository, IProductTypeRepositoryAsync productTypeRepository, IProductPackagingRepositoryAsync productPackagingRepository
            , ICarTypeRepositoryAsync carTypeRepository, ICarListRepositoryAsync carListRepository, ICarDescriptionRepositoryAsync carDescriptionRepository, ICarSpecificationRepositoryAsync carSpecificationRepository
            , ICarFeatureRepositoryAsync carFeatureRepository, ICountryRepositoryAsync countryRepository, IProvinceRepositoryAsync provinceRepository, IDistrictRepositoryAsync districtRepository
            , ISubdistrictRepositoryAsync subdistrictRepository, IApplicationLogRepositoryAsync applicationLogRepository
            , INotificationRepositoryAsync notificationRepository, INotificationUserRepositoryAsync notificationUserRepository, IContainerTypeRepositoryAsync containerTypeRepository, IContainerSpecificationRepositoryAsync containerSpecificationRepository, IEnergySavingDeviceRepositoryAsync energySavingDeviceRepository
            , IMapper mapper, IConfiguration configuration, IOrderingHistoryRepositoryAsync orderingHistoryRepository, IDriverInboxRepositoryAsync driverInboxRepository
            , IOrderingProductFileRepositoryAsync orderingProductFileRepository, IOrderingContainerRepositoryAsync orderingContainerRepository, IOrderingContainerFileRepositoryAsync orderingContainerFileRepository
            , IOrderingAddressProductFileRepositoryAsync orderingAddressProductFileRepository, IOrderingAddressProductRepositoryAsync orderingAddressProductRepository, IOrderingAddressFileRepositoryAsync orderingAddressFileRepository
            //, IDriverRepositoryAsync driverRepository, ICustomerActivityRepositoryAsync customerActivityRepository, IDriverAnnouncementRepositoryAsync driverAnnouncementRepository
            )
        {
            _customerRepository = customerRepository;
            //_customerLevelRepository = customerLevelRepository;
            // _driverRepository = driverRepository;
            //_driverAnnouncementRepository = driverAnnouncementRepository;
            _customerProductRepository = customerProductRepository;
            _orderingRepository = orderingRepository;
            _orderingProductRepository = orderingProductRepository;
            _orderingAddressRepository = orderingAddressRepository;
            _orderingCarRepository = orderingCarRepository;
            _orderingContainerRepository = orderingContainerRepository;
            _orderingContainerFileRepository = orderingContainerFileRepository;
            _productTypeRepository = productTypeRepository;
            _productPackagingRepository = productPackagingRepository;
            _carTypeRepository = carTypeRepository;
            _carListRepository = carListRepository;
            _carDescriptionRepository = carDescriptionRepository;
            _carSpecificationRepository = carSpecificationRepository;
            _carFeatureRepository = carFeatureRepository;
            _countryRepository = countryRepository;
            _provinceRepository = provinceRepository;
            _districtRepository = districtRepository;
            _subdistrictRepository = subdistrictRepository;
            _applicationLogRepository = applicationLogRepository;
            _containerTypeRepository = containerTypeRepository;
            _containerSpecificationRepository = containerSpecificationRepository;
            _energySavingDeviceRepository = energySavingDeviceRepository;
            _notificationRepository = notificationRepository;
            _notificationUserRepository = notificationUserRepository;
            _orderingHistoryRepository = orderingHistoryRepository;
            _driverInboxRepository = driverInboxRepository;
            //_customerActivityRepository = customerActivityRepository;
            _orderingProductFileRepository = orderingProductFileRepository;
            _orderingAddressProductFileRepository = orderingAddressProductFileRepository;
            _orderingAddressProductRepository = orderingAddressProductRepository;
            _orderingAddressFileRepository = orderingAddressFileRepository;
            _mapper = mapper;
            _configuration = configuration;

        }

        public async Task<Response<Guid>> Handle(UpdateOrderingCommand_Bk20220826 request, CancellationToken cancellationToken)
        {
            try
            {
                //var customerObject = (await _customerRepository.FindByCondition(x => x.Id.Equals(request.CustomerId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                //if (customerObject == null)
                //{
                //    throw new ApiException($"Customer Not Found.");
                //}

                var ordering = (await _orderingRepository.FindByCondition(x => x.Id == request.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (ordering == null)
                {
                    throw new ApiException($"Ordering Not Found.");
                }
                string currentTimeStr = DateTime.Now.ToString("yyyyMMddHHmmss");
                int detectArea = Convert.ToInt32(_configuration.GetSection("DetectArea").Value);
                string api_key = _configuration.GetSection("GoogleAPIKEY").Value;

                var carTypes = (await _carTypeRepository.FindByCondition(x => x.Active.Equals(true)).ConfigureAwait(false)).AsQueryable().ToList();
                var carSpecifications = (await _carSpecificationRepository.FindByCondition(x => x.Active.Equals(true)).ConfigureAwait(false)).AsQueryable().ToList();
                //var carLists = (await _carListRepository.FindByCondition(x => x.Active.Equals(true)).ConfigureAwait(false)).AsQueryable().ToList();
                //var carDescriptions = (await _carDescriptionRepository.FindByCondition(x => x.Active.Equals(true)).ConfigureAwait(false)).ToList().ToList();
                //var carFeatures = (await _carFeatureRepository.FindByCondition(x => x.Active.Equals(true)).ConfigureAwait(false)).AsQueryable().ToList();
                var productTypes = (await _productTypeRepository.FindByCondition(x => x.Active.Equals(true)).ConfigureAwait(false)).AsQueryable().ToList();
                var packagings = (await _productPackagingRepository.FindByCondition(x => x.Active.Equals(true)).ConfigureAwait(false)).AsQueryable().ToList();
                var containerTypes = (await _containerTypeRepository.FindByCondition(x => x.Active.Equals(true)).ConfigureAwait(false)).AsQueryable().ToList();
                var containerSpecifications = (await _containerSpecificationRepository.FindByCondition(x => x.Active.Equals(true)).ConfigureAwait(false)).AsQueryable().ToList();
                var energySavingDevices = (await _energySavingDeviceRepository.FindByCondition(x => x.Active.Equals(true)).ConfigureAwait(false)).AsQueryable().ToList();

                string orderStatusPath = $"Shared\\orderingstatus.json";
                var orderingStatusPathFile = Path.Combine(Directory.GetCurrentDirectory(), orderStatusPath);

                var statusJson = System.IO.File.ReadAllText(orderingStatusPathFile);
                List<OrderingStatusViewModel> orderingStatus = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OrderingStatusViewModel>>(statusJson);

                var getContentPath = _configuration.GetSection("ContentPath");
                string contentPath = getContentPath.Value; //"D://iTrans/content/";

                ////Ordering ordering = new Ordering();
                ////OrderingHistory orderingHistory = new OrderingHistory();
                ////ordering.Customer = customerObject;
                ////ordering.CustomerName_Str = customerObject.Name;
                ////ordering.IsRequestTax = request.IsRequestTax;
                ////ordering.TransportType = request.TransportType;
                //ordering.ProductCBM = request.ProductCBM;
                //ordering.ProductTotalWeight = request.ProductTotalWeight;
                //ordering.AdditionalDetail = request.ProductAdditionalDetail;
                //ordering.OrderingDesiredPrice = request.OrderingDesiredPrice;
                //ordering.IsOrderingDriverOffer = request.IsOrderingDriverOffer;
                //ordering.IsMutipleRoutes = request.IsMutipleRoutes;
                //ordering.Note = request.ProductNote;
                ////ordering.Created = DateTime.Now;
                ////ordering.CreatedBy = request.CustomerId != null ? request.CustomerId.ToString() : "";
                //ordering.Modified = DateTime.Now;
                //ordering.ModifiedBy = request.CustomerId != null ? request.CustomerId.ToString() : "";

                ////ordering.Status = 1;
                ////var orderingStatusObject = orderingStatus.Where(o => o.id == ordering.Status).FirstOrDefault();
                ////ordering.StatusName_Str = orderingStatusObject != null ? orderingStatusObject.th : "-";

                #region cars
                //if (request.Cars != null)
                //{
                //    var removeCars = ordering.Cars.Where(x => x.Id != request.Cars.Id).ToList();
                //    foreach (OrderingCar removeCar in removeCars)
                //    {
                //        ordering.Cars.Remove(removeCar);
                //        (await _orderingCarRepository.CreateSQLQuery("DELETE Ordering_car where Id = " + removeCar.Id.ToString()).ConfigureAwait(false)).ExecuteUpdate();
                //    }
                //    var car = ordering.Cars.Where(x => x.Id == request.Cars.Id).FirstOrDefault();
                //    if (car != null)
                //    {
                //        car.CarType = carTypes.Where(c => c.Id.Equals(request.Cars.CarTypeId)).FirstOrDefault();
                //        car.CarSpecification = carSpecifications.Where(c => c.Id.Equals(request.Cars.CarSpecificationId)).FirstOrDefault();
                //        car.EnergySavingDevice = energySavingDevices.Where(c => c.Id == request.Cars.EnergySavingDeviceId).FirstOrDefault();
                //        car.Width = request.Cars.Width;
                //        car.Length = request.Cars.Length;
                //        car.Height = request.Cars.Height;
                //        car.Temperature = request.Cars.Temperature;
                //        car.ProductInsurance = request.Cars.ProductInsurance;
                //        car.ProductInsuranceAmount = request.Cars.ProductInsuranceAmount;
                //        car.Note = request.Cars.Note;
                //        car.IsCharter = request.Cars.IsCharter;
                //        car.IsRequestTax = request.Cars.IsRequestTax;
                //        car.TransportType = request.Cars.TransportType;
                //        car.Modified = DateTime.Now;
                //        car.ModifiedBy = request.CustomerId != null ? request.CustomerId.ToString() : "";

                //        ordering.IsRequestTax = car.IsRequestTax;
                //        ordering.TransportType = car.TransportType;
                //    }
                //    else
                //    {
                //        if (ordering.Cars == null) ordering.Cars = new List<OrderingCar>();
                //        car = _mapper.Map<OrderingCar>(request.Cars);
                //        car.Id = 0;
                //        car.Ordering = ordering;
                //        car.CarType = carTypes.Where(c => c.Id.Equals(request.Cars.CarTypeId)).FirstOrDefault();
                //        car.CarSpecification = carSpecifications.Where(c => c.Id.Equals(request.Cars.CarSpecificationId)).FirstOrDefault();
                //        car.EnergySavingDevice = energySavingDevices.Where(c => c.Id == request.Cars.EnergySavingDeviceId).FirstOrDefault();
                //        ordering.IsRequestTax = car.IsRequestTax;
                //        ordering.TransportType = car.TransportType;
                //        car.Created = DateTime.Now;
                //        car.Modified = DateTime.Now;
                //        car.CreatedBy = request.CustomerId != null ? request.CustomerId.ToString() : "";
                //        car.ModifiedBy = request.CustomerId != null ? request.CustomerId.ToString() : "";
                //        ordering.Cars.Add(car);
                //    }
                //}
                //else
                //{
                //    (await _orderingCarRepository.CreateSQLQuery("DELETE Ordering_Car where OrderingId = '" + ordering.Id.ToString() + "'").ConfigureAwait(false)).ExecuteUpdate();
                //    for (int i = 0; i < ordering.Cars.Count(); i++)
                //    {
                //        ordering.Cars.RemoveAt(i);
                //    }
                //    ordering.Cars.Clear();
                //}
                #endregion

                #region product
                if (request.Products != null)
                {
                    if (ordering.Products == null) ordering.Products = new List<OrderingProduct>();
                    List<int> productIds = new List<int>();
                    foreach (UpdateProductViewModel product in request.Products)
                    {
                        productIds.Add(product.Id);
                    }
                    var removeProducts = ordering.Products.Where(p => !productIds.Contains(p.Id)).ToList();
                    foreach (OrderingProduct removeProduct in removeProducts)
                    {
                        foreach (OrderingProductFile productFile in removeProduct.ProductFiles)
                        {
                            if (File.Exists(Path.Combine(contentPath, productFile.FilePath)))
                                File.Delete(Path.Combine(contentPath, productFile.FilePath));
                        }
                        (await _orderingProductFileRepository.CreateSQLQuery("DELETE Ordering_ProductFile where OrderingProductId = " + removeProduct.Id.ToString()).ConfigureAwait(false)).ExecuteUpdate();

                        ordering.Products.Remove(removeProduct);
                        (await _orderingProductRepository.CreateSQLQuery("DELETE Ordering_Product where Id = " + removeProduct.Id.ToString()).ConfigureAwait(false)).ExecuteUpdate();
                    }
                    int productCount = ordering.Products.Count();
                    foreach (UpdateProductViewModel product in request.Products)
                    {
                        var existProduct = ordering.Products.Where(p => p.Id == product.Id).FirstOrDefault();
                        if (existProduct != null)
                        {
                            foreach (OrderingProductFile productFile in existProduct.ProductFiles)
                            {
                                if (File.Exists(Path.Combine(contentPath, productFile.FilePath)))
                                    File.Delete(Path.Combine(contentPath, productFile.FilePath));
                            }
                             (await _orderingProductFileRepository.CreateSQLQuery("DELETE Ordering_ProductFile where OrderingProductId = " + existProduct.Id.ToString()).ConfigureAwait(false)).ExecuteUpdate();

                            existProduct.Name = product.Name;
                            existProduct.Width = product.Width;
                            existProduct.Length = product.Length;
                            existProduct.Height = product.Height;
                            existProduct.Weight = product.Weight;
                            existProduct.Quantity = product.Quantity;
                            existProduct.Quantity = product.Quantity;
                            existProduct.Sequence = product.Sequence;
                            existProduct.ProductType = "";
                            foreach (int productId in product.ProductType)
                            {
                                var productType = productTypes.Where(p => p.Id == productId).FirstOrDefault();
                                if (productType != null)
                                    existProduct.ProductType = existProduct.ProductType + productId + ",";
                            }
                            if (existProduct.ProductType != "")
                                existProduct.ProductType = existProduct.ProductType.Remove(existProduct.ProductType.Length - 1);
                            existProduct.Packaging = "";
                            foreach (int packagingId in product.Packaging)
                            {
                                var packaging = packagings.Where(p => p.Id == packagingId).FirstOrDefault();
                                if (packaging != null)
                                    existProduct.Packaging = existProduct.Packaging + packagingId + ",";
                            }
                            if (existProduct.Packaging != "")
                                existProduct.Packaging = existProduct.Packaging.Remove(existProduct.Packaging.Length - 1);
                            string folderPath = contentPath + "ordering/" + request.CustomerId.ToString() + "/" + currentTimeStr + "/product/" + productCount + "/";
                            if (!Directory.Exists(folderPath))
                            {
                                Directory.CreateDirectory(folderPath);
                            }
                            int fileCount = 0;
                            List<OrderingProductFile> productFiles = new List<OrderingProductFile>();
                            if (product.Files != null)
                            {
                                foreach (IFormFile file in product.Files)
                                {
                                    fileCount = fileCount + 1;
                                    string fileName = file.FileName;
                                    string contentType = file.ContentType;
                                    if (fileName.Trim() == "" || fileName.ToLower().Trim() == "blob")
                                    {
                                        fileName = DateTime.Now.ToString("ddMMyyyyHHmmss") + fileCount.ToString("D" + 4) + ".jpg";
                                        contentType = "image/jpeg";
                                    }
                                    string filePath = Path.Combine(folderPath, fileName);
                                    if (File.Exists(filePath))
                                        File.Delete(filePath);

                                    using (FileStream fileStream = new FileStream(filePath, FileMode.Create))
                                    {
                                        await file.CopyToAsync(fileStream);
                                        FileInfo fi = new FileInfo(filePath);

                                        OrderingProductFile orderingProductFile = new OrderingProductFile
                                        {
                                            OrderingProduct = existProduct,
                                            FileName = fileName,
                                            ContentType = contentType,
                                            FilePath = "ordering/" + request.CustomerId.ToString() + "/" + currentTimeStr + "/product/" + productCount + "/" + fileName,
                                            DirectoryPath = "ordering/" + request.CustomerId.ToString() + "/" + currentTimeStr + "/product/" + productCount + "/",
                                            FileEXT = fi.Extension,
                                            Created = DateTime.Now,
                                            Modified = DateTime.Now,
                                            CreatedBy = request.CustomerId != null ? request.CustomerId.ToString() : "",
                                            ModifiedBy = request.CustomerId != null ? request.CustomerId.ToString() : "",
                                        };
                                        productFiles.Add(orderingProductFile);
                                    }
                                }
                            }
                            if (product.CustomerProductId != null && product.CustomerProductId != 0)
                            {
                                var customerProductObject = (await _customerProductRepository.FindByCondition(x => x.Id.Equals(product.CustomerProductId) && x.CustomerId.Equals(request.CustomerId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                                if (customerProductObject != null)
                                {
                                    foreach (CustomerProductFile productFile in customerProductObject.ProductFiles)
                                    {
                                        if (File.Exists(Path.Combine(contentPath, productFile.FilePath)))
                                        {
                                            string fileName = productFile.FileName;
                                            string contentType = productFile.ContentType;
                                            if (fileName.Trim() == "" || fileName.ToLower().Trim() == "blob")
                                            {
                                                fileName = DateTime.Now.ToString("ddMMyyyyHHmmss") + fileCount.ToString("D" + 4) + ".jpg";
                                                contentType = "image/jpeg";
                                            }
                                            string filePath = Path.Combine(folderPath, fileName);
                                            if (File.Exists(filePath))
                                                File.Delete(filePath);
                                            File.Copy(Path.Combine(contentPath, productFile.FilePath), filePath);
                                            OrderingProductFile orderingProductFile = new OrderingProductFile
                                            {
                                                OrderingProduct = existProduct,
                                                FileName = fileName,
                                                ContentType = contentType,
                                                FilePath = "ordering/" + request.CustomerId.ToString() + "/" + currentTimeStr + "/product/" + productCount + "/" + fileName,
                                                DirectoryPath = "ordering/" + request.CustomerId.ToString() + "/" + currentTimeStr + "/product/" + productCount + "/",
                                                FileEXT = productFile.FileEXT,
                                                Created = DateTime.Now,
                                                Modified = DateTime.Now,
                                                CreatedBy = request.CustomerId != null ? request.CustomerId.ToString() : "",
                                                ModifiedBy = request.CustomerId != null ? request.CustomerId.ToString() : "",
                                            };
                                            productFiles.Add(orderingProductFile);
                                        }
                                    }
                                }
                            }
                            existProduct.ProductFiles = productFiles;
                        }
                        else
                        {
                            productCount = productCount + 1;
                            existProduct = _mapper.Map<OrderingProduct>(product);
                            existProduct.Id = 0;
                            existProduct.Ordering = ordering;
                            existProduct.ProductType = "";
                            foreach (int productId in product.ProductType)
                            {
                                var productType = productTypes.Where(p => p.Id == productId).FirstOrDefault();
                                if (productType != null)
                                    existProduct.ProductType = existProduct.ProductType + productId + ",";
                            }
                            if (existProduct.ProductType != "")
                                existProduct.ProductType = existProduct.ProductType.Remove(existProduct.ProductType.Length - 1);
                            existProduct.Packaging = "";
                            foreach (int packagingId in product.Packaging)
                            {
                                var packaging = packagings.Where(p => p.Id == packagingId).FirstOrDefault();
                                if (packaging != null)
                                    existProduct.Packaging = existProduct.Packaging + packagingId + ",";
                            }
                            if (existProduct.Packaging != "")
                                existProduct.Packaging = existProduct.Packaging.Remove(existProduct.Packaging.Length - 1);
                            //product.ProductType = productTypes.Where(p => p.Id.Equals(productView.ProductTypeId)).FirstOrDefault();
                            //product.Packaging = packagings.Where(p => p.Id.Equals(productView.PackagingId)).FirstOrDefault();
                            string folderPath = contentPath + "ordering/" + request.CustomerId.ToString() + "/" + currentTimeStr + "/product/" + productCount + "/";
                            if (!Directory.Exists(folderPath))
                            {
                                Directory.CreateDirectory(folderPath);
                            }
                            int fileCount = 0;
                            List<OrderingProductFile> productFiles = new List<OrderingProductFile>();
                            if (product.Files != null)
                            {
                                foreach (IFormFile file in product.Files)
                                {
                                    fileCount = fileCount + 1;
                                    string fileName = file.FileName;
                                    string contentType = file.ContentType;
                                    if (fileName.Trim() == "" || fileName.ToLower().Trim() == "blob")
                                    {
                                        fileName = DateTime.Now.ToString("ddMMyyyyHHmmss") + fileCount.ToString("D" + 4) + ".jpg";
                                        contentType = "image/jpeg";
                                    }
                                    string filePath = Path.Combine(folderPath, fileName);
                                    if (File.Exists(filePath))
                                        File.Delete(filePath);

                                    using (FileStream fileStream = new FileStream(filePath, FileMode.Create))
                                    {
                                        await file.CopyToAsync(fileStream);
                                        FileInfo fi = new FileInfo(filePath);

                                        OrderingProductFile orderingProductFile = new OrderingProductFile
                                        {
                                            OrderingProduct = existProduct,
                                            FileName = fileName,
                                            ContentType = contentType,
                                            FilePath = "ordering/" + request.CustomerId.ToString() + "/" + currentTimeStr + "/product/" + productCount + "/" + fileName,
                                            DirectoryPath = "ordering/" + request.CustomerId.ToString() + "/" + currentTimeStr + "/product/" + productCount + "/",
                                            FileEXT = fi.Extension,
                                            Created = DateTime.Now,
                                            Modified = DateTime.Now,
                                            CreatedBy = request.CustomerId != null ? request.CustomerId.ToString() : "",
                                            ModifiedBy = request.CustomerId != null ? request.CustomerId.ToString() : "",
                                        };
                                        productFiles.Add(orderingProductFile);
                                    }
                                }
                            }
                            if (product.CustomerProductId != null && product.CustomerProductId != 0)
                            {
                                var customerProductObject = (await _customerProductRepository.FindByCondition(x => x.Id.Equals(product.CustomerProductId) && x.CustomerId.Equals(request.CustomerId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                                if (customerProductObject != null)
                                {
                                    foreach (CustomerProductFile productFile in customerProductObject.ProductFiles)
                                    {
                                        if (File.Exists(Path.Combine(contentPath, productFile.FilePath)))
                                        {
                                            string fileName = productFile.FileName;
                                            string contentType = productFile.ContentType;
                                            if (fileName.Trim() == "" || fileName.ToLower().Trim() == "blob")
                                            {
                                                fileName = DateTime.Now.ToString("ddMMyyyyHHmmss") + fileCount.ToString("D" + 4) + ".jpg";
                                                contentType = "image/jpeg";
                                            }
                                            string filePath = Path.Combine(folderPath, fileName);
                                            if (File.Exists(filePath))
                                                File.Delete(filePath);
                                            File.Copy(Path.Combine(contentPath, productFile.FilePath), filePath);
                                            OrderingProductFile orderingProductFile = new OrderingProductFile
                                            {
                                                OrderingProduct = existProduct,
                                                FileName = fileName,
                                                ContentType = contentType,
                                                FilePath = "ordering/" + request.CustomerId.ToString() + "/" + currentTimeStr + "/product/" + productCount + "/" + fileName,
                                                DirectoryPath = "ordering/" + request.CustomerId.ToString() + "/" + currentTimeStr + "/product/" + productCount + "/",
                                                FileEXT = productFile.FileEXT,
                                                Created = DateTime.Now,
                                                Modified = DateTime.Now,
                                                CreatedBy = request.CustomerId != null ? request.CustomerId.ToString() : "",
                                                ModifiedBy = request.CustomerId != null ? request.CustomerId.ToString() : "",
                                            };
                                            productFiles.Add(orderingProductFile);
                                        }
                                    }
                                }
                            }
                            existProduct.ProductFiles = productFiles;
                            ordering.Products.Add(existProduct);
                        }
                    }
                }
                else
                {
                    foreach (OrderingProduct product in ordering.Products)
                    {

                        foreach (OrderingProductFile productFile in product.ProductFiles)
                        {
                            if (File.Exists(Path.Combine(contentPath, productFile.FilePath)))
                                File.Delete(Path.Combine(contentPath, productFile.FilePath));
                        }
                        (await _orderingProductFileRepository.CreateSQLQuery("DELETE Ordering_ProductFile where OrderingProductId = " + product.Id.ToString()).ConfigureAwait(false)).ExecuteUpdate();
                    }
                    for (int i = 0; i < ordering.Products.Count; i++)
                    {
                        ordering.Products.RemoveAt(i);
                    }
                    ordering.Products.Clear();
                    (await _orderingProductRepository.CreateSQLQuery("DELETE Ordering_Product where OrderingId = " + request.Id.ToString()).ConfigureAwait(false)).ExecuteUpdate();
                }
                #endregion

                #region address
                if (request.Addresses != null)
                {
                    if (ordering.Addresses == null) ordering.Addresses = new List<OrderingAddress>();
                    List<int> addressIds = new List<int>();
                    foreach (UpdateAddressViewModel address in request.Addresses)
                    {
                        addressIds.Add(address.Id);
                    }
                    var removeAddresses = ordering.Addresses.Where(p => !addressIds.Contains(p.Id)).ToList();
                    //var removeAddressIds = new List<int>();
                    foreach (OrderingAddress removeAddress in removeAddresses)
                    {
                        foreach (OrderingAddressProduct addressProduct in removeAddress.AddressProducts)
                        {
                            foreach (OrderingAddressProductFile productFile in addressProduct.OrderingAddressProductFiles)
                            {
                                if (File.Exists(Path.Combine(contentPath, productFile.FilePath)))
                                    File.Delete(Path.Combine(contentPath, productFile.FilePath));
                            }
                            (await _orderingAddressProductFileRepository.CreateSQLQuery("DELETE Ordering_AddressProductFile where OrderingAddressProductId = " + addressProduct.Id.ToString()).ConfigureAwait(false)).ExecuteUpdate();
                            for (int i = 0; i < addressProduct.OrderingAddressProductFiles.Count; i++)
                            {
                                addressProduct.OrderingAddressProductFiles.RemoveAt(i);
                            }
                            addressProduct.OrderingAddressProductFiles.Clear();
                        }

                        (await _orderingAddressProductRepository.CreateSQLQuery("DELETE Ordering_AddressProduct where OrderingAddressId = " + removeAddress.Id.ToString()).ConfigureAwait(false)).ExecuteUpdate();
                        for (int i = 0; i < removeAddress.AddressProducts.Count; i++)
                        {
                            removeAddress.AddressProducts.RemoveAt(i);
                        }
                        removeAddress.AddressProducts.Clear();

                        (await _orderingAddressRepository.CreateSQLQuery("DELETE Ordering_Address where Id = " + removeAddress.Id.ToString()).ConfigureAwait(false)).ExecuteUpdate();
                        ordering.Addresses.Remove(removeAddress);
                    }

                    int addressCount = ordering.Addresses.Count;

                    foreach (UpdateAddressViewModel addressView in request.Addresses)
                    {
                        var address = ordering.Addresses.Where(a => a.Id == addressView.Id).FirstOrDefault();
                        if (address != null)
                        {
                            if (address.AddressProducts == null) address.AddressProducts = new List<OrderingAddressProduct>();

                            List<int> productIds = new List<int>();
                            foreach (UpdateProductFormViewModel product in addressView.Products)
                            {
                                productIds.Add(product.Id);
                            }
                            var removeProducts = address.AddressProducts.Where(p => !productIds.Contains(p.Id)).ToList();
                            foreach (OrderingAddressProduct removeProduct in removeProducts)
                            {
                                foreach (OrderingAddressProductFile productFile in removeProduct.OrderingAddressProductFiles)
                                {
                                    if (File.Exists(Path.Combine(contentPath, productFile.FilePath)))
                                        File.Delete(Path.Combine(contentPath, productFile.FilePath));
                                }
                                (await _orderingAddressProductFileRepository.CreateSQLQuery("DELETE Ordering_AddressProductFile where OrderingAddressProductId = " + removeProduct.Id.ToString()).ConfigureAwait(false)).ExecuteUpdate();
                                
                                address.AddressProducts.Remove(removeProduct);
                                (await _orderingAddressProductRepository.CreateSQLQuery("DELETE Ordering_AddressProduct where Id = " + removeProduct.Id.ToString()).ConfigureAwait(false)).ExecuteUpdate();
                            }

                            var productCount = address.AddressProducts.Count() + 1;
                            foreach (UpdateProductFormViewModel addressProductView in addressView.Products)
                            {
                                var addressProduct = address.AddressProducts.Where(x => x.Id == addressProductView.Id).FirstOrDefault();
                                if (addressProduct != null)
                                {
                                    foreach (OrderingAddressProductFile productFile in addressProduct.OrderingAddressProductFiles)
                                    {
                                        if (File.Exists(Path.Combine(contentPath, productFile.FilePath)))
                                            File.Delete(Path.Combine(contentPath, productFile.FilePath));
                                    }
                                    (await _orderingAddressProductFileRepository.CreateSQLQuery("DELETE Ordering_AddressProductFile where OrderingAddressProductId = " + addressProduct.Id.ToString()).ConfigureAwait(false)).ExecuteUpdate();
                                   
                                    var selProduct = request.Products.Where(p => p.Id == addressProductView.ProductId).FirstOrDefault();
                                    if (selProduct == null) selProduct = request.Products.Where(p => p.Sequence.Equals(addressProductView.Product)).FirstOrDefault();
                                    if (selProduct != null)
                                    {
                                        addressProduct = _mapper.Map<OrderingAddressProduct>(selProduct);
                                        addressProduct.ProductType = "";
                                        foreach (int productId in selProduct.ProductType)
                                        {
                                            var productType = productTypes.Where(p => p.Id == productId).FirstOrDefault();
                                            if (productType != null)
                                                addressProduct.ProductType = addressProduct.ProductType + productId + ",";
                                        }
                                        if (addressProduct.ProductType != "")
                                            addressProduct.ProductType = addressProduct.ProductType.Remove(addressProduct.ProductType.Length - 1);
                                        addressProduct.Packaging = "";
                                        foreach (int packagingId in selProduct.Packaging)
                                        {
                                            var packaging = packagings.Where(p => p.Id == packagingId).FirstOrDefault();
                                            if (packaging != null)
                                                addressProduct.Packaging = addressProduct.Packaging + packagingId + ",";
                                        }
                                        if (addressProduct.Packaging != "")
                                            addressProduct.Packaging = addressProduct.Packaging.Remove(addressProduct.Packaging.Length - 1);

                                        addressProduct.OrderingProduct = ordering.Products.Where(p => p.Sequence.Equals(addressProductView.Product)).FirstOrDefault();
                                        addressProduct.Quantity = addressProductView.Quantity;
                                        addressProduct.Sequence = addressProductView.Sequence;
                                        addressProduct.OrderingAddress = address;
                                        //addressProduct.ProductType = productTypes.Where(p => p.Id.Equals(selProduct.ProductTypeId)).FirstOrDefault();
                                        //addressProduct.Packaging = packagings.Where(p => p.Id.Equals(selProduct.PackagingId)).FirstOrDefault();
                                        address.AddressProducts.Add(addressProduct);
                                        string folderPath = contentPath + "ordering/" + request.CustomerId.ToString() + "/" + currentTimeStr + "/addressproduct/" + addressCount + "/" + productCount + "/";
                                        if (!Directory.Exists(folderPath))
                                        {
                                            Directory.CreateDirectory(folderPath);
                                        }
                                        int fileCount = 0;
                                        List<OrderingAddressProductFile> AddressProductFiles = new List<OrderingAddressProductFile>();
                                        if (selProduct.Files != null)
                                        {

                                            foreach (IFormFile file in selProduct.Files)
                                            {
                                                fileCount = fileCount + 1;
                                                string fileName = file.FileName;
                                                string contentType = file.ContentType;
                                                if (fileName.Trim() == "" || fileName.ToLower().Trim() == "blob")
                                                {
                                                    fileName = DateTime.Now.ToString("ddMMyyyyHHmmss") + fileCount.ToString("D" + 4) + ".jpg";
                                                    contentType = "image/jpeg";
                                                }
                                                string filePath = Path.Combine(folderPath, fileName);
                                                using (FileStream fileStream = new FileStream(filePath, FileMode.Create))
                                                {
                                                    await file.CopyToAsync(fileStream);
                                                    FileInfo fi = new FileInfo(filePath);

                                                    OrderingAddressProductFile addressProductFile = new OrderingAddressProductFile
                                                    {
                                                        OrderingAddressProduct = addressProduct,
                                                        FileName = fileName,
                                                        ContentType = contentType,
                                                        FilePath = "ordering/" + request.CustomerId.ToString() + "/" + currentTimeStr + "/addressproduct/" + addressCount + "/" + productCount + "/" + fileName,
                                                        DirectoryPath = "ordering/" + request.CustomerId.ToString() + "/" + currentTimeStr + "/addressproduct/" + addressCount + "/" + productCount + "/",
                                                        FileEXT = fi.Extension,
                                                        Type = "fromordering",
                                                        Created = DateTime.Now,
                                                        Modified = DateTime.Now,
                                                        CreatedBy = request.CustomerId != null ? request.CustomerId.ToString() : "",
                                                        ModifiedBy = request.CustomerId != null ? request.CustomerId.ToString() : "",
                                                    };
                                                    AddressProductFiles.Add(addressProductFile);
                                                }
                                            }
                                        }
                                        if (selProduct.CustomerProductId != null && selProduct.CustomerProductId != 0)
                                        {
                                            var customerProductObject = (await _customerProductRepository.FindByCondition(x => x.Id.Equals(selProduct.CustomerProductId) && x.CustomerId.Equals(request.CustomerId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                                            if (customerProductObject != null)
                                            {
                                                fileCount = 0;

                                                foreach (CustomerProductFile productFile in customerProductObject.ProductFiles)
                                                {
                                                    if (File.Exists(Path.Combine(contentPath, productFile.FilePath)))
                                                    {
                                                        string fileName = productFile.FileName;
                                                        string contentType = productFile.ContentType;
                                                        if (fileName.Trim() == "" || fileName.ToLower().Trim() == "blob")
                                                        {
                                                            fileName = DateTime.Now.ToString("ddMMyyyyHHmmss") + fileCount.ToString("D" + 4) + ".jpg";
                                                            contentType = "image/jpeg";
                                                        }
                                                        string filePath = Path.Combine(folderPath, fileName);
                                                        File.Copy(Path.Combine(contentPath, productFile.FilePath), filePath);
                                                        OrderingAddressProductFile addressProductFile = new OrderingAddressProductFile
                                                        {
                                                            OrderingAddressProduct = addressProduct,
                                                            FileName = fileName,
                                                            ContentType = contentType,
                                                            FilePath = "ordering/" + request.CustomerId.ToString() + "/" + currentTimeStr + "/addressproduct/" + addressCount + "/" + productCount + "/" + fileName,
                                                            DirectoryPath = "ordering/" + request.CustomerId.ToString() + "/" + currentTimeStr + "/addressproduct/" + addressCount + "/" + productCount + "/",
                                                            FileEXT = productFile.FileEXT,
                                                            Type = "fromordering",
                                                            Created = DateTime.Now,
                                                            Modified = DateTime.Now,
                                                            CreatedBy = request.CustomerId != null ? request.CustomerId.ToString() : "",
                                                            ModifiedBy = request.CustomerId != null ? request.CustomerId.ToString() : "",
                                                        };
                                                        AddressProductFiles.Add(addressProductFile);
                                                    }
                                                }
                                            }
                                        }
                                        addressProduct.OrderingAddressProductFiles = AddressProductFiles;
                                    }
                                }
                                else
                                {

                                    var selProduct = request.Products.Where(p => p.Sequence.Equals(addressProductView.Product)).FirstOrDefault();
                                    if (selProduct != null)
                                    {

                                        addressProduct = _mapper.Map<OrderingAddressProduct>(selProduct);

                                        addressProduct.ProductType = "";
                                        foreach (int productId in selProduct.ProductType)
                                        {
                                            var productType = productTypes.Where(p => p.Id == productId).FirstOrDefault();
                                            if (productType != null)
                                                addressProduct.ProductType = addressProduct.ProductType + productId + ",";
                                        }
                                        if (addressProduct.ProductType != "")
                                            addressProduct.ProductType = addressProduct.ProductType.Remove(addressProduct.ProductType.Length - 1);
                                        addressProduct.Packaging = "";
                                        foreach (int packagingId in selProduct.Packaging)
                                        {
                                            var packaging = packagings.Where(p => p.Id == packagingId).FirstOrDefault();
                                            if (packaging != null)
                                                addressProduct.Packaging = addressProduct.Packaging + packagingId + ",";
                                        }
                                        if (addressProduct.Packaging != "")
                                            addressProduct.Packaging = addressProduct.Packaging.Remove(addressProduct.Packaging.Length - 1);

                                        addressProduct.OrderingProduct = ordering.Products.Where(p => p.Sequence.Equals(addressProductView.Product)).FirstOrDefault();
                                        addressProduct.Quantity = addressProductView.Quantity;
                                        addressProduct.Sequence = addressProductView.Sequence;
                                        addressProduct.OrderingAddress = address;
                                        //addressProduct.ProductType = productTypes.Where(p => p.Id.Equals(selProduct.ProductTypeId)).FirstOrDefault();
                                        //addressProduct.Packaging = packagings.Where(p => p.Id.Equals(selProduct.PackagingId)).FirstOrDefault();
                                        address.AddressProducts.Add(addressProduct);
                                        string folderPath = contentPath + "ordering/" + request.CustomerId.ToString() + "/" + currentTimeStr + "/addressproduct/" + addressCount + "/" + productCount + "/";
                                        if (!Directory.Exists(folderPath))
                                        {
                                            Directory.CreateDirectory(folderPath);
                                        }
                                        int fileCount = 0;
                                        List<OrderingAddressProductFile> AddressProductFiles = new List<OrderingAddressProductFile>();
                                        if (selProduct.Files != null)
                                        {

                                            foreach (IFormFile file in selProduct.Files)
                                            {
                                                fileCount = fileCount + 1;
                                                string fileName = file.FileName;
                                                string contentType = file.ContentType;
                                                if (fileName.Trim() == "" || fileName.ToLower().Trim() == "blob")
                                                {
                                                    fileName = DateTime.Now.ToString("ddMMyyyyHHmmss") + fileCount.ToString("D" + 4) + ".jpg";
                                                    contentType = "image/jpeg";
                                                }
                                                string filePath = Path.Combine(folderPath, fileName);
                                                using (FileStream fileStream = new FileStream(filePath, FileMode.Create))
                                                {
                                                    await file.CopyToAsync(fileStream);
                                                    FileInfo fi = new FileInfo(filePath);

                                                    OrderingAddressProductFile addressProductFile = new OrderingAddressProductFile
                                                    {
                                                        OrderingAddressProduct = addressProduct,
                                                        FileName = fileName,
                                                        ContentType = contentType,
                                                        FilePath = "ordering/" + request.CustomerId.ToString() + "/" + currentTimeStr + "/addressproduct/" + addressCount + "/" + productCount + "/" + fileName,
                                                        DirectoryPath = "ordering/" + request.CustomerId.ToString() + "/" + currentTimeStr + "/addressproduct/" + addressCount + "/" + productCount + "/",
                                                        FileEXT = fi.Extension,
                                                        Type = "fromordering",
                                                        Created = DateTime.Now,
                                                        Modified = DateTime.Now,
                                                        CreatedBy = request.CustomerId != null ? request.CustomerId.ToString() : "",
                                                        ModifiedBy = request.CustomerId != null ? request.CustomerId.ToString() : "",
                                                    };
                                                    AddressProductFiles.Add(addressProductFile);
                                                }
                                            }
                                        }
                                        if (selProduct.CustomerProductId != null && selProduct.CustomerProductId != 0)
                                        {
                                            var customerProductObject = (await _customerProductRepository.FindByCondition(x => x.Id.Equals(selProduct.CustomerProductId) && x.CustomerId.Equals(request.CustomerId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                                            if (customerProductObject != null)
                                            {
                                                fileCount = 0;

                                                foreach (CustomerProductFile productFile in customerProductObject.ProductFiles)
                                                {
                                                    if (File.Exists(Path.Combine(contentPath, productFile.FilePath)))
                                                    {
                                                        string fileName = productFile.FileName;
                                                        string contentType = productFile.ContentType;
                                                        if (fileName.Trim() == "" || fileName.ToLower().Trim() == "blob")
                                                        {
                                                            fileName = DateTime.Now.ToString("ddMMyyyyHHmmss") + fileCount.ToString("D" + 4) + ".jpg";
                                                            contentType = "image/jpeg";
                                                        }
                                                        string filePath = Path.Combine(folderPath, fileName);
                                                        File.Copy(Path.Combine(contentPath, productFile.FilePath), filePath);
                                                        OrderingAddressProductFile addressProductFile = new OrderingAddressProductFile
                                                        {
                                                            OrderingAddressProduct = addressProduct,
                                                            FileName = fileName,
                                                            ContentType = contentType,
                                                            FilePath = "ordering/" + request.CustomerId.ToString() + "/" + currentTimeStr + "/addressproduct/" + addressCount + "/" + productCount + "/" + fileName,
                                                            DirectoryPath = "ordering/" + request.CustomerId.ToString() + "/" + currentTimeStr + "/addressproduct/" + addressCount + "/" + productCount + "/",
                                                            FileEXT = productFile.FileEXT,
                                                            Type = "fromordering",
                                                            Created = DateTime.Now,
                                                            Modified = DateTime.Now,
                                                            CreatedBy = request.CustomerId != null ? request.CustomerId.ToString() : "",
                                                            ModifiedBy = request.CustomerId != null ? request.CustomerId.ToString() : "",
                                                        };
                                                        AddressProductFiles.Add(addressProductFile);
                                                    }
                                                }
                                            }
                                        }
                                        addressProduct.OrderingAddressProductFiles = AddressProductFiles;
                                    }
                                }
                            }
                            address.AddressType = addressView.AddressType;
                            address.PersonalName = addressView.PersonalName;
                            address.PhoneCode = addressView.PhoneCode;
                            address.PhoneNumber = addressView.PhoneNumber;
                            address.Email = addressView.Email;
                            address.Country = (await _countryRepository.FindByCondition(x => x.Id.Equals(addressView.CountryId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                            address.Province = (await _provinceRepository.FindByCondition(x => x.Id.Equals(addressView.ProvinceId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                            address.District = (await _districtRepository.FindByCondition(x => x.Id.Equals(addressView.DistrictId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                            address.Subdistrict = (await _subdistrictRepository.FindByCondition(x => x.Id.Equals(addressView.SubdistrictId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                            address.PostCode = addressView.PostCode;
                            address.Road = addressView.Road;
                            address.Alley = addressView.Alley;
                            address.Address = addressView.Address;
                            address.Maps = addressView.Maps;
                            address.Date = addressView.Date;
                            address.Sequence = addressView.Sequence;
                            address.Note = addressView.Note;
                        }
                        else
                        {
                            addressCount = addressCount + 1;
                            address = _mapper.Map<OrderingAddress>(addressView);
                            address.Id = 0;
                            Country country = (await _countryRepository.FindByCondition(x => x.Id.Equals(addressView.CountryId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                            Province province = (await _provinceRepository.FindByCondition(x => x.Id.Equals(addressView.ProvinceId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                            District district = (await _districtRepository.FindByCondition(x => x.Id.Equals(addressView.DistrictId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                            Subdistrict subdistrict = (await _subdistrictRepository.FindByCondition(x => x.Id.Equals(addressView.SubdistrictId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();

                            string phoneNumber = addressView.PhoneNumber != null ? addressView.PhoneNumber.Trim() : "";
                            if (addressView.PhoneCode == "+66" && addressView.PhoneNumber != null && addressView.PhoneNumber.Length > 9 && addressView.PhoneNumber.First() == '0')
                            {
                                phoneNumber = addressView.PhoneNumber.Remove(0, 1);
                            }
                            addressView.PhoneNumber = phoneNumber;

                            if (address.AddressProducts == null) address.AddressProducts = new List<OrderingAddressProduct>();
                            if (addressView.Products != null)
                            {
                                int productCount = 0;
                                foreach (UpdateProductFormViewModel viewProduct in addressView.Products)
                                {
                                    var selProduct = request.Products.Where(p => p.Sequence.Equals(viewProduct.Product)).FirstOrDefault();
                                    if (selProduct != null)
                                    {
                                        productCount = productCount + 1;
                                        var addressProduct = _mapper.Map<OrderingAddressProduct>(selProduct);

                                        addressProduct.ProductType = "";
                                        foreach (int productId in selProduct.ProductType)
                                        {
                                            var productType = productTypes.Where(p => p.Id == productId).FirstOrDefault();
                                            if (productType != null)
                                                addressProduct.ProductType = addressProduct.ProductType + productId + ",";
                                        }
                                        if (addressProduct.ProductType != "")
                                            addressProduct.ProductType = addressProduct.ProductType.Remove(addressProduct.ProductType.Length - 1);
                                        addressProduct.Packaging = "";
                                        foreach (int packagingId in selProduct.Packaging)
                                        {
                                            var packaging = packagings.Where(p => p.Id == packagingId).FirstOrDefault();
                                            if (packaging != null)
                                                addressProduct.Packaging = addressProduct.Packaging + packagingId + ",";
                                        }
                                        if (addressProduct.Packaging != "")
                                            addressProduct.Packaging = addressProduct.Packaging.Remove(addressProduct.Packaging.Length - 1);

                                        addressProduct.OrderingProduct = ordering.Products.Where(p => p.Sequence.Equals(viewProduct.Product)).FirstOrDefault();
                                        addressProduct.Quantity = viewProduct.Quantity;
                                        addressProduct.Sequence = viewProduct.Sequence;
                                        addressProduct.OrderingAddress = address;
                                        //addressProduct.ProductType = productTypes.Where(p => p.Id.Equals(selProduct.ProductTypeId)).FirstOrDefault();
                                        //addressProduct.Packaging = packagings.Where(p => p.Id.Equals(selProduct.PackagingId)).FirstOrDefault();
                                        address.AddressProducts.Add(addressProduct);
                                        string folderPath = contentPath + "ordering/" + request.CustomerId.ToString() + "/" + currentTimeStr + "/addressproduct/" + addressCount + "/" + productCount + "/";
                                        if (!Directory.Exists(folderPath))
                                        {
                                            Directory.CreateDirectory(folderPath);
                                        }
                                        int fileCount = 0;
                                        List<OrderingAddressProductFile> AddressProductFiles = new List<OrderingAddressProductFile>();
                                        if (selProduct.Files != null)
                                        {

                                            foreach (IFormFile file in selProduct.Files)
                                            {
                                                fileCount = fileCount + 1;
                                                string fileName = file.FileName;
                                                string contentType = file.ContentType;
                                                if (fileName.Trim() == "" || fileName.ToLower().Trim() == "blob")
                                                {
                                                    fileName = DateTime.Now.ToString("ddMMyyyyHHmmss") + fileCount.ToString("D" + 4) + ".jpg";
                                                    contentType = "image/jpeg";
                                                }
                                                string filePath = Path.Combine(folderPath, fileName);
                                                using (FileStream fileStream = new FileStream(filePath, FileMode.Create))
                                                {
                                                    await file.CopyToAsync(fileStream);
                                                    FileInfo fi = new FileInfo(filePath);

                                                    OrderingAddressProductFile addressProductFile = new OrderingAddressProductFile
                                                    {
                                                        OrderingAddressProduct = addressProduct,
                                                        FileName = fileName,
                                                        ContentType = contentType,
                                                        FilePath = "ordering/" + request.CustomerId.ToString() + "/" + currentTimeStr + "/addressproduct/" + addressCount + "/" + productCount + "/" + fileName,
                                                        DirectoryPath = "ordering/" + request.CustomerId.ToString() + "/" + currentTimeStr + "/addressproduct/" + addressCount + "/" + productCount + "/",
                                                        FileEXT = fi.Extension,
                                                        Type = "fromordering",
                                                        Created = DateTime.Now,
                                                        Modified = DateTime.Now,
                                                        CreatedBy = request.CustomerId != null ? request.CustomerId.ToString() : "",
                                                        ModifiedBy = request.CustomerId != null ? request.CustomerId.ToString() : "",
                                                    };
                                                    AddressProductFiles.Add(addressProductFile);
                                                }
                                            }
                                        }
                                        if (selProduct.CustomerProductId != null && selProduct.CustomerProductId != 0)
                                        {
                                            var customerProductObject = (await _customerProductRepository.FindByCondition(x => x.Id.Equals(selProduct.CustomerProductId) && x.CustomerId.Equals(request.CustomerId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                                            if (customerProductObject != null)
                                            {
                                                fileCount = 0;

                                                foreach (CustomerProductFile productFile in customerProductObject.ProductFiles)
                                                {
                                                    if (File.Exists(Path.Combine(contentPath, productFile.FilePath)))
                                                    {
                                                        string fileName = productFile.FileName;
                                                        string contentType = productFile.ContentType;
                                                        if (fileName.Trim() == "" || fileName.ToLower().Trim() == "blob")
                                                        {
                                                            fileName = DateTime.Now.ToString("ddMMyyyyHHmmss") + fileCount.ToString("D" + 4) + ".jpg";
                                                            contentType = "image/jpeg";
                                                        }
                                                        string filePath = Path.Combine(folderPath, fileName);
                                                        File.Copy(Path.Combine(contentPath, productFile.FilePath), filePath);
                                                        OrderingAddressProductFile addressProductFile = new OrderingAddressProductFile
                                                        {
                                                            OrderingAddressProduct = addressProduct,
                                                            FileName = fileName,
                                                            ContentType = contentType,
                                                            FilePath = "ordering/" + request.CustomerId.ToString() + "/" + currentTimeStr + "/addressproduct/" + addressCount + "/" + productCount + "/" + fileName,
                                                            DirectoryPath = "ordering/" + request.CustomerId.ToString() + "/" + currentTimeStr + "/addressproduct/" + addressCount + "/" + productCount + "/",
                                                            FileEXT = productFile.FileEXT,
                                                            Type = "fromordering",
                                                            Created = DateTime.Now,
                                                            Modified = DateTime.Now,
                                                            CreatedBy = request.CustomerId != null ? request.CustomerId.ToString() : "",
                                                            ModifiedBy = request.CustomerId != null ? request.CustomerId.ToString() : "",
                                                        };
                                                        AddressProductFiles.Add(addressProductFile);
                                                    }
                                                }
                                            }
                                        }
                                        addressProduct.OrderingAddressProductFiles = AddressProductFiles;
                                    }

                                }
                            }
                            address.Ordering = ordering;
                            address.Country = country;
                            address.Province = province;
                            address.District = district;
                            address.Subdistrict = subdistrict;
                            address.Status = 1;
                            address.Created = DateTime.Now;
                            address.CreatedBy = request.CustomerId != null ? request.CustomerId.ToString() : "-";
                            address.Modified = DateTime.Now;
                            address.ModifiedBy = request.CustomerId != null ? request.CustomerId.ToString() : "-";
                            address.NotiAmount = 0;
                            ordering.Addresses.Add(address);
                        }
                    }


                    if (ordering.Addresses.Count() > 1)
                    {
                        var locationDistince = LocationHelper.GetDistanceAndEstimateTime(detectArea, api_key, ordering.Addresses[0].Maps, ordering.Addresses[ordering.Addresses.Count() - 1].Maps);
                        ordering.TotalDistance = locationDistince.Distance;
                        ordering.TotalEstimateTime = locationDistince.EstimateTime;
                        ordering.Distance = locationDistince.Distance;
                        ordering.EstimateTime = locationDistince.EstimateTime;
                    }

                    var firstPickupPoint = ordering.Addresses.Where(a => a.AddressType == "pickuppoint").OrderBy(a => a.Sequence).FirstOrDefault();
                    var lastDeliveryPoint = ordering.Addresses.Where(a => a.AddressType == "deliverypoint").OrderByDescending(a => a.Sequence).FirstOrDefault();
                    if (firstPickupPoint != null)
                    {
                        ordering.PickupPoint_Str = firstPickupPoint.Province != null ? firstPickupPoint.Province.Name_TH : "-";
                        ordering.PickupPointDate_Str = firstPickupPoint.Date;
                    }
                    if (lastDeliveryPoint != null)
                    {
                        ordering.RecipientName_Str = lastDeliveryPoint.PersonalName;
                        ordering.PickupPoint_Str = lastDeliveryPoint.Province != null ? lastDeliveryPoint.Province.Name_TH : "-";
                        ordering.PickupPointDate_Str = lastDeliveryPoint.Date;
                    }

                }
                else
                {
                    foreach (OrderingAddress address in ordering.Addresses)
                    {
                        foreach (OrderingAddressProduct product in address.AddressProducts)
                        {
                            foreach (OrderingAddressProductFile file in product.OrderingAddressProductFiles)
                            {
                                if (File.Exists(Path.Combine(contentPath, file.FilePath)))
                                    File.Delete(Path.Combine(contentPath, file.FilePath));
                            }
                            (await _orderingAddressProductFileRepository.CreateSQLQuery("DELETE Ordering_AddressProductFile where OrderingAddressProductId = " + product.Id.ToString()).ConfigureAwait(false)).ExecuteUpdate();
                            for (int i = 0; i < product.OrderingAddressProductFiles.Count; i++)
                            {
                                product.OrderingAddressProductFiles.RemoveAt(i);
                            }
                            product.OrderingAddressProductFiles.Clear();
                        }

                        for (int i = 0; i < address.AddressProducts.Count; i++)
                        {
                            address.AddressProducts.RemoveAt(i);
                        }
                        address.AddressProducts.Clear();
                        (await _orderingAddressProductRepository.CreateSQLQuery("DELETE Ordering_AddressProduct where OrderingAddressId = " + address.Id.ToString()).ConfigureAwait(false)).ExecuteUpdate();

                        foreach (OrderingAddressFile file in address.Files)
                        {
                            if (File.Exists(Path.Combine(contentPath, file.FilePath)))
                                File.Delete(Path.Combine(contentPath, file.FilePath));
                        }

                        for (int i = 0; i < address.Files.Count; i++)
                        {
                            address.Files.RemoveAt(i);
                        }
                        address.Files.Clear();
                        (await _orderingAddressFileRepository.CreateSQLQuery("DELETE Ordering_AddressFile where OrderingAddressId = " + address.Id.ToString()).ConfigureAwait(false)).ExecuteUpdate();
                    }

                    for (int i = 0; i < ordering.Addresses.Count; i++)
                    {
                        ordering.Addresses.RemoveAt(i);
                    }
                    ordering.Addresses.Clear();
                    (await _orderingAddressRepository.CreateSQLQuery("DELETE Ordering_Address where OrderingId = '" + ordering.Id.ToString() + "'").ConfigureAwait(false)).ExecuteUpdate();
                }
                #endregion

                #region container
                //if (request.Containers != null)
                //{
                //    if (ordering.Containers == null) ordering.Containers = new List<OrderingContainer>();
                //    List<int> containerIds = new List<int>();
                //    foreach (UpdateContainerViewModel address in request.Containers)
                //    {
                //        containerIds.Add(address.Id);
                //    }
                //    var removeContainers = ordering.Containers.Where(p => !containerIds.Contains(p.Id)).ToList();

                //    foreach (OrderingContainer removeContainer in removeContainers)
                //    {
                //        foreach (OrderingContainerFile file in removeContainer.Files)
                //        {
                //            if (File.Exists(Path.Combine(contentPath, file.FilePath)))
                //                File.Delete(Path.Combine(contentPath, file.FilePath));
                //        }
                //        for (int i = 0; i < removeContainer.Files.Count; i++)
                //        {
                //            removeContainer.Files.RemoveAt(i);
                //        }
                //        removeContainer.Files.Clear();
                //        (await _orderingProductFileRepository.CreateSQLQuery("DELETE Ordering_ContainerFile where OrderingContainerId = " + removeContainer.Id.ToString()).ConfigureAwait(false)).ExecuteUpdate();
                //        ordering.Containers.Remove(removeContainer);
                //        (await _orderingContainerRepository.CreateSQLQuery("DELETE Ordering_Container where Id = " + removeContainer.Id.ToString()).ConfigureAwait(false)).ExecuteUpdate();
                //    }

                //    foreach (UpdateContainerViewModel updateContainer in request.Containers)
                //    {
                //        var container = ordering.Containers.Where(x => x.Id == updateContainer.Id).FirstOrDefault();
                //        if (container != null)
                //        {
                //            foreach (OrderingContainerFile file in container.Files)
                //            {
                //                if (File.Exists(Path.Combine(contentPath, file.FilePath)))
                //                    File.Delete(Path.Combine(contentPath, file.FilePath));
                //            }
                //            (await _orderingProductFileRepository.CreateSQLQuery("DELETE Ordering_ContainerFile where OrderingContainerId = " + container.Id.ToString()).ConfigureAwait(false)).ExecuteUpdate();
                //            for(int i =0; i< container.Files.Count; i++)
                //            {
                //                container.Files.RemoveAt(i);
                //            }
                //            container.Files.Clear();
                //            container.Section = updateContainer.Section;
                //            container.ContainerBooking = updateContainer.ContainerBooking;
                //            container.Temperature = updateContainer.Temperature;
                //            container.Moisture = updateContainer.Moisture;
                //            container.Ventilation = updateContainer.Ventilation;
                //            container.SpecialSpecify = updateContainer.SpecialSpecify;
                //            container.Weight = updateContainer.Weight;
                //            container.Commodity = updateContainer.Commodity;
                //            container.DateOfBooking = updateContainer.DateOfBooking;
                //            container.PickupPointPostCode = updateContainer.PickupPointPostCode;
                //            container.PickupPointRoad = updateContainer.PickupPointRoad;
                //            container.PickupPointAlley = updateContainer.PickupPointAlley;
                //            container.PickupPointAddress = updateContainer.PickupPointAddress;
                //            container.PickupPointMaps = updateContainer.PickupPointMaps;
                //            container.PickupPointDate = updateContainer.PickupPointDate == null ?new DateTime(): updateContainer.PickupPointDate.Value;
                //            container.ReturnPointPostCode = updateContainer.ReturnPointPostCode;
                //            container.ReturnPointRoad = updateContainer.ReturnPointRoad;
                //            container.ReturnPointAlley = updateContainer.ReturnPointAlley;
                //            container.ReturnPointAddress = updateContainer.ReturnPointAddress;
                //            container.ReturnPointMaps = updateContainer.ReturnPointMaps;
                //            container.ReturnPointDate = updateContainer.ReturnPointDate == null ? new DateTime() : updateContainer.ReturnPointDate.Value;
                //            container.CutOffDate = updateContainer.CutOffDate;
                //            container.ShippingContact = updateContainer.ShippingContact;
                //            container.ShippingContactPhoneCode = updateContainer.ShippingContactPhoneCode;
                //            //container.ShippingContactPhoneNumber = updateContainer.ShippingContactPhoneNumber;
                //            container.YardContact = updateContainer.YardContact;
                //            container.YardContactPhoneCode = updateContainer.YardContactPhoneCode;
                //            //container.YardContactPhoneNumber = updateContainer.YardContactPhoneNumber;
                //            container.LinerContact = updateContainer.LinerContact;
                //            container.LinerContactPhoneCode = updateContainer.LinerContactPhoneCode;
                //            //container.LinerContactPhoneNumber = updateContainer.LinerContactPhoneNumber;
                //            container.DeliveryOrderNumber = updateContainer.DeliveryOrderNumber;
                //            container.BookingNumber = updateContainer.BookingNumber;
                //            container.ContainerAmount = updateContainer.ContainerAmount;
                //            container.SpecialOrder = updateContainer.SpecialOrder;

                //            string phoneNumber = updateContainer.ShippingContactPhoneNumber != null ? updateContainer.ShippingContactPhoneNumber : "";
                //            if (updateContainer.ShippingContactPhoneCode == "+66" && updateContainer.ShippingContactPhoneNumber != null && updateContainer.ShippingContactPhoneNumber.Length > 9 && updateContainer.ShippingContactPhoneNumber.First() == '0')
                //            {
                //                phoneNumber = updateContainer.ShippingContactPhoneNumber.Remove(0, 1);
                //            }
                //            updateContainer.ShippingContactPhoneNumber = phoneNumber;

                //            phoneNumber = updateContainer.YardContactPhoneNumber != null ? updateContainer.YardContactPhoneNumber : "";
                //            if (updateContainer.YardContactPhoneCode == "+66" && updateContainer.YardContactPhoneNumber != null && updateContainer.YardContactPhoneNumber.Length > 9 && updateContainer.YardContactPhoneNumber.First() == '0')
                //            {
                //                phoneNumber = updateContainer.YardContactPhoneNumber.Remove(0, 1);
                //            }
                //            updateContainer.YardContactPhoneNumber = phoneNumber;

                //            phoneNumber = updateContainer.LinerContactPhoneNumber != null ? updateContainer.LinerContactPhoneNumber : "";
                //            if (updateContainer.LinerContactPhoneCode == "+66" && updateContainer.LinerContactPhoneNumber != null && updateContainer.LinerContactPhoneNumber.Length > 9 && updateContainer.LinerContactPhoneNumber.First() == '0')
                //            {
                //                phoneNumber = updateContainer.LinerContactPhoneNumber.Remove(0, 1);
                //            }
                //            updateContainer.LinerContactPhoneNumber = phoneNumber;

                //            container.ContainerType = containerTypes.Where(c => c.Id == updateContainer.ContainerTypeId).FirstOrDefault();
                //            container.ContainerSpecification = containerSpecifications.Where(c => c.Id == updateContainer.ContainerSpecificationId).FirstOrDefault();
                //            container.EnergySavingDevice = energySavingDevices.Where(c => c.Id == updateContainer.EnergySavingDeviceId).FirstOrDefault();
                //            container.PickupPointCountry = (await _countryRepository.FindByCondition(x => x.Id == updateContainer.PickupPointCountryId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                //            container.PickupPointProvince = (await _provinceRepository.FindByCondition(x => x.Id == updateContainer.PickupPointProvinceId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                //            container.PickupPointDistrict = (await _districtRepository.FindByCondition(x => x.Id == updateContainer.PickupPointDistrictId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                //            container.PickupPointSubdistrict = (await _subdistrictRepository.FindByCondition(x => x.Id == updateContainer.PickupPointSubdistrictId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                //            container.ReturnPointCountry = (await _countryRepository.FindByCondition(x => x.Id == updateContainer.ReturnPointCountryId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                //            container.ReturnPointProvince = (await _provinceRepository.FindByCondition(x => x.Id == updateContainer.ReturnPointProvinceId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                //            container.ReturnPointDistrict = (await _districtRepository.FindByCondition(x => x.Id == updateContainer.ReturnPointDistrictId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                //            container.ReturnPointSubdistrict = (await _subdistrictRepository.FindByCondition(x => x.Id == updateContainer.ReturnPointSubdistrictId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                //            container.Modified = DateTime.Now;
                //            container.ModifiedBy = request.UserId != null ? request.UserId.ToString() : "";

                //            if (updateContainer.AttachedFiles != null && updateContainer.AttachedFiles.Count > 0)
                //            {
                //                if (container.Files == null)
                //                {
                //                    container.Files = new List<OrderingContainerFile>();
                //                }
                //                string folderPath = contentPath + "ordering/" + request.Id.ToString() + "/" + currentTimeStr + "/container/";
                //                if (!Directory.Exists(folderPath))
                //                {
                //                    Directory.CreateDirectory(folderPath);
                //                }
                //                int fileCount = 0;
                //                foreach (IFormFile file in updateContainer.AttachedFiles)
                //                {
                //                    fileCount = fileCount + 1;
                //                    string fileName = file.FileName;
                //                    string contentType = file.ContentType;
                //                    if (fileName.Trim() == "" || fileName.ToLower().Trim() == "blob")
                //                    {
                //                        fileName = DateTime.Now.ToString("ddMMyyyyHHmmss") + fileCount.ToString("D" + 4) + ".jpg";
                //                        contentType = "image/jpeg";
                //                    }
                //                    string filePath = Path.Combine(folderPath, fileName);
                //                    using (FileStream fileStream = new FileStream(filePath, FileMode.Create))
                //                    {
                //                        await file.CopyToAsync(fileStream);
                //                        FileInfo fi = new FileInfo(filePath);

                //                        OrderingContainerFile orderingContainerFile = new OrderingContainerFile
                //                        {
                //                            OrderingContainer = container,
                //                            FileName = fileName,
                //                            ContentType = contentType,
                //                            FilePath = "ordering/" + request.Id.ToString() + "/" + currentTimeStr + "/container/" + fileName,
                //                            DirectoryPath = "ordering/" + request.Id.ToString() + "/" + currentTimeStr + "/container/",
                //                            FileEXT = fi.Extension,
                //                            Created = DateTime.Now,
                //                            Modified = DateTime.Now,
                //                            CreatedBy = request.UserId != null ? request.UserId.ToString() : "",
                //                            ModifiedBy = request.UserId != null ? request.UserId.ToString() : "",
                //                        };
                //                        container.Files.Add(orderingContainerFile);
                //                    }
                //                }
                //            }
                //        }
                //        else
                //        {
                //            container = _mapper.Map<OrderingContainer>(updateContainer);
                //            container.Id = 0;
                //            container.Ordering = ordering;
                //            container.ContainerType = containerTypes.Where(c => c.Id == updateContainer.ContainerTypeId).FirstOrDefault();
                //            container.ContainerSpecification = containerSpecifications.Where(c => c.Id == updateContainer.ContainerSpecificationId).FirstOrDefault();
                //            container.EnergySavingDevice = energySavingDevices.Where(c => c.Id == updateContainer.EnergySavingDeviceId).FirstOrDefault();
                //            container.PickupPointCountry = (await _countryRepository.FindByCondition(x => x.Id == updateContainer.PickupPointCountryId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                //            container.PickupPointProvince = (await _provinceRepository.FindByCondition(x => x.Id == updateContainer.PickupPointProvinceId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                //            container.PickupPointDistrict = (await _districtRepository.FindByCondition(x => x.Id == updateContainer.PickupPointDistrictId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                //            container.PickupPointSubdistrict = (await _subdistrictRepository.FindByCondition(x => x.Id == updateContainer.PickupPointSubdistrictId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                //            container.ReturnPointCountry = (await _countryRepository.FindByCondition(x => x.Id == updateContainer.ReturnPointCountryId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                //            container.ReturnPointProvince = (await _provinceRepository.FindByCondition(x => x.Id == updateContainer.ReturnPointProvinceId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                //            container.ReturnPointDistrict = (await _districtRepository.FindByCondition(x => x.Id == updateContainer.ReturnPointDistrictId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                //            container.ReturnPointSubdistrict = (await _subdistrictRepository.FindByCondition(x => x.Id == updateContainer.ReturnPointSubdistrictId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                //            container.Created = DateTime.Now;
                //            container.Modified = DateTime.Now;
                //            container.CreatedBy = request.UserId != null ? request.UserId.ToString() : "";
                //            container.ModifiedBy = request.UserId != null ? request.UserId.ToString() : "";
                //            string phoneNumber = updateContainer.ShippingContactPhoneNumber != null ? updateContainer.ShippingContactPhoneNumber : "";
                //            if (updateContainer.ShippingContactPhoneCode == "+66" && updateContainer.ShippingContactPhoneNumber != null && updateContainer.ShippingContactPhoneNumber.Length > 9 && updateContainer.ShippingContactPhoneNumber.First() == '0')
                //            {
                //                phoneNumber = updateContainer.ShippingContactPhoneNumber.Remove(0, 1);
                //            }
                //            updateContainer.ShippingContactPhoneNumber = phoneNumber;

                //            phoneNumber = updateContainer.YardContactPhoneNumber != null ? updateContainer.YardContactPhoneNumber : "";
                //            if (updateContainer.YardContactPhoneCode == "+66" && updateContainer.YardContactPhoneNumber != null && updateContainer.YardContactPhoneNumber.Length > 9 && updateContainer.YardContactPhoneNumber.First() == '0')
                //            {
                //                phoneNumber = updateContainer.YardContactPhoneNumber.Remove(0, 1);
                //            }
                //            updateContainer.YardContactPhoneNumber = phoneNumber;

                //            phoneNumber = updateContainer.LinerContactPhoneNumber != null ? updateContainer.LinerContactPhoneNumber : "";
                //            if (updateContainer.LinerContactPhoneCode == "+66" && updateContainer.LinerContactPhoneNumber != null && updateContainer.LinerContactPhoneNumber.Length > 9 && updateContainer.LinerContactPhoneNumber.First() == '0')
                //            {
                //                phoneNumber = updateContainer.LinerContactPhoneNumber.Remove(0, 1);
                //            }
                //            updateContainer.LinerContactPhoneNumber = phoneNumber;

                //            if (updateContainer.AttachedFiles != null && updateContainer.AttachedFiles.Count > 0)
                //            {
                //                if (container.Files == null)
                //                {
                //                    container.Files = new List<OrderingContainerFile>();
                //                }
                //                string folderPath = contentPath + "ordering/" + request.Id.ToString() + "/" + currentTimeStr + "/container/";
                //                if (!Directory.Exists(folderPath))
                //                {
                //                    Directory.CreateDirectory(folderPath);
                //                }
                //                int fileCount = 0;
                //                foreach (IFormFile file in updateContainer.AttachedFiles)
                //                {
                //                    fileCount = fileCount + 1;
                //                    string fileName = file.FileName;
                //                    string contentType = file.ContentType;
                //                    if (fileName.Trim() == "" || fileName.ToLower().Trim() == "blob")
                //                    {
                //                        fileName = DateTime.Now.ToString("ddMMyyyyHHmmss") + fileCount.ToString("D" + 4) + ".jpg";
                //                        contentType = "image/jpeg";
                //                    }
                //                    string filePath = Path.Combine(folderPath, fileName);
                //                    using (FileStream fileStream = new FileStream(filePath, FileMode.Create))
                //                    {
                //                        await file.CopyToAsync(fileStream);
                //                        FileInfo fi = new FileInfo(filePath);

                //                        OrderingContainerFile orderingContainerFile = new OrderingContainerFile
                //                        {
                //                            OrderingContainer = container,
                //                            FileName = fileName,
                //                            ContentType = contentType,
                //                            FilePath = "ordering/" + request.Id.ToString() + "/" + currentTimeStr + "/container/" + fileName,
                //                            DirectoryPath = "ordering/" + request.Id.ToString() + "/" + currentTimeStr + "/container/",
                //                            FileEXT = fi.Extension,
                //                            Created = DateTime.Now,
                //                            Modified = DateTime.Now,
                //                            CreatedBy = request.UserId != null ? request.UserId.ToString() : "",
                //                            ModifiedBy = request.UserId != null ? request.UserId.ToString() : "",
                //                        };
                //                        container.Files.Add(orderingContainerFile);
                //                    }
                //                }
                //            }
                //            foreach (OrderingAddress addrss in ordering.Addresses)
                //            {
                //                foreach (OrderingAddressProduct product in addrss.AddressProducts)
                //                {
                //                    if (product.Name == "#container#")
                //                    {
                //                        product.OrderingContainer = container;
                //                    }
                //                }
                //            }

                //            ordering.Containers.Add(container);
                //        }
                //    }
                //}
                //else
                //{
                //    foreach (OrderingContainer container in ordering.Containers)
                //    {
                //        foreach (OrderingContainerFile file in container.Files)
                //        {
                //            if (File.Exists(Path.Combine(contentPath, file.FilePath)))
                //                File.Delete(Path.Combine(contentPath, file.FilePath));
                //        }
                //        (await _orderingContainerFileRepository.CreateSQLQuery("DELETE Ordering_ContainerFile where OrderingContainerId = " + container.Id.ToString()).ConfigureAwait(false)).ExecuteUpdate();
                //        for (int i = 0; i < container.Files.Count; i++)
                //        {
                //            container.Files.RemoveAt(i);
                //        }
                //        container.Files.Clear();
                //    }
                //    (await _orderingContainerRepository.CreateSQLQuery("DELETE Ordering_Container where OrderingId = '" + ordering.Id.ToString() +"'").ConfigureAwait(false)).ExecuteUpdate();
                //    for (int i = 0; i < ordering.Containers.Count; i++)
                //    {
                //        ordering.Containers.RemoveAt(i);
                //    }
                //    ordering.Containers.Clear();
                //}
                #endregion

                var dataObject = await _orderingRepository.UpdateAsync(ordering);

                #region
                //var driverObject = (await _driverRepository.FindByCondition(x => x.Id.Equals(request.DriverId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                //if(driverObject != null)
                //{
                //    OrderingDriverReserve driverReserve = new OrderingDriverReserve();

                //    driverReserve.Ordering = ordering;
                //    driverReserve.Driver = driverObject;
                //    driverReserve.Status = 1;
                //    driverReserve.Created = DateTime.Now;
                //    driverReserve.Modified = DateTime.Now;
                //    driverReserve.CreatedBy = request.CustomerId != null ? request.CustomerId.ToString() : "";
                //    driverReserve.ModifiedBy = request.CustomerId != null ? request.CustomerId.ToString() : "";
                //    if (ordering.DriverReserve == null)
                //    {
                //        ordering.DriverReserve = new List<OrderingDriverReserve>();
                //    }
                //    ordering.DriverReserve.Add(driverReserve);
                //}

                //orderingHistory.TrackingCode = ordering.TrackingCode;
                //orderingHistory.OrderingId = ordering.Id;
                //orderingHistory.Driver = ordering.Driver;
                //orderingHistory.Customer = ordering.Customer;
                //orderingHistory.ProductCBM = ordering.ProductCBM;
                //orderingHistory.ProductTotalWeight = ordering.ProductTotalWeight;
                //orderingHistory.OrderingPrice = ordering.OrderingPrice;
                //orderingHistory.OrderingStatus = ordering.Status;
                //orderingHistory.AdditionalDetail = ordering.AdditionalDetail;
                //orderingHistory.Note = ordering.Note;
                //orderingHistory.OrderNumber = ordering.TrackingCode;
                //orderingHistory.OrderingDesiredPrice = ordering.OrderingDesiredPrice;
                //orderingHistory.OrderingDriverOffering = ordering.OrderingDriverOffering;
                //orderingHistory.IsOrderingDriverOffer = ordering.IsOrderingDriverOffer;
                //orderingHistory.IsMutipleRoutes = ordering.IsMutipleRoutes;
                //orderingHistory.CancelStatus = ordering.CancelStatus;
                //orderingHistory.CustomerRanking = ordering.CustomerRanking;
                //orderingHistory.DriverRanking = ordering.DriverRanking;
                //orderingHistory.PinnedDate = ordering.PinnedDate;
                //orderingHistory.Distance = ordering.Distance;
                //orderingHistory.EstimateTime = ordering.EstimateTime;
                //orderingHistory.IsDriverPay = ordering.IsDriverPay;
                //orderingHistory.CurrentLocation = ordering.CurrentLocation;
                //orderingHistory.GPValue = ordering.GPValue;
                //orderingHistory.DriverPayValue = ordering.DriverPayValue;
                //orderingHistory.CustomerCancelValue = ordering.CustomerCancelValue;
                //orderingHistory.DriverCancelValue = ordering.DriverCancelValue;
                //orderingHistory.CustomerCashBack = ordering.CustomerCashBack;
                //orderingHistory.DriverCashBack = ordering.DriverCashBack;
                //orderingHistory.Insurances = ordering.Insurances;
                //orderingHistory.Payments = ordering.Payments;
                //orderingHistory.DriverAnnouncements = ordering.DriverAnnouncements;
                //orderingHistory.Created = DateTime.UtcNow;
                //orderingHistory.Modified = DateTime.UtcNow;
                //orderingHistory.CreatedBy = request.CustomerId != null ? request.CustomerId.ToString() : "";
                //orderingHistory.ModifiedBy = request.CustomerId != null ? request.CustomerId.ToString() : "";

                //await _orderingHistoryRepository.AddAsync(orderingHistory);

                //if (driverObject != null)
                //{
                //    #region
                //    var driverOrderingStatusFile = Path.Combine(Directory.GetCurrentDirectory(), $"Shared\\driver_orderingStatus.json");
                //    var driverOrderingStatusJson = System.IO.File.ReadAllText(driverOrderingStatusFile);
                //    List<OrderingStatusViewModel> driverOrderingStatus = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OrderingStatusViewModel>>(driverOrderingStatusJson);

                //    //var cloneOrdering = _mapper.Map<OrderingViewModel>(dataObject);
                //    var cloneOrdering = new OrderingNotificationViewModel
                //    {
                //        id = dataObject.Id,
                //        trackingCode = dataObject.TrackingCode,
                //        orderNumber = dataObject.TrackingCode,
                //        status = dataObject.Status,
                //        products = _mapper.Map<List<OrderingProductNotificationViewModel>>(dataObject.Products)
                //    };
                //    if (dataObject.Cars != null && dataObject.Cars.Count > 0)
                //    {
                //        cloneOrdering.carType = _mapper.Map<CarTypeViewModel>(dataObject.Cars[0].CarType);
                //        cloneOrdering.carList = _mapper.Map<CarListViewModel>(dataObject.Cars[0].CarList);
                //    }
                //    if (cloneOrdering.status != null)
                //    {
                //        cloneOrdering.statusObj = orderingStatus.Where(o => o.id == cloneOrdering.status).FirstOrDefault();
                //    }
                //    foreach (OrderingProductNotificationViewModel productViewModel in cloneOrdering.products)
                //    {
                //        if (productViewModel.productType != null)
                //        {
                //            string[] productTypeIds = productViewModel.productType.Split(",");
                //            productViewModel.productTypes = new List<ProductTypeViewModel>();
                //            foreach (string productId in productTypeIds)
                //            {
                //                int id = 0;
                //                if (int.TryParse(productId, out id))
                //                {
                //                    var productType = productTypes.Where(p => p.Id == id).FirstOrDefault();
                //                    if (productType != null)
                //                        productViewModel.productTypes.Add(_mapper.Map<ProductTypeViewModel>(productType));
                //                }
                //            }
                //        }
                //        if (productViewModel.packaging != null)
                //        {
                //            string[] packagingIds = productViewModel.packaging.Split(",");
                //            productViewModel.packagings = new List<ProductPackagingViewModel>();
                //            foreach (string packagingId in packagingIds)
                //            {
                //                int id = 0;
                //                if (int.TryParse(packagingId, out id))
                //                {
                //                    var packaging = packagings.Where(p => p.Id == id).FirstOrDefault();
                //                    if (packaging != null)
                //                        productViewModel.packagings.Add(_mapper.Map<ProductPackagingViewModel>(packaging));
                //                }
                //            }
                //        }

                //    }
                //    if (dataObject.Addresses != null && dataObject.Addresses.Count > 0)
                //    {
                //        cloneOrdering.startPoint = _mapper.Map<OrderingAddressNotificationViewModel>(dataObject.Addresses[0]);
                //        cloneOrdering.endPoint = _mapper.Map<OrderingAddressNotificationViewModel>(dataObject.Addresses[dataObject.Addresses.Count - 1]);
                //    }

                //    #endregion
                //    //#region add noti list
                //    {
                //        if (cloneOrdering.status != null)
                //        {
                //            cloneOrdering.statusObj = driverOrderingStatus.Where(o => o.id == cloneOrdering.status).FirstOrDefault();
                //        }
                //        var driverSignalIds = (await _notificationUserRepository.FindByCondition(x => x.UserId.Equals(driverObject.Id)).ConfigureAwait(false)).AsQueryable().Select(x => x.OneSignalId).ToList();
                //        var notificationDetails = Path.Combine(Directory.GetCurrentDirectory(), $"Shared\\NotificationWord.json");
                //        var notificationJson = System.IO.File.ReadAllText(notificationDetails);
                //        List<NotificationWordStatusViewModel> notificationStatus = Newtonsoft.Json.JsonConvert.DeserializeObject<List<NotificationWordStatusViewModel>>(notificationJson);
                //        string title = "";
                //        string detail = "";
                //        var notificationObj = notificationStatus.Where(o => o.id == 1).FirstOrDefault();
                //        if (notificationObj != null)
                //        {
                //            title = notificationObj.title.th;
                //            if (ordering.Customer != null)
                //            {
                //                detail = notificationObj.detail.th.Replace("#customertitle#", ordering.Customer.Title != null ? ordering.Customer.Title.Name_TH : "")
                //                         .Replace("#customername#", ordering.Customer.Name)
                //                         .Replace("#orderprice#", ordering.OrderingDesiredPrice != null || ordering.OrderingDesiredPrice != 0 ? ordering.OrderingDesiredPrice.ToString() : "-");
                //            }
                //        }
                //        //if (cloneOrdering.products.Count > 2)
                //        //{
                //        //    cloneOrdering.products.RemoveRange(2, cloneOrdering.products.Count - 2);
                //        //}
                //        Domain.Notification notification = new Domain.Notification();
                //        notification.OneSignalId = driverSignalIds.FirstOrDefault();
                //        notification.UserId = driverObject.Id;
                //        notification.OwnerId = request.CustomerId;
                //        notification.Title = title;
                //        notification.Detail = detail;
                //        notification.ServiceCode = "Ordering";
                //        //notification.ReferenceContentKey = request.ReferenceContentKey;
                //        notification.Payload = Newtonsoft.Json.JsonConvert.SerializeObject(cloneOrdering);
                //        notification.Created = DateTime.UtcNow;
                //        notification.Modified = DateTime.UtcNow;
                //        notification.CreatedBy = request.CustomerId.ToString();
                //        notification.ModifiedBy = request.CustomerId.ToString();
                //        await _notificationRepository.AddAsync(notification);
                //        #region insert to Inbox
                //        DriverInbox driverInbox = new DriverInbox()
                //        {
                //            DriverId = driverObject.Id,
                //            Title = "-",
                //            Title_ENG = "-",
                //            Module = "การแจ้งเตือน",
                //            Module_ENG = "Notification",
                //            Content = Newtonsoft.Json.JsonConvert.SerializeObject(cloneOrdering),
                //            FromUser = customerObject.FirstName + customerObject.LastName,
                //            IsDelete = false,
                //            Created = DateTime.Now
                //        };
                //        await _driverInboxRepository.AddAsync(driverInbox);
                //        #endregion
                //        driverSignalIds = driverSignalIds.Where(x => x.Trim() != "").ToList();
                //        if (driverSignalIds.Count > 0)
                //        {
                //            NotificationManager notiMgr = new NotificationManager(request.DRIVER_APP_ID, request.DRIVER_REST_API_KEY, request.DRIVER_AUTH_ID);
                //            notiMgr.SendNotificationAsync(title, detail, driverSignalIds, notification.ServiceCode, "", "{id:'" + cloneOrdering.id + "'}");
                //        }
                //    }
                //    //#endregion
                //}
                #endregion
                var log = new CreateAppLog(_applicationLogRepository);
                log.Create("Ordering", "Ordering", "Update", Newtonsoft.Json.JsonConvert.SerializeObject(request));
                return new Response<Guid>(dataObject.Id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}

