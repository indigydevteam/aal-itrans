﻿using AutoMapper;
using iTrans.Transportation.Application.DTOs.CarList;
using iTrans.Transportation.Application.DTOs.CarType;
using iTrans.Transportation.Application.DTOs.Ordering;
using iTrans.Transportation.Application.DTOs.OrderingAddress;
using iTrans.Transportation.Application.DTOs.OrderingAddressProduct;
using iTrans.Transportation.Application.DTOs.OrderingInsurance;
using iTrans.Transportation.Application.DTOs.OrderingProduct;
using iTrans.Transportation.Application.DTOs.OrderingStatus;
using iTrans.Transportation.Application.DTOs.ProductPackaging;
using iTrans.Transportation.Application.DTOs.ProductType;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.Orderings.Commands
{
    public class CustomerPaymentCommandBK : IRequest<Response<Guid>>
    {
        public Guid CustomerId { get; set; }
        public Guid DriverId { get; set; }
        public Guid OrderingId { get; set; }
        public int DriverReserveId { get; set; }
        public string Channel { get; set; }
        public decimal Amount { get; set; }
        //public CreateInsuranceViewModel Insurance { get; set; }
        public int Status { get; set; }
        public string APP_ID { get; set; }
        public string REST_API_KEY { get; set; }
        public string AUTH_ID { get; set; }

        public string DRIVER_APP_ID { get; set; }
        public string DRIVER_REST_API_KEY { get; set; }
        public string DRIVER_AUTH_ID { get; set; }
    }
    public class CustomerPaymentCommandBKHandler : IRequestHandler<CustomerPaymentCommandBK, Response<Guid>>
    {
        private readonly IOrderingRepositoryAsync _orderingRepository;
        private readonly IOrderingDriverRepositoryAsync _orderingDriverRepository;
        private readonly IOrderingDriverFileRepositoryAsync _orderingDriverFileRepository;
        private readonly IOrderingCarRepositoryAsync _orderingCarRepository;
        private readonly IOrderingCarFileRepositoryAsync _orderingCarFileRepository;
        private readonly IOrderingDriverReserveRepositoryAsync _orderingDriverReserveRepository;
        private readonly INotificationRepositoryAsync _notificationRepository;
        private readonly INotificationUserRepositoryAsync _notificationUserRepository;
        private readonly IOrderingTrackingCodeRepositoryAsync _orderingTrackingCodeRepository;
        private readonly IProductTypeRepositoryAsync _productTypeRepository;
        private readonly IProductPackagingRepositoryAsync _productPackagingRepository;
        private readonly IDriverRepositoryAsync _driverRepository;
        private readonly IDriverAnnouncementRepositoryAsync _driverAnnouncementRepository;
        private readonly IMapper _mapper;
        private readonly IConfiguration _configuration;
        private readonly IDummyWalletRepositoryAsync _dummyWalletRepository;

        public CustomerPaymentCommandBKHandler(IOrderingRepositoryAsync orderingRepository, IOrderingDriverRepositoryAsync orderingDriverRepository, IOrderingDriverFileRepositoryAsync orderingDriverFileRepository
            , IOrderingCarRepositoryAsync orderingCarRepository, IOrderingCarFileRepositoryAsync orderingCarFileRepository, IOrderingDriverReserveRepositoryAsync orderingDriverReserveRepository
            , INotificationRepositoryAsync notificationRepository, INotificationUserRepositoryAsync notificationUserRepository, IOrderingTrackingCodeRepositoryAsync orderingTrackingCodeRepository
            , IProductTypeRepositoryAsync productTypeRepository, IProductPackagingRepositoryAsync productPackagingRepository, IDriverRepositoryAsync driverRepository, IDriverAnnouncementRepositoryAsync driverAnnouncementRepository
            , IMapper mapper, IConfiguration configuration, IDummyWalletRepositoryAsync dummyWalletRepository
            )
        {
            _orderingRepository = orderingRepository;
            _orderingDriverRepository = orderingDriverRepository;
            _orderingDriverFileRepository = orderingDriverFileRepository;
            _orderingCarRepository = orderingCarRepository;
            _orderingCarFileRepository = orderingCarFileRepository;
            _orderingDriverReserveRepository = orderingDriverReserveRepository;
            _notificationRepository = notificationRepository;
            _notificationUserRepository = notificationUserRepository;
            _orderingTrackingCodeRepository = orderingTrackingCodeRepository;
            _productTypeRepository = productTypeRepository;
            _productPackagingRepository = productPackagingRepository;
            _driverRepository = driverRepository;
            _driverAnnouncementRepository = driverAnnouncementRepository;
            _mapper = mapper;
            _configuration = configuration;
            _dummyWalletRepository = dummyWalletRepository;
        }

        public async Task<Response<Guid>> Handle(CustomerPaymentCommandBK request, CancellationToken cancellationToken)
        {
            try
            {
                var data = (await _orderingRepository.FindByCondition(x => x.Id == request.OrderingId && x.Customer.Id == request.CustomerId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (data == null)
                {
                    throw new ApiException($"Ordering Not Found.");
                }
                if (data.Status != 2)
                {
                    throw new ApiException($"Ordering Not allow accept.");
                }
                //var wallet = (await _dummyWalletRepository.FindByCondition(x => x.UserId == request.CustomerId && x.Module == "customer").ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                //if (wallet == null || wallet.Amount < request.Amount)
                //{
                //    throw new ApiException($"Your wallet don't have enough.");
                //}
                
                var getContentPath = _configuration.GetSection("ContentPath");
                string contentPath = getContentPath.Value;
                string currentTimeStr = DateTime.Now.ToString("yyyyMMddHHmmss");
                var driverObject = (await _driverRepository.FindByCondition(x => x.Id.Equals(request.DriverId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (driverObject == null)
                {
                    throw new ApiException($"Driver Not Found.");
                }
                //var driverAnnouncement = (await _driverAnnouncementRepository.FindByCondition(x => x.OrderId.Equals(request.OrderingId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                //if (request.Status != 0)
                //{
                //    if (driverAnnouncement != null)
                //    {

                //        driverAnnouncement.Status = request.Status;
                //        await _driverAnnouncementRepository.UpdateAsync(driverAnnouncement);

                //    }
                //    data.Status = request.Status;
                //}

                var productTypes = (await _productTypeRepository.FindByCondition(x => x.Active.Equals(true)).ConfigureAwait(false)).AsQueryable().ToList();
                var packagings = (await _productPackagingRepository.FindByCondition(x => x.Active.Equals(true)).ConfigureAwait(false)).AsQueryable().ToList();
                var folderDetails = Path.Combine(Directory.GetCurrentDirectory(), $"Shared\\OrderingStatus.json");
                var statusJson = System.IO.File.ReadAllText(folderDetails);
                List<OrderingStatusViewModel> orderingStatus = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OrderingStatusViewModel>>(statusJson);
                data.OrderingPrice = request.Amount;
                data.Driver = driverObject;
                //var orderingDriverReserve = (await _orderingDriverReserveRepository.FindByCondition(x => x.Id == request.DriverReserveId && x.Ordering.Id == request.OrderingId && x.Driver.Id == request.DriverId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                //if (orderingDriverReserve != null && data.IsOrderingDriverOffer == true)
                //{
                //    data.OrderingPrice = orderingDriverReserve.DriverOfferingPrice;
                //}
                IList<OrderingDriver> orderingDrivers = new List<OrderingDriver>();
                if (data.Drivers.Count() > 0)
                {
                    foreach (OrderingDriver driver in data.Drivers)
                    {

                        foreach (OrderingDriverFile orderingDriverFile in driver.DriverFiles)
                        {
                            if (File.Exists(Path.Combine(contentPath, orderingDriverFile.FilePath)))
                            {
                                File.Delete(Path.Combine(contentPath, orderingDriverFile.FilePath));
                            }
                            (await _orderingDriverFileRepository.CreateSQLQuery("DELETE Ordering_DriverFile where Id = '" + orderingDriverFile.Id.ToString() + "'").ConfigureAwait(false)).ExecuteUpdate();
                            //await _orderingDriverFileRepository.DeleteAsync(orderingDriverFile);
                        }
                        driver.DriverFiles.Clear();
                        (await _orderingDriverRepository.CreateSQLQuery("DELETE Ordering_Driver where Id = '" + driver.Id.ToString() + "'").ConfigureAwait(false)).ExecuteUpdate();
                        //await _orderingDriverRepository.DeleteAsync(driver);
                    }
                    data.Drivers.Clear();
                }

                OrderingDriver orderingDriver = new OrderingDriver
                {
                    Ordering = data,
                    DriverId = driverObject.Id,
                    DriverType = driverObject.DriverType,
                    Title = driverObject.Title,
                    FirstName = driverObject.FirstName,
                    MiddleName = driverObject.MiddleName,
                    LastName = driverObject.LastName,
                    Name = driverObject.Name,
                    IdentityNumber = driverObject.IdentityNumber,
                    PhoneCode = driverObject.PhoneCode,
                    PhoneNumber = driverObject.PhoneNumber,
                    Email = driverObject.Email,
                    Facbook = driverObject.Facbook,
                    Line = driverObject.Line,
                    Twitter = driverObject.Twitter,
                    Whatapp = driverObject.Whatapp,
                    Level = driverObject.Level,
                    Rating = driverObject.Rating,
                    Corparate = driverObject.Corparate,
                    Star = driverObject.Star,
                };
                string folderPath = contentPath + "ordering/" + request.DriverId.ToString() + "/" + currentTimeStr + "/orderingdriver/";
                if (!Directory.Exists(folderPath))
                {
                    Directory.CreateDirectory(folderPath);
                }
                int fileCount = 0;
                if (driverObject.Files != null)
                {
                    List<OrderingDriverFile> orderingDriverFiles = new List<OrderingDriverFile>();
                    foreach (DriverFile driverFile in driverObject.Files)
                    {
                        //if (driverFile.DocumentType == "DriverPicture" || driverFile.DocumentType == "profilepicture")
                        //{
                        fileCount = fileCount + 1;

                        string desPath = Path.Combine(folderPath, driverFile.FileName);
                        if (File.Exists(Path.Combine(contentPath, driverFile.FilePath)))
                        {
                            File.Copy(Path.Combine(contentPath, driverFile.FilePath), desPath);
                            OrderingDriverFile orderingDriverFile = new OrderingDriverFile
                            {
                                OrderingDriver = orderingDriver,
                                FileName = driverFile.FileName,
                                ContentType = driverFile.ContentType,
                                DocumentType = driverFile.DocumentType,
                                FilePath = "ordering/" + request.DriverId.ToString() + "/" + currentTimeStr + "/orderingdriver/" + driverFile.FileName,
                                DirectoryPath = "ordering/" + request.DriverId.ToString() + "/" + currentTimeStr + "/orderingdriver/",
                                FileEXT = driverFile.FileEXT

                            };
                            orderingDriverFiles.Add(orderingDriverFile);
                        }
                        //}
                    }
                    orderingDriver.DriverFiles = orderingDriverFiles;
                }
                orderingDrivers.Add(orderingDriver);
                data.Drivers = orderingDrivers;

                var orderingcar = (await _orderingCarRepository.FindByCondition(x => x.Ordering.Id.Equals(request.OrderingId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                var driverCars = _mapper.Map<IList<DriverCar>>(driverObject.Cars);
                IList<OrderingCar> orderingCars = new List<OrderingCar>();// _mapper.Map<IList<OrderingCar>>(data.Cars);
                if (data.Cars.Count() > 0)
                {
                    foreach (OrderingCar orderingCar in data.Cars)
                    {
                        foreach (OrderingCarFile orderingCarFile in orderingCar.CarFiles)
                        {
                            if (File.Exists(Path.Combine(contentPath, orderingCarFile.FilePath)))
                            {
                                File.Delete(Path.Combine(contentPath, orderingCarFile.FilePath));
                            }
                            (await _orderingCarFileRepository.CreateSQLQuery("DELETE Ordering_CarFile where Id = '" + orderingCarFile.Id.ToString() + "'").ConfigureAwait(false)).ExecuteUpdate();
                            //await _orderingCarFileRepository.DeleteAsync(orderingCarFile);
                        }
                        (await _orderingCarRepository.CreateSQLQuery("DELETE Ordering_Car where Id = '" + orderingCar.Id.ToString() + "'").ConfigureAwait(false)).ExecuteUpdate();
                        //await _orderingCarRepository.DeleteAsync(orderingCar);
                    }
                }

                int index = 0;
                foreach (var driverCar in driverCars)
                {
                    index = index + 1;
                    OrderingCar car = new OrderingCar();
                    car.Ordering = data;
                    car.OrderingDriver = orderingDriver;
                    car.DriverCarId = driverCar.Id;
                    car.CarType = driverCar.CarType;
                    car.ActExpiry = driverCar.ActExpiry;
                    car.CarRegistration = driverCar.CarRegistration;
                    car.CarDescriptionDetail = driverCar.CarDescriptionDetail;
                    car.CarType = driverCar.CarType;
                    car.CarList = driverCar.CarList;
                    car.CarDescription = driverCar.CarDescription;
                    car.CarDescriptionDetail = driverCar.CarDescriptionDetail;
                    car.CarFeature = driverCar.CarFeature;
                    car.CarFeatureDetail = driverCar.CarFeatureDetail;
                    car.CarSpecification = driverCar.CarSpecification;
                    car.CarSpecificationDetail = driverCar.CarSpecificationDetail;
                    car.Width = driverCar.Width;
                    car.Length = driverCar.Length;
                    car.Height = driverCar.Height;
                    car.DriverName = driverCar.DriverName;
                    car.DriverPhoneCode = driverCar.DriverPhoneCode;
                    car.DriverPhoneNumber = driverCar.DriverPhoneNumber;
                    car.ProductInsurance = driverCar.ProductInsurance;
                    car.ProductInsuranceAmount = driverCar.ProductInsuranceAmount;
                    car.Note = driverCar.Note;

                    folderPath = contentPath + "ordering/" + data.Customer.Id.ToString() + "/" + currentTimeStr + "/orderingcar/" + index + "/";
                    if (!Directory.Exists(folderPath))
                    {
                        Directory.CreateDirectory(folderPath);
                    }
                    fileCount = 0;
                    if (driverCar.CarFiles != null)
                    {
                        List<OrderingCarFile> orderingCarFiles = new List<OrderingCarFile>();
                        foreach (DriverCarFile driverCarFile in driverCar.CarFiles)
                        {
                            fileCount = fileCount + 1;

                            string desPath = Path.Combine(folderPath, driverCarFile.FileName);
                            if (File.Exists(Path.Combine(contentPath, driverCarFile.FilePath)))
                            {
                                File.Copy(Path.Combine(contentPath, driverCarFile.FilePath), desPath);
                                OrderingCarFile orderingCarFile = new OrderingCarFile
                                {
                                    OrderingCar = car,
                                    FileName = driverCarFile.FileName,
                                    ContentType = driverCarFile.ContentType,
                                    DocumentType = driverCarFile.DocumentType,
                                    FilePath = "ordering/" + data.Customer.Id.ToString() + "/" + currentTimeStr + "/orderingcar/" + index + "/" + driverCarFile.FileName,
                                    DirectoryPath = "ordering/" + data.Customer.Id.ToString() + "/" + currentTimeStr + "/orderingcar/" + index + "/",
                                    FileEXT = driverCarFile.FileEXT

                                };
                                orderingCarFiles.Add(orderingCarFile);
                            }
                        }
                        car.CarFiles = orderingCarFiles;
                    }
                    orderingCars.Add(car);
                };
                data.Cars = orderingCars;
                data.Status = request.Status;


                //if (request.Insurance != null)
                //{
                //    var insurance = _mapper.Map<OrderingInsurance>(request.Insurance);
                //    insurance.Ordering = data;
                //    insurance.ProductType = productTypes.Where(p => p.Id == request.Insurance.ProductTypeId).FirstOrDefault();
                //    if (data.Insurances == null)
                //    {
                //        data.Insurances = new List<OrderingInsurance>();
                //    }
                //    data.Insurances.Add(insurance);
                //}
                OrderingPayment payment = new OrderingPayment
                {
                    Ordering = data,
                    Channel = request.Channel,
                    Amount = request.Amount,
                    Created = DateTime.UtcNow,
                    Modified = DateTime.UtcNow,
                    CreatedBy = request.CustomerId.ToString(),
                    ModifiedBy = request.CustomerId.ToString()
                };
                if (data.Payments == null)
                {
                    data.Payments = new List<OrderingPayment>();
                }
                data.Payments.Add(payment);
                if (request.Status == 3)
                {
                    #region
                    string trackingCode = "";
                    while (trackingCode == "")
                    {
                        trackingCode = Guid.NewGuid().ToString().Replace("-", "").ToUpper().Remove(13);
                        var objordering = (await _orderingTrackingCodeRepository.FindByCondition(x => x.TrackingCode.ToUpper() == trackingCode).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                        if (objordering != null)
                            trackingCode = "";
                    }
                    data.TrackingCode = trackingCode;
                    (await _orderingTrackingCodeRepository.CreateSQLQuery("INSERT INTO Ordering_TrackingCode (TrackingCode,Section) VALUES ('" + trackingCode + "','ordering')").ConfigureAwait(false)).ExecuteUpdate();
                    #endregion
                    
                    foreach (OrderingAddress address in data.Addresses)
                    {
                        trackingCode = "";
                        while (trackingCode == "")
                        {
                            trackingCode = Guid.NewGuid().ToString().Replace("-", "").ToUpper().Remove(13);
                            var objordering = (await _orderingTrackingCodeRepository.FindByCondition(x => x.TrackingCode.ToUpper() == trackingCode).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                            if (objordering != null)
                                trackingCode = "";
                        }
                        address.TrackingCode = trackingCode;
                        (await _orderingTrackingCodeRepository.CreateSQLQuery("INSERT INTO Ordering_TrackingCode (TrackingCode,Section) VALUES ('" + trackingCode + "','address')").ConfigureAwait(false)).ExecuteUpdate();
                        address.Status = 3;
                    }
                    #region
                    //var isContainer = data.Addresses.Where(a => a.AddressName == "#container#").ToList();
                    //string containerTrackingCode = "";
                    //if (isContainer.Count > 0)
                    //{
                    //    while (containerTrackingCode == "")
                    //    {
                    //        containerTrackingCode = Guid.NewGuid().ToString().Replace("-", "").ToUpper().Remove(13);
                    //        var objordering = (await _orderingTrackingCodeRepository.FindByCondition(x => x.TrackingCode.ToUpper() == containerTrackingCode).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                    //        if (objordering != null)
                    //            containerTrackingCode = "";
                    //    }
                    //     (await _orderingTrackingCodeRepository.CreateSQLQuery("INSERT INTO Ordering_TrackingCode (TrackingCode,Section) VALUES ('" + containerTrackingCode + "','ordering')").ConfigureAwait(false)).ExecuteUpdate();
                    //}

                    //foreach (OrderingProduct orderingProduct in data.Products)
                    //{
                    //    string trackingCode_deliver = "";
                    //    trackingCode = "";
                    //    while (trackingCode == "")
                    //    {
                    //        Guid tempTrackingCode = Guid.NewGuid();
                    //        trackingCode = tempTrackingCode.ToString().Replace("-", "").ToUpper().Remove(13);
                    //        trackingCode_deliver = tempTrackingCode.ToString().Replace("-", "").ToUpper().Remove(15);
                    //        var objordering = (await _orderingTrackingCodeRepository.FindByCondition(x => x.TrackingCode.ToUpper() == trackingCode).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                    //        if (objordering != null)
                    //            trackingCode = "";
                    //    }
                    //    int runNumber = 0;
                    //    foreach (OrderingAddress orderingAddress in data.Addresses)
                    //    {
                    //        runNumber = runNumber + 1;
                    //        foreach (OrderingAddressProduct addressProduct in orderingAddress.AddressProducts)
                    //        {
                    //            if (addressProduct.OrderingProduct == null)
                    //            {
                    //                if (addressProduct.Name == "#container#")
                    //                {
                    //                    addressProduct.TrackingCode = containerTrackingCode;
                    //                }
                    //                string section = "";
                    //                if (orderingAddress.AddressType == "pickuppoint")
                    //                {
                    //                    section = "pickuppoint-product";
                    //                }
                    //                else
                    //                {
                    //                    section = "deliverypoint-product";
                    //                }
                    //                (await _orderingTrackingCodeRepository.CreateSQLQuery("INSERT INTO Ordering_TrackingCode (TrackingCode,Section) VALUES ('" + containerTrackingCode + "','" + section + "')").ConfigureAwait(false)).ExecuteUpdate();
                    //            }
                    //            else
                    //            {
                    //                if (addressProduct.OrderingProduct.Id == orderingProduct.Id)
                    //                {
                    //                    string section = "";
                    //                    if (orderingAddress.AddressType == "pickuppoint")
                    //                    {
                    //                        section = "pickuppoint-product";
                    //                        addressProduct.TrackingCode = trackingCode;
                    //                        (await _orderingTrackingCodeRepository.CreateSQLQuery("INSERT INTO Ordering_TrackingCode (TrackingCode,Section) VALUES ('" + trackingCode + "','" + section + "')").ConfigureAwait(false)).ExecuteUpdate();
                    //                    }
                    //                    else
                    //                    {
                    //                        section = "deliverypoint-product";
                    //                        addressProduct.TrackingCode = trackingCode_deliver + runNumber;
                    //                        (await _orderingTrackingCodeRepository.CreateSQLQuery("INSERT INTO Ordering_TrackingCode (TrackingCode,Section) VALUES ('" + trackingCode_deliver + runNumber + "','" + section + "')").ConfigureAwait(false)).ExecuteUpdate();
                    //                    }
                    //                }
                    //            }
                    //        }
                    //    }
                    //}
                    #endregion
                }
                //wallet.Amount = wallet.Amount - request.Amount;
                //await _dummyWalletRepository.UpdateAsync(wallet);
                await _orderingRepository.UpdateAsync(data);
                (await _orderingDriverReserveRepository.CreateSQLQuery("DELETE FROM Ordering_DriverReserve  WHERE OrderingId = '" + data.Id.ToString() + "'").ConfigureAwait(false)).ExecuteUpdate();

                if (request.Status == 3)
                {

                    #region
                    //var cloneOrdering = _mapper.Map<OrderingViewModel>(data);
                    var cloneOrdering = new OrderingNotificationViewModel
                    {
                        id = data.Id,
                        status = data.Status,
                        trackingCode = data.TrackingCode,
                        orderNumber = data.TrackingCode,
                        products = _mapper.Map<List<OrderingProductNotificationViewModel>>(data.Products)
                    };

                    if (cloneOrdering.status != null)
                    {
                        cloneOrdering.statusObj = orderingStatus.Where(o => o.id == cloneOrdering.status).FirstOrDefault();
                    }
                    if (data.Cars.Count > 0)
                    {
                        cloneOrdering.carType = _mapper.Map<CarTypeViewModel>(data.Cars[0].CarType);
                        cloneOrdering.carList = _mapper.Map<CarListViewModel>(data.Cars[0].CarList);
                    }

                    foreach (OrderingProductNotificationViewModel productViewModel in cloneOrdering.products)
                    {
                        if (productViewModel.productType != null)
                        {
                            string[] productTypeIds = productViewModel.productType.Split(",");
                            productViewModel.productTypes = new List<ProductTypeViewModel>();
                            foreach (string productId in productTypeIds)
                            {
                                int id = 0;
                                if (int.TryParse(productId, out id))
                                {
                                    var productType = productTypes.Where(p => p.Id == id).FirstOrDefault();
                                    if (productType != null)
                                        productViewModel.productTypes.Add(_mapper.Map<ProductTypeViewModel>(productType));
                                }
                            }
                        }
                        if (productViewModel.packaging != null)
                        {
                            string[] packagingIds = productViewModel.packaging.Split(",");
                            productViewModel.packagings = new List<ProductPackagingViewModel>();
                            foreach (string packagingId in packagingIds)
                            {
                                int id = 0;
                                if (int.TryParse(packagingId, out id))
                                {
                                    var packaging = packagings.Where(p => p.Id == id).FirstOrDefault();
                                    if (packaging != null)
                                        productViewModel.packagings.Add(_mapper.Map<ProductPackagingViewModel>(packaging));
                                }
                            }
                        }

                    }
                    if (data.Addresses.Count > 0)
                    {
                        cloneOrdering.startPoint = _mapper.Map<OrderingAddressNotificationViewModel>(data.Addresses[0]);
                        cloneOrdering.endPoint = _mapper.Map<OrderingAddressNotificationViewModel>(data.Addresses[data.Addresses.Count - 1]);
                    }
                    #endregion
                    // noti driver
                    {
                        var driverSignalIds = (await _notificationUserRepository.FindByCondition(x => x.UserId.Equals(data.Driver.Id)).ConfigureAwait(false)).AsQueryable().Select(x => x.OneSignalId).ToList();
                        var notificationDetails = Path.Combine(Directory.GetCurrentDirectory(), $"Shared\\NotificationWord.json");
                        var notificationJson = System.IO.File.ReadAllText(notificationDetails);
                        List<NotificationWordStatusViewModel> notificationStatus = Newtonsoft.Json.JsonConvert.DeserializeObject<List<NotificationWordStatusViewModel>>(notificationJson);

                        string title = "";
                        string detail = "";
                        var notificationObj = notificationStatus.Where(o => o.id == 4).FirstOrDefault();
                        if (notificationObj != null)
                        {
                            title = notificationObj.title.th;
                            if (data.Customer != null)
                            {
                                detail = notificationObj.detail.th;
                            }
                        }
                        //if (cloneOrdering.products.Count > 2)
                        //{
                        //    cloneOrdering.products.RemoveRange(2, cloneOrdering.products.Count - 2);
                        //}
                        Domain.Notification notification = new Domain.Notification();

                        notification.OneSignalId = driverSignalIds.FirstOrDefault();
                        notification.UserId = data.Driver.Id;
                        notification.OwnerId = request.CustomerId;
                        notification.Title = title;
                        notification.Detail = detail;
                        notification.ServiceCode = "Ordering";
                        //notification.ReferenceContentKey = request.ReferenceContentKey;
                        notification.Payload = Newtonsoft.Json.JsonConvert.SerializeObject(cloneOrdering);
                        notification.Created = DateTime.UtcNow;
                        notification.Modified = DateTime.UtcNow;
                        notification.CreatedBy = request.CustomerId.ToString();
                        notification.ModifiedBy = request.CustomerId.ToString();
                        await _notificationRepository.AddAsync(notification);
                        if (driverSignalIds.Count > 0)
                        {
                            NotificationManager notiMgr = new NotificationManager(request.DRIVER_APP_ID, request.DRIVER_REST_API_KEY, request.DRIVER_AUTH_ID);
                            notiMgr.SendNotificationAsync(title, detail, driverSignalIds, notification.ServiceCode, "", "{id:'" + cloneOrdering.id + "'}");
                        }
                    }
                    // noti customer
                    {
                        var customerSignalIds = (await _notificationUserRepository.FindByCondition(x => x.UserId.Equals(data.Customer.Id)).ConfigureAwait(false)).AsQueryable().Select(x => x.OneSignalId).ToList();
                        var notificationDetails = Path.Combine(Directory.GetCurrentDirectory(), $"Shared\\NotificationWord.json");
                        var notificationJson = System.IO.File.ReadAllText(notificationDetails);
                        List<NotificationWordStatusViewModel> notificationStatus = Newtonsoft.Json.JsonConvert.DeserializeObject<List<NotificationWordStatusViewModel>>(notificationJson);

                        string title = "";
                        string detail = "";
                        var notificationObj = notificationStatus.Where(o => o.id == 4).FirstOrDefault();
                        if (notificationObj != null)
                        {
                            title = notificationObj.title.th;
                            if (data.Customer != null)
                            {
                                detail = notificationObj.detail.th;
                            }
                        }
                        if (cloneOrdering.products.Count > 2)
                        {
                            cloneOrdering.products.RemoveRange(2, cloneOrdering.products.Count - 2);
                        }
                        Domain.Notification notification = new Domain.Notification();

                        notification.OneSignalId = customerSignalIds.FirstOrDefault();
                        notification.UserId = data.Customer.Id;
                        notification.OwnerId = data.Customer.Id;
                        notification.Title = title;
                        notification.Detail = detail;
                        notification.ServiceCode = "Ordering";
                        //notification.ReferenceContentKey = request.ReferenceContentKey;
                        notification.Payload = Newtonsoft.Json.JsonConvert.SerializeObject(cloneOrdering);
                        notification.Created = DateTime.UtcNow;
                        notification.Modified = DateTime.UtcNow;
                        notification.CreatedBy = request.CustomerId.ToString();
                        notification.ModifiedBy = request.CustomerId.ToString();
                        await _notificationRepository.AddAsync(notification);

                        if (customerSignalIds.Count > 0)
                        {

                            NotificationManager notiMgr = new NotificationManager(request.APP_ID, request.REST_API_KEY, request.AUTH_ID);
                            notiMgr.SendNotificationAsync(title, detail, customerSignalIds, notification.ServiceCode, "", "{id:'" + cloneOrdering.id + "'}");
                        }
                    }
                }
                return new Response<Guid>(data.Id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
