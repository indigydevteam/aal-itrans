﻿using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;


namespace iTrans.Transportation.Application.Features.Orderings.Commands
{
    public class UpdateTrackingCodeCommand : IRequest<Response<Guid>>
    {
        public virtual Guid? UserId { get; set; }
        public virtual Guid Id { get; set; }
    }
    public class UpdateTrackingCodeCommandHandler : IRequestHandler<UpdateTrackingCodeCommand, Response<Guid>>
    {
        private readonly IOrderingRepositoryAsync _orderingRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;

        public UpdateTrackingCodeCommandHandler(IOrderingRepositoryAsync orderingRepository, IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _orderingRepository = orderingRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
        }

        public async Task<Response<Guid>> Handle(UpdateTrackingCodeCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var data = (await _orderingRepository.FindByCondition(x => x.Id == request.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (data == null)
                {
                    throw new ApiException($"Ordering Not Found.");
                }
                else
                {
                    string trackingCode = "";
                    while (trackingCode == "")
                    {
                        trackingCode = Guid.NewGuid().ToString().Replace("-", "").ToUpper().Remove(13);
                        var ordering = (await _orderingRepository.FindByCondition(x => x.TrackingCode.ToUpper() == trackingCode).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                        if (ordering != null)
                            trackingCode = "";
                    }
                    data.TrackingCode = trackingCode;

                    await _orderingRepository.UpdateAsync(data);
                    var log = new CreateAppLog(_applicationLogRepository);
                    log.CreateLog("Ordering", "CustomerCreateOrdering", "Edit", Newtonsoft.Json.JsonConvert.SerializeObject(request),request.UserId.ToString());
                    return new Response<Guid>(data.Id);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
