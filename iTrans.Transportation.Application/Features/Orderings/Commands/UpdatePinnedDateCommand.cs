﻿using AutoMapper;
using iTrans.Transportation.Application.DTOs.CarList;
using iTrans.Transportation.Application.DTOs.CarType;
using iTrans.Transportation.Application.DTOs.Ordering;
using iTrans.Transportation.Application.DTOs.OrderingAddress;
using iTrans.Transportation.Application.DTOs.OrderingAddressProduct;
using iTrans.Transportation.Application.DTOs.OrderingProduct;
using iTrans.Transportation.Application.DTOs.OrderingStatus;
using iTrans.Transportation.Application.DTOs.ProductPackaging;
using iTrans.Transportation.Application.DTOs.ProductType;
using iTrans.Transportation.Application.Enums;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;


namespace iTrans.Transportation.Application.Features.Orderings.Commands
{
    public class UpdatePinnedDateCommand : IRequest<Response<Guid>>
    {
        public virtual Guid OrderingId { get; set; }
        public virtual Guid CustomerId { get; set; }
        public virtual DateTime PinnedDate { get; set; }
    }
    public class UpdatePinnedDateCommandHandler : IRequestHandler<UpdatePinnedDateCommand, Response<Guid>>
    {
        private readonly IOrderingRepositoryAsync _orderingRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;

        public UpdatePinnedDateCommandHandler(IOrderingRepositoryAsync orderingRepository, IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _orderingRepository = orderingRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
        }

        public async Task<Response<Guid>> Handle(UpdatePinnedDateCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var data = (await _orderingRepository.FindByCondition(x => x.Id == request.OrderingId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (data == null)
                {
                    throw new ApiException($"Ordering Not Found.");
                }
                else
                {
                    
                    data.PinnedDate = request.PinnedDate;
                    data.Modified = DateTime.Now;
                    data.ModifiedBy = request.CustomerId != null ? request.CustomerId.ToString() : "";
                    await _orderingRepository.UpdateAsync(data);
                    var log = new CreateAppLog(_applicationLogRepository);
                    log.Create("Ordering", "UpdatePinnedDate", "Edit", Newtonsoft.Json.JsonConvert.SerializeObject(data));
                    return new Response<Guid>(data.Id);

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}