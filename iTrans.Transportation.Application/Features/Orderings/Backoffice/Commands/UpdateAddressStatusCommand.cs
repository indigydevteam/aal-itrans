﻿using AutoMapper;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.Orderings.Backoffice.Commands
{
    public class UpdateAddressStatusCommand : IRequest<Response<Guid>>
    {
        public virtual Guid? UserId { get; set; }
        public virtual Guid OrderingId { get; set; }
        public virtual int OrderAddressingId { get; set; }
        public virtual int StatusId { get; set; }
        public virtual int CancelStatus { get; set; }
    }
    public class UpdateAddressStatusCommandHandler : IRequestHandler<UpdateAddressStatusCommand, Response<Guid>>
    {
        private readonly IOrderingRepositoryAsync _orderingRepository;
        private readonly IOrderingAddressRepositoryAsync _orderingAddressRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IOrderingCancelStatusRepositoryAsync _orderingCancelStatusRepository;
        private readonly IMapper _mapper;
        public UpdateAddressStatusCommandHandler(IOrderingRepositoryAsync orderingRepository, IApplicationLogRepositoryAsync applicationLogRepository, IOrderingCancelStatusRepositoryAsync orderingCancelStatusRepository, IOrderingAddressRepositoryAsync orderingAddressRepository)
        {
            _orderingRepository = orderingRepository;
            _orderingCancelStatusRepository = orderingCancelStatusRepository;
            _orderingAddressRepository = orderingAddressRepository;
            _applicationLogRepository = applicationLogRepository;
        }
        public async Task<Response<Guid>> Handle(UpdateAddressStatusCommand request, CancellationToken cancellationToken)
        {
            var ordering = (await _orderingRepository.FindByCondition(x => x.Id == request.OrderingId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();

            var address = ordering.Addresses.Where(x => x.Id == request.OrderAddressingId).FirstOrDefault();
            //ordering.Status = request.StatusId;
            //ordering.Modified = DateTime.Now;
            //ordering.ModifiedBy = request.UserId != null ? request.UserId.Value.ToString() : "-";
            if (request.StatusId >= 4 && request.StatusId <= 12)
            {
                ordering.Status = 0;
                if (address != null)
                {
                    if (request.StatusId == 6)
                    {
                        address.DriverWaitingToPickUpDate = DateTime.Now;
                    }
                    if (request.StatusId == 11)
                    {
                        address.DriverWaitingToDeliveryDate = DateTime.Now;
                    }
                    if ((address.Status == 6 && request.StatusId == 7) || (address.Status == 11 && request.StatusId == 12))
                    {
                        //TimeSpan? periodTime = DateTime.Now - address.Modified;
                        //var driver = ordering.Drivers.Where(x => x.DriverId.Equals(request.DriverId)).FirstOrDefault();
                        //bool isCharter = true;
                        //address.Compensation = 0;
                        //address.PeriodTime = periodTime.ToString();
                        //if (driver != null)
                        //{
                        //    var car = ordering.Cars.Where(x => x.OrderingDriver.Id.Equals(driver.Id)).FirstOrDefault();
                        //    if (car != null)
                        //    {
                        //        isCharter = car.IsCharter;
                        //    }
                        //}
                        //double totalMinutes = periodTime.Value.TotalMinutes;
                        //if (ordering.TransportType == 1)
                        //{
                        //    if (totalMinutes > 60 && totalMinutes <= 300)
                        //    {
                        //        address.Compensation = ordering.OrderingPrice * 0.05m;
                        //    }
                        //    else if (totalMinutes > 300)
                        //    {
                        //        address.Compensation = ordering.OrderingPrice * 0.1m;
                        //    }
                        //}
                        //else if (ordering.TransportType == 2)
                        //{
                        //    if (totalMinutes > 15 && totalMinutes <= 30)
                        //    {
                        //        address.Compensation = ordering.OrderingPrice * 0.05m;
                        //    }
                        //    else if (totalMinutes > 15 && totalMinutes <= 45)
                        //    {
                        //        address.Compensation = ordering.OrderingPrice * 0.05m;
                        //        address.Compensation = address.Compensation + (ordering.OrderingPrice * 0.05m);
                        //    }
                        //    else if (totalMinutes > 15 && totalMinutes <= 60)
                        //    {
                        //        address.Compensation = ordering.OrderingPrice * 0.05m;
                        //        address.Compensation = address.Compensation + (ordering.OrderingPrice * 0.05m);
                        //        address.Compensation = address.Compensation + (ordering.OrderingPrice * 0.05m);
                        //    }
                        //    else if (totalMinutes > 60)
                        //    {
                        //        address.Compensation = ordering.OrderingPrice * 0.05m;
                        //        address.Compensation = address.Compensation + (ordering.OrderingPrice * 0.05m);
                        //        address.Compensation = address.Compensation + (ordering.OrderingPrice * 0.05m);
                        //        int hourTime = (int)Math.Round((totalMinutes - 60) / 60);
                        //        for (int i = 0; i < hourTime; i++)
                        //        {
                        //            address.Compensation = address.Compensation + (ordering.OrderingPrice * 0.1m);
                        //        }
                        //    }
                        //}
                    }

                    address.Status = request.StatusId;
                    address.Modified = DateTime.Now;
                    address.ModifiedBy = request.UserId != null ? request.UserId.Value.ToString() : "";
                }
            }
            if (request.StatusId == 13)
            {
                ordering.Status = 0;
                var successAddress = ordering.Addresses.Where(x => x.Status != 13 && x.AddressType == "deliverypoint" && x.Id != request.OrderAddressingId).ToList();
                if (address != null)
                {
                    address.Status = request.StatusId;
                    if (successAddress.Count == 0)
                    {
                        ordering.Status = 13;
                        ordering.Modified = DateTime.Now;
                        ordering.ModifiedBy = request.UserId != null ? request.UserId.Value.ToString() : "-";
                    }
                }
                //var lastAddress = ordering.Addresses.Max(x => x.Sequence);
                //if (address != null)
                //{
                //    address.Status = request.StatusId;
                //    if (address.Sequence == lastAddress)
                //    {
                //        ordering.Status = 16;
                //    }
                //    address.Modified = DateTime.Now;
                //    address.ModifiedBy = request.UserId != null ? request.UserId.Value.ToString() : "";
                //}
            }
            //var cancelStatus = (await _orderingCancelStatusRepository.FindByCondition(x => x.Id.Equals(request.CancelStatus)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            //ordering.CancelStatus = cancelStatus;
            await _orderingRepository.UpdateAsync(ordering);
            var log = new CreateAppLog(_applicationLogRepository);
            log.CreateLog("Ordering", "DriverAcceptOrdering", "Edit", Newtonsoft.Json.JsonConvert.SerializeObject(request), request.UserId.ToString());
            return new Response<Guid>(request.OrderingId);
        }
    }
}
