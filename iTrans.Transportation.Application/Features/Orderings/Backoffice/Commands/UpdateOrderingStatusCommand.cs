﻿using AutoMapper;
using iTrans.Transportation.Application.DTOs.CarList;
using iTrans.Transportation.Application.DTOs.CarType;
using iTrans.Transportation.Application.DTOs.Ordering;
using iTrans.Transportation.Application.DTOs.OrderingAddress;
using iTrans.Transportation.Application.DTOs.OrderingProduct;
using iTrans.Transportation.Application.DTOs.OrderingStatus;
using iTrans.Transportation.Application.DTOs.ProductPackaging;
using iTrans.Transportation.Application.DTOs.ProductType;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Features.Locations;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;
using NHibernate.Mapping.ByCode.Impl;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.Orderings.Backoffice.Commands
{
    public class UpdateOrderingStatusCommand : IRequest<Response<Guid>>
    {
        public virtual Guid? UserId { get; set; }
        public virtual Guid OrderingId { get; set; }
        //public virtual int AddressId { get; set; }
        public virtual int StatusId { get; set; }
        public virtual int CancelStatus { get; set; }
        public string APP_ID { get; set; }
        public string REST_API_KEY { get; set; }
        public string AUTH_ID { get; set; }
        public string DRIVER_APP_ID { get; set; }
        public string DRIVER_REST_API_KEY { get; set; }
        public string DRIVER_AUTH_ID { get; set; }
        public string UserRole { get; set; }
    }
    public class UpdateOrderingStatusCommandHandler : IRequestHandler<UpdateOrderingStatusCommand, Response<Guid>>
    {
        private readonly IOrderingRepositoryAsync _orderingRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IOrderingCancelStatusRepositoryAsync _orderingCancelStatusRepository;
        private readonly IProductTypeRepositoryAsync _productTypeRepository;
        private readonly IProductPackagingRepositoryAsync _productPackagingRepository;
        private readonly INotificationRepositoryAsync _notificationRepository;
        private readonly INotificationUserRepositoryAsync _notificationUserRepository;
        private readonly IMapper _mapper;
        public UpdateOrderingStatusCommandHandler(IOrderingRepositoryAsync orderingRepository, IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper
            , IProductTypeRepositoryAsync productTypeRepository, IProductPackagingRepositoryAsync productPackagingRepository, IOrderingCancelStatusRepositoryAsync orderingCancelStatusRepository
            , INotificationRepositoryAsync notificationRepository, INotificationUserRepositoryAsync notificationUserRepository)
        {
            _orderingRepository = orderingRepository;
            _orderingCancelStatusRepository = orderingCancelStatusRepository;
            _applicationLogRepository = applicationLogRepository;
            _productTypeRepository = productTypeRepository;
            _productPackagingRepository = productPackagingRepository;
            _notificationRepository = notificationRepository;
            _notificationUserRepository = notificationUserRepository;
            _mapper = mapper;
        }
        public async Task<Response<Guid>> Handle(UpdateOrderingStatusCommand request, CancellationToken cancellationToken)
        {
            var ordering = (await _orderingRepository.FindByCondition(x => x.Id == request.OrderingId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();

            //var address = ordering.Addresses.Where(x => x.Id == request.AddressId).FirstOrDefault();
            ordering.Status = request.StatusId;
            if (request.StatusId == 13 || request.StatusId == 15)
            {
                foreach (OrderingAddress Address in ordering.Addresses)
                {
                    if (Address.Status != 8 || Address.Status != 13)
                    {
                        Address.Status = request.StatusId;
                    }
                }
            }
            ordering.Modified = DateTime.Now;
            ordering.ModifiedBy = request.UserId != null ? request.UserId.Value.ToString() : "-";
            //if (request.StatusId >= 4 && request.StatusId <= 12)
            //{
            //    ordering.Status = 0;
            //    if (address != null)
            //    {
            //        if (request.StatusId == 6)
            //        {
            //            address.DriverWaitingToPickUpDate = DateTime.Now;
            //        }
            //        if (request.StatusId == 11)
            //        {
            //            address.DriverWaitingToDeliveryDate = DateTime.Now;
            //        }
            //        if ((address.Status == 6 && request.StatusId == 7) || (address.Status == 11 && request.StatusId == 12))
            //        {
            //            TimeSpan? periodTime = DateTime.Now - address.Modified;
            //            //var driver = ordering.Drivers.Where(x => x.DriverId.Equals(request.DriverId)).FirstOrDefault();
            //            //bool isCharter = true;
            //            //address.Compensation = 0;
            //            //address.PeriodTime = periodTime.ToString();
            //            //if (driver != null)
            //            //{
            //            //    var car = ordering.Cars.Where(x => x.OrderingDriver.Id.Equals(driver.Id)).FirstOrDefault();
            //            //    if (car != null)
            //            //    {
            //            //        isCharter = car.IsCharter;
            //            //    }
            //            //}
            //            double totalMinutes = periodTime.Value.TotalMinutes;
            //            if (ordering.TransportType == 1)
            //            {
            //                if (totalMinutes > 60 && totalMinutes <= 300)
            //                {
            //                    address.Compensation = ordering.OrderingPrice * 0.05m;
            //                }
            //                else if (totalMinutes > 300)
            //                {
            //                    address.Compensation = ordering.OrderingPrice * 0.1m;
            //                }
            //            }
            //            else if (ordering.TransportType == 2)
            //            {
            //                if (totalMinutes > 15 && totalMinutes <= 30)
            //                {
            //                    address.Compensation = ordering.OrderingPrice * 0.05m;
            //                }
            //                else if (totalMinutes > 15 && totalMinutes <= 45)
            //                {
            //                    address.Compensation = ordering.OrderingPrice * 0.05m;
            //                    address.Compensation = address.Compensation + (ordering.OrderingPrice * 0.05m);
            //                }
            //                else if (totalMinutes > 15 && totalMinutes <= 60)
            //                {
            //                    address.Compensation = ordering.OrderingPrice * 0.05m;
            //                    address.Compensation = address.Compensation + (ordering.OrderingPrice * 0.05m);
            //                    address.Compensation = address.Compensation + (ordering.OrderingPrice * 0.05m);
            //                }
            //                else if (totalMinutes > 60)
            //                {
            //                    address.Compensation = ordering.OrderingPrice * 0.05m;
            //                    address.Compensation = address.Compensation + (ordering.OrderingPrice * 0.05m);
            //                    address.Compensation = address.Compensation + (ordering.OrderingPrice * 0.05m);
            //                    int hourTime = (int)Math.Round((totalMinutes - 60) / 60);
            //                    for (int i = 0; i < hourTime; i++)
            //                    {
            //                        address.Compensation = address.Compensation + (ordering.OrderingPrice * 0.1m);
            //                    }
            //                }
            //            }
            //        }

            //        address.Status = request.StatusId;
            //        address.Modified = DateTime.Now;
            //        address.ModifiedBy = request.UserId != null ? request.UserId.Value.ToString() : "";
            //    }
            //}
            //if (request.StatusId == 13)
            //{
            //    ordering.Status = 0;
            //    var lastAddress = ordering.Addresses.Max(x => x.Sequence);
            //    if (address != null)
            //    {
            //        address.Status = request.StatusId;
            //        if (address.Sequence == lastAddress)
            //        {
            //            ordering.Status = 16;
            //        }
            //        address.Modified = DateTime.Now;
            //        address.ModifiedBy = request.UserId != null ? request.UserId.Value.ToString() : "";
            //    }
            //}

            var cancelStatus = (await _orderingCancelStatusRepository.FindByCondition(x => x.Id.Equals(request.CancelStatus)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            ordering.CancelStatus = cancelStatus;
            await _orderingRepository.UpdateAsync(ordering);
            #region send noti
            if (request.StatusId == 15)
            {
                var productTypes = (await _productTypeRepository.FindByCondition(x => x.Active.Equals(true)).ConfigureAwait(false)).AsQueryable().ToList();
                var packagings = (await _productPackagingRepository.FindByCondition(x => x.Active.Equals(true)).ConfigureAwait(false)).AsQueryable().ToList();

                var customerOrderingStatusFile = Path.Combine(Directory.GetCurrentDirectory(), $"Shared\\customer_orderingStatus.json");
                var customerOrderingStatusJson = System.IO.File.ReadAllText(customerOrderingStatusFile);
                List<OrderingStatusViewModel> customerOrderingStatus = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OrderingStatusViewModel>>(customerOrderingStatusJson);

                var userId = ordering.Customer.Id;
                var ownerId = request.UserId;

                var APP_ID = request.APP_ID;
                var REST_API_KEY = request.REST_API_KEY;
                var AUTH_ID = request.AUTH_ID;
                if (userId != null)
                {
                    var cloneOrdering = new OrderingNotificationViewModel
                    {
                        id = ordering.Id,
                        status = ordering.Status,
                        trackingCode = ordering.TrackingCode,
                        orderNumber = ordering.TrackingCode,
                        products = _mapper.Map<List<OrderingProductNotificationViewModel>>(ordering.Products)
                    };

                    if (cloneOrdering.status != null)
                    {
                        cloneOrdering.statusObj = customerOrderingStatus.Where(o => o.id == cloneOrdering.status).FirstOrDefault();
                    }

                    if (ordering.Cars.Count > 0)
                    {
                        cloneOrdering.carType = _mapper.Map<CarTypeViewModel>(ordering.Cars[0].CarType);
                        cloneOrdering.carList = _mapper.Map<CarListViewModel>(ordering.Cars[0].CarList);
                    }

                    foreach (OrderingProductNotificationViewModel productViewModel in cloneOrdering.products)
                    {
                        if (productViewModel.productType != null)
                        {
                            string[] productTypeIds = productViewModel.productType.Split(",");
                            productViewModel.productTypes = new List<ProductTypeViewModel>();
                            foreach (string productId in productTypeIds)
                            {
                                int id = 0;
                                if (int.TryParse(productId, out id))
                                {
                                    var productType = productTypes.Where(p => p.Id == id).FirstOrDefault();
                                    if (productType != null)
                                        productViewModel.productTypes.Add(_mapper.Map<ProductTypeViewModel>(productType));
                                }
                            }
                        }
                        if (productViewModel.packaging != null)
                        {
                            string[] packagingIds = productViewModel.packaging.Split(",");
                            productViewModel.packagings = new List<ProductPackagingViewModel>();
                            foreach (string packagingId in packagingIds)
                            {
                                int id = 0;
                                if (int.TryParse(packagingId, out id))
                                {
                                    var packaging = packagings.Where(p => p.Id == id).FirstOrDefault();
                                    if (packaging != null)
                                        productViewModel.packagings.Add(_mapper.Map<ProductPackagingViewModel>(packaging));
                                }
                            }
                        }

                    }
                    if (ordering.Addresses.Count > 0)
                    {
                        cloneOrdering.startPoint = _mapper.Map<OrderingAddressNotificationViewModel>(ordering.Addresses[0]);
                        cloneOrdering.endPoint = _mapper.Map<OrderingAddressNotificationViewModel>(ordering.Addresses[ordering.Addresses.Count - 1]);
                    }
                    // noti customer
                    {
                        if (cloneOrdering.status != null)
                        {
                            cloneOrdering.statusObj = customerOrderingStatus.Where(o => o.id == cloneOrdering.status).FirstOrDefault();
                        }
                        var customerSignalIds = (await _notificationUserRepository.FindByCondition(x => x.UserId.Equals(ordering.Customer.Id)).ConfigureAwait(false)).AsQueryable().Select(x => x.OneSignalId).ToList();
                        var notificationDetails = Path.Combine(Directory.GetCurrentDirectory(), $"Shared\\NotificationWord.json");
                        var notificationJson = System.IO.File.ReadAllText(notificationDetails);
                        List<NotificationWordStatusViewModel> notificationStatus = Newtonsoft.Json.JsonConvert.DeserializeObject<List<NotificationWordStatusViewModel>>(notificationJson);

                        string title = "";
                        string detail = "";
                        var notificationObj = notificationStatus.Where(o => o.id == 9).FirstOrDefault();
                        if (notificationObj != null)
                        {
                            title = notificationObj.title.th;
                            if (ordering.Customer != null)
                            {
                                detail = notificationObj.detail.th.Replace("#customertitle#", ordering.Customer.Title != null ? ordering.Customer.Title.Name_TH : "")
                                         .Replace("#customername#", ordering.Customer.Name)
                                         .Replace("#orderprice#", ordering.OrderingDesiredPrice != null || ordering.OrderingDesiredPrice != 0 ? ordering.OrderingDesiredPrice.ToString() : "-");

                                if (cancelStatus != null)
                                {
                                    detail = detail.Replace("#cancelstatus#", cancelStatus.Name_TH);
                                }
                            }
                        }
                        //if (cloneOrdering.products.Count > 2)
                        //{
                        //    cloneOrdering.products.RemoveRange(2, cloneOrdering.products.Count - 2);
                        //}
                        Domain.Notification notification = new Domain.Notification();

                        notification.OneSignalId = customerSignalIds.FirstOrDefault();
                        notification.UserId = ordering.Customer.Id;
                        notification.OwnerId = request.UserId;
                        notification.Title = title;
                        notification.Detail = detail;
                        notification.ServiceCode = "Ordering";
                        //notification.ReferenceContentKey = request.ReferenceContentKey;
                        notification.Payload = Newtonsoft.Json.JsonConvert.SerializeObject(cloneOrdering);
                        notification.Created = DateTime.UtcNow;
                        notification.Modified = DateTime.UtcNow;
                        notification.CreatedBy = request.UserId.ToString();
                        notification.ModifiedBy = request.UserId.ToString();
                        await _notificationRepository.AddAsync(notification);
                        #region insert to Inbox
                        //CustomerInbox customerInbox = new CustomerInbox()
                        //{
                        //    CustomerId = data.Customer.Id,
                        //    Title = "-",
                        //    Title_ENG = "-",
                        //    Module = "การแจ้งเตือน",
                        //    Module_ENG = "Notification",
                        //    Content = Newtonsoft.Json.JsonConvert.SerializeObject(cloneOrdering),
                        //    FromUser = data.Customer.FirstName + data.Customer.LastName,
                        //    IsDelete = false,
                        //    Created = DateTime.Now
                        //};
                        //await _customerInboxRepository.AddAsync(customerInbox);
                        #endregion
                        customerSignalIds = customerSignalIds.Where(x => x.Trim() != "").ToList();
                        if (customerSignalIds.Count > 0)
                        {

                            NotificationManager notiMgr = new NotificationManager(request.APP_ID, request.REST_API_KEY, request.AUTH_ID);
                            notiMgr.SendNotificationAsync(title, detail, customerSignalIds, notification.ServiceCode, "", "{id:'" + cloneOrdering.id + "'}");
                        }
                    }

                }
            }
            #endregion
            var log = new CreateAppLog(_applicationLogRepository);
            log.CreateLog("Ordering", "DriverAcceptOrdering", "Edit", Newtonsoft.Json.JsonConvert.SerializeObject(request), request.UserId.ToString());
            return new Response<Guid>(request.OrderingId);
        }
    }
}
