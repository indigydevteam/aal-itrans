﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.IO;
using iTrans.Transportation.Application.DTOs.OrderingStatus;

namespace iTrans.Transportation.Application.Features.Orderings.Backoffice.Commands
{
    public class UpdateOrderingStatusCommandValidator : AbstractValidator<UpdateOrderingStatusCommand>
    {
        private readonly IOrderingRepositoryAsync orderingRepository;

        public UpdateOrderingStatusCommandValidator(IOrderingRepositoryAsync orderingRepository)
        {
            this.orderingRepository = orderingRepository;

            RuleFor(p => p.OrderingId)
                .NotEmpty().WithMessage("{PropertyName} is required.")
                .NotNull()
                .MustAsync(IsExistOrdering).WithMessage("{PropertyName} is not exists.");

            RuleFor(p => p.UserId)
              .NotEmpty().WithMessage("{PropertyName} is required.")
              .NotNull();

            RuleFor(p => p.StatusId)
               .NotEmpty().WithMessage("{PropertyName} is required.")
               .NotNull()
               .MustAsync(IsExistStatus).WithMessage("{PropertyName} is not exists.");
        }

        private async Task<bool> IsExistOrdering(Guid orderingId, CancellationToken cancellationToken)
        {
            var customerObject = (await orderingRepository.FindByCondition(x => x.Id.Equals(orderingId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            if (customerObject == null)
            {
                return false;
            }
            return true;
        }

        private async Task<bool> IsExistStatus(int statusId, CancellationToken cancellationToken)
        {
            var orderingStatusPathFile = Path.Combine(Directory.GetCurrentDirectory(), $"Shared\\OrderingStatus.json");
            var statusJson = System.IO.File.ReadAllText(orderingStatusPathFile);
            List<OrderingStatusViewModel> orderingStatus = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OrderingStatusViewModel>>(statusJson);
            var existStatus = orderingStatus.Where(x => x.id == statusId).FirstOrDefault();
            if (existStatus == null)
            {
                return false;
            }
            return true;
        }
    }
}
