﻿using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.Orderings.Backoffice.Commands
{
   public class DeleteOrderingPayment : IRequest<Response<int>>
    {
        public int Id { get; set; }
        public class DeleteOrderingPaymentHandler : IRequestHandler<DeleteOrderingPayment, Response<int>>
        {
            private readonly IOrderingPaymentRepositoryAsync _orderingPaymentRepository;
            public DeleteOrderingPaymentHandler(IOrderingPaymentRepositoryAsync orderingPaymentRepository)
            {
                _orderingPaymentRepository = orderingPaymentRepository;
            }
            public async Task<Response<int>> Handle(DeleteOrderingPayment command, CancellationToken cancellationToken)
            {
                var payment = (await _orderingPaymentRepository.FindByCondition(x => x.Id == command.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (payment == null)
                {
                    throw new ApiException($"FAQ Not Found.");
                }
                else
                {
                    await _orderingPaymentRepository.DeleteAsync(payment);
                    return new Response<int>(payment.Id);
                }
            }
        }
    }
}
