﻿using AutoMapper;
using iTrans.Transportation.Application.DTOs.OrderingStatus;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.Orderings.Backoffice.Queries
{
    public class GetOrderingAddressStatus : IRequest<Response<IEnumerable<OrderingStatusViewModel>>>
    {

    }
    public class GetOrderingAddressStatusHandler : IRequestHandler<GetOrderingAddressStatus, Response<IEnumerable<OrderingStatusViewModel>>>
    {
        private readonly IMapper _mapper;
        public GetOrderingAddressStatusHandler(IMapper mapper)
        {
            _mapper = mapper;
        }
        public async Task<Response<IEnumerable<OrderingStatusViewModel>>> Handle(GetOrderingAddressStatus request, CancellationToken cancellationToken)
        {
            var orderingStatusPathFile = Path.Combine(Directory.GetCurrentDirectory(), $"Shared\\OrderingStatus.json");
            var statusJson = System.IO.File.ReadAllText(orderingStatusPathFile);
            List<OrderingStatusViewModel> orderingStatus = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OrderingStatusViewModel>>(statusJson);
            orderingStatus = orderingStatus.Where(x => x.id >= 4 && x.id <= 13  ).ToList();
            return new Response<IEnumerable<OrderingStatusViewModel>>(orderingStatus);
        }
    }
}
