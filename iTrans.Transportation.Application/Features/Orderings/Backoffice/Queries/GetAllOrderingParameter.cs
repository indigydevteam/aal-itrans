﻿using AutoMapper;
using iTrans.Transportation.Application.DTOs.Customer;
using iTrans.Transportation.Application.DTOs.Ordering.Backoffice;
using iTrans.Transportation.Application.DTOs.OrderingAddress;
using iTrans.Transportation.Application.DTOs.OrderingCar;
using iTrans.Transportation.Application.DTOs.OrderingDriver;
using iTrans.Transportation.Application.DTOs.OrderingStatus;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using LinqKit;
using MediatR;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;


namespace iTrans.Transportation.Application.Features.Orderings.Backoffice.Commands
{
    public class GetAllOrderingParameter : IRequest<PagedResponse<IEnumerable<OrderingConditionPageViewModel>>>
    {
        public int Filter { set; get; }
        public string Search { set; get; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public int Active { set; get; }
        public string Column { set; get; }
    }
    public class GetAllOrderingParameterHandler : IRequestHandler<GetAllOrderingParameter, PagedResponse<IEnumerable<OrderingConditionPageViewModel>>>
    {
        private readonly IOrderingRepositoryAsync _orderingRepository;

        private readonly IMapper _mapper;

        public GetAllOrderingParameterHandler(IOrderingRepositoryAsync orderingRepository, IMapper mapper)
        {
            _orderingRepository = orderingRepository;
            _mapper = mapper;
        }

        public async Task<PagedResponse<IEnumerable<OrderingConditionPageViewModel>>> Handle(GetAllOrderingParameter request, CancellationToken cancellationToken)
        {

            var orderingStatusPathFile = Path.Combine(Directory.GetCurrentDirectory(), $"Shared\\OrderingStatus.json");
            var statusJson = System.IO.File.ReadAllText(orderingStatusPathFile);
            List<OrderingStatusViewModel> orderingStatus = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OrderingStatusViewModel>>(statusJson);

            try
            {
                Expression<Func<Domain.Ordering, bool>> expression = PredicateBuilder.New<Domain.Ordering>(false);

                expression = x => x.Status >= 0;

                var orderings = new List<Ordering>();
                int itemCount = 0;
                if(request.Search != null && request.Search.Trim() != "")
                {
                    string search = request.Search.ToLower().Trim();
                    expression = expression.And(c => c.TrackingCode.ToLower().Trim().Contains(search) || c.CarRegistration_Str.ToLower().Trim().Contains(search) || c.DriverName_Str.ToLower().Trim().Contains(search) || c.Distance.ToLower().Trim().Contains(search)
                                 || c.StatusName_Str.ToLower().Trim().Contains(search) || c.CustomerName_Str.ToLower().Trim().Contains(search) || c.CustomerName_Str.ToLower().Trim().Contains(search) || c.PickupPoint_Str.ToLower().Trim().Contains(search)
                                 || c.PickupPointDate_Str.ToString().ToLower().Trim().Contains(search) || c.RecipientName_Str.ToLower().Trim().Contains(search) || c.DeliveryPoint_Str.ToLower().Trim().Contains(search)
                                 || c.DeliveryPointDate_Str.ToString().ToLower().Trim().Contains(search) || c.OrderingPrice.ToString().ToLower().Trim().Contains(search)
                                 );
                    //expression = expression.And(c => c.carRegistration.Contains(request.Search) || c.carRegistration.Contains(request.Search.Trim()) || c.trackingCode.Contains(request.Search.Trim()) || c.distance.Contains(request.Search.Trim())
                    //    || c.customerName.Contains(request.Search.Trim()) || c.customerName.Contains(request.Search) || c.customerName.Contains(request.Search.ToLower()) || c.customerName.Contains(request.Search.ToUpper())
                    //    || c.driverName.Contains(request.Search.Trim()) || c.driverName.Contains(request.Search) || c.driverName.Contains(request.Search.ToUpper()) || c.driverName.Contains(request.Search.ToLower()) || c.pickupPoint.Contains(request.Search.Trim()) || c.statusName.Contains(request.Search.Trim())
                    //    || c.deliveryPoint.Contains(request.Search.Trim()) || c.deliveryPointDate.Contains(request.Search.Trim()) || c.recipientnName.Contains(request.Search.Trim()) || c.recipientnName.Contains(request.Search.ToLower()) || c.recipientnName.Contains(request.Search.ToUpper()) || c.recipientnName.Contains(request.Search)
                    //    || c.pickupPointDate.Contains(request.Search.Trim()) || c.orderingPrice.ToString().Contains(request.Search.Trim()));
                }
                bool isDescending = true;
                if (request.Column == "trackingCode")
                {
                    if (request.Active == 2)
                    {
                        isDescending = true;
                    }
                    else if (request.Active == 3)
                    {
                        isDescending = false;
                    }

                    Expression<Func<Domain.Ordering, string>> orderExpression = x => x.TrackingCode;
                    itemCount = _orderingRepository.GetItemCount(expression);
                    var results = await _orderingRepository.FindByConditionWithPage(expression, orderExpression, isDescending, request.PageNumber, request.PageSize);
                    orderings = results.ToList();
                }
                if (request.Column == "carRegistration")
                {
                    if (request.Active == 2)
                    {
                        isDescending = true;
                    }
                    else if (request.Active == 3)
                    {
                        isDescending = false;
                    }

                    Expression<Func<Domain.Ordering, string>> orderExpression = x => x.CarRegistration_Str;
                    itemCount = _orderingRepository.GetItemCount(expression);
                    var results = await _orderingRepository.FindByConditionWithPage(expression, orderExpression, isDescending, request.PageNumber, request.PageSize);
                    orderings = results.ToList();
                }
                if (request.Column == "driverName")
                {
                    if (request.Active == 2)
                    {
                        isDescending = true;
                    }
                    else if (request.Active == 3)
                    {
                        isDescending = false;
                    }

                    Expression<Func<Domain.Ordering, string>> orderExpression = x => x.DriverName_Str;
                    orderExpression = x => x.TrackingCode;
                    itemCount = _orderingRepository.GetItemCount(expression);
                    var results = await _orderingRepository.FindByConditionWithPage(expression, orderExpression, isDescending, request.PageNumber, request.PageSize);
                    orderings = results.ToList();
                }
                if (request.Column == "distance")
                {
                    if (request.Active == 2)
                    {
                        isDescending = true;
                    }
                    else if (request.Active == 3)
                    {
                        isDescending = false;
                    }

                    Expression<Func<Domain.Ordering, string>> orderExpression = x => x.Distance;
                    itemCount = _orderingRepository.GetItemCount(expression);
                    var results = await _orderingRepository.FindByConditionWithPage(expression, orderExpression, isDescending, request.PageNumber, request.PageSize);
                    orderings = results.ToList();
                }
                if (request.Column == "statusName")
                {
                    if (request.Active == 2)
                    {
                        isDescending = true;
                    }
                    else if (request.Active == 3)
                    {
                        isDescending = false;
                    }

                    Expression<Func<Domain.Ordering, string>> orderExpression = x => x.StatusName_Str;
                    itemCount = _orderingRepository.GetItemCount(expression);
                    var results = await _orderingRepository.FindByConditionWithPage(expression, orderExpression, isDescending, request.PageNumber, request.PageSize);
                    orderings = results.ToList();
                }
                if (request.Column == "customerName")
                {
                    if (request.Active == 2)
                    {
                        isDescending = true;
                    }
                    else if (request.Active == 3)
                    {
                        isDescending = false;
                    }

                    Expression<Func<Domain.Ordering, string>> orderExpression = x => x.CustomerName_Str;
                    itemCount = _orderingRepository.GetItemCount(expression);
                    var results = await _orderingRepository.FindByConditionWithPage(expression, orderExpression, isDescending, request.PageNumber, request.PageSize);
                    orderings = results.ToList();
                }
                if (request.Column == "pickupPoint")
                {
                    if (request.Active == 2)
                    {
                        isDescending = true;
                    }
                    else if (request.Active == 3)
                    {
                        isDescending = false;
                    }

                    Expression<Func<Domain.Ordering, string>> orderExpression = x => x.PickupPoint_Str;
                    itemCount = _orderingRepository.GetItemCount(expression);
                    var results = await _orderingRepository.FindByConditionWithPage(expression, orderExpression, isDescending, request.PageNumber, request.PageSize);
                    orderings = results.ToList();
                }
                if (request.Column == "pickupPointDate")
                {
                    if (request.Active == 2)
                    {
                        isDescending = true;
                    }
                    else if (request.Active == 3)
                    {
                        isDescending = false;
                    }

                    Expression<Func<Domain.Ordering, DateTime>> orderExpression = x => x.PickupPointDate_Str;
                    itemCount = _orderingRepository.GetItemCount(expression);
                    var results = await _orderingRepository.FindByConditionWithPage(expression, orderExpression, isDescending, request.PageNumber, request.PageSize);
                    orderings = results.ToList();
                }
                if (request.Column == "recipientName")
                {
                    if (request.Active == 2)
                    {
                        isDescending = true;
                    }
                    else if (request.Active == 3)
                    {
                        isDescending = false;
                    }

                    Expression<Func<Domain.Ordering, string>> orderExpression = x => x.RecipientName_Str;
                    itemCount = _orderingRepository.GetItemCount(expression);
                    var results = await _orderingRepository.FindByConditionWithPage(expression, orderExpression, isDescending, request.PageNumber, request.PageSize);
                    orderings = results.ToList();
                }
                if (request.Column == "deliveryPoint")
                {
                    if (request.Active == 2)
                    {
                        isDescending = true;
                    }
                    else if (request.Active == 3)
                    {
                        isDescending = false;
                    }

                    Expression<Func<Domain.Ordering, string>> orderExpression = x => x.DeliveryPoint_Str;
                    itemCount = _orderingRepository.GetItemCount(expression);
                    var results = await _orderingRepository.FindByConditionWithPage(expression, orderExpression, isDescending, request.PageNumber, request.PageSize);
                    orderings = results.ToList();
                }
                if (request.Column == "deliveryPointDate")
                {
                    if (request.Active == 2)
                    {
                        isDescending = true;
                    }
                    else if (request.Active == 3)
                    {
                        isDescending = false;
                    }

                    Expression<Func<Domain.Ordering, DateTime>> orderExpression = x => x.DeliveryPointDate_Str;
                    itemCount = _orderingRepository.GetItemCount(expression);
                    var results = await _orderingRepository.FindByConditionWithPage(expression, orderExpression, isDescending, request.PageNumber, request.PageSize);
                    orderings = results.ToList();
                }
                if (request.Column == "orderingPrice")
                {
                    if (request.Active == 2)
                    {
                        isDescending = true;
                    }
                    else if (request.Active == 3)
                    {
                        isDescending = false;
                    }

                    Expression<Func<Domain.Ordering, decimal>> orderExpression = x => x.OrderingPrice;
                    itemCount = _orderingRepository.GetItemCount(expression);
                    var results = await _orderingRepository.FindByConditionWithPage(expression, orderExpression, isDescending, request.PageNumber, request.PageSize);
                    orderings = results.ToList();
                }
                else
                {
                    Expression<Func<Domain.Ordering, DateTime>> orderExpression = x => x.Created;
                    itemCount = _orderingRepository.GetItemCount(expression);
                    var results = await _orderingRepository.FindByConditionWithPage(expression, orderExpression, isDescending, request.PageNumber, request.PageSize);
                    orderings = results.ToList();
                }

                //var itemCount =  _orderingRepository.GetItemCount(expression);
                //var orderings = await _orderingRepository.FindByConditionWithPage(expression, orderExpression, isDescending, request.PageNumber, request.PageSize);

                List<OrderingConditionPageViewModel> result = new List<OrderingConditionPageViewModel>();
                foreach (Ordering r in orderings)
                {
                    OrderingConditionPageViewModel ordering = new OrderingConditionPageViewModel
                    {
                        id = r.Id,
                        trackingCode = r.TrackingCode,
                        customerName = r.Customer != null ? r.Customer.Name != null ? r.Customer.Name : "-" : "-",
                        carRegistration = r.Cars.Where(c => c.Ordering.Id.Equals(r.Id)).Select(d => d.CarRegistration).FirstOrDefault() != null ? r.Cars.Where(c => c.Ordering.Id.Equals(r.Id)).Select(d => d.CarRegistration).FirstOrDefault() : "-",
                        driverName = r.Drivers.Where(d => d.Ordering.Id.Equals(r.Id)).Select(d => d.Name).FirstOrDefault() != null ? r.Drivers.Where(d => d.Ordering.Id.Equals(r.Id)).Select(d => d.Name).FirstOrDefault() : "-",
                        recipientnName = r.Addresses.Where(a => a.Ordering.Id.Equals(r.Id)).Select(a => a.PersonalName).FirstOrDefault() != null ? r.Addresses.Where(a => a.Ordering.Id.Equals(r.Id)).Select(a => a.PersonalName).FirstOrDefault() : "-",
                        //statusName = s.th != null ? s.th : "-",
                        //statusColor = s.color != null ? s.color : "#000",
                        distance = r.Distance != null && r.Distance.Trim() != "" ? r.Distance : "-",
                        pickupPoint = r.Addresses.Where(a => a.Ordering.Id.Equals(r.Id) && a.AddressType == "pickuppoint" && a.Province != null).Select(a => a.Province.Name_TH).FirstOrDefault() != null ? r.Addresses.Where(a => a.Ordering.Id.Equals(r.Id) && a.AddressType == "pickuppoint" && a.Province != null).Select(a => a.Province.Name_TH).FirstOrDefault() : "-",
                        pickupPointDate = r.Addresses.Where(a => a.Ordering.Id.Equals(r.Id) && a.AddressType == "pickuppoint").Select(a => a.Date.ToString("yyyy/MM/dd HH:MM")).FirstOrDefault() != null ? r.Addresses.Where(a => a.Ordering.Id.Equals(r.Id) && a.AddressType == "pickuppoint").Select(a => a.Date.ToString("yyyy/MM/dd HH:MM")).FirstOrDefault() : "-",
                        deliveryPoint = r.Addresses.Where(a => a.Ordering.Id.Equals(r.Id) && a.AddressType == "deliverypoint" && a.Province != null).Select(a => a.Province.Name_TH).FirstOrDefault() != null ? r.Addresses.Where(a => a.Ordering.Id.Equals(r.Id) && a.AddressType == "deliverypoint" && a.Province != null).Select(a => a.Province.Name_TH).FirstOrDefault() : "-",
                        deliveryPointDate = r.Addresses.Where(a => a.Ordering.Id.Equals(r.Id) && a.AddressType == "deliverypoint").Select(a => a.Date.ToString("yyyy/MM/dd HH:MM")).FirstOrDefault() != null ? r.Addresses.Where(a => a.Ordering.Id.Equals(r.Id) && a.AddressType == "deliverypoint").Select(a => a.Date.ToString("yyyy/MM/dd HH:MM")).FirstOrDefault() : "-",
                        orderingPrice = r.OrderingPrice.ToString() != null ? r.OrderingPrice.ToString() : "-",
                    };
                    var status = orderingStatus.Where(s => s.id == r.Status).FirstOrDefault();
                    if (status != null)
                    {
                        ordering.statusName = status.th;
                        ordering.statusColor = status.color != null ? status.color : "#000";
                    }
                    result.Add(ordering);
                }

                return new PagedResponse<IEnumerable<OrderingConditionPageViewModel>>(_mapper.Map<IEnumerable<OrderingConditionPageViewModel>>(result), request.PageNumber, request.PageSize, itemCount);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}