﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.Ordering.Backoffice;
using iTrans.Transportation.Application.DTOs.OrderingAddress;
using iTrans.Transportation.Application.DTOs.OrderingStatus;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;

namespace iTrans.Transportation.Application.Features.Orderings.Backoffice.Queries
{
   public class GetOrderingRegionsPopular : IRequest<Response<IEnumerable<OrderingAddressPopularViewModel>>>
    {

    }
    public class GetOrderingRegionsPopularHandler : IRequestHandler<GetOrderingRegionsPopular, Response<IEnumerable<OrderingAddressPopularViewModel>>>
    {
        private readonly IOrderingAddressRepositoryAsync _orderingAddressRepository;
        private readonly IRegionRepositoryAsync _regionRepository;
        private readonly IMapper _mapper;

        public GetOrderingRegionsPopularHandler(IOrderingAddressRepositoryAsync orderingAddressRepository, IRegionRepositoryAsync regionRepository, IMapper mapper)
        {
            _orderingAddressRepository = orderingAddressRepository;
            _regionRepository = regionRepository;
            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<OrderingAddressPopularViewModel>>> Handle(GetOrderingRegionsPopular request, CancellationToken cancellationToken)
        {

            var OrderingAddress = _mapper.Map<List<OrderingAddressViewModel>>((await _orderingAddressRepository.FindByCondition(p => p.Province != null).ConfigureAwait(false)).AsQueryable().ToList());
            var region = await _regionRepository.GetAllAsync();
       
            var result = region.Select(p => new OrderingAddressPopularViewModel
            {
                id = p.Id,
                name = p.Name_TH,
                count = OrderingAddress.Count(),
                percent = OrderingAddress.Where(x => x.province.regionId.Equals(p.Id)).Count()

            });

            var data = result.Select(s=> new OrderingAddressPopularViewModel
            {
                id = s.id,
                name = s.name,
                percent = (s.percent/s.count)*100,

            });

            return new Response<IEnumerable<OrderingAddressPopularViewModel>>(data);
        }
    }
}
