﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.Ordering.Backoffice;
using iTrans.Transportation.Application.DTOs.OrderingAddress;
using iTrans.Transportation.Application.DTOs.OrderingStatus;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;
namespace iTrans.Transportation.Application.Features.Orderings.Backoffice.Queries
{
  public  class GetOrderingProvincesPopular : IRequest<Response<IEnumerable<OrderingAddressPopularViewModel>>>
    {
       
    }
public class GetOrderingProvincesPopularHandler : IRequestHandler<GetOrderingProvincesPopular, Response<IEnumerable<OrderingAddressPopularViewModel>>>
{
    private readonly IOrderingAddressRepositoryAsync _orderingAddressRepository;
    private readonly IProvinceRepositoryAsync _provinceRepository;
    private readonly IMapper _mapper;

    public GetOrderingProvincesPopularHandler(IOrderingAddressRepositoryAsync orderingAddressRepository, IProvinceRepositoryAsync provinceRepository, IMapper mapper)
    {
        _orderingAddressRepository = orderingAddressRepository;
        _provinceRepository = provinceRepository;
        _mapper = mapper;
    }

    public async Task<Response<IEnumerable<OrderingAddressPopularViewModel>>> Handle(GetOrderingProvincesPopular request, CancellationToken cancellationToken)
    {

         var OrderingAddress = _mapper.Map<List<OrderingAddressViewModel>>((await _orderingAddressRepository.FindByCondition(p => p.Province != null).ConfigureAwait(false)).AsQueryable().ToList()); 
          var province = await _provinceRepository.GetAllAsync();

                  
                var result = province.Select(p => new OrderingAddressPopularViewModel
                    {
                        name = p.Name_TH,
                        count = OrderingAddress.Where(x => x.province.id.Equals(p.Id)).Count()

                    }).OrderByDescending(x => x.count);

            return new Response<IEnumerable<OrderingAddressPopularViewModel>>(result.Take(5));
    }
    }
}


