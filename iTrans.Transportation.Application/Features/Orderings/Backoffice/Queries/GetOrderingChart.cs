﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.Ordering.Backoffice;
using iTrans.Transportation.Application.DTOs.OrderingAddress;
using iTrans.Transportation.Application.DTOs.OrderingStatus;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;


namespace iTrans.Transportation.Application.Features.Orderings.Backoffice.Queries
{
   public class GetOrderingChart : IRequest<Response<OrderingCountDataChartViewModel>>
    {
        public string selecttype { get; set; }
        public int selectdate { get; set; }
      
    }
    public class GetOrderingChartHandler : IRequestHandler<GetOrderingChart, Response<OrderingCountDataChartViewModel>>
    {
        private readonly IOrderingRepositoryAsync _orderingRepository;
        private readonly IMapper _mapper;

        public GetOrderingChartHandler(IOrderingRepositoryAsync orderingRepository, IMapper mapper)
        {
            _orderingRepository = orderingRepository;
            _mapper = mapper;
        }

        public async Task<Response<OrderingCountDataChartViewModel>> Handle(GetOrderingChart request, CancellationToken cancellationToken)
        {
            
            //var start =  DateTime.Now.AddDays(request.selectdate); 
            //var ordering = _orderingRepository.GetAllAsync().Result.Where(c=>c.Created > start).ToList();
            var ordering = _orderingRepository.GetAllAsync().Result.ToList();

            OrderingCountDataChartViewModel data = new OrderingCountDataChartViewModel();
            List<int> sucess = new List<int>();
            List<int> failed = new List<int>();
            List<string> date = new List<string>();

            sucess.Add(0);
            failed.Add(0);
            //var Year = ordering.Result.GroupBy(x => new { year = x.Created.Year }).ToList(); 
            //var Month = ordering.Result.GroupBy(x => new { month = x.Created.Month }).ToList(); 
            //var Day = ordering.Result.GroupBy(x => new { day = x.Created.Month }).ToList();
            if (request.selecttype == "M") { 
                var Months = ordering.Where(item=> item.Status.Equals(16)).GroupBy(item => item.Created.Month)
                 .Select(group => new { Date = group.Key, Items = group.ToList() })
                 .ToList().OrderBy(group => group.Date);    
            
                var Monthf = ordering.Where(item=> item.Status.Equals(15)).GroupBy(item => item.Created.Month)
                 .Select(group => new { Date = group.Key, Items = group.ToList() })
                 .ToList().OrderBy(group => group.Date);

                foreach (var i in Months)
                {
                    date.Add(i.Date.ToString());
                    sucess.Add(i.Items.Count());
                } 
                foreach (var i in Monthf)
                {
                    date.Add(i.Date.ToString());
                    failed.Add(i.Items.Count());
                }
            }else if (request.selecttype == "D")
            {
                var Days = ordering.Where(item => item.Status.Equals(16)).GroupBy(item => item.Created.ToString("d"))
                                .Select(group => new { Date = group.Key, Items = group.ToList() })
                                .ToList().OrderBy(group => group.Date);

                var Dayf = ordering.Where(item => item.Status.Equals(15)).GroupBy(item => item.Created.ToString("d"))
                 .Select(group => new { Date = group.Key, Items = group.ToList() })
                 .ToList().OrderBy(group => group.Date);

                foreach (var i in Days)
                {
                    date.Add(i.Date.ToString());
                    sucess.Add(i.Items.Count());
                }
                foreach (var i in Dayf)
                {
                    date.Add(i.Date.ToString());
                    failed.Add(i.Items.Count());
                }
            }
            data.date = date.OrderBy(date => date).ToList();
            data.sucess = sucess;
            data.failed = failed;
            data.type = request.selecttype;

            

            return new Response<OrderingCountDataChartViewModel>(data);
        }

        private void Concat(object p)
        {
            throw new NotImplementedException();
        }
    }
}

