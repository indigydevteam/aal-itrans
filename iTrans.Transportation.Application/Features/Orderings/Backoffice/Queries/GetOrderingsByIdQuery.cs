﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.Ordering.Backoffice;
using iTrans.Transportation.Application.DTOs.OrderingAddress;
using iTrans.Transportation.Application.DTOs.OrderingAddressProduct;
using iTrans.Transportation.Application.DTOs.OrderingDriverReserve;
using iTrans.Transportation.Application.DTOs.OrderingDriverReserveStatus;
using iTrans.Transportation.Application.DTOs.OrderingProduct;
using iTrans.Transportation.Application.DTOs.OrderingStatus;
using iTrans.Transportation.Application.DTOs.ProductPackaging;
using iTrans.Transportation.Application.DTOs.ProductType;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using LinqKit;
using MediatR;


namespace iTrans.Transportation.Application.Features.Orderings.Backoffice.Commands
{
    public class GetOrderingsByIdQuery : IRequest<Response<OrderingByIdViewModel>>
    {
        public Guid Id { get; set; }
    }
    public class GetOrderingsByIdQueryHandler : IRequestHandler<GetOrderingsByIdQuery, Response<OrderingByIdViewModel>>
    {
        private readonly IOrderingRepositoryAsync _orderingRepository;
        private readonly IProductTypeRepositoryAsync _productTypeRepository;
        private readonly IProductPackagingRepositoryAsync _productPackagingRepository;
        private readonly IMapper _mapper;
        public GetOrderingsByIdQueryHandler(IOrderingRepositoryAsync orderingRepository, IProductTypeRepositoryAsync productTypeRepository, IProductPackagingRepositoryAsync productPackagingRepository, IMapper mapper)
        {
            _orderingRepository = orderingRepository;
            _productTypeRepository = productTypeRepository;
            _productPackagingRepository = productPackagingRepository;
            _mapper = mapper;
        }
        public async Task<Response<OrderingByIdViewModel>> Handle(GetOrderingsByIdQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var productTypes = (await _productTypeRepository.FindByCondition(x => x.Active.Equals(true)).ConfigureAwait(false)).AsQueryable().ToList();
                var packagings = (await _productPackagingRepository.FindByCondition(x => x.Active.Equals(true)).ConfigureAwait(false)).AsQueryable().ToList();

                var orderingStatusPathFile = Path.Combine(Directory.GetCurrentDirectory(), $"Shared\\OrderingStatus.json");
                var statusJson = System.IO.File.ReadAllText(orderingStatusPathFile);
                List<OrderingStatusViewModel> orderingStatus = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OrderingStatusViewModel>>(statusJson);

                var driverReserveStatusPathFile = Path.Combine(Directory.GetCurrentDirectory(), $"Shared\\DriverReserveStatus.json");
                var driverReserveStatusJson = System.IO.File.ReadAllText(driverReserveStatusPathFile);
                List<OrderingDriverReserveStatusViewModel> driverReserveStatus = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OrderingDriverReserveStatusViewModel>>(driverReserveStatusJson);

                var dataObject = (await _orderingRepository.FindByCondition(x => x.Id.Equals(request.Id)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                var ordering = _mapper.Map<OrderingByIdViewModel>(dataObject);
                if (dataObject != null)
                {
                    if (ordering.status != null)
                    {
                        ordering.statusObj = orderingStatus.Where(o => o.id == ordering.status).FirstOrDefault();
                    }
                    foreach (OrderingProductViewModel productViewModel in ordering.products)
                    {
                        if (productViewModel.productType != null)
                        {
                            string[] productTypeIds = productViewModel.productType.Split(",");
                            productViewModel.productTypes = new List<ProductTypeViewModel>();
                            foreach (string productId in productTypeIds)
                            {
                                int id = 0;
                                if (int.TryParse(productId, out id))
                                {
                                    var productType = productTypes.Where(p => p.Id == id).FirstOrDefault();
                                    if (productType != null)
                                        productViewModel.productTypes.Add(_mapper.Map<ProductTypeViewModel>(productType));
                                }
                            }
                        }
                        if (productViewModel.packaging != null)
                        {
                            string[] packagingIds = productViewModel.packaging.Split(",");
                            productViewModel.packagings = new List<ProductPackagingViewModel>();
                            foreach (string packagingId in packagingIds)
                            {
                                int id = 0;
                                if (int.TryParse(packagingId, out id))
                                {
                                    var packaging = packagings.Where(p => p.Id == id).FirstOrDefault();
                                    if (packaging != null)
                                        productViewModel.packagings.Add(_mapper.Map<ProductPackagingViewModel>(packaging));
                                }
                            }
                        }
                    }

                    foreach (OrderingAddressViewModel orderingAddressViewModel in ordering.addresses)
                    {
                        if (orderingAddressViewModel.status != null)
                        {
                            orderingAddressViewModel.statusObj = orderingStatus.Where(o => o.id == orderingAddressViewModel.status).FirstOrDefault();
                        }
                        foreach (OrderingAddressProductViewModel orderingAddressProductViewModel in orderingAddressViewModel.products)
                        {
                            if (orderingAddressProductViewModel.productType != null)
                            {
                                string[] productTypeIds = orderingAddressProductViewModel.productType.Split(",");
                                orderingAddressProductViewModel.productTypes = new List<ProductTypeViewModel>();
                                foreach (string productId in productTypeIds)
                                {
                                    int id = 0;
                                    if (int.TryParse(productId, out id))
                                    {
                                        var productType = productTypes.Where(p => p.Id == id).FirstOrDefault();
                                        if (productType != null)
                                            orderingAddressProductViewModel.productTypes.Add(_mapper.Map<ProductTypeViewModel>(productType));
                                    }
                                }
                            }
                            if (orderingAddressProductViewModel.packaging != null)
                            {
                                string[] packagingIds = orderingAddressProductViewModel.packaging.Split(",");
                                orderingAddressProductViewModel.packagings = new List<ProductPackagingViewModel>();
                                foreach (string packagingId in packagingIds)
                                {
                                    int id = 0;
                                    if (int.TryParse(packagingId, out id))
                                    {
                                        var packaging = packagings.Where(p => p.Id == id).FirstOrDefault();
                                        if (packaging != null)
                                            orderingAddressProductViewModel.packagings.Add(_mapper.Map<ProductPackagingViewModel>(packaging));
                                    }
                                }
                            }
                        }
                    }

                    foreach (OrderingDriverReserveViewModel orderingDriverReserve in ordering.driverReserve)
                    {
                        orderingDriverReserve.statusObj = driverReserveStatus.Where(o => o.id == orderingDriverReserve.status).FirstOrDefault();
                    }
                }
                ordering.pickuppoint = ordering.addresses.Where(x => x.addressType == "pickuppoint").FirstOrDefault();
                ordering.deliverypoint = ordering.addresses.Where(x => x.addressType == "deliverypoint").FirstOrDefault();
                return new Response<OrderingByIdViewModel>(ordering);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
