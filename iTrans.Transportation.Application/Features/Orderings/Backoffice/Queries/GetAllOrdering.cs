﻿using AutoMapper;
using iTrans.Transportation.Application.DTOs.Customer;
using iTrans.Transportation.Application.DTOs.Ordering.Backoffice;
using iTrans.Transportation.Application.DTOs.OrderingAddress;
using iTrans.Transportation.Application.DTOs.OrderingCar;
using iTrans.Transportation.Application.DTOs.OrderingDriver;
using iTrans.Transportation.Application.DTOs.OrderingStatus;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;


namespace iTrans.Transportation.Application.Features.Orderings.Backoffice.Commands
{
    public class GetAllOrdering : IRequest<PagedResponse<IEnumerable<OrderingConditionPageViewModel>>>
    {
        public int Filter { set; get; }
        public string Search { set; get; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public int Active { set; get; }
        public string Column { set; get; }
    }
    public class GetAllOrderingParametersHandler : IRequestHandler<GetAllOrdering, PagedResponse<IEnumerable<OrderingConditionPageViewModel>>>
    {
        private readonly IOrderingRepositoryAsync _orderingRepository;

        private readonly IMapper _mapper;

        public GetAllOrderingParametersHandler(IOrderingRepositoryAsync orderingRepository, IMapper mapper)
        {
            _orderingRepository = orderingRepository;
            _mapper = mapper;
        }

        public async Task<PagedResponse<IEnumerable<OrderingConditionPageViewModel>>> Handle(GetAllOrdering request, CancellationToken cancellationToken)
        {

            var orderingStatusPathFile = Path.Combine(Directory.GetCurrentDirectory(), $"Shared\\OrderingStatus.json");
            var statusJson = System.IO.File.ReadAllText(orderingStatusPathFile);
            List<OrderingStatusViewModel> orderingStatus = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OrderingStatusViewModel>>(statusJson);

            try
            {
                var validFilter = _mapper.Map<GetAllOrdering>(request);
                var ordering = (await _orderingRepository.FindByCondition(x => x.Status.Equals(validFilter.Filter)).ConfigureAwait(false)).AsQueryable().ToList().OrderByDescending(x => x.Modified);
                if (validFilter.Filter == 99)
                {
                    ordering = (await _orderingRepository.GetAllAsync().ConfigureAwait(false)).AsQueryable().ToList().OrderByDescending(x => x.Modified);
                }
                var result = (from r in ordering
                              join s in orderingStatus on r.Status equals s.id
                              select new OrderingConditionPageViewModel
                              {
                                  id = r.Id,
                                  trackingCode = r.TrackingCode != null ? r.TrackingCode : "-",
                                  customerName = r.Customer != null ? r.Customer.Name != null ? r.Customer.Name : "-" : "-",
                                  carRegistration = r.Cars.Where(c=>c.Ordering.Id.Equals(r.Id)).Select(d => d.CarRegistration).FirstOrDefault() != null ? r.Cars.Where(c => c.Ordering.Id.Equals(r.Id)).Select(d => d.CarRegistration).FirstOrDefault():"-",
                                  driverName =r.Drivers.Where(d => d.Ordering.Id.Equals(r.Id)).Select(d=>d.Name).FirstOrDefault() != null ? r.Drivers.Where(d => d.Ordering.Id.Equals(r.Id)).Select(d => d.Name).FirstOrDefault():"-",
                                  recipientnName = r.Addresses.Where(a => a.Ordering.Id.Equals(r.Id)).Select(a=>a.PersonalName).FirstOrDefault() != null ? r.Addresses.Where(a => a.Ordering.Id.Equals(r.Id)).Select(a => a.PersonalName).FirstOrDefault() : "-",
                                  statusName = s.th != null ? s.th : "-",
                                  statusColor = s.color != null ? s.color : "#000",
                                  distance = r.Distance != null && r.Distance.Trim() != "" ? r.Distance : "-",
                                  pickupPoint = r.Addresses.Where(a => a.Ordering.Id.Equals(r.Id) && a.AddressType == "pickuppoint" && a.Province !=null).Select(a=>a.Province.Name_TH).FirstOrDefault() != null ? r.Addresses.Where(a => a.Ordering.Id.Equals(r.Id) && a.AddressType == "pickuppoint" && a.Province != null).Select(a => a.Province.Name_TH).FirstOrDefault() : "-",
                                  pickupPointDate = r.Addresses.Where(a => a.Ordering.Id.Equals(r.Id) && a.AddressType == "pickuppoint").Select(a=>a.Date.ToString("yyyy/MM/dd HH:MM")).FirstOrDefault() != null ? r.Addresses.Where(a => a.Ordering.Id.Equals(r.Id) && a.AddressType == "pickuppoint").Select(a => a.Date.ToString("yyyy/MM/dd HH:MM")).FirstOrDefault() : "-",
                                  deliveryPoint = r.Addresses.Where(a => a.Ordering.Id.Equals(r.Id) && a.AddressType == "deliverypoint" && a.Province != null).Select(a => a.Province.Name_TH).FirstOrDefault() != null ? r.Addresses.Where(a => a.Ordering.Id.Equals(r.Id) && a.AddressType == "deliverypoint" && a.Province != null).Select(a => a.Province.Name_TH).FirstOrDefault(): "-",
                                  deliveryPointDate = r.Addresses.Where(a => a.Ordering.Id.Equals(r.Id) && a.AddressType == "deliverypoint").Select(a=>a.Date.ToString("yyyy/MM/dd HH:MM")).FirstOrDefault() != null ? r.Addresses.Where(a => a.Ordering.Id.Equals(r.Id) && a.AddressType == "deliverypoint").Select(a => a.Date.ToString("yyyy/MM/dd HH:MM")).FirstOrDefault() : "-",
                                  orderingPrice =r.OrderingPrice.ToString() != null ? r.OrderingPrice.ToString() : "-",
                              });
                if (validFilter.Search != null && validFilter.Search.Trim() != "")
                {
                    result = result.Where(c => c.carRegistration.Contains(validFilter.Search) || c.carRegistration.Contains(validFilter.Search.Trim()) || c.trackingCode.Contains(validFilter.Search.Trim()) || c.distance.Contains(validFilter.Search.Trim())
                        || c.customerName.Contains(validFilter.Search.Trim()) || c.customerName.Contains(validFilter.Search) || c.customerName.Contains(validFilter.Search.ToLower()) || c.customerName.Contains(validFilter.Search.ToUpper()) 
                        || c.driverName.Contains(validFilter.Search.Trim()) || c.driverName.Contains(validFilter.Search) || c.driverName.Contains(validFilter.Search.ToUpper()) || c.driverName.Contains(validFilter.Search.ToLower()) || c.pickupPoint.Contains(validFilter.Search.Trim()) || c.statusName.Contains(validFilter.Search.Trim())
                        || c.deliveryPoint.Contains(validFilter.Search.Trim()) || c.deliveryPointDate.Contains(validFilter.Search.Trim()) || c.recipientnName.Contains(validFilter.Search.Trim()) || c.recipientnName.Contains(validFilter.Search.ToLower()) || c.recipientnName.Contains(validFilter.Search.ToUpper()) || c.recipientnName.Contains(validFilter.Search)
                        || c.pickupPointDate.Contains(validFilter.Search.Trim()) || c.orderingPrice.ToString().Contains(validFilter.Search.Trim())).ToList();
                }
                 var itemCount = result.Count();

                if (request.Column == "trackingCode" && request.Active == 2)
                {
                    result = result.OrderByDescending(x => x.trackingCode);
                }
                if (request.Column == "trackingCode" && request.Active == 3)
                {
                    result = result.OrderBy(x => x.trackingCode);
                }
                if (request.Column == "carRegistration" && request.Active == 2)
                {
                    result = result.OrderByDescending(x => x.carRegistration);
                }
                if (request.Column == "carRegistration" && request.Active == 3)
                {
                    result = result.OrderBy(x => x.carRegistration);
                }
                if (request.Column == "driverName" && request.Active == 2)
                {
                    result = result.OrderByDescending(x => x.driverName);
                }
                if (request.Column == "driverName" && request.Active == 3)
                {
                    result = result.OrderBy(x => x.driverName);
                }
                if (request.Column == "distance" && request.Active == 2)
                {
                    result = result.OrderByDescending(x => x.distance);
                }
                if (request.Column == "distance" && request.Active == 3)
                {
                    result = result.OrderBy(x => x.distance);
                }
                if (request.Column == "statusName" && request.Active == 2)
                {
                    result = result.OrderByDescending(x => x.statusName);
                }
                if (request.Column == "statusName" && request.Active == 3)
                {
                    result = result.OrderBy(x => x.statusName);
                }
                if (request.Column == "customerName" && request.Active == 2)
                {
                    result = result.OrderByDescending(x => x.customerName);
                }
                if (request.Column == "customerName" && request.Active == 3)
                {
                    result = result.OrderBy(x => x.customerName);
                }
                if (request.Column == "pickupPoint" && request.Active == 2)
                {
                    result = result.OrderByDescending(x => x.pickupPoint);
                }
                if (request.Column == "pickupPoint" && request.Active == 3)
                {
                    result = result.OrderBy(x => x.pickupPoint);
                }
                if (request.Column == "pickupPointDate" && request.Active == 2)
                {
                    result = result.OrderByDescending(x => x.pickupPointDate);
                }
                if (request.Column == "pickupPointDate" && request.Active == 3)
                {
                    result = result.OrderBy(x => x.pickupPointDate);
                }
                if (request.Column == "recipientnName" && request.Active == 2)
                {
                    result = result.OrderByDescending(x => x.recipientnName);
                }
                if (request.Column == "recipientnName" && request.Active == 3)
                {
                    result = result.OrderBy(x => x.recipientnName);
                }
                if (request.Column == "deliveryPoint" && request.Active == 2)
                {
                    result = result.OrderByDescending(x => x.deliveryPoint);
                }
                if (request.Column == "deliveryPoint" && request.Active == 3)
                {
                    result = result.OrderBy(x => x.deliveryPoint);
                }
                if (request.Column == "deliveryPointDate" && request.Active == 2)
                {
                    result = result.OrderByDescending(x => x.deliveryPointDate);
                }
                if (request.Column == "deliveryPointDate" && request.Active == 3)
                {
                    result = result.OrderBy(x => x.deliveryPointDate);
                }
                if (request.Column == "deliveryPointDate" && request.Active == 2)
                {
                    result = result.OrderByDescending(x => x.deliveryPointDate);
                }
                if (request.Column == "deliveryPointDate" && request.Active == 3)
                {
                    result = result.OrderBy(x => x.deliveryPointDate);
                } 
                if (request.Column == "orderingPrice" && request.Active == 2)
                {
                    result = result.OrderByDescending(x => x.orderingPrice);
                }
                if (request.Column == "orderingPrice" && request.Active == 3)
                {
                    result = result.OrderBy(x => x.orderingPrice);
                }


                result = result.Skip((validFilter.PageNumber - 1) * validFilter.PageSize).Take(validFilter.PageSize);
                return new PagedResponse<IEnumerable<OrderingConditionPageViewModel>>(result, validFilter.PageNumber, validFilter.PageSize, itemCount);
            }
            catch (Exception ex )
            {
                throw ex;
            }
        }
    }
}