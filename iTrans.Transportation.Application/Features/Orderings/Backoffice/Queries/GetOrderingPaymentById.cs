﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.Ordering.Backoffice;
using iTrans.Transportation.Application.DTOs.OrderingCar;
using iTrans.Transportation.Application.DTOs.OrderingStatus;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.Orderings.Backoffice.Queries
{
  public  class GetOrderingPaymentById : IRequest<Response<OrderingPaymentViewModel>>
    {
        public int Id { get; set; }
    }
    public class GetOrderingPaymentByIdHandler : IRequestHandler<GetOrderingPaymentById, Response<OrderingPaymentViewModel>>
    {
        private readonly IOrderingPaymentRepositoryAsync _orderingPaymentRepository;
        private readonly IOrderingRepositoryAsync _orderingRepository;
        private readonly IMapper _mapper;
        public GetOrderingPaymentByIdHandler(IOrderingPaymentRepositoryAsync orderingPaymentRepository, IOrderingRepositoryAsync orderingRepository, IMapper mapper)
        {
            _orderingPaymentRepository = orderingPaymentRepository;
            _orderingRepository = orderingRepository;
            _mapper = mapper;
        }
        public async Task<Response<OrderingPaymentViewModel>> Handle(GetOrderingPaymentById request, CancellationToken cancellationToken)
        {
            var folderDetails = Path.Combine(Directory.GetCurrentDirectory(), $"Shared\\customer_orderingStatus.json");
            var statusJson = System.IO.File.ReadAllText(folderDetails);
            List<OrderingStatusViewModel> orderingStatus = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OrderingStatusViewModel>>(statusJson);

            var payments = (await _orderingPaymentRepository.GetAllAsync());
            var ordering = (await _orderingRepository.GetAllAsync());



            var result = (from r in payments
                          where r.Id.Equals(request.Id) 
                          select new OrderingPaymentViewModel
                          {
                              Id = r.Id,
                              orderingId = r.Ordering.Id.ToString(),
                              trackingCode = ordering.Where(x => x.Id.Equals(r.Ordering.Id)).Select(x => x.TrackingCode).FirstOrDefault(),
                              customer = ordering.Where(x => x.Id.Equals(r.Ordering.Id)).Select(x => x.Customer.Name).FirstOrDefault(),
                              channel = r.Channel,
                              statusObj = orderingStatus.Where(o => o.id == r.Ordering.Status).FirstOrDefault(),
                              created = r.Created.ToString()

                          }).FirstOrDefault();

            var date = _mapper.Map<OrderingPaymentViewModel>(result);

            return new Response<OrderingPaymentViewModel>(date);
        }
    }
}
