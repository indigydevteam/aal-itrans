﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.Ordering.Backoffice;
using iTrans.Transportation.Application.DTOs.OrderingStatus;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;

namespace iTrans.Transportation.Application.Features.Orderings.Backoffice.Commands
{
    public class GetAllOrderingPayment : IRequest<PagedResponse<IEnumerable<OrderingPaymentViewModel>>>
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public string Search { set; get; }
        public int Active { set; get; }
        public string Column { set; get; }

        public class GetAllOrderingPaymentHandler : IRequestHandler<GetAllOrderingPayment, PagedResponse<IEnumerable<OrderingPaymentViewModel>>>
        {
            private readonly IOrderingPaymentRepositoryAsync _OrderingPaymentRepository;
            private readonly IOrderingRepositoryAsync _orderingRepository;
            private readonly IMapper _mapper;

            public GetAllOrderingPaymentHandler(IOrderingPaymentRepositoryAsync OrderingPaymentRepository, IOrderingRepositoryAsync orderingRepository, IMapper mapper)
            {
                _OrderingPaymentRepository = OrderingPaymentRepository;
                _orderingRepository = orderingRepository;
                _mapper = mapper;
            }

            public async Task<PagedResponse<IEnumerable<OrderingPaymentViewModel>>> Handle(GetAllOrderingPayment request, CancellationToken cancellationToken)
            {
                try
                {
                    var folderDetails = Path.Combine(Directory.GetCurrentDirectory(), $"Shared\\customer_orderingStatus.json");
                    var statusJson = System.IO.File.ReadAllText(folderDetails);
                    List<OrderingStatusViewModel> orderingStatus = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OrderingStatusViewModel>>(statusJson);

                    var validFilter = _mapper.Map<GetAllOrderingPayment>(request);
                    var payments = (await _OrderingPaymentRepository.GetAllAsync());
                    var ordering = (await _orderingRepository.GetAllAsync());

                    var result = from r in payments.Where(r=>r.Ordering != null)
                                 join o in ordering on r.Ordering.Id equals o.Id
                                 join s in orderingStatus on r.Ordering.Status equals s.id
                                 select new OrderingPaymentViewModel
                                  {
                                      Id = r.Id,
                                      orderingId = r.Ordering != null ? r.Ordering.Id.ToString() : "-",
                                      trackingCode = o.TrackingCode != null ? o.TrackingCode : "-",
                                      customer = o.Customer != null ? o.Customer.Name != null ? o.Customer.Name :"-":"-",
                                      channel = r.Channel != null ? r.Channel : "-",
                                      statusName = s.th != null ? s.th : "-",
                                      statusColor = s.color != null ? s.color : "-",
                                      created = r.Created.ToString("yyyy/MM/dd HH:MM")

                                  };

                    var itemCount = result.Count();
                    result = result.Where(x => x.customer.Contains(request.Search) || x.customer.Contains(request.Search) || x.customer.Contains(request.Search.Trim()) || x.customer.Contains(request.Search.ToLower()) || x.customer.Contains(request.Search.ToUpper()) || x.orderingId.ToString().Contains(request.Search.Trim()) || x.channel.Contains(request.Search.Trim()) 
                             || x.statusName.Contains(request.Search.Trim()) || x.created.Contains(request.Search.Trim()) || x.trackingCode.Contains(request.Search.Trim()));

                    if (request.Column == "trackingCode" && request.Active == 2)
                    {
                        result = result.OrderByDescending(x => x.trackingCode);
                    }
                    if (request.Column == "trackingCode" && request.Active == 3)
                    {
                        result = result.OrderBy(x => x.trackingCode);
                    }
                    if (request.Column=="customer" && request.Active ==2)
                    {
                        result = result.OrderByDescending(x => x.customer);
                    }
                    if (request.Column == "customer" && request.Active == 3)
                    {
                        result = result.OrderBy(x => x.customer);
                    }
                    if (request.Column == "channel" && request.Active == 2)
                    {
                        result = result.OrderByDescending(x => x.channel);
                    }
                    if (request.Column == "channel" && request.Active == 3)
                    {
                        result = result.OrderBy(x => x.channel);
                    }
                    if (request.Column == "status" && request.Active == 2)
                    {
                        result = result.OrderByDescending(x => x.statusName);
                    }
                    if (request.Column == "status" && request.Active == 3)
                    {
                        result = result.OrderBy(x => x.statusName);
                    }
                    if (request.Column == "created" && request.Active == 2)
                    {
                        result = result.OrderByDescending(x => x.created);
                    }
                    if (request.Column == "created" && request.Active == 3)
                    {
                        result = result.OrderBy(x => x.created);
                    }

                    result = result.Skip((validFilter.PageNumber - 1) * validFilter.PageSize).Take(validFilter.PageSize);
                    return new PagedResponse<IEnumerable<OrderingPaymentViewModel>>(result, validFilter.PageNumber, validFilter.PageSize, itemCount);
              
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
    }
}