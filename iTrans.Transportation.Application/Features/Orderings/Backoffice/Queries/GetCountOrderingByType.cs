﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.Ordering.Backoffice;
using iTrans.Transportation.Application.DTOs.Ordering;
using iTrans.Transportation.Application.DTOs.OrderingStatus;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;

namespace iTrans.Transportation.Application.Features.Orderings.Backoffice.Queries
{
    public class GetCountOrderingByType : IRequest<Response<CountOrderingByTypeVeiwModel>>
    {

    }
    public class GetCountOrderingByTypeHandler : IRequestHandler<GetCountOrderingByType, Response<CountOrderingByTypeVeiwModel>>
    {
        private readonly IOrderingRepositoryAsync _orderingRepository;
        private readonly IMapper _mapper;

        public GetCountOrderingByTypeHandler(IOrderingRepositoryAsync orderingRepository, IMapper mapper)
        {
            _orderingRepository = orderingRepository;
            _mapper = mapper;
        }

        public async Task<Response<CountOrderingByTypeVeiwModel>> Handle(GetCountOrderingByType request, CancellationToken cancellationToken)
        {

            var Ordering = await _orderingRepository.GetAllAsync();

            CountOrderingByTypeVeiwModel result = new CountOrderingByTypeVeiwModel();

            result.orderingsuccess = Ordering.Where(x => x.Status.Equals(16)).Count();
            result.orderingworking = Ordering.Where(x => x.Status.Equals(0)).Count();
            result.orderingfailed = Ordering.Where(x => x.Status.Equals(15)).Count();






            return new Response<CountOrderingByTypeVeiwModel>(result);
        }
    }
}
