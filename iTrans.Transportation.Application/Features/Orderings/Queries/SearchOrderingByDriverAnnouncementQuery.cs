﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.Ordering;
using iTrans.Transportation.Application.DTOs.OrderingAddress;
using iTrans.Transportation.Application.DTOs.OrderingAddressProduct;
using iTrans.Transportation.Application.DTOs.OrderingContainer;
using iTrans.Transportation.Application.DTOs.OrderingProduct;
using iTrans.Transportation.Application.DTOs.OrderingStatus;
using iTrans.Transportation.Application.DTOs.ProductPackaging;
using iTrans.Transportation.Application.DTOs.ProductType;
using iTrans.Transportation.Application.Enums;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using LinqKit;
using MediatR;
namespace iTrans.Transportation.Application.Features.Orderings.Queries
{
    public class SearchOrderingByDriverAnnouncementQuery : IRequest<Response<IEnumerable<OrderingWithCustomerViewModel>>>
    {
        public Guid DriverId { get; set; }
        public int DriverAnnouncementId { get; set; }
        public int PageNumber { set; get; }
        public int PageSize { set; get; }
    }
    public class SearchOrderingByDriverAnnouncementQueryHandler : IRequestHandler<SearchOrderingByDriverAnnouncementQuery, Response<IEnumerable<OrderingWithCustomerViewModel>>>
    {
        private readonly IOrderingRepositoryAsync _orderingRepository;
        private readonly IDriverAnnouncementRepositoryAsync _driverAnnouncementRepository;
        private readonly IProductTypeRepositoryAsync _productTypeRepository;
        private readonly IProductPackagingRepositoryAsync _productPackagingRepository;
        private readonly IMapper _mapper;
        public SearchOrderingByDriverAnnouncementQueryHandler(IOrderingRepositoryAsync orderingRepository, IDriverAnnouncementRepositoryAsync driverAnnouncementRepository, IProductTypeRepositoryAsync productTypeRepository, IProductPackagingRepositoryAsync productPackagingRepository, IMapper mapper)
        {
            _orderingRepository = orderingRepository;
            _productTypeRepository = productTypeRepository;
            _productPackagingRepository = productPackagingRepository;
            _driverAnnouncementRepository = driverAnnouncementRepository;
            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<OrderingWithCustomerViewModel>>> Handle(SearchOrderingByDriverAnnouncementQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var dataObject = (await _driverAnnouncementRepository.FindByCondition(x => x.Id.Equals(request.DriverAnnouncementId) && x.Driver.Id.Equals(request.DriverAnnouncementId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (dataObject == null)
                {
                    throw new ApiException($"Driver Announcement Not Found.");
                }
                int pageNumber = 1;
                int pageSize = 50;
                List<Ordering> ordering = new List<Ordering>();
                List<Domain.ProductType> productTypes = (await _productTypeRepository.FindByCondition(x => x.Active.Equals(true)).ConfigureAwait(false)).AsQueryable().ToList();
                List<Domain.ProductPackaging> packagings = (await _productPackagingRepository.FindByCondition(x => x.Active.Equals(true)).ConfigureAwait(false)).AsQueryable().ToList();

                var folderDetails = Path.Combine(Directory.GetCurrentDirectory(), $"Shared\\customer_orderingStatus.json");
                var statusJson = System.IO.File.ReadAllText(folderDetails);
                List<OrderingStatusViewModel> orderingStatus = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OrderingStatusViewModel>>(statusJson);

                var validFilter = _mapper.Map<SearchOrderingByFilter>(request);
                Expression<Func<Domain.Ordering, bool>> expression = PredicateBuilder.New<Domain.Ordering>(false);
                expression = x => x.Status == 1;
                #region ordering
                Expression<Func<Domain.Ordering, DateTime>> orderExpression = x => x.Created;
                bool isDescending = true;
                #endregion
                #region location condition
                if (dataObject.Locations.Count() > 0)
                {
                    expression = expression.And(x => x.Addresses.Where(l => l.Country.Id == dataObject.Locations[0].Country.Id && l.Province.Id == dataObject.Locations[0].Province.Id && l.AddressType == "pickuppoint").Count() > 0
                                                  && x.Addresses.Where(l => l.Country.Id == dataObject.Locations[dataObject.Locations.Count() - 1].Country.Id && l.Province.Id == dataObject.Locations[dataObject.Locations.Count() - 1].Province.Id && l.AddressType == "deliverypoint").Count() > 0);
                }
                #endregion
                #region car condition
                if (dataObject.Driver.Cars.Count() > 0 && dataObject.Driver.Cars[0].CarType != null)
                {
                    expression = expression.And(x => x.Driver.Cars.Where(c => c.CarType.Id == dataObject.Driver.Cars[0].CarType.Id).Count() > 0);

                }
                #endregion
                #region Product
                if (dataObject.AcceptProduct != null && dataObject.AcceptProduct.Trim() != "")
                {
                    foreach (string productTypeId in dataObject.AcceptProduct.Split(","))
                    {
                        expression = expression.And(x => x.Products.Where(p => p.ProductType.Contains(productTypeId)).Count() > 0);
                    }


                }
                if (dataObject.AcceptPackaging == null && dataObject.AcceptPackaging.Trim() != "")
                {
                    foreach (string packagingId in dataObject.AcceptPackaging.Split(","))
                    {
                        expression = expression.And(x => x.Products.Where(p => p.Packaging.Contains(packagingId)).Count() > 0);
                    }


                }
                #endregion
                #region car Reject Product
                if (dataObject.RejectProduct != null && dataObject.RejectProduct.Trim() != "")
                {
                    foreach (string productTypeId in dataObject.RejectProduct.Split(","))
                    {
                        expression = expression.And(x => !(x.Products.Where(p => p.ProductType.Contains(productTypeId)).Count() > 0));
                    }
                }
                if (dataObject.AcceptPackaging == null && dataObject.AcceptPackaging.Trim() != "")
                {
                    foreach (string packagingId in dataObject.AcceptPackaging.Split(","))
                    {
                        expression = expression.And(x => !(x.Products.Where(p => p.Packaging.Contains(packagingId)).Count() > 0));
                    }


                }
                #endregion
                
                var orderingTemp = await _orderingRepository.FindByConditionWithPage(expression, orderExpression, isDescending, pageNumber, pageSize);
                ordering.AddRange(orderingTemp.Except(ordering));
                if (ordering.Count() < 50 )
                {
                    expression = x => x.Status == 1;
                    if (dataObject.Locations.Count() > 0)
                    {
                        expression = expression.And(x => x.Addresses.Where(l => l.Country.Id == dataObject.Locations[0].Country.Id && l.Province.Region.Id == dataObject.Locations[0].Province.Region.Id && l.AddressType == "pickuppoint").Count() > 0
                                                      && x.Addresses.Where(l => l.Country.Id == dataObject.Locations[dataObject.Locations.Count() - 1].Country.Id && l.Province.Region.Id == dataObject.Locations[dataObject.Locations.Count() - 1].Province.Region.Id && l.AddressType == "deliverypoint").Count() > 0);
                    }
                    #region car condition
                    if (dataObject.Driver.Cars.Count() > 0 && dataObject.Driver.Cars[0].CarType != null)
                    {
                        expression = expression.And(x => x.Driver.Cars.Where(c => c.CarType.Id == dataObject.Driver.Cars[0].CarType.Id).Count() > 0);

                    }
                    #endregion
                    #region car Reject Product
                    if (dataObject.RejectProduct != null && dataObject.RejectProduct.Trim() != "")
                    {
                        foreach (string productTypeId in dataObject.RejectProduct.Split(","))
                        {
                            expression = expression.And(x => !(x.Products.Where(p => p.ProductType.Contains(productTypeId)).Count() > 0));
                        }
                    }
                    if (dataObject.AcceptPackaging == null && dataObject.AcceptPackaging.Trim() != "")
                    {
                        foreach (string packagingId in dataObject.AcceptPackaging.Split(","))
                        {
                            expression = expression.And(x => !(x.Products.Where(p => p.Packaging.Contains(packagingId)).Count() > 0));
                        }


                    }
                    #endregion
                    orderingTemp = await _orderingRepository.FindByConditionWithPage(expression, orderExpression, isDescending, pageNumber, pageSize - ordering.Count());
                    ordering.AddRange(orderingTemp.Except(ordering));
                }
                if (ordering.Count() < pageSize)
                {
                    expression = x => x.Status == 1;
                    if (dataObject.Locations.Count() > 0)
                    {
                        expression = expression.And(x => x.Addresses.Where(l => l.Country.Id == dataObject.Locations[dataObject.Locations.Count() - 1].Country.Id && l.Province.Region.Id == dataObject.Locations[dataObject.Locations.Count() - 1].Province.Region.Id && l.AddressType == "deliverypoint").Count() > 0);
                    }
                    #region car Reject Product
                    if (dataObject.RejectProduct != null && dataObject.RejectProduct.Trim() != "")
                    {
                        foreach (string productTypeId in dataObject.RejectProduct.Split(","))
                        {
                            expression = expression.And(x => !(x.Products.Where(p => p.ProductType.Contains(productTypeId)).Count() > 0));
                        }
                    }
                    if (dataObject.AcceptPackaging == null && dataObject.AcceptPackaging.Trim() != "")
                    {
                        foreach (string packagingId in dataObject.AcceptPackaging.Split(","))
                        {
                            expression = expression.And(x => !(x.Products.Where(p => p.Packaging.Contains(packagingId)).Count() > 0));
                        }


                    }
                    #endregion
                    orderingTemp = await _orderingRepository.FindByConditionWithPage(expression, orderExpression, isDescending, pageNumber, pageSize - ordering.Count());
                    ordering.AddRange(orderingTemp.Except(ordering));
                }
                if (ordering.Count() < pageSize)
                {
                    expression = x => x.Status == 1;
                    if (dataObject.Driver != null)
                    {
                        if (dataObject.Driver.Cars != null && dataObject.Driver.Cars.Count() > 0 && dataObject.Driver.Cars[0].CarType != null)
                        {
                            expression = expression.And(x => x.Cars.Where(c => c.CarType.Id == dataObject.Driver.Cars[0].CarType.Id).Count() > 0);
                            if (dataObject.Driver.Cars[0].Locations != null && dataObject.Driver.Cars[0].Locations.Count() > 0 && !dataObject.Driver.Cars[0].AllLocation)
                            {
                                var workLocations = dataObject.Driver.Cars[0].Locations.Where(x => x.LocationType == "working-area").ToList();

                                bool isFirstLocation = true;
                                Expression<Func<Domain.Ordering, bool>> locationExpression = PredicateBuilder.New<Domain.Ordering>(false);
                                foreach (DriverCarLocation workLocation in workLocations)
                                {
                                    if (isFirstLocation)
                                    {
                                        locationExpression = x => x.Addresses.Where(l => l.Country.Id == workLocation.Country.Id && l.Province.Region.Id == workLocation.Region.Id).Count() > 0;
                                    }
                                    else
                                    {
                                        locationExpression = locationExpression.Or(x => x.Addresses.Where(l => l.Country.Id == workLocation.Country.Id && l.Province.Region.Id == workLocation.Region.Id).Count() > 0);
                                    }
                                    isFirstLocation = false;
                                }
                                expression = expression.And(locationExpression);
                            }
                        }
                    }

                    orderingTemp = await _orderingRepository.FindByConditionWithPage(expression, orderExpression, isDescending, pageNumber, pageSize - ordering.Count());
                    ordering.AddRange(orderingTemp.Except(ordering));
                }
                var orderingViewModel = _mapper.Map<IEnumerable<OrderingWithCustomerViewModel>>(ordering);
                foreach (OrderingWithCustomerViewModel dataOrdering in orderingViewModel)
                {
                    if (dataOrdering.orderingPrice == 0 && dataOrdering.orderingDesiredPrice > 0)
                    {
                        dataOrdering.orderingPrice = dataOrdering.orderingDesiredPrice;
                    }
                    if (dataOrdering.status != null && dataOrdering.status != 0)
                    {
                        dataOrdering.statusObj = orderingStatus.Where(o => o.id == dataOrdering.status).FirstOrDefault();
                    }

                    if (dataOrdering.products != null) dataOrdering.products = dataOrdering.products.OrderBy(x => x.sequence).ToList();
                    foreach (OrderingProductViewModel productViewModel in dataOrdering.products)
                    {
                        if (productViewModel.productType != null)
                        {
                            string[] productTypeIds = productViewModel.productType.Split(",");
                            productViewModel.productTypes = new List<ProductTypeViewModel>();
                            foreach (string productId in productTypeIds)
                            {
                                int id = 0;
                                if (int.TryParse(productId, out id))
                                {
                                    var productType = productTypes.Where(p => p.Id == id).FirstOrDefault();
                                    if (productType != null)
                                        productViewModel.productTypes.Add(_mapper.Map<ProductTypeViewModel>(productType));
                                }
                            }
                        }
                        if (productViewModel.packaging != null)
                        {
                            string[] packagingIds = productViewModel.packaging.Split(",");
                            productViewModel.packagings = new List<ProductPackagingViewModel>();
                            foreach (string packagingId in packagingIds)
                            {
                                int id = 0;
                                if (int.TryParse(packagingId, out id))
                                {
                                    var packaging = packagings.Where(p => p.Id == id).FirstOrDefault();
                                    if (packaging != null)
                                        productViewModel.packagings.Add(_mapper.Map<ProductPackagingViewModel>(packaging));
                                }
                            }
                        }
                    }

                    if (dataOrdering.addresses != null) dataOrdering.addresses = dataOrdering.addresses.OrderBy(x => x.sequence).ToList();
                    foreach (OrderingAddressViewModel orderingAddressViewModel in dataOrdering.addresses)
                    {
                        if (orderingAddressViewModel.phoneCode == "+66" && orderingAddressViewModel.phoneNumber != null && orderingAddressViewModel.phoneNumber.Trim() != "")
                        {
                            orderingAddressViewModel.phoneNumber = "0" + orderingAddressViewModel.phoneNumber;
                        }

                        if (orderingAddressViewModel.products != null) orderingAddressViewModel.products = orderingAddressViewModel.products.OrderBy(x => x.sequence).ToList();
                        foreach (OrderingAddressProductViewModel orderingAddressProductViewModel in orderingAddressViewModel.products)
                        {
                            if (orderingAddressProductViewModel.productType != null)
                            {
                                string[] productTypeIds = orderingAddressProductViewModel.productType.Split(",");
                                orderingAddressProductViewModel.productTypes = new List<ProductTypeViewModel>();
                                foreach (string productId in productTypeIds)
                                {
                                    int id = 0;
                                    if (int.TryParse(productId, out id))
                                    {
                                        var productType = productTypes.Where(p => p.Id == id).FirstOrDefault();
                                        if (productType != null)
                                            orderingAddressProductViewModel.productTypes.Add(_mapper.Map<ProductTypeViewModel>(productType));
                                    }
                                }
                            }
                            if (orderingAddressProductViewModel.packaging != null)
                            {
                                string[] packagingIds = orderingAddressProductViewModel.packaging.Split(",");
                                orderingAddressProductViewModel.packagings = new List<ProductPackagingViewModel>();
                                foreach (string packagingId in packagingIds)
                                {
                                    int id = 0;
                                    if (int.TryParse(packagingId, out id))
                                    {
                                        var packaging = packagings.Where(p => p.Id == id).FirstOrDefault();
                                        if (packaging != null)
                                            orderingAddressProductViewModel.packagings.Add(_mapper.Map<ProductPackagingViewModel>(packaging));
                                    }
                                }
                            }
                        }
                    }
                    foreach (OrderingContainerViewModel container in dataOrdering.containers)
                    {
                        if (container.shippingContactPhoneCode == "+66" && container.shippingContactPhoneNumber != null && container.shippingContactPhoneNumber.Trim() != "")
                        {
                            container.shippingContactPhoneNumber = "0" + container.shippingContactPhoneNumber;
                        }
                        if (container.yardContactPhoneCode == "+66" && container.yardContactPhoneNumber != null && container.yardContactPhoneNumber.Trim() != "")
                        {
                            container.yardContactPhoneNumber = "0" + container.yardContactPhoneNumber;
                        }
                        if (container.linerContactPhoneCode == "+66" && container.linerContactPhoneNumber != null && container.linerContactPhoneNumber.Trim() != "")
                        {
                            container.linerContactPhoneNumber = "0" + container.linerContactPhoneNumber;
                        }
                    }
                }
                return new Response<IEnumerable<OrderingWithCustomerViewModel>>(orderingViewModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
