﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.OrderingAddress;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;


namespace iTrans.Transportation.Application.Features.Orderings.Queries
{
    public class GetOrderingCompensationByIdQuery : IRequest<Response<OrderingCompensationViewModel>>
    {
        public int OrderingAddressId { get; set; }
        public Guid OrderingId { get; set; }
    }
    public class GetOrderingCompensationByIdQueryHandler : IRequestHandler<GetOrderingCompensationByIdQuery, Response<OrderingCompensationViewModel>>
    {
        private readonly IOrderingAddressRepositoryAsync _orderingAddressRepository;
        private readonly IMapper _mapper;
        public GetOrderingCompensationByIdQueryHandler(IOrderingAddressRepositoryAsync orderingAddressRepository, IMapper mapper)
        {
            _orderingAddressRepository = orderingAddressRepository;
            _mapper = mapper;
        }
        public async Task<Response<OrderingCompensationViewModel>> Handle(GetOrderingCompensationByIdQuery request, CancellationToken cancellationToken)
        {
            var orderingAddress = (await _orderingAddressRepository.FindByCondition(x => x.Id.Equals(request.OrderingAddressId) && x.Ordering.Id == request.OrderingId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            if (orderingAddress == null)
            {
                throw new ApiException($"ordering address not found.");
            }
            TimeSpan? periodTime = DateTime.Now - orderingAddress.Modified;
            orderingAddress.Compensation = 0;
            orderingAddress.PeriodTime = periodTime.ToString();
            double totalMinutes = periodTime.Value.TotalMinutes;
            int transportType = orderingAddress.Ordering != null ? orderingAddress.Ordering.TransportType : 0;
            OrderingCompensationViewModel orderingCompensation = new OrderingCompensationViewModel()
            {
                id = orderingAddress.Id,
                orderingId = orderingAddress.Ordering.Id,
                trackingCode = orderingAddress.Ordering.TrackingCode,
                Compensation = 0,
                PeriodTime = periodTime.ToString(),
            };
            if (transportType == 1)
            {
                if (totalMinutes > 60 && totalMinutes <= 300)
                {
                    //orderingCompensation.Compensation =   orderingAddress.Ordering.OrderingPrice * 0.05m;
                    int loopCal = (int)Math.Ceiling((totalMinutes - 60) / 60);
                    for (int i = 0; i < loopCal; i++)
                    {
                        orderingCompensation.Compensation = orderingCompensation.Compensation + orderingAddress.Ordering.OrderingPrice * 0.05m;
                    }

                }
                else if (totalMinutes > 300)
                {
                    //orderingCompensation.Compensation = orderingAddress.Ordering.OrderingPrice * 0.1m;
                    int loopCal = (int)Math.Ceiling((totalMinutes - 300) / 60);
                    for (int i = 0; i < loopCal; i++)
                    {
                        orderingCompensation.Compensation = orderingCompensation.Compensation + orderingAddress.Ordering.OrderingPrice * 0.1m;
                    }
                }
            }
            else
            {
                if (totalMinutes > 15 && totalMinutes <= 30)
                {
                    orderingCompensation.Compensation = orderingAddress.Ordering.OrderingPrice * 0.05m;
                }
                else if (totalMinutes > 15 && totalMinutes <= 45)
                {
                    orderingCompensation.Compensation = orderingAddress.Ordering.OrderingPrice * 0.05m;
                    orderingCompensation.Compensation = orderingCompensation.Compensation + (orderingAddress.Ordering.OrderingPrice * 0.05m);
                }
                else if (totalMinutes > 15 && totalMinutes <= 60)
                {
                    orderingCompensation.Compensation = orderingAddress.Ordering.OrderingPrice * 0.05m;
                    orderingCompensation.Compensation = orderingCompensation.Compensation + (orderingAddress.Ordering.OrderingPrice * 0.05m);
                    orderingCompensation.Compensation = orderingCompensation.Compensation + (orderingAddress.Ordering.OrderingPrice * 0.05m);
                }
                else if (totalMinutes > 60)
                {
                    orderingCompensation.Compensation = orderingAddress.Ordering.OrderingPrice * 0.05m;
                    orderingCompensation.Compensation = orderingCompensation.Compensation + (orderingAddress.Ordering.OrderingPrice * 0.05m);
                    orderingCompensation.Compensation = orderingCompensation.Compensation + (orderingAddress.Ordering.OrderingPrice * 0.05m);
                    int hourTime = (int)Math.Round((totalMinutes - 60) / 60);
                    for (int i = 0; i < hourTime; i++)
                    {
                        orderingCompensation.Compensation = orderingCompensation.Compensation + (orderingAddress.Ordering.OrderingPrice * 0.1m);
                    }
                }
            }
            orderingAddress.Compensation = orderingCompensation.Compensation;
            await _orderingAddressRepository.UpdateAsync(orderingAddress);
            return new Response<OrderingCompensationViewModel>(orderingCompensation);
        }
    }
}
