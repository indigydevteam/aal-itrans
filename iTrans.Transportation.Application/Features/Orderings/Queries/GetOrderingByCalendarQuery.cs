﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.Ordering;
using iTrans.Transportation.Application.DTOs.OrderingAddress;
using iTrans.Transportation.Application.DTOs.OrderingAddressProduct;
using iTrans.Transportation.Application.DTOs.OrderingContainer;
using iTrans.Transportation.Application.DTOs.OrderingProduct;
using iTrans.Transportation.Application.DTOs.OrderingStatus;
using iTrans.Transportation.Application.DTOs.ProductPackaging;
using iTrans.Transportation.Application.DTOs.ProductType;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using LinqKit;
using MediatR;
namespace iTrans.Transportation.Application.Features.Orderings.Queries
{
    public class GetOrderingByCalendarQuery : IRequest<Response<IEnumerable<OrderingWithCustomerViewModel>>>
    {
        public Guid DriverId { set; get; }
        public string Day { set; get; }
        public string Month { set; get; }
        public string Year { set; get; }
        public int PageNumber { set; get; }
        public int PageSize { set; get; }
        public Guid UserId { get; set; }
        public string UserRole { get; set; }
    }
    public class GetOrderingByCalendarQueryHandler : IRequestHandler<GetOrderingByCalendarQuery, Response<IEnumerable<OrderingWithCustomerViewModel>>>
    {
        private readonly IOrderingRepositoryAsync _orderingRepository;
        private readonly IProductTypeRepositoryAsync _productTypeRepository;
        private readonly IProductPackagingRepositoryAsync _productPackagingRepository;
        private readonly IMapper _mapper;
        public GetOrderingByCalendarQueryHandler(IOrderingRepositoryAsync orderingRepository, IProductTypeRepositoryAsync productTypeRepository, IProductPackagingRepositoryAsync productPackagingRepository, IMapper mapper)
        {
            _orderingRepository = orderingRepository;
            _productTypeRepository = productTypeRepository;
            _productPackagingRepository = productPackagingRepository;
            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<OrderingWithCustomerViewModel>>> Handle(GetOrderingByCalendarQuery request, CancellationToken cancellationToken)
        {
            try
            {
                List<Domain.ProductType> productTypes = (await _productTypeRepository.FindByCondition(x => x.Active.Equals(true)).ConfigureAwait(false)).AsQueryable().ToList();
                List<Domain.ProductPackaging> packagings = (await _productPackagingRepository.FindByCondition(x => x.Active.Equals(true)).ConfigureAwait(false)).AsQueryable().ToList();

                string orderStatusPath = $"Shared\\orderingstatus.json";
                if (request.UserRole != null && request.UserRole.Trim() != "")
                {
                    orderStatusPath = $"Shared\\" + request.UserRole.Trim().ToLower() + "_orderingstatus.json";
                }
                var orderingStatusPathFile = Path.Combine(Directory.GetCurrentDirectory(), orderStatusPath);
                var statusJson = System.IO.File.ReadAllText(orderingStatusPathFile);

                List<OrderingStatusViewModel> orderingStatus = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OrderingStatusViewModel>>(statusJson);

                var validFilter = _mapper.Map<GetOrderingByCalendarQuery>(request);
                Expression<Func<Domain.Ordering, bool>> expression = PredicateBuilder.New<Domain.Ordering>(false);
                expression = x => x.Driver.Id.Equals(validFilter.DriverId);
                int day = 0;
                if (validFilter.Day != null && int.TryParse(validFilter.Day, out day))
                {
                    expression = expression.And(x => x.Addresses.Where(a => a.Date.Day == day).Count() > 0);
                }
                int month = 0;
                if (validFilter.Month != null && int.TryParse(validFilter.Month, out month))
                {
                    expression = expression.And(x => x.Addresses.Where(a => a.Date.Month == month).Count() > 0);
                }
                int year = 0;
                if (validFilter.Year != null && int.TryParse(validFilter.Year, out year))
                {
                    expression = expression.And(x => x.Addresses.Where(a => a.Date.Year == year).Count() > 0);
                }
                
                #region ordering
                Expression<Func<Domain.Ordering, DateTime>> orderExpression = x => x.Created;
                bool isDescending = true;
                #endregion
                var ordering = await _orderingRepository.FindByConditionWithPage(expression, orderExpression, isDescending, validFilter.PageNumber, validFilter.PageSize);
                var orderingViewModel = _mapper.Map<IEnumerable<OrderingWithCustomerViewModel>>(ordering);
                foreach (OrderingWithCustomerViewModel dataOrdering in orderingViewModel)
                {
                    if (dataOrdering.orderingPrice == 0 && dataOrdering.orderingDesiredPrice > 0)
                    {
                        dataOrdering.orderingPrice = dataOrdering.orderingDesiredPrice;
                    }
                    if (dataOrdering.status != null)
                    {
                        int orderingStatusId = dataOrdering.status;
                        if (orderingStatusId == 0)
                        {
                            var orderingAddress = dataOrdering.addresses.Where(x => x.status != 3).OrderByDescending(x => x.Modified).ToList();
                            if (orderingAddress.Count > 0)
                            {
                                orderingStatusId = orderingAddress[0].status;
                            }

                            //var orderingAddress = dataOrdering.addresses.Where(x => x.status != 3).OrderBy(x => x.sequence).ToList();
                            //if (orderingAddress.Count > 0)
                            //{
                            //    orderingStatusId = orderingAddress[0].status;
                            //}
                        }
                        dataOrdering.statusObj = orderingStatus.Where(o => o.id == orderingStatusId).FirstOrDefault();
                    }

                    if (dataOrdering.products != null) dataOrdering.products = dataOrdering.products.OrderBy(x => x.sequence).ToList();
                    foreach (OrderingProductViewModel productViewModel in dataOrdering.products)
                    {
                        if (productViewModel.productType != null)
                        {
                            string[] productTypeIds = productViewModel.productType.Split(",");
                            productViewModel.productTypes = new List<ProductTypeViewModel>();
                            foreach (string productId in productTypeIds)
                            {
                                int id = 0;
                                if (int.TryParse(productId, out id))
                                {
                                    var productType = productTypes.Where(p => p.Id == id).FirstOrDefault();
                                    if (productType != null)
                                        productViewModel.productTypes.Add(_mapper.Map<ProductTypeViewModel>(productType));
                                }
                            }
                        }
                        if (productViewModel.packaging != null)
                        {
                            string[] packagingIds = productViewModel.packaging.Split(",");
                            productViewModel.packagings = new List<ProductPackagingViewModel>();
                            foreach (string packagingId in packagingIds)
                            {
                                int id = 0;
                                if (int.TryParse(packagingId, out id))
                                {
                                    var packaging = packagings.Where(p => p.Id == id).FirstOrDefault();
                                    if (packaging != null)
                                        productViewModel.packagings.Add(_mapper.Map<ProductPackagingViewModel>(packaging));
                                }
                            }
                        }
                    }

                    if (dataOrdering.addresses != null) dataOrdering.addresses = dataOrdering.addresses.OrderBy(x => x.sequence).ToList();
                    foreach (OrderingAddressViewModel orderingAddressViewModel in dataOrdering.addresses)
                    {
                        if (orderingAddressViewModel.phoneCode == "+66" && orderingAddressViewModel.phoneNumber != null && orderingAddressViewModel.phoneNumber.Trim() != "")
                        {
                            orderingAddressViewModel.phoneNumber = "0" + orderingAddressViewModel.phoneNumber;
                        }
                        if (orderingAddressViewModel.driverWaitingToPickUpDate.Equals(new DateTime()))
                        {
                            orderingAddressViewModel.driverWaitingToPickUpDate = null;
                        }
                        if (orderingAddressViewModel.driverWaitingToDeliveryDate.Equals(new DateTime()))
                        {
                            orderingAddressViewModel.driverWaitingToDeliveryDate = null;
                        }
                        if (orderingAddressViewModel.status != null)
                        {
                            orderingAddressViewModel.statusObj = orderingStatus.Where(o => o.id == orderingAddressViewModel.status).FirstOrDefault();
                        }

                        if (orderingAddressViewModel.products != null) orderingAddressViewModel.products = orderingAddressViewModel.products.OrderBy(x => x.sequence).ToList();
                        foreach (OrderingAddressProductViewModel orderingAddressProductViewModel in orderingAddressViewModel.products)
                        {
                            if (orderingAddressProductViewModel.productType != null)
                            {
                                string[] productTypeIds = orderingAddressProductViewModel.productType.Split(",");
                                orderingAddressProductViewModel.productTypes = new List<ProductTypeViewModel>();
                                foreach (string productId in productTypeIds)
                                {
                                    int id = 0;
                                    if (int.TryParse(productId, out id))
                                    {
                                        var productType = productTypes.Where(p => p.Id == id).FirstOrDefault();
                                        if (productType != null)
                                            orderingAddressProductViewModel.productTypes.Add(_mapper.Map<ProductTypeViewModel>(productType));
                                    }
                                }
                            }
                            if (orderingAddressProductViewModel.packaging != null)
                            {
                                string[] packagingIds = orderingAddressProductViewModel.packaging.Split(",");
                                orderingAddressProductViewModel.packagings = new List<ProductPackagingViewModel>();
                                foreach (string packagingId in packagingIds)
                                {
                                    int id = 0;
                                    if (int.TryParse(packagingId, out id))
                                    {
                                        var packaging = packagings.Where(p => p.Id == id).FirstOrDefault();
                                        if (packaging != null)
                                            orderingAddressProductViewModel.packagings.Add(_mapper.Map<ProductPackagingViewModel>(packaging));
                                    }
                                }
                            }
                        }
                    }

                    foreach (OrderingContainerViewModel container in dataOrdering.containers)
                    {
                        if (container.shippingContactPhoneCode == "+66" && container.shippingContactPhoneNumber != null && container.shippingContactPhoneNumber.Trim() != "")
                        {
                            container.shippingContactPhoneNumber = "0" + container.shippingContactPhoneNumber;
                        }
                        if (container.yardContactPhoneCode == "+66" && container.yardContactPhoneNumber != null && container.yardContactPhoneNumber.Trim() != "")
                        {
                            container.yardContactPhoneNumber = "0" + container.yardContactPhoneNumber;
                        }
                        if (container.linerContactPhoneCode == "+66" && container.linerContactPhoneNumber != null && container.linerContactPhoneNumber.Trim() != "")
                        {
                            container.linerContactPhoneNumber = "0" + container.linerContactPhoneNumber;
                        }
                    }
                }
                return new Response<IEnumerable<OrderingWithCustomerViewModel>>(orderingViewModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
