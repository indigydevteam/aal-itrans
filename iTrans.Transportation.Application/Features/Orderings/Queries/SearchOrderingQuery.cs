﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.Ordering;
using iTrans.Transportation.Application.DTOs.OrderingAddress;
using iTrans.Transportation.Application.DTOs.OrderingAddressProduct;
using iTrans.Transportation.Application.DTOs.OrderingContainer;
using iTrans.Transportation.Application.DTOs.OrderingProduct;
using iTrans.Transportation.Application.DTOs.OrderingStatus;
using iTrans.Transportation.Application.DTOs.ProductPackaging;
using iTrans.Transportation.Application.DTOs.ProductType;
using iTrans.Transportation.Application.Enums;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using LinqKit;
using MediatR;
using static System.Data.Entity.Infrastructure.Design.Executor;

namespace iTrans.Transportation.Application.Features.Orderings.Queries
{
    public class SearchOrderingQuery : IRequest<Response<IEnumerable<OrderingWithCustomerViewModel>>>
    {
        public DateTime FromDate { set; get; }
        public DateTime ToDate { set; get; }
        public List<OrderingFilterLocationViewModel> BeginLocations { get; set; }
        public List<OrderingFilterLocationViewModel> EndLocations { get; set; }
        public int[] ProductIds { get; set; }
        public decimal Price { get; set; }
        public int PageNumber { set; get; }
        public int PageSize { set; get; }
    }
    public class SearchOrderingQueryHandler : IRequestHandler<SearchOrderingQuery, Response<IEnumerable<OrderingWithCustomerViewModel>>>
    {
        private readonly IOrderingRepositoryAsync _orderingRepository;
        private readonly IProductTypeRepositoryAsync _productTypeRepository;
        private readonly IProductPackagingRepositoryAsync _productPackagingRepository;
        private readonly IMapper _mapper;
        public SearchOrderingQueryHandler(IOrderingRepositoryAsync orderingRepository, IProductTypeRepositoryAsync productTypeRepository, IProductPackagingRepositoryAsync productPackagingRepository, IMapper mapper)
        {
            _orderingRepository = orderingRepository;
            _productTypeRepository = productTypeRepository;
            _productPackagingRepository = productPackagingRepository;
            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<OrderingWithCustomerViewModel>>> Handle(SearchOrderingQuery request, CancellationToken cancellationToken)
        {
            try
            {
                List<Domain.ProductType> productTypes = (await _productTypeRepository.FindByCondition(x => x.Active.Equals(true)).ConfigureAwait(false)).AsQueryable().ToList();
                List<Domain.ProductPackaging> packagings = (await _productPackagingRepository.FindByCondition(x => x.Active.Equals(true)).ConfigureAwait(false)).AsQueryable().ToList();

                var folderDetails = Path.Combine(Directory.GetCurrentDirectory(), $"Shared\\OrderingStatus.json");
                var statusJson = System.IO.File.ReadAllText(folderDetails);
                List<OrderingStatusViewModel> orderingStatus = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OrderingStatusViewModel>>(statusJson);



                if (request.PageNumber == 0 || request.PageSize == 0)
                {
                    request.PageNumber = 1;
                    request.PageSize = 10;
                }
                int startItem = (int)((request.PageSize * request.PageNumber) - request.PageSize);

                Expression<Func<Domain.Ordering, bool>> expression = PredicateBuilder.New<Domain.Ordering>(false);
                expression = x => x.Status == 1;

                DateTime fromDate = new DateTime();
                DateTime toDate = new DateTime();
                if (request.FromDate != null)
                {
                    fromDate = new DateTime(request.FromDate.Year, request.FromDate.Month, request.FromDate.Day, 0, 0, 0);
                }
                if (request.ToDate != null)
                {
                    toDate = new DateTime(request.ToDate.Year, request.ToDate.Month, request.ToDate.Day, 23, 59, 59);
                }

                Expression<Func<Domain.Ordering, bool>> expressionTime = PredicateBuilder.New<Domain.Ordering>(false);
                if (fromDate != new DateTime() && toDate != new DateTime())
                {

                    expressionTime = x => x.Addresses.Where(l => l.Date >= fromDate).Count() > 0 && x.Addresses.Where(l => l.Date <= toDate).Count() > 0;
                }
                else if (fromDate != new DateTime())
                {
                    expressionTime = x => x.Addresses.Where(l => l.Date >= fromDate).Count() > 0;
                }
                else if (toDate != new DateTime())
                {
                    expressionTime = x => x.Addresses.Where(l => l.Date <= toDate).Count() > 0;
                }

                Expression<Func<Domain.Ordering, bool>> expressionBeginLocation = PredicateBuilder.New<Domain.Ordering>(false);
                if (request.BeginLocations != null && request.BeginLocations.Count() > 0)
                {
                    int locationItemCount = 0;
                    foreach (OrderingFilterLocationViewModel location in request.BeginLocations)
                    {
                        Expression<Func<Domain.Ordering, bool>> expressionLocation = PredicateBuilder.New<Domain.Ordering>(false);
                        if (location.RegionId == 0)
                        {
                            if (locationItemCount == 0)
                                expressionBeginLocation = x => (x.Addresses.Where(a => a.Country.Id == location.CountryId &&
                                                                                           a.AddressType == "pickuppoint").Count() > 0);
                            else
                                expressionBeginLocation = expressionBeginLocation.Or(x => (x.Addresses.Where(a => a.Country.Id == location.CountryId &&
                                                                                           a.AddressType == "pickuppoint").Count() > 0));
                        }
                        else
                        {
                            bool isDistrictExpression = false;
                            if (location.DistrictIds != null && location.DistrictIds.Count() > 0)
                            {
                                Expression<Func<Domain.Ordering, bool>> expressionDistrict = PredicateBuilder.New<Domain.Ordering>(false);
                                int districtItemCount = 0;

                                foreach (int? districtId in location.DistrictIds)
                                {
                                    if (districtId > 0)
                                    {
                                        isDistrictExpression = true;
                                        if (districtItemCount == 0)
                                            expressionDistrict = x => (x.Addresses.Where(l => l.District.Id == districtId &&
                                                                                           l.AddressType == "pickuppoint").Count() > 0);
                                        else
                                            expressionDistrict = expressionDistrict.Or(x => (x.Addresses.Where(l => l.District.Id == districtId &&
                                                                                           l.AddressType == "pickuppoint").Count() > 0));
                                    }
                                    districtItemCount = districtItemCount + 1;
                                }
                                if (isDistrictExpression)
                                    expressionLocation = expressionDistrict;
                            }
                            if (!isDistrictExpression)
                            {
                                if (location.ProvinceId > 0)
                                {
                                    expressionLocation = x => (x.Addresses.Where(l => l.Province.Id == location.ProvinceId
                                                                                   && l.AddressType == "pickuppoint").Count() > 0);
                                }
                                else if (location.RegionId > 0)
                                {
                                    expressionLocation = x => (x.Addresses.Where(l => l.Province.Region.Id == location.RegionId
                                                                                   && l.AddressType == "pickuppoint").Count() > 0);
                                }
                                else if (location.CountryId > 0)
                                {
                                    expressionLocation = x => (x.Addresses.Where(l => l.Country.Id == location.CountryId
                                                                                   && l.AddressType == "pickuppoint").Count() > 0);
                                }
                            }
                            if (locationItemCount == 0)
                                expressionBeginLocation = expressionLocation;
                            else
                                expressionBeginLocation = expressionBeginLocation.Or(expressionLocation);
                            locationItemCount = locationItemCount + 1;
                        }
                    }
                    //expression = expression.And(expressionBeginLocation);
                }
                Expression<Func<Domain.Ordering, bool>> expressionEndLocation = PredicateBuilder.New<Domain.Ordering>(false);
                if (request.EndLocations != null && request.EndLocations.Count() > 0)
                {
                    int locationItemCount = 0;
                    foreach (OrderingFilterLocationViewModel location in request.EndLocations)
                    {
                        Expression<Func<Domain.Ordering, bool>> expressionLocation = PredicateBuilder.New<Domain.Ordering>(false);
                        if (location.RegionId == 0)
                        {
                            if (locationItemCount == 0)
                                expressionEndLocation = x => (x.Addresses.Where(a => a.Country.Id == location.CountryId &&
                                                                                           a.AddressType == "deliverypoint").Count() > 0);
                            else
                                expressionEndLocation = expressionEndLocation.Or(x => (x.Addresses.Where(a => a.Country.Id == location.CountryId &&
                                                                                           a.AddressType == "deliverypoint").Count() > 0));
                        }
                        else
                        {
                            bool isDistrictExpression = false;
                            if (location.DistrictIds != null && location.DistrictIds.Count() > 0)
                            {
                                Expression<Func<Domain.Ordering, bool>> expressionDistrict = PredicateBuilder.New<Domain.Ordering>(false);
                                int districtItemCount = 0;
                                foreach (int? districtId in location.DistrictIds)
                                {
                                    if (districtId > 0)
                                    {
                                        isDistrictExpression = true;
                                        if (districtItemCount == 0)
                                            expressionDistrict = x => (x.Addresses.Where(l => l.District.Id == districtId &&
                                                                                           l.AddressType == "deliverypoint").Count() > 0);
                                        else
                                            expressionDistrict = expressionDistrict.Or(x => (x.Addresses.Where(l => l.District.Id == districtId &&
                                                                                           l.AddressType == "deliverypoint").Count() > 0));
                                    }
                                    districtItemCount = districtItemCount + 1;
                                }
                                if (isDistrictExpression)
                                    expressionLocation = expressionDistrict;
                            }
                            if (!isDistrictExpression)
                            {
                                if (location.ProvinceId > 0)
                                {
                                    expressionLocation = x => (x.Addresses.Where(l => l.Province.Id == location.ProvinceId
                                                                                   && l.AddressType == "deliverypoint").Count() > 0);
                                }
                                else if (location.RegionId > 0)
                                {
                                    expressionLocation = x => (x.Addresses.Where(l => l.Province.Region.Id == location.RegionId
                                                                                   && l.AddressType == "deliverypoint").Count() > 0);
                                }
                                else if (location.CountryId > 0)
                                {
                                    expressionLocation = x => (x.Addresses.Where(l => l.Country.Id == location.CountryId
                                                                                   && l.AddressType == "deliverypoint").Count() > 0);
                                }
                            }
                            if (locationItemCount == 0)
                                expressionEndLocation = expressionLocation;
                            else
                                expressionEndLocation = expressionEndLocation.Or(expressionLocation);
                            locationItemCount = locationItemCount + 1;
                        }
                    }
                    // expression = expression.And(expressionEndLocation);
                }
                Expression<Func<Domain.Ordering, bool>> expressionProduct = PredicateBuilder.New<Domain.Ordering>(false);
                int productCount = 0;
                if (request.ProductIds != null)
                {
                    foreach (int productId in request.ProductIds)
                    {
                        if (productCount == 0)
                            expressionProduct = x => x.Products.Where(p => p.ProductType.Contains(productId.ToString())).Count() > 0;
                        else
                            expressionProduct = expressionProduct.Or(x => x.Products.Where(p => p.ProductType.Contains(productId.ToString())).Count() > 0);
                        productCount = productCount + 1;
                    }
                    //expression = expression.And(expressionProduct);
                }
                expression = expression.And(expressionTime).And(expressionBeginLocation).And(expressionEndLocation).And(expressionProduct);

                #region ordering
                Expression<Func<Domain.Ordering, DateTime>> orderExpression = x => x.Created;
                bool isDescending = true;
                #endregion
                List<Ordering> ordering = new List<Ordering>();

                int itemCount = _orderingRepository.GetItemCount(expression);
                if (startItem <= itemCount)
                {
                    var orderingTemp = await _orderingRepository.FindByConditionWithPage(expression, orderExpression, isDescending, request.PageNumber, request.PageSize);
                    ordering.AddRange(orderingTemp.Except(ordering));
                    startItem = 0;
                }
                else
                {
                    startItem = (startItem - itemCount) + 1;
                }


                #region condition 2
                if (ordering.Count() < request.PageSize)
                {
                    expression = PredicateBuilder.New<Domain.Ordering>(false);
                    expression = x => x.Status == 1;
                    expressionTime = PredicateBuilder.New<Domain.Ordering>(false);
                    if (fromDate != new DateTime() && toDate != new DateTime())
                    {

                        expressionTime = x => x.Addresses.Where(l => l.Date >= fromDate).Count() > 0 && x.Addresses.Where(l => l.Date <= toDate).Count() > 0;
                    }
                    else if (fromDate != new DateTime())
                    {
                        expressionTime = x => x.Addresses.Where(l => l.Date >= fromDate).Count() > 0;
                    }
                    else if (toDate != new DateTime())
                    {
                        expressionTime = x => x.Addresses.Where(l => l.Date <= toDate).Count() > 0;
                    }

                    expressionBeginLocation = PredicateBuilder.New<Domain.Ordering>(false);
                    if (request.BeginLocations != null && request.BeginLocations.Count() > 0)
                    {
                        int locationItemCount = 0;
                        foreach (OrderingFilterLocationViewModel location in request.BeginLocations)
                        {
                            Expression<Func<Domain.Ordering, bool>> expressionLocation = PredicateBuilder.New<Domain.Ordering>(false);
                            if (location.RegionId == 0)
                            {
                                if (locationItemCount == 0)
                                    expressionBeginLocation = x => (x.Addresses.Where(a => a.Country.Id == location.CountryId &&
                                                                                               a.AddressType == "pickuppoint").Count() > 0);
                                else
                                    expressionBeginLocation = expressionBeginLocation.Or(x => (x.Addresses.Where(a => a.Country.Id == location.CountryId &&
                                                                                               a.AddressType == "pickuppoint").Count() > 0));
                            }
                            else
                            {
                                if (location.DistrictIds != null && location.DistrictIds.Count() != 0)
                                {
                                    if (location.ProvinceId > 0)
                                    {
                                        expressionLocation = x => (x.Addresses.Where(l => l.Province.Id == location.ProvinceId
                                                                                       && l.AddressType == "pickuppoint").Count() > 0);
                                    }
                                    else if (location.RegionId > 0)
                                    {
                                        expressionLocation = x => (x.Addresses.Where(l => l.Province.Region.Id == location.RegionId
                                                                                       && l.AddressType == "pickuppoint").Count() > 0);
                                    }
                                    else if (location.CountryId > 0)
                                    {
                                        expressionLocation = x => (x.Addresses.Where(l => l.Country.Id == location.CountryId
                                                                                       && l.AddressType == "pickuppoint").Count() > 0);
                                    }
                                }

                                if (locationItemCount == 0)
                                    expressionBeginLocation = expressionLocation;
                                else
                                    expressionBeginLocation = expressionBeginLocation.Or(expressionLocation);
                                locationItemCount = locationItemCount + 1;
                            }
                        }
                        //expression = expression.And(expressionBeginLocation);
                    }
                    expressionEndLocation = PredicateBuilder.New<Domain.Ordering>(false);
                    if (request.EndLocations != null && request.EndLocations.Count() > 0)
                    {
                        int locationItemCount = 0;
                        foreach (OrderingFilterLocationViewModel location in request.EndLocations)
                        {
                            Expression<Func<Domain.Ordering, bool>> expressionLocation = PredicateBuilder.New<Domain.Ordering>(false);
                            if (location.RegionId == 0)
                            {
                                if (locationItemCount == 0)
                                    expressionEndLocation = x => (x.Addresses.Where(a => a.Country.Id == location.CountryId &&
                                                                                               a.AddressType == "pickuppoint").Count() > 0);
                                else
                                    expressionEndLocation = expressionEndLocation.Or(x => (x.Addresses.Where(a => a.Country.Id == location.CountryId &&
                                                                                               a.AddressType == "pickuppoint").Count() > 0));
                            }
                            else
                            {
                                if (location.DistrictIds != null && location.DistrictIds.Count() != 0)
                                {
                                    if (location.ProvinceId > 0)
                                    {
                                        expressionLocation = x => (x.Addresses.Where(l => l.Province.Id == location.ProvinceId
                                                                                       && l.AddressType == "deliverypoint").Count() > 0);
                                    }
                                    else if (location.RegionId > 0)
                                    {
                                        expressionLocation = x => (x.Addresses.Where(l => l.Province.Region.Id == location.RegionId
                                                                                       && l.AddressType == "deliverypoint").Count() > 0);
                                    }
                                    else if (location.CountryId > 0)
                                    {
                                        expressionLocation = x => (x.Addresses.Where(l => l.Country.Id == location.CountryId
                                                                                       && l.AddressType == "deliverypoint").Count() > 0);
                                    }
                                }
                                if (locationItemCount == 0)
                                    expressionEndLocation = expressionLocation;
                                else
                                    expressionEndLocation = expressionEndLocation.Or(expressionLocation);
                                locationItemCount = locationItemCount + 1;
                            }
                        }
                        //expression = expression.And(expressionEndLocation);
                    }

                    expression = expression.And(expressionTime).And(expressionBeginLocation).And(expressionEndLocation).And(expressionProduct);
                    int itemCount2 = _orderingRepository.GetItemCount(expression);
                    if (startItem < itemCount2)
                    {
                        var orderTemp = await _orderingRepository.FindByConditionWithPage(expression, orderExpression, isDescending, startItem, request.PageSize - ordering.Count());

                        ordering.AddRange(orderTemp.Except(ordering));
                    }
                }
                #endregion condition 2

                var orderingViewModel = _mapper.Map<IEnumerable<OrderingWithCustomerViewModel>>(ordering);
                foreach (OrderingWithCustomerViewModel dataOrdering in orderingViewModel)
                {
                    if (dataOrdering.orderingPrice == 0 && dataOrdering.orderingDesiredPrice > 0)
                    {
                        dataOrdering.orderingPrice = dataOrdering.orderingDesiredPrice;
                    }
                    if (dataOrdering.status != null && dataOrdering.status != 0)
                    {
                        dataOrdering.statusObj = orderingStatus.Where(o => o.id == dataOrdering.status).FirstOrDefault();
                    }

                    if (dataOrdering.products != null) dataOrdering.products = dataOrdering.products.OrderBy(x => x.sequence).ToList();
                    foreach (OrderingProductViewModel productViewModel in dataOrdering.products)
                    {
                        if (productViewModel.productType != null)
                        {
                            string[] productTypeIds = productViewModel.productType.Split(",");
                            productViewModel.productTypes = new List<ProductTypeViewModel>();
                            foreach (string productId in productTypeIds)
                            {
                                int id = 0;
                                if (int.TryParse(productId, out id))
                                {
                                    var productType = productTypes.Where(p => p.Id == id).FirstOrDefault();
                                    if (productType != null)
                                        productViewModel.productTypes.Add(_mapper.Map<ProductTypeViewModel>(productType));
                                }
                            }
                        }
                        if (productViewModel.packaging != null)
                        {
                            string[] packagingIds = productViewModel.packaging.Split(",");
                            productViewModel.packagings = new List<ProductPackagingViewModel>();
                            foreach (string packagingId in packagingIds)
                            {
                                int id = 0;
                                if (int.TryParse(packagingId, out id))
                                {
                                    var packaging = packagings.Where(p => p.Id == id).FirstOrDefault();
                                    if (packaging != null)
                                        productViewModel.packagings.Add(_mapper.Map<ProductPackagingViewModel>(packaging));
                                }
                            }
                        }
                    }

                    if (dataOrdering.addresses != null) dataOrdering.addresses = dataOrdering.addresses.OrderBy(x => x.sequence).ToList();
                    foreach (OrderingAddressViewModel orderingAddressViewModel in dataOrdering.addresses)
                    {
                        if (orderingAddressViewModel.phoneCode == "+66" && orderingAddressViewModel.phoneNumber != null && orderingAddressViewModel.phoneNumber.Trim() != "")
                        {
                            orderingAddressViewModel.phoneNumber = "0" + orderingAddressViewModel.phoneNumber;
                        }

                        if (orderingAddressViewModel.products != null) orderingAddressViewModel.products = orderingAddressViewModel.products.OrderBy(x => x.sequence).ToList();
                        foreach (OrderingAddressProductViewModel orderingAddressProductViewModel in orderingAddressViewModel.products)
                        {
                            if (orderingAddressProductViewModel.productType != null)
                            {
                                string[] productTypeIds = orderingAddressProductViewModel.productType.Split(",");
                                orderingAddressProductViewModel.productTypes = new List<ProductTypeViewModel>();
                                foreach (string productId in productTypeIds)
                                {
                                    int id = 0;
                                    if (int.TryParse(productId, out id))
                                    {
                                        var productType = productTypes.Where(p => p.Id == id).FirstOrDefault();
                                        if (productType != null)
                                            orderingAddressProductViewModel.productTypes.Add(_mapper.Map<ProductTypeViewModel>(productType));
                                    }
                                }
                            }
                            if (orderingAddressProductViewModel.packaging != null)
                            {
                                string[] packagingIds = orderingAddressProductViewModel.packaging.Split(",");
                                orderingAddressProductViewModel.packagings = new List<ProductPackagingViewModel>();
                                foreach (string packagingId in packagingIds)
                                {
                                    int id = 0;
                                    if (int.TryParse(packagingId, out id))
                                    {
                                        var packaging = packagings.Where(p => p.Id == id).FirstOrDefault();
                                        if (packaging != null)
                                            orderingAddressProductViewModel.packagings.Add(_mapper.Map<ProductPackagingViewModel>(packaging));
                                    }
                                }
                            }
                        }
                    }
                    foreach (OrderingContainerViewModel container in dataOrdering.containers)
                    {
                        if (container.shippingContactPhoneCode == "+66" && container.shippingContactPhoneNumber != null && container.shippingContactPhoneNumber.Trim() != "")
                        {
                            container.shippingContactPhoneNumber = "0" + container.shippingContactPhoneNumber;
                        }
                        if (container.yardContactPhoneCode == "+66" && container.yardContactPhoneNumber != null && container.yardContactPhoneNumber.Trim() != "")
                        {
                            container.yardContactPhoneNumber = "0" + container.yardContactPhoneNumber;
                        }
                        if (container.linerContactPhoneCode == "+66" && container.linerContactPhoneNumber != null && container.linerContactPhoneNumber.Trim() != "")
                        {
                            container.linerContactPhoneNumber = "0" + container.linerContactPhoneNumber;
                        }
                    }
                }
                return new Response<IEnumerable<OrderingWithCustomerViewModel>>(orderingViewModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
