﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.Ordering;
using iTrans.Transportation.Application.DTOs.OrderingAddress;
using iTrans.Transportation.Application.DTOs.OrderingAddressProduct;
using iTrans.Transportation.Application.DTOs.OrderingContainer;
using iTrans.Transportation.Application.DTOs.OrderingProduct;
using iTrans.Transportation.Application.DTOs.OrderingStatus;
using iTrans.Transportation.Application.DTOs.ProductPackaging;
using iTrans.Transportation.Application.DTOs.ProductType;
using iTrans.Transportation.Application.Enums;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using LinqKit;
using MediatR;
namespace iTrans.Transportation.Application.Features.Orderings.Queries
{
    public class SearchOrderingByFilter : IRequest<Response<IEnumerable<OrderingWithCustomerViewModel>>>
    {
        public string Month { set; get; }
        public List<OrderingFilterLocationViewModel> Locations { get; set; }
        public List<OrderingFilterProductViewModel> Products { get; set; }
        public List<OrderingFilterProductViewModel> RejectsProducts { get; set; }
        public int PageNumber { set; get; }
        public int PageSize { set; get; }
    }
    public class SearchOrderingByFilterHandler : IRequestHandler<SearchOrderingByFilter, Response<IEnumerable<OrderingWithCustomerViewModel>>>
    {
        private readonly IOrderingRepositoryAsync _orderingRepository;
        private readonly IProductTypeRepositoryAsync _productTypeRepository;
        private readonly IProductPackagingRepositoryAsync _productPackagingRepository;
        private readonly IMapper _mapper;
        public SearchOrderingByFilterHandler(IOrderingRepositoryAsync orderingRepository, IProductTypeRepositoryAsync productTypeRepository, IProductPackagingRepositoryAsync productPackagingRepository, IMapper mapper)
        {
            _orderingRepository = orderingRepository;
            _productTypeRepository = productTypeRepository;
            _productPackagingRepository = productPackagingRepository;
            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<OrderingWithCustomerViewModel>>> Handle(SearchOrderingByFilter request, CancellationToken cancellationToken)
        {
            try
            {
                List<Domain.ProductType> productTypes = (await _productTypeRepository.FindByCondition(x => x.Active.Equals(true)).ConfigureAwait(false)).AsQueryable().ToList();
                List<Domain.ProductPackaging> packagings = (await _productPackagingRepository.FindByCondition(x => x.Active.Equals(true)).ConfigureAwait(false)).AsQueryable().ToList();

                var folderDetails = Path.Combine(Directory.GetCurrentDirectory(), $"Shared\\OrderingStatus.json");
                var statusJson = System.IO.File.ReadAllText(folderDetails);
                List<OrderingStatusViewModel> orderingStatus = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OrderingStatusViewModel>>(statusJson);

                var validFilter = _mapper.Map<SearchOrderingByFilter>(request);
                Expression<Func<Domain.Ordering, bool>> expression = PredicateBuilder.New<Domain.Ordering>(false);
                expression = x => x.Status == 1;
                //if (request.Locations != null && request.Locations.Count() > 0)
                //{
                //    expression = expression.And(x => (x.Addresses.Where(l => l.Country.Id == request.Locations[0].CountryId
                //                                                         && l.Province.Id == request.Locations[0].ProvinceId
                //                                                         && l.District.Id == request.Locations[0].DistrictId
                //                                                         && l.Subdistrict.Id == request.Locations[0].SubdistrictId).Count() > 0)
                //                                                         || (x.Addresses.Where(l => l.Country.Id == request.Locations[request.Locations.Count() - 1].CountryId
                //                                                         && l.Province.Id == request.Locations[request.Locations.Count() - 1].ProvinceId
                //                                                         && l.District.Id == request.Locations[request.Locations.Count() - 1].DistrictId
                //                                                         && l.Subdistrict.Id == request.Locations[request.Locations.Count() - 1].SubdistrictId).Count() > 0));
                //}

                if (request.Products != null)
                {
                    Expression<Func<Domain.Ordering, bool>> expressionProduct = PredicateBuilder.New<Domain.Ordering>(false);
                    foreach (OrderingFilterProductViewModel product in request.Products)
                    {
                        foreach (int tempId in product.ProductTypeId)
                        {
                            expressionProduct = expressionProduct.Or(x => x.Products.Where(p => p.ProductType.Contains(tempId.ToString())).Count() > 0);
                        }
                        foreach (int tempId in product.PackagingId)
                        {
                            expressionProduct = expressionProduct.Or(x => x.Products.Where(p => p.Packaging.Contains(tempId.ToString())).Count() > 0);
                        }
                    }
                    expression.And(expressionProduct);
                }


                if (request.RejectsProducts != null)
                {
                    Expression<Func<Domain.Ordering, bool>> expressionProduct = PredicateBuilder.New<Domain.Ordering>(false);
                    foreach (OrderingFilterProductViewModel product in request.RejectsProducts)
                    {
                        foreach (int tempId in product.ProductTypeId)
                        {
                            expression = expressionProduct.Or(x => x.Products.Where(p => p.ProductType.Contains(tempId.ToString())).Count() > 0);
                        }
                        foreach (int tempId in product.PackagingId)
                        {
                            expression = expressionProduct.Or(x => x.Products.Where(p => p.Packaging.Contains(tempId.ToString())).Count() > 0);
                        }
                    }
                    expression.And(expressionProduct);
                }
                #region ordering
                Expression<Func<Domain.Ordering, DateTime>> orderExpression = x => x.Created;
                bool isDescending = true;
                #endregion
                var ordering = await _orderingRepository.FindByConditionWithPage(expression, orderExpression, isDescending, validFilter.PageNumber, validFilter.PageSize);
                var orderingViewModel = _mapper.Map<IEnumerable<OrderingWithCustomerViewModel>>(ordering);
                foreach (OrderingWithCustomerViewModel dataOrdering in orderingViewModel)
                {
                    if (dataOrdering.orderingPrice == 0 && dataOrdering.orderingDesiredPrice > 0)
                    {
                        dataOrdering.orderingPrice = dataOrdering.orderingDesiredPrice;
                    }
                    if (dataOrdering.status != null && dataOrdering.status != 0)
                    {
                        dataOrdering.statusObj = orderingStatus.Where(o => o.id == dataOrdering.status).FirstOrDefault();
                    }
                    foreach (OrderingProductViewModel productViewModel in dataOrdering.products)
                    {
                        if (productViewModel.productType != null)
                        {
                            string[] productTypeIds = productViewModel.productType.Split(",");
                            productViewModel.productTypes = new List<ProductTypeViewModel>();
                            foreach (string productId in productTypeIds)
                            {
                                int id = 0;
                                if (int.TryParse(productId, out id))
                                {
                                    var productType = productTypes.Where(p => p.Id == id).FirstOrDefault();
                                    if (productType != null)
                                        productViewModel.productTypes.Add(_mapper.Map<ProductTypeViewModel>(productType));
                                }
                            }
                        }
                        if (productViewModel.packaging != null)
                        {
                            string[] packagingIds = productViewModel.packaging.Split(",");
                            productViewModel.packagings = new List<ProductPackagingViewModel>();
                            foreach (string packagingId in packagingIds)
                            {
                                int id = 0;
                                if (int.TryParse(packagingId, out id))
                                {
                                    var packaging = packagings.Where(p => p.Id == id).FirstOrDefault();
                                    if (packaging != null)
                                        productViewModel.packagings.Add(_mapper.Map<ProductPackagingViewModel>(packaging));
                                }
                            }
                        }
                    }

                    foreach (OrderingAddressViewModel orderingAddressViewModel in dataOrdering.addresses)
                    {
                        if (orderingAddressViewModel.phoneCode == "+66" && orderingAddressViewModel.phoneNumber != null && orderingAddressViewModel.phoneNumber.Trim() != "")
                        {
                            orderingAddressViewModel.phoneNumber = "0" + orderingAddressViewModel.phoneNumber;
                        }
                        foreach (OrderingAddressProductViewModel orderingAddressProductViewModel in orderingAddressViewModel.products)
                        {
                            if (orderingAddressProductViewModel.productType != null)
                            {
                                string[] productTypeIds = orderingAddressProductViewModel.productType.Split(",");
                                orderingAddressProductViewModel.productTypes = new List<ProductTypeViewModel>();
                                foreach (string productId in productTypeIds)
                                {
                                    int id = 0;
                                    if (int.TryParse(productId, out id))
                                    {
                                        var productType = productTypes.Where(p => p.Id == id).FirstOrDefault();
                                        if (productType != null)
                                            orderingAddressProductViewModel.productTypes.Add(_mapper.Map<ProductTypeViewModel>(productType));
                                    }
                                }
                            }
                            if (orderingAddressProductViewModel.packaging != null)
                            {
                                string[] packagingIds = orderingAddressProductViewModel.packaging.Split(",");
                                orderingAddressProductViewModel.packagings = new List<ProductPackagingViewModel>();
                                foreach (string packagingId in packagingIds)
                                {
                                    int id = 0;
                                    if (int.TryParse(packagingId, out id))
                                    {
                                        var packaging = packagings.Where(p => p.Id == id).FirstOrDefault();
                                        if (packaging != null)
                                            orderingAddressProductViewModel.packagings.Add(_mapper.Map<ProductPackagingViewModel>(packaging));
                                    }
                                }
                            }
                        }
                    }
                    foreach (OrderingContainerViewModel container in dataOrdering.containers)
                    {
                        if (container.shippingContactPhoneCode == "+66" && container.shippingContactPhoneNumber != null && container.shippingContactPhoneNumber.Trim() != "")
                        {
                            container.shippingContactPhoneNumber = "0" + container.shippingContactPhoneNumber;
                        }
                        if (container.yardContactPhoneCode == "+66" && container.yardContactPhoneNumber != null && container.yardContactPhoneNumber.Trim() != "")
                        {
                            container.yardContactPhoneNumber = "0" + container.yardContactPhoneNumber;
                        }
                        if (container.linerContactPhoneCode == "+66" && container.linerContactPhoneNumber != null && container.linerContactPhoneNumber.Trim() != "")
                        {
                            container.linerContactPhoneNumber = "0" + container.linerContactPhoneNumber;
                        }
                    }
                }
                return new Response<IEnumerable<OrderingWithCustomerViewModel>>(orderingViewModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
