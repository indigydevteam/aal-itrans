﻿using AutoMapper;
using iTrans.Transportation.Application.DTOs;
using iTrans.Transportation.Application.DTOs.Ordering;
using iTrans.Transportation.Application.DTOs.UserLevel;
using iTrans.Transportation.Application.DTOs.UserLevelCondition;
using iTrans.Transportation.Application.DTOs.WelcomeInformation;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.Orderings.Queries
{
    public class GetAllOrderingQuery : IRequest<Response<IEnumerable<OrderingViewModel>>>
    {

    }
    public class GetAllOrdingQueryHandler : IRequestHandler<GetAllOrderingQuery, Response<IEnumerable<OrderingViewModel>>>
    {
        private readonly IOrderingRepositoryAsync _orderingRepository;
        private readonly IMapper _mapper;
        public GetAllOrdingQueryHandler(IOrderingRepositoryAsync orderingRepository, IMapper mapper)
        {
            _orderingRepository = orderingRepository;
            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<OrderingViewModel>>> Handle(GetAllOrderingQuery request, CancellationToken cancellationToken)
        {
            var userlevel = await _orderingRepository.GetAllAsync();
            var OrderingViewModel = _mapper.Map<IEnumerable<OrderingViewModel>>(userlevel);
            return new Response<IEnumerable<OrderingViewModel>>(OrderingViewModel);
        }
    }
}
