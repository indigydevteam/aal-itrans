﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.Ordering;
using iTrans.Transportation.Application.DTOs.OrderingAddress;
using iTrans.Transportation.Application.DTOs.OrderingAddressProduct;
using iTrans.Transportation.Application.DTOs.OrderingContainer;
using iTrans.Transportation.Application.DTOs.OrderingDriver;
using iTrans.Transportation.Application.DTOs.OrderingStatus;
using iTrans.Transportation.Application.DTOs.ProductPackaging;
using iTrans.Transportation.Application.DTOs.ProductType;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using LinqKit;
using MediatR;


namespace iTrans.Transportation.Application.Features.Orderings.Queries
{
    public class GetOrderingByTrackingCodeQuery : IRequest<Response<OrderingViewModel>>
    {
        public string TrackingCode { get; set; }
    }
    public class GetOrderingByTrackingCodeQueryHandler : IRequestHandler<GetOrderingByTrackingCodeQuery, Response<OrderingViewModel>>
    {
        private readonly IOrderingRepositoryAsync _orderingRepository;
        private readonly IDriverRepositoryAsync _driverRepository;
        private readonly IOrderingTrackingCodeRepositoryAsync _orderingTrackingCodeRepository;
        private readonly IOrderingAddressRepositoryAsync _orderingAddressRepository;
        private readonly IProductTypeRepositoryAsync _productTypeRepository;
        private readonly IProductPackagingRepositoryAsync _productPackagingRepository;
        private readonly IMapper _mapper;
        public GetOrderingByTrackingCodeQueryHandler(IOrderingRepositoryAsync orderingRepository, IOrderingTrackingCodeRepositoryAsync orderingTrackingCodeRepository
            , IDriverRepositoryAsync driverRepository, IProductTypeRepositoryAsync productTypeRepository, IProductPackagingRepositoryAsync productPackagingRepository, IOrderingAddressRepositoryAsync orderingAddressRepository, IMapper mapper)
        {
            _orderingRepository = orderingRepository;
            _orderingTrackingCodeRepository = orderingTrackingCodeRepository;
            _orderingAddressRepository = orderingAddressRepository;
            _productTypeRepository = productTypeRepository;
            _productPackagingRepository = productPackagingRepository;
            _driverRepository = driverRepository;
            _mapper = mapper;
        }
        public async Task<Response<OrderingViewModel>> Handle(GetOrderingByTrackingCodeQuery request, CancellationToken cancellationToken)
        {
            try
            {
                
                var folderDetails = Path.Combine(Directory.GetCurrentDirectory(), $"Shared\\OrderingStatus.json");
                var statusJson = System.IO.File.ReadAllText(folderDetails);
                List<OrderingStatusViewModel> orderingStatus = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OrderingStatusViewModel>>(statusJson);
                #region
                var trackingCode = (await _orderingTrackingCodeRepository.FindByCondition(x => x.TrackingCode.Equals(request.TrackingCode)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (trackingCode == null)
                {
                    throw new ApiException($"trackingCode Not Found.");
                }
                if (trackingCode.Section == "ordering")
                {
                    var productTypes = (await _productTypeRepository.FindByCondition(x => x.Active.Equals(true)).ConfigureAwait(false)).AsQueryable().ToList();
                    var packagings = (await _productPackagingRepository.FindByCondition(x => x.Active.Equals(true)).ConfigureAwait(false)).AsQueryable().ToList();
                    var dataObject = (await _orderingRepository.FindByCondition(x => x.TrackingCode.Equals(request.TrackingCode)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                    var ordering = _mapper.Map<OrderingViewModel>(dataObject);
                    if (ordering.status != null)
                    {
                        int orderingStatusId = ordering.status;
                        if (orderingStatusId == 0)
                        {
                            var orderingAddress = ordering.addresses.Where(x => x.status != 3).OrderByDescending(x => x.Modified).ToList();
                            if (orderingAddress.Count > 0)
                            {
                                orderingStatusId = orderingAddress[0].status;
                            }
                            //var orderingAddress = ordering.addresses.Where(x => x.status != 3).OrderBy(x => x.sequence).ToList();
                            //if (orderingAddress.Count > 0)
                            //{
                            //    orderingStatusId = orderingAddress[0].status;
                            //}
                        }
                        ordering.statusObj = orderingStatus.Where(o => o.id == orderingStatusId).FirstOrDefault();
                    }

                    if (ordering.addresses != null) ordering.addresses = ordering.addresses.OrderBy(x => x.sequence).ToList();
                    foreach (OrderingAddressViewModel orderingAddressViewModel in ordering.addresses)
                    {
                        if (orderingAddressViewModel.phoneCode == "+66" && orderingAddressViewModel.phoneNumber != null && orderingAddressViewModel.phoneNumber.Trim() != "")
                        {
                            orderingAddressViewModel.phoneNumber = "0" + orderingAddressViewModel.phoneNumber;

                        }
                        if (orderingAddressViewModel.status != null)
                        {
                            orderingAddressViewModel.statusObj = orderingStatus.Where(o => o.id == orderingAddressViewModel.status).FirstOrDefault();
                        }
                        foreach (OrderingAddressProductViewModel orderingAddressProductViewModel in orderingAddressViewModel.products)
                        {
                            if (orderingAddressProductViewModel.productType != null)
                            {
                                string[] productTypeIds = orderingAddressProductViewModel.productType.Split(",");
                                orderingAddressProductViewModel.productTypes = new List<ProductTypeViewModel>();
                                foreach (string productId in productTypeIds)
                                {
                                    int id = 0;
                                    if (int.TryParse(productId, out id))
                                    {
                                        var productType = productTypes.Where(p => p.Id == id).FirstOrDefault();
                                        if (productType != null)
                                            orderingAddressProductViewModel.productTypes.Add(_mapper.Map<ProductTypeViewModel>(productType));
                                    }
                                }
                            }
                            if (orderingAddressProductViewModel.packaging != null)
                            {
                                string[] packagingIds = orderingAddressProductViewModel.packaging.Split(",");
                                orderingAddressProductViewModel.packagings = new List<ProductPackagingViewModel>();
                                foreach (string packagingId in packagingIds)
                                {
                                    int id = 0;
                                    if (int.TryParse(packagingId, out id))
                                    {
                                        var packaging = packagings.Where(p => p.Id == id).FirstOrDefault();
                                        if (packaging != null)
                                            orderingAddressProductViewModel.packagings.Add(_mapper.Map<ProductPackagingViewModel>(packaging));
                                    }
                                }
                            }
                        }
                    }
                    foreach (OrderingContainerViewModel container in ordering.containers)
                    {
                        if (container.shippingContactPhoneCode == "+66" && container.shippingContactPhoneNumber != null && container.shippingContactPhoneNumber.Trim() != "")
                        {
                            container.shippingContactPhoneNumber = "0" + container.shippingContactPhoneNumber;
                        }
                        if (container.yardContactPhoneCode == "+66" && container.yardContactPhoneNumber != null && container.yardContactPhoneNumber.Trim() != "")
                        {
                            container.yardContactPhoneNumber = "0" + container.yardContactPhoneNumber;
                        }
                        if (container.linerContactPhoneCode == "+66" && container.linerContactPhoneNumber != null && container.linerContactPhoneNumber.Trim() != "")
                        {
                            container.linerContactPhoneNumber = "0" + container.linerContactPhoneNumber;
                        }
                    }
                    return new Response<OrderingViewModel>(ordering);
                }
                else
                {
                    var DataObject = (await _orderingAddressRepository.FindByCondition(a => a.TrackingCode == request.TrackingCode).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                    if (DataObject != null)
                    {
                        var productTypes = (await _productTypeRepository.FindByCondition(x => x.Active.Equals(true)).ConfigureAwait(false)).AsQueryable().ToList();
                        var packagings = (await _productPackagingRepository.FindByCondition(x => x.Active.Equals(true)).ConfigureAwait(false)).AsQueryable().ToList();

                        Ordering orderingObj = new Ordering();
                         
                        if (DataObject.Ordering != null)
                        {
                            orderingObj.Id = DataObject.Ordering.Id;
                            orderingObj.Distance = DataObject.Ordering.Distance;
                            orderingObj.EstimateTime = DataObject.Ordering.EstimateTime;
                            orderingObj.CurrentLocation = DataObject.Ordering.CurrentLocation;
                            orderingObj.Status = DataObject.Ordering.Status;
                            orderingObj.TrackingCode = DataObject.Ordering.TrackingCode;
                            orderingObj.TotalDistance = DataObject.Ordering.TotalDistance;
                            orderingObj.TotalEstimateTime = DataObject.Ordering.TotalEstimateTime;
                            orderingObj.Distance = DataObject.Ordering.Distance;
                            orderingObj.EstimateTime = DataObject.Ordering.EstimateTime;
                            // orderingObj.OrderNumber = DataObject.Ordering.OrderNumber;
                        }
                        orderingObj.Addresses = new List<OrderingAddress>();
                        orderingObj.Addresses.Add(DataObject);
                        orderingObj.Drivers = new List<OrderingDriver>();

                        var productContainer = DataObject.AddressProducts.Where(x => x.Name == "#container#").FirstOrDefault();
                        if (productContainer != null)
                        {
                            orderingObj.Containers = new List<OrderingContainer>();
                            foreach (OrderingContainer contaciner in DataObject.Ordering.Containers)
                            {
                                orderingObj.Containers.Add(contaciner);
                            }
                        }
                        foreach (OrderingDriver driver in DataObject.Ordering.Drivers)
                        {
                            orderingObj.Drivers.Add(driver);
                        }
                        var ordering = _mapper.Map<OrderingViewModel>(orderingObj);

                        foreach (OrderingDriverViewModel driver in ordering.drivers)
                        {
                            var existDriver = (await _driverRepository.FindByCondition(x => x.Id.Equals(driver.driverId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                            OrderingDriverViewModel tempDriver = _mapper.Map<OrderingDriverViewModel>(existDriver);
                            if (tempDriver != null && tempDriver.driverFiles != null)
                            {
                                tempDriver.driverFiles.RemoveAll(f => f.IsApprove == false);
                            }
                            if (existDriver != null)
                            {
                                driver.title = tempDriver.title;
                                driver.firstName = tempDriver.firstName;
                                driver.middleName = tempDriver.middleName;
                                driver.lastName = tempDriver.lastName;
                                driver.name = tempDriver.name;
                                driver.identityNumber = tempDriver.identityNumber;
                                driver.contactPersonTitle = tempDriver.contactPersonTitle;
                                driver.contactPersonFirstName = tempDriver.contactPersonFirstName;
                                driver.contactPersonMiddleName = tempDriver.contactPersonMiddleName;
                                driver.contactPersonLastName = tempDriver.contactPersonLastName;
                                driver.phoneCode = tempDriver.phoneCode;
                                if (tempDriver.phoneCode == "+66" && tempDriver.phoneNumber != null && tempDriver.phoneNumber.Trim() != "")
                                {
                                    driver.phoneNumber = "0" + tempDriver.phoneNumber;
                                }
                                driver.email = tempDriver.email;
                                driver.level = tempDriver.level;
                                driver.driverLevel = tempDriver.driverLevel;
                                driver.facbook = tempDriver.facbook;
                                driver.line = tempDriver.line;
                                driver.twitter = tempDriver.twitter;
                                driver.whatapp = tempDriver.whatapp;
                                driver.driverFiles = tempDriver.driverFiles;
                                driver.star = tempDriver.star;
                                driver.rating = tempDriver.rating;
                                driver.grade = tempDriver.grade;
                            }
                        }
                        if (ordering.status != null)
                        {
                            int orderingStatusId = ordering.status;
                            if (orderingStatusId == 0)
                            {
                                var orderingAddress = ordering.addresses.Where(x => x.status != 0).OrderBy(x => x.sequence).ToList();
                                if (orderingAddress.Count > 0)
                                {
                                    orderingStatusId = orderingAddress[0].status;
                                }
                            }
                            ordering.statusObj = orderingStatus.Where(o => o.id == orderingStatusId).FirstOrDefault();
                        }
                        
                        if (ordering.addresses != null) ordering.addresses = ordering.addresses.OrderBy(x => x.sequence).ToList();
                        foreach (OrderingAddressViewModel orderingAddressViewModel in ordering.addresses)
                        {
                            if (orderingAddressViewModel.phoneCode == "+66" && orderingAddressViewModel.phoneNumber != null && orderingAddressViewModel.phoneNumber.Trim() != "")
                            {
                                orderingAddressViewModel.phoneNumber = "0" + orderingAddressViewModel.phoneNumber;

                            }
                            if (orderingAddressViewModel.status != null)
                            {
                                orderingAddressViewModel.statusObj = orderingStatus.Where(o => o.id == orderingAddressViewModel.status).FirstOrDefault();
                            }
                            if (orderingAddressViewModel.products != null) orderingAddressViewModel.products = orderingAddressViewModel.products.OrderBy(x => x.sequence).ToList();
                            foreach (OrderingAddressProductViewModel orderingAddressProductViewModel in orderingAddressViewModel.products)
                            {
                                if (orderingAddressProductViewModel.productType != null)
                                {
                                    string[] productTypeIds = orderingAddressProductViewModel.productType.Split(",");
                                    orderingAddressProductViewModel.productTypes = new List<ProductTypeViewModel>();
                                    foreach (string productId in productTypeIds)
                                    {
                                        int id = 0;
                                        if (int.TryParse(productId, out id))
                                        {
                                            var productType = productTypes.Where(p => p.Id == id).FirstOrDefault();
                                            if (productType != null)
                                                orderingAddressProductViewModel.productTypes.Add(_mapper.Map<ProductTypeViewModel>(productType));
                                        }
                                    }
                                }
                                if (orderingAddressProductViewModel.packaging != null)
                                {
                                    string[] packagingIds = orderingAddressProductViewModel.packaging.Split(",");
                                    orderingAddressProductViewModel.packagings = new List<ProductPackagingViewModel>();
                                    foreach (string packagingId in packagingIds)
                                    {
                                        int id = 0;
                                        if (int.TryParse(packagingId, out id))
                                        {
                                            var packaging = packagings.Where(p => p.Id == id).FirstOrDefault();
                                            if (packaging != null)
                                                orderingAddressProductViewModel.packagings.Add(_mapper.Map<ProductPackagingViewModel>(packaging));
                                        }
                                    }
                                }
                            }
                        }
                        foreach (OrderingContainerViewModel container in ordering.containers)
                        {
                            if (container.shippingContactPhoneCode == "+66" && container.shippingContactPhoneNumber != null && container.shippingContactPhoneNumber.Trim() != "")
                            {
                                container.shippingContactPhoneNumber = "0" + container.shippingContactPhoneNumber;
                            }
                            if (container.yardContactPhoneCode == "+66" && container.yardContactPhoneNumber != null && container.yardContactPhoneNumber.Trim() != "")
                            {
                                container.yardContactPhoneNumber = "0" + container.yardContactPhoneNumber;
                            }
                            if (container.linerContactPhoneCode == "+66" && container.linerContactPhoneNumber != null && container.linerContactPhoneNumber.Trim() != "")
                            {
                                container.linerContactPhoneNumber = "0" + container.linerContactPhoneNumber;
                            }
                        }
                        return new Response<OrderingViewModel>(ordering);
                    }
                    Ordering emptyOrder = new Ordering();
                    return new Response<OrderingViewModel>(_mapper.Map<OrderingViewModel>(emptyOrder));
                }
                #endregion
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
