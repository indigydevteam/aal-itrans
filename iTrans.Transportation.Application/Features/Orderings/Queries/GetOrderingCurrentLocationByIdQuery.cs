﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.Ordering;
using iTrans.Transportation.Application.DTOs.OrderingAddress;
using iTrans.Transportation.Application.DTOs.OrderingAddressProduct;
using iTrans.Transportation.Application.DTOs.OrderingDriverReserve;
using iTrans.Transportation.Application.DTOs.OrderingDriverReserveStatus;
using iTrans.Transportation.Application.DTOs.OrderingProduct;
using iTrans.Transportation.Application.DTOs.OrderingStatus;
using iTrans.Transportation.Application.DTOs.ProductPackaging;
using iTrans.Transportation.Application.DTOs.ProductType;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using LinqKit;
using MediatR;


namespace iTrans.Transportation.Application.Features.Orderings.Queries
{
    public class GetOrderingCurrentLocationByIdQuery : IRequest<Response<OrderingCurrentLocationViewModel>>
    {
        public Guid OrderingId { get; set; }
    }
    public class GetOrderingCurrentLocationByIdQueryHandler : IRequestHandler<GetOrderingCurrentLocationByIdQuery, Response<OrderingCurrentLocationViewModel>>
    {
        private readonly IOrderingRepositoryAsync _orderingRepository;
        private readonly IProductTypeRepositoryAsync _productTypeRepository;
        private readonly IProductPackagingRepositoryAsync _productPackagingRepository;
        private readonly IMapper _mapper;
        public GetOrderingCurrentLocationByIdQueryHandler(IOrderingRepositoryAsync orderingRepository, IProductTypeRepositoryAsync productTypeRepository, IProductPackagingRepositoryAsync productPackagingRepository, IMapper mapper)
        {
            _orderingRepository = orderingRepository;
            _productTypeRepository = productTypeRepository;
            _productPackagingRepository = productPackagingRepository;
            _mapper = mapper;
        }
        public async Task<Response<OrderingCurrentLocationViewModel>> Handle(GetOrderingCurrentLocationByIdQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var dataObject = (await _orderingRepository.FindByCondition(x => x.Id.Equals(request.OrderingId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                OrderingCurrentLocationViewModel currentLocation = new OrderingCurrentLocationViewModel();
                if (dataObject != null)
                {
                    //currentLocation.id = dataObject.Id;
                    currentLocation.currentLocation = dataObject.CurrentLocation;
                    currentLocation.distance = dataObject.Distance;
                    currentLocation.estimateTime = dataObject.EstimateTime;
                }
                return new Response<OrderingCurrentLocationViewModel>(currentLocation);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }

}
