﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.Ordering;
using iTrans.Transportation.Application.DTOs.OrderingAddress;
using iTrans.Transportation.Application.DTOs.OrderingAddressProduct;
using iTrans.Transportation.Application.DTOs.OrderingContainer;
using iTrans.Transportation.Application.DTOs.OrderingDriver;
using iTrans.Transportation.Application.DTOs.OrderingProduct;
using iTrans.Transportation.Application.DTOs.OrderingStatus;
using iTrans.Transportation.Application.DTOs.ProductPackaging;
using iTrans.Transportation.Application.DTOs.ProductType;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using LinqKit;
using MediatR;
namespace iTrans.Transportation.Application.Features.Orderings.Queries
{
    public class GetOrderingByCorporateQuery : IRequest<Response<IEnumerable<OrderingViewModel>>>
    {
        public Guid CorporateId { set; get; }
        public string Month { set; get; }
        public string Year { set; get; }
        public string Search { set; get; }
        public int Status { set; get; }
        public int PageNumber { set; get; }
        public int PageSize { set; get; }
        public Guid UserId { get; set; }
        public string UserRole { get; set; }
    }
    public class GetOrderingByCorporateQueryHandler : IRequestHandler<GetOrderingByCorporateQuery, Response<IEnumerable<OrderingViewModel>>>
    {
        private readonly IOrderingRepositoryAsync _orderingRepository;
        private readonly IProductTypeRepositoryAsync _productTypeRepository;
        private readonly IProductPackagingRepositoryAsync _productPackagingRepository;
        private readonly IDriverRepositoryAsync _driverRepository;
        private readonly IMapper _mapper;
        public GetOrderingByCorporateQueryHandler(IOrderingRepositoryAsync orderingRepository, IProductTypeRepositoryAsync productTypeRepository
             , IDriverRepositoryAsync driverRepository, IProductPackagingRepositoryAsync productPackagingRepository, IMapper mapper)
        {
            _orderingRepository = orderingRepository;
            _productTypeRepository = productTypeRepository;
            _productPackagingRepository = productPackagingRepository;
            _driverRepository = driverRepository;
            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<OrderingViewModel>>> Handle(GetOrderingByCorporateQuery request, CancellationToken cancellationToken)
        {
            try
            {
                List<Domain.ProductType> productTypes = (await _productTypeRepository.FindByCondition(x => x.Active.Equals(true)).ConfigureAwait(false)).AsQueryable().ToList();
                List<Domain.ProductPackaging> packagings = (await _productPackagingRepository.FindByCondition(x => x.Active.Equals(true)).ConfigureAwait(false)).AsQueryable().ToList();

                string orderStatusPath = $"Shared\\orderingstatus.json";
                if (request.UserRole != null && request.UserRole.Trim() != "")
                {
                    orderStatusPath = $"Shared\\" + request.UserRole.Trim().ToLower() + "_orderingstatus.json";
                }
                var orderingStatusPathFile = Path.Combine(Directory.GetCurrentDirectory(), orderStatusPath);
                var statusJson = System.IO.File.ReadAllText(orderingStatusPathFile);

                List<OrderingStatusViewModel> orderingStatus = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OrderingStatusViewModel>>(statusJson);

                var validFilter = _mapper.Map<GetOrderingByCorporateQuery>(request);
                Expression<Func<Domain.Ordering, bool>> expression = PredicateBuilder.New<Domain.Ordering>(false);
                expression = x => x.Driver.Corparate.Equals(validFilter.CorporateId);
                int month = 0;
                if (validFilter.Month != null && int.TryParse(validFilter.Month, out month))
                {
                    expression = expression.And(x => x.Addresses.Where(a => a.Date.Month == month).Count() > 0);
                }
                int year = 0;
                if (validFilter.Year != null && int.TryParse(validFilter.Year, out year))
                {
                    expression = expression.And(x => x.Addresses.Where(a => a.Date.Year == year).Count() > 0);
                }
                if (validFilter.Status != null && validFilter.Status != 0)
                {

                    if (validFilter.Status == 1)
                    {
                        expression = expression.And(x => (x.Status >= 0 && x.Status <= 13) || x.Status == 17);
                    }
                    else if (validFilter.Status == 2)
                    {
                        expression = expression.And(x => x.Status == 16);
                    }
                    if (validFilter.Status == 3)
                    {
                        expression = expression.And(x => x.Status == 14 || x.Status == 15);
                    }
                }
                if (validFilter.Search != null && validFilter.Search.Trim() != "")
                {
                    Expression<Func<Domain.Ordering, bool>> expressionSearch = PredicateBuilder.New<Domain.Ordering>(false);

                    string search = validFilter.Search.Trim();
                    var productList = productTypes.Where(p => p.Name_TH.Contains(search) || p.Name_ENG.Contains(search)).ToList();

                    Expression<Func<Domain.Ordering, bool>> expressionProduct = PredicateBuilder.New<Domain.Ordering>(false);
                    foreach (Domain.ProductType productType in productList)
                    {
                        expressionProduct = o => (o.Products.Where(p => p.ProductType.ToLower().Contains(productType.Id.ToString())).Count() > 0);
                    };
                    //expressionProduct = expressionProduct.Or(o => (o.Products.Where(p => p.ProductTypeDetail.Contains(search)).Count() > 0));

                    var packagingList = packagings.Where(p => p.Name_TH.Contains(search) || p.Name_ENG.Contains(search)).ToList();

                    Expression<Func<Domain.Ordering, bool>> expressionPackaging = PredicateBuilder.New<Domain.Ordering>(false);
                    foreach (Domain.ProductPackaging packaging in packagingList)
                    {
                        expressionPackaging = o => (o.Products.Where(p => p.Packaging.ToLower().Contains(packaging.Id.ToString())).Count() > 0);
                    };
                    //expressionPackaging = expressionPackaging.Or(o => (o.Products.Where(p => p.PackagingDetail.Contains(search)).Count() > 0));

                    Expression<Func<Domain.Ordering, bool>> expressionDriver = PredicateBuilder.New<Domain.Ordering>(false);
                    expressionDriver = o => o.Drivers.Where(d => d.Name.ToLower().ToLower().Contains(search)).Count() > 0;

                    Expression<Func<Domain.Ordering, bool>> expressionCar = PredicateBuilder.New<Domain.Ordering>(false);
                    expressionCar = o => o.Cars.Where(c => c.CarType.Name_TH.ToLower().Contains(search) || c.CarType.Name_ENG.ToLower().Contains(search)
                                                                               //|| c.CarList.Name_TH.Contains(search) || c.CarList.Name_ENG.Contains(search)
                                                                               //|| c.CarDescription.Name_TH.Contains(search) || c.CarDescription.Name_ENG.Contains(search) || c.CarDescriptionDetail.Contains(search)
                                                                               //|| c.CarFeature.Name_TH.Contains(search) || c.CarFeature.Name_ENG.Contains(search) || c.CarFeatureDetail.Contains(search)
                                                                               || c.CarSpecification.Name_TH.ToLower().Contains(search) || c.CarSpecification.Name_ENG.ToLower().Contains(search) //|| c.CarSpecificationDetail.Contains(search)
                                                                               || c.Note.ToLower().Contains(search)).Count() > 0;

                    Expression<Func<Domain.Ordering, bool>> expressionAddress = PredicateBuilder.New<Domain.Ordering>(false);
                    expressionAddress = o => o.Addresses.Where(a => a.Country.Name_TH.ToLower().Contains(search) || a.Country.Name_ENG.ToLower().Contains(search)
                                                                                 || a.Province.Name_TH.ToLower().Contains(search) || a.Province.Name_ENG.ToLower().Contains(search)
                                                                                 || a.District.Name_TH.ToLower().Contains(search) || a.District.Name_ENG.ToLower().Contains(search)
                                                                                 || a.Subdistrict.Name_TH.ToLower().Contains(search) || a.Subdistrict.Name_ENG.ToLower().Contains(search) || a.Subdistrict.PostCode.ToLower().Contains(search)
                                                                                 ).Count() > 0;


                    //expressionSearch.Or(expressionProduct);
                    //expressionSearch.Or(expressionPackaging);
                    Expression<Func<Domain.Ordering, bool>> expressionStatus = PredicateBuilder.New<Domain.Ordering>(false);
                    bool isExpressionStatus = false;
                    if (validFilter.Status != 1)
                    {
                        isExpressionStatus = true;
                        // Expression<Func<Domain.Ordering, bool>> expressionStatus = PredicateBuilder.New<Domain.Ordering>(false);
                        var searchStatus = orderingStatus.Where(x => x.th.Contains(search) || x.th.Contains(search)).ToList();
                        bool firstLoop = true;
                        foreach (OrderingStatusViewModel orderStatus in searchStatus)
                        {
                            if (firstLoop)
                            {
                                if (orderStatus.id >= 4 && orderStatus.id <= 12)
                                {
                                    expressionStatus = o => o.Status == 0 && o.Addresses.Where(a => a.Status == orderStatus.id).Count() > 0;
                                }
                                else
                                {
                                    expressionStatus = o => o.Status == orderStatus.id;
                                }
                                //expressionStatus = o => o.Status == orderStatus.id || o.Addresses.Where(a => a.Status == orderStatus.id).Count() > 0;
                            }
                            else
                            {
                                if (orderStatus.id >= 4 && orderStatus.id <= 12)
                                {
                                    expressionStatus = expressionStatus.Or(o => o.Status == 0 && o.Addresses.Where(a => a.Status == orderStatus.id).Count() > 0);
                                }
                                else
                                {
                                    expressionStatus = expressionStatus.Or(o => o.Status == orderStatus.id);
                                }
                                // expressionStatus = expressionStatus.Or(o => o.Status == orderStatus.id || o.Addresses.Where(a => a.Status == orderStatus.id).Count() > 0);
                            }
                            firstLoop = false;
                        }
                    }
                    expressionSearch = o => o.TrackingCode.Contains(search);
                    if (isExpressionStatus)
                        expression = expression.And(expressionSearch.Or(expressionProduct).Or(expressionPackaging).Or(expressionDriver).Or(expressionCar).Or(expressionAddress).Or(expressionStatus));
                    else
                        expression = expression.And(expressionSearch.Or(expressionProduct).Or(expressionPackaging).Or(expressionDriver).Or(expressionCar).Or(expressionAddress));
                }
                #region ordering
                Expression<Func<Domain.Ordering, DateTime>> orderExpression = x => x.Created;
                bool isDescending = true;
                #endregion
                var ordering = await _orderingRepository.FindByConditionWithPage(expression, orderExpression, isDescending, validFilter.PageNumber, validFilter.PageSize);
                var orderingViewModel = _mapper.Map<IEnumerable<OrderingViewModel>>(ordering);
                foreach (OrderingViewModel dataOrdering in orderingViewModel)
                {
                    if (dataOrdering.orderingPrice == 0 && dataOrdering.orderingDesiredPrice > 0)
                    {
                        dataOrdering.orderingPrice = dataOrdering.orderingDesiredPrice;
                    }
                    foreach (OrderingDriverViewModel driver in dataOrdering.drivers)
                    {
                        var existDriver = (await _driverRepository.FindByCondition(x => x.Id.Equals(driver.driverId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                        OrderingDriverViewModel tempDriver = _mapper.Map<OrderingDriverViewModel>(existDriver);
                        if (tempDriver != null && tempDriver.driverFiles != null)
                        {
                            tempDriver.driverFiles.RemoveAll(f => f.IsApprove == false);
                        }
                        if (existDriver != null)
                        {
                            driver.title = tempDriver.title;
                            driver.firstName = tempDriver.firstName;
                            driver.middleName = tempDriver.middleName;
                            driver.lastName = tempDriver.lastName;
                            driver.name = tempDriver.name;
                            driver.identityNumber = tempDriver.identityNumber;
                            driver.contactPersonTitle = tempDriver.contactPersonTitle;
                            driver.contactPersonFirstName = tempDriver.contactPersonFirstName;
                            driver.contactPersonMiddleName = tempDriver.contactPersonMiddleName;
                            driver.contactPersonLastName = tempDriver.contactPersonLastName;
                            driver.phoneCode = tempDriver.phoneCode;
                            if (tempDriver.phoneCode == "+66" && tempDriver.phoneNumber != null && tempDriver.phoneNumber.Trim() != "")
                            {
                                driver.phoneNumber = "0" + tempDriver.phoneNumber;
                            }
                            driver.email = tempDriver.email;
                            driver.level = tempDriver.level;
                            driver.driverLevel = tempDriver.driverLevel;
                            driver.facbook = tempDriver.facbook;
                            driver.line = tempDriver.line;
                            driver.twitter = tempDriver.twitter;
                            driver.whatapp = tempDriver.whatapp;
                            driver.driverFiles = tempDriver.driverFiles;
                            driver.star = tempDriver.star;
                            driver.rating = tempDriver.rating;
                            driver.grade = tempDriver.grade;
                        }
                    }

                    if (dataOrdering.status != null)
                    {
                        int orderingStatusId = dataOrdering.status;
                        if (orderingStatusId == 0)
                        {
                            var orderingAddress = dataOrdering.addresses.Where(x => x.status != 3).OrderByDescending(x => x.Modified).ToList();
                            if (orderingAddress.Count > 0)
                            {
                                orderingStatusId = orderingAddress[0].status;
                            }
                            //var orderingAddress = dataOrdering.addresses.Where(x => x.status != 3).OrderBy(x => x.sequence).ToList();
                            //if (orderingAddress.Count > 0)
                            //{
                            //    orderingStatusId = orderingAddress[0].status;
                            //}
                        }
                        dataOrdering.statusObj = orderingStatus.Where(o => o.id == orderingStatusId).FirstOrDefault();
                    }

                    if (dataOrdering.products != null) dataOrdering.products = dataOrdering.products.OrderBy(x => x.sequence).ToList();
                    foreach (OrderingProductViewModel productViewModel in dataOrdering.products)
                    {
                        if (productViewModel.productType != null)
                        {
                            string[] productTypeIds = productViewModel.productType.Split(",");
                            productViewModel.productTypes = new List<ProductTypeViewModel>();
                            foreach (string productId in productTypeIds)
                            {
                                int id = 0;
                                if (int.TryParse(productId, out id))
                                {
                                    var productType = productTypes.Where(p => p.Id == id).FirstOrDefault();
                                    if (productType != null)
                                        productViewModel.productTypes.Add(_mapper.Map<ProductTypeViewModel>(productType));
                                }
                            }
                        }
                        if (productViewModel.packaging != null)
                        {
                            string[] packagingIds = productViewModel.packaging.Split(",");
                            productViewModel.packagings = new List<ProductPackagingViewModel>();
                            foreach (string packagingId in packagingIds)
                            {
                                int id = 0;
                                if (int.TryParse(packagingId, out id))
                                {
                                    var packaging = packagings.Where(p => p.Id == id).FirstOrDefault();
                                    if (packaging != null)
                                        productViewModel.packagings.Add(_mapper.Map<ProductPackagingViewModel>(packaging));
                                }
                            }
                        }
                    }

                    if (dataOrdering.addresses != null) dataOrdering.addresses = dataOrdering.addresses.OrderBy(x => x.sequence).ToList();
                    foreach (OrderingAddressViewModel orderingAddressViewModel in dataOrdering.addresses)
                    {
                        if (orderingAddressViewModel.phoneCode == "+66" && orderingAddressViewModel.phoneNumber != null && orderingAddressViewModel.phoneNumber.Trim() != "")
                        {
                            orderingAddressViewModel.phoneNumber = "0" + orderingAddressViewModel.phoneNumber;
                        }
                        if (orderingAddressViewModel.driverWaitingToPickUpDate.Equals(new DateTime()))
                        {
                            orderingAddressViewModel.driverWaitingToPickUpDate = null;
                        }
                        if (orderingAddressViewModel.driverWaitingToDeliveryDate.Equals(new DateTime()))
                        {
                            orderingAddressViewModel.driverWaitingToDeliveryDate = null;
                        }
                        if (orderingAddressViewModel.status != null)
                        {
                            orderingAddressViewModel.statusObj = orderingStatus.Where(o => o.id == orderingAddressViewModel.status).FirstOrDefault();
                        }

                        if (orderingAddressViewModel.products != null) orderingAddressViewModel.products = orderingAddressViewModel.products.OrderBy(x => x.sequence).ToList();
                        foreach (OrderingAddressProductViewModel orderingAddressProductViewModel in orderingAddressViewModel.products)
                        {
                            if (orderingAddressProductViewModel.productType != null)
                            {
                                string[] productTypeIds = orderingAddressProductViewModel.productType.Split(",");
                                orderingAddressProductViewModel.productTypes = new List<ProductTypeViewModel>();
                                foreach (string productId in productTypeIds)
                                {
                                    int id = 0;
                                    if (int.TryParse(productId, out id))
                                    {
                                        var productType = productTypes.Where(p => p.Id == id).FirstOrDefault();
                                        if (productType != null)
                                            orderingAddressProductViewModel.productTypes.Add(_mapper.Map<ProductTypeViewModel>(productType));
                                    }
                                }
                            }
                            if (orderingAddressProductViewModel.packaging != null)
                            {
                                string[] packagingIds = orderingAddressProductViewModel.packaging.Split(",");
                                orderingAddressProductViewModel.packagings = new List<ProductPackagingViewModel>();
                                foreach (string packagingId in packagingIds)
                                {
                                    int id = 0;
                                    if (int.TryParse(packagingId, out id))
                                    {
                                        var packaging = packagings.Where(p => p.Id == id).FirstOrDefault();
                                        if (packaging != null)
                                            orderingAddressProductViewModel.packagings.Add(_mapper.Map<ProductPackagingViewModel>(packaging));
                                    }
                                }
                            }
                        }
                    }

                    foreach (OrderingContainerViewModel container in dataOrdering.containers)
                    {
                        if (container.shippingContactPhoneCode == "+66" && container.shippingContactPhoneNumber != null && container.shippingContactPhoneNumber.Trim() != "")
                        {
                            container.shippingContactPhoneNumber = "0" + container.shippingContactPhoneNumber;
                        }
                        if (container.yardContactPhoneCode == "+66" && container.yardContactPhoneNumber != null && container.yardContactPhoneNumber.Trim() != "")
                        {
                            container.yardContactPhoneNumber = "0" + container.yardContactPhoneNumber;
                        }
                        if (container.linerContactPhoneCode == "+66" && container.linerContactPhoneNumber != null && container.linerContactPhoneNumber.Trim() != "")
                        {
                            container.linerContactPhoneNumber = "0" + container.linerContactPhoneNumber;
                        }
                    }
                }
                return new Response<IEnumerable<OrderingViewModel>>(orderingViewModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
