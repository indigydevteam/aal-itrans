﻿using AutoMapper;
using iTrans.Transportation.Application.DTOs.Ordering;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;


namespace iTrans.Transportation.Application.Features.Orderings.Queries
{
    public class GetAllOrderingParameter : IRequest<PagedResponse<IEnumerable<OrderingViewModel>>>
    {
        public string Search { set; get; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }

    }
    public class GetAllOrderingParameterHandler : IRequestHandler<GetAllOrderingParameter, PagedResponse<IEnumerable<OrderingViewModel>>>
    {
        private readonly IOrderingRepositoryAsync _orderingRepository;
        private readonly IMapper _mapper;
        private Expression<Func<Ordering, bool>> expression;

        public GetAllOrderingParameterHandler(IOrderingRepositoryAsync orderingRepository, IMapper mapper)
        {
            _orderingRepository = orderingRepository;
            _mapper = mapper;
        }

        public async Task<PagedResponse<IEnumerable<OrderingViewModel>>> Handle(GetAllOrderingParameter request, CancellationToken cancellationToken)
        {
            var validFilter = _mapper.Map<GetAllOrderingParameter>(request);
            if (validFilter.Search != null && validFilter.Search.Trim() != "")
            {
                expression = x => (x.TrackingCode.Contains(validFilter.Search.Trim()));
            }
            int itemCount = _orderingRepository.GetItemCount(expression);
            //Expression<Func<Ordering, bool>> expression = x => x.Note != "";
            var data = await _orderingRepository.FindByConditionWithPage(expression, validFilter.PageNumber, validFilter.PageSize);
            var OrderingViewModel = _mapper.Map<IEnumerable<OrderingViewModel>>(data);
            return new PagedResponse<IEnumerable<OrderingViewModel>>(OrderingViewModel, validFilter.PageNumber, validFilter.PageSize, itemCount);
        }
    }
}