﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.Ordering;
using iTrans.Transportation.Application.DTOs.OrderingAddress;
using iTrans.Transportation.Application.DTOs.OrderingAddressProduct;
using iTrans.Transportation.Application.DTOs.OrderingContainer;
using iTrans.Transportation.Application.DTOs.OrderingDriver;
using iTrans.Transportation.Application.DTOs.OrderingDriverReserve;
using iTrans.Transportation.Application.DTOs.OrderingDriverReserveStatus;
using iTrans.Transportation.Application.DTOs.OrderingProduct;
using iTrans.Transportation.Application.DTOs.OrderingStatus;
using iTrans.Transportation.Application.DTOs.ProductPackaging;
using iTrans.Transportation.Application.DTOs.ProductType;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using LinqKit;
using MediatR;


namespace iTrans.Transportation.Application.Features.Orderings.Queries
{
    public class GetOrderingByIdQuery : IRequest<Response<OrderingViewModel>>
    {
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public string UserRole { get; set; }
    }
    public class GetOrderingByIdQueryHandler : IRequestHandler<GetOrderingByIdQuery, Response<OrderingViewModel>>
    {
        private readonly IOrderingRepositoryAsync _orderingRepository;
        private readonly IDriverRepositoryAsync _driverRepository;
        private readonly IProductTypeRepositoryAsync _productTypeRepository;
        private readonly IProductPackagingRepositoryAsync _productPackagingRepository;
        private readonly IMapper _mapper;
        public GetOrderingByIdQueryHandler(IOrderingRepositoryAsync orderingRepository, IProductTypeRepositoryAsync productTypeRepository
            , IDriverRepositoryAsync driverRepository, IProductPackagingRepositoryAsync productPackagingRepository, IMapper mapper)
        {
            _orderingRepository = orderingRepository;
            _productTypeRepository = productTypeRepository;
            _productPackagingRepository = productPackagingRepository;
            _driverRepository = driverRepository;
            _mapper = mapper;
        }
        public async Task<Response<OrderingViewModel>> Handle(GetOrderingByIdQuery request, CancellationToken cancellationToken)
        {
            try
            { 
                var productTypes = (await _productTypeRepository.FindByCondition(x => x.Active.Equals(true)).ConfigureAwait(false)).AsQueryable().ToList();
                var packagings = (await _productPackagingRepository.FindByCondition(x => x.Active.Equals(true)).ConfigureAwait(false)).AsQueryable().ToList();
                string orderStatusPath = $"Shared\\orderingstatus.json";
                if (request.UserRole != null && request.UserRole.Trim() != "")
                {
                    orderStatusPath = $"Shared\\" + request.UserRole.Trim().ToLower() + "_orderingstatus.json";
                }
                var orderingStatusPathFile = Path.Combine(Directory.GetCurrentDirectory(), orderStatusPath);
                var statusJson = System.IO.File.ReadAllText(orderingStatusPathFile);
                List<OrderingStatusViewModel> orderingStatus = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OrderingStatusViewModel>>(statusJson);

                var driverReserveStatusPathFile = Path.Combine(Directory.GetCurrentDirectory(), $"Shared\\DriverReserveStatus.json");
                var driverReserveStatusJson = System.IO.File.ReadAllText(driverReserveStatusPathFile);
                List<OrderingDriverReserveStatusViewModel> driverReserveStatus = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OrderingDriverReserveStatusViewModel>>(driverReserveStatusJson);

                var dataObject = (await _orderingRepository.FindByCondition(x => x.Id.Equals(request.Id)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                var ordering = _mapper.Map<OrderingViewModel>(dataObject);
                if (dataObject != null)
                {
                    if (ordering.orderingPrice == 0 && ordering.orderingDesiredPrice > 0)
                    {
                        ordering.orderingPrice = ordering.orderingDesiredPrice;
                    }
                    foreach (OrderingDriverViewModel driver in ordering.drivers)
                    {
                        var existDriver = (await _driverRepository.FindByCondition(x => x.Id.Equals(driver.driverId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                        OrderingDriverViewModel tempDriver = _mapper.Map<OrderingDriverViewModel>(existDriver);
                        if (tempDriver != null && tempDriver.driverFiles != null)
                        {
                            tempDriver.driverFiles.RemoveAll(f => f.IsApprove == false && f.IsDelete == true);
                        }
                        if (existDriver != null)
                        {
                            driver.title = tempDriver.title;
                            driver.firstName = tempDriver.firstName;
                            driver.middleName = tempDriver.middleName;
                            driver.lastName = tempDriver.lastName;
                            driver.name = tempDriver.name;
                            driver.identityNumber = tempDriver.identityNumber;
                            driver.contactPersonTitle = tempDriver.contactPersonTitle;
                            driver.contactPersonFirstName = tempDriver.contactPersonFirstName;
                            driver.contactPersonMiddleName = tempDriver.contactPersonMiddleName;
                            driver.contactPersonLastName = tempDriver.contactPersonLastName;
                            driver.phoneCode = tempDriver.phoneCode;
                            if (tempDriver.phoneCode == "+66" && tempDriver.phoneNumber != null && tempDriver.phoneNumber.Trim() != "")
                            {
                                driver.phoneNumber = "0" + tempDriver.phoneNumber;
                            }
                            driver.email = tempDriver.email;
                            driver.level = tempDriver.level;
                            driver.driverLevel = tempDriver.driverLevel;
                            driver.facbook = tempDriver.facbook;
                            driver.line = tempDriver.line;
                            driver.twitter = tempDriver.twitter;
                            driver.whatapp = tempDriver.whatapp;
                            driver.driverFiles = tempDriver.driverFiles;
                            driver.star = tempDriver.star;
                            driver.rating = tempDriver.rating;
                            driver.grade = tempDriver.grade;
                        }
                    }
                    if (ordering.status != null)
                    {
                        int orderingStatusId = ordering.status;
                        if(orderingStatusId == 0)
                        {
                            var orderingAddress = ordering.addresses.Where(x => x.status != 3).OrderByDescending(x => x.Modified).ToList();
                            if (orderingAddress.Count > 0)
                            {
                                orderingStatusId = orderingAddress[0].status;
                            }
                            //foreach (OrderingAddressViewModel address in orderingAddress)
                            //{
                            //    orderingStatusId = address.status;
                            //}
                        }
                        ordering.statusObj = orderingStatus.Where(o => o.id == orderingStatusId).FirstOrDefault();
                        //if (request.UserRole != null)
                        //{
                        //    if (request.UserRole == "driver" && ordering.statusObj != null && ordering.statusObj.driver_th != null && ordering.statusObj.driver_th.Trim() != "")
                        //    {
                        //        ordering.statusObj.th = ordering.statusObj.driver_th;
                        //    }
                        //    if (request.UserRole == "driver" && ordering.statusObj != null && ordering.statusObj.driver_eng != null && ordering.statusObj.driver_eng.Trim() != "")
                        //    {
                        //        ordering.statusObj.eng = ordering.statusObj.driver_eng;
                        //    }
                        //    if (request.UserRole == "driver" && ordering.statusObj != null && ordering.statusObj.driver_description != null && ordering.statusObj.driver_description.Trim() != "")
                        //    {
                        //        ordering.statusObj.description = ordering.statusObj.driver_description;
                        //    }
                        //}
                    }
                    if (ordering.products != null) ordering.products = ordering.products.OrderBy(x => x.sequence).ToList();
                    foreach (OrderingProductViewModel productViewModel in ordering.products)
                    {
                        if (productViewModel.productType != null)
                        {
                            string[] productTypeIds = productViewModel.productType.Split(",");
                            productViewModel.productTypes = new List<ProductTypeViewModel>();
                            foreach (string productId in productTypeIds)
                            {
                                int id = 0;
                                if (int.TryParse(productId, out id))
                                {
                                    var productType = productTypes.Where(p => p.Id == id).FirstOrDefault();
                                    if (productType != null)
                                        productViewModel.productTypes.Add(_mapper.Map<ProductTypeViewModel>(productType));
                                }
                            }
                        }
                        if (productViewModel.packaging != null)
                        {
                            string[] packagingIds = productViewModel.packaging.Split(",");
                            productViewModel.packagings = new List<ProductPackagingViewModel>();
                            foreach (string packagingId in packagingIds)
                            {
                                int id = 0;
                                if (int.TryParse(packagingId, out id))
                                {
                                    var packaging = packagings.Where(p => p.Id == id).FirstOrDefault();
                                    if (packaging != null)
                                        productViewModel.packagings.Add(_mapper.Map<ProductPackagingViewModel>(packaging));
                                }
                            }
                        }
                    }

                    if (ordering.addresses != null) ordering.addresses = ordering.addresses.OrderBy(x => x.sequence).ToList();
                    foreach (OrderingAddressViewModel orderingAddressViewModel in ordering.addresses)
                    {
                        if (orderingAddressViewModel.phoneCode == "+66" && orderingAddressViewModel.phoneNumber != null && orderingAddressViewModel.phoneNumber.Trim() != "")
                        {
                            orderingAddressViewModel.phoneNumber = "0" + orderingAddressViewModel.phoneNumber;
                        }
                        if (orderingAddressViewModel.driverWaitingToPickUpDate.Equals(new DateTime()))
                        {
                            orderingAddressViewModel.driverWaitingToPickUpDate = null;
                        }
                        if (orderingAddressViewModel.driverWaitingToDeliveryDate.Equals(new DateTime()))
                        {
                            orderingAddressViewModel.driverWaitingToDeliveryDate = null;
                        }
                        if (orderingAddressViewModel.status != null)
                        {
                            orderingAddressViewModel.statusObj = orderingStatus.Where(o => o.id == orderingAddressViewModel.status).FirstOrDefault();
                        }

                        if (orderingAddressViewModel.products != null) orderingAddressViewModel.products = orderingAddressViewModel.products.OrderBy(x => x.sequence).ToList();
                        foreach (OrderingAddressProductViewModel orderingAddressProductViewModel in orderingAddressViewModel.products)
                        {
                            if (orderingAddressProductViewModel.productType != null)
                            {
                                string[] productTypeIds = orderingAddressProductViewModel.productType.Split(",");
                                orderingAddressProductViewModel.productTypes = new List<ProductTypeViewModel>();
                                foreach (string productId in productTypeIds)
                                {
                                    int id = 0;
                                    if (int.TryParse(productId, out id))
                                    {
                                        var productType = productTypes.Where(p => p.Id == id).FirstOrDefault();
                                        if (productType != null)
                                            orderingAddressProductViewModel.productTypes.Add(_mapper.Map<ProductTypeViewModel>(productType));
                                    }
                                }
                            }
                            if (orderingAddressProductViewModel.packaging != null)
                            {
                                string[] packagingIds = orderingAddressProductViewModel.packaging.Split(",");
                                orderingAddressProductViewModel.packagings = new List<ProductPackagingViewModel>();
                                foreach (string packagingId in packagingIds)
                                {
                                    int id = 0;
                                    if (int.TryParse(packagingId, out id))
                                    {
                                        var packaging = packagings.Where(p => p.Id == id).FirstOrDefault();
                                        if (packaging != null)
                                            orderingAddressProductViewModel.packagings.Add(_mapper.Map<ProductPackagingViewModel>(packaging));
                                    }
                                }
                            }
                        }
                    }

                    foreach (OrderingContainerViewModel container in ordering.containers)
                    {
                        if (container.shippingContactPhoneCode == "+66" && container.shippingContactPhoneNumber != null && container.shippingContactPhoneNumber.Trim() != "")
                        {
                            container.shippingContactPhoneNumber = "0" + container.shippingContactPhoneNumber;
                        }
                        if (container.yardContactPhoneCode == "+66" && container.yardContactPhoneNumber != null && container.yardContactPhoneNumber.Trim() != "")
                        {
                            container.yardContactPhoneNumber = "0" + container.yardContactPhoneNumber;
                        }
                        if (container.linerContactPhoneCode == "+66" && container.linerContactPhoneNumber != null && container.linerContactPhoneNumber.Trim() != "")
                        {
                            container.linerContactPhoneNumber = "0" + container.linerContactPhoneNumber;
                        }
                    }

                    foreach (OrderingDriverReserveViewModel orderingDriverReserve in ordering.driverReserve)
                    {
                        orderingDriverReserve.statusObj = driverReserveStatus.Where(o => o.id == orderingDriverReserve.status).FirstOrDefault();
                    }
                }
                return new Response<OrderingViewModel>(ordering);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
