﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.CustomerInbox;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.CustomerInboxs.Queries
{
   public class GetCustomerInboxByCustomerQuery : IRequest<Response<IEnumerable<CustomerInboxViewModel>>>
    {
        public Guid CustomerId { get; set; }
        public int Status { get; set; }
        public string Search { set; get; }
        public int PageNumber { set; get; }
        public int PageSize { set; get; }
    }
    public class GetCustomerInboxByCustomerQueryHandler : IRequestHandler<GetCustomerInboxByCustomerQuery, Response<IEnumerable<CustomerInboxViewModel>>>
    {
        private readonly INotificationRepositoryAsync _notificationRepository;
        private readonly IMapper _mapper;
        public GetCustomerInboxByCustomerQueryHandler(INotificationRepositoryAsync notificationRepository, IMapper mapper)
        {
            _notificationRepository = notificationRepository;
            _mapper = mapper;
        }
        public async Task<Response<IEnumerable<CustomerInboxViewModel>>> Handle(GetCustomerInboxByCustomerQuery request, CancellationToken cancellationToken)
        {
            var notification = (await _notificationRepository.FindByCondition(x => x.UserId.Equals(request.CustomerId) && x.IsDelete == false).ConfigureAwait(false)).AsQueryable().ToList();
            
            
            return new Response<IEnumerable<CustomerInboxViewModel>>(_mapper.Map<IEnumerable<CustomerInboxViewModel>>(null));
        }
    }
}
