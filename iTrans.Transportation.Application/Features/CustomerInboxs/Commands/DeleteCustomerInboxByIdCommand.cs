﻿using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
namespace iTrans.Transportation.Application.Features.CustomerInboxs.Commands
{
    public class DeleteCustomerInboxByIdCommand : IRequest<Response<int>>
    {
        public Guid CustomerId { get; set; }
        public int Id { get; set; }
        public class DeleteCustomerInboxByIdCommandHandler : IRequestHandler<DeleteCustomerInboxByIdCommand, Response<int>>
        {
            private readonly ICustomerInboxRepositoryAsync _customerInboxRepository;
            public DeleteCustomerInboxByIdCommandHandler(ICustomerInboxRepositoryAsync customerInboxRepository)
            {
                _customerInboxRepository = customerInboxRepository;
            }
            public async Task<Response<int>> Handle(DeleteCustomerInboxByIdCommand command, CancellationToken cancellationToken)
            {
                var customerInbox = (await _customerInboxRepository.FindByCondition(x => x.Id == command.Id && x.CustomerId == command.CustomerId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (customerInbox == null)
                {
                    throw new ApiException($"Customer Inbox Not Found.");
                }
                else
                {
                    customerInbox.IsDelete = true;
                    await _customerInboxRepository.UpdateAsync(customerInbox);
                    return new Response<int>(customerInbox.Id);
                }
            }
        }
    }
}
