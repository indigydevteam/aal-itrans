﻿using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.DriverCarLocations.Commands
{
    public class DeleteDriverCarLocationByIdCommand : IRequest<Response<int>>
    {
        public int Id { get; set; }
        public class DeleteDriverCarLocationByIdCommandHandler : IRequestHandler<DeleteDriverCarLocationByIdCommand, Response<int>>
        {
            private readonly IDriverCarLocationRepositoryAsync _driverCarLocationRepository;
            public DeleteDriverCarLocationByIdCommandHandler(IDriverCarLocationRepositoryAsync driverCarLocationRepository)
            {
                _driverCarLocationRepository = driverCarLocationRepository;
            }
            public async Task<Response<int>> Handle(DeleteDriverCarLocationByIdCommand command, CancellationToken cancellationToken)
            {
                var driverCarLocation = (await _driverCarLocationRepository.FindByCondition(x => x.Id == command.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (driverCarLocation == null)
                {
                    throw new ApiException($"DriverCarLocation Not Found.");
                }
                else
                {
                    await _driverCarLocationRepository.DeleteAsync(driverCarLocation);
                    return new Response<int>(driverCarLocation.Id);
                }
            }
        }
    }
}
