﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.DriverCarLocations.Commands
{
    public class UpdateDriverCarLocationCommandValidator : AbstractValidator<UpdateDriverCarLocationCommand>
    {
        private readonly IDriverCarLocationRepositoryAsync driverCarLocationRepository;
        public UpdateDriverCarLocationCommandValidator(IDriverCarLocationRepositoryAsync driverCarLocationRepository)
        {
            this.driverCarLocationRepository = driverCarLocationRepository;

            RuleFor(p => p.DriverCarId)
               .NotNull().WithMessage("{PropertyName} is required.");

        }
    }
}
