﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;

namespace iTrans.Transportation.Application.Features.DriverCarLocations.Commands
{
    public class UpdateDriverCarLocationCommand : IRequest<Response<int>>
    {
        public int Id { get; set; }
        public int DriverCarId { get; set; }
        public int Country { get; set; }
        public int Region { get; set; }
        public int Province { get; set; }
        public int District { get; set; }
        public string LocationType { get; set; }
        public int Sequence { get; set; }
    }

    public class UpdateDriverCarLocationCommandHandler : IRequestHandler<UpdateDriverCarLocationCommand, Response<int>>
    {
        private readonly IDriverCarLocationRepositoryAsync _driverCarLocationRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;

        public UpdateDriverCarLocationCommandHandler(IDriverCarLocationRepositoryAsync driverCarLocationRepository,IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _driverCarLocationRepository = driverCarLocationRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(UpdateDriverCarLocationCommand request, CancellationToken cancellationToken)
        {
            var driverCarLocation = (await _driverCarLocationRepository.FindByCondition(x => x.Id == request.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            if (driverCarLocation == null)
            {
                throw new ApiException($"DriverCarLocation Not Found.");
            }
            else
            {
                //driverCarLocation.DriverCarId = request.DriverCarId;
                //driverCarLocation.Country = request.Country;
                //driverCarLocation.Region = request.Region;
                //driverCarLocation.Province = request.Province;
                //driverCarLocation.District = request.District;
                driverCarLocation.LocationType = request.LocationType;
                driverCarLocation.Sequence = request.Sequence;
                driverCarLocation.Modified = DateTime.UtcNow;
                await _driverCarLocationRepository.UpdateAsync(driverCarLocation);
                var log = new CreateAppLog(_applicationLogRepository);
                log.Create("Drivercar", "Drivercar Location", "Edit", Newtonsoft.Json.JsonConvert.SerializeObject(driverCarLocation));
                return new Response<int>(driverCarLocation.Id);
            }
        }
    }
}
