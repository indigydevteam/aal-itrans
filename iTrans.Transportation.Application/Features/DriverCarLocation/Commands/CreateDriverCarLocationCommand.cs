﻿using AutoMapper;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Interfaces.Services;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.DriverCarLocations.Commands
{
    public partial class CreateDriverCarLocationCommand : IRequest<Response<int>>
    {
        public  int DriverCarId { get; set; }
        public  int Country { get; set; }
        public  int Region { get; set; }
        public  int Province { get; set; }
        public  int District { get; set; }
        public string LocationType { get; set; }
        public  int Sequence { get; set; }

    }
    public class CreateDriverCarLocationCommandHandler : IRequestHandler<CreateDriverCarLocationCommand, Response<int>>
    {
        private readonly IDriverCarLocationRepositoryAsync _driverCarLocationRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;
        public CreateDriverCarLocationCommandHandler(IDriverCarLocationRepositoryAsync driverCarLocationRepository, IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _driverCarLocationRepository = driverCarLocationRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(CreateDriverCarLocationCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var driverCarLocation = _mapper.Map<DriverCarLocation>(request);
                driverCarLocation.Created = DateTime.UtcNow;
                driverCarLocation.Modified = DateTime.UtcNow;
                var driverCarLocationObject = await _driverCarLocationRepository.AddAsync(driverCarLocation);
                var log = new CreateAppLog(_applicationLogRepository);
                log.Create("Drivercar", "Drivercar Location", "Create", Newtonsoft.Json.JsonConvert.SerializeObject(driverCarLocation));
                return new Response<int>(driverCarLocationObject.Id);
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
    }
}
