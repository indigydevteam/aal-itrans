﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.DriverCarLocation;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.DriverCarLocations.Queries
{
    public class GetDriverCarLocationByIdQuery : IRequest<Response<DriverCarLocationViewModel>>
    {
        public int Id { get; set; }
    }
    public class GetDriverCarLocationByIdQueryHandler : IRequestHandler<GetDriverCarLocationByIdQuery, Response<DriverCarLocationViewModel>>
    {
        private readonly IDriverCarLocationRepositoryAsync _driverCarLocationRepository;
        private readonly IMapper _mapper;
        public GetDriverCarLocationByIdQueryHandler(IDriverCarLocationRepositoryAsync driverCarLocationRepository, IMapper mapper)
        {
            _driverCarLocationRepository = driverCarLocationRepository;
            _mapper = mapper;
        }
        public async Task<Response<DriverCarLocationViewModel>> Handle(GetDriverCarLocationByIdQuery request, CancellationToken cancellationToken)
        {
            var driverObject = (await _driverCarLocationRepository.FindByCondition(x => x.Id.Equals(request.Id)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            return new Response<DriverCarLocationViewModel>(_mapper.Map<DriverCarLocationViewModel>(driverObject));
        }
    }
}
