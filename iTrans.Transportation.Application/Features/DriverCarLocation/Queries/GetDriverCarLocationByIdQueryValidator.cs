﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using iTrans.Transportation.Application.Interfaces.Repositories;
using FluentValidation;

namespace iTrans.Transportation.Application.Features.DriverCarLocations.Queries
{
    public class GetDriverCarLocationByIdQueryValidator : AbstractValidator<GetDriverCarLocationByIdQuery>
    {
        private readonly IDriverCarLocationRepositoryAsync driverCarLocationRepository;

        public GetDriverCarLocationByIdQueryValidator(IDriverCarLocationRepositoryAsync driverCarLocationRepository)
        {
            this.driverCarLocationRepository = driverCarLocationRepository;
            RuleFor(p => p.Id)
               .NotEmpty().WithMessage("{PropertyName} is required.")
               .NotNull()
               .MustAsync(IsDriverCarLocationExists).WithMessage("{PropertyName} not exists.");
        }

        private async Task<bool> IsDriverCarLocationExists(int DriverId, CancellationToken cancellationToken)
        {
            var userObject = (await driverCarLocationRepository.FindByCondition(x => x.Id.Equals(DriverId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            if (userObject != null)
            {
                return true;
            }
            return false;
        }
    }
}
