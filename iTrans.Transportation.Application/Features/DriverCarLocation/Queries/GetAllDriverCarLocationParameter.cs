﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.DriverCarLocation;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;

namespace iTrans.Transportation.Application.Features.DriverCarLocations.Queries
{
    public class GetAllDriverCarLocationParameter : IRequest<PagedResponse<IEnumerable<DriverCarLocationViewModel>>>
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
    }
    public class GetAllDriverCarLocationParameterHandler : IRequestHandler<GetAllDriverCarLocationParameter, PagedResponse<IEnumerable<DriverCarLocationViewModel>>>
    {
        private readonly IDriverCarLocationRepositoryAsync _driverCarLocationRepository;
        private readonly IMapper _mapper;
        public GetAllDriverCarLocationParameterHandler(IDriverCarLocationRepositoryAsync driverCarLocationRepository, IMapper mapper)
        {
            _driverCarLocationRepository = driverCarLocationRepository;
            _mapper = mapper;
        }

        public async Task<PagedResponse<IEnumerable<DriverCarLocationViewModel>>> Handle(GetAllDriverCarLocationParameter request, CancellationToken cancellationToken)
        {
            var validFilter = _mapper.Map<GetAllDriverCarLocationParameter>(request);
            Expression<Func<DriverCarLocation, bool>> expression = x => x.DriverCar.Id != null;
            var driverCarLocation = await _driverCarLocationRepository.FindByConditionWithPage(expression, validFilter.PageNumber, validFilter.PageSize);
            var driverCarLocationViewModel = _mapper.Map<IEnumerable<DriverCarLocationViewModel>>(driverCarLocation);
            return new PagedResponse<IEnumerable<DriverCarLocationViewModel>>(driverCarLocationViewModel, validFilter.PageNumber, validFilter.PageSize);
        }
    }
}