﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.DriverCarLocation;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.DriverCarLocations.Queries
{
    public class GetAllDriverCarLocationQuery : IRequest<Response<IEnumerable<DriverCarLocationViewModel>>>
    {
        
    }
    public class GetAllDriverCarLocationQueryHandler : IRequestHandler<GetAllDriverCarLocationQuery, Response<IEnumerable<DriverCarLocationViewModel>>>
    {
        private readonly IDriverCarLocationRepositoryAsync _driverCarLocationRepository;
        private readonly IMapper _mapper;
        public GetAllDriverCarLocationQueryHandler(IDriverCarLocationRepositoryAsync driverCarLocationRepository, IMapper mapper)
        {
            _driverCarLocationRepository = driverCarLocationRepository;
            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<DriverCarLocationViewModel>>> Handle(GetAllDriverCarLocationQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var validFilter = _mapper.Map<GetAllDriverCarLocationQuery>(request);
                var driver = await _driverCarLocationRepository.GetAllAsync();
                var driverCarLocationViewModel = _mapper.Map<IEnumerable<DriverCarLocationViewModel>>(driver);
                return new Response<IEnumerable<DriverCarLocationViewModel>>(driverCarLocationViewModel);
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
    }
}
