﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;

namespace iTrans.Transportation.Application.Features.DriverAnnouncements.Commands
{
    public class UpdateDriverAnnouncementCommand : IRequest<Response<int>>
    {
        public  int Id { get; set; }
        public  Guid DriverId { get; set; }
        public  int AcceptProduct { get; set; }
        public  string AcceptProductDetail { get; set; }
        public  int AcceptPackaging { get; set; }
        public  string AcceptPackagingDetail { get; set; }
        public  int RejectProduct { get; set; }
        public  string RejectProductDetail { get; set; }
        public  int RejectPackaging { get; set; }
        public  string RejectPackagingDetail { get; set; }
        public  DateTime SendDate { get; set; }
        public  DateTime ReceiveDate { get; set; }
        public  string CarRegistration { get; set; }
        public  bool AllRent { get; set; }
        public  bool Additional { get; set; }
        public  string AdditionalDetail { get; set; }
        public  string Note { get; set; }
        public  int OrderId { get; set; }

    }

    public class UpdateDriverAnnouncementCommandHandler : IRequestHandler<UpdateDriverAnnouncementCommand, Response<int>>
    {
        private readonly IDriverAnnouncementRepositoryAsync _driverAnnouncementRepository;
        private readonly IMapper _mapper;

        public UpdateDriverAnnouncementCommandHandler(IDriverAnnouncementRepositoryAsync driverAnnouncementRepository, IMapper mapper)
        {
            _driverAnnouncementRepository = driverAnnouncementRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(UpdateDriverAnnouncementCommand request, CancellationToken cancellationToken)
        {
            var driverAnnouncement = (await _driverAnnouncementRepository.FindByCondition(x => x.Id == request.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            if (driverAnnouncement == null)
            {
                throw new ApiException($"DriverAnnouncement Not Found.");
            }
            else
            {
                driverAnnouncement.Driver.Id = request.DriverId;
                //driverAnnouncement.AcceptProduct = request.AcceptProduct;
                driverAnnouncement.AcceptProductDetail = request.AcceptProductDetail;
                //driverAnnouncement.AcceptPackaging = request.AcceptPackaging;
                driverAnnouncement.AcceptPackagingDetail = request.AcceptPackagingDetail;
                //driverAnnouncement.RejectProduct = request.RejectProduct;
                driverAnnouncement.RejectProductDetail = request.RejectProductDetail;
                //driverAnnouncement.RejectPackaging = request.RejectPackaging;
                driverAnnouncement.RejectPackagingDetail = request.RejectPackagingDetail;
                driverAnnouncement.SendDate = request.SendDate;
                driverAnnouncement.ReceiveDate = request.ReceiveDate;
                driverAnnouncement.CarRegistration = request.CarRegistration;
                driverAnnouncement.AllRent = request.AllRent;
                driverAnnouncement.Additional = request.Additional;
                driverAnnouncement.AdditionalDetail = request.AdditionalDetail;
                driverAnnouncement.Note = request.Note;
                //driverAnnouncement.OrderId = request.OrderId;
       
                await _driverAnnouncementRepository.UpdateAsync(driverAnnouncement);
                return new Response<int>(driverAnnouncement.Id);
            }
        }
    }
}
