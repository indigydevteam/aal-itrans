﻿using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.DriverAnnouncements.Commands
{
    public class DeleteAnnouncementByIdCommand : IRequest<Response<int>>
    {
        public int Id { get; set; }
        public Guid DriverId { get; set; }
        public class DeleteAnnouncementByIdCommandHandler : IRequestHandler<DeleteAnnouncementByIdCommand, Response<int>>
        {
            private readonly IDriverAnnouncementRepositoryAsync _driverAnnouncementRepository;
            private readonly IDriverAnnouncementLocationRepositoryAsync _driverAnnouncementLocationRepository;
            public DeleteAnnouncementByIdCommandHandler(IDriverAnnouncementRepositoryAsync driverAnnouncementRepository, IDriverAnnouncementLocationRepositoryAsync driverAnnouncementLocationRepository)
            {
                _driverAnnouncementRepository = driverAnnouncementRepository;
                _driverAnnouncementLocationRepository = driverAnnouncementLocationRepository;
            }
            public async Task<Response<int>> Handle(DeleteAnnouncementByIdCommand command, CancellationToken cancellationToken)
            {
                var driverAnnouncement = (await _driverAnnouncementRepository.FindByCondition(x => x.Id == command.Id && x.Driver.Id.Equals(command.DriverId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (driverAnnouncement == null)
                {
                    throw new ApiException($"DriverAnnouncement Not Found.");
                }
                else
                {
                    (await _driverAnnouncementLocationRepository.CreateSQLQuery("DELETE Driver_AnnouncementLocation where DriverAnnouncementId = '" + command.Id.ToString() + "'").ConfigureAwait(false)).ExecuteUpdate();
                    await _driverAnnouncementRepository.DeleteAsync(driverAnnouncement);
                    return new Response<int>(driverAnnouncement.Id);
                }
            }
        }
    }
}
