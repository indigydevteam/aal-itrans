﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using iTrans.Transportation.Application.Helper;
using System.Text.RegularExpressions;

namespace iTrans.Transportation.Application.Features.DriverAnnouncements.Commands
{
    public class CreateAnnouncementCommandValidator : AbstractValidator<CreateAnnouncementCommand>
    {
        private readonly IDriverRepositoryAsync driverRepository;

        public CreateAnnouncementCommandValidator(IDriverRepositoryAsync driverRepository)
        {
            this.driverRepository = driverRepository;

            RuleFor(p => p.UserId)
                .NotEmpty().WithMessage("{PropertyName} is required.")
                .NotNull()
                .MustAsync(IsExistDriver).WithMessage("driver or car not exists.");

        }

        private async Task<bool> IsExistDriver(Guid? value, CancellationToken cancellationToken)
        {
            var driverObject = (await driverRepository.FindByCondition(x => x.Id.Equals(value.Value)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            if (driverObject == null)
            {
                return false;
            }
            if (driverObject.Cars.Count == 0 )
            {
                return false;
            }
            return true;
        }
    }
}
