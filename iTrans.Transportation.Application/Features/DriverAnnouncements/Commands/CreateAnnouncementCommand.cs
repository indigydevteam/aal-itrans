﻿using AutoMapper;
using iTrans.Transportation.Application.DTOs.DriverAnnouncement;
using iTrans.Transportation.Application.DTOs.DriverAnnouncementLocation;
using iTrans.Transportation.Application.Enums;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Interfaces.Services;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using iTrans.Transportation.Domain.Entities;
using MediatR;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.DriverAnnouncements.Commands
{
    public partial class CreateAnnouncementCommand : IRequest<Response<int>>
    {
        public Guid? UserId { get; set; }
        public Guid DriverId { get; set; }
        public int[] AcceptProduct { get; set; }
        public string AcceptProductDetail { get; set; }
        public int[] AcceptPackaging { get; set; }
        public string AcceptPackagingDetail { get; set; }
        public int[] RejectProduct { get; set; }
        public string RejectProductDetail { get; set; }
        public int[] RejectPackaging { get; set; }
        public string RejectPackagingDetail { get; set; }
        public DateTime SendDate { get; set; }
        public DateTime ReceiveDate { get; set; }
        public string CarRegistration { get; set; }
        public bool AllRent { get; set; }
        public bool Additional { get; set; }
        public string AdditionalDetail { get; set; }
        public decimal DesiredPrice { get; set; }
        public string Note { get; set; }
        //public int Status { get; set; }
        //public Guid OrderId { get; set; }
         public string Location { get; set; }
        //public List<LocationViewModel> Location { get; set; }
    }
    public class CreateAnnouncementCommandHandler : IRequestHandler<CreateAnnouncementCommand, Response<int>>
    {
        private readonly IDriverRepositoryAsync _driverRepository;
        private readonly IDriverAnnouncementRepositoryAsync _driverAnnouncementRepository;
        private readonly IDriverAnnouncementLocationRepositoryAsync _driverAnnouncementLocationRepository;
        private readonly IProductTypeRepositoryAsync _productTypeRepository;
        private readonly IProductPackagingRepositoryAsync _productPackagingRepository;
        private readonly ICountryRepositoryAsync _countryRepository;
        private readonly IProvinceRepositoryAsync _provinceRepository;
        private readonly IDistrictRepositoryAsync _districtRepository;
        private readonly ISubdistrictRepositoryAsync _subdistrictRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;
        public CreateAnnouncementCommandHandler(IDriverRepositoryAsync driverRepository, IDriverAnnouncementRepositoryAsync driverAnnouncementRepository, IDriverAnnouncementLocationRepositoryAsync driverAnnouncementLocationRepository, IProductTypeRepositoryAsync productTypeRepository, IProductPackagingRepositoryAsync productPackagingRepository,
            ICountryRepositoryAsync countryRepository, IProvinceRepositoryAsync provinceRepository, IDistrictRepositoryAsync districtRepository, ISubdistrictRepositoryAsync subdistrictRepository, IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _driverRepository = driverRepository;
            _driverAnnouncementRepository = driverAnnouncementRepository;
            _driverAnnouncementLocationRepository = driverAnnouncementLocationRepository;
            _productTypeRepository = productTypeRepository;
            _productPackagingRepository = productPackagingRepository;
            _countryRepository = countryRepository;
            _provinceRepository = provinceRepository;
            _districtRepository = districtRepository;
            _subdistrictRepository = subdistrictRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(CreateAnnouncementCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var productTypes = (await _productTypeRepository.FindByCondition(x => x.Active == true).ConfigureAwait(false)).AsQueryable().ToList();
                var packagings = (await _productPackagingRepository.FindByCondition(x => x.Active == true).ConfigureAwait(false)).AsQueryable().ToList();
                var driverObj = (await _driverRepository.FindByCondition(x => x.Id.Equals(request.UserId.Value)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (driverObj == null)
                {
                    throw new ApiException($"Driver Not Found.");
                }
                var driverAnnouncement = _mapper.Map<DriverAnnouncement>(request);
                driverAnnouncement.Driver = driverObj;
                if (driverObj.Cars != null)
                {
                    if (driverObj.Cars.Count > 0)
                    {
                        driverAnnouncement.CarRegistration = driverObj.Cars[0].CarRegistration;
                    }
                }

                driverAnnouncement.AcceptProduct = "";
                if (request.AcceptProduct != null)
                {
                    foreach (int productId in request.AcceptProduct)
                    {
                        var productType = productTypes.Where(p => p.Id == productId).FirstOrDefault();
                        if (productType != null)
                            driverAnnouncement.AcceptProduct = driverAnnouncement.AcceptProduct + productId + ",";
                    }
                }
                if (driverAnnouncement.AcceptProduct != "")
                    driverAnnouncement.AcceptProduct = driverAnnouncement.AcceptProduct.Remove(driverAnnouncement.AcceptProduct.Length - 1);


                driverAnnouncement.AcceptPackaging = "";
                if (request.AcceptPackaging != null)
                {
                    foreach (int packagingId in request.AcceptPackaging)
                    {
                        var packaging = packagings.Where(p => p.Id == packagingId).FirstOrDefault();
                        if (packaging != null)
                            driverAnnouncement.AcceptPackaging = driverAnnouncement.AcceptPackaging + packagingId + ",";
                    }
                }
                if (driverAnnouncement.AcceptPackaging != "")
                    driverAnnouncement.AcceptPackaging = driverAnnouncement.AcceptPackaging.Remove(driverAnnouncement.AcceptPackaging.Length - 1);

                driverAnnouncement.RejectProduct = "";
                if (request.RejectProduct != null)
                {
                    foreach (int productId in request.RejectProduct)
                    {
                        var productType = productTypes.Where(p => p.Id == productId).FirstOrDefault();
                        if (productType != null)
                            driverAnnouncement.RejectProduct = driverAnnouncement.RejectProduct + productId + ",";
                    }
                }
                if (driverAnnouncement.RejectProduct != "")
                    driverAnnouncement.RejectProduct = driverAnnouncement.RejectProduct.Remove(driverAnnouncement.RejectProduct.Length - 1);

                driverAnnouncement.RejectPackaging = "";
                if (request.RejectPackaging != null)
                {
                    foreach (int packagingId in request.RejectPackaging)
                    {
                        var packaging = packagings.Where(p => p.Id == packagingId).FirstOrDefault();
                        if (packaging != null)
                            driverAnnouncement.RejectPackaging = driverAnnouncement.RejectPackaging + packagingId + ",";
                    }
                }
                if (driverAnnouncement.RejectPackaging != "")
                    driverAnnouncement.RejectPackaging = driverAnnouncement.RejectPackaging.Remove(driverAnnouncement.RejectPackaging.Length - 1);

                //driverAnnouncement.AcceptProduct = (await _productTypeRepository.FindByCondition(x => x.Id.Equals(request.AcceptProductTypeId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                //driverAnnouncement.AcceptPackaging = (await _productPackagingRepository.FindByCondition(x => x.Id.Equals(request.AcceptProductPackagingId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                //driverAnnouncement.RejectProduct = (await _productTypeRepository.FindByCondition(x => x.Id.Equals(request.RejectProductTypeId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                //driverAnnouncement.RejectPackaging = (await _productPackagingRepository.FindByCondition(x => x.Id.Equals(request.RejectProductPackagingId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                driverAnnouncement.Created = DateTime.UtcNow;
                if (request.Location != null && request.Location != "")
                {
                     List<LocationViewModel> locations = JsonConvert.DeserializeObject<List<LocationViewModel>>(request.Location);
                    driverAnnouncement.Locations = new List<DriverAnnouncementLocation>();

                    foreach (LocationViewModel location in locations)
                    {
                        Country country = (await _countryRepository.FindByCondition(x => x.Id == location.CountryId ).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                        Province province = (await _provinceRepository.FindByCondition(x => x.Id == location.ProvinceId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                        District district = (await _districtRepository.FindByCondition(x => x.Id == location.DistrictId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                        Subdistrict subdistrict = (await _subdistrictRepository.FindByCondition(x => x.Id ==location.SubdistrictId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();

                        DriverAnnouncementLocation announcementLocation = new DriverAnnouncementLocation
                        {
                            DriverAnnouncement = driverAnnouncement,
                            Country = country,
                            Province = province,
                            District = district,
                            Subdistrict = subdistrict,
                            LocationType = location.LocationType,
                            Note = location.Note,
                            SendDate = location.SendDate,
                            ReceiveDate = location.ReceiveDate,
                            Sequence = location.Sequence,
                        };
                        driverAnnouncement.Locations.Add(announcementLocation);
                        //await _driverAnnouncementLocationRepository.AddAsync(announcementLocation);
                    }
                }
                var status = 1; //Enum.GetName(typeof(DriverAnnouncementStatus), 1);
                driverAnnouncement.Status = status;
                driverAnnouncement.Created = DateTime.Now;
                driverAnnouncement.CreatedBy = request.UserId != null ? request.UserId.ToString() : "";
                driverAnnouncement.Modified = DateTime.Now;
                driverAnnouncement.ModifiedBy = request.UserId != null ? request.UserId.ToString() : "";
                var driverAnnouncementObject = await _driverAnnouncementRepository.AddAsync(driverAnnouncement);
                var log = new CreateAppLog(_applicationLogRepository);
                log.CreateLog("Driver", "Driver Announcement", "Create", Newtonsoft.Json.JsonConvert.SerializeObject(request),request.UserId.ToString());
                return new Response<int>(driverAnnouncementObject.Id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
