﻿using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.DriverAnnouncements.Commands
{
    public class DeleteDriverAnnouncementCommand: IRequest<Response<int>>
    {
        public int Id { get; set; }
        public class DeleteDriverAnnouncementCommandHandler : IRequestHandler<DeleteDriverAnnouncementCommand, Response<int>>
        {
            private readonly IDriverAnnouncementRepositoryAsync _driverAnnouncementRepository;
            public DeleteDriverAnnouncementCommandHandler(IDriverAnnouncementRepositoryAsync driverAnnouncementRepository)
            {
                _driverAnnouncementRepository = driverAnnouncementRepository;
            }
            public async Task<Response<int>> Handle(DeleteDriverAnnouncementCommand command, CancellationToken cancellationToken)
            {
                var driverAnnouncement = (await _driverAnnouncementRepository.FindByCondition(x => x.Id == command.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (driverAnnouncement == null)
                {
                    throw new ApiException($"DriverAnnouncement Not Found.");
                }
                else
                {
                    await _driverAnnouncementRepository.DeleteAsync(driverAnnouncement);
                    return new Response<int>(driverAnnouncement.Id);
                }
            }
        }
    }
}
