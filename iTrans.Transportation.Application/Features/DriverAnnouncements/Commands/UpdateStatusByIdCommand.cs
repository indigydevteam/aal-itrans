﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.DriverAnnouncement;
using iTrans.Transportation.Application.DTOs.DriverAnnouncementStatus;
using iTrans.Transportation.Application.DTOs.Ordering;
using iTrans.Transportation.Application.DTOs.OrderingAddress;
using iTrans.Transportation.Application.DTOs.OrderingAddressProduct;
using iTrans.Transportation.Application.DTOs.OrderingProduct;
using iTrans.Transportation.Application.DTOs.OrderingStatus;
using iTrans.Transportation.Application.DTOs.ProductPackaging;
using iTrans.Transportation.Application.DTOs.ProductType;
using iTrans.Transportation.Application.Enums;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;

namespace iTrans.Transportation.Application.Features.DriverAnnouncements.Commands
{
    public class UpdateStatusByIdCommand : IRequest<Response<int>>
    {
        public Guid? idUser { get; set; }
        public int Id { get; set; }
        public Guid DriverId { get; set; }
        public Guid CustomerId { get; set; }
        public Guid OrderingId { get; set; }
        public int Status { get; set; }
        public string APP_ID { get; set; }
        public string REST_API_KEY { get; set; }
        public string AUTH_ID { get; set; }
        public Guid UserId { get; set; }
        public string UserRole { get; set; }
    }

    public class UpdateStatusByIdCommandHandler : IRequestHandler<UpdateStatusByIdCommand, Response<int>>
    {
        private readonly IDriverAnnouncementRepositoryAsync _driverAnnouncementRepository;
        private readonly IOrderingRepositoryAsync _orderingRepository;
        private readonly IProductTypeRepositoryAsync _productTypeRepository;
        private readonly IProductPackagingRepositoryAsync _productPackagingRepository;
        private readonly INotificationRepositoryAsync _notificationRepository;
        private readonly INotificationUserRepositoryAsync _notificationUserRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;

        public UpdateStatusByIdCommandHandler(IOrderingRepositoryAsync orderingRepository, IDriverAnnouncementRepositoryAsync driverAnnouncementRepository
            , INotificationRepositoryAsync notificationRepository, INotificationUserRepositoryAsync notificationUserRepository
            , IProductTypeRepositoryAsync productTypeRepository, IProductPackagingRepositoryAsync productPackagingRepository, IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _driverAnnouncementRepository = driverAnnouncementRepository;
            _productTypeRepository = productTypeRepository;
            _productPackagingRepository = productPackagingRepository;
            _orderingRepository = orderingRepository;
            _notificationRepository = notificationRepository;
            _notificationUserRepository = notificationUserRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(UpdateStatusByIdCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var driverAnnouncement = (await _driverAnnouncementRepository.FindByCondition(x => x.Id == request.Id && x.Driver.Id.Equals(request.DriverId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (driverAnnouncement == null)
                {
                    throw new ApiException($"DriverAnnouncement Not Found.");
                }
                else
                {
                    var productTypes = (await _productTypeRepository.FindByCondition(x => x.Active == true).ConfigureAwait(false)).AsQueryable().ToList();
                    var packagings = (await _productPackagingRepository.FindByCondition(x => x.Active == true).ConfigureAwait(false)).AsQueryable().ToList();
                    var ordering = (await _orderingRepository.FindByCondition(x => x.Id.Equals(request.OrderingId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                    
                    string orderStatusPath = $"Shared\\orderingstatus.json";
                    if (request.UserRole != null && request.UserRole.Trim() != "")
                    {
                        orderStatusPath = $"Shared\\" + request.UserRole.Trim().ToLower() + "_orderingstatus.json";
                    }

                    var folderStatusDetails = Path.Combine(Directory.GetCurrentDirectory(), orderStatusPath);
                    var statusOrderingJson = System.IO.File.ReadAllText(folderStatusDetails);
                    List<OrderingStatusViewModel> orderingStatus = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OrderingStatusViewModel>>(statusOrderingJson);

                    var folderDetails = Path.Combine(Directory.GetCurrentDirectory(), $"Shared\\DriverAnnouncementStatus.json");
                    var statusJson = System.IO.File.ReadAllText(folderDetails);
                    List<DriverAnnouncementStatusViewModel> announcementStatus = Newtonsoft.Json.JsonConvert.DeserializeObject<List<DriverAnnouncementStatusViewModel>>(statusJson);

                    driverAnnouncement.Status = request.Status;// Enum.GetName(typeof(DriverAnnouncementStatus), request.Status);
                    driverAnnouncement.Modified = DateTime.Now;
                    await _driverAnnouncementRepository.UpdateAsync(driverAnnouncement);
                    OrderingViewModel cloneOrdering = new OrderingViewModel();
                    if (ordering != null)
                    {
                        driverAnnouncement.Order = ordering;

                        #region
                        cloneOrdering = _mapper.Map<OrderingViewModel>(ordering);

                        if (cloneOrdering.status != null && cloneOrdering.status != 0)
                        {
                            cloneOrdering.statusObj = orderingStatus.Where(o => o.id == cloneOrdering.status).FirstOrDefault();
                        }
                        foreach (OrderingProductViewModel productViewModel in cloneOrdering.products)
                        {
                            if (productViewModel.productType != null)
                            {
                                string[] productTypeIds = productViewModel.productType.Split(",");
                                productViewModel.productTypes = new List<ProductTypeViewModel>();
                                foreach (string productId in productTypeIds)
                                {
                                    int id = 0;
                                    if (int.TryParse(productId, out id))
                                    {
                                        var productType = productTypes.Where(p => p.Id == id).FirstOrDefault();
                                        if (productType != null)
                                            productViewModel.productTypes.Add(_mapper.Map<ProductTypeViewModel>(productType));
                                    }
                                }
                            }
                            if (productViewModel.packaging != null)
                            {
                                string[] packagingIds = productViewModel.packaging.Split(",");
                                productViewModel.packagings = new List<ProductPackagingViewModel>();
                                foreach (string packagingId in packagingIds)
                                {
                                    int id = 0;
                                    if (int.TryParse(packagingId, out id))
                                    {
                                        var packaging = packagings.Where(p => p.Id == id).FirstOrDefault();
                                        if (packaging != null)
                                            productViewModel.packagings.Add(_mapper.Map<ProductPackagingViewModel>(packaging));
                                    }
                                }
                            }
                        }

                        foreach (OrderingAddressViewModel orderingAddressViewModel in cloneOrdering.addresses)
                        {
                            foreach (OrderingAddressProductViewModel orderingAddressProductViewModel in orderingAddressViewModel.products)
                            {
                                if (orderingAddressProductViewModel.productType != null)
                                {
                                    string[] productTypeIds = orderingAddressProductViewModel.productType.Split(",");
                                    orderingAddressProductViewModel.productTypes = new List<ProductTypeViewModel>();
                                    foreach (string productId in productTypeIds)
                                    {
                                        int id = 0;
                                        if (int.TryParse(productId, out id))
                                        {
                                            var productType = productTypes.Where(p => p.Id == id).FirstOrDefault();
                                            if (productType != null)
                                                orderingAddressProductViewModel.productTypes.Add(_mapper.Map<ProductTypeViewModel>(productType));
                                        }
                                    }
                                }
                                if (orderingAddressProductViewModel.packaging != null)
                                {
                                    string[] packagingIds = orderingAddressProductViewModel.packaging.Split(",");
                                    orderingAddressProductViewModel.packagings = new List<ProductPackagingViewModel>();
                                    foreach (string packagingId in packagingIds)
                                    {
                                        int id = 0;
                                        if (int.TryParse(packagingId, out id))
                                        {
                                            var packaging = packagings.Where(p => p.Id == id).FirstOrDefault();
                                            if (packaging != null)
                                                orderingAddressProductViewModel.packagings.Add(_mapper.Map<ProductPackagingViewModel>(packaging));
                                        }
                                    }
                                }
                            }
                        }
                        #endregion
                    }

                    #region
                    var announcementViewModel = _mapper.Map<AnnouncementWithDriverViewModel>(driverAnnouncement);
                    announcementViewModel.acceptProducts = new List<ProductTypeViewModel>();

                    if (announcementViewModel.status != null && announcementViewModel.status != 0)
                    {
                        announcementViewModel.statusObj = orderingStatus.Where(o => o.id == announcementViewModel.status).FirstOrDefault();
                    }

                    if (announcementViewModel.acceptProduct != null)
                    {
                        foreach (string productIdStr in announcementViewModel.acceptProduct.Split(","))
                        {
                            int id = 0;
                            if (int.TryParse(productIdStr, out id))
                            {
                                var productType = productTypes.Where(p => p.Id == id).FirstOrDefault();
                                if (productType != null)
                                {
                                    announcementViewModel.acceptProducts.Add(_mapper.Map<ProductTypeViewModel>(productType));
                                }
                            }
                        }
                    }

                    if (announcementViewModel.acceptPackaging != null)
                    {
                        announcementViewModel.acceptPackagings = new List<ProductPackagingViewModel>();
                        foreach (string packagingIdStr in announcementViewModel.acceptPackaging.Split(","))
                        {
                            int id = 0;
                            if (int.TryParse(packagingIdStr, out id))
                            {
                                var packaging = packagings.Where(p => p.Id == id).FirstOrDefault();
                                if (packaging != null)
                                {
                                    announcementViewModel.acceptPackagings.Add(_mapper.Map<ProductPackagingViewModel>(packaging));
                                }
                            }
                        }
                    }

                    if (announcementViewModel.rejectProduct != null)
                    {
                        announcementViewModel.rejectProducts = new List<ProductTypeViewModel>();
                        foreach (string productIdStr in announcementViewModel.rejectProduct.Split(","))
                        {
                            int id = 0;
                            if (int.TryParse(productIdStr, out id))
                            {
                                var productType = productTypes.Where(p => p.Id == id).FirstOrDefault();
                                if (productType != null)
                                {
                                    announcementViewModel.rejectProducts.Add(_mapper.Map<ProductTypeViewModel>(productType));
                                }
                            }
                        }
                    }

                    if (announcementViewModel.rejectPackaging != null)
                    {
                        announcementViewModel.rejectPackagings = new List<ProductPackagingViewModel>();
                        foreach (string packagingIdStr in announcementViewModel.rejectPackaging.Split(","))
                        {
                            int id = 0;
                            if (int.TryParse(packagingIdStr, out id))
                            {
                                var packaging = packagings.Where(p => p.Id == id).FirstOrDefault();
                                if (packaging != null)
                                {
                                    announcementViewModel.rejectPackagings.Add(_mapper.Map<ProductPackagingViewModel>(packaging));
                                }
                            }
                        }
                    }
                    #endregion
                    string payload = "";
                    if (ordering != null)
                    {
                        payload = "{\"driverAnnouncement\" : " + Newtonsoft.Json.JsonConvert.SerializeObject(announcementViewModel) + ",";
                        payload = payload + "\"ordering\" : " + Newtonsoft.Json.JsonConvert.SerializeObject(cloneOrdering) + "}";
                    }
                    else
                    {
                        payload = "{\"driverAnnouncement\" : " + Newtonsoft.Json.JsonConvert.SerializeObject(announcementViewModel) + "}";
                    }
                    var userId = driverAnnouncement.Driver.Id;
                    var oneSignalIds = (await _notificationUserRepository.FindByCondition(x => x.UserId.Equals(userId)).ConfigureAwait(false)).AsQueryable().Select(x => x.OneSignalId).ToList();
                    if (oneSignalIds.Count > 0)
                    {
                        Domain.Notification notification = new Domain.Notification();

                        notification.OneSignalId = oneSignalIds.FirstOrDefault();
                        notification.UserId = userId;
                        notification.OwnerId = request.DriverId;
                        notification.Title = "Announcement : " + (announcementViewModel.statusObj != null ? announcementViewModel.statusObj.eng : "");
                        notification.Detail = "Announcement : " + (announcementViewModel.statusObj != null ? announcementViewModel.statusObj.eng : "");
                        notification.ServiceCode = "Announcement";
                        //notification.ReferenceContentKey = request.ReferenceContentKey;
                        notification.Payload = Newtonsoft.Json.JsonConvert.SerializeObject(cloneOrdering);//payload;
                        notification.Created = DateTime.UtcNow;
                        notification.Modified = DateTime.UtcNow;
                        notification.CreatedBy = request.DriverId.ToString();
                        notification.ModifiedBy = request.DriverId.ToString();
                        await _notificationRepository.AddAsync(notification);

                        NotificationManager notiMgr = new NotificationManager(request.APP_ID, request.REST_API_KEY, request.AUTH_ID);
                        notiMgr.SendNotificationAsync("Announcement : " + announcementViewModel.statusObj != null ? announcementViewModel.statusObj.eng : "", "Announcement : " + announcementViewModel.statusObj != null ? announcementViewModel.statusObj.eng : "", oneSignalIds, notification.ServiceCode, "", Newtonsoft.Json.JsonConvert.SerializeObject(cloneOrdering));
                    }
                    var log = new CreateAppLog(_applicationLogRepository);
                    log.CreateLog("Driver", "Driver Status", "Edit", Newtonsoft.Json.JsonConvert.SerializeObject(request),request.idUser.ToString());
                    return new Response<int>(driverAnnouncement.Id);
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
    }
}
