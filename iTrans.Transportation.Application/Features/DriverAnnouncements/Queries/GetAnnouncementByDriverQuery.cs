﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.DriverAnnouncement;
using iTrans.Transportation.Application.DTOs.OrderingStatus;
using iTrans.Transportation.Application.DTOs.ProductPackaging;
using iTrans.Transportation.Application.DTOs.ProductType;
using iTrans.Transportation.Application.Enums;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using LinqKit;
using MediatR;

namespace iTrans.Transportation.Application.Features.DriverAnnouncements.Queries
{
    public class GetAnnouncementByDriverQuery : IRequest<Response<IEnumerable<DriverAnnouncementViewModel>>>
    {
        public Guid driverId { set; get; }
        public string Month { set; get; }
        public string Year { set; get; }
        public string Search { set; get; }
        public int Status { set; get; }
        public int PageNumber { set; get; }
        public int PageSize { set; get; }
    }
    public class GetAnnouncementByDriverQueryHandler : IRequestHandler<GetAnnouncementByDriverQuery, Response<IEnumerable<DriverAnnouncementViewModel>>>
    {
        private readonly IDriverAnnouncementRepositoryAsync _driverAnnouncementRepository;
        private readonly IProductTypeRepositoryAsync _productTypeRepository;
        private readonly IProductPackagingRepositoryAsync _productPackagingRepository;
        private readonly IMapper _mapper;
        public GetAnnouncementByDriverQueryHandler(IDriverAnnouncementRepositoryAsync driverAnnouncementRepository, IProductTypeRepositoryAsync productTypeRepository, IProductPackagingRepositoryAsync productPackagingRepository, IMapper mapper)
        {
            _driverAnnouncementRepository = driverAnnouncementRepository;
            _productTypeRepository = productTypeRepository;
            _productPackagingRepository = productPackagingRepository;
            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<DriverAnnouncementViewModel>>> Handle(GetAnnouncementByDriverQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var productTypes = (await _productTypeRepository.FindByCondition(x => x.Active == true).ConfigureAwait(false)).AsQueryable().ToList();
                var packagings = (await _productPackagingRepository.FindByCondition(x => x.Active == true).ConfigureAwait(false)).AsQueryable().ToList();
                
                string orderStatusPath = $"Shared\\driver_orderingstatus.json";
                var orderingStatusPathFile = Path.Combine(Directory.GetCurrentDirectory(), orderStatusPath);
                var statusJson = System.IO.File.ReadAllText(orderingStatusPathFile);
                List<OrderingStatusViewModel> orderingStatus = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OrderingStatusViewModel>>(statusJson);

                var validFilter = _mapper.Map<GetAnnouncementByDriverQuery>(request);
                Expression<Func<DriverAnnouncement, bool>> expression = PredicateBuilder.New<DriverAnnouncement>(false);
                expression = x => x.Driver.Id == validFilter.driverId;
                int month = 0;
                if (validFilter.Month != null && int.TryParse(validFilter.Month, out month))
                {
                    expression = expression.And(x => x.SendDate.Month == month || x.ReceiveDate.Month == month);
                }
                int year = 0;
                if (validFilter.Year != null && int.TryParse(validFilter.Year, out year))
                {
                    expression = expression.And(x => x.SendDate.Year == year || x.ReceiveDate.Year == year);
                }
                if (validFilter.Status != null && validFilter.Status != 0)
                {
                    if (validFilter.Status == 1)
                    {
                        expression = expression.And(x => x.Status == 1);
                    }
                    else if (validFilter.Status == 2)
                    {
                        expression = expression.And(x => x.Status != 1);
                    }
                }
                if (validFilter.Search != null && validFilter.Search.Trim() != "")
                {

                    Expression<Func<DriverAnnouncement, bool>> expressionSearch = PredicateBuilder.New<DriverAnnouncement>(false);
                    string search = validFilter.Search.Trim().ToLower().Replace(" ", "");
                    expressionSearch = x => (x.Driver.Name.Contains(search));
                   
                    var productList = productTypes.Where(p => p.Name_TH.Trim().ToLower().Replace(" ", "").Contains(search) || p.Name_ENG.Trim().ToLower().Replace(" ", "").Contains(search)).ToList();

                    Expression<Func<DriverAnnouncement, bool>> expressionProduct = PredicateBuilder.New<DriverAnnouncement>(false);
                    expressionProduct = o => (o.AcceptProductDetail.Trim().ToLower().Replace(" ", "").Contains(search));
                    foreach (ProductType productType in productList)
                    {
                        expressionProduct = expressionProduct.Or(o => (o.AcceptProduct.Contains(productType.Id.ToString())));
                    };

                    foreach (ProductType productType in productList)
                    {
                        expressionProduct = expressionProduct.Or(o => (o.RejectProduct.Contains(productType.Id.ToString())));
                    };
                    expressionProduct = expressionProduct.Or(o => (o.RejectProductDetail.Trim().ToLower().Replace(" ", "").Contains(search)));

                    var packagingList = productTypes.Where(p => p.Name_TH.Trim().ToLower().Replace(" ", "").Contains(search) || p.Name_ENG.Trim().ToLower().Replace(" ", "").Contains(search)).ToList();

                    Expression<Func<Domain.DriverAnnouncement, bool>> expressionPackaging = PredicateBuilder.New<Domain.DriverAnnouncement>(false);
                    expressionPackaging = o => (o.AcceptPackagingDetail.Trim().ToLower().Replace(" ", "").Contains(search));
                    foreach (Domain.ProductType packaging in packagingList)
                    {
                        expressionPackaging = expressionPackaging.Or(o => (o.AcceptPackaging.Contains(packaging.Id.ToString())));
                    };


                    foreach (Domain.ProductType packaging in packagingList)
                    {
                        expressionPackaging = expressionPackaging.Or(o => (o.RejectPackaging.Contains(packaging.Id.ToString())));
                    };
                    expressionPackaging = expressionPackaging.Or(o => (o.RejectPackagingDetail.Trim().ToLower().Replace(" ", "").Contains(search)));

                    //expressionSearch.Or(expressionProduct);
                    //expressionSearch.Or(expressionPackaging);
                    //expressionSearch = expressionSearch.Or(x => (x.Driver.Name.Contains(search)));
                    Expression<Func<Domain.DriverAnnouncement, bool>> expressionDriver = PredicateBuilder.New<Domain.DriverAnnouncement>(false);
                    expressionDriver = expressionDriver.Or(x => x.Driver.Cars.Where(c => c.CarRegistration.Trim().ToLower().Replace(" ", "").Contains(search)
                                                                                   || c.CarType.Name_TH.Trim().ToLower().Replace(" ", "").Contains(search) || c.CarType.Name_ENG.Trim().ToLower().Replace(" ", "").Contains(search)
                                                                                   || c.CarList.Name_TH.Trim().ToLower().Replace(" ", "").Contains(search) || c.CarList.Name_ENG.Trim().ToLower().Replace(" ", "").Contains(search)
                                                                                   || c.CarDescription.Name_TH.Trim().ToLower().Replace(" ", "").Contains(search) || c.CarDescription.Name_ENG.Trim().ToLower().Replace(" ", "").Contains(search) || c.CarDescriptionDetail.Trim().ToLower().Replace(" ", "").Contains(search)
                                                                                   || c.CarFeature.Name_TH.Trim().ToLower().Replace(" ", "").Contains(search) || c.CarFeature.Name_ENG.Trim().ToLower().Replace(" ", "").Contains(search) || c.CarFeatureDetail.Trim().ToLower().Replace(" ", "").Contains(search)
                                                                                   || c.CarSpecification.Name_TH.Trim().ToLower().Replace(" ", "").Contains(search) || c.CarSpecification.Name_ENG.Trim().ToLower().Replace(" ", "").Contains(search) || c.CarSpecificationDetail.Trim().ToLower().Replace(" ", "").Contains(search)
                                                                                   || c.DriverName.Trim().ToLower().Replace(" ", "").Contains(search) || c.Note.Trim().ToLower().Replace(" ", "").Contains(search)).Count() > 0);
                    Expression<Func<Domain.DriverAnnouncement, bool>> expressionLocatio = PredicateBuilder.New<Domain.DriverAnnouncement>(false);
                    expressionLocatio = expressionLocatio.Or(x => x.Locations.Where(l => l.Country.Name_TH.Trim().ToLower().Replace(" ", "").Contains(search) || l.Country.Name_ENG.Trim().ToLower().Replace(" ", "").Contains(search)
                                                                                     || l.Province.Name_TH.Trim().ToLower().Replace(" ", "").Contains(search) || l.Province.Name_ENG.Trim().ToLower().Replace(" ", "").Contains(search)
                                                                                     || l.District.Name_TH.Trim().ToLower().Replace(" ", "").Contains(search) || l.District.Name_ENG.Trim().ToLower().Replace(" ", "").Contains(search)
                                                                                     || l.Subdistrict.Name_TH.Trim().ToLower().Replace(" ", "").Contains(search) || l.Subdistrict.Name_ENG.Trim().ToLower().Replace(" ", "").Contains(search)
                                                                                     || l.Subdistrict.PostCode.Trim().ToLower().Replace(" ", "").Contains(search)).Count() > 0);
                    //expressionSearch.Or(expressionProduct);
                    //expressionSearch.Or(expressionPackaging);

                    if (validFilter.Status == 1)
                    {

                    }
                    else
                    {
                        Expression<Func<DriverAnnouncement, bool>> expressionStatus = PredicateBuilder.New<DriverAnnouncement>(false);
                        var searchStatus = orderingStatus.Where(x => x.th.Contains(search) || x.th.Contains(search)).ToList();
                        foreach (OrderingStatusViewModel orderStatus in searchStatus)
                        {
                            expressionStatus = expressionStatus.Or(a => a.Status == orderStatus.id);
                            expressionStatus = expressionStatus.Or(a => a.Order.Status == orderStatus.id);
                            expressionStatus = expressionStatus.Or(a => a.Order.Addresses.Where(a => a.Status == orderStatus.id).Count() > 0);
                        }
                        expressionSearch.Or(expressionStatus);
                    }
                    expression = expression.And(expressionSearch.Or(expressionLocatio).Or(expressionDriver).Or(expressionProduct).Or(expressionPackaging));
                }
                #region ordering
                Expression<Func<Domain.DriverAnnouncement, DateTime>> orderExpression = x => x.Created;
                bool isDescending = true;
                #endregion
                var driverAnnouncement = await _driverAnnouncementRepository.FindByConditionWithPage(expression, orderExpression, isDescending,validFilter.PageNumber, validFilter.PageSize);
                //var driverAnnouncement = (await _driverAnnouncementRepository.FindByCondition(x => x.DriverId.Equals(validFilter.driverId)).ConfigureAwait(false)).AsQueryable().ToList();
                var driverAnnouncementViewModel = _mapper.Map<IEnumerable<DriverAnnouncementViewModel>>(driverAnnouncement);
                foreach (DriverAnnouncementViewModel data in driverAnnouncementViewModel)
                {
                    if (data.driver != null)
                    {
                        data.driver.files.RemoveAll(f => f.IsApprove == false);
                    }
                    if (data.status == 1)
                    {
                        OrderingStatusViewModel statusObj = new OrderingStatusViewModel()
                        {
                             id = 1,
                             th =  "กำลังหางาน",
                             eng =  "looking for a job",
                              description  = "กำลังหางาน",
                              color = "#3bbac1"
                        };
                    }
                    else 
                    {
                         if(data.order != null)
                        {
                            if (data.order.status != null)
                            {
                                int orderingStatusId = data.order.status;
                                if (orderingStatusId == 0)
                                {
                                    var orderingAddress = data.order.addresses.Where(x => x.status != 3).OrderBy(x => x.sequence).ToList();
                                    if (orderingAddress.Count > 0)
                                    {
                                        orderingStatusId = orderingAddress[0].status;
                                    }
                                }
                                data.statusObj = orderingStatus.Where(o => o.id == orderingStatusId).FirstOrDefault();
                            }
                        }
                    } 
                    if (data.acceptProduct != null)
                    {
                        data.acceptProducts = new List<ProductTypeViewModel>();
                        foreach (string productIdStr in data.acceptProduct.Split(","))
                        {
                            int id = 0;
                            if (int.TryParse(productIdStr, out id))
                            {
                                var productType = productTypes.Where(p => p.Id == id).FirstOrDefault();
                                if (productType != null)
                                {
                                    data.acceptProducts.Add(_mapper.Map<ProductTypeViewModel>(productType));
                                }
                            }
                        }
                    }

                    if (data.acceptPackaging != null)
                    {
                        data.acceptPackagings = new List<ProductPackagingViewModel>();
                        foreach (string packagingIdStr in data.acceptPackaging.Split(","))
                        {
                            int id = 0;
                            if (int.TryParse(packagingIdStr, out id))
                            {
                                var packaging = packagings.Where(p => p.Id == id).FirstOrDefault();
                                if (packaging != null)
                                {
                                    data.acceptPackagings.Add(_mapper.Map<ProductPackagingViewModel>(packaging));
                                }
                            }
                        }
                    }

                    if (data.rejectProduct != null)
                    {
                        data.rejectProducts = new List<ProductTypeViewModel>();
                        foreach (string productIdStr in data.rejectProduct.Split(","))
                        {
                            int id = 0;
                            if (int.TryParse(productIdStr, out id))
                            {
                                var productType = productTypes.Where(p => p.Id == id).FirstOrDefault();
                                if (productType != null)
                                {
                                    data.rejectProducts.Add(_mapper.Map<ProductTypeViewModel>(productType));
                                }
                            }
                        }
                    }

                    if (data.rejectPackaging != null)
                    {
                        data.rejectPackagings = new List<ProductPackagingViewModel>();
                        foreach (string packagingIdStr in data.rejectPackaging.Split(","))
                        {
                            int id = 0;
                            if (int.TryParse(packagingIdStr, out id))
                            {
                                var packaging = packagings.Where(p => p.Id == id).FirstOrDefault();
                                if (packaging != null)
                                {
                                    data.rejectPackagings.Add(_mapper.Map<ProductPackagingViewModel>(packaging));
                                }
                            }
                        }
                    }
                }
                return new Response<IEnumerable<DriverAnnouncementViewModel>>(driverAnnouncementViewModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}