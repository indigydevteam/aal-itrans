﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.DriverAnnouncement;
using iTrans.Transportation.Application.DTOs.OrderingAddress;
using iTrans.Transportation.Application.DTOs.OrderingProduct;
using iTrans.Transportation.Application.DTOs.ProductPackaging;
using iTrans.Transportation.Application.DTOs.ProductType;
using iTrans.Transportation.Application.Enums;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using LinqKit;
using MediatR;

namespace iTrans.Transportation.Application.Features.DriverAnnouncements.Queries
{
    public class SendNotiToDriverQuery : IRequest<Response<bool>>
    {
        public virtual Guid OrderingId { get; set; }
    }
    public class SendNotiToDriverQueryHandler : IRequestHandler<SendNotiToDriverQuery, Response<bool>>
    {
        private readonly IOrderingRepositoryAsync _orderingRepository;
        private readonly IDriverAnnouncementRepositoryAsync _driverAnnouncementRepository;
        private readonly IProductTypeRepositoryAsync _productTypeRepository;
        private readonly IProductPackagingRepositoryAsync _productPackagingRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;
        public SendNotiToDriverQueryHandler(IOrderingRepositoryAsync orderingRepository, IDriverAnnouncementRepositoryAsync driverAnnouncementRepository, IProductTypeRepositoryAsync productTypeRepository
            , IProductPackagingRepositoryAsync productPackagingRepository, IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _orderingRepository = orderingRepository;
            _driverAnnouncementRepository = driverAnnouncementRepository;
            _productTypeRepository = productTypeRepository;
            _productPackagingRepository = productPackagingRepository;
            _mapper = mapper;
            _applicationLogRepository = applicationLogRepository;
        }

        public async Task<Response<bool>> Handle(SendNotiToDriverQuery request, CancellationToken cancellationToken)
        {
            int count = 0;
            try
            {
                var data = (await _orderingRepository.FindByCondition(x => x.Id == request.OrderingId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (data == null)
                {
                    return new Response<bool>(false);
                }

                //var productTypes = (await _productTypeRepository.FindByCondition(x => x.Active == true).ConfigureAwait(false)).AsQueryable().ToList();
                //var packagings = (await _productPackagingRepository.FindByCondition(x => x.Active == true).ConfigureAwait(false)).AsQueryable().ToList();


                Expression<Func<DriverAnnouncement, bool>> expression = PredicateBuilder.New<DriverAnnouncement>(false);
                var status = 1;
                expression = x => x.Status == status;
               
                #region location condition
                if (data.Addresses.Count() > 0)
                {
                    expression = expression.And(x => x.Locations.Where(l => l.Country.Id == data.Addresses[0].Country.Id && l.Province.Id == data.Addresses[0].Province.Id && l.LocationType == "beginning").Count() > 0
                                                  && x.Locations.Where(l => l.Country.Id == data.Addresses[data.Addresses.Count() - 1].Country.Id && l.Province.Id == data.Addresses[data.Addresses.Count() - 1].Province.Id && l.LocationType == "destination").Count() > 0);
                }
                #endregion
                #region car condition
                //if (data.Cars.Count() > 0)
                //{
                //    expression = expression.And(x => x.Driver.Cars.Where(c => c.CarType.Id == data.Cars[0].CarType.Id).Count() > 0);

                //}
                #endregion
                //#region car Reject Product
                //if (data.Products.Count() > 0)
                //{
                //    List<string> productIds = new List<string>();
                //    List<string> packagingIds = new List<string>();
                //    foreach (OrderingProduct product in data.Products)
                //    {
                //        foreach (string productTypeId in product.ProductType.Split(","))
                //        {
                //            expression = expression.And(x => !x.RejectProduct.Contains(productTypeId));
                //        }
                //        foreach (string packagingId in product.Packaging.Split(","))
                //        {
                //            expression = expression.And(x => !x.RejectPackaging.Contains(packagingId));
                //        }
                //    }
                //}
                //#endregion
                #region ordering
                Expression<Func<Domain.DriverAnnouncement, decimal>> orderExpression = x => x.DesiredPrice;
                bool isDescending = true;
                #endregion
                //var driverAnnouncement = await _driverAnnouncementRepository.FindByConditionWithPage(expression, orderExpression, isDescending, request.PageNumber, request.PageSize);
                //if (driverAnnouncement.Count() == 0)
                //{
                //    expression = x => x.Status == status;
                //    #region location condition
                //    if (data.Addresses.Count() > 0)
                //    {
                //        expression = expression.And(x => x.Locations.Where(l => l.Country.Id == data.Addresses[0].Country.Id && l.Province.Region.Id == data.Addresses[0].Province.Region.Id && l.LocationType == "beginning").Count() > 0
                //                                      && x.Locations.Where(l => l.Country.Id == data.Addresses[data.Addresses.Count() - 1].Country.Id && l.Province.Region.Id == data.Addresses[data.Addresses.Count() - 1].Province.Region.Id && l.LocationType == "destination").Count() > 0);
                //    }
                //    #endregion
                //    driverAnnouncement = await _driverAnnouncementRepository.FindByConditionWithPage(expression, orderExpression, isDescending, request.PageNumber, request.PageSize);

                //}

                return new Response<bool>(true);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}