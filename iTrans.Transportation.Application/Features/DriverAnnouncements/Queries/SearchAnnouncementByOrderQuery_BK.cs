﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.DriverAnnouncement;
using iTrans.Transportation.Application.DTOs.OrderingAddress;
using iTrans.Transportation.Application.DTOs.OrderingProduct;
using iTrans.Transportation.Application.DTOs.ProductPackaging;
using iTrans.Transportation.Application.DTOs.ProductType;
using iTrans.Transportation.Application.Enums;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using LinqKit;
using MediatR;

namespace iTrans.Transportation.Application.Features.DriverAnnouncements.Queries
{
    public class SearchAnnouncementByOrderQuery_BK : IRequest<Response<IEnumerable<DriverAnnouncementViewModel>>>
    {
        public virtual Guid OrderingId { get; set; }
        public decimal Price { set; get; }
        public virtual bool IsDriverOffer { get; set; }
        public List<FilterLocationViewModel> Locations { get; set; }
        public List<FilterProductViewModel> Products { get; set; }
        public int PageNumber { set; get; }
        public int PageSize { set; get; }
    }
    public class SearchAnnouncementByOrderQuery_BKHandler : IRequestHandler<SearchAnnouncementByOrderQuery_BK, Response<IEnumerable<DriverAnnouncementViewModel>>>
    {
        private readonly IOrderingRepositoryAsync _orderingRepository;
        private readonly IDriverAnnouncementRepositoryAsync _driverAnnouncementRepository;
        private readonly IProductTypeRepositoryAsync _productTypeRepository;
        private readonly IProductPackagingRepositoryAsync _productPackagingRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;
        public SearchAnnouncementByOrderQuery_BKHandler(IOrderingRepositoryAsync orderingRepository, IDriverAnnouncementRepositoryAsync driverAnnouncementRepository, IProductTypeRepositoryAsync productTypeRepository
            , IProductPackagingRepositoryAsync productPackagingRepository, IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _orderingRepository = orderingRepository;
            _driverAnnouncementRepository = driverAnnouncementRepository;
            _productTypeRepository = productTypeRepository;
            _productPackagingRepository = productPackagingRepository;
            _mapper = mapper;
            _applicationLogRepository = applicationLogRepository;
        }

        public async Task<Response<IEnumerable<DriverAnnouncementViewModel>>> Handle(SearchAnnouncementByOrderQuery_BK request, CancellationToken cancellationToken)
        {
            int count = 0;
            try
            {
                var data = (await _orderingRepository.FindByCondition(x => x.Id == request.OrderingId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (data == null)
                {
                    throw new ApiException($"Ordering Not Found.");
                }
                data.OrderingDesiredPrice = request.Price;
                data.IsOrderingDriverOffer = request.IsDriverOffer;
                data.Modified = DateTime.UtcNow;
                await _orderingRepository.UpdateAsync(data);
 
                var productTypes = (await _productTypeRepository.FindByCondition(x => x.Active == true).ConfigureAwait(false)).AsQueryable().ToList();
                var packagings = (await _productPackagingRepository.FindByCondition(x => x.Active == true).ConfigureAwait(false)).AsQueryable().ToList();
                var validFilter = _mapper.Map<SearchAnnouncementByOrderQuery_BK>(request);

                Expression<Func<DriverAnnouncement, bool>> expression = PredicateBuilder.New<DriverAnnouncement>(false);
                var status = 1;
                expression = x => x.Status == status;
                if (request.Locations != null && request.Locations.Count() > 0)
                {
                    expression = expression.And(x => (x.Locations.Where(l => l.Country.Id == request.Locations[0].CountryId
                                                                         && l.Province.Id == request.Locations[0].ProvinceId
                                                                         && l.District.Id == request.Locations[0].DistrictId
                                                                         && l.Subdistrict.Id == request.Locations[0].SubdistrictId).Count() > 0)
                                                                         || (x.Locations.Where(l => l.Country.Id == request.Locations[request.Locations.Count() - 1].CountryId
                                                                         && l.Province.Id == request.Locations[request.Locations.Count() - 1].ProvinceId
                                                                         && l.District.Id == request.Locations[request.Locations.Count() - 1].DistrictId
                                                                         && l.Subdistrict.Id == request.Locations[request.Locations.Count() - 1].SubdistrictId).Count() > 0));
                }

                if (request.Products != null)
                {
                    List<string> productIds = new List<string>();
                    List<string> packagingIds = new List<string>();
                    foreach (FilterProductViewModel product in request.Products)
                    {
                        expression = expression.And(x => !x.RejectProduct.Contains(product.ProductTypeId.ToString()));
                        expression = expression.And(x => !x.RejectPackaging.Contains(product.PackagingId.ToString()));
                    }

                }

                #region ordering
                Expression<Func<Domain.DriverAnnouncement, DateTime>> orderExpression = x => x.Created;
                bool isDescending = true;
                #endregion
                var driverAnnouncement = await _driverAnnouncementRepository.FindByConditionWithPage(expression, orderExpression, isDescending, validFilter.PageNumber, validFilter.PageSize);

                var driverAnnouncementViewModel = _mapper.Map<IEnumerable<DriverAnnouncementViewModel>>(driverAnnouncement);
                foreach (DriverAnnouncementViewModel dataAnnouncement in driverAnnouncementViewModel)
                {
                    count = count + 1;
                    if (dataAnnouncement.acceptProduct != null)
                    {
                        dataAnnouncement.acceptProducts = new List<ProductTypeViewModel>();
                        foreach (string productIdStr in dataAnnouncement.acceptProduct.Split(","))
                        {
                            int id = 0;
                            if (int.TryParse(productIdStr, out id))
                            {
                                var productType = productTypes.Where(p => p.Id == id).FirstOrDefault();
                                if (productType != null)
                                {
                                    dataAnnouncement.acceptProducts.Add(_mapper.Map<ProductTypeViewModel>(productType));
                                }
                            }
                        }
                    }

                    if (dataAnnouncement.acceptPackaging != null)
                    {
                        dataAnnouncement.acceptPackagings = new List<ProductPackagingViewModel>();
                        foreach (string packagingIdStr in dataAnnouncement.acceptPackaging.Split(","))
                        {
                            int id = 0;
                            if (int.TryParse(packagingIdStr, out id))
                            {
                                var packaging = packagings.Where(p => p.Id == id).FirstOrDefault();
                                if (packaging != null)
                                {
                                    dataAnnouncement.acceptPackagings.Add(_mapper.Map<ProductPackagingViewModel>(packaging));
                                }
                            }
                        }
                    }

                    if (dataAnnouncement.rejectProduct != null)
                    {
                        dataAnnouncement.rejectProducts = new List<ProductTypeViewModel>();
                        foreach (string productIdStr in dataAnnouncement.rejectProduct.Split(","))
                        {
                            int id = 0;
                            if (int.TryParse(productIdStr, out id))
                            {
                                var productType = productTypes.Where(p => p.Id == id).FirstOrDefault();
                                if (productType != null)
                                {
                                    dataAnnouncement.rejectProducts.Add(_mapper.Map<ProductTypeViewModel>(productType));
                                }
                            }
                        }
                    }

                    if (dataAnnouncement.rejectPackaging != null)
                    {
                        dataAnnouncement.rejectPackagings = new List<ProductPackagingViewModel>();
                        foreach (string packagingIdStr in dataAnnouncement.rejectPackaging.Split(","))
                        {
                            int id = 0;
                            if (int.TryParse(packagingIdStr, out id))
                            {
                                var packaging = packagings.Where(p => p.Id == id).FirstOrDefault();
                                if (packaging != null)
                                {
                                    dataAnnouncement.rejectPackagings.Add(_mapper.Map<ProductPackagingViewModel>(packaging));
                                }
                            }
                        }
                    }
                }
                return new Response<IEnumerable<DriverAnnouncementViewModel>>(driverAnnouncementViewModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}