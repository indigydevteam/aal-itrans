﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.DriverAnnouncement;
using iTrans.Transportation.Application.DTOs.ProductPackaging;
using iTrans.Transportation.Application.DTOs.ProductType;
using iTrans.Transportation.Application.Enums;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using iTrans.Transportation.Domain.Entities;
using LinqKit;
using MediatR;

namespace iTrans.Transportation.Application.Features.DriverAnnouncements.Queries
{
    public class GetAllLookingJobAnnouncementQuery : IRequest<Response<IEnumerable<AnnouncementWithDriverViewModel>>>
    {
        public string Month { set; get; }
        public string Year { set; get; }
        public string Search { set; get; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
    }
    public class GetAllLookingJobAnnouncementQueryHandler : IRequestHandler<GetAllLookingJobAnnouncementQuery, Response<IEnumerable<AnnouncementWithDriverViewModel>>>
    {
        private readonly IDriverAnnouncementRepositoryAsync _driverAnnouncementRepository;
        private readonly IProductTypeRepositoryAsync _productTypeRepository;
        private readonly IProductPackagingRepositoryAsync _productPackagingRepository;
        private readonly IMapper _mapper;
        public GetAllLookingJobAnnouncementQueryHandler(IDriverAnnouncementRepositoryAsync driverAnnouncementRepository, IProductTypeRepositoryAsync productTypeRepository, IProductPackagingRepositoryAsync productPackagingRepository, IMapper mapper)
        {
            _driverAnnouncementRepository = driverAnnouncementRepository;
            _productTypeRepository = productTypeRepository;
            _productPackagingRepository = productPackagingRepository;
            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<AnnouncementWithDriverViewModel>>> Handle(GetAllLookingJobAnnouncementQuery request, CancellationToken cancellationToken)
        {
            var productTypes = (await _productTypeRepository.FindByCondition(x => x.Active == true).ConfigureAwait(false)).AsQueryable().ToList();
            var packagings = (await _productPackagingRepository.FindByCondition(x => x.Active == true).ConfigureAwait(false)).AsQueryable().ToList();

            var validFilter = _mapper.Map<GetAllLookingJobAnnouncementQuery>(request);
            Expression<Func<DriverAnnouncement, bool>> expression = PredicateBuilder.New<DriverAnnouncement>(false);
            var status = 1;// Enum.GetName(typeof(DriverAnnouncementStatus), 1);
            expression = x => x.Status == status;
            int month = 0;
            if (validFilter.Month != null && int.TryParse(validFilter.Month, out month))
            {
                expression = expression.And(x => x.SendDate.Month == month || x.ReceiveDate.Month == month);
            }
            int year = 0;
            if (validFilter.Year != null && int.TryParse(validFilter.Year, out year))
            {
                expression = expression.And(x => x.SendDate.Year == year || x.ReceiveDate.Year == year);
            }
            if (validFilter.Search != null && validFilter.Search.Trim() != "")
            {

                #region
                //Expression<Func<DriverAnnouncement, bool>> expressionSearch = PredicateBuilder.New<DriverAnnouncement>(false);

                //string search = validFilter.Search.Trim();
                //var productList = productTypes.Where(p => p.Name_TH.Contains(search) || p.Name_ENG.Contains(search)).ToList();

                //Expression<Func<Domain.DriverAnnouncement, bool>> expressionProduct = PredicateBuilder.New<Domain.DriverAnnouncement>(false);
                //foreach (Domain.ProductType productType in productList)
                //{
                //    expressionProduct = expressionProduct.Or(o => (o.AcceptProduct.Contains(productType.Id.ToString())));
                //};
                //expressionProduct = expressionProduct.Or(o => (o.AcceptProductDetail.Contains(search)));
                //foreach (Domain.ProductType productType in productList)
                //{
                //    expressionProduct = expressionProduct.Or(o => (o.RejectProduct.Contains(productType.Id.ToString())));
                //};
                //expressionProduct = expressionProduct.Or(o => (o.RejectProductDetail.Contains(search)));

                //var packagingList = productTypes.Where(p => p.Name_TH.Contains(search) || p.Name_ENG.Contains(search)).ToList();

                //Expression<Func<Domain.DriverAnnouncement, bool>> expressionPackaging = PredicateBuilder.New<Domain.DriverAnnouncement>(false);
                //foreach (Domain.ProductType packaging in packagingList)
                //{
                //    expressionPackaging = expressionPackaging.Or(o => (o.AcceptPackaging.Contains(packaging.Id.ToString())));
                //};
                //expressionPackaging = expressionPackaging.Or(o => (o.AcceptPackagingDetail.Contains(search)));

                //foreach (Domain.ProductType packaging in packagingList)
                //{
                //    expressionPackaging = expressionPackaging.Or(o => (o.RejectPackaging.Contains(packaging.Id.ToString())));
                //};
                //expressionPackaging = expressionPackaging.Or(o => (o.RejectPackagingDetail.Contains(search)));

                //expressionSearch = x => x.Driver.Name.Contains(search);
                //expressionSearch = expressionSearch.Or(x => x.Driver.Cars.Where(c => c.CarType.Name_TH.Contains(search) || c.CarType.Name_ENG.Contains(search)
                //                                                               || c.CarList.Name_TH.Contains(search) || c.CarList.Name_ENG.Contains(search)
                //                                                               || c.CarDescription.Name_TH.Contains(search) || c.CarDescription.Name_ENG.Contains(search) || c.CarDescriptionDetail.Contains(search)
                //                                                               || c.CarFeature.Name_TH.Contains(search) || c.CarFeature.Name_ENG.Contains(search) || c.CarFeatureDetail.Contains(search)
                //                                                               || c.CarSpecification.Name_TH.Contains(search) || c.CarSpecification.Name_ENG.Contains(search) || c.CarSpecificationDetail.Contains(search)
                //                                                               || c.Note.Contains(search)).Count() > 0);
                //expressionSearch = expressionSearch.Or(x =>  x.Locations.Where(l => l.Country.Name_TH.Trim().ToLower().Contains(validFilter.Search.Trim().ToLower()) || l.Country.Name_ENG.Trim().ToLower().Contains(validFilter.Search.Trim().ToLower())
                //                                                                  || l.Province.Name_TH.Trim().ToLower().Contains(validFilter.Search.Trim().ToLower()) || l.Province.Name_ENG.Trim().ToLower().Contains(validFilter.Search.Trim().ToLower())
                //                                                                  || l.District.Name_TH.Trim().ToLower().Contains(validFilter.Search.Trim().ToLower()) || l.District.Name_ENG.Trim().ToLower().Contains(validFilter.Search.Trim().ToLower())
                //                                                                  || l.Subdistrict.Name_TH.Trim().ToLower().Contains(validFilter.Search.Trim().ToLower()) || l.Subdistrict.Name_ENG.Trim().ToLower().Contains(validFilter.Search.Trim().ToLower())
                //                                                                  || l.Subdistrict.PostCode.Trim().ToLower().Contains(validFilter.Search.Trim().ToLower())).Count() > 0);
                //expressionSearch.Or(expressionProduct);
                //expressionSearch.Or(expressionPackaging);
                #endregion
                Expression<Func<DriverAnnouncement, bool>> expressionSearch = PredicateBuilder.New<DriverAnnouncement>(false);

                string search = validFilter.Search.Trim().ToLower().Replace(" ", "");
                var productList = productTypes.Where(p => p.Name_TH.Trim().ToLower().Replace(" ", "").Contains(search) || p.Name_ENG.Trim().ToLower().Replace(" ", "").Contains(search)).ToList();

                Expression<Func<DriverAnnouncement, bool>> expressionProduct = PredicateBuilder.New<DriverAnnouncement>(false);
                foreach (ProductType productType in productList)
                {
                    expressionProduct = expressionProduct.Or(o => (o.AcceptProduct.Contains(productType.Id.ToString())));
                };
                expressionProduct = expressionProduct.Or(o => (o.AcceptProductDetail.Trim().ToLower().Replace(" ", "").Contains(search)));
                foreach (ProductType productType in productList)
                {
                    expressionProduct = expressionProduct.Or(o => (o.RejectProduct.Contains(productType.Id.ToString())));
                };
                expressionProduct = expressionProduct.Or(o => (o.RejectProductDetail.Trim().ToLower().Replace(" ", "").Contains(search)));

                var packagingList = productTypes.Where(p => p.Name_TH.Trim().ToLower().Replace(" ", "").Contains(search) || p.Name_ENG.Trim().ToLower().Replace(" ", "").Contains(search)).ToList();

                Expression<Func<Domain.DriverAnnouncement, bool>> expressionPackaging = PredicateBuilder.New<Domain.DriverAnnouncement>(false);
                foreach (Domain.ProductType packaging in packagingList)
                {
                    expressionPackaging = expressionPackaging.Or(o => (o.AcceptPackaging.Contains(packaging.Id.ToString())));
                };
                expressionPackaging = expressionPackaging.Or(o => (o.AcceptPackagingDetail.Trim().ToLower().Replace(" ", "").Contains(search)));

                foreach (Domain.ProductType packaging in packagingList)
                {
                    expressionPackaging = expressionPackaging.Or(o => (o.RejectPackaging.Contains(packaging.Id.ToString())));
                };
                expressionPackaging = expressionPackaging.Or(o => (o.RejectPackagingDetail.Trim().ToLower().Replace(" ", "").Contains(search)));

                expressionSearch = x => x.Driver.Name.Contains(search);
                expressionSearch = expressionSearch.Or(x => x.Driver.Cars.Where(c => c.CarRegistration.Trim().ToLower().Replace(" ", "").Contains(search)
                                                                               || c.CarType.Name_TH.Trim().ToLower().Replace(" ", "").Contains(search) || c.CarType.Name_ENG.Trim().ToLower().Replace(" ", "").Contains(search)
                                                                               || c.CarList.Name_TH.Trim().ToLower().Replace(" ", "").Contains(search) || c.CarList.Name_ENG.Trim().ToLower().Replace(" ", "").Contains(search)
                                                                               || c.CarDescription.Name_TH.Trim().ToLower().Replace(" ", "").Contains(search) || c.CarDescription.Name_ENG.Trim().ToLower().Replace(" ", "").Contains(search) || c.CarDescriptionDetail.Trim().ToLower().Replace(" ", "").Contains(search)
                                                                               || c.CarFeature.Name_TH.Trim().ToLower().Replace(" ", "").Contains(search) || c.CarFeature.Name_ENG.Trim().ToLower().Replace(" ", "").Contains(search) || c.CarFeatureDetail.Trim().ToLower().Replace(" ", "").Contains(search)
                                                                               || c.CarSpecification.Name_TH.Trim().ToLower().Replace(" ", "").Contains(search) || c.CarSpecification.Name_ENG.Trim().ToLower().Replace(" ", "").Contains(search) || c.CarSpecificationDetail.Trim().ToLower().Replace(" ", "").Contains(search)
                                                                               || c.DriverName.Trim().ToLower().Replace(" ", "").Contains(search) || c.Note.Trim().ToLower().Replace(" ", "").Contains(search)).Count() > 0);
                expressionSearch = expressionSearch.Or(x => x.Locations.Where(l => l.Country.Name_TH.Trim().ToLower().Replace(" ", "").Contains(search) || l.Country.Name_ENG.Trim().ToLower().Replace(" ", "").Contains(search)
                                                                                 || l.Province.Name_TH.Trim().ToLower().Replace(" ", "").Contains(search) || l.Province.Name_ENG.Trim().ToLower().Replace(" ", "").Contains(search)
                                                                                 || l.District.Name_TH.Trim().ToLower().Replace(" ", "").Contains(search) || l.District.Name_ENG.Trim().ToLower().Replace(" ", "").Contains(search)
                                                                                 || l.Subdistrict.Name_TH.Trim().ToLower().Replace(" ", "").Contains(search) || l.Subdistrict.Name_ENG.Trim().ToLower().Replace(" ", "").Contains(search)
                                                                                 || l.Subdistrict.PostCode.Trim().ToLower().Replace(" ", "").Contains(search)).Count() > 0);
                expressionSearch.Or(expressionProduct);
                expressionSearch.Or(expressionPackaging);

                expression = expression.And(expressionSearch);
            }
            Expression<Func<Domain.DriverAnnouncement, DateTime>> orderExpression = x => x.Created;
            bool isDescending = true;
            var driverAnnouncement = await _driverAnnouncementRepository.FindByConditionWithPage(expression, orderExpression, isDescending, validFilter.PageNumber, validFilter.PageSize);
            var driverAnnouncementViewModel = _mapper.Map<IEnumerable<AnnouncementWithDriverViewModel>>(driverAnnouncement);
            foreach (AnnouncementWithDriverViewModel data in driverAnnouncementViewModel)
            {
                if (data.driver != null)
                {
                    data.driver.files.RemoveAll(f => f.IsApprove == false);
                }
                
                if (data.acceptProduct != null)
                {
                    data.acceptProducts = new List<ProductTypeViewModel>();
                    foreach (string productIdStr in data.acceptProduct.Split(","))
                    {
                        int id = 0;
                        if (int.TryParse(productIdStr, out id))
                        {
                            var productType = productTypes.Where(p => p.Id == id).FirstOrDefault();
                            if (productType != null)
                            {
                                data.acceptProducts.Add(_mapper.Map<ProductTypeViewModel>(productType));
                            }
                        }
                    }
                }
                if (data.acceptPackaging != null)
                {
                    data.acceptPackagings = new List<ProductPackagingViewModel>();
                    foreach (string packagingIdStr in data.acceptPackaging.Split(","))
                    {
                        int id = 0;
                        if (int.TryParse(packagingIdStr, out id))
                        {
                            var packaging = packagings.Where(p => p.Id == id).FirstOrDefault();
                            if (packaging != null)
                            {
                                data.acceptPackagings.Add(_mapper.Map<ProductPackagingViewModel>(packaging));
                            }
                        }
                    }
                }
                if (data.rejectProduct != null)
                {
                    data.rejectProducts = new List<ProductTypeViewModel>();
                    foreach (string productIdStr in data.rejectProduct.Split(","))
                    {
                        int id = 0;
                        if (int.TryParse(productIdStr, out id))
                        {
                            var productType = productTypes.Where(p => p.Id == id).FirstOrDefault();
                            if (productType != null)
                            {
                                data.rejectProducts.Add(_mapper.Map<ProductTypeViewModel>(productType));
                            }
                        }
                    }
                }
                if (data.rejectPackaging != null)
                {
                    data.rejectPackagings = new List<ProductPackagingViewModel>();
                    foreach (string packagingIdStr in data.rejectPackaging.Split(","))
                    {
                        int id = 0;
                        if (int.TryParse(packagingIdStr, out id))
                        {
                            var packaging = packagings.Where(p => p.Id == id).FirstOrDefault();
                            if (packaging != null)
                            {
                                data.rejectPackagings.Add(_mapper.Map<ProductPackagingViewModel>(packaging));
                            }
                        }
                    }
                }
            }

            return new Response<IEnumerable<AnnouncementWithDriverViewModel>>(driverAnnouncementViewModel);
        }
    }
}
