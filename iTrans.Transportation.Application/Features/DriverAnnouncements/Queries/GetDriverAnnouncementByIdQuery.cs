﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.DriverAnnouncement;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.DriverAnnouncements.Queries
{
    public class GetDriverAnnouncementByIdQuery : IRequest<Response<DriverAnnouncementViewModel>>
    {
        public int Id { get; set; }
    }
    public class GetDriverAnnouncementByIdQueryHandler : IRequestHandler<GetDriverAnnouncementByIdQuery, Response<DriverAnnouncementViewModel>>
    {
        private readonly IDriverAnnouncementRepositoryAsync _driverAnnouncementRepository;
        private readonly IMapper _mapper;
        public GetDriverAnnouncementByIdQueryHandler(IDriverAnnouncementRepositoryAsync driverAnnouncementRepository, IMapper mapper)
        {
            _driverAnnouncementRepository = driverAnnouncementRepository;
            _mapper = mapper;
        }
        public async Task<Response<DriverAnnouncementViewModel>> Handle(GetDriverAnnouncementByIdQuery request, CancellationToken cancellationToken)
        {
            var driverObject = (await _driverAnnouncementRepository.FindByCondition(x => x.Id.Equals(request.Id)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            DriverAnnouncementViewModel announceView = _mapper.Map<DriverAnnouncementViewModel>(driverObject);
            if (announceView.driver != null)
            {
                announceView.driver.files.RemoveAll(f => f.IsDelete);
            }
            return new Response<DriverAnnouncementViewModel>(announceView);
        }
    }
}
