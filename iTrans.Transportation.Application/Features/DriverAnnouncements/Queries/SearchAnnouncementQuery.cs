﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.DriverAnnouncement;
using iTrans.Transportation.Application.DTOs.OrderingAddress;
using iTrans.Transportation.Application.DTOs.OrderingProduct;
using iTrans.Transportation.Application.DTOs.ProductPackaging;
using iTrans.Transportation.Application.DTOs.ProductType;
using iTrans.Transportation.Application.Enums;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using LinqKit;
using MediatR;

namespace iTrans.Transportation.Application.Features.DriverAnnouncements.Queries
{
    public class SearchAnnouncementQuery : IRequest<Response<IEnumerable<DriverAnnouncementViewModel>>>
    {
        public Guid OrderingId { get; set; }
        public DateTime FromDate { set; get; }
        public DateTime ToDate { set; get; }
        public int CountryId { get; set; }
        public int RegionId { get; set; }
        public int ProvinceId { get; set; }
        public int DistrictId { get; set; }
        public int TransportType { get; set; }
        public int CarTypeId { get; set; }
        public int CarSpecificationId { get; set; }
        public int PageNumber { set; get; }
        public int PageSize { set; get; }
    }
    public class SearchAnnouncementQueryHandler : IRequestHandler<SearchAnnouncementQuery, Response<IEnumerable<DriverAnnouncementViewModel>>>
    {
        private readonly IOrderingRepositoryAsync _orderingRepository;
        private readonly IDriverAnnouncementRepositoryAsync _driverAnnouncementRepository;
        private readonly ICountryRepositoryAsync _countryRepository;
        private readonly IRegionRepositoryAsync _regionRepository;
        private readonly IProvinceRepositoryAsync _provinceRepository;
        private readonly IDistrictRepositoryAsync _districtRepository;
        private readonly ICarTypeRepositoryAsync _carTypeRepository;
        private readonly ICarSpecificationRepositoryAsync _carSpecificationRepository;
        private readonly IProductTypeRepositoryAsync _productTypeRepository;
        private readonly IProductPackagingRepositoryAsync _productPackagingRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;
        public SearchAnnouncementQueryHandler(IOrderingRepositoryAsync orderingRepository, IDriverAnnouncementRepositoryAsync driverAnnouncementRepository
            , ICountryRepositoryAsync countryRepository, IRegionRepositoryAsync regionRepository, IProvinceRepositoryAsync provinceRepository, IDistrictRepositoryAsync districtRepository
            , ICarTypeRepositoryAsync carTypeRepository, ICarSpecificationRepositoryAsync carSpecificationRepository, IProductTypeRepositoryAsync productTypeRepository, IProductPackagingRepositoryAsync productPackagingRepository
            , IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _orderingRepository = orderingRepository;
            _driverAnnouncementRepository = driverAnnouncementRepository;
            _countryRepository = countryRepository;
            _regionRepository = regionRepository;
            _provinceRepository = provinceRepository;
            _districtRepository = districtRepository;
            _carTypeRepository = carTypeRepository;
            _carSpecificationRepository = carSpecificationRepository;
            _productTypeRepository = productTypeRepository;
            _productPackagingRepository = productPackagingRepository;
            _mapper = mapper;
            _applicationLogRepository = applicationLogRepository;
        }

        public async Task<Response<IEnumerable<DriverAnnouncementViewModel>>> Handle(SearchAnnouncementQuery request, CancellationToken cancellationToken)
        {
            int count = 0;
            try
            {
                var orderingData = (await _orderingRepository.FindByCondition(x => x.Id == request.OrderingId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                var validFilter = _mapper.Map<SearchAnnouncementQuery>(request);
                int maxItem = 100;

                if (validFilter.PageNumber == 0 || validFilter.PageSize == 0)
                {
                    validFilter.PageNumber = 1;
                    validFilter.PageSize = 10;
                }
                int startItem = (int)((request.PageSize * request.PageNumber) - request.PageSize);

                var productTypes = (await _productTypeRepository.FindByCondition(x => x.Active == true).ConfigureAwait(false)).AsQueryable().ToList();
                var packagings = (await _productPackagingRepository.FindByCondition(x => x.Active == true).ConfigureAwait(false)).AsQueryable().ToList();
                var country = (await _countryRepository.FindByCondition(x => x.Id == validFilter.CountryId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                var region = (await _regionRepository.FindByCondition(x => x.Id == validFilter.RegionId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                var province = (await _provinceRepository.FindByCondition(x => x.Id == validFilter.ProvinceId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                var district = (await _districtRepository.FindByCondition(x => x.Id == validFilter.DistrictId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                var carType = (await _carTypeRepository.FindByCondition(x => x.Id == validFilter.CarTypeId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                var carSpecification = (await _carSpecificationRepository.FindByCondition(x => x.Id == validFilter.CarSpecificationId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();

                Expression<Func<DriverAnnouncement, bool>> expression = PredicateBuilder.New<DriverAnnouncement>(false);
                #region ordering
                Expression<Func<Domain.DriverAnnouncement, decimal>> orderExpression = x => x.DesiredPrice;
                bool isDescending = true;
                #endregion

                var status = 1;
                expression = x => x.Status == status;
                DateTime fromDate = new DateTime();
                DateTime toDate = new DateTime();
                Expression<Func<DriverAnnouncement, bool>> expressionTime = PredicateBuilder.New<DriverAnnouncement>(false);
                if (request.FromDate != null)
                {
                    fromDate = new DateTime(request.FromDate.Year, request.FromDate.Month, request.FromDate.Day, 0, 0, 0);
                }
                if (request.ToDate != null)
                {
                    toDate = new DateTime(request.ToDate.Year, request.ToDate.Month, request.ToDate.Day, 23, 59, 59);
                }

                if (fromDate != new DateTime() && toDate != new DateTime())
                {
                    expressionTime = x => (x.ReceiveDate >= fromDate && x.ReceiveDate <= toDate) && (x.SendDate >= fromDate && x.SendDate <= toDate);
                }
                else if (fromDate != new DateTime())
                {
                    expressionTime =  x => x.ReceiveDate >= fromDate && x.SendDate >= fromDate;
                }
                else if (toDate != new DateTime())
                {
                    expressionTime =  x => x.ReceiveDate <= toDate && x.SendDate <= toDate;
                }
                bool isExpressionAddress = false;
                Expression<Func<DriverAnnouncement, bool>> expressionAddress = PredicateBuilder.New<DriverAnnouncement>(false);
                if (country != null && province != null && district != null)
                {
                    isExpressionAddress = true;
                    expressionAddress = x => x.Locations.Where(l => l.Country == country && l.Province == province && l.District == district).Count() > 0;
                }
                else if (country != null && province != null)
                {
                    isExpressionAddress = true;
                    expressionAddress = x => x.Locations.Where(l => l.Country == country && l.Province == province).Count() > 0;
                }
                else if (country != null && region != null)
                {
                    isExpressionAddress = true;
                    expressionAddress = x => x.Locations.Where(l => l.Country == country && l.Province.Region == region).Count() > 0;
                }
                else if (country != null)
                {
                    isExpressionAddress = true;
                    expressionAddress = x => x.Locations.Where(l => l.Country == country).Count() > 0;
                }

                bool isExpressionTransportType = false;
                Expression<Func<DriverAnnouncement, bool>> expressionTransportType = PredicateBuilder.New<DriverAnnouncement>(false);
                if (validFilter.TransportType == 1)
                {
                    isExpressionTransportType = true;
                    expressionTransportType = x => x.AllRent == true;
                }
                else if (validFilter.TransportType == 2)
                {
                    isExpressionTransportType = true;
                    expressionTransportType = x => x.AllRent == false;
                }

                bool isExpressionCar = false;
                Expression<Func<DriverAnnouncement, bool>> expressionCar = PredicateBuilder.New<DriverAnnouncement>(false);
                if (carType != null && carSpecification != null)
                {
                    isExpressionCar = true;
                    expressionCar = x => x.Driver.Cars.Where(c => c.CarType == carType && c.CarSpecification == carSpecification).Count() > 0;
                }
                else if (carType != null)
                {
                    isExpressionCar = true;
                    expressionCar = x => x.Driver.Cars.Where(c => c.CarType == carType).Count() > 0;
                }

                if(isExpressionAddress && isExpressionTransportType && isExpressionCar )
                {
                    expression = expression.And(expressionAddress).And(expressionTransportType).And(expressionCar);
                }
                else if (isExpressionAddress && isExpressionTransportType && !isExpressionCar)
                {
                    expression = expression.And(expressionAddress).And(expressionTransportType);
                }
                else if (isExpressionAddress && !isExpressionTransportType && !isExpressionCar)
                {
                    expression = expression.And(expressionAddress);
                }
                else if (!isExpressionAddress &&  isExpressionTransportType &&  isExpressionCar)
                {
                    expression = expression.And(expressionTransportType).And(expressionCar);
                }
                else if (!isExpressionAddress && !isExpressionTransportType && isExpressionCar)
                {
                    expression = expression.And(expressionCar);
                }
                else if (!isExpressionAddress && isExpressionTransportType && !isExpressionCar)
                {
                    expression = expression.And(expressionTransportType);
                }

                List<DriverAnnouncement> driverAnnouncement = new List<DriverAnnouncement>();
                int itemCount = _driverAnnouncementRepository.GetItemCount(expression);
                if (startItem <= itemCount)
                {
                    var driverAnnouncementTemp = await _driverAnnouncementRepository.FindByConditionWithPage(expression, orderExpression, isDescending, request.PageNumber, request.PageSize);
                    driverAnnouncement.AddRange(driverAnnouncementTemp.Except(driverAnnouncement));
                    startItem = 0;
                }
                else
                {
                    startItem = (startItem - itemCount) + 1;
                }

                
                if (driverAnnouncement.Count() < validFilter.PageSize)
                {
                    isExpressionAddress = false;
                    isExpressionTransportType = false;
                    isExpressionCar = false;

                    expression = PredicateBuilder.New<DriverAnnouncement>(false);
                    expression = x => x.Status == status;
                    if (fromDate != new DateTime() && toDate != new DateTime())
                    {
                        expression = expression.And(x => (x.ReceiveDate >= fromDate && x.ReceiveDate <= toDate) && (x.SendDate >= fromDate && x.SendDate <= toDate));
                    }
                    else if (fromDate != new DateTime())
                    {
                        expression = expression.And(x => x.ReceiveDate >= fromDate && x.SendDate >= fromDate);
                    }
                    else if (toDate != new DateTime())
                    {
                        expression = expression.And(x => x.ReceiveDate <= toDate && x.SendDate <= toDate);
                    }

                    expressionCar = PredicateBuilder.New<DriverAnnouncement>(false);
                    if (carType != null && carSpecification != null)
                    {
                        isExpressionCar = true;
                        expressionCar = x => x.Driver.Cars.Where(c => c.CarType == carType && c.CarSpecification == carSpecification).Count() > 0;
                    }
                    else if (carType != null)
                    {
                        isExpressionCar = true;
                        expressionCar = x => x.Driver.Cars.Where(c => c.CarType == carType).Count() > 0;
                    }
                    expressionAddress = PredicateBuilder.New<DriverAnnouncement>(false);

                    if (country != null && region != null)
                    {
                        isExpressionAddress = true;
                        expressionAddress = x => x.Locations.Where(l => l.Country == country && l.Province.Region == region).Count() > 0;
                    }

                    if (carType != null)
                    {
                        isExpressionTransportType = true;
                        expressionCar = x => x.Driver.Cars.Where(c => c.CarType == carType).Count() > 0;
                    }

                    if (isExpressionAddress && isExpressionTransportType && isExpressionCar)
                    {
                        expression = expression.And(expressionAddress).And(expressionTransportType).And(expressionCar);
                    }
                    else if (isExpressionAddress && isExpressionTransportType && !isExpressionCar)
                    {
                        expression = expression.And(expressionAddress).And(expressionTransportType);
                    }
                    else if (isExpressionAddress && !isExpressionTransportType && !isExpressionCar)
                    {
                        expression = expression.And(expressionAddress);
                    }
                    else if (!isExpressionAddress && isExpressionTransportType && isExpressionCar)
                    {
                        expression = expression.And(expressionTransportType).And(expressionCar);
                    }
                    else if (!isExpressionAddress && !isExpressionTransportType && isExpressionCar)
                    {
                        expression = expression.And(expressionCar);
                    }
                    else if (!isExpressionAddress && isExpressionTransportType && !isExpressionCar)
                    {
                        expression = expression.And(expressionTransportType);
                    }

                    expressionCar = PredicateBuilder.New<DriverAnnouncement>(false);
                    int itemCount2 = _driverAnnouncementRepository.GetItemCount(expression);

                    if (startItem < itemCount2)
                    {
                        var driverAnnouncementTemp = await _driverAnnouncementRepository.FindByConditionWithItemNumber(expression, orderExpression, isDescending, startItem, validFilter.PageSize - driverAnnouncement.Count());

                        driverAnnouncement.AddRange(driverAnnouncementTemp.Except(driverAnnouncement));
                    }
                }
                
                var driverAnnouncementViewModel = _mapper.Map<IEnumerable<DriverAnnouncementViewModel>>(driverAnnouncement);
                foreach (DriverAnnouncementViewModel dataAnnouncement in driverAnnouncementViewModel)
                {
                    count = count + 1;
                    if (dataAnnouncement.driver != null)
                    {
                        dataAnnouncement.driver.files.RemoveAll(f => f.IsApprove == false);
                    }
                    if (dataAnnouncement.acceptProduct != null)
                    {
                        dataAnnouncement.acceptProducts = new List<ProductTypeViewModel>();
                        foreach (string productIdStr in dataAnnouncement.acceptProduct.Split(","))
                        {
                            int id = 0;
                            if (int.TryParse(productIdStr, out id))
                            {
                                var productType = productTypes.Where(p => p.Id == id).FirstOrDefault();
                                if (productType != null)
                                {
                                    dataAnnouncement.acceptProducts.Add(_mapper.Map<ProductTypeViewModel>(productType));
                                }
                            }
                        }
                    }

                    if (dataAnnouncement.acceptPackaging != null)
                    {
                        dataAnnouncement.acceptPackagings = new List<ProductPackagingViewModel>();
                        foreach (string packagingIdStr in dataAnnouncement.acceptPackaging.Split(","))
                        {
                            int id = 0;
                            if (int.TryParse(packagingIdStr, out id))
                            {
                                var packaging = packagings.Where(p => p.Id == id).FirstOrDefault();
                                if (packaging != null)
                                {
                                    dataAnnouncement.acceptPackagings.Add(_mapper.Map<ProductPackagingViewModel>(packaging));
                                }
                            }
                        }
                    }

                    if (dataAnnouncement.rejectProduct != null)
                    {
                        dataAnnouncement.rejectProducts = new List<ProductTypeViewModel>();
                        foreach (string productIdStr in dataAnnouncement.rejectProduct.Split(","))
                        {
                            int id = 0;
                            if (int.TryParse(productIdStr, out id))
                            {
                                var productType = productTypes.Where(p => p.Id == id).FirstOrDefault();
                                if (productType != null)
                                {
                                    dataAnnouncement.rejectProducts.Add(_mapper.Map<ProductTypeViewModel>(productType));
                                }
                            }
                        }
                    }

                    if (dataAnnouncement.rejectPackaging != null)
                    {
                        dataAnnouncement.rejectPackagings = new List<ProductPackagingViewModel>();
                        foreach (string packagingIdStr in dataAnnouncement.rejectPackaging.Split(","))
                        {
                            int id = 0;
                            if (int.TryParse(packagingIdStr, out id))
                            {
                                var packaging = packagings.Where(p => p.Id == id).FirstOrDefault();
                                if (packaging != null)
                                {
                                    dataAnnouncement.rejectPackagings.Add(_mapper.Map<ProductPackagingViewModel>(packaging));
                                }
                            }
                        }
                    }
                }
                return new Response<IEnumerable<DriverAnnouncementViewModel>>(driverAnnouncementViewModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}