﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.DriverAnnouncement;
using iTrans.Transportation.Application.Enums;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using iTrans.Transportation.Domain.Entities;
using LinqKit;
using MediatR;

namespace iTrans.Transportation.Application.Features.DriverAnnouncements.Queries
{
    public class GetAnnouncementParameter : IRequest<Response<IEnumerable<DriverAnnouncementViewModel>>>
    {
        public int AcceptProductTypeId { get; set; }
        public string AcceptProductDetail { get; set; }
        public int AcceptProductPackagingId { get; set; }
        public string AcceptPackagingDetail { get; set; }
        public decimal DesiredPrice { get; set; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
    }
    public class GetAnnouncementParameterHandler : IRequestHandler<GetAnnouncementParameter, Response<IEnumerable<DriverAnnouncementViewModel>>>
    {
        private readonly IDriverAnnouncementRepositoryAsync _driverAnnouncementRepository;
        private readonly IMapper _mapper;
        public GetAnnouncementParameterHandler(IDriverAnnouncementRepositoryAsync driverAnnouncementRepository, IMapper mapper)
        {
            _driverAnnouncementRepository = driverAnnouncementRepository;
            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<DriverAnnouncementViewModel>>> Handle(GetAnnouncementParameter request, CancellationToken cancellationToken)
        {
            var validFilter = _mapper.Map<GetAnnouncementParameter>(request);
            Expression<Func<DriverAnnouncement, bool>> expression = PredicateBuilder.New<DriverAnnouncement>(false);
            var status = 1;// Enum.GetName(typeof(DriverAnnouncementStatus), 1);
            expression = x => x.Status == status;
            if (validFilter.DesiredPrice != null && validFilter.DesiredPrice != 0)
            {
                expression = expression.And(x => x.DesiredPrice == validFilter.DesiredPrice);
            }
             
            var driverAnnouncement = await _driverAnnouncementRepository.FindByConditionWithPage(expression, validFilter.PageNumber, validFilter.PageSize);
            var driverAnnouncementViewModel = _mapper.Map<IEnumerable<DriverAnnouncementViewModel>>(driverAnnouncement);
            return new Response<IEnumerable<DriverAnnouncementViewModel>>(driverAnnouncementViewModel);
        }
    }
}
