﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.DriverAnnouncement;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using iTrans.Transportation.Domain.Entities;
using MediatR;

namespace iTrans.Transportation.Application.Features.DriverAnnouncementes.Queries
{
    public class GetAllDriverAnnouncementParameter : IRequest<PagedResponse<IEnumerable<DriverAnnouncementViewModel>>>
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
    }
    public class GetAllDriverAnnouncementParameterHandler : IRequestHandler<GetAllDriverAnnouncementParameter, PagedResponse<IEnumerable<DriverAnnouncementViewModel>>>
    {
        private readonly IDriverAnnouncementRepositoryAsync _driverAnnouncementRepository;
        private readonly IMapper _mapper;
        public GetAllDriverAnnouncementParameterHandler(IDriverAnnouncementRepositoryAsync driverAnnouncementRepository, IMapper mapper)
        {
            _driverAnnouncementRepository = driverAnnouncementRepository;
            _mapper = mapper;
        }

        public async Task<PagedResponse<IEnumerable<DriverAnnouncementViewModel>>> Handle(GetAllDriverAnnouncementParameter request, CancellationToken cancellationToken)
        {
            var validFilter = _mapper.Map<GetAllDriverAnnouncementParameter>(request);
            Expression<Func<DriverAnnouncement, bool>> expression = x => x.Driver.Id != null;
            var driverAnnouncement = await _driverAnnouncementRepository.FindByConditionWithPage(expression, validFilter.PageNumber, validFilter.PageSize);
            var driverAnnouncementViewModel = _mapper.Map<IEnumerable<DriverAnnouncementViewModel>>(driverAnnouncement);
            return new PagedResponse<IEnumerable<DriverAnnouncementViewModel>>(driverAnnouncementViewModel, validFilter.PageNumber, validFilter.PageSize);
        }
    }
}
