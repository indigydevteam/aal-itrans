﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.DriverAnnouncement;
using iTrans.Transportation.Application.DTOs.OrderingAddress;
using iTrans.Transportation.Application.DTOs.OrderingProduct;
using iTrans.Transportation.Application.DTOs.ProductPackaging;
using iTrans.Transportation.Application.DTOs.ProductType;
using iTrans.Transportation.Application.Enums;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using LinqKit;
using MediatR;
using static System.Data.Entity.Infrastructure.Design.Executor;

namespace iTrans.Transportation.Application.Features.DriverAnnouncements.Queries
{
    public class SearchAnnouncementByOrderQuery : IRequest<Response<IEnumerable<DriverAnnouncementViewModel>>>
    {
        public virtual Guid OrderingId { get; set; }
        public decimal Price { set; get; }
        public virtual bool IsDriverOffer { get; set; }
        //public List<FilterLocationViewModel> Locations { get; set; }
        //public List<FilterProductViewModel> Products { get; set; }
        public int PageNumber { set; get; }
        public int PageSize { set; get; }
    }
    public class SearchAnnouncementByOrderQueryHandler : IRequestHandler<SearchAnnouncementByOrderQuery, Response<IEnumerable<DriverAnnouncementViewModel>>>
    {
        private readonly IOrderingRepositoryAsync _orderingRepository;
        private readonly IDriverAnnouncementRepositoryAsync _driverAnnouncementRepository;
        private readonly IProductTypeRepositoryAsync _productTypeRepository;
        private readonly IProductPackagingRepositoryAsync _productPackagingRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;
        public SearchAnnouncementByOrderQueryHandler(IOrderingRepositoryAsync orderingRepository, IDriverAnnouncementRepositoryAsync driverAnnouncementRepository, IProductTypeRepositoryAsync productTypeRepository
            , IProductPackagingRepositoryAsync productPackagingRepository, IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _orderingRepository = orderingRepository;
            _driverAnnouncementRepository = driverAnnouncementRepository;
            _productTypeRepository = productTypeRepository;
            _productPackagingRepository = productPackagingRepository;
            _mapper = mapper;
            _applicationLogRepository = applicationLogRepository;
        }

        public async Task<Response<IEnumerable<DriverAnnouncementViewModel>>> Handle(SearchAnnouncementByOrderQuery request, CancellationToken cancellationToken)
        {
            int count = 0;
            try
            {
                var data = (await _orderingRepository.FindByCondition(x => x.Id == request.OrderingId).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (data == null)
                {
                    throw new ApiException($"Ordering Not Found.");
                }


                if (request.PageNumber == 0 || request.PageSize == 0)
                {
                    request.PageNumber = 1;
                    request.PageSize = 10;
                }
                int startItem = (int)((request.PageSize * request.PageNumber) - request.PageSize);
                data.OrderingDesiredPrice = request.Price;
                data.IsOrderingDriverOffer = request.IsDriverOffer;
                data.Modified = DateTime.UtcNow;
                await _orderingRepository.UpdateAsync(data);

                var productTypes = (await _productTypeRepository.FindByCondition(x => x.Active == true).ConfigureAwait(false)).AsQueryable().ToList();
                var packagings = (await _productPackagingRepository.FindByCondition(x => x.Active == true).ConfigureAwait(false)).AsQueryable().ToList();
                var validFilter = _mapper.Map<SearchAnnouncementByOrderQuery>(request);
                Expression<Func<DriverAnnouncement, bool>> expression = PredicateBuilder.New<DriverAnnouncement>(false);
                #region ordering
                Expression<Func<Domain.DriverAnnouncement, decimal>> orderExpression = x => x.DesiredPrice;
                bool isDescending = false;
                #endregion
                var status = 1;
                expression = x => x.Status == status;
                if (request.IsDriverOffer && data.OrderingDesiredPrice > 0)
                {
                    expression = x => x.Status == status && x.DesiredPrice == data.OrderingDesiredPrice;
                }

                List<DriverAnnouncement> driverAnnouncement = new List<DriverAnnouncement>();

                bool isAddressExpression = false;
                Expression<Func<DriverAnnouncement, bool>> expressionTime = PredicateBuilder.New<DriverAnnouncement>(false);
                Expression<Func<DriverAnnouncement, bool>> expressionpickuppointAddress = PredicateBuilder.New<DriverAnnouncement>(false);
                Expression<Func<DriverAnnouncement, bool>> expressiondeliveryAddress = PredicateBuilder.New<DriverAnnouncement>(false);
                bool isCarExpression = false;
                Expression<Func<DriverAnnouncement, bool>> expressionCar = PredicateBuilder.New<DriverAnnouncement>(false);
                bool isProductExpression = false;
                Expression<Func<DriverAnnouncement, bool>> expressionProduct = PredicateBuilder.New<DriverAnnouncement>(false);
                bool isRejectProductExpression = false;
                Expression<Func<DriverAnnouncement, bool>> expressionRejectProduct = PredicateBuilder.New<DriverAnnouncement>(false);
                Expression<Func<DriverAnnouncement, bool>> expressionRejectPackaging = PredicateBuilder.New<DriverAnnouncement>(false);

                if (data.Addresses != null && data.Addresses.Count > 0)
                {
                    isAddressExpression = true;
                    OrderingAddress pickuppointAddress = data.Addresses.Where(a => a.AddressType == "pickuppoint" && a.Sequence == 1).FirstOrDefault();
                    OrderingAddress deliverypointAddress = data.Addresses.Where(a => a.AddressType == "deliverypoint").OrderByDescending(a => a.Sequence).FirstOrDefault();
                    
                    DateTime fromDate = new DateTime();
                    DateTime toDate = new DateTime();
                 
                    if (pickuppointAddress != null && pickuppointAddress.Date != null)
                    {
                        fromDate = new DateTime(pickuppointAddress.Date.Year, pickuppointAddress.Date.Month, pickuppointAddress.Date.Day, 0, 0, 0);
                    }
                    if (deliverypointAddress  != null && deliverypointAddress.Date != null)
                    {
                        toDate = new DateTime(deliverypointAddress.Date.Year, deliverypointAddress.Date.Month, deliverypointAddress.Date.Day, 23, 59, 59);
                    }
                    if (pickuppointAddress.Date != null && pickuppointAddress.Date != new DateTime() && deliverypointAddress.Date != null && deliverypointAddress.Date != new DateTime())
                    {
                        expressionTime =  x => (x.ReceiveDate.Date >= fromDate && x.ReceiveDate.Date <= toDate) && (x.SendDate.Date >= fromDate && x.SendDate.Date <= toDate);
                    }
                    else if (pickuppointAddress.Date != null && pickuppointAddress.Date != new DateTime())
                    {
                        expressionTime =  x => x.ReceiveDate.Date >= fromDate && x.SendDate.Date >= fromDate;
                    }
                    else if (deliverypointAddress.Date != null && deliverypointAddress.Date != new DateTime())
                    {
                        expressionTime =  x => x.ReceiveDate.Date <= toDate && x.SendDate.Date <= toDate;
                    }
                    #region pickup location condition

                    if (pickuppointAddress.Country != null && pickuppointAddress.Province != null && pickuppointAddress.District != null)
                    {
                        expressionpickuppointAddress = x => x.Locations.Where(l => l.Country == pickuppointAddress.Country && l.Province == pickuppointAddress.Province && l.District == pickuppointAddress.District).Count() > 0;
                    }
                    else if (pickuppointAddress.Country != null && pickuppointAddress.Province != null)
                    {
                        expressionpickuppointAddress = x => x.Locations.Where(l => l.Country == pickuppointAddress.Country && l.Province == pickuppointAddress.Province).Count() > 0;
                    }
                    else if (pickuppointAddress.Country != null)
                    {
                        expressionpickuppointAddress = x => x.Locations.Where(l => l.Country == pickuppointAddress.Country).Count() > 0;
                    }
                    #endregion

                    #region pickup location condition

                    if (deliverypointAddress.Country != null && deliverypointAddress.Province != null && deliverypointAddress.District != null)
                    {
                        expressiondeliveryAddress = x => x.Locations.Where(l => l.Country == deliverypointAddress.Country && l.Province == deliverypointAddress.Province && l.District == deliverypointAddress.District).Count() > 0;
                    }
                    else if (deliverypointAddress.Country != null && deliverypointAddress.Province != null)
                    {
                        expressiondeliveryAddress = x => x.Locations.Where(l => l.Country == deliverypointAddress.Country && l.Province == deliverypointAddress.Province).Count() > 0;
                    }
                    else if (deliverypointAddress.Country != null)
                    {
                        expressiondeliveryAddress = x => x.Locations.Where(l => l.Country == deliverypointAddress.Country).Count() > 0;
                    }
                    #endregion
                }

                #region car condition
                if (data.Cars.Count() > 0 && data.Cars[0].CarType != null  && data.Containers.Count() == 0)
                {
                    isCarExpression = true;
                    expressionCar = x => x.Driver.Cars.Where(c => c.CarType == data.Cars[0].CarType).Count() > 0;
                }
                else if (data.Containers.Count() > 0)
                {
                    isCarExpression = true;
                    expressionCar = x => x.Driver.Cars.Where(c => c.CarType.IsContainer == true).Count() > 0;
                }
                //if (data.Cars.Count() > 0 && data.Cars[0].CarType != null && data.Cars[0].CarSpecification != null && data.Containers.Count() == 0)
                //{
                //    isCarExpression = true;
                //    expressionCar = x => x.Driver.Cars.Where(c => c.CarType == data.Cars[0].CarType && c.CarSpecification == data.Cars[0].CarSpecification).Count() > 0;
                //}
                //else if (data.Cars.Count() > 0 && data.Cars[0].CarType != null && data.Containers.Count() == 0)
                //{
                //    isCarExpression = true;
                //    expressionCar = x => x.Driver.Cars.Where(c => c.CarType == data.Cars[0].CarType).Count() > 0;
                //}
                //else if (data.Containers.Count() > 0)
                //{
                //    isCarExpression = true;
                //    expressionCar = x => x.Driver.Cars.Where(c => c.CarType.IsContainer == true).Count() > 0;
                //}
                #endregion
                #region Product
                if (data.Products.Count() > 0)
                {
                    //List<string> productIds = new List<string>();
                    //List<string> packagingIds = new List<string>();
                    bool firstloop = true;
                    foreach (OrderingProduct product in data.Products)
                    {
                        if (product.ProductType.Trim() != "" && product.Packaging.Trim() != "") isProductExpression = true;
                        foreach (string productTypeId in product.ProductType.Split(","))
                        {
                            if (productTypeId.Trim() != "")
                            {
                               if( firstloop) expressionProduct = x => x.AcceptProduct.Contains(productTypeId) || x.AcceptProduct == null || x.AcceptProduct.Trim() == "";
                                else expressionProduct = expressionProduct.And(x => x.AcceptProduct.Contains(productTypeId) || x.AcceptProduct == null || x.AcceptProduct.Trim() == "");
                                firstloop = false;
                            }
                        }
                        //foreach (string packagingId in product.Packaging.Split(","))
                        //{
                        //    if (packagingId.Trim() != "")
                        //        expressionProduct = expressionProduct.And(x => x.AcceptPackaging.Contains(packagingId) || x.AcceptPackaging == null || x.AcceptPackaging.Trim() == "");
                        //}
                    }
                }
                #endregion
                #region Reject Product
                if (data.Products.Count() > 0)
                {
                    bool firstloop = true;
                    foreach (OrderingProduct product in data.Products)
                    {
                        if (product.ProductType.Trim() != "" && product.Packaging.Trim() != "") isRejectProductExpression = true;
                        foreach (string productTypeId in product.ProductType.Split(","))
                        {
                            if (productTypeId.Trim() != "")
                            {
                                if (firstloop) expressionRejectProduct =  x => !x.RejectProduct.Contains(productTypeId);
                                else expressionRejectProduct = expressionRejectProduct.Or(x => !x.RejectProduct.Contains(productTypeId));
                                firstloop = false;
                            }
                        }
                        //foreach (string packagingId in product.Packaging.Split(","))
                        //{
                        //    if (packagingId.Trim() != "")
                        //        expressionRejectProduct = expressionRejectProduct.Or(x => !x.RejectPackaging.Contains(packagingId));
                        //}
                    }

                }
                #endregion
                if (isAddressExpression && isCarExpression && isProductExpression && isRejectProductExpression)
                {
                    expression = expression.And(expressionTime).And(expressionpickuppointAddress).And(expressiondeliveryAddress).And(expressionCar).And(expressionProduct).And(expressionRejectProduct) ;
                }
                else if (isAddressExpression && isCarExpression && isProductExpression && !isRejectProductExpression)
                {
                    expression = expression.And(expressionTime).And(expressionpickuppointAddress).And(expressiondeliveryAddress).And(expressionCar).And(expressionProduct);
                }
                else if (isAddressExpression && isCarExpression && !isProductExpression && !isRejectProductExpression)
                {
                    expression = expression.And(expressionTime).And(expressionpickuppointAddress).And(expressiondeliveryAddress).And(expressionCar);
                }
                else if (isAddressExpression && !isCarExpression && !isProductExpression && !isRejectProductExpression)
                {
                    expression = expression.And(expressionTime).And(expressionpickuppointAddress).And(expressiondeliveryAddress);
                }
                else if (!isAddressExpression && isCarExpression && isProductExpression && isRejectProductExpression)
                {
                    expression = expression.And(expressionCar).And(expressionProduct).And(expressionRejectProduct);
                }
                else if (!isAddressExpression && !isCarExpression && isProductExpression && isRejectProductExpression)
                {
                    expression = expression.And(expressionProduct).And(expressionRejectProduct);
                }
                else if (!isAddressExpression && !isCarExpression && !isProductExpression && isRejectProductExpression)
                {
                    expression = expression.And(expressionRejectProduct);
                }
                else if (isAddressExpression && !isCarExpression && isProductExpression && isRejectProductExpression)
                {
                    expression = expression.And(expressionTime).And(expressionpickuppointAddress).And(expressiondeliveryAddress).And(expressionProduct).And(expressionRejectProduct);
                }
                else if (isAddressExpression && !isCarExpression && !isProductExpression && isRejectProductExpression)
                {
                    expression = expression.And(expressionTime).And(expressionpickuppointAddress).And(expressiondeliveryAddress).And(expressionRejectProduct);
                }
                else if (!isAddressExpression && !isCarExpression && isProductExpression && !isRejectProductExpression)
                {
                    expression = expression.And(expressionProduct);
                }
                else if (isAddressExpression && !isCarExpression &&  isProductExpression && !isRejectProductExpression)
                {
                    expression = expression.And(expressionTime).And(expressionpickuppointAddress).And(expressiondeliveryAddress).And(expressionProduct);
                }
                else if (!isAddressExpression && isCarExpression && !isProductExpression && isRejectProductExpression)
                {
                    expression = expression.And(expressionCar).And(expressionRejectProduct);
                }

                int itemCount = _driverAnnouncementRepository.GetItemCount(expression);
                if (startItem <= itemCount)
                {
                    var driverAnnouncementTemp = await _driverAnnouncementRepository.FindByConditionWithPage(expression, orderExpression, isDescending, request.PageNumber, request.PageSize);
                    driverAnnouncement.AddRange(driverAnnouncementTemp.Except(driverAnnouncement));
                    startItem = 0;
                }
                else
                {
                    startItem = (startItem - itemCount) + 1;
                }
                //// confition 2
                if (driverAnnouncement.Count() < validFilter.PageSize)
                {
                    expression = PredicateBuilder.New<DriverAnnouncement>(false);
                    expression = x => x.Status == status;
                    if (request.IsDriverOffer && data.OrderingDesiredPrice > 0)
                    {
                        expression = x => x.Status == status && x.DesiredPrice == data.OrderingDesiredPrice;
                    }

                    isAddressExpression = false;
                    expressionTime = PredicateBuilder.New<DriverAnnouncement>(false);
                    expressionpickuppointAddress = PredicateBuilder.New<DriverAnnouncement>(false);
                    expressiondeliveryAddress = PredicateBuilder.New<DriverAnnouncement>(false);
                    //isCarExpression = false;
                    //expressionCar = PredicateBuilder.New<DriverAnnouncement>(false);
                    //isProductExpression = false;
                    //expressionProduct = PredicateBuilder.New<DriverAnnouncement>(false);
                    //isRejectProductExpression = false;
                    //expressionRejectProduct = PredicateBuilder.New<DriverAnnouncement>(false);
                    //expressionRejectPackaging = PredicateBuilder.New<DriverAnnouncement>(false);

                    if (data.Addresses != null && data.Addresses.Count > 0)
                    {
                        isAddressExpression = true;
                        OrderingAddress pickuppointAddress = data.Addresses.Where(a => a.AddressType == "pickuppoint" && a.Sequence == 1).FirstOrDefault();
                        OrderingAddress deliverypointAddress = data.Addresses.Where(a => a.AddressType == "deliverypoint").OrderByDescending(a => a.Sequence).FirstOrDefault();
                        DateTime fromDate = new DateTime();
                        DateTime toDate = new DateTime();
                        if (pickuppointAddress != null && pickuppointAddress.Date != null)
                        {
                            fromDate = new DateTime(pickuppointAddress.Date.Year, pickuppointAddress.Date.Month, pickuppointAddress.Date.Day, 0, 0, 0);
                        }
                        if (deliverypointAddress != null && deliverypointAddress.Date != null)
                        {
                            toDate = new DateTime(deliverypointAddress.Date.Year, deliverypointAddress.Date.Month, deliverypointAddress.Date.Day, 23, 59, 59);
                        }
                        if (pickuppointAddress.Date != null && pickuppointAddress.Date != new DateTime() && deliverypointAddress.Date != null && deliverypointAddress.Date != new DateTime())
                        {
                            expressionTime = x => (x.ReceiveDate.Date >= fromDate && x.ReceiveDate.Date <= toDate) && (x.SendDate.Date >= fromDate && x.SendDate.Date <= toDate);
                        }
                        else if (pickuppointAddress.Date != null && pickuppointAddress.Date != new DateTime())
                        {
                            expressionTime = x => x.ReceiveDate.Date >= fromDate && x.SendDate.Date >= fromDate;
                        }
                        else if (deliverypointAddress.Date != null && deliverypointAddress.Date != new DateTime())
                        {
                            expressionTime = x => x.ReceiveDate.Date <= toDate && x.SendDate.Date <= toDate;
                        }
                        #region pickup location condition

                        if (pickuppointAddress.Country != null && pickuppointAddress.Province != null)
                        {
                            expressionpickuppointAddress = x => x.Locations.Where(l => l.Country == pickuppointAddress.Country && l.Province == pickuppointAddress.Province).Count() > 0;
                        }
                        else if (pickuppointAddress.Country != null)
                        {
                            expressionpickuppointAddress = x => x.Locations.Where(l => l.Country == pickuppointAddress.Country).Count() > 0;
                        }
                        #endregion

                        #region pickup location condition

                        if (deliverypointAddress.Country != null && deliverypointAddress.Province != null)
                        {
                            expressiondeliveryAddress = x => x.Locations.Where(l => l.Country == deliverypointAddress.Country && l.Province == deliverypointAddress.Province).Count() > 0;
                        }
                        else if (deliverypointAddress.Country != null)
                        {
                            expressiondeliveryAddress = x => x.Locations.Where(l => l.Country == deliverypointAddress.Country).Count() > 0;
                        }
                        #endregion
                    }

                    #region car condition
                    if (data.Cars.Count() > 0 && data.Cars[0].CarType != null && data.Containers.Count() == 0)
                    {
                        isCarExpression = true;
                        expressionCar = x => x.Driver.Cars.Where(c => c.CarType == data.Cars[0].CarType).Count() > 0;
                    }
                    else if (data.Containers.Count() > 0)
                    {
                        isCarExpression = true;
                        expressionCar = x => x.Driver.Cars.Where(c => c.CarType.IsContainer == true).Count() > 0;
                    }
                    //if (data.Cars.Count() > 0 && data.Cars[0].CarType != null && data.Cars[0].CarSpecification != null && data.Containers.Count() == 0)
                    //{
                    //    isCarExpression = true;
                    //    expressionCar = x => x.Driver.Cars.Where(c => c.CarType == data.Cars[0].CarType && c.CarSpecification == data.Cars[0].CarSpecification).Count() > 0;
                    //}
                    //else if (data.Cars.Count() > 0 && data.Cars[0].CarType != null && data.Containers.Count() == 0)
                    //{
                    //    isCarExpression = true;
                    //    expressionCar = x => x.Driver.Cars.Where(c => c.CarType == data.Cars[0].CarType).Count() > 0;
                    //}
                    //else if (data.Containers.Count() > 0)
                    //{
                    //    isCarExpression = true;
                    //    expressionCar = x => x.Driver.Cars.Where(c => c.CarType.IsContainer == true).Count() > 0;
                    //}
                    #endregion

                    #region Product
                    if (data.Products.Count() > 0)
                    {

                        //List<string> productIds = new List<string>();
                        //List<string> packagingIds = new List<string>();
                        bool firstloop = true;
                        foreach (OrderingProduct product in data.Products)
                        {
                            if (product.ProductType.Trim() != "" && product.Packaging.Trim() != "") isProductExpression = true;
                            foreach (string productTypeId in product.ProductType.Split(","))
                            {
                                if (productTypeId.Trim() != "")
                                {
                                    if (firstloop) expressionProduct = x => x.AcceptProduct.Contains(productTypeId) || x.AcceptProduct == null || x.AcceptProduct.Trim() == "";
                                    else expressionProduct = expressionProduct.And(x => x.AcceptProduct.Contains(productTypeId) || x.AcceptProduct == null || x.AcceptProduct.Trim() == "");
                                    firstloop = false;
                                }
                            }
                            //foreach (string packagingId in product.Packaging.Split(","))
                            //{
                            //    if (packagingId.Trim() != "")
                            //        expressionProduct = expressionProduct.And(x => x.AcceptPackaging.Contains(packagingId) || x.AcceptPackaging == null || x.AcceptPackaging.Trim() == "");
                            //}
                        }
                    }
                    #endregion
                    #region car Reject Product
                    if (data.Products.Count() > 0)
                    {
                        bool firstloop = true;
                        foreach (OrderingProduct product in data.Products)
                        {
                            if (product.ProductType.Trim() != "" && product.Packaging.Trim() != "") isRejectProductExpression = true;
                            foreach (string productTypeId in product.ProductType.Split(","))
                            {
                                if (productTypeId.Trim() != "")
                                {
                                    if (firstloop) expressionRejectProduct = x => !x.RejectProduct.Contains(productTypeId);
                                    else expressionRejectProduct = expressionRejectProduct.Or(x => !x.RejectProduct.Contains(productTypeId));
                                    firstloop = false;
                                }
                            }
                            //foreach (string packagingId in product.Packaging.Split(","))
                            //{
                            //    if (packagingId.Trim() != "")
                            //        expressionRejectProduct = expressionRejectProduct.Or(x => !x.RejectPackaging.Contains(packagingId));
                            //}
                        }

                    }
                    #endregion

                    if (isAddressExpression && isCarExpression && isProductExpression && isRejectProductExpression)
                    {
                        expression = expression.And(expressionTime).And(expressionpickuppointAddress).And(expressiondeliveryAddress).And(expressionCar).And(expressionProduct).And(expressionRejectProduct);
                    }
                    else if (isAddressExpression && isCarExpression && isProductExpression && !isRejectProductExpression)
                    {
                        expression = expression.And(expressionTime).And(expressionpickuppointAddress).And(expressiondeliveryAddress).And(expressionCar).And(expressionProduct);
                    }
                    else if (isAddressExpression && isCarExpression && !isProductExpression && !isRejectProductExpression)
                    {
                        expression = expression.And(expressionTime).And(expressionpickuppointAddress).And(expressiondeliveryAddress).And(expressionCar);
                    }
                    else if (isAddressExpression && !isCarExpression && !isProductExpression && !isRejectProductExpression)
                    {
                        expression = expression.And(expressionTime).And(expressionpickuppointAddress).And(expressiondeliveryAddress);
                    }
                    else if (!isAddressExpression && isCarExpression && isProductExpression && isRejectProductExpression)
                    {
                        expression = expression.And(expressionCar).And(expressionProduct).And(expressionRejectProduct);
                    }
                    else if (!isAddressExpression && !isCarExpression && isProductExpression && isRejectProductExpression)
                    {
                        expression = expression.And(expressionProduct).And(expressionRejectProduct);
                    }
                    else if (!isAddressExpression && !isCarExpression && !isProductExpression && isRejectProductExpression)
                    {
                        expression = expression.And(expressionRejectProduct);
                    }
                    else if (isAddressExpression && !isCarExpression && isProductExpression && isRejectProductExpression)
                    {
                        expression = expression.And(expressionTime).And(expressionpickuppointAddress).And(expressiondeliveryAddress).And(expressionProduct).And(expressionRejectProduct);
                    }
                    else if (isAddressExpression && !isCarExpression && !isProductExpression && isRejectProductExpression)
                    {
                        expression = expression.And(expressionTime).And(expressionpickuppointAddress).And(expressiondeliveryAddress).And(expressionRejectProduct);
                    }
                    else if (!isAddressExpression && !isCarExpression && isProductExpression && !isRejectProductExpression)
                    {
                        expression = expression.And(expressionProduct);
                    }
                    else if (isAddressExpression && !isCarExpression && isProductExpression && !isRejectProductExpression)
                    {
                        expression = expression.And(expressionTime).And(expressionpickuppointAddress).And(expressiondeliveryAddress).And(expressionProduct);
                    }
                    else if (!isAddressExpression && isCarExpression && !isProductExpression && isRejectProductExpression)
                    {
                        expression = expression.And(expressionCar).And(expressionRejectProduct);
                    }

                    int itemCount2 = _driverAnnouncementRepository.GetItemCount(expression);
                    if (startItem < itemCount2)
                    {
                        var driverAnnouncementTemp = await _driverAnnouncementRepository.FindByConditionWithItemNumber(expression, orderExpression, isDescending, startItem, validFilter.PageSize - driverAnnouncement.Count());

                        driverAnnouncement.AddRange(driverAnnouncementTemp.Except(driverAnnouncement));
                    }
                }

                string Test999 = "";
                #region test
                foreach (DriverAnnouncement loopItem in driverAnnouncement)
                {
                    Test999 = Test999 + loopItem.Id.ToString() + "\r\n";
                }
                #endregion

                var driverAnnouncementViewModel = _mapper.Map<IEnumerable<DriverAnnouncementViewModel>>(driverAnnouncement);
                foreach (DriverAnnouncementViewModel dataAnnouncement in driverAnnouncementViewModel)
                {
                    count = count + 1;
                    if (dataAnnouncement.driver != null)
                    {
                        dataAnnouncement.driver.files.RemoveAll(f => f.IsApprove == false);
                    }
                    if (dataAnnouncement.acceptProduct != null)
                    {
                        dataAnnouncement.acceptProducts = new List<ProductTypeViewModel>();
                        foreach (string productIdStr in dataAnnouncement.acceptProduct.Split(","))
                        {
                            int id = 0;
                            if (int.TryParse(productIdStr, out id))
                            {
                                var productType = productTypes.Where(p => p.Id == id).FirstOrDefault();
                                if (productType != null)
                                {
                                    dataAnnouncement.acceptProducts.Add(_mapper.Map<ProductTypeViewModel>(productType));
                                }
                            }
                        }
                    }

                    if (dataAnnouncement.acceptPackaging != null)
                    {
                        dataAnnouncement.acceptPackagings = new List<ProductPackagingViewModel>();
                        foreach (string packagingIdStr in dataAnnouncement.acceptPackaging.Split(","))
                        {
                            int id = 0;
                            if (int.TryParse(packagingIdStr, out id))
                            {
                                var packaging = packagings.Where(p => p.Id == id).FirstOrDefault();
                                if (packaging != null)
                                {
                                    dataAnnouncement.acceptPackagings.Add(_mapper.Map<ProductPackagingViewModel>(packaging));
                                }
                            }
                        }
                    }

                    if (dataAnnouncement.rejectProduct != null)
                    {
                        dataAnnouncement.rejectProducts = new List<ProductTypeViewModel>();
                        foreach (string productIdStr in dataAnnouncement.rejectProduct.Split(","))
                        {
                            int id = 0;
                            if (int.TryParse(productIdStr, out id))
                            {
                                var productType = productTypes.Where(p => p.Id == id).FirstOrDefault();
                                if (productType != null)
                                {
                                    dataAnnouncement.rejectProducts.Add(_mapper.Map<ProductTypeViewModel>(productType));
                                }
                            }
                        }
                    }

                    if (dataAnnouncement.rejectPackaging != null)
                    {
                        dataAnnouncement.rejectPackagings = new List<ProductPackagingViewModel>();
                        foreach (string packagingIdStr in dataAnnouncement.rejectPackaging.Split(","))
                        {
                            int id = 0;
                            if (int.TryParse(packagingIdStr, out id))
                            {
                                var packaging = packagings.Where(p => p.Id == id).FirstOrDefault();
                                if (packaging != null)
                                {
                                    dataAnnouncement.rejectPackagings.Add(_mapper.Map<ProductPackagingViewModel>(packaging));
                                }
                            }
                        }
                    }
                }
                return new Response<IEnumerable<DriverAnnouncementViewModel>>(driverAnnouncementViewModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}