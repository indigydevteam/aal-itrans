﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.DriverAnnouncement;
using iTrans.Transportation.Application.DTOs.ProductPackaging;
using iTrans.Transportation.Application.DTOs.ProductType;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.DriverAnnouncements.Queries
{
    public class GetAnnouncementByIdQuery : IRequest<Response<AnnouncementWithDriverViewModel>>
    {
        public int Id { get; set; }
        public Guid DriverId { get; set; }
    }
    public class GetAnnouncementByIdQueryHandler : IRequestHandler<GetAnnouncementByIdQuery, Response<AnnouncementWithDriverViewModel>>
    {
        private readonly IDriverAnnouncementRepositoryAsync _driverAnnouncementRepository;
        private readonly IProductTypeRepositoryAsync _productTypeRepository;
        private readonly IProductPackagingRepositoryAsync _productPackagingRepository;
        private readonly IMapper _mapper;
        public GetAnnouncementByIdQueryHandler(IDriverAnnouncementRepositoryAsync driverAnnouncementRepository, IProductTypeRepositoryAsync productTypeRepository, IProductPackagingRepositoryAsync productPackagingRepository, IMapper mapper)
        {
            _driverAnnouncementRepository = driverAnnouncementRepository;
            _productTypeRepository = productTypeRepository;
            _productPackagingRepository = productPackagingRepository;
            _mapper = mapper;
        }
        public async Task<Response<AnnouncementWithDriverViewModel>> Handle(GetAnnouncementByIdQuery request, CancellationToken cancellationToken)
        {
            var productTypes = (await _productTypeRepository.FindByCondition(x => x.Active == true).ConfigureAwait(false)).AsQueryable().ToList();
            var packagings = (await _productPackagingRepository.FindByCondition(x => x.Active == true).ConfigureAwait(false)).AsQueryable().ToList();

            var driverObject = (await _driverAnnouncementRepository.FindByCondition(x => x.Id.Equals(request.Id) && x.Driver.Id.Equals(request.DriverId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            var driverAnnouncement = _mapper.Map<AnnouncementWithDriverViewModel>(driverObject);
            if (driverAnnouncement != null && driverAnnouncement.driver != null)
            {
                driverAnnouncement.driver.files.RemoveAll(f => f.IsDelete);
            }
            driverAnnouncement.acceptProducts = new List<ProductTypeViewModel>();

            if (driverAnnouncement.acceptProduct != null)
            {
                foreach (string productIdStr in driverAnnouncement.acceptProduct.Split(","))
                {
                    int id = 0;
                    if (int.TryParse(productIdStr, out id))
                    {
                        var productType = productTypes.Where(p => p.Id == id).FirstOrDefault();
                        if (productType != null)
                        {
                            driverAnnouncement.acceptProducts.Add(_mapper.Map<ProductTypeViewModel>(productType));
                        }
                    }
                }
            }

            if (driverAnnouncement.acceptPackaging != null)
            {
                driverAnnouncement.acceptPackagings = new List<ProductPackagingViewModel>();
                foreach (string packagingIdStr in driverAnnouncement.acceptPackaging.Split(","))
                {
                    int id = 0;
                    if (int.TryParse(packagingIdStr, out id))
                    {
                        var packaging = packagings.Where(p => p.Id == id).FirstOrDefault();
                        if (packaging != null)
                        {
                            driverAnnouncement.acceptPackagings.Add(_mapper.Map<ProductPackagingViewModel>(packaging));
                        }
                    }
                }
            }

            if (driverAnnouncement.rejectProduct != null)
            {
                driverAnnouncement.rejectProducts = new List<ProductTypeViewModel>();
                foreach (string productIdStr in driverAnnouncement.rejectProduct.Split(","))
                {
                    int id = 0;
                    if (int.TryParse(productIdStr, out id))
                    {
                        var productType = productTypes.Where(p => p.Id == id).FirstOrDefault();
                        if (productType != null)
                        {
                            driverAnnouncement.rejectProducts.Add(_mapper.Map<ProductTypeViewModel>(productType));
                        }
                    }
                }
            }

            if (driverAnnouncement.rejectPackaging != null)
            {
                driverAnnouncement.rejectPackagings = new List<ProductPackagingViewModel>();
                foreach (string packagingIdStr in driverAnnouncement.rejectPackaging.Split(","))
                {
                    int id = 0;
                    if (int.TryParse(packagingIdStr, out id))
                    {
                        var packaging = packagings.Where(p => p.Id == id).FirstOrDefault();
                        if (packaging != null)
                        {
                            driverAnnouncement.rejectPackagings.Add(_mapper.Map<ProductPackagingViewModel>(packaging));
                        }
                    }
                }
            }
            return new Response<AnnouncementWithDriverViewModel>(driverAnnouncement);
        }
    }
}
