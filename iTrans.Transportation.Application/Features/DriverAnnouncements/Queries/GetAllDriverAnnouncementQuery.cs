﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.DriverAnnouncement;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.DriverAnnouncements.Queries
{
    public class GetAllDriverAnnouncementQuery : IRequest<Response<IEnumerable<DriverAnnouncementViewModel>>>
    {

    }
    public class GetAllDriverAnnouncementQueryHandler : IRequestHandler<GetAllDriverAnnouncementQuery, Response<IEnumerable<DriverAnnouncementViewModel>>>
    {
        private readonly IDriverAnnouncementRepositoryAsync _driverAnnouncementRepository;
        private readonly IMapper _mapper;
        public GetAllDriverAnnouncementQueryHandler(IDriverAnnouncementRepositoryAsync driverAnnouncementRepository, IMapper mapper)
        {
            _driverAnnouncementRepository = driverAnnouncementRepository;
            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<DriverAnnouncementViewModel>>> Handle(GetAllDriverAnnouncementQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var validFilter = _mapper.Map<GetAllDriverAnnouncementQuery>(request);
                var driver = await _driverAnnouncementRepository.GetAllAsync();
                var driverAnnouncementViewModel = _mapper.Map<IEnumerable<DriverAnnouncementViewModel>>(driver);
                return new Response<IEnumerable<DriverAnnouncementViewModel>>(driverAnnouncementViewModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
