﻿using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Interfaces.Services;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using iTrans.Transportation.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;


namespace iTrans.Transportation.Application.Features.NotificationUser.Command
{
    public partial class CreateNotificationUserCommand : IRequest<Response<int>>
    {
        public Guid? UserId { get; set; }
        public string Device { get; set; }
        public string OneSignalId { get; set; }
        public string Action { get; set; }
    }
    public class CreateNotificationUserCommandHandler : IRequestHandler<CreateNotificationUserCommand, Response<int>>
    {
        private readonly INotificationUserRepositoryAsync _notificationUserRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;
        public CreateNotificationUserCommandHandler(INotificationUserRepositoryAsync notificationUserRepository, IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _notificationUserRepository = notificationUserRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(CreateNotificationUserCommand request, CancellationToken cancellationToken)
        {
            try
            {
                if (request.Action == "Register")
                { 
                    var notificationUserObject = (await _notificationUserRepository.FindByCondition(x => x.OneSignalId == request.OneSignalId && x.UserId == request.UserId).ConfigureAwait(false)).SingleOrDefault();

                    if (notificationUserObject == null)
                    {
                        var notificationUser = _mapper.Map<Domain.NotificationUser>(request);

                        notificationUser.IsActive = true;
                        notificationUser.Created = DateTime.UtcNow;
                        notificationUser.Modified = DateTime.UtcNow;
                        notificationUser.CreatedBy = request.UserId.ToString();
                        notificationUser.ModifiedBy = request.UserId.ToString();

                        await _notificationUserRepository.AddAsync(notificationUser);
                        var log = new CreateAppLog(_applicationLogRepository);
                        log.Create("Notification", "Register Notification", "Create", Newtonsoft.Json.JsonConvert.SerializeObject(notificationUser));
                        return new Response<int>(notificationUser.OneSignalId);
                    }

                    return new Response<int>(notificationUserObject.OneSignalId);
                }
                else
                {
                    var notificationUser = (await _notificationUserRepository.FindByCondition(x => x.OneSignalId == request.OneSignalId).ConfigureAwait(false)).FirstOrDefault();

                    notificationUser.IsActive = false;
                    notificationUser.Created = DateTime.UtcNow;
                    notificationUser.Modified = DateTime.UtcNow;
                    notificationUser.CreatedBy = request.UserId.ToString();
                    notificationUser.ModifiedBy = request.UserId.ToString();

                    var notificationUserObject = await _notificationUserRepository.AddAsync(notificationUser);
                    var log = new CreateAppLog(_applicationLogRepository);
                    log.CreateLog("Notification", "Unregister Notification", "Create", Newtonsoft.Json.JsonConvert.SerializeObject(notificationUser),request.UserId.ToString());
                    return new Response<int>(notificationUserObject.OneSignalId);
                } 
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
