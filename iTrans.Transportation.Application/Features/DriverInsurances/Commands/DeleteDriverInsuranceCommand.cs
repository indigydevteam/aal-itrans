﻿using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.DriverInsurances.Commands
{
    public class DeleteDriverInsuranceByIdCommand : IRequest<Response<int>>
    {
        public int Id { get; set; }
        public class DeleteDriverInsuranceByIdCommandHandler : IRequestHandler<DeleteDriverInsuranceByIdCommand, Response<int>>
        {
            private readonly IDriverInsuranceRepositoryAsync _driverInsuranceRepository;
            public DeleteDriverInsuranceByIdCommandHandler(IDriverInsuranceRepositoryAsync driverInsuranceRepository)
            {
                _driverInsuranceRepository = driverInsuranceRepository;
            }
            public async Task<Response<int>> Handle(DeleteDriverInsuranceByIdCommand command, CancellationToken cancellationToken)
            {
                var driverInsurance = (await _driverInsuranceRepository.FindByCondition(x => x.Id == command.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (driverInsurance == null)
                {
                    throw new ApiException($"DriverInsurance Not Found.");
                }
                else
                {
                    await _driverInsuranceRepository.DeleteAsync(driverInsurance);
                    return new Response<int>(driverInsurance.Id);
                }
            }
        }
    }
}
