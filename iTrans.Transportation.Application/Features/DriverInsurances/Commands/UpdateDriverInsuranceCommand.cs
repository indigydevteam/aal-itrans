﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;

namespace iTrans.Transportation.Application.Features.DriverInsurances.Commands
{
    public class UpdateDriverInsuranceCommand : IRequest<Response<int>>
    {
        public int Id { get; set; }
        public Guid DriverId { get; set; }
        public string Product { get; set; }
        public string ProductType { get; set; }
        public string ProductTypeDetail { get; set; }
        public string WarrantyLimit { get; set; }
        public string WarrantyPeriod { get; set; }
        public decimal TotalAmount { get; set; }

        public class UpdateDriverInsuranceCommandHandler : IRequestHandler<UpdateDriverInsuranceCommand, Response<int>>
        {
            private readonly IDriverInsuranceRepositoryAsync _driverInsuranceRepository;
            private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
            private readonly IMapper _mapper;

            public UpdateDriverInsuranceCommandHandler(IDriverInsuranceRepositoryAsync driverInsuranceRepository, IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
            {
                _driverInsuranceRepository = driverInsuranceRepository;
                _applicationLogRepository = applicationLogRepository;
                _mapper = mapper;
            }

            public async Task<Response<int>> Handle(UpdateDriverInsuranceCommand request, CancellationToken cancellationToken)
            {
                var driverInsurance = (await _driverInsuranceRepository.FindByCondition(x => x.Id == request.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (driverInsurance == null)
                {
                    throw new ApiException($"DriverInsurance Not Found.");
                }
                else
                {
                    driverInsurance.DriverId = request.DriverId;
                    driverInsurance.Product = request.Product;
                    driverInsurance.ProductType = request.ProductType;
                    driverInsurance.ProductTypeDetail = request.ProductTypeDetail;
                    driverInsurance.WarrantyLimit = request.WarrantyLimit;
                    driverInsurance.WarrantyPeriod = request.WarrantyPeriod;
                    driverInsurance.TotalAmount = request.TotalAmount;
                    driverInsurance.Modified =DateTime.UtcNow;
                
                    await _driverInsuranceRepository.UpdateAsync(driverInsurance);
                    var log = new CreateAppLog(_applicationLogRepository);
                    log.Create("Driver", "Driver Insurance", "Edit", Newtonsoft.Json.JsonConvert.SerializeObject(driverInsurance));
                    return new Response<int>(driverInsurance.Id);
                }
            }
        }
    }
}
