﻿using AutoMapper;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Interfaces.Services;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using iTrans.Transportation.Domain.Entities;
using MediatR;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.DriverInsurances.Commands
{
    public partial class CreateDriverInsuranceCommand : IRequest<Response<int>>
    {

        public  Guid DriverId { get; set; }
        public  string Product { get; set; }
        public  string ProductType { get; set; }
        public  string ProductTypeDetail { get; set; }
        public  string WarrantyLimit { get; set; }
        public  string WarrantyPeriod { get; set; }
        public  decimal TotalAmount { get; set; }
    }
    public class CreateDriverInsuranceCommandHandler : IRequestHandler<CreateDriverInsuranceCommand, Response<int>>
    {
        private readonly IDriverInsuranceRepositoryAsync _driverInsuranceRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IMapper _mapper;
        public CreateDriverInsuranceCommandHandler(IDriverInsuranceRepositoryAsync driverInsuranceRepository,IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
        {
            _driverInsuranceRepository = driverInsuranceRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(CreateDriverInsuranceCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var driverInsurance = _mapper.Map<DriverInsurance>(request);
                driverInsurance.Created = DateTime.UtcNow;
                driverInsurance.Modified = DateTime.UtcNow;
                var driverInsuranceObject = await _driverInsuranceRepository.AddAsync(driverInsurance);
                var log = new CreateAppLog(_applicationLogRepository);
                log.Create("Driver", "Driver Insurance", "Create", Newtonsoft.Json.JsonConvert.SerializeObject(driverInsurance));
                return new Response<int>(driverInsuranceObject.Id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
