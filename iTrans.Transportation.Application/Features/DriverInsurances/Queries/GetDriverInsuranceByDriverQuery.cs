﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.DriverInsurance;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.DriverInsurances.Queries
{
    public class GetDriverInsuranceByDriverQuery : IRequest<Response<IEnumerable<DriverInsuranceViewModel>>>
    {
        public Guid DriverId { get; set; }
    }
    public class GetDriverInsuranceByDriverQueryHandler : IRequestHandler<GetDriverInsuranceByDriverQuery, Response<IEnumerable<DriverInsuranceViewModel>>>
    {
        private readonly IDriverInsuranceRepositoryAsync _driverInsuranceRepository;
        private readonly IMapper _mapper;
        public GetDriverInsuranceByDriverQueryHandler(IDriverInsuranceRepositoryAsync driverInsuranceRepository, IMapper mapper)
        {
            _driverInsuranceRepository = driverInsuranceRepository;
            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<DriverInsuranceViewModel>>> Handle(GetDriverInsuranceByDriverQuery request, CancellationToken cancellationToken)
        {
            try
            {

                var driverInsurance = (await _driverInsuranceRepository.FindByCondition(x => x.DriverId == request.DriverId).ConfigureAwait(false)).AsQueryable().ToList();

                var driverInsuranceViewModel = _mapper.Map<IEnumerable<DriverInsuranceViewModel>>(driverInsurance);
                return new Response<IEnumerable<DriverInsuranceViewModel>>(driverInsuranceViewModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
