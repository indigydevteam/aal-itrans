﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.DriverInsurance;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.DriverInsurances.Queries
{
    public class GetDriverInsuranceByIdQuery : IRequest<Response<DriverInsuranceViewModel>>
    {
        public int Id { get; set; }
    }
    public class GetDriverInsuranceByIdQueryHandler : IRequestHandler<GetDriverInsuranceByIdQuery, Response<DriverInsuranceViewModel>>
    {
        private readonly IDriverInsuranceRepositoryAsync _driverInsuranceRepository;
        private readonly IMapper _mapper;
        public GetDriverInsuranceByIdQueryHandler(IDriverInsuranceRepositoryAsync driverInsuranceRepository, IMapper mapper)
        {
            _driverInsuranceRepository = driverInsuranceRepository;
            _mapper = mapper;
        }
        public async Task<Response<DriverInsuranceViewModel>> Handle(GetDriverInsuranceByIdQuery request, CancellationToken cancellationToken)
        {
            var driverObject = (await _driverInsuranceRepository.FindByCondition(x => x.Id.Equals(request.Id)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            return new Response<DriverInsuranceViewModel>(_mapper.Map<DriverInsuranceViewModel>(driverObject));
        }
    }
}
