﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.DriverInsurance;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.DriverInsurances.Queries
{
    public class GetAllDriverInsuranceQuery : IRequest<Response<IEnumerable<DriverInsuranceViewModel>>>
    {

    }
    public class GetAllDriverInsuranceQueryHandler : IRequestHandler<GetAllDriverInsuranceQuery, Response<IEnumerable<DriverInsuranceViewModel>>>
    {
        private readonly IDriverInsuranceRepositoryAsync _driverInsuranceRepository;
        private readonly IMapper _mapper;
        public GetAllDriverInsuranceQueryHandler(IDriverInsuranceRepositoryAsync driverInsuranceRepository, IMapper mapper)
        {
            _driverInsuranceRepository = driverInsuranceRepository;
            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<DriverInsuranceViewModel>>> Handle(GetAllDriverInsuranceQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var validFilter = _mapper.Map<GetAllDriverInsuranceQuery>(request);
                var driver = await _driverInsuranceRepository.GetAllAsync();
                var driverInsuranceViewModel = _mapper.Map<IEnumerable<DriverInsuranceViewModel>>(driver);
                return new Response<IEnumerable<DriverInsuranceViewModel>>(driverInsuranceViewModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
