﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.DriverInsurance;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using iTrans.Transportation.Domain.Entities;
using MediatR;

namespace iTrans.Transportation.Application.Features.DriverInsurances.Queries
{
    public class GetAllDriverInsuranceParameter : IRequest<PagedResponse<IEnumerable<DriverInsuranceViewModel>>>
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
    }
    public class GetAllDriverInsuranceParameterHandler : IRequestHandler<GetAllDriverInsuranceParameter, PagedResponse<IEnumerable<DriverInsuranceViewModel>>>
    {
        private readonly IDriverInsuranceRepositoryAsync _driverInsuranceRepository;
        private readonly IMapper _mapper;
        public GetAllDriverInsuranceParameterHandler(IDriverInsuranceRepositoryAsync driverInsuranceRepository, IMapper mapper)
        {
            _driverInsuranceRepository = driverInsuranceRepository;
            _mapper = mapper;
        }

        public async Task<PagedResponse<IEnumerable<DriverInsuranceViewModel>>> Handle(GetAllDriverInsuranceParameter request, CancellationToken cancellationToken)
        {
            var validFilter = _mapper.Map<GetAllDriverInsuranceParameter>(request);
            Expression<Func<DriverInsurance, bool>> expression = x => x.DriverId != null;
            var driverInsurance = await _driverInsuranceRepository.FindByConditionWithPage(expression, validFilter.PageNumber, validFilter.PageSize);
            var driverInsuranceViewModel = _mapper.Map<IEnumerable<DriverInsuranceViewModel>>(driverInsurance);
            return new PagedResponse<IEnumerable<DriverInsuranceViewModel>>(driverInsuranceViewModel, validFilter.PageNumber, validFilter.PageSize);
        }
    }
}