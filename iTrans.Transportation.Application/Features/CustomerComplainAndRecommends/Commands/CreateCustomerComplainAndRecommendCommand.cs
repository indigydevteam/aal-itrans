﻿using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Interfaces.Services;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using iTrans.Transportation.Domain.Entities;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.CustomerComplainAndRecommends.Commands
{
    public partial class CreateCustomerComplainAndRecommendCommand : IRequest<Response<int>>
    { 
        public Guid? UserId { get; set; }
        public Guid CustomerId { get; set; }
        public string Title { get; set; }
        public string Detail { get; set; }
        public List<IFormFile> Files { get; set; }
    }
    public class CreateCustomerComplainAndRecommendCommandHandler : IRequestHandler<CreateCustomerComplainAndRecommendCommand, Response<int>>
    {
        private readonly ICustomerRepositoryAsync _customerRepository;
        private readonly ICustomerComplainAndRecommendRepositoryAsync _customerComplainAndRecommendRepository;
        private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
        private readonly IConfiguration _configuration;

        private readonly IMapper _mapper;
        public CreateCustomerComplainAndRecommendCommandHandler(ICustomerRepositoryAsync customerRepository,ICustomerComplainAndRecommendRepositoryAsync customerComplainAndRecommendRepository
            , IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper,IConfiguration configuration)
        {
            _customerRepository = customerRepository;
            _customerComplainAndRecommendRepository = customerComplainAndRecommendRepository;
            _applicationLogRepository = applicationLogRepository;
            _mapper = mapper;
            _configuration = configuration;
        }

        public async Task<Response<int>> Handle(CreateCustomerComplainAndRecommendCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var customerObject = (await _customerRepository.FindByCondition(x => x.Id.Equals(request.CustomerId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (customerObject == null)
                {
                    throw new ApiException($"Customer Not Found.");
                }
                CustomerComplainAndRecommend CustomerComplainAndRecommend = new CustomerComplainAndRecommend
                {
                    Customer = customerObject,
                    Title = request.Title,
                    Detail = request.Detail,
                    Files = new List<CustomerComplainAndRecommendFile>()
                };

                if (request.Files != null)
                {
                    string currentTimeStr = DateTime.Now.ToString("yyyyMMddHHmmss");
                    var getContentPath = _configuration.GetSection("ContentPath");
                    var contentPath = getContentPath.Value;

                    string folderPath = contentPath + "customer/complainandrecommend/" + customerObject.Id.ToString()+ "/" + currentTimeStr;
                    if (!Directory.Exists(folderPath))
                    {
                        Directory.CreateDirectory(folderPath);
                    }
                    int fileCount = 0;
                    foreach (IFormFile file in request.Files)
                    {
                        fileCount = fileCount + 1;
                        string filePath = Path.Combine(folderPath, file.FileName);
                        using (FileStream fileStream = new FileStream(filePath, FileMode.Create))
                        {
                            await file.CopyToAsync(fileStream);
                            FileInfo fi = new FileInfo(filePath);
                            CustomerComplainAndRecommendFile CustomerComplainAndRecommendFile = new CustomerComplainAndRecommendFile
                            {
                                CustomerComplainAndRecommend = CustomerComplainAndRecommend,
                                FileName = file.FileName,
                                ContentType = file.ContentType,
                                FilePath = "customer/complainandrecommend/" + customerObject.Id.ToString() + "/" + currentTimeStr + "/" + file.FileName,
                                DirectoryPath = "customer/complainandrecommend/" + customerObject.Id.ToString() + "/" + currentTimeStr + "/",
                                FileEXT = fi.Extension,
                                Sequence = fileCount,
                            };
                            CustomerComplainAndRecommend.Files.Add(CustomerComplainAndRecommendFile);
                        }
                    }
                }

                CustomerComplainAndRecommend.Created = DateTime.UtcNow;
                CustomerComplainAndRecommend.Modified = DateTime.UtcNow;
                var CustomerComplainAndRecommendObject = await _customerComplainAndRecommendRepository.AddAsync(CustomerComplainAndRecommend);
                var log = new CreateAppLog(_applicationLogRepository);
                log.CreateLog("Customer", "Customer ComplainAndRecommend", "Create", Newtonsoft.Json.JsonConvert.SerializeObject(request),request.UserId.ToString());
                return new Response<int>(CustomerComplainAndRecommendObject.Id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
