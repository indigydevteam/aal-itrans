﻿using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Features.CustomerComplainAndRecommendes.Commands
{
    public class DeleteCustomerComplainAndRecommendeByIdCommand : IRequest<Response<int>>
    {
        public int Id { get; set; }
        public class DeleteCustomerComplainAndRecommendeByIdCommandHandler : IRequestHandler<DeleteCustomerComplainAndRecommendeByIdCommand, Response<int>>
        {
            private readonly ICustomerComplainAndRecommendRepositoryAsync _customerComplainAndRecommendeRepository;
            public DeleteCustomerComplainAndRecommendeByIdCommandHandler(ICustomerComplainAndRecommendRepositoryAsync customerComplainAndRecommendeRepository)
            {
                _customerComplainAndRecommendeRepository = customerComplainAndRecommendeRepository;
            }
            public async Task<Response<int>> Handle(DeleteCustomerComplainAndRecommendeByIdCommand command, CancellationToken cancellationToken)
            {
                var customerComplainAndRecommende = (await _customerComplainAndRecommendeRepository.FindByCondition(x => x.Id == command.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (customerComplainAndRecommende == null)
                {
                    throw new ApiException($"CustomerComplainAndRecommende Not Found.");
                }
                else
                {
                    await _customerComplainAndRecommendeRepository.DeleteAsync(customerComplainAndRecommende);
                    return new Response<int>(customerComplainAndRecommende.Id);
                }
            }
        }
    }
}
