﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.Exceptions;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using MediatR;

namespace iTrans.Transportation.Application.Features.CustomerComplainAndRecommends.Commands
{
    public class UpdateCustomerComplainAndRecommendCommand : IRequest<Response<int>>
    {
        public Guid? UserId { get; set; }
        public int Id { get; set; }
        public Guid CustomerId { get; set; }
        public string Title { get; set; }
        public string Detail { get; set; }

        public class UpdateCustomerComplainAndRecommendCommandHandler : IRequestHandler<UpdateCustomerComplainAndRecommendCommand, Response<int>>
        {
            private readonly ICustomerComplainAndRecommendRepositoryAsync _customerComplainAndRecommendRepository;
            private readonly IApplicationLogRepositoryAsync _applicationLogRepository;
            private readonly IMapper _mapper;

            public UpdateCustomerComplainAndRecommendCommandHandler(ICustomerComplainAndRecommendRepositoryAsync customerComplainAndRecommendRepository, IApplicationLogRepositoryAsync applicationLogRepository, IMapper mapper)
            {
                _customerComplainAndRecommendRepository = customerComplainAndRecommendRepository;
                _applicationLogRepository = applicationLogRepository;
                _mapper = mapper;
            }

            public async Task<Response<int>> Handle(UpdateCustomerComplainAndRecommendCommand request, CancellationToken cancellationToken)
            {
                var customerComplainAndRecommend = (await _customerComplainAndRecommendRepository.FindByCondition(x => x.Id == request.Id).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
                if (customerComplainAndRecommend == null)
                {
                    throw new ApiException($"CustomerComplainAndRecommend Not Found.");
                }
                else
                {
                    //customerComplainAndRecommend.CustomerId = request.CustomerId;
                    customerComplainAndRecommend.Title = request.Title;
                    customerComplainAndRecommend.Detail = request.Detail;
                    customerComplainAndRecommend.Modified = DateTime.UtcNow;
                    await _customerComplainAndRecommendRepository.UpdateAsync(customerComplainAndRecommend);
                    var log = new CreateAppLog(_applicationLogRepository);
                    log.CreateLog("Customer", "Customer ComplainAndRecommend", "Edit", Newtonsoft.Json.JsonConvert.SerializeObject(customerComplainAndRecommend),request.UserId.ToString());

                    return new Response<int>(customerComplainAndRecommend.Id);
                }
            }
        }
    }
}
