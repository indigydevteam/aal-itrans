﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.Customer.Backoffice;
using iTrans.Transportation.Application.DTOs.CustomerComplainAndRecommend.BackOffice;
using iTrans.Transportation.Application.DTOs.Driver.Backoffice;
using iTrans.Transportation.Application.DTOs.DriverComplainAndRecommend.backoffice;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using iTrans.Transportation.Domain.Entities;
using MediatR;

namespace iTrans.Transportation.Application.Features.CustomerComplainAndRecommends.backoffice
{
   public class GetAllComplainAndRecommend : IRequest<PagedResponse<IEnumerable<CustomerComplainAndRecommendViewModel>>>
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public string Search { set; get; }
        public int Active { set; get; }
        public string Column { set; get; }

    }
    public class GetAllComplainAndRecommendHandler : IRequestHandler<GetAllComplainAndRecommend, PagedResponse<IEnumerable<CustomerComplainAndRecommendViewModel>>>
    {
        private readonly ICustomerComplainAndRecommendRepositoryAsync _customerComplainAndRecommendRepository;
        private readonly IDriverComplainAndRecommendRepositoryAsync _driverrComplainAndRecommendRepository;

        private readonly IMapper _mapper;
        //private Expression<Func<CustomerComplainAndRecommend, bool>> expression;

        public GetAllComplainAndRecommendHandler(ICustomerComplainAndRecommendRepositoryAsync customerComplainAndRecommendRepository, IDriverComplainAndRecommendRepositoryAsync driverrComplainAndRecommendRepository, IMapper mapper)
        {
            _customerComplainAndRecommendRepository = customerComplainAndRecommendRepository;
            _driverrComplainAndRecommendRepository = driverrComplainAndRecommendRepository;
            _mapper = mapper;
        }

        public async Task<PagedResponse<IEnumerable<CustomerComplainAndRecommendViewModel>>> Handle(GetAllComplainAndRecommend request, CancellationToken cancellationToken)
        {
            var validFilter = _mapper.Map<GetAllComplainAndRecommend>(request);

            var customer = _mapper.Map<List<CustomerComplainAndRecommend>>((await _customerComplainAndRecommendRepository.FindByCondition(x => x.Customer.Name.Contains(request.Search)|| x.Title.Trim().Contains(request.Search)).ConfigureAwait(false)).AsQueryable().ToList());
            var driver = _mapper.Map<List<DriverComplainAndRecommend>>((await _driverrComplainAndRecommendRepository.FindByCondition(x => x.Driver.Name.Contains(request.Search) || x.Title.Contains(request.Search)).ConfigureAwait(false)).AsQueryable().ToList());

            var result = (from x in customer select new CustomerComplainAndRecommendViewModel
            { 
                id = x.Id, 
                customer = _mapper.Map<CustomerViewModel>(x.Customer), 
                name = x.Customer != null ? x.Customer.Name : "-",
                module ="customer", 
                title = x.Title != null ? x.Title : "-",
                detail = x.Detail != null ? x.Detail : "-",
                created = x.Created.ToString("yyyy/MM/dd HH:MM") != null ? x.Created.ToString("yyyy/MM/dd HH:MM") : "-",
            }).Concat(from d in driver select new CustomerComplainAndRecommendViewModel 
            {
                id = d.Id, 
                driver = _mapper.Map<DriverViewModel>(d.Driver), 
                name =d.Driver != null ? d.Driver.Name : "-",
                module = "driver", 
                title =d.Title != null ? d.Title : "-",
                detail = d.Detail != null ? d.Detail : "-",
                created = d.Created.ToString("yyyy/MM/dd HH:MM") != null ? d.Created.ToString("yyyy/MM/dd HH:MM") : "-",
            });

           
            if (validFilter.Search != null && validFilter.Search.Trim() != "")
            {
                result = result.Where(l => l.name.Contains(validFilter.Search.Trim()) || l.module.Contains(validFilter.Search.Trim())
                        || l.title.Contains(validFilter.Search.Trim()) || l.detail.Contains(validFilter.Search.Trim())
                        || l.created.Contains(validFilter.Search) || l.module.Contains(validFilter.Search)
                        || l.title.Contains(validFilter.Search) || l.detail.Contains(validFilter.Search)
                        || l.created.Contains(validFilter.Search) || l.name.Contains(validFilter.Search)).ToList();
            }
            if (request.Column == "name" && request.Active == 3)
            {
                result = result.OrderBy(x => x.name);
            }
            if (request.Column == "name" && request.Active == 2)
            {
                result = result.OrderByDescending(x => x.name);
            }
            if (request.Column == "module" && request.Active == 3)
            {
                result = result.OrderBy(x => x.module);
            }
            if (request.Column == "module" && request.Active == 2)
            {
                result = result.OrderByDescending(x => x.module);
            }
            if (request.Column == "title" && request.Active == 3)
            {
                result = result.OrderBy(x => x.title);
            }
            if (request.Column == "title" && request.Active == 2)
            {
                result = result.OrderByDescending(x => x.title);
            }
            if (request.Column == "detail" && request.Active == 3)
            {
                result = result.OrderBy(x => x.detail);
            }
            if (request.Column == "detail" && request.Active == 2)
            {
                result = result.OrderByDescending(x => x.detail);
            }
            if (request.Column == "created" && request.Active == 3)
            {
                result = result.OrderBy(x => x.created);
            }
            if (request.Column == "created" && request.Active == 2)
            {
                result = result.OrderByDescending(x => x.title);
            }
            if (request.Column == "created" && request.Active == 3)
            {
                result = result.OrderBy(x => x.created);
            }
           
            var itemCount = result.Count();
            result = result.Skip((validFilter.PageNumber - 1) * validFilter.PageSize).Take(validFilter.PageSize);

            return new PagedResponse<IEnumerable<CustomerComplainAndRecommendViewModel>>(result, validFilter.PageNumber, validFilter.PageSize, itemCount);
        }
    }
}