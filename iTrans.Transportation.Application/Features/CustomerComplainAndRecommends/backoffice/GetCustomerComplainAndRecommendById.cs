﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.CustomerComplainAndRecommend.BackOffice;
using iTrans.Transportation.Application.DTOs.DriverComplainAndRecommend.backoffice;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.CustomerComplainAndRecommends.backoffice
{
    public class GetCustomerComplainAndRecommendById : IRequest<Response<CustomerComplainAndRecommendViewModel>>
    {
        public int Id { get; set; }
        public string module { get; set; }
    }
    public class GetCustomerComplainAndRecommendByIdHandler : IRequestHandler<GetCustomerComplainAndRecommendById, Response<CustomerComplainAndRecommendViewModel>>
    {
        private readonly ICustomerComplainAndRecommendRepositoryAsync _customerComplainAndRecommendRepository;
        private readonly IDriverComplainAndRecommendRepositoryAsync _driverrComplainAndRecommendRepository;

        private readonly IMapper _mapper;
        public GetCustomerComplainAndRecommendByIdHandler(ICustomerComplainAndRecommendRepositoryAsync customerComplainAndRecommendRepository, IDriverComplainAndRecommendRepositoryAsync driverrComplainAndRecommendRepository, IMapper mapper)
        {
            _customerComplainAndRecommendRepository = customerComplainAndRecommendRepository;
            _driverrComplainAndRecommendRepository = driverrComplainAndRecommendRepository;
            _mapper = mapper;
        }
        public async Task<Response<CustomerComplainAndRecommendViewModel>> Handle(GetCustomerComplainAndRecommendById request, CancellationToken cancellationToken)
        {

            var customer = _mapper.Map<List<CustomerComplainAndRecommendViewModel>>((await _customerComplainAndRecommendRepository.GetAllAsync().ConfigureAwait(false)).AsQueryable().ToList());
            var driver = _mapper.Map<List<DriverComplainAndRecommendViewModel>>((await _driverrComplainAndRecommendRepository.GetAllAsync().ConfigureAwait(false)).AsQueryable().ToList());

            //var unionList = customer.Concat(driver).ToList();
            var query = (from x in customer select new CustomerComplainAndRecommendViewModel { id = x.id, customer = x.customer, name = x.customer.Name, module = "customer", title = x.title, detail = x.detail })

                       .Concat(from d in driver select new CustomerComplainAndRecommendViewModel { id = d.Id, driver = d.Driver, name = d.Driver.Name, module = "driver", title = d.Title, detail = d.Detail});


            return new Response<CustomerComplainAndRecommendViewModel>(_mapper.Map<CustomerComplainAndRecommendViewModel>(query.Where(x => x.id.Equals(request.Id)&&x.module.Equals(request.module)).FirstOrDefault()));
        }
    }
}
