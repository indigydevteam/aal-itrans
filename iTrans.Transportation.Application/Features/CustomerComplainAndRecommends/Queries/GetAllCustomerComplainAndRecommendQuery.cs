﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.CustomerComplainAndRecommend;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.CustomerComplainAndRecommends.Queries
{
    public class GetAllCustomerComplainAndRecommendQuery : IRequest<Response<IEnumerable<CustomerComplainAndRecommendViewModel>>>
    {

    }
    public class GetAllCustomerComplainAndRecommendQueryHandler : IRequestHandler<GetAllCustomerComplainAndRecommendQuery, Response<IEnumerable<CustomerComplainAndRecommendViewModel>>>
    {
        private readonly ICustomerComplainAndRecommendRepositoryAsync _customerComplainAndRecommendRepository;
        private readonly IMapper _mapper;
        public GetAllCustomerComplainAndRecommendQueryHandler(ICustomerComplainAndRecommendRepositoryAsync customerComplainAndRecommendRepository, IMapper mapper)
        {
            _customerComplainAndRecommendRepository = customerComplainAndRecommendRepository;
            _mapper = mapper;
        }

        public async Task<Response<IEnumerable<CustomerComplainAndRecommendViewModel>>> Handle(GetAllCustomerComplainAndRecommendQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var validFilter = _mapper.Map<GetAllCustomerComplainAndRecommendQuery>(request);
                var customer = await _customerComplainAndRecommendRepository.GetAllAsync();
                var customerComplainAndRecommendViewModel = _mapper.Map<IEnumerable<CustomerComplainAndRecommendViewModel>>(customer);
                return new Response<IEnumerable<CustomerComplainAndRecommendViewModel>>(customerComplainAndRecommendViewModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
