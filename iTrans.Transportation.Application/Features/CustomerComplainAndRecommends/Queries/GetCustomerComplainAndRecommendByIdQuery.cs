﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.CustomerComplainAndRecommend;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.CustomerComplainAndRecommends.Queries
{
    public class GetCustomerComplainAndRecommendByIdQuery : IRequest<Response<CustomerComplainAndRecommendViewModel>>
    {
        public int Id { get; set; }
    }
    public class GetCustomerComplainAndRecommendByIdQueryHandler : IRequestHandler<GetCustomerComplainAndRecommendByIdQuery, Response<CustomerComplainAndRecommendViewModel>>
    {
        private readonly ICustomerComplainAndRecommendRepositoryAsync _customerComplainAndRecommendRepository;
        private readonly IMapper _mapper;
        public GetCustomerComplainAndRecommendByIdQueryHandler(ICustomerComplainAndRecommendRepositoryAsync customerComplainAndRecommendRepository, IMapper mapper)
        {
            _customerComplainAndRecommendRepository = customerComplainAndRecommendRepository;
            _mapper = mapper;
        }
        public async Task<Response<CustomerComplainAndRecommendViewModel>> Handle(GetCustomerComplainAndRecommendByIdQuery request, CancellationToken cancellationToken)
        {
            var customerObject = (await _customerComplainAndRecommendRepository.FindByCondition(x => x.Id.Equals(request.Id)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            return new Response<CustomerComplainAndRecommendViewModel>(_mapper.Map<CustomerComplainAndRecommendViewModel>(customerObject));
        }
    }
}
