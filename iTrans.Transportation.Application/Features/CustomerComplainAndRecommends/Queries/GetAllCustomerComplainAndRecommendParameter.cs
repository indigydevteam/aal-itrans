﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.CustomerComplainAndRecommend.BackOffice;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using iTrans.Transportation.Domain;
using iTrans.Transportation.Domain.Entities;
using MediatR;

namespace iTrans.Transportation.Application.Features.CustomerComplainAndRecommends.Queries
{
    public class GetAllCustomerComplainAndRecommendParameter : IRequest<PagedResponse<IEnumerable<CustomerComplainAndRecommendViewModel>>>
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public string Search { set; get; }

    }
    public class GetAllCustomerComplainAndRecommendParameterHandler : IRequestHandler<GetAllCustomerComplainAndRecommendParameter, PagedResponse<IEnumerable<CustomerComplainAndRecommendViewModel>>>
    {
        private readonly ICustomerComplainAndRecommendRepositoryAsync _customerComplainAndRecommendRepository;
        private readonly IMapper _mapper;
        private Expression<Func<CustomerComplainAndRecommend, bool>> expression;

        public GetAllCustomerComplainAndRecommendParameterHandler(ICustomerComplainAndRecommendRepositoryAsync customerComplainAndRecommendRepository, IMapper mapper)
        {
            _customerComplainAndRecommendRepository = customerComplainAndRecommendRepository;
            _mapper = mapper;
        }

        public async Task<PagedResponse<IEnumerable<CustomerComplainAndRecommendViewModel>>> Handle(GetAllCustomerComplainAndRecommendParameter request, CancellationToken cancellationToken)
        {
            var validFilter = _mapper.Map<GetAllCustomerComplainAndRecommendParameter>(request);
            if (validFilter.Search != null && validFilter.Search.Trim() != "")
            {
                expression = x => (x.Title.Contains(validFilter.Search.Trim()));
            }
            int itemCount = _customerComplainAndRecommendRepository.GetItemCount(expression);

            //Expression<Func<Country, bool>> expression = x => x.Name_TH != "";
            var customerComplainAndRecommend = await _customerComplainAndRecommendRepository.FindByConditionWithPage(expression, validFilter.PageNumber, validFilter.PageSize);
            var customerComplainAndRecommendViewModel = _mapper.Map<IEnumerable<CustomerComplainAndRecommendViewModel>>(customerComplainAndRecommend);
            return new PagedResponse<IEnumerable<CustomerComplainAndRecommendViewModel>>(customerComplainAndRecommendViewModel, validFilter.PageNumber, validFilter.PageSize, itemCount);
        }
    }
}