﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using iTrans.Transportation.Application.DTOs.CustomerComplainAndRecommend;
using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Application.Wrappers;
using MediatR;

namespace iTrans.Transportation.Application.Features.CustomerComplainAndRecommends.Queries
{
    public class GetCustomerComplainAndRecommendByCustomerQuery : IRequest<Response<CustomerComplainAndRecommendViewModel>>
    {
        public Guid CustomerId { get; set; }
    }
    public class GetCustomerComplainAndRecommendByCustomerQueryHandler : IRequestHandler<GetCustomerComplainAndRecommendByCustomerQuery, Response<CustomerComplainAndRecommendViewModel>>
    {
        private readonly ICustomerComplainAndRecommendRepositoryAsync _customerComplainAndRecommendRepository;
        private readonly IMapper _mapper;
        public GetCustomerComplainAndRecommendByCustomerQueryHandler(ICustomerComplainAndRecommendRepositoryAsync customerComplainAndRecommendRepository, IMapper mapper)
        {
            _customerComplainAndRecommendRepository = customerComplainAndRecommendRepository;
            _mapper = mapper;
        }
        public async Task<Response<CustomerComplainAndRecommendViewModel>> Handle(GetCustomerComplainAndRecommendByCustomerQuery request, CancellationToken cancellationToken)
        {
            var customerObject = (await _customerComplainAndRecommendRepository.FindByCondition(x => x.Customer.Id.Equals(request.CustomerId)).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            return new Response<CustomerComplainAndRecommendViewModel>(_mapper.Map<CustomerComplainAndRecommendViewModel>(customerObject));
        }
    }
}
