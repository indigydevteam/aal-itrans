﻿using AutoMapper;
using iTrans.Transportation.Application.Enums;
using System;
using iTrans.Transportation.Domain;
using iTrans.Transportation.Application.Features.Customers.Commands;
using iTrans.Transportation.Application.Features.CustomerAddresses.Commands;
using iTrans.Transportation.Application.Features.CustomerProducts.Commands;
using iTrans.Transportation.Application.Features.CustomerFiles.Commands;
//using iTrans.Transportation.Application.Features.Titles.Commands;
using iTrans.Transportation.Application.Features.Titles.Backoffice.Commands;
using iTrans.Transportation.Application.Features.CorporateTypes.Backoffice.Commands;
//using iTrans.Transportation.Application.Features.Countries.Commands;
using iTrans.Transportation.Application.Features.CarTypes.Backoffice.Commands;
using iTrans.Transportation.Application.Features.CarLists.Commands;
using iTrans.Transportation.Application.Features.CarDescriptions.Commands;
using iTrans.Transportation.Application.Features.CarSpecifications.Backoffice.Commands;
using iTrans.Transportation.Application.Features.CarFeatures.Commands;
using iTrans.Transportation.Application.Features.ProductTypes.Backoffice.Commands;
using iTrans.Transportation.Application.Features.ProductPackagings.Backoffice.Commands;

using iTrans.Transportation.Application.Features.FAQs.Commands;
using iTrans.Transportation.Application.Features.ProblemTopics.Commands;

//using iTrans.Transportation.Application.Features.Provinces.Commands;
using iTrans.Transportation.Application.Features.Provinces.Backoffice.Commands;
//using iTrans.Transportation.Application.Features.Districts.Commands;
using iTrans.Transportation.Application.Features.Districts.Backoffice.Commands;
//using iTrans.Transportation.Application.Features.Subdistricts.Commands;
using iTrans.Transportation.Application.Features.Subdistricts.Backoffice.Commands;
using iTrans.Transportation.Application.Features.Drivers.Commands;
using iTrans.Transportation.Application.Features.DriverAddresses.Commands;
using iTrans.Transportation.Application.Features.DriverCars.Commands;
using iTrans.Transportation.Application.Features.DriverFiles.Commands;
using iTrans.Transportation.Application.Features.Regions.Backoffice.Commands;
using iTrans.Transportation.Application.Features.UserLevels.Commands;
using iTrans.Transportation.Domain.Entities;
using iTrans.Transportation.Application.Features.UserLevelConditions.Commands;
using iTrans.Transportation.Application.Features.DriverCarFiles.Commands;
using iTrans.Transportation.Application.Features.DriverCarLocations.Commands;
using iTrans.Transportation.Application.Features.RegisterDocuments.Commands;
using iTrans.Transportation.Application.Features.RegisterInformations.Backoffice.Commands;
using iTrans.Transportation.Application.Features.TermAndConditions.Commands;
using iTrans.Transportation.Application.Features.WelcomeInformations.Backoffice.Commands;
using iTrans.Transportation.Application.Features.Orderings.Commands;
using iTrans.Transportation.Application.Features.OrderingAddresses.Commands;
using iTrans.Transportation.Application.Features.OrderingCars.Commands;
using iTrans.Transportation.Application.Features.OrderingContainers.Commands;
//using iTrans.Transportation.Application.Features.OrderingContainerAddresses.Commands;
using iTrans.Transportation.Application.Features.OrderingDrivers.Commands;
using iTrans.Transportation.Application.Features.OrderingHistorys.Commands;
using iTrans.Transportation.Application.Features.OrderingInsurances.Commands;
//using iTrans.Transportation.Application.Features.OrderingPersons.Commands;
using iTrans.Transportation.Application.Features.OrderingProducts.Commands;
using iTrans.Transportation.Application.Features.DriverAnnouncements.Commands;
using iTrans.Transportation.Application.Features.DriverTermAndConditions.Commands;
using iTrans.Transportation.Application.Features.CustomerTermAndConditions.Commands;
using iTrans.Transportation.Application.Features.DriverInsurances.Commands;
using iTrans.Transportation.Application.Features.DriverComplainAndRecommends.Commands;
using iTrans.Transportation.Application.Features.CustomerComplainAndRecommends.Commands;
using iTrans.Transportation.Application.Features.CustomerProductFiles.Commands;
using iTrans.Transportation.Application.Features.DriverAnnouncementLocations.Commands;
using iTrans.Transportation.Application.Features.ApplicationLogs.Commands;
using iTrans.Transportation.Application.Features.OrderingProductFiles.Commands;
using iTrans.Transportation.Application.Features.OrderingCarFiles.Commands;
using iTrans.Transportation.Application.Features.CustomerFavoriteCars.Commands;
using iTrans.Transportation.Application.Features.CustomerPayments.Commands;
using iTrans.Transportation.Application.Features.CustomerPaymentHistorys.Commands;
using iTrans.Transportation.Application.Features.CustomerPaymentTransactions.Commands;
using iTrans.Transportation.Application.Features.DriverPayments.Commands;
using iTrans.Transportation.Application.Features.DriverPaymentHistories.Commands;
using iTrans.Transportation.Application.Features.DriverPaymentTransactions.Commands;
using iTrans.Transportation.Application.Features.OrderingCancelStatuses.Commands;
using iTrans.Transportation.Application.Features.CustomerPassWords.Commands;
using iTrans.Transportation.Application.Features.driverPassWords.Commands;
using iTrans.Transportation.Application.Features.OrderingAddressProducts.Commands;
//using iTrans.Transportation.Application.Features.OrderingCarDrivers.Commands;
using iTrans.Transportation.Application.Features.Stars.Commands;
//using iTrans.Transportation.Application.Features.OrderingCarDriverFlies.Commands;
using iTrans.Transportation.Application.Features.Countries.Backoffice.Commands;
using iTrans.Transportation.Application.Features.Notification.Command;
using iTrans.Transportation.Application.Features.NotificationUser.Command;
using iTrans.Transportation.Application.Features.News.Commands;
using iTrans.Transportation.Application.Features.ContainerTypes.Commands;
using iTrans.Transportation.Application.Features.ContainerSpecifications.Commands;
using iTrans.Transportation.Application.Features.EnergySavingDevices.Backoffice.Commands;
using iTrans.Transportation.Application.Features.Users.Commands;
using iTrans.Transportation.Application.Features.CustomerProblems.Commands;
using iTrans.Transportation.Application.Features.CustomerProblemFiles.Commands;
using iTrans.Transportation.Application.Features.FAQs.backoffice;
using iTrans.Transportation.Application.Features.DriverCars.Backoffice;
using iTrans.Transportation.Application.Features.Drivers.Backoffice.Commands;
using iTrans.Transportation.Application.Features.Customers.Backoffice.Commands;
using iTrans.Transportation.Application.Features.Role.Commands;
using iTrans.Transportation.Application.DTOs.DummyWallet;
using iTrans.Transportation.Application.DTOs.Role;
using iTrans.Transportation.Application.Features.TermAndConditions.Backoffice;
using iTrans.Transportation.Application.Features.OrderingAddressHistories.Commands;
using iTrans.Transportation.Application.Features.Users.Backoffice;
using iTrans.Transportation.Application.Features.QRCodes.Commands;
using iTrans.Transportation.Application.Features.CustomerLevelCharacteristics.Commands;
using iTrans.Transportation.Application.Features.DriverLevelCharacteristics.Commands;
using iTrans.Transportation.Application.Features.TermAndConditions.Backoffice.Commands;
using iTrans.Transportation.Application.DTOs.CountryFile;

//using iTrans.Transportation.Application.Features.DriverProblems.Commands;
//using iTrans.Transportation.Application.Features.DriverProblemFiles.Commands;

namespace iTrans.Transportation.Application
{
    public class AutomapperProfile : Profile
    {
        public AutomapperProfile()
        {
            CreateMap<CreateCustomerCommand, Customer>();
            CreateMap<UpdateCustomerCommand, Customer>();
            CreateMap<UpdateCustomerStarByIdCommand, Customer>();
            CreateMap<UpdateCustomerPasswordByIdCommand, Customer>();
            CreateMap<Customer, DTOs.Customer.CustomerViewModel>().ReverseMap();
            CreateMap<Customer, DTOs.Customer.Backoffice.CustomerViewModel>().ReverseMap();

            CreateMap<Customer, DTOs.Customer.CustomerWithAddressViewModel>()
                .ForMember(dest => dest.addresses, act => act.MapFrom(src => src.Addresses));

            CreateMap<Customer, DTOs.Customer.CustomerInformationViewModel>()
                .ForMember(dest => dest.addresses, act => act.MapFrom(src => src.Addresses))
                .ForMember(dest => dest.files, act => act.MapFrom(src => src.Files))
                .ForMember(opts => opts.termAndConditions, act => act.Ignore());
                //.ForSourceMember(x => x.TermAndConditions, y => y.DoNotValidate());

            CreateMap<CreateCustomerAddressCommand, CustomerAddress>();
            CreateMap<UpdateCustomerAddressCommand, CustomerAddress>();
            CreateMap<UpdateCustomerTaxById, CustomerAddress>();
            CreateMap<CustomerAddress, DTOs.CustomerAddress.CustomerAddressViewModel>()
                .ForMember(dest => dest.Road, act => act.MapFrom(src => src.Road == null ? "" : src.Road))
                .ForMember(dest => dest.Alley, act => act.MapFrom(src => src.Alley == null ? "" : src.Alley))
                .ReverseMap();
            CreateMap<CustomerAddress, DTOs.CustomerAddress.ProfileAddressViewModel>()
                .ForMember(dest => dest.Road, act => act.MapFrom(src => src.Road == null ? "" : src.Road))
                .ForMember(dest => dest.Alley, act => act.MapFrom(src => src.Alley == null ? "" : src.Alley))
                .ForMember(dest => dest.AddressType, act => act.Condition(src => (src.AddressType.Equals("register"))));

            CreateMap<CreateCustomerFileCommand, CustomerFile>();
            CreateMap<UpdateCustomerFileCommand, CustomerFile>();
            CreateMap<CustomerFile, DTOs.CustomerFile.CustomerFileViewModel>().ReverseMap();


            CreateMap<CreateCustomerProductCommand, CustomerProduct>();
            CreateMap<UpdateCustomerProductCommand, CustomerProduct>();
            CreateMap<CreateProductCommand, CustomerProduct>();
            CreateMap<CustomerProduct, DTOs.CustomerProduct.CustomerProductViewModel>().ReverseMap()
                .ForMember(dest => dest.ProductFiles, act => act.MapFrom(src => src.ProductFiles));

            CreateMap<CreateCustomerProductFileCommand, CustomerProductFile>();
            CreateMap<UpdateCustomerProductFileCommand, CustomerProductFile>();
            CreateMap<CustomerProductFile, DTOs.CustomerProductFile.CustomerProductFileViewModel>().ReverseMap();

            CreateMap<CreateTitleCommand, Title>();
            CreateMap<UpdateTitleCommand, Title>();
            CreateMap<Title, DTOs.Title.TitleViewModel>().ReverseMap();
            CreateMap<Title, DTOs.Backoffice.Title.TitleViewModel>();

            CreateMap<CreateCorporateTypeCommand, CorporateType>();
            CreateMap<UpdateCorporateTypeCommand, CorporateType>();
            CreateMap<CorporateType, DTOs.CorporateType.CorporateTypeViewModel>().ReverseMap();

            CreateMap<CreateCountryCommand, Country>();
            CreateMap<UpdateCountryCommand, Country>();
            CreateMap<Country, DTOs.Country.CountryViewModel>().ReverseMap();
            CreateMap<Country, DTOs.Country.Backoffice.CountryViewModel>().ReverseMap();

            CreateMap<CreateRegionCommand, Region>();
            CreateMap<UpdateRegionCommand, Region>();
            CreateMap<Region, DTOs.Region.RegionViewModel>().ReverseMap();
            CreateMap<Region, DTOs.Region.Backoffice.RegionViewModel>()
                .ForMember(dest => dest.CountryName, act => act.MapFrom(src => src.Country.Name_TH));

            CreateMap<CreateProvinceCommand, Province>();
            CreateMap<UpdateProvinceCommand, Province>();
            CreateMap<Province, DTOs.Province.ProvinceViewModel>().ReverseMap();
            CreateMap<Province, DTOs.Province.Backoffice.ProvinceViewModel>()
               .ForMember(dest => dest.CountryName, act => act.MapFrom(src => src.Country.Name_TH))
               .ForMember(dest => dest.RegionName, act => act.MapFrom(src => src.Region.Name_TH));

            CreateMap<CreateDistrictCommand, District>();
            CreateMap<UpdateDistrictCommand, District>();
            CreateMap<District, DTOs.District.DistrictViewModel>().ReverseMap();
            CreateMap<District, DTOs.District.Backoffice.DistrictViewModel>()
                .ForMember(dest => dest.ProvinceName, act => act.MapFrom(src => src.Province.Name_TH));

            CreateMap<CreateSubdistrictCommand, Subdistrict>();
            CreateMap<UpdateSubdistrictCommand, Subdistrict>();
            CreateMap<Subdistrict, DTOs.Subdistrict.SubdistrictViewModel>().ReverseMap();
            CreateMap<Subdistrict, DTOs.Subdistrict.Backoffice.SubdistrictViewModel>()
                .ForMember(dest => dest.DistrictName, act => act.MapFrom(src => src.District.Name_TH));

            CreateMap<CreateCarTypeCommand, CarType>();
            CreateMap<UpdateCarTypeCommand, CarType>();


            CreateMap<CarType, DTOs.CarType.CarTypeViewModel>()
                .ForMember(dest => dest.File, act => act.MapFrom(src => src.File));

            CreateMap<CarType, DTOs.Backoffice.CarType.CarTypeViewModel>().ReverseMap();

            CreateMap<CreateCarListCommand, CarList>();
            CreateMap<UpdateCarListCommand, CarList>();
            //CreateMap<CarList, DTOs.CarList.CarListViewModel>().ReverseMap();
            CreateMap<CarList, DTOs.CarList.CarListViewModel>().ReverseMap();
            CreateMap<CarList, DTOs.CarList.Backoffice.CarListViewModel>().ReverseMap();

            CreateMap<CreateCarDescriptionCommand, CarDescription>();
            CreateMap<UpdateCarDescriptionCommand, CarDescription>();
            CreateMap<CarDescription, DTOs.CarDescription.CarDescriptionViewModel>().ReverseMap();
            CreateMap<CarDescription, DTOs.CarDescription.Backoffice.CarDescriptionViewModel>()
                .ForMember(dest => dest.carType, act => act.MapFrom(src => src.CarList.CarType))
                .ForMember(dest => dest.carTypeId, act => act.MapFrom(src => src.CarList.CarType.Id))
                .ForMember(dest => dest.carListId, act => act.MapFrom(src => src.CarList.Id));

            CreateMap<CreateCarSpecificationCommand, CarSpecification>();
            CreateMap<UpdateCarSpecificationCommand, CarSpecification>();
            CreateMap<CarSpecification, DTOs.CarSpecification.CarSpecificationViewModel>().ReverseMap();
            CreateMap<CarSpecification, DTOs.Backoffice.CarSpecification.CarSpecificationViewModel>().ReverseMap();
            //.ForMember(dest => dest.carType, act => act.MapFrom(src => src.CarList.CarType))
            //.ForMember(dest => dest.carTypeId, act => act.MapFrom(src => src.CarList.CarType.Id))
            //.ForMember(dest => dest.carListId, act => act.MapFrom(src => src.CarList.Id));

            CreateMap<CreateCarFeatureCommand, CarFeature>();
            CreateMap<UpdateCarFeatureCommand, CarFeature>();
            CreateMap<CarFeature, DTOs.CarFeature.CarFeatureViewModel>().ReverseMap();
            CreateMap<CarFeature, DTOs.CarFeature.Backoffice.CarFeatureViewModel>()
                .ForMember(dest => dest.carType, act => act.MapFrom(src => src.CarList.CarType))
                .ForMember(dest => dest.carTypeId, act => act.MapFrom(src => src.CarList.CarType.Id))
                .ForMember(dest => dest.carListId, act => act.MapFrom(src => src.CarList.Id));

            CreateMap<CreateProductTypeCommand, ProductType>();
            CreateMap<UpdateProductTypeCommand, ProductType>();
            CreateMap<ProductType, DTOs.ProductType.ProductTypeViewModel>()
                .ForMember(dest => dest.File, act => act.MapFrom(src => src.File));


            CreateMap<CreateProductPackagingCommand, ProductPackaging>();
            CreateMap<UpdateProductPackagingCommand, ProductPackaging>();
            CreateMap<ProductPackaging, DTOs.ProductPackaging.ProductPackagingViewModel>().ReverseMap();
            CreateMap<ProductPackaging, DTOs.Backoffice.ProductPackaging.ProductPackagingViewModel>().ReverseMap();

            CreateMap<CreateFAQCommand, FAQ>();
            CreateMap<CreateFAQ, FAQ>();
            CreateMap<UpdateFAQCommand, FAQ>();
            CreateMap<FAQ, DTOs.FAQ.FAQViewModel>().ReverseMap();
            CreateMap<FAQ, DTOs.FAQ.FAQViewModelFormCreate>().ReverseMap();

            CreateMap<CreateProblemTopicCommand, ProblemTopic>();
            CreateMap<UpdateProblemTopicCommand, ProblemTopic>();
            CreateMap<ProblemTopic, DTOs.ProblemTopic.ProblemTopicViewModel>().ReverseMap();

            CreateMap<CreateDriverCommand, Driver>();
            CreateMap<UpdateDriverCommand, Driver>();
            CreateMap<UpdatedriverPasswordByIdCommand, Driver>();
            CreateMap<UpdateDriverStarBIdCommand, Driver>();
            CreateMap<Driver, DTOs.Driver.DriverViewModel>().ReverseMap();
            CreateMap<Driver, DTOs.Driver.DriverViewModel>().ReverseMap();
            CreateMap<Driver, DTOs.Driver.Backoffice.DriverViewModel>();

            CreateMap<Driver, DTOs.Driver.DriverProfileViewModel>()
                .ForMember(dest => dest.files, act => act.MapFrom(src => src.Files));
            CreateMap<Driver, DTOs.Driver.DriverInformationViewModel>()
                .ForMember(dest => dest.title, act => act.MapFrom(src => src.Title))
                .ForMember(dest => dest.contactPersonTitle, act => act.MapFrom(src => src.ContactPersonTitle))
                .ForMember(dest => dest.addresses, act => act.MapFrom(src => src.Addresses))
                .ForMember(dest => dest.files, act => act.MapFrom(src => src.Files));
            CreateMap<Driver, DTOs.Driver.DriverCorparateInformationViewModel>()
                .ForMember(dest => dest.contactPersonTitle, act => act.MapFrom(src => src.ContactPersonTitle))
                .ForMember(dest => dest.files, act => act.MapFrom(src => src.Files));

            CreateMap<CreateDriverAddressCommand, DriverAddress>();
            CreateMap<UpdateDriverAddressCommand, DriverAddress>();
            CreateMap<UpdataDriverAddressTaxById, DriverAddress>();
            CreateMap<DriverAddress, DTOs.DriverAddress.DriverAddressViewModel>()
                 .ForMember(dest => dest.road, act => act.MapFrom(src => src.Road == null ? "" : src.Road))
                .ForMember(dest => dest.alley, act => act.MapFrom(src => src.Alley == null ? "" : src.Alley)).ReverseMap();
            CreateMap<DriverAddress, DTOs.DriverAddress.ProfileAddressViewModel>()
                 .ForMember(dest => dest.Road, act => act.MapFrom(src => src.Road == null ? "" : src.Road))
                .ForMember(dest => dest.Alley, act => act.MapFrom(src => src.Alley == null ? "" : src.Alley))
                .ForMember(dest => dest.AddressType, act => act.Condition(src => (src.AddressType.Equals("register"))));

            CreateMap<CreateDriverCarCommand, DriverCar>();
            CreateMap<CreateDriverCarWithLocationCommand, DriverCar>();
            CreateMap<CreateCarInformationCommand, DriverCar>();
            CreateMap<UpdateDriverCarCommand, DriverCar>();
            CreateMap<UpdateDriverCarById, DriverCar>();
            CreateMap<DriverCar, DTOs.DriverCar.DriverCarViewModel>().ReverseMap();
            CreateMap<DriverCar, DTOs.DriverCar.backoffice.DriverCarViewModel>().ReverseMap();
            CreateMap<DriverCar, DTOs.DriverCar.DriverCarWithFilesViewModel>()
                .ForMember(dest => dest.carFiles, act => act.MapFrom(src => src.CarFiles));
            CreateMap<DriverCar, DTOs.DriverCar.DriverCarInformationViewModel>()
              .ForMember(dest => dest.carFiles, act => act.MapFrom(src => src.CarFiles))
              .ForMember(dest => dest.locations, act => act.MapFrom(src => src.Locations));

            CreateMap<CreateDriverCarLocationCommand, DriverCarLocation>();
            CreateMap<UpdateDriverCarLocationCommand, DriverCarLocation>();
            CreateMap<DriverCarLocation, DTOs.DriverCarLocation.DriverCarLocationViewModel>().ReverseMap();
            CreateMap<DriverCarLocation, DTOs.DriverCarLocation.Backoffice.DriverCarLocationViewModel>().ReverseMap();

            CreateMap<CreateDriverFileCommand, DriverFile>();
            CreateMap<UpdateDriverFileCommand, DriverFile>();
            CreateMap<DriverFile, DTOs.DriverFile.DriverFileViewModel>().ReverseMap();


            CreateMap<CreateDriverCarFileCommand, DriverCarFile>();
            CreateMap<UpdateDriverCarFileCommand, DriverCarFile>();
            CreateMap<DriverCarFile, DTOs.DriverCarFile.DriverCarFileViewModel>().ReverseMap();


            CreateMap<News, DTOs.News.NewsViewModel>().ReverseMap();
            CreateMap<News, CreateNewsCommand>().ReverseMap();
            CreateMap<News, UpdateNewsCommand>().ReverseMap();

            CreateMap<CreateUserLevelCommand, UserLevel>();
            CreateMap<UpdateUserLevelCommand, UserLevel>();
            CreateMap<UserLevel, DTOs.UserLevel.UserLevelViewModel>().ReverseMap();
            CreateMap<UserLevel, DTOs.UserLevel.UserLevelInformationViewModel>()
                .ForMember(dest => dest.conditions, act => act.MapFrom(src => src.Conditions));
            CreateMap<UserLevel, DTOs.Backoffice.UserLevel.UserLevelInformationViewModel>()
                .ForMember(dest => dest.conditions, act => act.MapFrom(src => src.Conditions));

            CreateMap<CreateUserLevelConditionCommand, UserLevelCondition>();
            CreateMap<UpdateUserLevelConditionCommand, UserLevelCondition>();
            CreateMap<UserLevelCondition, DTOs.UserLevelCondition.UserLevelConditionViewModel>().ReverseMap();

            CreateMap<CreateRegisterDocumentCommand, RegisterDocument>();
            CreateMap<UpdateRegisterDocumentCommand, RegisterDocument>();
            CreateMap<RegisterDocument, DTOs.Register_Document.RegisterDocumentViweModel>().ReverseMap();

            CreateMap<CreateRegisterInformationCommand, RegisterInformation>();
            CreateMap<UpdateRegisterinformationCommand, RegisterInformation>();
            CreateMap<RegisterInformation, DTOs.RegisterInformation.RegisterInformationViewModel>().ReverseMap();


            CreateMap<CreateTermAndConditionCommand, TermAndCondition>();
            CreateMap<CreateTermAndCondition, TermAndCondition>();
            CreateMap<UpdatePubilcTermAndCondition, TermAndCondition>();
            CreateMap<UpdateTermAndConditionCommand, TermAndCondition>();
            CreateMap<TermAndCondition, DTOs.TermAndCondition.TermAndConditionViewModel>().ReverseMap();
            CreateMap<TermAndCondition, DTOs.TermAndCondition.UserTermAndConditionViewModel>().ReverseMap();

            CreateMap<CreateWelcomeInformationCommand, WelcomeInformation>();
            CreateMap<UpdateWelcomeInformationCommand, WelcomeInformation>();
            CreateMap<WelcomeInformation, DTOs.WelcomeInformation.WelcomeInformationViewModel>().ReverseMap();
            CreateMap<WelcomeInformation, DTOs.Backoffice.WelcomeInformation.WelcomeInformationViewModel>().ReverseMap();

            CreateMap<CreateOrderingCommand, Ordering>();
            CreateMap<UpdateOrderingCommand, Ordering>();
            CreateMap<DriverAcceptCommand, Ordering>();
            CreateMap<Ordering, DTOs.Ordering.OrderingViewModel>()
            .ForMember(dest => dest.orderNumber, act => act.MapFrom(src => src.TrackingCode))
            .ForMember(dest => dest.drivers, act => act.MapFrom(src => src.Drivers))
            .ForMember(dest => dest.products, act => act.MapFrom(src => src.Products))
            .ForMember(dest => dest.cars, act => act.MapFrom(src => src.Cars))
            .ForMember(dest => dest.addresses, act => act.MapFrom(src => src.Addresses))
            .ForMember(dest => dest.containers, act => act.MapFrom(src => src.Containers))
            .ForMember(dest => dest.insurances, act => act.MapFrom(src => src.Insurances));
            CreateMap<Ordering, DTOs.Ordering.OrderingWithDriverViewModel>()
                .ForMember(dest => dest.orderNumber, act => act.MapFrom(src => src.TrackingCode))
                .ForMember(dest => dest.drivers, act => act.MapFrom(src => src.Drivers))
                .ForMember(dest => dest.products, act => act.MapFrom(src => src.Products))
                .ForMember(dest => dest.cars, act => act.MapFrom(src => src.Cars))
                .ForMember(dest => dest.addresses, act => act.MapFrom(src => src.Addresses))
                .ForMember(dest => dest.containers, act => act.MapFrom(src => src.Containers))
                .ForMember(dest => dest.insurances, act => act.MapFrom(src => src.Insurances));
            CreateMap<Ordering, DTOs.Ordering.OrderingWithCustomerViewModel>()
                .ForMember(dest => dest.orderNumber, act => act.MapFrom(src => src.TrackingCode))
                .ForMember(dest => dest.customer, act => act.MapFrom(src => src.Customer))
                .ForMember(dest => dest.products, act => act.MapFrom(src => src.Products))
                .ForMember(dest => dest.insurances, act => act.MapFrom(src => src.Insurances))
                .ForMember(dest => dest.cars, act => act.MapFrom(src => src.Cars))
                .ForMember(dest => dest.addresses, act => act.MapFrom(src => src.Addresses))
                .ForMember(dest => dest.containers, act => act.MapFrom(src => src.Containers));
            CreateMap<Ordering, DTOs.Ordering.Backoffice.OrderingViewModel>()
                .ForMember(dest => dest.orderNumber, act => act.MapFrom(src => src.TrackingCode))
               .ForMember(dest => dest.drivers, act => act.MapFrom(src => src.Drivers))
               .ForMember(dest => dest.products, act => act.MapFrom(src => src.Products))
               .ForMember(dest => dest.cars, act => act.MapFrom(src => src.Cars))
               .ForMember(dest => dest.addresses, act => act.MapFrom(src => src.Addresses))
               .ForMember(dest => dest.containers, act => act.MapFrom(src => src.Containers))
               .ForMember(dest => dest.insurances, act => act.MapFrom(src => src.Insurances));






            CreateMap<CreateOrderingAddressCommand, OrderingAddress>();
            CreateMap<UpdateOrderingAddressCommand, OrderingAddress>();
            CreateMap<OrderingAddress, DTOs.OrderingAddress.OrderingAddressViewModel>()
                .ForMember(dest => dest.orderingId, act => act.MapFrom(src => src.Ordering.Id))
                .ForMember(dest => dest.products, act => act.MapFrom(src => src.AddressProducts));
            CreateMap<DTOs.OrderingAddress.OrderingAddressViewModel, OrderingAddress>()
                .ForMember(dest => dest.AddressProducts, act => act.MapFrom(src => src.products));



            CreateMap<OrderingAddress, DTOs.OrderingAddress.BackOffice.OrderingAddressViewModel>()
                .ForMember(dest => dest.orderingId, act => act.MapFrom(src => src.Ordering.Id))
                .ForMember(dest => dest.products, act => act.MapFrom(src => src.AddressProducts));
            CreateMap<DTOs.OrderingAddress.BackOffice.OrderingAddressViewModel, OrderingAddress>()
               .ForMember(dest => dest.AddressProducts, act => act.MapFrom(src => src.products));



            CreateMap<OrderingAddress, DTOs.OrderingAddress.CreateAddressViewModel>().ReverseMap();
            CreateMap<OrderingAddress, DTOs.OrderingAddress.UpdateAddressViewModel>().ReverseMap();
            CreateMap<OrderingAddress, DTOs.OrderingAddress.AddressTrackingCodeViewModel>().ReverseMap();
            CreateMap<OrderingAddress, DTOs.OrderingAddress.OrderingCompensationViewModel>()
               .ForMember(dest => dest.orderingId, act => act.MapFrom(src => src.Ordering.Id));
            CreateMap<OrderingAddress, DTOs.OrderingAddress.OrderingAddressNotificationViewModel>().ReverseMap();




            CreateMap<OrderingAddressFile, DTOs.OrderingAddressFile.OrderingAddressFileViewModel>().ReverseMap();

            CreateMap<CreateOrderingAddressProductCommand, OrderingAddressProduct>();
            CreateMap<OrderingAddressProduct, DTOs.OrderingAddressProduct.OrderingAddressProductViewModel>()
                .ForMember(dest => dest.productFiles, act => act.MapFrom(src => src.OrderingAddressProductFiles))
                .ForMember(dest => dest.OrderingProductId, act => act.MapFrom(src => src.OrderingProduct.Id))
                .ForMember(dest => dest.OrderingContainerId, act => act.MapFrom(src => src.OrderingContainer.Id));
            CreateMap<OrderingAddressProduct, DTOs.OrderingAddressProduct.CreateAddressProductViewModel>().ReverseMap();
            CreateMap<OrderingAddressProduct, DTOs.OrderingAddressProduct.UpdateAddressProductViewModel>().ReverseMap();
            CreateMap<OrderingAddressProduct, DTOs.OrderingProduct.CreateProductViewModel>().ReverseMap();
            CreateMap<OrderingAddressProduct, DTOs.OrderingProduct.UpdateProductViewModel>().ReverseMap();
            CreateMap<OrderingAddressProduct, OrderingProduct>().ReverseMap();
            CreateMap<OrderingAddressProduct, DTOs.OrderingAddressProduct.ProductTrackingCodeViewModel>().ReverseMap();

            CreateMap<OrderingAddressProductFile, DTOs.OrderingAddressProductFile.OrderingAddressProductFileViewModel>().ReverseMap();

            CreateMap<CreateOrderingCarCommand, OrderingCar>();
            CreateMap<UpdateOrderingCarCommand, OrderingCar>();
            CreateMap<OrderingCar, DTOs.OrderingCar.OrderingCarViewModel>()
                .ForMember(dest => dest.driverId, act => act.MapFrom(src => src.OrderingDriver.DriverId));
            CreateMap<OrderingCar, DTOs.OrderingCar.CreateCarViweModel>().ReverseMap();
            CreateMap<OrderingCar, DTOs.OrderingCar.UpdateCarViweModel>().ReverseMap();

            //CreateMap<CreateOrderingCarDriverCommand, OrderingCarDriver>();
            //CreateMap<UpdateOrderingCarDriverCommand, OrderingCarDriver>();
            //CreateMap<OrderingCarDriver, DTOs.OrderingCarDriver.OrderingCarDriverViewModel>().ReverseMap();
            //CreateMap<OrderingCarDriver, DTOs.OrderingCarDriver.CreateDriverViewModel>().ReverseMap();

            CreateMap<CreateOrderingCarFileCommand, OrderingCarFile>();
            CreateMap<UpdateOrderingCarFileCommand, OrderingCarFile>();
            CreateMap<OrderingCarFile, DTOs.OrderingCarFile.OrderingCarFileViewModel>().ReverseMap();

            CreateMap<CreateOrderingContainerCommand, OrderingContainer>();
            CreateMap<UpdateOrderingContainerByIdCommand, OrderingContainer>();
            CreateMap<OrderingContainer, DTOs.OrderingContainer.OrderingContainerViewModel>();
            //.ForMember(dest => dest.Addresses, act => act.MapFrom(src => src.Addresses));
            CreateMap<OrderingContainer, DTOs.OrderingContainer.CreateContainerViewModel>().ReverseMap();
            CreateMap<OrderingContainer, DTOs.OrderingContainer.UpdateContainerViewModel>().ReverseMap();

            //CreateMap<CreateOrderingContainerAddressCommand, OrderingContainerAddress>();
            //CreateMap<UpdateOrderingContainerAddressCommand, OrderingContainerAddress>();
            //CreateMap<OrderingContainerAddress, DTOs.OrderingContainerAddress.OrderingContainerAddressViewModel>().ReverseMap();
            //CreateMap<OrderingContainerAddress, DTOs.OrderingContainerAddress.CreateContainerAddressViewModel>().ReverseMap();

            CreateMap<CreateOrderingDriverCommand, OrderingDriver>();
            CreateMap<UpdateOrderingDriverCommand, OrderingDriver>();
            CreateMap<OrderingDriver, DTOs.OrderingDriver.OrderingDriverViewModel>().ReverseMap();
            CreateMap<Driver, DTOs.OrderingDriver.OrderingDriverViewModel>()
                .ForMember(dest => dest.id, act => act.Ignore())
                .ForMember(dest => dest.driverId, act => act.MapFrom(src => src.Id))
                .ForMember(dest => dest.driverFiles, act => act.MapFrom(src => src.Files));

            CreateMap<OrderingDriverFile, DTOs.OrderingDriverFile.OrderingDriverFileViewModel>().ReverseMap();
            CreateMap<DriverFile, DTOs.OrderingDriverFile.OrderingDriverFileViewModel>().ReverseMap();

            CreateMap<CreateOrderingHistoryCommand, OrderingHistory>();
            CreateMap<UpdateOrderingHistoryCommand, OrderingHistory>();
            CreateMap<OrderingHistory, DTOs.OrderingHistory.OrderingHistoryViewModel>()
            .ForMember(dest => dest.drivers, act => act.MapFrom(src => src.Drivers))
            .ForMember(dest => dest.products, act => act.MapFrom(src => src.Products))
            .ForMember(dest => dest.cars, act => act.MapFrom(src => src.Cars))
            .ForMember(dest => dest.addresses, act => act.MapFrom(src => src.Addresses))
            .ForMember(dest => dest.containers, act => act.MapFrom(src => src.Containers))
            .ForMember(dest => dest.insurances, act => act.MapFrom(src => src.Insurances));

            CreateMap<CreateOrderingInsuranceCommand, OrderingInsurance>();
            CreateMap<UpdateOrderingInsuranceCommand, OrderingInsurance>();
            CreateMap<OrderingInsurance, DTOs.OrderingInsurance.OrderingInsuranceViewModel>()
                .ForMember(dest => dest.orderingId, act => act.MapFrom(src => src.Ordering.Id));
            CreateMap<OrderingInsurance, DTOs.OrderingInsurance.CreateInsuranceViewModel>().ReverseMap();


            //CreateMap<CreateOrderingPersonCommand, OrderingPerson>();
            //CreateMap<UpdateOrderingPersonCommand, OrderingPerson>();
            //CreateMap<OrderingPerson, DTOs.OrderingPerson.OrderingPersonViewModel>().ReverseMap();

            CreateMap<CreateOrderingProductCommand, OrderingProduct>();
            CreateMap<UpdateOrderingProductCommand, OrderingProduct>();
            CreateMap<OrderingProduct, DTOs.OrderingProduct.OrderingProductViewModel>()
                .ForMember(dest => dest.files, act => act.MapFrom(src => src.ProductFiles));
            CreateMap<OrderingProduct, DTOs.OrderingProduct.CreateProductViewModel>().ReverseMap();
            CreateMap<OrderingProduct, DTOs.OrderingProduct.UpdateProductViewModel>().ReverseMap();
            CreateMap<OrderingProduct, DTOs.OrderingProduct.OrderingProductNotificationViewModel>()
                .ForMember(dest => dest.files, act => act.MapFrom(src => src.ProductFiles));

            CreateMap<CreateOrderingProductFileCommand, OrderingProductFile>();
            CreateMap<UpdateOrderingProductFileCommand, OrderingProductFile>();
            CreateMap<OrderingProductFile, DTOs.OrderingProductFile.OrderingProductFileViewModel>().ReverseMap();

            CreateMap<CreateDriverAnnouncementCommand, DriverAnnouncement>();
            CreateMap<CreateAnnouncementCommand, DriverAnnouncement>();
            CreateMap<UpdateDriverAnnouncementCommand, DriverAnnouncement>();
            CreateMap<DriverAnnouncement, DTOs.DriverAnnouncement.DriverAnnouncementViewModel>().ReverseMap();
            CreateMap<DriverAnnouncement, DTOs.DriverAnnouncement.AnnouncementWithDriverViewModel>().ReverseMap();

            CreateMap<CreateDriverAnnouncementLocationCommand, DriverAnnouncementLocation>();
            CreateMap<UpdateDriverAnnouncementLocationCommand, DriverAnnouncementLocation>();
            CreateMap<DriverAnnouncementLocation, DTOs.DriverAnnouncementLocation.DriverAnnouncementLocationViewModel>().ReverseMap();

            CreateMap<CreateDriverTermAndConditionCommand, DriverTermAndCondition>();
            CreateMap<UpdateDriverTermAndConditionCommand, DriverTermAndCondition>();
            CreateMap<DriverTermAndCondition, DTOs.DriverTermAndCondition.DriverTermAndConditionViewModel>().ReverseMap();

            CreateMap<CreateCustomerTermAndConditionCommand, CustomerTermAndCondition>();
            CreateMap<UpdateCustomerTermAndConditionCommand, CustomerTermAndCondition>();
            CreateMap<CustomerTermAndCondition, DTOs.CustomerTermAndCondition.CustomerTermAndConditionViewModel>().ReverseMap();



            CreateMap<CreateDriverInsuranceCommand, DriverInsurance>();
            CreateMap<UpdateDriverInsuranceCommand, DriverInsurance>();
            CreateMap<DriverInsurance, DTOs.DriverInsurance.DriverInsuranceViewModel>().ReverseMap();

            CreateMap<CreateDriverComplainAndRecommendCommand, DriverComplainAndRecommend>();
            CreateMap<UpdateDriverComplainAndRecommendCommand, DriverComplainAndRecommend>();
            CreateMap<DriverComplainAndRecommend, DTOs.DriverComplainAndRecommend.DriverComplainAndRecommendViewModel>().ReverseMap();
            CreateMap<DriverComplainAndRecommend, DTOs.DriverComplainAndRecommend.backoffice.DriverComplainAndRecommendViewModel>()
            .ForMember(dest => dest.Driver, act => act.MapFrom(src => src.Driver));

            CreateMap<CreateCustomerComplainAndRecommendCommand, CustomerComplainAndRecommend>();
            CreateMap<UpdateCustomerComplainAndRecommendCommand, CustomerComplainAndRecommend>();
            CreateMap<CustomerComplainAndRecommend, DTOs.CustomerComplainAndRecommend.CustomerComplainAndRecommendViewModel>().ReverseMap();
            CreateMap<CustomerComplainAndRecommendFile, DTOs.CustomerComplainAndRecommendFile.CustomerComplainAndRecommendFileViewModel>().ReverseMap();
            CreateMap<CustomerComplainAndRecommend, DTOs.CustomerComplainAndRecommend.BackOffice.CustomerComplainAndRecommendViewModel>()
            .ForMember(dest => dest.customer, act => act.MapFrom(src => src.Customer));

            CreateMap<CreateApplicationLogCommand, ApplicationLog>();
            CreateMap<UpdateApplicationLogCommand, ApplicationLog>();
            CreateMap<ApplicationLog, DTOs.ApplicationLog.ApplicationLogViewModel>().ReverseMap();
            CreateMap<ApplicationLog, DTOs.ApplicationLog.BackOffice.ApplicationLogViewModel>().ReverseMap();


            CreateMap<CreateCustomerFavoriteCarCommand, CustomerFavoriteCar>();
            CreateMap<UpdateCustomerFavoriteCarCommand, CustomerFavoriteCar>();
            CreateMap<CustomerFavoriteCar, DTOs.CustomerFavoriteCar.CustomerFavoriteCarViewModel>().ReverseMap();
            CreateMap<CustomerFavoriteCar, DTOs.CustomerFavoriteCar.CustomerFavoriteCarInfomationViewModel>().ReverseMap();

            //CreateMap<UserLevelCondition, DTOs.UserLevel.Condition>().ReverseMap();

            CreateMap<CreateCustomerPaymentCommand, CustomerPayment>();
            CreateMap<UpdateCustomerPaymentCommand, CustomerPayment>();
            CreateMap<CustomerPayment, DTOs.CustomerPayment.CustomerPaymentViewModel>().ReverseMap();

            CreateMap<CreateCustomerPaymentHistoryCommand, CustomerPaymentHistory>();
            CreateMap<UpdateCustomerPaymentHistoryCommand, CustomerPaymentHistory>();
            CreateMap<CustomerPaymentHistory, DTOs.CustomerPaymentHistory.CustomerPaymentHistoryViewModel>().ReverseMap();

            CreateMap<CreateCustomerPaymentTransactionCommand, CustomerPaymentTransaction>();
            CreateMap<UpdateCustomerPaymentTransactionCommand, CustomerPaymentTransaction>();
            CreateMap<CustomerPaymentTransaction, DTOs.CustomerPaymentTransaction.CustomerPaymentTransactionViewModel>().ReverseMap();

            CreateMap<CreateDriverPaymentCommand, DriverPayment>();
            CreateMap<UpdateDriverPaymentCommand, DriverPayment>();
            CreateMap<DriverPayment, DTOs.DriverPayment.DriverPaymentViewModel>().ReverseMap();

            CreateMap<CreateDriverPaymentHistoryCommand, DriverPaymentHistory>();
            CreateMap<UpdateDriverPaymentHistoryCommand, DriverPaymentHistory>();
            CreateMap<DriverPaymentHistory, DTOs.DriverPaymentHistory.DriverPaymentHistoryViewModel>().ReverseMap();

            CreateMap<CreateDriverPaymentTransactionCommand, DriverPaymentTransaction>();
            CreateMap<UpdateDriverPaymentTransactionCommand, DriverPaymentTransaction>();
            CreateMap<DriverPaymentTransaction, DTOs.DriverPaymentTransaction.DriverPaymentTransactionViewModel>().ReverseMap();

            CreateMap<CreateOrderingCancelStatusCommand, OrderingCancelStatus>();
            CreateMap<UpdateOrderingCancelStatusCommand, OrderingCancelStatus>();
            CreateMap<OrderingCancelStatus, DTOs.OrderingCancelStatus.OrderingCancelStatusViewModel>().ReverseMap();


            CreateMap<CreateNotificationCommand, Notification>();
            CreateMap<CreateNotificationUserCommand, NotificationUser>();

            //CreateMap<CreateOrderingCarDriverFileCommand, OrderingCarDriverFile>();
            //CreateMap<UpdateOrderingCarDriverFileCommand, OrderingCarDriverFile>();
            //CreateMap<OrderingCarDriverFile, DTOs.OrderingCarDriverFlie.OrderingCarDriverFileViewModel>().ReverseMap();

            CreateMap<CarList, DTOs.CarList.CarListInfomationViewModel>().ReverseMap();
            CreateMap<CarDescription, DTOs.CarDescription.CarDescriptionInformationViewModel>().ReverseMap();

            CreateMap<CreateContainerTypeCommand, ContainerType>();
            CreateMap<UpdateContainerTypeCommand, ContainerType>();
            CreateMap<ContainerType, DTOs.ContainerType.ContainerTypeViewModel>().ReverseMap();

            CreateMap<CreateContainerSpecificationCommand, ContainerSpecification>();
            CreateMap<UpdateContainerSpecificationCommand, ContainerSpecification>();
            CreateMap<ContainerSpecification, DTOs.ContainerSpecification.ContainerSpecificationViewModel>().ReverseMap();

            CreateMap<CreateEnergySavingDeviceCommand, EnergySavingDevice>();
            CreateMap<UpdateEnergySavingDeviceCommand, EnergySavingDevice>();
            CreateMap<EnergySavingDevice, DTOs.EnergySavingDevice.EnergySavingDeviceViewModel>().ReverseMap();

            CreateMap<OrderingDriverReserve, DTOs.OrderingDriverReserve.OrderingDriverReserveViewModel>().ReverseMap();

            //CreateMap<CreateDriverProblemCommand, DriverProblem>();
            //CreateMap<UpdateDriverProblemCommand, DriverProblem>();
            //CreateMap<DriverProblem, DTOs.DriverProblem.DriverProblemViewModel>().ReverseMap();

            //CreateMap<CreateDriverProblemFileCommand, DriverProblemFile>();
            //CreateMap<UpdateDriverProblemFileCommand, DriverProblemFile>();
            //CreateMap<DriverProblemFile, DTOs.DriverProblemFile.DriverProblemFileViewModel>().ReverseMap();

            CreateMap<CreateUserCommand, SystemUsers>();
            CreateMap<UpdateUserCommand, SystemUsers>();
            CreateMap<CreateUser, SystemUsers>();
            CreateMap<UpdateUserById, SystemUsers>();
            CreateMap<SystemUsers, DTOs.User.UserViewModel>();
            //.ForMember(dest => dest.RoleMenu, act => act.MapFrom(src => src.RoleMenu));
            CreateMap<RoleMenu, DTOs.Role.RoleMenuDataViewModel>().ReverseMap();

            CreateMap<CreateCustomerProblemCommand, CustomerProblem>();
            CreateMap<UpdateCustomerProblemCommand, CustomerProblem>();


            CreateMap<CreateCustomerProblemFileCommand, CustomerProblemFile>();
            CreateMap<UpdateCustomerProblemFileCommand, CustomerProblemFile>();
            CreateMap<CustomerProblemFile, DTOs.CustomerProblemFile.CustomerProblemFileViewModel>().ReverseMap();

            CreateMap<OrderingPayment, DTOs.Ordering.Backoffice.OrderingPaymentViewModel>().ReverseMap();

            CreateMap<Contact, DTOs.Contact.ContactViewModel>().ReverseMap();

            CreateMap<CreateRoleCommand, Role>();
            CreateMap<UpdateRoleCommand, Role>();
            CreateMap<Role, RoleViewModel>();
            CreateMap<DummyWalletViewModel, DummyWallet>();


            CreateMap<CreateOrderingAddressHistoryCommand, OrderingAddressHistory>();
            CreateMap<UpdateOrderingAddressHistoryCommand, OrderingAddressHistory>();
            CreateMap<OrderingAddressHistory, DTOs.OrderingAddressHistory.OrderingAddressHistoryViewModel>().ReverseMap();

            CreateMap<MenuPermission, DTOs.MenuPermission.MenuPermissionViewModel>().ReverseMap();
            CreateMap<MenuPermission, DTOs.Menu.MenuPermissionViewModel>().ReverseMap();

            CreateMap<CustomerInbox, DTOs.CustomerInbox.CustomerInboxViewModel>().ReverseMap();
            CreateMap<DriverInbox, DTOs.DriverInbox.DriverInboxViewModel>().ReverseMap();

            CreateMap<QRCode, DTOs.QRCode.QRCodeViewModel>().ReverseMap();
            CreateMap<CreateQRCodeCommand, QRCode>().ReverseMap();

            CreateMap<CustomerProblem, DTOs.CustomerProblem.CustomerProblemViewModel>().ReverseMap();
            CreateMap<UserInbox, DTOs.Inbox.InboxViewModel>().ReverseMap();

            CreateMap<CustomerLevelCharacteristic, DTOs.CustomerLevelCharacteristic.CustomerLevelCharacteristicViewModel>().ReverseMap();
            CreateMap<CreateCustomerLevelCharacteristicCommand, CustomerLevelCharacteristic>().ReverseMap();

            CreateMap<Config, DTOs.Config.ConfigViewModel>().ReverseMap();

            CreateMap<SuggestionInformation, DTOs.SuggestionInformation.SuggestionInformationViewModel>().ReverseMap();

            CreateMap<DriverLevelCharacteristic, DTOs.DriverLevelCharacteristic.DriverLevelCharacteristicViewModel>().ReverseMap();
            CreateMap<CreateDriverLevelCharacteristicCommand, DriverLevelCharacteristic>().ReverseMap();

            CreateMap<PaymentType, DTOs.PaymentType.PaymentTypeViewModel>().ReverseMap();

            CreateMap<CountryFile, DTOs.CountryFile.CountryFileViewModel>().ReverseMap();
            CreateMap<CarTypeFile, DTOs.CarTypeFile.CarTypeFileViewModel>().ReverseMap();
            CreateMap<ProductTypeFile, DTOs.ProductTypeFile.ProductTypeFileViewModel>().ReverseMap();
            CreateMap<ProductPackagingFile, DTOs.ProductPackagingFile.ProductPackagingFileViewModel>().ReverseMap();
            CreateMap<Country, DTOs.Country.CountryCodeViewModel>().ReverseMap();
            CreateMap<CarDescriptionFile, DTOs.CarDescriptionFile.CarDescriptionFileViewModel>().ReverseMap();
            CreateMap<CarFeatureFile, DTOs.CarFeatureFile.CarFeatureFileViewModel>().ReverseMap();
            CreateMap<CarSpecificationFile, DTOs.CarSpecificationFile.CarSpecificationFileViewModel>().ReverseMap();
            CreateMap<CarListFile, DTOs.CarListFile.CarListFileViewModel>().ReverseMap();

            CreateMap<Ordering, DTOs.Ordering.Backoffice.OrderingConditionPageViewModel>().ReverseMap();
            CreateMap<Ordering, DTOs.Ordering.Backoffice.OrderingByIdViewModel>().ReverseMap();
            CreateMap<OrderingContainerFile, DTOs.OrderingContainerFile.OrderingContainerFileViewModel>().ReverseMap();

        }
    }
}
