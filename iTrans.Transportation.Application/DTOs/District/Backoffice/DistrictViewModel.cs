﻿using System;

namespace iTrans.Transportation.Application.DTOs.District.Backoffice
{
    public class DistrictViewModel
    {
        public virtual int Id { get; set; }
        public virtual int ProvinceId { get; set; }
        public virtual string ProvinceName { get; set; }
        public virtual string Name_TH { get; set; }
        public virtual string Name_ENG { get; set; }
        public virtual bool Active { get; set; }
    }
}
