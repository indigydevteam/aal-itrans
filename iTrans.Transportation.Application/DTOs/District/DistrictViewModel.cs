﻿using System;

namespace iTrans.Transportation.Application.DTOs.District
{
    public class DistrictViewModel
    {
        public virtual int id { get; set; }
        public virtual int provinceId { get; set; }
        public virtual string name_TH { get; set; }
        public virtual string name_ENG { get; set; }
       //public virtual bool Active { get; set; }
    }
}
