﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Application.DTOs.PaymentType
{
   public class PaymentTypeViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Name_EN { get; set; }
        public int Active { get; set; }
        public decimal Sequence { get; set; }
    }
}
