﻿using iTrans.Transportation.Application.DTOs.TermAndCondition;
using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Application.DTOs.CustomerTermAndCondition
{
    public class AcceptTermAndConditionViewModel
    {
        public List<CustomerTermAndConditionViewModel> termAndCondition { get; set; }
        public List<TermAndConditionViewModel> nonAcceptTermAndCondition { get; set; }
    }
}
