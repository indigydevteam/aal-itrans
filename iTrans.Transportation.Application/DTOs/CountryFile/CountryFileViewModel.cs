﻿using System;
namespace iTrans.Transportation.Application.DTOs.CountryFile
{
    public class CountryFileViewModel
    {
        public int id { get; set; }
        public int countryId { get; set; }
        public string fileName { get; set; }
        public string contentType { get; set; }
        public string filePath { get; set; }
        public string fileEXT { get; set; }
    }
}
