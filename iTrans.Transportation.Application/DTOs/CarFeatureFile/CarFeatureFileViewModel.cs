﻿using System;
namespace iTrans.Transportation.Application.DTOs.CarFeatureFile
{
    public class CarFeatureFileViewModel
    {
        public int id { get; set; }
        public int carFeatureId { get; set; }
        public string fileName { get; set; }
        public string contentType { get; set; }
        public string filePath { get; set; }
        public string fileEXT { get; set; }
    }
}
