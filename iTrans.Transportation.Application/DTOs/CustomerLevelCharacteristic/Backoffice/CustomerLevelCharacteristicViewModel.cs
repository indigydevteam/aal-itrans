﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Application.DTOs.Backoffice.CustomerLevelCharacteristic
{
    public class CustomerLevelCharacteristicViewModel
    {
        public int id { get; set; }
        public int level { get; set; }
        public int star { get; set; }
        public int requestCarPerWeek { get; set; }
        public int requestCarPerMonth { get; set; }
        public int requestCarPerYear { get; set; }
        public int cancelPerWeek { get; set; }
        public int cancelPerMonth { get; set; }
        public int cancelPerYear { get; set; }
        public decimal orderingValuePerWeek { get; set; }
        public decimal orderingValuePerMonth { get; set; }
        public decimal orderingValuePerYear { get; set; }
        public decimal discount { get; set; }
        public decimal fine { get; set; }
        public bool isDelete { get; set; }
        public DateTime created { get; set; }
        public DateTime modified { get; set; }
    }

}