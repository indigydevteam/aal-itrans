﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Application.DTOs.CustomerLevelCharacteristic
{
    public class CustomerLevelCharacteristicViewModel
    {
        public int Id { get; set; }
        public int Level { get; set; }
        public int Star { get; set; }
        public int RequestCarPerWeek { get; set; }
        public int RequestCarPerMonth { get; set; }
        public int RequestCarPerYear { get; set; }
        public int CancelPerWeek { get; set; }
        public int CancelPerMonth { get; set; }
        public int CancelPerYear { get; set; }
        public decimal OrderingValuePerWeek { get; set; }
        public decimal OrderingValuePerMonth { get; set; }
        public decimal OrderingValuePerYear { get; set; }
        public decimal Discount { get; set; }
        public decimal Fine { get; set; }
    }
  
}