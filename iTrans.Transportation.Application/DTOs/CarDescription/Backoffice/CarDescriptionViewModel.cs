﻿using iTrans.Transportation.Application.DTOs.CarDescriptionFile;
using iTrans.Transportation.Application.DTOs.CarList;
using iTrans.Transportation.Application.DTOs.CarType;

namespace iTrans.Transportation.Application.DTOs.CarDescription.Backoffice
{
    public class CarDescriptionViewModel
    {
        public virtual int id { get; set; }
        public virtual int carTypeId { get; set; }
        public virtual string carType { get; set; }
        public virtual int carListId { get; set; }
        public virtual string carList { get; set; }
        public virtual string name_TH { get; set; }
        public virtual string name_ENG { get; set; }
        public virtual int sequence { get; set; }
        public virtual bool specified { get; set; }
        public virtual bool active { get; set; }
        public CarDescriptionFileViewModel file { get; set; }
    }
}
