﻿using System;

namespace iTrans.Transportation.Application.DTOs.Inbox
{
    public class InboxViewModel
    {
        public virtual int Id { get; set; }
        public virtual Guid UserId { get; set; }
        public virtual string Title { get; set; }
        public virtual string Title_ENG { get; set; }
        public virtual string Module { get; set; }
        public virtual string Module_ENG { get; set; }
        public virtual string Detail { get; set; }
        public virtual string Content { get; set; }
        public virtual string ServiceCode { get; set; }
        public virtual string FromUser { get; set; }
        public virtual bool IsRead { get; set; }
        public virtual bool IsDelete { get; set; }
        public virtual DateTime Date { get; set; }
    }
}
