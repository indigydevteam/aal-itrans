﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Application.DTOs.TermAndCondition
{
   public class UserTermAndConditionViewModel
    {
        public virtual int Id { get; set; }
        public virtual string Module { get; set; }
        public virtual string Name_TH { get; set; }
        public virtual string Name_ENG { get; set; }
        public virtual string Section { get; set; }
        public virtual string Version { get; set; }
        public virtual int Sequence { get; set; }
        public virtual bool IsAccept { get; set; }
        public virtual bool IsPublic { get; set; }
        public virtual bool Active { get; set; }
        public string created { get; set; }
        public string createdBy { get; set; }
        public virtual bool isUserAccept{ get; set; }

}
}