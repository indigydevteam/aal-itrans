﻿using System;
namespace iTrans.Transportation.Application.DTOs.CarTypeFile
{
    public class CarTypeFileViewModel
    {
        public int id { get; set; }
        public int carTypeId { get; set; }
        public string fileName { get; set; }
        public string contentType { get; set; }
        public string filePath { get; set; }
        public string fileEXT { get; set; }
    }
}
