﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Application.DTOs.ApplicationLog
{
   public class ApplicationLogViewModel
    {
        public virtual int Id { get; set; }
        public virtual string Module { get; set; }
        public virtual string Section { get; set; }
        public virtual string Function { get; set; }
        public virtual string Content { get; set; }
    }
}
