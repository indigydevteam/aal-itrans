﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Application.DTOs.ApplicationLog.BackOffice
{
  public  class ApplicationLogViewModel
    {
        public virtual int Id { get; set; }
        public virtual string Module { get; set; }
        public virtual string Section { get; set; }
        public virtual string Function { get; set; }
        public virtual string Content { get; set; }
        public virtual string CreatedText { get; set; }
        public virtual DateTime Created { get; set; }
        public virtual string ModifiedBy { get; set; }

    }
    public class ApplicationLogSelectModifiedByViewModel
    {
        public virtual String ModifiedBy { get; set; }
    }
    public class ApplicationLogSelectModuleViewModel
    {
        public virtual String Module { get; set; }
    }

}
