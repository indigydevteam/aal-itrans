﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Application.DTOs.OrderingStatus
{
    public class OrderingStatusViewModel
    {
        public int id { set; get; }
        public string th { set; get; }
        public string eng { set; get; }
        public string description { set; get; }
        public string color { set; get; }
        //public string driver_th { set; get; }
        //public string driver_eng { set; get; }
        //public string driver_description { set; get; }
    }
}
