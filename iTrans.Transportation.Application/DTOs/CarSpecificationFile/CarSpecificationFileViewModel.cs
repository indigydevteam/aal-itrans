﻿using System;
namespace iTrans.Transportation.Application.DTOs.CarSpecificationFile
{
    public class CarSpecificationFileViewModel
    {
        public int id { get; set; }
        public int carSpecificationId { get; set; }
        public string fileName { get; set; }
        public string contentType { get; set; }
        public string filePath { get; set; }
        public string fileEXT { get; set; }
    }
}
