﻿using System;
namespace iTrans.Transportation.Application.DTOs.OrderingContainerFile
{
    public class OrderingContainerFileViewModel
    {
        public virtual int Id { get; set; }
        public virtual int OrderingContainerId { get; set; }
        public virtual string FileName { get; set; }
        public virtual string ContentType { get; set; }
        public virtual string FilePath { get; set; }
        public virtual string DirectoryPath { get; set; }
        public virtual string FileEXT { get; set; }
        public virtual string DocumentType { get; set; }
        public virtual int Sequence { get; set; }
    }
}
