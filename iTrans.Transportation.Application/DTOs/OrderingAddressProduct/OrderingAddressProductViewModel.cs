﻿using iTrans.Transportation.Application.DTOs.OrderingAddressProductFile;
using iTrans.Transportation.Application.DTOs.OrderingContainer;
using iTrans.Transportation.Application.DTOs.OrderingProduct;
using iTrans.Transportation.Application.DTOs.OrderingProductFile;
using iTrans.Transportation.Application.DTOs.ProductPackaging;
using iTrans.Transportation.Application.DTOs.ProductType;
using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Application.DTOs.OrderingAddressProduct
{
    public class OrderingAddressProductViewModel
    {
        public int id { get; set; }
        // public virtual Guid OrderingId { get; set; }
        //public OrderingProductViewModel OrderingProduct { get; set; }
        //public OrderingContainerViewModel OrderingContainer { get; set; }
        public int OrderingProductId { get; set; }
        public int OrderingContainerId { get; set; }
        public string name { get; set; }
        public string productType { get; set; }
        public List<ProductTypeViewModel> productTypes { get; set; }
        public string productTypeDetail { get; set; }
        public string packaging { get; set; }
        public List<ProductPackagingViewModel> packagings { get; set; }
        public string packagingDetail { get; set; }
        public int width { get; set; }
        public int length { get; set; }
        public int height { get; set; }
        public int weight { get; set; }
        public int quantity { get; set; }
        public int sequence { get; set; }
        public List<OrderingAddressProductFileViewModel> productFiles { get; set; }

    }
}
