﻿using iTrans.Transportation.Application.DTOs.ProductPackaging;
using iTrans.Transportation.Application.DTOs.ProductType;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Application.DTOs.OrderingAddressProduct
{
  public  class CreateAddressProductViewModel
    {
        public virtual string Name { get; set; }
        public virtual int ProductTypeId { get; set; }
        public virtual string ProductTypeDetail { get; set; }
        public virtual int PackagingId { get; set; }
        public virtual string PackagingDetail { get; set; }
        public virtual int Width { get; set; }
        public virtual int Length { get; set; }
        public virtual int Height { get; set; }
        public virtual int Weight { get; set; }
        public virtual int Quantity { get; set; }
        public virtual int Sequence { get; set; }
        public virtual IList<IFormFile> Files { get; set; }
    }
}
