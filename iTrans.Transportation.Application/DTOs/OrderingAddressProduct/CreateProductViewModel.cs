﻿using iTrans.Transportation.Application.DTOs.ProductPackaging;
using iTrans.Transportation.Application.DTOs.ProductType;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Application.DTOs.OrderingAddressProduct
{
  public  class CreateProductFormViewModel
    {
        public virtual int Product { get; set; }
        public virtual int Quantity { get; set; }
        public virtual int Sequence { get; set; }
    }
}
