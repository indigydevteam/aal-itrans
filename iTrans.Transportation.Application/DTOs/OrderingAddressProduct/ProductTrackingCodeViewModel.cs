﻿using iTrans.Transportation.Application.DTOs.OrderingAddress;
using iTrans.Transportation.Application.DTOs.OrderingAddressProductFile;
using iTrans.Transportation.Application.DTOs.ProductPackaging;
using iTrans.Transportation.Application.DTOs.ProductType;
using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Application.DTOs.OrderingAddressProduct
{
  public  class ProductTrackingCodeViewModel
    {
        public virtual int Id { get; set; }
        public virtual AddressTrackingCodeViewModel OrderingAddress { get; set; }
        public virtual string Name { get; set; }
        public virtual ProductTypeViewModel ProductType { get; set; }
        public virtual string ProductTypeDetail { get; set; }
        public virtual ProductPackagingViewModel Packaging { get; set; }
        public virtual string PackagingDetail { get; set; }
        public virtual int Width { get; set; }
        public virtual int Length { get; set; }
        public virtual int Height { get; set; }
        public virtual int Weight { get; set; }
        public virtual int Quantity { get; set; }
        public virtual int Sequence { get; set; }
        public virtual List<OrderingAddressProductFileViewModel> ProductFiles { get; set; }
    }
}
