﻿using iTrans.Transportation.Application.DTOs.CustomerAddress;
using iTrans.Transportation.Application.DTOs.UserLevel;
using System;
using System.Collections.Generic;

namespace iTrans.Transportation.Application.DTOs.VerifyStatus
{
    public class VerifyStatusViewModel
    {
        public int id { set; get; }
        public string th { set; get; }
        public string eng { set; get; }
        public string description { set; get; }
        public string color { set; get; }
    }
}
