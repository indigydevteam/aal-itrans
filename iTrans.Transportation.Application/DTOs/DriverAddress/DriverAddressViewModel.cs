﻿using iTrans.Transportation.Application.DTOs.Country;
using iTrans.Transportation.Application.DTOs.District;
using iTrans.Transportation.Application.DTOs.Province;
using iTrans.Transportation.Application.DTOs.Subdistrict;
using System;

namespace iTrans.Transportation.Application.DTOs.DriverAddress
{
    public class DriverAddressViewModel
    {
        public int id { get; set; }
        public Guid driverId { get; set; }
        //public int CountryId { get; set; }
        public CountryViewModel country { get; set; }
        //public int ProvinceId { get; set; }
        public ProvinceViewModel province { get; set; }
        //public int DistrictId { get; set; }
        public DistrictViewModel district { get; set; }
        //public int SubdistrictId { get; set; }
        public SubdistrictViewModel subdistrict { get; set; }
        public string postCode { get; set; }
        public string road { get; set; }
        public string alley { get; set; }
        public string address { get; set; }
        public string branch { get; set; }
        public string addressType { get; set; }
        public string addressName { get; set; }
        public string contactPerson { get; set; }
        public string contactPhoneCode { get; set; }
        public string contactPhoneNumber { get; set; }
        public string contactEmail { get; set; }
        public string maps { get; set; }
        public bool isMainData { get; set; }
        public int sequence { get; set; }
    }
}
