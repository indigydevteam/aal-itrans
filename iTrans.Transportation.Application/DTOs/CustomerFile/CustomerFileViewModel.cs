﻿using System;
namespace iTrans.Transportation.Application.DTOs.CustomerFile
{
    public class CustomerFileViewModel
    {
        public int Id { get; set; }
        public Guid CustomerId { get; set; }
        public string FileName { get; set; }
        public string ContentType { get; set; }
        public string FilePath { get; set; }
        public string FileEXT { get; set; }
        public string DocumentType { get; set; }
        public int Sequence { get; set; }
        public bool IsApprove { get; set; }
        public bool IsDelete { get; set; }
    }
}
