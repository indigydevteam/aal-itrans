﻿using System;

namespace iTrans.Transportation.Application.DTOs.Backoffice.Title
{
    public class TitleViewModel
    {
        public int Id { get; set; }
        public string Name_TH { get; set; }
        public string Name_ENG { get; set; }
        public int Sequence { get; set; }
        public bool Active { get; set; }
        public bool IsDelete { get; set; }
        public bool Specified { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
    }
}
