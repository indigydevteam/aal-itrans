﻿using System;

namespace iTrans.Transportation.Application.DTOs.Title
{
    public class TitleViewModel
    {
        public int id { get; set; }
        public string name_TH { get; set; }
        public string name_ENG { get; set; }
        public bool specified { get; set; }

        //public virtual int Sequence { get; set; }
        //public virtual bool Active { get; set; }
    }
}
