﻿using iTrans.Transportation.Application.DTOs.PaymentType;
using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Application.DTOs.DriverPayment
{
   public class DriverPaymentViewModel
    {
        public int Id { get; set; }
        public Guid DriverId { get; set; }
        public PaymentTypeViewModel Type { get; set; }
        public string Bank { get; set; }
        public string AccountType { get; set; }
        public string Value { get; set; }
        public decimal Amount { get; set; }
        public string Description { get; set; }

    }
}
