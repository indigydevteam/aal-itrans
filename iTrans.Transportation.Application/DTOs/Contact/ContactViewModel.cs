﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Application.DTOs.Contact
{
   public class ContactViewModel
    {
        public virtual int Id { get; set; }
        public virtual string address { get; set; }
        public virtual string email { get; set; }
        public virtual string fax { get; set; }
        public virtual string faecbook { get; set; }
        public virtual string youtube { get; set; }
        public virtual string line { get; set; }
        public virtual string instagrame { get; set; }
        public virtual string twitter { get; set; }
        public virtual string phone { get; set; }
    }
}
