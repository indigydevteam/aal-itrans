﻿using System;

namespace iTrans.Transportation.Application.DTOs.DummyWallet
{
    public class DummyWalletViewModel
    {
        public virtual int id { get; set; }
        public virtual Guid userId { get; set; }
        public virtual string module { get; set; }
        public virtual decimal amount { get; set; }
    }
}
