﻿using iTrans.Transportation.Application.DTOs.OrderingAddress;
using iTrans.Transportation.Application.DTOs.OrderingProduct;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;

namespace iTrans.Transportation.Application.DTOs.OrderingAddressFile
{
    public class CreateOrderingAddressFileViewModel
    {
        public int OrderingAddressProductId { get; set; }
        public List<IFormFile>  file { get; set; }
    }
}
