﻿using iTrans.Transportation.Application.DTOs.OrderingAddress;
using iTrans.Transportation.Application.DTOs.OrderingProduct;
using System;
namespace iTrans.Transportation.Application.DTOs.OrderingAddressFile
{
    public class OrderingAddressFileViewModel
    {
        public int id { get; set; }
        public int OrderingAddressProductId { get; set; }
        public string fileName { get; set; }
        public string contentType { get; set; }
        public string filePath { get; set; }
        public string directoryPath { get; set; }
        public string fileEXT { get; set; }
        public int sequence { get; set; }

    }
}
