﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Application.DTOs.UserLevelCondition
{
   public class UserLevelConditionViewModel
    {
        public virtual int Id { get; set; }
        public virtual int UserLevelId { get; set; }
        public virtual string Characteristics_TH { get; set; }
        public virtual string Characteristics_ENG { get; set; }
        public virtual int Sequence { get; set; }
        //public virtual bool Active { get; set; }
    }
}
