﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Application.DTOs.News
{
    public class NewsViewModel
    {
        public virtual int Id { get; set; }
        public virtual string Title { get; set; }
        public virtual string Detail { get; set; }
        public virtual string ImageFileUrl { get; set; }
        public virtual string? Url { get; set; }

        public virtual string FileUrl { get; set; }
        public virtual string FileType { get; set; }
        public virtual string FileName { get; set; }
        public virtual string? VideoEmbedUrl { get; set; }
        public virtual DateTime Published { get; set; }

        public virtual int TotalView { get; set; }
        public virtual string NewsType { get; set; }
        public virtual string VideoUrl { get; set; }

        public virtual bool IsHighlight { get; set; }
        public virtual bool IsAll { get; set; }
        public virtual string TargetCustomerType { get; set; }
        public virtual string TargetDriverType { get; set; }
        public virtual bool IsSelection { get; set; }

        public virtual DateTime StartDate { get; set; }
        public virtual DateTime EndDate { get; set; }
        public virtual DateTime HLStartDate { get; set; }
        public virtual DateTime HLEndDate { get; set; }

        public bool IsGuest { get; set; }
        public bool IsActive { get; set; }
        public bool IsNotification { get; set; }
         



    }
}
