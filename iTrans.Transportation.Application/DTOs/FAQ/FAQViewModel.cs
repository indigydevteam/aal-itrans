﻿namespace iTrans.Transportation.Application.DTOs.FAQ
{
    public class FAQViewModel
    {
        public  int id { get; set; }
        public  string title_TH { get; set; }
        public  string detail_TH { get; set; }
        public  string title_ENG { get; set; }
        public  string detail_ENG { get; set; }
        public  string module { get; set; }
        public  int sequence { get; set; }
        public  bool active { get; set; }
        public string created { get; set; }
        public string modified { get; set; }
    }
    public class FAQViewModelFormCreate
    {
        public string title_TH { get; set; }
        public string detail_TH { get; set; }
        public string module { get; set; }
        public bool active { get; set; }
    }
}
