﻿using System;
namespace iTrans.Transportation.Application.DTOs.OrderingDriverFile
{
    public class OrderingDriverFileViewModel
    {
        public int Id { get; set; }
        public int OrderingDriverId { get; set; }
        public string FileName { get; set; }
        public string ContentType { get; set; }
        public string FilePath { get; set; }
        public string DirectoryPath { get; set; }
        public string FileEXT { get; set; }
        public string DocumentType { get; set; }
        public int Sequence { get; set; }
        public bool IsApprove { get; set; }
        public bool IsDelete { get; set; }
    }
}
