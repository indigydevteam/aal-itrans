﻿using iTrans.Transportation.Application.DTOs.CorporateType;
using iTrans.Transportation.Application.DTOs.Ordering;
using iTrans.Transportation.Application.DTOs.OrderingDriverFile;
using iTrans.Transportation.Application.DTOs.Title;
using iTrans.Transportation.Application.DTOs.UserLevel;
using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Application.DTOs.OrderingDriver
{
   public class OrderingDriverViewModel
    {
        public virtual int id { get; set; }
        public virtual Guid orderingId { get; set; }
        public virtual Guid driverId { get; set; }
        public virtual string driverType { get; set; }
        public virtual CorporateTypeViewModel corporateType { get; set; }
        public virtual TitleViewModel title { get; set; }
        public virtual string firstName { get; set; }
        public virtual string middleName { get; set; }
        public virtual string lastName { get; set; }
        public virtual string name { get; set; }
        public virtual string identityNumber { get; set; }
        public virtual TitleViewModel contactPersonTitle { get; set; }
        public virtual string contactPersonFirstName { get; set; }
        public virtual string contactPersonMiddleName { get; set; }
        public virtual string contactPersonLastName { get; set; }
        public virtual string phoneCode { get; set; }
        public virtual string phoneNumber { get; set; }
        public virtual string email { get; set; }
        public virtual int level { get; set; }
        public virtual UserLevelViewModel driverLevel { get; set; }
        public virtual string facbook { get; set; }
        public virtual string line { get; set; }
        public virtual string twitter { get; set; }
        public virtual string whatapp { get; set; }
        public virtual List<OrderingDriverFileViewModel> driverFiles { get; set; }
        public virtual int star { get; set; }
        public virtual string rating { get; set; }
        public virtual string grade { get; set; }

    }
}
