﻿using iTrans.Transportation.Application.DTOs.Country;
using iTrans.Transportation.Application.DTOs.District;
using iTrans.Transportation.Application.DTOs.Province;
using iTrans.Transportation.Application.DTOs.Subdistrict;

namespace iTrans.Transportation.Application.DTOs.Location
{
    public class LocationViewModel
    {
        public virtual CountryViewModel country { get; set; }
        public virtual ProvinceViewModel province { get; set; }
        public virtual DistrictViewModel district { get; set; }
        public virtual SubdistrictViewModel subdistrict { get; set; }
        public virtual string road { get; set; }
        public virtual string alley { get; set; }
        public virtual string address { get; set; }
        public virtual string postCode { get; set; }
    }
}
