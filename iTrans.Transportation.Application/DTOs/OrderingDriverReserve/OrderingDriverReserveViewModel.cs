﻿using iTrans.Transportation.Application.DTOs.CorporateType;
using iTrans.Transportation.Application.DTOs.Driver;
using iTrans.Transportation.Application.DTOs.DriverAnnouncement;
using iTrans.Transportation.Application.DTOs.Ordering;
using iTrans.Transportation.Application.DTOs.OrderingDriverFile;
using iTrans.Transportation.Application.DTOs.OrderingDriverReserveStatus;
using iTrans.Transportation.Application.DTOs.Title;
using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Application.DTOs.OrderingDriverReserve
{
   public class OrderingDriverReserveViewModel
    {
        public virtual int id { get; set; }
        public virtual DriverProfileViewModel driver { get; set; }
        //public virtual OrderingDriverAnnouncementViewModel driverAnnouncement { get; set; }
        //public virtual decimal CustomerDesiredPrice { get; set; }
        public virtual decimal driverOfferingPrice { get; set; }
        //public virtual bool IsOrderingDriverOffer { get; set; }
        public virtual int status { get; set; }
        public virtual OrderingDriverReserveStatusViewModel statusObj { get; set; }
    }
}
