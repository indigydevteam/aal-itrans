﻿using iTrans.Transportation.Application.DTOs.Country;
using iTrans.Transportation.Application.DTOs.District;
using iTrans.Transportation.Application.DTOs.Province;
using iTrans.Transportation.Application.DTOs.Region;
using System;

namespace iTrans.Transportation.Application.DTOs.DriverCarLocation.Backoffice
{
   public class DriverCarLocationViewModel
    {
        public int id { get; set; }
        public int driverCarId { get; set; }
        public int countryId { get; set; }
        public CountryViewModel country { get; set; }
        public int regionId { get; set; }
        public RegionViewModel region { get; set; }
        public int provinceId { get; set; }
        public ProvinceViewModel province { get; set; }
        public int districtId { get; set; }
        public DistrictViewModel district { get; set; }
        public string locationType { get; set; }
        public int sequence { get; set; }
    }
}
