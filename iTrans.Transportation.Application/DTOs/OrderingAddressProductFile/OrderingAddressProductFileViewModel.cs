﻿using System;
namespace iTrans.Transportation.Application.DTOs.OrderingAddressProductFile
{
    public class OrderingAddressProductFileViewModel
    {
        public virtual int id { get; set; }
        public virtual int orderingAddressProductId { get; set; }
        public virtual string fileName { get; set; }
        public virtual string contentType { get; set; }
        public virtual string filePath { get; set; }
        public virtual string directoryPath { get; set; }
        public virtual string fileEXT { get; set; }
        public virtual string type { get; set; }
        public virtual int sequence { get; set; }
    }
}
