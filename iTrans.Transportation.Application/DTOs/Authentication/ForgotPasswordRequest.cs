﻿using System.ComponentModel.DataAnnotations;

namespace iTrans.Transportation.Application.DTOs.Authentication
{
    public class ForgotPasswordRequest
    {
        [Required]
        public string UserName { get; set; }
    }
}
