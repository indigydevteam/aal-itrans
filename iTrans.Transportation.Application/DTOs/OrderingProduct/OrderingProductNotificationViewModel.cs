﻿using iTrans.Transportation.Application.DTOs.OrderingProductFile;
using iTrans.Transportation.Application.DTOs.ProductPackaging;
using iTrans.Transportation.Application.DTOs.ProductType;
using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Application.DTOs.OrderingProduct
{
  public  class OrderingProductNotificationViewModel
    {
        public virtual int id { get; set; }
        public virtual string name { get; set; }
        public virtual string productType { get; set; }
        public virtual List<ProductTypeViewModel> productTypes { get; set; }
        public virtual string packaging { get; set; }
        public virtual List<ProductPackagingViewModel> packagings { get; set; }
        public virtual int quantity { get; set; }
        public virtual List<OrderingProductFileViewModel> files { get; set; }
    }
}
