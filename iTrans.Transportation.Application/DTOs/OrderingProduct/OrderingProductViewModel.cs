﻿using iTrans.Transportation.Application.DTOs.OrderingProductFile;
using iTrans.Transportation.Application.DTOs.ProductPackaging;
using iTrans.Transportation.Application.DTOs.ProductType;
using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Application.DTOs.OrderingProduct
{
  public  class OrderingProductViewModel
    {
        public int id { get; set; }
        public Guid orderingId { get; set; }
        public int CustomerProductId { get; set; }
        public string name { get; set; }
        public string productType { get; set; }
        public List<ProductTypeViewModel> productTypes { get; set; }
        public string productTypeDetail { get; set; }
        public string packaging { get; set; }
        public List<ProductPackagingViewModel> packagings { get; set; }
        public string packagingDetail { get; set; }
        public int width { get; set; }
        public int length { get; set; }
        public int height { get; set; }
        public decimal weight { get; set; }
        public int quantity { get; set; }
        public int sequence { get; set; }
        public List<OrderingProductFileViewModel>  files { get; set; }
    }
}
