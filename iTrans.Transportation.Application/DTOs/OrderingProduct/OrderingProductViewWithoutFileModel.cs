﻿using iTrans.Transportation.Application.DTOs.OrderingProductFile;
using iTrans.Transportation.Application.DTOs.ProductPackaging;
using iTrans.Transportation.Application.DTOs.ProductType;
using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Application.DTOs.OrderingProduct
{
  public  class OrderingProductWithoutFileViewModel
    {
        public virtual int id { get; set; }
        public virtual Guid orderingId { get; set; }
        public virtual string name { get; set; }
        public virtual string productType { get; set; }
        public virtual List<ProductTypeViewModel> productTypes { get; set; }
        public virtual string productTypeDetail { get; set; }
        public virtual string packaging { get; set; }
        public virtual List<ProductPackagingViewModel> packagings { get; set; }
        public virtual string packagingDetail { get; set; }
        public virtual int width { get; set; }
        public virtual int length { get; set; }
        public virtual int height { get; set; }
        public virtual decimal weight { get; set; }
        public virtual int quantity { get; set; }
        //public virtual List<OrderingProductFileViewModel>  files { get; set; }
    }
}
