﻿using iTrans.Transportation.Application.DTOs.ProductPackaging;
using iTrans.Transportation.Application.DTOs.ProductType;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Application.DTOs.OrderingProduct
{
  public  class CreateProductViewModel
    {
        public string Name { get; set; }
        public int[] ProductType { get; set; }
        public string ProductTypeDetail { get; set; }
        public int[] Packaging { get; set; }
        public string PackagingDetail { get; set; }
        public int Width { get; set; }
        public int Length { get; set; }
        public int Height { get; set; }
        public decimal Weight { get; set; }
        public int Quantity { get; set; }
        public int CustomerProductId { get; set; }
        public List<IFormFile> Files { get; set; }
        public int Sequence { get; set; }
    }
}
