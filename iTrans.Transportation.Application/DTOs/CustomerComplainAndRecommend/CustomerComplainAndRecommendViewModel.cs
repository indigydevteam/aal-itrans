﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Application.DTOs.CustomerComplainAndRecommend
{
   public class CustomerComplainAndRecommendViewModel
    {
        public virtual int Id { get; set; }
        public virtual Guid CustomerId { get; set; }
        public virtual string Title { get; set; }
        public virtual string Detail { get; set; }
    }
}
