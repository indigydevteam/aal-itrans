﻿using iTrans.Transportation.Application.DTOs.Customer.Backoffice;
using iTrans.Transportation.Application.DTOs.Driver.Backoffice;
using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Application.DTOs.CustomerComplainAndRecommend.BackOffice
{
   public class CustomerComplainAndRecommendViewModel
    {
        public  int id { get; set; }
        public  CustomerViewModel customer { get; set; }
        public  string title { get; set; }
        public  string detail { get; set; }
        public string created { get; set; }
        public  string name { get; set; }
        public  string module { get; set; }
        public DriverViewModel driver { get; set; }

    }
}
