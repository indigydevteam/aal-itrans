﻿using iTrans.Transportation.Application.DTOs.CorporateType;
using iTrans.Transportation.Application.DTOs.CustomerAddress;
using iTrans.Transportation.Application.DTOs.CustomerFile;
using iTrans.Transportation.Application.DTOs.CustomerPayment;
using iTrans.Transportation.Application.DTOs.Title;
using iTrans.Transportation.Application.DTOs.UserLevel;
using iTrans.Transportation.Application.DTOs.CustomerTermAndCondition;
using System;
using System.Collections.Generic;
using iTrans.Transportation.Application.DTOs.VerifyStatus;
using iTrans.Transportation.Application.DTOs.TermAndCondition;

namespace iTrans.Transportation.Application.DTOs.Customer
{
    public class CustomerInformationViewModel
    {
        public Guid id { get; set; }
        public string password { get; set; }
        public string customerType { get; set; }
        public CorporateTypeViewModel corporateType { get; set; }
        public TitleViewModel title { get; set; }
        public string firstName { get; set; }
        public string middleName { get; set; }
        public string lastName { get; set; }
        public string name { get; set; }
        public int identityType { get; set; }
        public string identityNumber { get; set; }
        public TitleViewModel contactPersonTitle { get; set; }
        public string contactPersonFirstName { get; set; }
        public string contactPersonMiddleName { get; set; }
        public string contactPersonLastName { get; set; }
        public string birthday { get; set; }
        public string phoneCode { get; set; }
        public string phoneNumber { get; set; }
        public string email { get; set; } 
        public int level { get; set; }
        public UserLevelViewModel customerLevel { get; set; }
        public string facbook { get; set; }
        public string line { get; set; }
        public string twitter { get; set; }
        public string whatapp { get; set; }
        public string wechat { get; set; }
        public IList<CustomerTermAndConditionViewModel> termAndConditions { get; set; }
        public List<CustomerPaymentViewModel> payments { get; set; }
        public List<ProfileAddressViewModel> addresses { get; set; }
        public List<CustomerFileViewModel> files { get; set; }
        public int star { get; set; }
        public UserLevelViewModel nextLevel { get; set; }
        public string rating { get; set; }
        public string grade { get; set; }
        public string created { get; set; }
        public int verifyStatus { get; set; }
        public VerifyStatusViewModel verifyStatusObj { get; set; }
        public bool isRegister { get; set; }
        public bool status { get; set; }
        public bool isDelete { get; set; }
    }
}
