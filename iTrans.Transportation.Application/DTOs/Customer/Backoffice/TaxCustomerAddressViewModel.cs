﻿using iTrans.Transportation.Application.DTOs.CustomerAddress;
using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Application.DTOs.Customer.Backoffice
{
  public  class TaxCustomerAddressViewModel
    {
        public Guid customerId { get; set; }
        public string customerType { get; set; }
        public CustomerAddressViewModel personal { get; set; }
        public List<CustomerAddressViewModel> corporate { get; set; }
    }
}
