﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Application.DTOs.Customer.Backoffice
{
   public class CustomerByVerifyStatusViewModel
    {
        public int presonalstatustrue { get; set; }
        public int presonalstatusfalse { get; set; }
        public int corporatestatustrue { get; set; }
        public int corporatestatusfalse { get; set; }
    }
}
