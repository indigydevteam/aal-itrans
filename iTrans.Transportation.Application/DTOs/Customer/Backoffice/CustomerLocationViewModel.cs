﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Application.DTOs.Customer.Backoffice
{
   public class CustomerLocationViewModel
    {
        public int Id { get; set; }
        public Guid CustomerId { get; set; }
        public int CountryId { get; set; }
        public int ProvinceId { get; set; }
        public int DistrictId { get; set; }
        public int SubdistrictId { get; set; }
        public string Branch { get; set; }
        public string PostCode { get; set; }
        public string Road { get; set; }
        public string Alley { get; set; }
        public string Address { get; set; }
        public string AddressType { get; set; }
        public bool IsMainData { get; set; }
        public int Sequence { get; set; }
    }
}
