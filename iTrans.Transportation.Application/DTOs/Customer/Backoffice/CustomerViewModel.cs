﻿using iTrans.Transportation.Application.DTOs.CorporateType;
using iTrans.Transportation.Application.DTOs.CustomerAddress;
using iTrans.Transportation.Application.DTOs.CustomerFile;
using iTrans.Transportation.Application.DTOs.CustomerPayment;
using iTrans.Transportation.Application.DTOs.Title;
using iTrans.Transportation.Application.DTOs.UserLevel;
using iTrans.Transportation.Application.DTOs.CustomerTermAndCondition;
using System;
using System.Collections.Generic;
using iTrans.Transportation.Application.DTOs.VerifyStatus;

namespace iTrans.Transportation.Application.DTOs.Customer.Backoffice
{
    public class CustomerViewModel
    {
        public virtual Guid Id { get; set; }
        public virtual string Password { get; set; }
        public virtual string CustomerType { get; set; }
        public virtual CorporateTypeViewModel CorporateType { get; set; }
        public virtual int CorporateTypeId { get; set; }
        public virtual TitleViewModel Title { get; set; }
        public virtual string FirstName { get; set; }
        public virtual string MiddleName { get; set; }
        public virtual string LastName { get; set; }
        public virtual string Name { get; set; }
        public virtual int IdentityType { get; set; }
        public virtual string IdentityNumber { get; set; }
        public virtual TitleViewModel ContactPersonTitle { get; set; }
        public virtual string ContactPersonFirstName { get; set; }
        public virtual string ContactPersonMiddleName { get; set; }
        public virtual string ContactPersonLastName { get; set; }
        public virtual DateTime Birthday { get; set; }
        public virtual string PhoneCode { get; set; }
        public virtual string PhoneNumber { get; set; }
        public virtual string Email { get; set; }
        public virtual int Level { get; set; }
        public virtual string LevelName { get; set; }
        public virtual UserLevelViewModel CustomerLevel { get; set; }
        public virtual string Facbook { get; set; }
        public virtual string Line { get; set; }
        public virtual string Twitter { get; set; }
        public virtual string Whatapp { get; set; }
        public virtual string Wechat { get; set; }
        public virtual int TermAndConditionsId { get; set; }
        public virtual List<CustomerPaymentViewModel> Payments { get; set; }
        public virtual List<CustomerAddressViewModel> Addresses { get; set; }
        public virtual List<CustomerFileViewModel> Files { get; set; }
        public virtual int Star { get; set; }
        public virtual int VerifyStatus { get; set; }
        public virtual VerifyStatusViewModel VerifyStatusObj { get; set; }
        public virtual string VerifyStatusName { get; set; }
        public virtual string VerifyStatusColor { get; set; }
        public virtual bool isRegister { get; set; }
        public virtual bool Status { get; set; }
        public virtual string imageProfile { get; set; }
        public virtual string idCard { get; set; }
        public virtual string companyCertificate { get; set; }
        public virtual string Created { get; set; }

    }
}