﻿using iTrans.Transportation.Application.DTOs.CarList;
using iTrans.Transportation.Application.DTOs.CarListFile;
using iTrans.Transportation.Application.DTOs.CarSpecificationFile;
using iTrans.Transportation.Application.DTOs.CarType;
using System;

namespace iTrans.Transportation.Application.DTOs.Backoffice.CarSpecification
{
    public class CarSpecificationViewModel
    {
        public int id { get; set; }
        public int carTypeId { get; set; }
        public string carType { get; set; }
        public int carListId { get; set; }
        public string carList { get; set; }
        public string name_TH { get; set; }
        public string name_ENG { get; set; }
        public int sequence { get; set; }
        public bool specified { get; set; }
        public bool specifiedTemperature { get; set; }
        public bool specifiedEnergySavingDevice { get; set; }
        public bool active { get; set; }
        public bool isDelete { get; set; }
        public DateTime created { get; set; }
        public DateTime modified { get; set; }
        public CarSpecificationFileViewModel file { get; set; }
    }
}
