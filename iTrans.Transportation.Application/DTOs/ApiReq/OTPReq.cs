﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.DTOs.ApiReq
{
    public class OTPReq
    { 
        public string phone_no { get; set; }
        public string ref_key { get; set; }
        public string otp { get; set; }
        public string user_type { get; set; }
    }
}
