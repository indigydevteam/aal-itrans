﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.DTOs.ApiReq
{
    public class UpdateOtpReq
    {
        public string token { get; set; }
        public string otp { get; set; }
        public string client_id { get; set; }
    }
}
