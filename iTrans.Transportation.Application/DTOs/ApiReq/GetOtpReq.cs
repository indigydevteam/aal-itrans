﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.DTOs.ApiReq
{
    public class GetOtpReq
    {
        public string to_client { get; set; }
        public string to_url{ get; set; }
        public object data { get; set; }
    }
}
