﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Application.DTOs.Backoffice.RegisterInformation
{
    public class RegisterInformationViewModel
    {
        public virtual int Id { get; set; }
        public virtual string Module { get; set; }
        public virtual string Title_TH { get; set; }
        public virtual string Information_TH { get; set; }
        public virtual string Title_ENG { get; set; }
        public virtual string Information_ENG { get; set; }
        public virtual string Picture { get; set; }
        public virtual int Sequence { get; set; }
        public bool Active { get; set; }
        public bool IsDelete { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
    }
}