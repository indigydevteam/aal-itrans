﻿namespace iTrans.Transportation.Application.DTOs.ProblemTopic
{
    public class ProblemTopicViewModel
    {
        public virtual int id { get; set; }
        public virtual string module { get; set; }
        public virtual string value_TH { get; set; }
        public virtual string value_ENG { get; set; }
        public virtual bool active { get; set; }
        public virtual int sequence { get; set; }
    }
}
