﻿using System;
namespace iTrans.Transportation.Application.DTOs.CustomerProductFile
{
    public class CustomerProductFileViewModel
    {
        public virtual int Id { get; set; }
        public virtual int CustomerProductId { get; set; }
        public virtual string FileName { get; set; }
        public virtual string ContentType { get; set; }
        public virtual string FilePath { get; set; }
        public virtual string FileEXT { get; set; }
    }
}
