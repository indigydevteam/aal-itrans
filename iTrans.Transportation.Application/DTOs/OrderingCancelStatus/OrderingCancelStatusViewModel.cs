﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Application.DTOs.OrderingCancelStatus
{
   public class OrderingCancelStatusViewModel
    {
        public virtual int Id { get; set; }
        public virtual string Module { get; set; }
        public virtual string Name_TH { get; set; }
        public virtual string Name_ENG { get; set; }
        public virtual int Sequence { get; set; }
        public virtual bool Active { get; set; }
    }
}
