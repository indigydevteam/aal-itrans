﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Application.DTOs.ContainerSpecification
{
   public class ContainerSpecificationViewModel
    {
        public virtual int id { get; set; }
        public virtual string name_TH { get; set; }
        public virtual string name_ENG { get; set; }
        public virtual bool active { get; set; }
        public virtual bool specified { get; set; }
        public virtual int sequence { get; set; }
    }
}
