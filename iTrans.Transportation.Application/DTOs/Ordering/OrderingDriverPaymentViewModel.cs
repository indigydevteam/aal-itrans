﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Application.DTOs.Ordering
{
    public class OrderingDriverPaymentViewModel
    {
        public Guid orderingId { get; set; }
        public decimal debitAmount { get; set; }
        public decimal balanceAmount { get; set; }
    }
}
