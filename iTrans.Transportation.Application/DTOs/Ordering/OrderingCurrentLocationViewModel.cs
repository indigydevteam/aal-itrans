﻿using iTrans.Transportation.Application.DTOs.Customer;
using iTrans.Transportation.Application.DTOs.Driver;
using iTrans.Transportation.Application.DTOs.OrderingAddress;
using iTrans.Transportation.Application.DTOs.OrderingCar;
using iTrans.Transportation.Application.DTOs.OrderingContainer;
//using iTrans.Transportation.Application.DTOs.OrderingContainerAddress;
using iTrans.Transportation.Application.DTOs.OrderingDriver;
using iTrans.Transportation.Application.DTOs.OrderingDriverReserve;
using iTrans.Transportation.Application.DTOs.OrderingInsurance;
using iTrans.Transportation.Application.DTOs.OrderingProduct;
using iTrans.Transportation.Application.DTOs.OrderingStatus;
using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Application.DTOs.Ordering
{
    public class OrderingCurrentLocationViewModel
    {
        public string currentLocation { get; set; }
        public string distance { get; set; }
        public string estimateTime { get; set; }
    }
}
