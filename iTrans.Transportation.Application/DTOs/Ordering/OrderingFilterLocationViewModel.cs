﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Application.DTOs.Ordering
{
    public class OrderingFilterLocationViewModel
    {
        public int CountryId { get; set; }
        public int? RegionId { get; set; }
        public int? ProvinceId { get; set; }
        public int?[] DistrictIds { get; set; }
    }
}
