﻿using iTrans.Transportation.Application.DTOs.Customer;
using iTrans.Transportation.Application.DTOs.Driver;
using iTrans.Transportation.Application.DTOs.DriverAnnouncement;
using iTrans.Transportation.Application.DTOs.OrderingAddress;
using iTrans.Transportation.Application.DTOs.OrderingCar;
using iTrans.Transportation.Application.DTOs.OrderingContainer;
//using iTrans.Transportation.Application.DTOs.OrderingContainerAddress;
using iTrans.Transportation.Application.DTOs.OrderingDriver;
using iTrans.Transportation.Application.DTOs.OrderingDriverReserve;
using iTrans.Transportation.Application.DTOs.OrderingInsurance;
using iTrans.Transportation.Application.DTOs.OrderingProduct;
using iTrans.Transportation.Application.DTOs.OrderingStatus;
using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Application.DTOs.Ordering
{
    public class OrderingUpdateViewModel
    {
        public int transportType { get; set; }
        public float productTotalWeight { get; set; }
        public int productCBM { get; set; }
        public decimal orderingDesiredPrice { get; set; }
        public decimal orderingDriverOffering { get; set; }
        public bool isOrderingDriverOffer { get; set; }
        public string additionalDetail { get; set; }
        public string note { get; set; }
    }
}
