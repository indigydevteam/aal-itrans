﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Application.DTOs.Ordering
{
    public class OrderingFilterProductViewModel
    {
        public int[] ProductTypeId { get; set; }
        public int[] PackagingId { get; set; }
    }
}
