﻿using iTrans.Transportation.Application.DTOs.Driver;
using iTrans.Transportation.Application.DTOs.DriverAnnouncement;
using iTrans.Transportation.Application.DTOs.OrderingAddress;
using iTrans.Transportation.Application.DTOs.OrderingCar;
using iTrans.Transportation.Application.DTOs.OrderingContainer;
//using iTrans.Transportation.Application.DTOs.OrderingContainerAddress;
using iTrans.Transportation.Application.DTOs.OrderingDriver;
using iTrans.Transportation.Application.DTOs.OrderingDriverReserve;
using iTrans.Transportation.Application.DTOs.OrderingInsurance;
using iTrans.Transportation.Application.DTOs.OrderingProduct;
using iTrans.Transportation.Application.DTOs.OrderingStatus;
using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Application.DTOs.Ordering
{
   public class OrderingWithDriverViewModel
    {
        public Guid id { get; set; }
        public string trackingCode { get; set; }
        public bool isRequestTax { get; set; }
        public int transportType { get; set; }
        public string orderNumber { get; set; }
        //public virtual DriverViewModel Driver { get; set; }
        public Guid customerId { get; set; }
        public float productTotalWeight { get; set; }
        public int productCBM { get; set; }
        public decimal orderingPrice { get; set; }
        public decimal orderingDesiredPrice { get; set; }
        public string additionalDetail { get; set; }
        public string note { get; set; }
        public int status { get; set; }
        public OrderingStatusViewModel statusObj { get; set; }
        public string statusReason { get; set; }
        public int customerRanking { get; set; }
        public int driverRanking { get; set; }
        public string totalDistance { get; set; }
        public string totalEstimateTime { get; set; }
        public string distance { get; set; }
        public string estimateTime { get; set; }
        public DateTime pinnedDate { get; set; }
        public bool isDriverPay { get; set; }
        public string currentLocation { get; set; }
        public bool isMutipleRoutes { get; set; }
        public List<OrderingDriverViewModel> drivers { get; set; }
        public List<OrderingProductViewModel> products { get; set; }
        public List<OrderingInsuranceViewModel> insurances { get; set; }
        public List<OrderingCarViewModel> cars { get; set; }
        public List<OrderingAddressViewModel> addresses { get; set; }
        public List<OrderingContainerViewModel> containers { get; set; }
        public List<OrderingDriverReserveViewModel> driverReserve { get; set; }
        public DriverAnnouncementViewModel driverAnnouncement { get; set; }

    }
}
