﻿using iTrans.Transportation.Application.DTOs.OrderingStatus;
using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Application.DTOs.Ordering.Backoffice
{
 public  class OrderingPaymentViewModel
    {
        public virtual int Id { get; set; }
        //public virtual OrderingViewModel Ordering { get; set; }
        public virtual string orderingId { get; set; }
        public virtual string trackingCode { get; set; }
        public virtual OrderingStatusViewModel statusObj { get; set; }
        public virtual string channel { get; set; }
        public virtual string customer { get; set; }
        public virtual decimal Amount { get; set; }
        public virtual string Description { get; set; }
        public virtual string statusName { get; set; }
        public virtual string statusColor { get; set; }
        public virtual string created { get; set; }

    }
}
