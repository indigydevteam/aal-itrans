﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Application.DTOs.Ordering.Backoffice
{
   public class OrderingAddressPopularViewModel
    {
        
        public int id { get; set; }
        public string name { get; set; }
        public int count { get; set; }
        public decimal percent { get; set; }
    }
}
