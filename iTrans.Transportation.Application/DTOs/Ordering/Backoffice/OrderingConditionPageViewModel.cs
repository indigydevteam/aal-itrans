﻿using iTrans.Transportation.Application.DTOs.Customer;
using iTrans.Transportation.Application.DTOs.Driver;
using iTrans.Transportation.Application.DTOs.OrderingAddress;
using iTrans.Transportation.Application.DTOs.OrderingCar;
using iTrans.Transportation.Application.DTOs.OrderingContainer;
//using iTrans.Transportation.Application.DTOs.OrderingContainerAddress;
using iTrans.Transportation.Application.DTOs.OrderingDriver;
using iTrans.Transportation.Application.DTOs.OrderingDriverReserve;
using iTrans.Transportation.Application.DTOs.OrderingInsurance;
using iTrans.Transportation.Application.DTOs.OrderingProduct;
using iTrans.Transportation.Application.DTOs.OrderingStatus;
using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Application.DTOs.Ordering.Backoffice
{
   public class OrderingConditionPageViewModel
    {
        public  Guid id { get; set; }
        public  string trackingCode { get; set; }
        public  string customerName { get; set; }
        public string statusName { get; set; }
        public string statusColor { get; set; }
        public string carRegistration { get; set; }
        public string driverName { get; set; }
        public string recipientnName { get; set; }
        public string pickupPoint { get; set; }
        public string pickupPointDate { get; set; }
        public string deliveryPoint { get; set; }
        public string deliveryPointDate { get; set; }
        public string distance { get; set; }
        public string orderingPrice { get; set; }

    }
}
