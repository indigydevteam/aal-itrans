﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Application.DTOs.Ordering.Backoffice
{
   public class CountOrderingByTypeVeiwModel
    {
        public int orderingsuccess { get; set; }
        public int orderingworking { get; set; }
        public int orderingfailed { get; set; }
    }
}
