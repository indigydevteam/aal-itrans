﻿using iTrans.Transportation.Application.DTOs.CarList;
using iTrans.Transportation.Application.DTOs.CarType;
using iTrans.Transportation.Application.DTOs.OrderingAddress;
using iTrans.Transportation.Application.DTOs.OrderingProduct;
using iTrans.Transportation.Application.DTOs.OrderingStatus;
using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Application.DTOs.Ordering
{
    public class OrderingNotificationViewModel
    {
        public Guid id { get; set; }
        public int status { get; set; }
        public OrderingStatusViewModel statusObj { get; set; }
        public string orderNumber { get; set; }
        public string trackingCode { get; set; }
        public CarTypeViewModel carType { get; set; }
        public CarListViewModel carList { get; set; }

        public List<OrderingProductNotificationViewModel> products { get; set; }
        public OrderingAddressNotificationViewModel startPoint { get; set; }
        public OrderingAddressNotificationViewModel endPoint { get; set; }
    }
}
