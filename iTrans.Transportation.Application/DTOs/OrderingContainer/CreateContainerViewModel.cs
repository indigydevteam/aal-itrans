﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace iTrans.Transportation.Application.DTOs.OrderingContainer
{
   public class CreateContainerViewModel
    {
        public int Id { get; set; }
        //public Guid OrderingId { get; set; }
        public Guid OrderingId { get; set; }
        public string Section { get; set; }
        public string ContainerBooking { get; set; }
        public int ContainerTypeId { get; set; }
        public int ContainerSpecificationId { get; set; }
        public string Temperature { get; set; }
        public string Moisture { get; set; }
        public string Ventilation { get; set; }
        public int EnergySavingDeviceId { get; set; }
        public string SpecialSpecify { get; set; }
        public double Weight { get; set; }
        public string Commodity { get; set; }
        public DateTime DateOfBooking { get; set; }
        public int PickupPointCountryId { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = true)]
        public int? PickupPointProvinceId { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = true)]
        public int? PickupPointDistrictId { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = true)]
        public int? PickupPointSubdistrictId { get; set; }
        public string PickupPointPostCode { get; set; }
        public string PickupPointRoad { get; set; }
        public string PickupPointAlley { get; set; }
        public string PickupPointAddress { get; set; }
        public string PickupPointMaps { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = true)]
        public DateTime? PickupPointDate { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = true)]
        public int? ReturnPointCountryId { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = true)]
        public int? ReturnPointProvinceId { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = true)]
        public int? ReturnPointDistrictId { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = true)]
        public int? ReturnPointSubdistrictId { get; set; }
        public string ReturnPointPostCode { get; set; }
        public string ReturnPointRoad { get; set; }
        public string ReturnPointAlley { get; set; }
        public string ReturnPointAddress { get; set; }
        public string ReturnPointMaps { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = true)]
        public DateTime? ReturnPointDate { get; set; }
        public DateTime CutOffDate { get; set; }
        public string ShippingContact { get; set; }
        public string ShippingContactPhoneCode { get; set; }
        public string ShippingContactPhoneNumber { get; set; }
        public string YardContact { get; set; }
        public string YardContactPhoneCode { get; set; }
        public string YardContactPhoneNumber { get; set; }
        public string LinerContact { get; set; }
        public string LinerContactPhoneCode { get; set; }
        public string LinerContactPhoneNumber { get; set; }
        public string DeliveryOrderNumber { get; set; }
        public string BookingNumber { get; set; }
        public int ContainerAmount { get; set; }
        public string SpecialOrder { get; set; }
        public List<IFormFile> AttachedFiles { get; set; }
    }
}

   