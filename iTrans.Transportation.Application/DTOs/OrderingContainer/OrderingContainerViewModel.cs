﻿using iTrans.Transportation.Application.DTOs.ContainerSpecification;
using iTrans.Transportation.Application.DTOs.ContainerType;
using iTrans.Transportation.Application.DTOs.Country;
using iTrans.Transportation.Application.DTOs.District;
using iTrans.Transportation.Application.DTOs.EnergySavingDevice;
using iTrans.Transportation.Application.DTOs.OrderingContainerFile;
using iTrans.Transportation.Application.DTOs.Province;
using iTrans.Transportation.Application.DTOs.Subdistrict;
using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Application.DTOs.OrderingContainer
{
   public class OrderingContainerViewModel
    {
        public int id { get; set; }
        //public Guid OrderingId { get; set; }
        //public Ordering Ordering { get; set; }
        public string section { get; set; }
        public string containerBooking { get; set; }
        public ContainerTypeViewModel containerType { get; set; }
        public ContainerSpecificationViewModel containerSpecification { get; set; }
        public string temperature { get; set; }
        public string moisture { get; set; }
        public string ventilation { get; set; }
        public EnergySavingDeviceViewModel energySavingDevice { get; set; }
        public string specialSpecify { get; set; }
        public double weight { get; set; }
        public string commodity { get; set; }
        public DateTime dateOfBooking { get; set; }
        public CountryViewModel pickupPointCountry { get; set; }
        public ProvinceViewModel pickupPointProvince { get; set; }
        public DistrictViewModel pickupPointDistrict { get; set; }
        public SubdistrictViewModel pickupPointSubdistrict { get; set; }
        public string pickupPointPostCode { get; set; }
        public string pickupPointRoad { get; set; }
        public string pickupPointAlley { get; set; }
        public string pickupPointAddress { get; set; }
        public string pickupPointMaps { get; set; }
        public DateTime pickupPointDate { get; set; }
        public CountryViewModel returnPointCountry { get; set; }
        public ProvinceViewModel returnPointProvince { get; set; }
        public DistrictViewModel returnPointDistrict { get; set; }
        public SubdistrictViewModel returnPointSubdistrict { get; set; }
        public string returnPointPostCode { get; set; }
        public string returnPointRoad { get; set; }
        public string returnPointAlley { get; set; }
        public string returnPointAddress { get; set; }
        public string returnPointMaps { get; set; }
        public DateTime returnPointDate { get; set; }
        public DateTime cutOffDate { get; set; }
        public string shippingContact { get; set; }
        public string shippingContactPhoneCode { get; set; }
        public string shippingContactPhoneNumber { get; set; }
        public string yardContact { get; set; }
        public string yardContactPhoneCode { get; set; }
        public string yardContactPhoneNumber { get; set; }
        public string linerContact { get; set; }
        public string linerContactPhoneCode { get; set; }
        public string linerContactPhoneNumber { get; set; }
        public string deliveryOrderNumber { get; set; }
        public string bookingNumber { get; set; }
        public int containerAmount { get; set; }
        public string specialOrder { get; set; }
        public List<OrderingContainerFileViewModel> files { get; set; }
    }
}

   