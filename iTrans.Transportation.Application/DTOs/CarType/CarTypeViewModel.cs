﻿using iTrans.Transportation.Application.DTOs.CarTypeFile;
using iTrans.Transportation.Application.DTOs.CountryFile;
using System.Collections.Generic;

namespace iTrans.Transportation.Application.DTOs.CarType
{
    public class CarTypeViewModel
    {
        public int id { get; set; }
        public string name_TH { get; set; }
        public string name_ENG { get; set; }
        public bool isTemperatureInfo { get; set; }
        public bool isContainer { get; set; }
        public int sequence { get; set; }
        public bool active { get; set; }
        public CarTypeFileViewModel File { get; set; }
    }
}
