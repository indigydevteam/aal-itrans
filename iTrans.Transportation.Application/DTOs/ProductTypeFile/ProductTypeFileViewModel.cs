﻿using System;
namespace iTrans.Transportation.Application.DTOs.ProductTypeFile
{
    public class ProductTypeFileViewModel
    {
        public int id { get; set; }
        public int productTypeId { get; set; }
        public string fileName { get; set; }
        public string contentType { get; set; }
        public string filePath { get; set; }
        public string fileEXT { get; set; }
    }
}
