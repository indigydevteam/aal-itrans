﻿using iTrans.Transportation.Application.DTOs.Country;
using iTrans.Transportation.Application.DTOs.District;
using iTrans.Transportation.Application.DTOs.OrderingAddressFile;
//using iTrans.Transportation.Application.DTOs.OrderingAddressFile;
using iTrans.Transportation.Application.DTOs.OrderingAddressProduct;
using iTrans.Transportation.Application.DTOs.OrderingProduct;
using iTrans.Transportation.Application.DTOs.OrderingStatus;
using iTrans.Transportation.Application.DTOs.Province;
using iTrans.Transportation.Application.DTOs.Subdistrict;
using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Application.DTOs.OrderingAddress
{
    public class AddressTrackingCodeViewModel
    {
        public virtual int Id { get; set; }
        public virtual Guid OrderingId { get; set; }
        public virtual string TrackingCode { get; set; }
        public virtual string PersonalName { get; set; }
        public virtual string PhoneCode { get; set; }
        public virtual string PhoneNumber { get; set; }
        public virtual string Email { get; set; }
        public virtual CountryViewModel Country { get; set; }
        public virtual ProvinceViewModel Province { get; set; }
        public virtual DistrictViewModel District { get; set; }
        public virtual SubdistrictViewModel Subdistrict { get; set; }
        public virtual string PostCode { get; set; }
        public virtual string Road { get; set; }
        public virtual string Alley { get; set; }
        public virtual string Address { get; set; }
        public virtual string Branch { get; set; }
        public virtual string AddressType { get; set; }
        public virtual string AddressName { get; set; }
        public virtual DateTime Date { get; set; }
        public virtual string Maps { get; set; }
        public virtual int Sequence { get; set; }
        public virtual string Note { get; set; }
        public virtual string Status { get; set; }
        public virtual OrderingStatusViewModel statusObj { get; set; }
        public virtual string StatusReason { get; set; }
        public virtual DateTime? DriverWaitingToPickUpDate { get; set; }
        public virtual DateTime? DriverWaitingToDeliveryDate { get; set; }
        public virtual string UploadNote { get; set; }
        public virtual IList<OrderingAddressProductViewModel> Products { get; set; }
        public virtual IList<OrderingAddressFileViewModel> Files { get; set; }
    }

}