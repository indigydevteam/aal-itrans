﻿using iTrans.Transportation.Application.DTOs.Country;
using iTrans.Transportation.Application.DTOs.District;
using iTrans.Transportation.Application.DTOs.OrderingAddressFile;
//using iTrans.Transportation.Application.DTOs.OrderingAddressFile;
using iTrans.Transportation.Application.DTOs.OrderingAddressProduct;
using iTrans.Transportation.Application.DTOs.OrderingProduct;
using iTrans.Transportation.Application.DTOs.OrderingStatus;
using iTrans.Transportation.Application.DTOs.Province;
using iTrans.Transportation.Application.DTOs.Subdistrict;
using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Application.DTOs.OrderingAddress
{
    public class OrderingAddressViewModel
    {
        public int id { get; set; }
        public string trackingCode { get; set; }
        public Guid orderingId { get; set; }
        //public OrderingProductViewModel OrderingProduct { get; set; }
        public string personalName { get; set; }
        public string phoneCode { get; set; }
        public string phoneNumber { get; set; }
        public string email { get; set; }
        public CountryViewModel country { get; set; }
        public ProvinceViewModel province { get; set; }
        public DistrictViewModel district { get; set; }
        public SubdistrictViewModel subdistrict { get; set; }
        public string postCode { get; set; }
        public string road { get; set; }
        public string alley { get; set; }
        public string address { get; set; }
        public string branch { get; set; }
        public string addressType { get; set; }
        public string addressName { get; set; }
        public DateTime date { get; set; }
        public string maps { get; set; }
        public int sequence { get; set; }
        public string note { get; set; }
        public int status { get; set; }
        public OrderingStatusViewModel statusObj { get; set; }
        public string statusReason { get; set; }
        public DateTime? driverWaitingToPickUpDate { get; set; }
        public DateTime? driverWaitingToDeliveryDate { get; set; }
        public string uploadNote { get; set; }
        public IList<OrderingAddressProductViewModel> products { get; set; }
        public IList<OrderingAddressFileViewModel> files { get; set; }
        public decimal compensation { get; set; }
        public DateTime Modified { get; set; }
        public string ModifiedBy { get; set; }

    }

}