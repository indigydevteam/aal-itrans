﻿using iTrans.Transportation.Application.DTOs.Country;
using iTrans.Transportation.Application.DTOs.District;
using iTrans.Transportation.Application.DTOs.OrderingAddressFile;
//using iTrans.Transportation.Application.DTOs.OrderingAddressFile;
using iTrans.Transportation.Application.DTOs.OrderingAddressProduct;
using iTrans.Transportation.Application.DTOs.OrderingProduct;
using iTrans.Transportation.Application.DTOs.Province;
using iTrans.Transportation.Application.DTOs.Subdistrict;
using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Application.DTOs.OrderingAddress
{
    public class OrderingCompensationViewModel
    {
        public virtual int id { get; set; }
        public virtual Guid orderingId { get; set; }
        public virtual string trackingCode { get; set; }
        public virtual decimal Compensation { get; set; }
        public virtual string PeriodTime { get; set; }
    }

}