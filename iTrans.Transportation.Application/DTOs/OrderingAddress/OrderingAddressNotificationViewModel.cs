﻿using iTrans.Transportation.Application.DTOs.Country;
using iTrans.Transportation.Application.DTOs.District;
using iTrans.Transportation.Application.DTOs.OrderingAddressFile;
//using iTrans.Transportation.Application.DTOs.OrderingAddressFile;
using iTrans.Transportation.Application.DTOs.OrderingAddressProduct;
using iTrans.Transportation.Application.DTOs.OrderingProduct;
using iTrans.Transportation.Application.DTOs.Province;
using iTrans.Transportation.Application.DTOs.Subdistrict;
using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Application.DTOs.OrderingAddress
{
    public class OrderingAddressNotificationViewModel
    {
        public virtual int id { get; set; }
        public virtual string trackingCode { get; set; }
        public virtual CountryViewModel country { get; set; }
        public virtual ProvinceViewModel province { get; set; }
        public virtual DistrictViewModel district { get; set; }
        public virtual SubdistrictViewModel subdistrict { get; set; }
        public virtual string postCode { get; set; }
        public virtual string road { get; set; }
        public virtual string alley { get; set; }
        public virtual string address { get; set; }
    }

}