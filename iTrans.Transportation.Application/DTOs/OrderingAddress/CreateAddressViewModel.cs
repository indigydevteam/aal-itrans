﻿using iTrans.Transportation.Application.DTOs.OrderingAddressProduct;
using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Application.DTOs.OrderingAddress
{
    public class CreateAddressViewModel
    {
        public virtual string AddressType { get; set; }
        public virtual string PersonalName { get; set; }
        public virtual string PhoneCode { get; set; }
        public virtual string PhoneNumber { get; set; }
        public virtual string Email { get; set; }
        public virtual int CountryId { get; set; }
        public virtual int ProvinceId { get; set; }
        public virtual int DistrictId { get; set; }
        public virtual int SubdistrictId { get; set; }
        public virtual string PostCode { get; set; }
        public virtual string Road { get; set; }
        public virtual string Alley { get; set; }
        public virtual string Address { get; set; }
        public virtual string Maps { get; set; }
        public virtual DateTime Date { get; set; }
        public virtual int Sequence { get; set; }
        public virtual string Note { get; set; }
        public virtual List<CreateProductFormViewModel> Products { get; set; }
    }
}
