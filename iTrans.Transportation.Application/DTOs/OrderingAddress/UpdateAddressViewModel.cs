﻿using iTrans.Transportation.Application.DTOs.OrderingAddressProduct;
using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Application.DTOs.OrderingAddress
{
    public class UpdateAddressViewModel
    {
        public int Id { get; set; }
        public string AddressType { get; set; }
        public string PersonalName { get; set; }
        public string PhoneCode { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public int CountryId { get; set; }
        public int ProvinceId { get; set; }
        public int DistrictId { get; set; }
        public int SubdistrictId { get; set; }
        public string PostCode { get; set; }
        public string Road { get; set; }
        public string Alley { get; set; }
        public string Address { get; set; }
        public string Maps { get; set; }
        public DateTime Date { get; set; }
        public int Sequence { get; set; }
        public string Note { get; set; }
        public List<int> DeleteProductId { get; set; }
        public List<UpdateProductFormViewModel> Products { get; set; }
    }
}
