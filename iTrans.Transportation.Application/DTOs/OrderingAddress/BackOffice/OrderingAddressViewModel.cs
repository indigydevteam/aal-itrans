﻿using iTrans.Transportation.Application.DTOs.Country;
using iTrans.Transportation.Application.DTOs.District;
using iTrans.Transportation.Application.DTOs.OrderingAddressFile;
//using iTrans.Transportation.Application.DTOs.OrderingAddressFile;
using iTrans.Transportation.Application.DTOs.OrderingAddressProduct;
using iTrans.Transportation.Application.DTOs.OrderingProduct;
using iTrans.Transportation.Application.DTOs.OrderingStatus;
using iTrans.Transportation.Application.DTOs.Province;
using iTrans.Transportation.Application.DTOs.Subdistrict;
using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Application.DTOs.OrderingAddress.BackOffice
{
    public class OrderingAddressViewModel
    {
        public virtual int id { get; set; }
        public virtual string trackingCode { get; set; }
        public virtual Guid orderingId { get; set; }
        //public virtual OrderingProductViewModel OrderingProduct { get; set; }
        public virtual string personalName { get; set; }
        public virtual string phoneCode { get; set; }
        public virtual string phoneNumber { get; set; }
        public virtual string email { get; set; }
        public virtual CountryViewModel country { get; set; }
        public virtual ProvinceViewModel province { get; set; }
        public virtual DistrictViewModel district { get; set; }
        public virtual SubdistrictViewModel subdistrict { get; set; }
        public virtual string postCode { get; set; }
        public virtual string road { get; set; }
        public virtual string alley { get; set; }
        public virtual string address { get; set; }
        public virtual string branch { get; set; }
        public virtual string addressType { get; set; }
        public virtual string addressName { get; set; }
        public virtual DateTime date { get; set; }
        public virtual string maps { get; set; }
        public virtual int sequence { get; set; }
        public virtual string note { get; set; }
        public virtual int status { get; set; }
        public virtual OrderingStatusViewModel statusObj { get; set; }
        public virtual string statusReason { get; set; }
        public virtual DateTime? driverWaitingToPickUpDate { get; set; }
        public virtual DateTime? driverWaitingToDeliveryDate { get; set; }
        public virtual string uploadNote { get; set; }
        //public List<OrderingProductViewModel> products { get; set; }
        public virtual IList<OrderingAddressProductViewModel> products { get; set; }
        public virtual IList<OrderingAddressFileViewModel> files { get; set; }
        public virtual decimal compensation { get; set; }
        public virtual decimal actualreceive { get; set; }

    }

}