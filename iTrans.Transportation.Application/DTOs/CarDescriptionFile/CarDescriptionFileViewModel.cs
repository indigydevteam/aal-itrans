﻿using System;
namespace iTrans.Transportation.Application.DTOs.CarDescriptionFile
{
    public class CarDescriptionFileViewModel
    {
        public int id { get; set; }
        public int carDescriptionId { get; set; }
        public string fileName { get; set; }
        public string contentType { get; set; }
        public string filePath { get; set; }
        public string fileEXT { get; set; }
    }
}
