﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Application.DTOs.CustomerPaymentTransaction
{
   public class CustomerPaymentTransactionViewModel
    {
        public virtual int Id { get; set; }
        public virtual int Customer_PaymentHistoryId { get; set; }
        public virtual string Detail { get; set; }
    }
}
