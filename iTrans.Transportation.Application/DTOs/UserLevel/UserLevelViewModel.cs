﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Application.DTOs.UserLevel
{
    public class UserLevelViewModel
    {
        public int id { get; set; }
        public string module { get; set; }
        public int level { get; set; }
        public string name_TH { get; set; }
        public string name_ENG { get; set; }
        public int star { get; set; }
        public bool active { get; set; }
    }
    public class UserLevelViewModelConditionWithPage
    {
        public int id { get; set; }
        public string module { get; set; }
        public int level { get; set; }
        public string name_TH { get; set; }
        public string name_ENG { get; set; }
        public int star { get; set; }
        public bool active { get; set; }
    }
}
