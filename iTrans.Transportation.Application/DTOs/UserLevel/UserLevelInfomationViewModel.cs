﻿using iTrans.Transportation.Application.DTOs.UserLevelCondition;
using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Application.DTOs.UserLevel
{
   public class UserLevelInformationViewModel
    {
        public virtual int id { get; set; }
        public virtual string module { get; set; }
        public virtual int level { get; set; }
        public virtual string name_TH { get; set; }
        public virtual string name_ENG { get; set; }
        public virtual int star { get; set; }
        public virtual List<UserLevelConditionViewModel> conditions { get; set; }
    }


}
