﻿using iTrans.Transportation.Application.DTOs.UserLevelCondition;
using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Application.DTOs.Backoffice.UserLevel
{
    public class UserLevelInformationViewModel
    {
        public int id { get; set; }
        public string module { get; set; }
        public int level { get; set; }
        public string name_TH { get; set; }
        public string name_ENG { get; set; }
        public int star { get; set; }
        public bool active { get; set; }
        public int customerStar { get; set; }
        public int customerRequestCarPerWeek { get; set; }
        public int customerRequestCarPerMonth { get; set; }
        public int customerRequestCarPerYear { get; set; }
        public int customerCancelPerWeek { get; set; }
        public int customerCancelPerMonth { get; set; }
        public int customerCancelPerYear { get; set; }
        public decimal customerOrderingValuePerWeek { get; set; }
        public decimal customerOrderingValuePerMonth { get; set; }
        public decimal customerOrderingValuePerYear { get; set; }
        public decimal customerDiscount { get; set; }
        public decimal customerFine { get; set; }
        
        
        public int driverStar { get; set; }
        public int driverAcceptJobPerWeek { get; set; }
        public int driverAcceptJobPerMonth { get; set; }
        public int driverAcceptJobPerYear { get; set; }
        public int driverComplaintPerWeek { get; set; }
        public int driverComplaintPerMonth { get; set; }
        public int driverComplaintPerYear { get; set; }
        public int driverRejectPerWeek { get; set; }
        public int driverRejectPerMonth { get; set; }
        public int driverRejectPerYear { get; set; }
        public int driverCancelPerWeek { get; set; }
        public int driverCancelPerMonth { get; set; }
        public int driverCancelPerYear { get; set; }
        public decimal driverInsuranceValue { get; set; }
        public decimal driverCommission { get; set; }
        public decimal driverFine { get; set; }
        public decimal driverDiscount { get; set; }
        public bool isDelete { get; set; }
        public DateTime created { get; set; }
        public DateTime modified { get; set; }
        public List<UserLevelConditionViewModel> conditions { get; set; }
    }
}
