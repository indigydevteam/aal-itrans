﻿using iTrans.Transportation.Application.DTOs.Customer.Backoffice;
using iTrans.Transportation.Application.DTOs.ProblemTopic;
using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Application.DTOs.CustomerProblem
{
   public class CustomerProblemViewModel
    {
        public virtual int Id { get; set; }
        public virtual CustomerViewModel Customer { get; set; }

        public virtual ProblemTopicViewModel ProblemTopic { get; set; }
        public virtual string Message { get; set; }
        public virtual string Email { get; set; }
        public string Module { get; set; }
        public string Created { get; set; }
        public string Modified { get; set; }

    }
}
