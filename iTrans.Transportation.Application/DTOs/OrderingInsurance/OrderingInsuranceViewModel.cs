﻿using iTrans.Transportation.Application.DTOs.Ordering;
using iTrans.Transportation.Application.DTOs.ProductType;
using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Application.DTOs.OrderingInsurance
{
  public  class OrderingInsuranceViewModel
    {
        public virtual int id { get; set; }
        // public virtual OrderingViewModel Ordering { get; set; }
        public virtual Guid orderingId { get; set; }
        public virtual string productName { get; set; }
        public virtual ProductTypeViewModel productType { get; set; }
        public virtual string warrantyPeriod { get; set; }
        public virtual decimal insurancePremium { get; set; }
        public virtual decimal insuranceLimit { get; set; }
    }
}
