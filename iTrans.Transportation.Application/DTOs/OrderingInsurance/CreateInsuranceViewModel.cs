﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Application.DTOs.OrderingInsurance
{
  public  class CreateInsuranceViewModel
    {
        public virtual int ProductTypeId { get; set; }
        public virtual string ProductName { get; set; }
        public virtual string WarrantyPeriod { get; set; }
        public virtual decimal InsurancePremium { get; set; }
        public virtual decimal InsuranceLimit { get; set; }
    }
}
