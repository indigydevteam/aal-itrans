﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Application.DTOs.Registers
{
   public class CreateTermAndConditionViewModel
    {
        public int Id { get; set; }
        public int TermAndConditionId { get; set; }
        public string Name_TH { get; set; }
        public string Name_ENG { get; set; }
        public string Section { get; set; }
        public string Version { get; set; }
        public bool IsAccept { get; set; }
        public bool IsUserAccept { get; set; }
    }
}
