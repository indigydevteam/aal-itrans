﻿using System;

namespace iTrans.Transportation.Application.DTOs.SystemMenu
{
    public class SystemMenuViewModel
    {
        public virtual int id { get; set; }
        public virtual string name_TH { get; set; }
        public virtual string name_ENG { get; set; }
        public virtual bool active { get; set; }
    }
}
