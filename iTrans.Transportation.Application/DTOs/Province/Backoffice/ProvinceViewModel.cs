﻿using System;

namespace iTrans.Transportation.Application.DTOs.Province.Backoffice
{
    public class ProvinceViewModel
    {
        public virtual int Id { get; set; }
        public virtual int CountryId { get; set; }
        public virtual string CountryName { get; set; }
        public virtual int RegionId { get; set; }
        public virtual string RegionName { get; set; }
        public virtual string Name_TH { get; set; }
        public virtual string Name_ENG { get; set; }
        public virtual bool Active { get; set; }
    }
}
