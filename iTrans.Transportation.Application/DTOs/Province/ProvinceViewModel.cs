﻿using System;

namespace iTrans.Transportation.Application.DTOs.Province
{
    public class ProvinceViewModel
    {
        public virtual int id { get; set; }
        public virtual int countryId { get; set; }
        public virtual int regionId { get; set; }
        public virtual string name_TH { get; set; }
        public virtual string name_ENG { get; set; }
        //public virtual bool Active { get; set; }
    }
}
