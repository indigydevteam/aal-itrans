﻿using iTrans.Transportation.Application.DTOs.Country;
using iTrans.Transportation.Application.DTOs.District;
using iTrans.Transportation.Application.DTOs.OrderingStatus;
using iTrans.Transportation.Application.DTOs.Province;
using iTrans.Transportation.Application.DTOs.Subdistrict;
using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Application.DTOs.CustomerFavoriteCar
{
    public class CustomerFavoriteCarHistoryViewModel
    {
        public virtual Guid OrderingId { get; set; }
        public virtual OrderingStatusViewModel OrderingStatus { get; set; }
        public virtual DateTime? startDate { get; set; }
        public virtual DateTime? endDate { get; set; }
        public virtual LocationViewModel startLocation { get; set; }
        public virtual LocationViewModel endLocation { get; set; }
        public virtual decimal price { get; set; }
    }

    public class LocationViewModel
    {
        public virtual CountryViewModel country { get; set; }
        public virtual ProvinceViewModel province { get; set; }
        public virtual DistrictViewModel district { get; set; }
        public virtual SubdistrictViewModel subDistrict { get; set; }
        public virtual string postCode { get; set; }
    }

}
