﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Application.DTOs.CustomerFavoriteCar
{
   public class CustomerFavoriteCarViewModel
    {
        public virtual int Id { get; set; }
        public virtual Guid Customer  { get; set; }
        public virtual Guid Driver  { get; set; }
        public virtual int DriverCar { get; set; }
        public CustomerFavoriteCarHistoryViewModel History { get; set; }
    }
}
