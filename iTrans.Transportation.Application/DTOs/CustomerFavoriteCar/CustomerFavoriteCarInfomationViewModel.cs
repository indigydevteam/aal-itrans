﻿using iTrans.Transportation.Application.DTOs.Driver;
using iTrans.Transportation.Application.DTOs.DriverCar;
using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Application.DTOs.CustomerFavoriteCar
{
  
    public class CustomerFavoriteCarInfomationViewModel
    {
        public int Id { get; set; }
        public Guid CustomerId { get; set; }
        public DriverViewModel Driver { get; set; }
        public DriverCarViewModel Car { get; set; }
        public List<CustomerFavoriteCarHistoryViewModel> History { get; set; }
    }
  
}
