﻿using iTrans.Transportation.Application.DTOs.CarFeatureFile;
using iTrans.Transportation.Application.DTOs.CarListFile;
using iTrans.Transportation.Application.DTOs.CarType;
using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Application.DTOs.CarList
{
   public class CarListInfomationViewModel
    {
        public virtual int Id { get; set; }
        public virtual CarTypeViewModel CarType { get; set; }
        public virtual string Name_TH { get; set; }
        public virtual string Name_ENG { get; set; }
        public virtual int Sequence { get; set; }
        public virtual bool Active { get; set; }
        public CarListFileViewModel file { get; set; }
    }
}
