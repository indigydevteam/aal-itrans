﻿using iTrans.Transportation.Application.DTOs.CarFeatureFile;
using iTrans.Transportation.Application.DTOs.CarListFile;
using iTrans.Transportation.Application.DTOs.CarType;

namespace iTrans.Transportation.Application.DTOs.CarList.Backoffice
{
    public class CarListViewModel
    {
        public virtual int id { get; set; }
        public virtual string carType { get; set; }
        public virtual string name_TH { get; set; }
        public virtual string name_ENG { get; set; }
        public virtual int sequence { get; set; }
        public virtual bool active { get; set; }
        public CarListFileViewModel file { get; set; }
    }
}
