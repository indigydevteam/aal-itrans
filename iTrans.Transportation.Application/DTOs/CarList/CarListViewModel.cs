﻿using iTrans.Transportation.Application.DTOs.CarFeatureFile;
using iTrans.Transportation.Application.DTOs.CarListFile;

namespace iTrans.Transportation.Application.DTOs.CarList
{
    public class CarListViewModel
    {
        public virtual int id { get; set; }
        public virtual int carTypeId { get; set; }
        public virtual string name_TH { get; set; }
        public virtual string name_ENG { get; set; }
        public virtual int sequence { get; set; }
        public virtual bool active { get; set; }
        public CarListFileViewModel file { get; set; }
    }
}
