﻿using iTrans.Transportation.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Application.DTOs.Menu
{
    public class MenuPermissionViewModel
    {
        public int Id { get; set; }
        public int MenuId { get; set; }

        public int MenuPermissionId { get; set; }

        public  string Permission { get; set; }

    }
}
