﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Application.DTOs.Menu
{
    public class MenuViewModel
    {
        public int Id { get; set; }
        public string NameTH { get; set; }
        public string NameEN { get; set; }
    }
}
