﻿using System;

namespace iTrans.Transportation.Application.DTOs.Backoffice.CorporateType
{
    public class CorporateTypeViewModel
    {
        public int id { get; set; }
        public string name_TH { get; set; }
        public string name_ENG { get; set; }
        public int sequence { get; set; }
        public bool active { get; set; }
        public bool isDelete { get; set; }
        public bool specified { get; set; }
        public DateTime created { get; set; }
        public DateTime modified { get; set; }
    }
}
