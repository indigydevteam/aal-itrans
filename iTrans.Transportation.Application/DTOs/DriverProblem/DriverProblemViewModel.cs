﻿using System;
using System.Collections.Generic;

namespace iTrans.Transportation.Application.DTOs.DriverProblem
{
    public class DriverProblemViewModel
    {
        public virtual int id { get; set; }
        public virtual Guid problemTopicId { get; set; }
        public virtual string message { get; set; }
        public virtual string email { get; set; }
        public string created { get; set; }
        public string modified { get; set; }
    }
}
