﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Application.DTOs.Role
{
    public class RoleViewModel
    {
        public int Id { get; set; }
        public string NameTH { get; set; }
        public string NameEN { get; set; }
        public int Countmenu { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? Modified { get; set; }
    }
}
