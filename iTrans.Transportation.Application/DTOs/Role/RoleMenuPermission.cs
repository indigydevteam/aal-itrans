﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Application.DTOs.Role
{
    public class RoleMenuPermission
    {
        public int Id { get; set; }
        public int RoleId { get; set; }
        public int MenuId { get; set; }
        public int MenuPermissionId { get; set; }
        public string Permission { get; set; }
        public bool Is_Check { get; set; }
    }
}
