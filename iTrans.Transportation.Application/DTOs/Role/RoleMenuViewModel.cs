﻿using iTrans.Transportation.Application.DTOs.Menu;
using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Application.DTOs.Role
{
    public class RoleMenuViewModel
    {
        public int Id { get; set; }
        public string NameTH { get; set; }
        public string NameEN { get; set; }

        public List<RoleMenuPermission> RoleMenuPermission { get; set; }

    } 
    public class RoleMenuDataViewModel
    {
        public  int Id { get; set; }
        public  int RoleId { get; set; }
        public  int MenuId { get; set; }
        public int MenuPermissionId { get; set; }

        public virtual MenuPermissionViewModel MenuPermission { get; set; }

    }
}
