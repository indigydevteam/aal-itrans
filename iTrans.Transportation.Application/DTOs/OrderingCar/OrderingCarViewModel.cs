﻿using iTrans.Transportation.Application.DTOs.CarDescription;
using iTrans.Transportation.Application.DTOs.CarFeature;
using iTrans.Transportation.Application.DTOs.CarList;
using iTrans.Transportation.Application.DTOs.CarSpecification;
using iTrans.Transportation.Application.DTOs.CarType;
using iTrans.Transportation.Application.DTOs.EnergySavingDevice;
using iTrans.Transportation.Application.DTOs.OrderingCarFile;
//using iTrans.Transportation.Application.DTOs.OrderingCarDriver;
using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Application.DTOs.OrderingCar
{
    public class OrderingCarViewModel
    {
        public int id { get; set; }
        public Guid orderingId { get; set; }
        public Guid driverId { get; set; }
        public int driverCarId { get; set; }
        public string carRegistration { get; set; }
        public DateTime actExpiry { get; set; }
        //public virtual int CarTypeId { get; set; }
        public CarTypeViewModel carType { get; set; }
        ///public virtual int CarListId { get; set;}
        public CarListViewModel carList { get; set; }
        //public virtual int CarDescriptionId { get; set; }
        public CarDescriptionViewModel carDescription { get; set; }
        public string carDescriptionDetail { get; set; }
        //public virtual int CarFeatureId { get; set; }
        public CarFeatureViewModel carFeature { get; set; }
        public string carFeatureDetail { get; set; }
        //public virtual int CarSpecificationId { get; set; }
        public CarSpecificationViewModel carSpecification { get; set; }
        public string carSpecificationDetail { get; set; }
        public int width { get; set; }
        public int length { get; set; }
        public int height { get; set; }
        public float temperature { get; set; }
        public EnergySavingDeviceViewModel energySavingDevice { get; set; }
        public string driverName { get; set; }
        public string driverPhoneCode { get; set; }
        public string driverPhoneNumber { get; set; }
        public bool productInsurance { get; set; }
        public decimal productInsuranceAmount { get; set; }
        public string note { get; set; }
        public bool isCharter { get; set; }
        public bool isRequestTax { get; set; }
        public int transportType { get; set; }
        public List<OrderingCarFileViewModel> carFiles { get; set; }
    }
}