﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Application.DTOs.OrderingCar
{
   public class CreateCarViweModel
    {
        public string CarRegistration { get; set; }
        public DateTime ActExpiry { get; set; }
        public int CarTypeId { get; set; }
        public int CarListId { get; set; }
        public int CarDescriptionId { get; set; }
        public string CarDescriptionDetail { get; set; }
        public int CarFeatureId { get; set; }
        public string CarFeatureDetail { get; set; }
        public int CarSpecificationId { get; set; }
        public string CarSpecificationDetail { get; set; }
        public int Width { get; set; }
        public int Length { get; set; }
        public int Height { get; set; }
        public float Temperature { get; set; }
        public int EnergySavingDeviceId { get; set; }
        public string DriverName { get; set; }
        public string DriverPhoneCode { get; set; }
        public string DriverPhoneNumber { get; set; }
        public bool ProductInsurance { get; set; }
        public decimal ProductInsuranceAmount { get; set; }
        public string Note { get; set; }
        public bool IsCharter { get; set; }
        public bool IsRequestTax { get; set; }
        public int TransportType { get; set; }
        public List<IFormFile> files { get; set; }
    }
}