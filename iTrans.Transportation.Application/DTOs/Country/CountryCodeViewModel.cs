﻿using iTrans.Transportation.Application.DTOs.CountryFile;
using System;
using System.Collections.Generic;

namespace iTrans.Transportation.Application.DTOs.Country
{
    public class CountryCodeViewModel
    {
       // public virtual int Id { get; set; }
        public string code { get; set; }
        // public virtual bool Active { get; set; }
        public CountryFileViewModel file { get; set; }
    }
}
