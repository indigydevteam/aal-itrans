﻿using iTrans.Transportation.Application.DTOs.CountryFile;
using System;
using System.Collections.Generic;

namespace iTrans.Transportation.Application.DTOs.Country
{
    public class CountryViewModel
    {
        public int id { get; set; }
        public string name_TH { get; set; }
        public string name_ENG { get; set; }
        public string code { get; set; }
        public CountryFileViewModel file { get; set; }
    }
}
