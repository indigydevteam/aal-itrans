﻿using iTrans.Transportation.Application.DTOs.CountryFile;
using System;
using System.Collections.Generic;

namespace iTrans.Transportation.Application.DTOs.Country.Backoffice
{
    public class CountryViewModel
    {
        public int Id { get; set; }
        public string Name_TH { get; set; }
        public string Name_ENG { get; set; }
        public string Code { get; set; }
        public bool Active { get; set; }
        public CountryFileViewModel File { get; set; }
    }
}
