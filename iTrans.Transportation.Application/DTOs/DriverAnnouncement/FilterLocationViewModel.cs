﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Application.DTOs.DriverAnnouncement
{
    public class FilterLocationViewModel
    {
        public virtual int CountryId { get; set; }
        public virtual int ProvinceId { get; set; }
        public virtual int DistrictId { get; set; }
        public virtual int SubdistrictId { get; set; }
    }
}
