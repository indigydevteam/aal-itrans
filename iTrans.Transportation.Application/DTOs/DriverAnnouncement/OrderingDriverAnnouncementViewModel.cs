﻿using iTrans.Transportation.Application.DTOs.Driver;
using iTrans.Transportation.Application.DTOs.DriverAnnouncementLocation;
using iTrans.Transportation.Application.DTOs.DriverAnnouncementStatus;
using iTrans.Transportation.Application.DTOs.ProductPackaging;
using iTrans.Transportation.Application.DTOs.ProductType;
using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Application.DTOs.DriverAnnouncement
{
   public class OrderingDriverAnnouncementViewModel
    {
        public virtual int id { get; set; }
        public virtual string acceptProduct { get; set; }
        public virtual List<ProductTypeViewModel> acceptProducts { get; set; }
        public virtual string acceptProductDetail { get; set; }
        public virtual string acceptPackaging { get; set; }
        public virtual List<ProductPackagingViewModel> acceptPackagings { get; set; }
        public virtual string acceptPackagingDetail { get; set; }
        public virtual string rejectProduct { get; set; }
        public virtual List<ProductTypeViewModel> rejectProducts { get; set; }
        public virtual string rejectProductDetail { get; set; }
        public virtual string rejectPackaging { get; set; }
        public virtual List<ProductPackagingViewModel> rejectPackagings { get; set; }
        public virtual string rejectPackagingDetail { get; set; }
        public virtual DateTime sendDate { get; set; }
        public virtual DateTime receiveDate { get; set; }
        public virtual string carRegistration { get; set; }
        public virtual bool allRent { get; set; }
        public virtual bool additional { get; set; }
        public virtual string additionalDetail { get; set; }
        public virtual string note { get; set; }
        public virtual int status { get; set; }
        public virtual DriverAnnouncementStatusViewModel statusObj { get; set; }
        public virtual string statusReason { get; set; }
        public virtual decimal desiredPrice { get; set; }
        public virtual IList<DriverAnnouncementLocationViewModel> locations { get; set; }

    }
}
