﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Application.DTOs.DriverAnnouncement
{
    public class FilterProductViewModel
    {
        public virtual int ProductTypeId { get; set; }
        public virtual int PackagingId { get; set; }
    }
}
