﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Application.DTOs.DriverPaymentHistory
{
   public class DriverPaymentHistoryViewModel
    {
        public virtual int Id { get; set; }
        public virtual int DriverPaymentId { get; set; }
        public virtual string Detail { get; set; }
    }
}
