﻿using iTrans.Transportation.Application.DTOs.CustomerProductFile;
using iTrans.Transportation.Application.DTOs.ProductPackaging;
using iTrans.Transportation.Application.DTOs.ProductType;
using System;
using System.Collections.Generic;

namespace iTrans.Transportation.Application.DTOs.CustomerProduct
{
    public class CustomerProductViewModel
    {
        public virtual int Id { get; set; }
        public virtual Guid CustomerId { get; set; }
        public virtual string Name { get; set; }
        public virtual ProductTypeViewModel ProductType { get; set; }
        public virtual string ProductTypeDetail { get; set; }
        public virtual ProductPackagingViewModel Packaging { get; set; }
        public virtual string PackagingDetail { get; set; }
        public virtual int Width { get; set; }
        public virtual int Length { get; set; }
        public virtual int Height { get; set; }
        public virtual decimal Weight { get; set; }
        public virtual int Quantity { get; set; }
        public virtual List<CustomerProductFileViewModel> ProductFiles { get; set; }
    }
}
