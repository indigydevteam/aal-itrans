﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Application.DTOs.OrderingDriverReserveStatus
{
    public class OrderingDriverReserveStatusViewModel
    {
        public int id { set; get; }
        public string th { set; get; }
        public string eng { set; get; }
        public string description { set; get; }
    }
}
