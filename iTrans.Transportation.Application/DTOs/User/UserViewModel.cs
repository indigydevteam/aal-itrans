﻿using iTrans.Transportation.Application.DTOs.MenuPermission;
using iTrans.Transportation.Application.DTOs.Role;
using iTrans.Transportation.Application.DTOs.Title;
using iTrans.Transportation.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Application.DTOs.User
{
  public  class UserViewModel  
    {
        public  string EmplID { get; set; }
        public  string Email { get; set; }
        public  string Password { get; set; }
        public  string UserToken { get; set; }
        public  bool IsNaverExpire { get; set; }
        public  bool IsExternalUser { get; set; }
        public  DateTime EffectiveDate { get; set; }
        public  DateTime ExpiredDate { get; set; }
        public  bool IsActive { get; set; }
        public  DateTime Activated { get; set; }
        public  DateTime Reactivated { get; set; }
        public  int IncorrectLoginCount { get; set; }
        public  string FirstNameTH { get; set; }
        public  string FirstNameEN { get; set; }
        public  string LastNameTH { get; set; }
        public  string LastNameEN { get; set; }
        public  string MiddleNameTH { get; set; }
        public  string MiddleNameEN { get; set; }
        public  string PhoneCode { get; set; }
        public  string PhoneNumber { get; set; }
        public  string IdentityNumber { get; set; }
        public  string Imageprofile { get; set; }
        public  TitleViewModel Title { get; set; }
        public  DateTime Birthday { get; set; }
        public  string CreatedByEN { get; set; }
        public  string CreatedByTH { get; set; }
        public  string CreatedByID { get; set; }
        public  string ModifiedByEN { get; set; }
        public  string ModifiedByTH { get; set; }
        public  string ModifiedByID { get; set; }
        public virtual RoleViewModel Role { get; set; }
        public virtual string ModifiedBy { get; set; }
        public virtual DateTime? Created { get; set; }
        public virtual DateTime? Modified { get; set; }
        public  List<MenuPermissionViewModel> MenuPermission { get; set; }



    }
}
