﻿using iTrans.Transportation.Application.DTOs.Menu;
using iTrans.Transportation.Application.DTOs.Role;
using iTrans.Transportation.Application.DTOs.Title;
using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Application.DTOs.User.backoffice
{
   public class UserViewModel
    {
        public Guid? emplID { get; set; }
        public string name_TH { get; set; }
        public string name_EN { get; set; }
        public string phoneNumber { get; set; } 
        public string email { get; set; } 
        public string role_Name { get; set; }
        public string modifiedBy { get; set; }
        public string modified { get; set; }
        public string imageprofile { get; set; }
    }
}
