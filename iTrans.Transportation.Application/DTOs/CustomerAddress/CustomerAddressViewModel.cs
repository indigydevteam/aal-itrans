﻿using iTrans.Transportation.Application.DTOs.Country;
using iTrans.Transportation.Application.DTOs.District;
using iTrans.Transportation.Application.DTOs.Province;
using iTrans.Transportation.Application.DTOs.Subdistrict;
using System;

namespace iTrans.Transportation.Application.DTOs.CustomerAddress
{
    public class CustomerAddressViewModel
    {
        public int Id { get; set; }
        public Guid CustomerId { get; set; }
        //public int CountryId { get; set; }
        public CountryViewModel Country { get; set; }
        //public int ProvinceId { get; set; }
        public ProvinceViewModel Province { get; set; }
        //public int DistrictId { get; set; }
        public DistrictViewModel District { get; set; }
        //public int SubdistrictId { get; set; }
        public SubdistrictViewModel Subdistrict { get; set; }
        public string PostCode { get; set; }
        public string Road { get; set; }
        public string Alley { get; set; }
        public string Address { get; set; }
        public string Branch { get; set; }
        public string AddressType { get; set; }
        public string AddressName { get; set; }
        public string ContactPerson { get; set; }
        public string ContactPhoneCode { get; set; }
        public string ContactPhoneNumber { get; set; }
        public string ContactEmail { get; set; }
        public string Maps { get; set; }
        public bool IsMainData { get; set; }
        public int Sequence { get; set; }
    }
}
