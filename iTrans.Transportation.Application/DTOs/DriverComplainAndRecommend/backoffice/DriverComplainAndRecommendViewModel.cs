﻿using iTrans.Transportation.Application.DTOs.Driver.Backoffice;
using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Application.DTOs.DriverComplainAndRecommend.backoffice
{
   public class DriverComplainAndRecommendViewModel
    {
        public virtual int Id { get; set; }
        public virtual DriverViewModel Driver { get; set; }
        public virtual string Title { get; set; }
        public virtual string Detail { get; set; }
        public DateTime created { get; set; }

    }
}
