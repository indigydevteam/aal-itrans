﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Application.DTOs.DriverComplainAndRecommend
{
   public class DriverComplainAndRecommendViewModel
    {
        public virtual int Id { get; set; }
        public virtual Guid DriverId { get; set; }
        public virtual string Title { get; set; }
        public virtual string Detail { get; set; }
    }
}
