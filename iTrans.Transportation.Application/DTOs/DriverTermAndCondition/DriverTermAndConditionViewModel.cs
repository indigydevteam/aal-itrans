﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Application.DTOs.DriverTermAndCondition
{
   public class DriverTermAndConditionViewModel
    {
        public int id { get; set; }
        public int termAndConditionId { get; set; }
        public string name_TH { get; set; }
        public string name_ENG { get; set; }
        public string section { get; set; }
        public string version { get; set; }
        public bool isAccept { get; set; }
        public bool isUserAccept { get; set; }
        public bool isComplete { get; set; }
    }
}
