﻿using iTrans.Transportation.Application.DTOs.TermAndCondition;
using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Application.DTOs.DriverTermAndCondition
{
   public class AcceptTermAndConditionViewModel
    {
        public List<DriverTermAndConditionViewModel> termAndCondition { get; set; }
        public List<TermAndConditionViewModel> nonAcceptTermAndCondition { get; set; }
    }
}
