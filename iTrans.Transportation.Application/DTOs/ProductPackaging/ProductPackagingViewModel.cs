﻿using iTrans.Transportation.Application.DTOs.ProductPackagingFile;

namespace iTrans.Transportation.Application.DTOs.ProductPackaging
{
    public class ProductPackagingViewModel
    {
        public int id { get; set; }
        public string name_TH { get; set; }
        public string name_ENG { get; set; }
        public int sequence { get; set; }
        public bool specified { get; set; }
        public bool active { get; set; }
        public ProductPackagingFileViewModel file { get; set; }
    }
}
