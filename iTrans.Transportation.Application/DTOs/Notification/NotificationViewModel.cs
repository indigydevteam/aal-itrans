﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Application.DTOs.Notification
{
    public class NotificationViewModel
    {
        public int MessageId { get; set; }
        public Guid? UserId { get; set; }
        public string Title { get; set; }
        public string Detail { get; set; } 
        public string ServiceCode { get; set; } 
        public string ReferentContentKey { get; set; } 
        public string Payload { get; set; } 
        public DateTime Created { get; set; }
        public string CreatedBy { get; set; }
    }
}
