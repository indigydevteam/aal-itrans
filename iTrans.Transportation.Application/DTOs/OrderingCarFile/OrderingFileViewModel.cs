﻿using System;
namespace iTrans.Transportation.Application.DTOs.OrderingCarFile
{
    public class OrderingCarFileViewModel
    {
        public virtual int id { get; set; }
        public virtual int orderingCarId { get; set; }
        public virtual string fileName { get; set; }
        public virtual string contentType { get; set; }
        public virtual string filePath { get; set; }
        public virtual string fileEXT { get; set; }
        public virtual string documentType { get; set; }
        public virtual int sequence { get; set; }
    }
}
