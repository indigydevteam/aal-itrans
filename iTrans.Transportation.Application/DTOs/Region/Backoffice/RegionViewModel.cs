﻿using System;

namespace iTrans.Transportation.Application.DTOs.Region.Backoffice
{
    public class RegionViewModel
    {
        public virtual int Id { get; set; }
        public virtual int CountryId { get; set; }
        public virtual string CountryName { get; set; }
        public virtual string Name_TH { get; set; }
        public virtual string Name_ENG { get; set; }
        public virtual int Sequence { get; set; }
        public virtual bool Active { get; set; }
    }
}
