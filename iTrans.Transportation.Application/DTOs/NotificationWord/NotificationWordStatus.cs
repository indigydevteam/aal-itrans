﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Application.DTOs.OrderingStatus
{
    public class NotificationWordStatusViewModel
    {
        public int id { set; get; }
        public Title title { set; get; }
        public Detail detail { set; get; }
    }
    public class Title
    {
        public string th { set; get; }
        public string eng { set; get; }
    }
    public class Detail
    {
        public string th { set; get; }
        public string eng { set; get; }
    }
}
