﻿using System;

namespace iTrans.Transportation.Application.DTOs.Config
{
    public class ConfigViewModel
    {
        public virtual string Key { get; set; }
        public virtual string Value { get; set; }
    }
}
