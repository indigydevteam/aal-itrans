﻿using System;

namespace iTrans.Transportation.Application.DTOs.Subdistrict.Backoffice
{
    public class SubdistrictViewModel
    {
        public virtual int Id { get; set; }
        public virtual int DistrictId { get; set; }
        public virtual string DistrictName { get; set; }
        public virtual string Name_TH { get; set; }
        public virtual string Name_ENG { get; set; }
        public virtual string PostCode { get; set; }
        public virtual bool Active { get; set; }
    }
}
