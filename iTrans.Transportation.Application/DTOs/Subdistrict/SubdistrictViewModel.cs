﻿using System;

namespace iTrans.Transportation.Application.DTOs.Subdistrict
{
    public class SubdistrictViewModel
    {
        public virtual int id { get; set; }
        public virtual int districtId { get; set; }
        public virtual string name_TH { get; set; }
        public virtual string name_ENG { get; set; }
        public virtual string postCode { get; set; }
        //public virtual bool Active { get; set; }
    }
}
