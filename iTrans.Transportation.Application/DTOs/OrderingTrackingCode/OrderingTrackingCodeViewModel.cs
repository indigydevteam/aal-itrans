﻿using System;

namespace iTrans.Transportation.Application.DTOs.OrderingTrackingCode
{
    public class OrderingTrackingCodeViewModel
    {
        public virtual string TrackingCode { get; set; }
        public virtual string Section { get; set; }
    }
}
