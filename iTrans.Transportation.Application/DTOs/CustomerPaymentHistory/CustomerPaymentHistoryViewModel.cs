﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Application.DTOs.CustomerPaymentHistory
{
  public  class CustomerPaymentHistoryViewModel
    {
        public virtual int Id { get; set; }
        public virtual int CustomerPaymentId { get; set; }
        public virtual string Detail { get; set; }
    }
}