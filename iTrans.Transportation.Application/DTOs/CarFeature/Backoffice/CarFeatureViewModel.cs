﻿using iTrans.Transportation.Application.DTOs.CarFeatureFile;
using iTrans.Transportation.Application.DTOs.CarList;
using iTrans.Transportation.Application.DTOs.CarType;

namespace iTrans.Transportation.Application.DTOs.CarFeature.Backoffice
{
    public class CarFeatureViewModel
    {
        public virtual int id { get; set; }
        public virtual int carTypeId { get; set; }
        public virtual string carType { get; set; }
        public virtual int carListId { get; set; }
        public virtual string carList { get; set; }
        public virtual string name_TH { get; set; }
        public virtual string name_ENG { get; set; }
        public virtual int sequence { get; set; }
        public virtual bool specified { get; set; }
        public virtual bool active { get; set; }
        public CarFeatureFileViewModel file { get; set; }
    }
}
