﻿using iTrans.Transportation.Application.DTOs.CarFeatureFile;

namespace iTrans.Transportation.Application.DTOs.CarFeature
{
    public class CarFeatureViewModel
    {
        public virtual int id { get; set; }
        public virtual int carListId { get; set; }
        public virtual string name_TH { get; set; }
        public virtual string name_ENG { get; set; }
        public virtual int sequence { get; set; }
        public virtual bool specified { get; set; }

        //public virtual bool Active { get; set; }
        public CarFeatureFileViewModel file { get; set; }
    }
}
