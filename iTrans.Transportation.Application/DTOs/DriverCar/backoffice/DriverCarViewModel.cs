﻿using iTrans.Transportation.Application.DTOs.CarDescription;
using iTrans.Transportation.Application.DTOs.CarFeature;
using iTrans.Transportation.Application.DTOs.CarList;
using iTrans.Transportation.Application.DTOs.CarSpecification;
using iTrans.Transportation.Application.DTOs.CarType;
using iTrans.Transportation.Application.DTOs.Driver;
using iTrans.Transportation.Application.DTOs.DriverCarFile;
using iTrans.Transportation.Application.DTOs.DriverCarLocation;
using iTrans.Transportation.Application.DTOs.DriverFile;
using iTrans.Transportation.Application.DTOs.EnergySavingDevice;
using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Application.DTOs.DriverCar.backoffice
{
    public class DriverCarViewModel
    {
        public int id { get; set; }
        public Guid driverId { get; set; }
        public DriverViewModel Owner { get; set; }

        public string carRegistration { get; set; }
        public DateTime actExpiry { get; set; }
        public CarTypeViewModel carType { get; set; }
        public CarListViewModel carList { get; set; }
        public CarDescriptionViewModel carDescription { get; set; }
        public string carDescriptionDetail { get; set; }
        public CarFeatureViewModel carFeature { get; set; }
        public string carFeatureDetail { get; set; }
        public CarSpecificationViewModel carSpecification { get; set; }
        public string carSpecificationDetail { get; set; }
        public int width { get; set; }
        public int length { get; set; }
        public int height { get; set; }
        public float temperature { get; set; }
        public EnergySavingDeviceViewModel energySavingDevice { get; set; }
        public string driverName { get; set; }
        public string driverPhoneCode { get; set; }
        public string driverPhoneNumber { get; set; }
        public bool productInsurance { get; set; }
        public decimal productInsuranceAmount { get; set; }
        public bool allLocation { get; set; }
        public string note { get; set; }
        public List<DriverCarFileViewModel> carFiles { get; set; }
        public List<DriverFileViewModel> driverFiles { get; set; }
        public List<DriverCarLocationViewModel> locations { get; set; }
        public DriverCarLocationViewModel worklocations { get; set; }
        public DriverCarLocationViewModel nonworklocations { get; set; }
        public string imageCarPicture { get; set; }
        public string imageCarRegistration { get; set; }
        public bool status { get; set; }
        public string created { get; set; }
        public int countCar { get; set; }
        public string corporateName { get; set; }
        public string corporateNumber { get; set; }

    }
}
