﻿using iTrans.Transportation.Application.DTOs.DriverCarFile;
using iTrans.Transportation.Application.DTOs.EnergySavingDevice;
using System;
using System.Collections.Generic;

namespace iTrans.Transportation.Application.DTOs.DriverCar
{
    public class DriverCarWithFilesViewModel
    {
        public int id { get; set; }
        public Guid driverId { get; set; }
        public string carRegistration { get; set; }
        public DateTime actExpiry { get; set; }
        public int carTypeId { get; set; }
        public int carListId { get; set; }
        public int carDescriptionId { get; set; }
        public string carDescriptionDetail { get; set; }
        public int carFeatureId { get; set; }
        public string carFeatureDetail { get; set; }
        public int carSpecificationId { get; set; }
        public string carSpecificationDetail { get; set; }
        public int width { get; set; }
        public int length { get; set; }
        public int height { get; set; }
        public float temperature { get; set; }
        public EnergySavingDeviceViewModel energySavingDevice { get; set; }
        public string driverName { get; set; }
        public string driverPhoneCode { get; set; }
        public string driverPhoneNumber { get; set; }
        public string driverIdentityNumber { get; set; }
        public bool productInsurance { get; set; }
        public string productInsuranceAmount { get; set; }
        public bool allLocation { get; set; }
        public string note { get; set; }
        public List<DriverCarFileViewModel> carFiles { get; set; }
    }
}
