﻿using iTrans.Transportation.Application.DTOs.CarDescription;
using iTrans.Transportation.Application.DTOs.CarFeature;
using iTrans.Transportation.Application.DTOs.CarList;
using iTrans.Transportation.Application.DTOs.CarSpecification;
using iTrans.Transportation.Application.DTOs.CarType;
using iTrans.Transportation.Application.DTOs.Driver;
using iTrans.Transportation.Application.DTOs.DriverCarFile;
using iTrans.Transportation.Application.DTOs.DriverCarLocation;
using iTrans.Transportation.Application.DTOs.EnergySavingDevice;
using System;
using System.Collections.Generic;

namespace iTrans.Transportation.Application.DTOs.DriverCar
{
    public class DriverCarInformationViewModel
    {
        public int id { get; set; }
        public DriverViewModel driver { get; set; }
        public DriverViewModel owner { get; set; }
        public string carRegistration { get; set; }
        public DateTime actExpiry { get; set; }
        public CarTypeViewModel carType { get; set; }
        public CarListViewModel carList { get; set; }
        public CarDescriptionViewModel carDescription { get; set; }
        public string carDescriptionDetail { get; set; }
        //public virtual int CarFeatureId { get; set; }
        public CarFeatureViewModel CarFeature { get; set; }
        public string carFeatureDetail { get; set; }
        //public virtual int CarSpecificationId { get; set; }
        public CarSpecificationViewModel carSpecification { get; set; }
        public string carSpecificationDetail { get; set; }
        public int width { get; set; }
        public int length { get; set; }
        public int height { get; set; }
        public float temperature { get; set; }
        public EnergySavingDeviceViewModel energySavingDevice { get; set; }
        public string driverName { get; set; }
        public string driverPhoneCode { get; set; }
        public string driverPhoneNumber { get; set; }
        public string driverIdentityNumber { get; set; }
        public bool productInsurance { get; set; }
        public string productInsuranceAmount { get; set; }
        public bool allLocation { get; set; }
        public string note { get; set; }
        public List<DriverCarFileViewModel> carFiles { get; set; }
        public List<DriverCarLocationViewModel> locations { get; set; }
    }
}
