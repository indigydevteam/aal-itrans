﻿using iTrans.Transportation.Application.DTOs.ProductTypeFile;
using System;

namespace iTrans.Transportation.Application.DTOs.Backoffice.ProductType
{
    public class ProductTypeViewModel
    {
        public int id { get; set; }
        public string name_TH { get; set; }
        public string name_ENG { get; set; }
        public int sequence { get; set; }   
        public bool active { get; set; }
        public bool specified { get; set; }
        public bool isDelete { get; set; }
        public DateTime created { get; set; }
        public DateTime modified { get; set; }
        public ProductTypeFileViewModel File { get; set; }
    }
}
