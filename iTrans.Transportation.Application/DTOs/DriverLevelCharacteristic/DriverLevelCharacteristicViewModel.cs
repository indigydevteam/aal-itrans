﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Application.DTOs.DriverLevelCharacteristic
{
    public class DriverLevelCharacteristicViewModel
    {
        public int Id { get; set; }
        public int Level { get; set; }
        public int Star { get; set; }
        public int AcceptJobPerMonth { get; set; }
        public int CancelPerMonth { get; set; }
        public virtual int AnnouncementPerMonth { get; set; }
        public virtual int AnnouncementPerYear { get; set; }
        public int ComplaintPerMonth { get; set; }
        public int RejectPerMonth { get; set; }
        public int ComplaintPerYear { get; set; }
        public int RejectPerYear { get; set; }
        public int CancelPerYear { get; set; }
        public decimal InsuranceValue { get; set; }
        public decimal Commission { get; set; }
    }
   
}