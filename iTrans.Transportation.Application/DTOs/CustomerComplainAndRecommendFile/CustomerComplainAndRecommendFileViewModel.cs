﻿using System;
namespace iTrans.Transportation.Application.DTOs.CustomerComplainAndRecommendFile
{
    public class CustomerComplainAndRecommendFileViewModel
    {
        public virtual int Id { get; set; }
        public virtual Guid CustomerId { get; set; }
        public virtual string FileName { get; set; }
        public virtual string ContentType { get; set; }
        public virtual string FilePath { get; set; }
        public virtual string FileEXT { get; set; }
        public virtual string DocumentType { get; set; }
        public virtual int Sequence { get; set; }
    }
}
