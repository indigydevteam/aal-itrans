﻿using System;

namespace iTrans.Transportation.Application.DTOs.SuggestionInformation
{
    public class SuggestionInformationViewModel
    {
        public int Id { get; set; }
        public  string Title { get; set; }
        public string Title_EN { get; set; }
        public string Detail { get; set; }
        public string Detail_EN { get; set; }
        public string Picture { get; set; }
        public string QRCode { get; set; }
    }
}
