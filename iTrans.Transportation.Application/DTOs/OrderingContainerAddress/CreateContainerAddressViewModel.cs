﻿using iTrans.Transportation.Application.DTOs.Country;
using iTrans.Transportation.Application.DTOs.District;
using iTrans.Transportation.Application.DTOs.Province;
using iTrans.Transportation.Application.DTOs.Subdistrict;
using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Application.DTOs.OrderingContainerAddress
{
    public class CreateContainerAddressViewModel
    {
        public virtual int LoadingCountryId { get; set; }
        public virtual int LoadingProvinceId { get; set; }
        public virtual int LoadingDistrictId { get; set; }
        public virtual int LoadingSubdistrictId { get; set; }
        public virtual string LoadingPostCode { get; set; }
        public virtual string LoadingRoad { get; set; }
        public virtual string LoadingAlley { get; set; }
        public virtual string LoadingAddress { get; set; }
        public virtual DateTime ETD { get; set; }
        public virtual string LoadingMaps { get; set; }
        public virtual int DestinationCountryId { get; set; }
        public virtual int DestinationProvinceId { get; set; }
        public virtual int DestinationDistrictId { get; set; }
        public virtual int DestinationSubdistrictId { get; set; }
        public virtual string DestinationPostCode { get; set; }
        public virtual string DestinationRoad { get; set; }
        public virtual string DestinationAlley { get; set; }
        public virtual string DestinationAddress { get; set; }
        public virtual DateTime ETA { get; set; }
        public virtual string DestinationMaps { get; set; }
        public virtual string FeederVessel { get; set; }
        public virtual DateTime TStime { get; set; }
        public virtual string MotherVessel { get; set; }
        public virtual DateTime CPSDate { get; set; }
        public virtual string CPSDetail { get; set; }
        public virtual DateTime CYDate { get; set; }
        public virtual string CYDetail { get; set; }
        public virtual DateTime ReturnDate { get; set; }
        public virtual string ReturnDetail { get; set; }
        public virtual DateTime ClosingTime { get; set; }
        public virtual DateTime CutOffVGMTime { get; set; }
        public virtual DateTime CutOffSITime { get; set; }
        public virtual string Agent { get; set; }
    }
}
