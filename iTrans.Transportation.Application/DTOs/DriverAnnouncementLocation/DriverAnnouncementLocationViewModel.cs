﻿using iTrans.Transportation.Application.DTOs.Country;
using iTrans.Transportation.Application.DTOs.District;
using iTrans.Transportation.Application.DTOs.Province;
using iTrans.Transportation.Application.DTOs.Subdistrict;
using System;

namespace iTrans.Transportation.Application.DTOs.DriverAnnouncementLocation
{
   public class DriverAnnouncementLocationViewModel
    {
        public virtual int id { get; set; }
        public virtual int driverAnnouncementId { get; set; }
        public virtual CountryViewModel country { get; set; }
        public virtual ProvinceViewModel province { get; set; }
        public virtual DistrictViewModel district { get; set; }
        public virtual SubdistrictViewModel subdistrict { get; set; }
        public virtual string locationType { get; set; }
        public virtual string note { get; set; }
        public virtual int sequence { get; set; }
        public virtual DateTime sendDate { get; set; }
        public virtual DateTime receiveDate { get; set; }

    }
}
