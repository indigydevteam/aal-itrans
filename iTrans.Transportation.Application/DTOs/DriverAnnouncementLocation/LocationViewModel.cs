﻿using iTrans.Transportation.Application.DTOs.Country;
using iTrans.Transportation.Application.DTOs.District;
using iTrans.Transportation.Application.DTOs.Province;
using iTrans.Transportation.Application.DTOs.Subdistrict;
using System;

namespace iTrans.Transportation.Application.DTOs.DriverAnnouncementLocation
{
   public class LocationViewModel
    {
        public virtual int Id { get; set; }
        public virtual int DriverAnnouncementId { get; set; }
        public virtual int CountryId { get; set; }
        public virtual int ProvinceId { get; set; }
        public virtual int DistrictId { get; set; }
        public virtual int SubdistrictId { get; set; }
        public virtual string LocationType { get; set; }
        public virtual string Note { get; set; }
        public virtual int Sequence { get; set; }
        public virtual DateTime SendDate { get; set; }
        public virtual DateTime ReceiveDate { get; set; }

    }
}
