﻿using iTrans.Transportation.Application.DTOs.Role;
using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Application.DTOs.MenuPermission
{
  public  class MenuPermissionViewModel
    {
        public  int Id { get; set; }
        public  int RoleId { get; set; }
        public string MenuName { get; set; }
        public string MenuNameEN { get; set; }
        public string Icon { get; set; }
        public string Path { get; set; }
        public string Rolename { get; set; }
        public string Permission { get; set; }      
        public bool IsShow { get; set; }      
        public List<RoleMenuPermission> RoleMenuPermission { get; set; }


    }
}
