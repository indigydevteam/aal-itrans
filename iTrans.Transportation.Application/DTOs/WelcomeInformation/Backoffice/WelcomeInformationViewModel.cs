﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Application.DTOs.Backoffice.WelcomeInformation
{
   public class WelcomeInformationViewModel 
  {
        public int Id { get; set; }
        public string Module { get; set; }
        public string Title_TH { get; set; }
        public string Information_TH { get; set; }
        public string Title_ENG { get; set; }
        public string Information_ENG { get; set; }
        public string Picture { get; set; }
        public int Sequence { get; set; }
        public bool Active { get; set; }
        public bool IsDelete { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
    }
}