﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Humantix.Authentication.Models.ApiRes
{
    public class BaseOutputModel
    {
        public bool success { get; set; }
        public int error_code { get; set; }
        public string error_messege { get; set; }
        public string error_messege_alt { get; set; }
        public dynamic result { get; set; }
    }
}
