﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Humantix.Authentication.Models.ApiRes
{
    public class OTPLoginRsp
    {
        public string access_token { get; set; }
        public string expires_in { get; set; }
        public string token_type { get; set; }
        public string scope { get; set; }
        public string full_name { get; set; }
        public bool is_first_login { get; set; }
        public bool is_primary { get; set; }
        public bool is_termcondition { get; set; }
        public bool isread_condition { get; set; }
        public string token { get; set; }
    }
}
