﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Humantix.Authentication.Models.ApiRes
{
    public class OTPRsp
    {
        public int time_out_second { get; set; }
        public string ref_key { get; set; }
    }
}
