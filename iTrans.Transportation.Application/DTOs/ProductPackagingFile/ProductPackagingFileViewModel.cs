﻿using System;
namespace iTrans.Transportation.Application.DTOs.ProductPackagingFile
{
    public class ProductPackagingFileViewModel
    {
        public int id { get; set; }
        public int productPackagingId { get; set; }
        public string fileName { get; set; }
        public string contentType { get; set; }
        public string filePath { get; set; }
        public string fileEXT { get; set; }
    }
}
