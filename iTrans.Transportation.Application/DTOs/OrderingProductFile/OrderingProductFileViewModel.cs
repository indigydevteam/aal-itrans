﻿using System;
namespace iTrans.Transportation.Application.DTOs.OrderingProductFile
{
    public class OrderingProductFileViewModel
    {
        public virtual int id { get; set; }
        public virtual int orderingProductId { get; set; }
        public virtual string fileName { get; set; }
        public virtual string contentType { get; set; }
        public virtual string filePath { get; set; }
        public virtual string fileEXT { get; set; }
    }
}
