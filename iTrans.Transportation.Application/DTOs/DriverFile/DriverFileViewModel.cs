﻿using System;
namespace iTrans.Transportation.Application.DTOs.DriverFile
{
    public class DriverFileViewModel
    {
        public virtual int id { get; set; }
        public virtual Guid driverId { get; set; }
        public virtual string fileName { get; set; }
        public virtual string contentType { get; set; }
        public virtual string filePath { get; set; }
        public virtual string fileEXT { get; set; }
        public virtual string documentType { get; set; }
        public virtual int sequence { get; set; }
        public bool IsApprove { get; set; }
        public bool IsDelete { get; set; }
    }
}
