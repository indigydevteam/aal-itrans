﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Application.DTOs.EnergySavingDevice
{
  public  class EnergySavingDeviceViewModel
    {
        public int id { get; set; }
        public string name_TH { get; set; }
        public string name_ENG { get; set; }
        public bool active { get; set; }
        public bool specified { get; set; }
        public int sequence { get; set; }
    }
}
