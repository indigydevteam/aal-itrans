﻿using iTrans.Transportation.Application.DTOs.CorporateType;
using iTrans.Transportation.Application.DTOs.DriverAddress;
using iTrans.Transportation.Application.DTOs.DriverFile;
using iTrans.Transportation.Application.DTOs.DriverPayment;
using iTrans.Transportation.Application.DTOs.DriverTermAndCondition;
using iTrans.Transportation.Application.DTOs.Title;
using iTrans.Transportation.Application.DTOs.UserLevel;
using iTrans.Transportation.Application.DTOs.VerifyStatus;
using System;
using System.Collections.Generic;

namespace iTrans.Transportation.Application.DTOs.Driver
{
    public class DriverCorparateInformationViewModel
    {
        public Guid id { get; set; }
        public string password { get; set; }
        public string driverType { get; set; }
        public CorporateTypeViewModel corporateType { get; set; }
        public string name { get; set; }
        public int identityType { get; set; }
        public string identityNumber { get; set; }
        public TitleViewModel contactPersonTitle { get; set; }
        public string contactPersonFirstName { get; set; }
        public string contactPersonMiddleName { get; set; }
        public string contactPersonLastName { get; set; }
        public string phoneCode { get; set; }
        public string phoneNumber { get; set; }
        public string email { get; set; } 
        public int level { get; set; }
        public UserLevelViewModel driverLevel { get; set; }
        public string facbook { get; set; }
        public string line { get; set; }
        public string twitter { get; set; }
        public string whatapp { get; set; }
        public IList<DriverTermAndConditionViewModel> termAndConditions { get; set; }
        public List<DriverPaymentViewModel> payments { get; set; }
        public List<ProfileAddressViewModel> addresses { get; set; }
        public List<DriverFileViewModel> files { get; set; }
        public int star { get; set; }
        public string rating { get; set; }
        public string grade { get; set; }
        public UserLevelViewModel nextLevel { get; set; }
        public int verifyStatus { get; set; }
        public VerifyStatusViewModel verifyStatusObj { get; set; }
        public bool isRegister { get; set; }
        public bool isDelete { get; set; }
    }
}
