﻿using iTrans.Transportation.Application.DTOs.DriverAddress;
using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Application.DTOs.Driver.Backoffice
{
    public class TaxDriverAddressViewModel
    {
        public Guid driverId { get; set; }
        public string driverType { get; set; }
        public DriverAddressViewModel personal { get; set; }
        public List<DriverAddressViewModel> corporate { get; set; }

    }
}
