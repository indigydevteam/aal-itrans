﻿using iTrans.Transportation.Application.DTOs.CorporateType;
using iTrans.Transportation.Application.DTOs.DriverAddress;
using iTrans.Transportation.Application.DTOs.DriverCar;
using iTrans.Transportation.Application.DTOs.DriverFile;
using iTrans.Transportation.Application.DTOs.DriverPayment;
using iTrans.Transportation.Application.DTOs.DriverTermAndCondition;
using iTrans.Transportation.Application.DTOs.Title;
using iTrans.Transportation.Application.DTOs.UserLevel;
using iTrans.Transportation.Application.DTOs.VerifyStatus;
using System;
using System.Collections.Generic;
namespace iTrans.Transportation.Application.DTOs.Driver.Backoffice
{
    public class DriverViewModel
    {
        public virtual Guid Id { get; set; }
        public virtual string Password { get; set; }
        public virtual string DriverType { get; set; }
        public virtual CorporateTypeViewModel CorporateType { get; set; }
        public virtual TitleViewModel Title { get; set; }
        public virtual string FirstName { get; set; }
        public virtual string MiddleName { get; set; }
        public virtual string LastName { get; set; }
        public virtual string Name { get; set; }
        public virtual int IdentityType { get; set; }
        public virtual int CorporateTypeId { get; set; }
        public virtual string IdentityNumber { get; set; }
        public virtual TitleViewModel ContactPersonTitle { get; set; }
        public virtual string ContactPersonFirstName { get; set; }
        public virtual string ContactPersonMiddleName { get; set; }
        public virtual string ContactPersonLastName { get; set; }
        public virtual DateTime Birthday { get; set; }
        public virtual string PhoneCode { get; set; }
        public virtual string PhoneNumber { get; set; }
        public virtual string Email { get; set; }
        public virtual int Level { get; set; }
        public virtual UserLevelViewModel DriverLevel { get; set; }
        public virtual string LevelName { get; set; }
        public virtual string Facbook { get; set; }
        public virtual string Line { get; set; }
        public virtual string Twitter { get; set; }
        public virtual string Whatapp { get; set; }
        public virtual string Wechat { get; set; }
        public virtual bool haveCorparate { get; set; }
        public virtual int TermAndConditionsId { get; set; }
        public virtual DriverCorparateInformationViewModel CorparateInformation { get; set; }
        public virtual List<DriverPaymentViewModel> Payments { get; set; }
        public virtual List<DriverAddressViewModel> Addresses { get; set; }
        public virtual List<DriverFileViewModel> Files { get; set; }
        public virtual List<DriverCarViewModel> Cars { get; set; }
        public virtual int Star { get; set; }
        public virtual int Checkdocuments { get; set; }
        public virtual int VerifyStatus { get; set; }
        public virtual VerifyStatusViewModel VerifyStatusObj { get; set; }
        public virtual string VerifyStatusName { get; set; }
        public virtual string VerifyStatusColor { get; set; }
        public virtual int CountCar { get; set; }
        public virtual bool Status { get; set; }
        public virtual string imageProfile { get; set; }
        public virtual string IdCard { get; set; }
        public virtual string DrivingLicense { get; set; }
        public virtual string DriverPicture { get; set; }
        public virtual string CompanyCertificate { get; set; }
        public virtual string NP20 { get; set; }
        public virtual string Created { get; set; }
        public virtual DateTime Modified { get; set; }



    }
}