﻿using iTrans.Transportation.Application.DTOs.DriverCar;
using iTrans.Transportation.Application.DTOs.DriverFile;
using iTrans.Transportation.Application.DTOs.Title;
using iTrans.Transportation.Application.DTOs.VerifyStatus;
using System;
using System.Collections.Generic;

namespace iTrans.Transportation.Application.DTOs.Driver
{
    public class DriverProfileViewModel
    {
        public Guid id { get; set; }
        public TitleViewModel title { get; set; }
        public string firstName { get; set; }
        public string middleName { get; set; }
        public string lastName { get; set; }
        public string name { get; set; }
        public int identityType { get; set; }
        public string identityNumber { get; set; }
        public string phoneCode { get; set; }
        public string phoneNumber { get; set; }
        public string email { get; set; }
        public string level { get; set; }
        public string facbook { get; set; }
        public string line { get; set; }
        public string twitter { get; set; }
        public string whatapp { get; set; }
        public List<DriverFileViewModel> files { get; set; }
        public List<DriverCarViewModel> cars { get; set; }
        public int star { get; set; }
        public string rating { get; set; }
        public string grade { get; set; }
        public int verifyStatus { get; set; }
        public VerifyStatusViewModel verifyStatusObj { get; set; }
        public bool isRegister { get; set; }
        public virtual bool isDelete { get; set; }
    }
}
