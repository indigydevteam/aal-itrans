﻿using iTrans.Transportation.Application.DTOs.DriverAddress;
using iTrans.Transportation.Application.DTOs.VerifyStatus;
using System;
using System.Collections.Generic;

namespace iTrans.Transportation.Application.DTOs.Driver
{
    public class DriverWithAddressViewModel
    {
        public Guid id { get; set; }
        public string password { get; set; }
        public string driverType { get; set; }
        public string corporateTypeId { get; set; }
        public string title { get; set; }
        public string firstName { get; set; }
        public string middleName { get; set; }
        public string lastName { get; set; }
        public string name { get; set; }
        public int identityType { get; set; }
        public string identityNumber { get; set; }
        public string contactPersonTitle { get; set; }
        public string contactPersonFirstName { get; set; }
        public string contactPersonMiddleName { get; set; }
        public string contactPersonLastName { get; set; }
        public string birthday { get; set; }
        public string phoneCode { get; set; }
        public string phoneNumber { get; set; }
        public string email { get; set; } 
        public string level { get; set; }
        public string facbook { get; set; }
        public string line { get; set; }
        public string twitter { get; set; }
        public string whatapp { get; set; }
        public int star { get; set; }
        public string rating { get; set; }
        public string grade { get; set; }
        public List<DriverAddressViewModel> addresses { get; set; }
        public int verifyStatus { get; set; }
        public VerifyStatusViewModel verifyStatusObj { get; set; }
        public bool isRegister { get; set; }
        public bool isDelete { get; set; }
    }
}
