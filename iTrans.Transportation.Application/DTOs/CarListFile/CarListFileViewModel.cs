﻿using System;
namespace iTrans.Transportation.Application.DTOs.CarListFile
{
    public class CarListFileViewModel
    {
        public int id { get; set; }
        public int carListId { get; set; }
        public string fileName { get; set; }
        public string contentType { get; set; }
        public string filePath { get; set; }
        public string fileEXT { get; set; }
    }
}
