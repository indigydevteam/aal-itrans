﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Application.DTOs.DriverInbox
{
    public class DriverInboxViewModel
    {
        public virtual int Id { get; set; }
        public virtual Guid DriverId { get; set; }
        public virtual string Title { get; set; }
        public virtual string Title_ENG { get; set; }
        public virtual string Module { get; set; }
        public virtual string Module_ENG { get; set; }
        public virtual string Content { get; set; }
        public virtual string FromUser { get; set; }
        public virtual bool IsDelete { get; set; }
    }
}
