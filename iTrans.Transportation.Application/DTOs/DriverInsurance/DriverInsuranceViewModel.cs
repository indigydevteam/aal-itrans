﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Application.DTOs.DriverInsurance
{
  public  class DriverInsuranceViewModel
    {
        public virtual int Id { get; set; }
        public virtual Guid DriverId { get; set; }
        public virtual string Product { get; set; }
        public virtual string ProductType { get; set; }
        public virtual string ProductTypeDetail { get; set; }
        public virtual string WarrantyLimit { get; set; }
        public virtual string WarrantyPeriod { get; set; }
        public virtual decimal TotalAmount { get; set; }
    }
}
