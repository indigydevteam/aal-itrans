﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Application.Enums
{
    public enum LoginLogTypeId
    {
        Success = 1,
        IncorrectUserNameOrPassword = 2,
        UserInActive = 3,
        SSOSuccess = 4,

    }
}
