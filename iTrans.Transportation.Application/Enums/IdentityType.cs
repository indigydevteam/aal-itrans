﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Application.Enums
{
    public enum IdentityType
    {
        CitizenID = 1,
        Passport = 2
    }
}
