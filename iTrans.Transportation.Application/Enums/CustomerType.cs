﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Application.Enums
{
    public enum CustomerType 
    {
        personal = 1,
        corporate = 2,
    }
}
