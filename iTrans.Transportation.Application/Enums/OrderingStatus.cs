﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Application.Enums
{
    public enum OrderingStatus_bk
    {
        lookingcar = 1,
        gotcar = 2,
        pickup = 3,
        deliver = 4,
        complete = 5,
        cancel = 6
    }
}
