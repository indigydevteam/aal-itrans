﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Application.Enums
{
    public enum DriverAnnouncementStatus_BK
    {
        lookingjob = 1,
        gotjob = 2,
        customerreserved = 3,
        complete = 4,
        customercancel = 5,
        drivercancel = 6
    }
}
