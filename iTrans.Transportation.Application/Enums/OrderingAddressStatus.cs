﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Application.Enums
{
    public enum OrderingAddressStatus
    {
        topickup = 1,
        waitingloading = 2,
        loadingcomplete = 3,
        todeliver = 4,
        waitingdrop = 5,
        dropcomplete = 6,
    }
}
