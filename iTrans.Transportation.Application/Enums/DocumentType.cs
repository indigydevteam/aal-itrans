﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Application.Enums
{
    public enum DocumentType
    {
        idcard = 1,
        drivinglicense = 2,
        driverpicture = 3,
        companycertificate = 4,
        np20 = 5,
    }
}
