﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Application.Enums
{
    public enum NewsType
    {
        ข่าวสาร = 1,
        ประกาศ = 2,
        โปรโมชัน = 3
    }

    public enum ServiceCode
    {
        News = 1,
        Announcement = 2,
        Promotion = 3,
        Order = 4,
        ChatOrder = 5,
        ChatAdmin = 6
    }
}
