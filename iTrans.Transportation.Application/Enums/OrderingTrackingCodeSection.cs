﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Application.Enums
{
    public enum OrderingTrackingCodeSection
    {
        pickuppointproduct = 1, //pickuppoint-product
        deliverypointproduct = 2, //deliverypoint-product
    }
}
