﻿using iTrans.Transportation.Application.Features.Notification.Command;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;

namespace iTrans.Transportation.Application
{
    public class HttpNotificationManager
    {
        public static HttpClient _httpClient { get; set; }
        public static string _endpoint { get; set; }

        public HttpNotificationManager(string endpoint)
        {
            if (_httpClient == null)
                _httpClient = new HttpClient();

            _endpoint = endpoint;
            _endpoint = endpoint;
        }

        public void SendNotification(string title, string detail, string code, Guid? toUserId, Guid fromUserId, string refKey,
                                     string token, string targetCustomers, string targetDrivers, bool isAll, string payload = "")
        {
            var data = new CreateNotificationCommand()
            {
                Title = title,
                Detail = detail,
                ServiceCode = code,
                UserId = toUserId,
                OwnerId = fromUserId,
                ReferenceContentKey = refKey,
                TargetCustomers = targetCustomers,
                TargetDrivers = targetDrivers,
                IsAll = isAll,
                IsListing = true,
                Payload = payload
            };

            var myContent = JsonConvert.SerializeObject(data);
            var buffer = Encoding.UTF8.GetBytes(myContent);
            var byteContent = new ByteArrayContent(buffer);
            byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("bearer", token.Replace("bearer ", ""));

            _httpClient.PostAsync(_endpoint, byteContent);
        }
    }
}
