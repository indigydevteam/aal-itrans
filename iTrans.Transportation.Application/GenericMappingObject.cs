﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace iTrans.Transportation.Application
{
    public class GenericMappingObject<T> where T : new()
    {
        public dynamic Mapping(object input)
        {
            string[] arr = Array.ConvertAll((object[])input, Convert.ToString);

            PropertyInfo[] properties = typeof(T).GetProperties();

            int index = 0;

            var response = new T();

            foreach (PropertyInfo property in properties)
            {

                if (property.PropertyType == typeof(int))
                {
                    var value = Convert.ToInt32(arr[index]);
                    property.SetValue(response, value);
                }
                else if (property.PropertyType == typeof(bool))
                {
                    var value = Convert.ToBoolean(arr[index]);
                    property.SetValue(response, value);
                }
                else if (property.PropertyType == typeof(DateTime))
                {
                    if (string.IsNullOrEmpty(arr[index])){
                        property.SetValue(response, new DateTime());
                    }
                    else
                    {
                        var value = Convert.ToDateTime(arr[index]);
                        property.SetValue(response, value);
                    } 
                }
                else
                {
                    property.SetValue(response, arr[index]);
                }
                index++;
            }
            return response;
        }
    }
}
