﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Helper
{
    public static class ImageHelper
    {
        public static string SaveImage(string rootPath, Guid id, string ext, byte[] data, string Comcode = "", string Catrgory = "")
        {
            string sPath = string.Concat(id, ext);
            string path = "";
            if (!String.IsNullOrEmpty(Comcode))
                path = @"\" + Comcode;
            if (!String.IsNullOrEmpty(Catrgory))
                path = path + @"\" + Catrgory;

            if (!Directory.Exists(rootPath))
                Directory.CreateDirectory(rootPath);

            File.WriteAllBytes(rootPath + path + @"\" + sPath, data);
            return path + @"\" + sPath;
        }
    }
}
