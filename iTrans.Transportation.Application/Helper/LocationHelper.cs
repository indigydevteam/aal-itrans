﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;

namespace iTrans.Transportation.Application.Helper
{
    public static class LocationHelper
    {
        public static DistanceAndEstimateTime GetDistanceAndEstimateTime(int detectArea, string api_key, string startLocation, string endLocation)
        {
            DistanceAndEstimateTime result = new DistanceAndEstimateTime()
            {
                Distance = "",
                EstimateTime = ""
            };

            try
            {
                if (string.IsNullOrEmpty(startLocation) || string.IsNullOrEmpty(endLocation))
                    return result;
                HttpClient hc = new HttpClient();
                var startLocate = startLocation.Split(',');
                decimal startLatitude = Convert.ToDecimal(startLocate[0]);
                decimal startLongitude = Convert.ToDecimal(startLocate[1]);

                string url = "https://maps.googleapis.com/maps/api/distancematrix/xml?origins=" + startLocation + "&destinations=" + endLocation + "&key=" + api_key;
                WebRequest request = WebRequest.Create(url);
                using (WebResponse response = (HttpWebResponse)request.GetResponse())
                {
                    using (StreamReader reader = new StreamReader(response.GetResponseStream(), Encoding.UTF8))
                    {

                        DataSet dsResult = new DataSet();
                        dsResult.ReadXml(reader);
                        long estimateTime = 0;
                        double durationTime = 0;
                        if (double.TryParse(dsResult.Tables["duration"].Rows[0]["value"].ToString(), out durationTime))
                        {
                            durationTime = double.Parse(dsResult.Tables["duration"].Rows[0]["value"].ToString());
                            estimateTime = (long)Math.Round(durationTime / 60);
                        }
                        result.EstimateTime = estimateTime.ToString();//dsResult.Tables["duration"].Rows[0]["text"].ToString();
                        result.Distance = dsResult.Tables["distance"].Rows[0]["value"].ToString(); //dsResult.Tables["distance"].Rows[0]["text"].ToString();
                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                //throw ex;
                return result;
            }
        }
    }
    public class DistanceAndEstimateTime
    {
        public string Distance { get; set; }
        public string EstimateTime { get; set; }
    }
}
