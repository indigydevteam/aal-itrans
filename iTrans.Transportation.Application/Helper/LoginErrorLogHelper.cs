﻿using iTrans.Transportation.Application.Interfaces.Repositories;
using iTrans.Transportation.Domain;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Helper
{
    public class LoginErrorLogHelper
    {
        public async Task<bool> IsAccountLocked(string username, ILoginErrorLogRepositoryAsync _loginErrorLogRepository)
        {
            var error = (await _loginErrorLogRepository.FindByCondition(x => x.Username == username).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            if (error != null && error.UnlockDate > DateTime.Now)
                return true;
            else
                return false;
        }
        public async void ClearIncorrect(string username, ILoginErrorLogRepositoryAsync _loginErrorLogRepository)
        {
            var error = (await _loginErrorLogRepository.FindByCondition(x => x.Username == username).ConfigureAwait(false)).AsQueryable().FirstOrDefault();

            if (error != null)
            {
                error.IncorrectTime = 0;
                error.UnlockDate = null;
                error.LastModified = DateTime.Now;
                await _loginErrorLogRepository.UpdateAsync(error);
            }
        }
        public async Task<Tuple<string, string>> UpdateIncorrect(string username, ILoginErrorLogRepositoryAsync _loginErrorLogRepository, IConfiguration configuration)
        {
            var error = (await _loginErrorLogRepository.FindByCondition(x => x.Username == username).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            if (error != null)
            {
                error.IncorrectTime += 1;
                error.LastModified = DateTime.Now;
                if (error.IncorrectTime > Convert.ToInt32(configuration["AccountLocked:Step1"]) && error.IncorrectTime <= Convert.ToInt32(configuration["AccountLocked:Step2"]))
                {
                    error.UnlockDate = DateTime.Now.AddMinutes(Convert.ToInt32(configuration["AccountLocked:Step1_TimeLocked"]));
                    await _loginErrorLogRepository.UpdateAsync(error);
                    return new Tuple<string, string>(configuration["AccountLocked:Step1_Msg_TH"], configuration["AccountLocked:Step1_Msg_EN"]);
                }
                else if (error.IncorrectTime > Convert.ToInt32(configuration["AccountLocked:Step2"]))
                {
                    error.UnlockDate = DateTime.Now.AddMinutes(Convert.ToInt32(configuration["AccountLocked:Step2_TimeLocked"]));
                    await _loginErrorLogRepository.UpdateAsync(error);
                    return new Tuple<string, string>(configuration["AccountLocked:Step2_Msg_TH"], configuration["AccountLocked:Step2_Msg_EN"]);
                }
                await _loginErrorLogRepository.UpdateAsync(error);
            }
            else
            {
                var model = new LoginErrorLog
                {
                    IncorrectTime = 1,
                    LastModified = DateTime.Now,
                    Username = username,
                    UnlockDate = null
                };
                await _loginErrorLogRepository.AddAsync(model);
            }
            return new Tuple<string, string>("รหัสผ่านไม่ถูกต้อง", "Username or password is invalid.");
        }

        public async Task<bool> LockUser(string username, ILoginErrorLogRepositoryAsync _loginErrorLogRepository, int hour)
        {
            var error = (await _loginErrorLogRepository.FindByCondition(x => x.Username == username).ConfigureAwait(false)).AsQueryable().FirstOrDefault();
            if (error != null)
            {
                error.IncorrectTime += 1;
                error.LastModified = DateTime.Now;
                error.UnlockDate = DateTime.Now.AddHours(hour);
                await _loginErrorLogRepository.UpdateAsync(error);
            }
            else
            {
                var model = new LoginErrorLog
                {
                    IncorrectTime = 1,
                    LastModified = DateTime.Now,
                    Username = username,
                    UnlockDate = DateTime.Now.AddHours(hour)
                };
                await _loginErrorLogRepository.AddAsync(model);
            }
            return true;
        }
    }
}
