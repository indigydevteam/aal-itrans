﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace iTrans.Transportation.Application.Helper
{
    public static class HttpHelper
    {
        private static readonly Encoding encoding = Encoding.UTF8;
        public static string Post(List<Tuple<string, string>> headers, string contentType, string url, string data)
        {
            try
            {
                HttpWebRequest http = (HttpWebRequest)WebRequest.Create(url);
                http.ContentType = String.IsNullOrEmpty(contentType) ? "application/json" : contentType;
                http.Method = "POST";
                http.Accept = "application/json; charset=utf-8";
                if (headers != null)
                {
                    foreach (var itemHeader in headers)
                    {
                        http.Headers.Add(itemHeader.Item1, itemHeader.Item2);
                    }
                }
                using (StreamWriter writer = new StreamWriter(http.GetRequestStream()))
                {

                    writer.Write(data);
                    writer.Flush();
                    writer.Close();

                    HttpWebResponse response = (HttpWebResponse)http.GetResponse();
                    using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                    {
                        string result = reader.ReadToEnd();
                        reader.Close();

                        return result;
                    }
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public static string Get(List<Tuple<string, string>> headers, string url, string accept = null, string contenttype = null)
        {
            try
            {
                WebRequest request = WebRequest.Create(url);
                request.Method = "GET";
                if (headers != null)
                {
                    foreach (var itemHeader in headers)
                    {
                        request.Headers.Add(itemHeader.Item1, itemHeader.Item2);
                    }
                }
                using (WebResponse response = request.GetResponse())
                {
                    using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                    {
                        string result = reader.ReadToEnd();
                        reader.Close();

                        return result;
                    }
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }            
        }
        public static async Task<byte[]> Download(List<Tuple<string, string>> headers, string url, string accept = null)
        {
            try
            {
                HttpClient client = new HttpClient();
                if (headers != null && headers.Count > 0)
                {
                    foreach (var itemHeader in headers)
                    {
                        client.DefaultRequestHeaders.Add(itemHeader.Item1, itemHeader.Item2);
                    }

                }
                using (var response = await client.GetAsync(url))
                {
                    if (response.IsSuccessStatusCode)
                    {
                        var stream = await response.Content.ReadAsStreamAsync();
                        byte[] bytes = new byte[stream.Length];
                        stream.Read(bytes, 0, (int)stream.Length);
                        return bytes;
                    }
                    else
                        return null;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
