﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.IO;

namespace iTrans.Transportation.Application.Helper
{
    public static class FileHelper
    {
        public static void WriteFile(string text,string path,string filename)
        {
            try
            {
                if (!File.Exists(path))
                    Directory.CreateDirectory(path);

                string filepath = Path.Combine(path, filename);
                File.WriteAllText(filepath, text);
            }
            catch
            {

            }
        }
    }
}
