﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace iTrans.Transportation.Application.Constant.Permission
{
    public static class MenuPermission
    {

        public static List<T> GetAllPublicConstantValues<T>(this Type type)
        {
            return type
                .GetFields(BindingFlags.Public | BindingFlags.Static | BindingFlags.FlattenHierarchy)
                .Where(fi => fi.IsLiteral && !fi.IsInitOnly && fi.FieldType == typeof(T))
                .Select(x => (T)x.GetRawConstantValue())
                .ToList();
        }

        public const string dashboardAndReportCreate = "permission.dashboard.create";
        public const string dashboardAndReportEdit = "permission.dashboard.edit";
        public const string dashboardAndReportDelete = "permission.dashboard.delete";
        public const string dashboardAndReportView = "permission.dashboard.view";

        public const string newsCreate = "permission.news.create";
        public const string newsEdit = "permission.news.edit";
        public const string newsDelete = "permission.news.delete";
        public const string newsView = "permission.news.view";


        public const string announcementsCreate = "permission.announcements.create";
        public const string announcementsEdit = "permission.announcements.edit";
        public const string announcementsDelete = "permission.announcements.delete";
        public const string announcementsView = "permission.announcements.view";

        public const string deliveryCreate = "permission.delivery.create";
        public const string deliveryEdit = "permission.delivery.edit";
        public const string deliveryDelete = "permission.delivery.delete";
        public const string deliveryView = "permission.delivery.view";

        public const string customerProfileCreate = "permission.customerprofile.create";
        public const string customerProfileEdit = "permission.customerprofile.edit";
        public const string customerProfileDelete = "permission.customerprofile.delete";
        public const string customerProfileView = "permission.customerprofile.view";

        public const string DriverProfileCreate = "permission.driverprofile.create";
        public const string DriverProfileEdit = "permission.driverprofile.edit";
        public const string DriverProfileDelete = "permission.driverprofile.delete";
        public const string DriverProfileView = "permission.driverprofile.view";

        public const string carProfileCreate = "permission.carprofile.create";
        public const string carProfileEdit = "permission.carprofile.edit";
        public const string carProfileDelete = "permission.carprofile.delete";
        public const string carProfileView = "permission.carprofile.view";

        public const string paymentCreate = "permission.payment.create";
        public const string paymentEdit = "permission.payment.edit";
        public const string paymentDelete = "permission.payment.delete";
        public const string paymentView = "permission.payment.view";

        public const string securityCreate = "permission.security.create";
        public const string securityEdit = "permission.security.edit";
        public const string securityDelete = "permission.security.delete";
        public const string securityView = "permission.security.view";

        public const string helpAndFeedbackCreate = "permission.helpandfeedback.create";
        public const string helpAndFeedbackEdit = "permission.helpandfeedback.edit";
        public const string helpAndFeedbackDelete = "permission.helpandfeedback.delete";
        public const string helpAndFeedbackView = "permission.helpandfeedback.view";

        public const string auditTrialsCreate = "permission.audittrials.create";
        public const string auditTrialsEdit = "permission.audittrials.edit";
        public const string auditTrialsDelete = "permission.audittrials.delete";
        public const string auditTrialsView = "permission.audittrials.view";

        public const string reportCreate = "permission.report.create";
        public const string reportEdit = "permission.report.edit";
        public const string reportDelete = "permission.report.delete";
        public const string reportView = "permission.report.view";

    }
}
