﻿using OneSignal.RestAPIv3.Client;
using OneSignal.RestAPIv3.Client.Resources;
using OneSignal.RestAPIv3.Client.Resources.Notifications;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace iTrans.Transportation.Application
{
    public class NotificationManager
    {
        public static string APPLICATION_ID { get; set; }
        public static string REST_API_KEY { get; set; }
        public static string AUTHENTICATION_ID { get; set; }

        public NotificationManager(string appID, string restID, string authID)
        {
            APPLICATION_ID = appID;
            REST_API_KEY = restID;
            AUTHENTICATION_ID = authID;
        }

        public string SendNotificationAsync(
                             string title,
                             string message,
                             List<string> oneSignalId,
                             string service_code,
                             string refKey,  
                             string payload, 
                             DateTime? sendDate = null

            )
        {
            DateTime now = DateTime.UtcNow; 

            var user = MaxOneUser(oneSignalId);
            var sendId = new List<string>();
            foreach (var o in user)
            {
                var client = new OneSignalClient(REST_API_KEY); 

                Dictionary<string, string> dic = new Dictionary<string, string>(); 
                dic.Add("ServiceCode", service_code); 
                dic.Add("Payload", payload);
                dic.Add("ReferentContentKey", refKey); 


                var options = new NotificationCreateOptions
                {
                    AppId = new Guid(APPLICATION_ID),   
                    IncludePlayerIds = o,
                    Data = dic,

                };

                
                options.Headings.Add(LanguageCodes.English, title);
                options.Contents.Add(LanguageCodes.English, message);

                var result = client.Notifications.Create(options);
                sendId.Add(result.Id);
                System.Threading.Thread.Sleep(300);

            }
            return string.Join(",", sendId);
        }

 

        public static List<List<string>> MaxOneUser(List<string> oId)
        {
            var oneSignalId = oId;
            double lenghtOfId = oneSignalId.Count();
            double maxSender = 500;
            double round = (int)Math.Ceiling((double)(lenghtOfId / maxSender));
            List<List<string>> userArry = new List<List<string>>();
            for (var i = 0; i < (int)round; i++)
            {
                userArry.Add(oneSignalId.Skip(i * (int)maxSender).Take((int)maxSender).ToList());
            }
            return userArry;
        }

        public void CancleNoti(string id)
        {
            var client = new OneSignalClient(REST_API_KEY); // Use your Api Key
            var oId = id.Split(',');
            foreach (var o in oId)
            {
                var noti = new NotificationCancelOptions
                {
                    AppId = APPLICATION_ID,
                    Id = o
                };
                try
                {
                    client.Notifications.Cancel(noti);
                }
                catch (Exception e)
                {

                }
            }

        }
    }
}

