﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Domain.Entities
{
    public class RoleMenu : AuditableBaseEntity
    {
        public virtual int Id { get; set; }
        public virtual int RoleId { get; set; }
        public virtual int MenuId { get; set; }
        public virtual MenuPermission MenuPermission { get; set; }
    }
}
