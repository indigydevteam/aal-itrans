﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Domain
{
    public class CustomerProductFile : AuditableBaseEntity
    {
        public virtual int Id { get; set; }
        public virtual int CustomerProductId { get; set; }
        public virtual string FileName { get; set; }
        public virtual string ContentType { get; set; }
        public virtual string FilePath { get; set; }
        public virtual string FileEXT { get; set; }
    }
}
