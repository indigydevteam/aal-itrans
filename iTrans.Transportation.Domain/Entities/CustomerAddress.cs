﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Domain
{
    public class CustomerAddress : AuditableBaseEntity
    {
        public virtual int Id { get; set; }
        public virtual Guid CustomerId { get; set; }
        public virtual Country Country { get; set; }
        public virtual Province Province { get; set; }
        public virtual District District { get; set; }
        public virtual Subdistrict Subdistrict { get; set; }
        public virtual string PostCode { get; set; }
        public virtual string Road { get; set; }
        public virtual string Alley { get; set; }
        public virtual string Address { get; set; }
        public virtual string Branch { get; set; }
        public virtual string AddressType { get; set; }
        public virtual string AddressName { get; set; }
        public virtual string ContactPerson { get; set; }
        public virtual string ContactPhoneCode { get; set; }
        public virtual string ContactPhoneNumber { get; set; }
        public virtual string ContactEmail { get; set; }
        public virtual string Maps { get; set; }
        public virtual bool IsMainData { get; set; }
        public virtual int Sequence { get; set; }
    }
}