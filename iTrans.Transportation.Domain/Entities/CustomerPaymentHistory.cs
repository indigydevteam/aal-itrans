﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Domain
{
   public class CustomerPaymentHistory : AuditableBaseEntity
    {
        public virtual int Id { get; set; }
        public virtual CustomerPayment Payment { get; set; }
        public virtual string Detail { get; set; }
    }
}
