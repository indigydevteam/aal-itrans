﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Domain
{
    public class DriverProblem : AuditableBaseEntity
    {
        public virtual int Id { get; set; }
        public virtual Driver Driver { get; set; }
        public virtual ProblemTopic ProblemTopic { get; set; }
        public virtual string Message { get; set; }
        public virtual string Email { get; set; }
        public virtual IList<DriverProblemFile> Files { get; set; }
    }
}
