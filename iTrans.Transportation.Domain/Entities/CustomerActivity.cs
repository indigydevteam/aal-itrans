﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Domain
{
    public class CustomerActivity : AuditableBaseEntity
    {
        public virtual int Id { get; set; }
        public virtual Guid CustomerId { get; set; }
        public virtual string Date { get; set; }
        public virtual int Star { get; set; }
        public virtual int RequestCar { get; set; }
        public virtual int Cancel { get; set; }
        public virtual decimal OrderingValue { get; set; }
    }
}