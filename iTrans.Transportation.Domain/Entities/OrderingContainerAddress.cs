﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Domain
{
    public class OrderingContainerAddress : AuditableBaseEntity
    {
        public virtual int Id {get; set;}
        //public virtual int OrderingContainerId { get; set;}
        public virtual OrderingContainer OrderingContainer { get; set; }
        public virtual Country LoadingCountry { get; set; }
        public virtual Province LoadingProvince { get; set; }
        public virtual District LoadingDistrict { get; set; }
        public virtual Subdistrict LoadingSubdistrict { get; set; }
        public virtual string LoadingPostCode { get; set;}
        public virtual string LoadingRoad { get; set;}
        public virtual string LoadingAlley { get; set;}
        public virtual string LoadingAddress { get; set;}
        public virtual string LoadingBranch { get; set;}
        public virtual string LoadingAddressType { get; set;}
        public virtual string LoadingAddressName { get; set;}
        public virtual DateTime ETD { get; set;}
        public virtual string LoadingMaps { get; set;}
        public virtual Country DestinationCountry { get; set; }
        public virtual Province DestinationProvince { get; set; }
        public virtual District DestinationDistrict { get; set; }
        public virtual Subdistrict DestinationSubdistrict { get; set; }
        public virtual string DestinationPostCode { get; set; }
        public virtual string DestinationRoad { get; set; }
        public virtual string DestinationAlley { get; set; }
        public virtual string DestinationAddress { get; set; }
        public virtual string DestinationBranch { get; set; }
        public virtual string DestinationAddressType { get; set; }
        public virtual string DestinationAddressName { get; set; }
        public virtual DateTime ETA { get; set; }
        public virtual string DestinationMaps { get; set; }
        public virtual string FeederVessel { get; set;}
        public virtual DateTime TStime { get; set;}
        public virtual string MotherVessel { get; set;}
        public virtual DateTime CPSDate { get; set;}
        public virtual string CPSDetail { get; set;}
        public virtual DateTime CYDate { get; set;}
        public virtual string CYDetail { get; set; }
        public virtual DateTime ReturnDate { get; set;}
        public virtual string ReturnDetail { get; set;}
        public virtual DateTime ClosingTime { get; set;}
        public virtual DateTime CutOffVGMTime { get; set;}
        public virtual DateTime CutOffSITime { get; set;}
        public virtual string Agent { get; set;}
    }
}
