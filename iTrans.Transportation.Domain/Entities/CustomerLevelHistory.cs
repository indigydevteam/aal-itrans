﻿using System;
using System.Collections.Generic;

namespace iTrans.Transportation.Domain
{
    public class CustomerLevelHistory : AuditableBaseEntity
    {
        public virtual int Id { get; set; }
        public virtual Guid CustomerId { get; set; }
        public virtual int Level { get; set; }
        public virtual int Star { get; set; }
        public virtual int Rating { get; set; }
        public virtual int RequestCar { get; set; }
        public virtual int Cancel { get; set; }
        public virtual decimal OrderingValue { get; set; }
    }
}
