﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Domain
{
    public class OrderingAddress : AuditableBaseEntity
    {
        public virtual int Id { get; set; }
        public virtual Ordering Ordering { get; set; }
        //public virtual OrderingProduct OrderingProduct { get; set; }
        public virtual string TrackingCode { get; set; }
        public virtual string PersonalName { get; set; }
        public virtual string PhoneCode { get; set; }
        public virtual string PhoneNumber { get; set; }
        public virtual string Email { get; set; }
        public virtual Country Country { get; set; }
        public virtual Province Province { get; set; }
        public virtual District District { get; set; }
        public virtual Subdistrict Subdistrict { get; set; }
        public virtual string PostCode { get; set; }
        public virtual string Road { get; set; }
        public virtual string Alley { get; set; }
        public virtual string Address { get; set; }
        public virtual string Branch { get; set; }
        public virtual string AddressType { get; set; }
        public virtual string AddressName { get; set; }
        public virtual DateTime Date { get; set; }
        public virtual string Maps { get; set; }
        public virtual int Sequence { get; set; }
        public virtual string Note { get; set; }
        public virtual int Status { get; set; }
        public virtual string StatusReason { get; set; }
        public virtual decimal Compensation { get; set; }
        public virtual decimal ActualReceive { get; set; }
        public virtual string PeriodTime { get; set; }
        public virtual DateTime DriverWaitingToPickUpDate { get; set; }
        public virtual DateTime DriverWaitingToDeliveryDate { get; set; }
        public virtual string UploadNote { get; set; }
        public virtual int NotiAmount { get; set; }
        public virtual IList<OrderingAddressProduct> AddressProducts { get; set; }
        public virtual IList<OrderingAddressFile> Files { get; set; }
    }
}
