﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Domain
{
    public class DriverLevelCharacteristic : AuditableBaseEntity
    {
        public virtual int Id { get; set; }
        public virtual int Level { get; set; }
        public virtual int Star { get; set; }
        public virtual int AcceptJobPerWeek { get; set; }
        public virtual int AcceptJobPerMonth { get; set; }
        public virtual int AcceptJobPerYear { get; set; }
        public virtual int CancelPerWeek { get; set; }
        public virtual int CancelPerMonth { get; set; }
        public virtual int CancelPerYear { get; set; }
        public virtual int ComplaintPerWeek { get; set; }
        public virtual int ComplaintPerMonth { get; set; }
        public virtual int ComplaintPerYear { get; set; }
        public virtual int RejectPerWeek { get; set; }
        public virtual int RejectPerMonth { get; set; }
        public virtual int RejectPerYear { get; set; }
        public virtual decimal InsuranceValue { get; set; }
        public virtual decimal Commission { get; set; }
        public virtual decimal Discount { get; set; }
        public virtual decimal Fine { get; set; }
        public virtual bool IsDelete { get; set; }
    }
}