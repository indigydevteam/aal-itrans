﻿using System;

namespace iTrans.Transportation.Domain
{
    public class LoginErrorLog  
    {
        public virtual int Id { get; set; }
        public virtual string Username { get; set; }
        public virtual int IncorrectTime { get; set; }
        public virtual DateTime? UnlockDate { get; set; }
        public virtual DateTime LastModified { get; set; }
    }
}
