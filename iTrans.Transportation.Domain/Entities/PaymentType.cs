﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Domain
{
    public class PaymentType : AuditableBaseEntity
    {
        public virtual int Id { get; set; }
        public virtual string Name { get; set; }
        public virtual string Name_EN { get; set; }
        public virtual bool Active { get; set; }
        public virtual decimal Sequence { get; set; }
    }
}
