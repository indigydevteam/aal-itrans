﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Domain
{
    public class Driver : AuditableBaseEntity
    {
        public virtual Guid Id { get; set; }
        public virtual string Password { get; set; }
        public virtual string DriverType { get; set; }
        public virtual CorporateType CorporateType { get; set; }
        public virtual Title Title { get; set; }
        public virtual string FirstName { get; set; }
        public virtual string MiddleName { get; set; }
        public virtual string LastName { get; set; }
        public virtual string Name { get; set; }
        public virtual int IdentityType { get; set; }
        public virtual string IdentityNumber { get; set; }
        public virtual Title ContactPersonTitle { get; set; }
        public virtual string ContactPersonFirstName { get; set; }
        public virtual string ContactPersonMiddleName { get; set; }
        public virtual string ContactPersonLastName { get; set; }
        public virtual DateTime Birthday { get; set; }
        public virtual string PhoneCode { get; set; }
        public virtual string PhoneNumber { get; set; }
        public virtual string Email { get; set; }
        public virtual string Facbook { get; set; }
        public virtual string Line { get; set; }
        public virtual string Twitter { get; set; }
        public virtual string Whatapp { get; set; }
        public virtual string Wechat { get; set; }
        public virtual int Level { get; set; }
        public virtual string Rating { get; set; }
        public virtual string Grade { get; set; }
        public virtual int AcceptJobPerYear { get; set; }
        public virtual int AcceptJobPerMonth { get; set; }
        public virtual int ComplaintPerYear { get; set; }
        public virtual int ComplaintPerMonth { get; set; }
        public virtual int RejectPerYear { get; set; }
        public virtual int RejectPerMonth { get; set; }
        public virtual int CancelPerYear { get; set; }
        public virtual int CancelPerMonth { get; set; }
        public virtual int InsuranceValue { get; set; }
        public virtual Guid Corparate { get; set; }
        public virtual IList<DriverTermAndCondition> TermAndConditions { get; set; }
        public virtual IList<DriverPayment> Payments { get; set; }
        public virtual IList<DriverAddress> Addresses { get; set; }
        public virtual IList<DriverFile> Files { get; set; }
        public virtual IList<DriverCar> Cars { get; set; }
        public virtual IList<DriverAnnouncement> Announcements { get; set; }
        public virtual int Star { get; set; }
        public virtual int VerifyStatus { get; set; }
        public virtual bool IsRegister { get; set; }
        public virtual bool Status { get; set; }
        public virtual bool IsDelete { get; set; }
    }
}