﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;  

namespace iTrans.Transportation.Domain
{
    public class News : AuditableBaseEntity
    {
        public virtual int Id { get; set; }  
        public virtual string Title { get; set; }  
        public virtual string Detail { get; set; } 
        public virtual DateTime? StartDate { get; set; } 
        public virtual DateTime? EndDate { get; set; }   
        public virtual DateTime? HLStartDate { get; set; }
        public virtual DateTime? HLEndDate { get; set; }
        public virtual Guid? ImageFileId { get; set; }
        public virtual bool IsActive { get; set; }
        public virtual bool IsHighlight { get; set; }
        public virtual bool IsNotification { get; set; }   
        public virtual bool IsDelete { get; set; }  
        public virtual string Url { get; set; } 
        public virtual string VideoUrl { get; set; }
        public virtual Guid? FileId { get; set; } 
        public virtual Guid? VideoId { get; set; }
        public virtual int NewsType { get; set; }  
        public virtual int TotalView { get; set; } 
        public virtual bool IsAll { get; set; }
        public virtual bool IsSelection { get; set; }
        public virtual string TargetCustomerType { get; set; } 
        public virtual string TargetDriverType { get; set; } 
        public virtual bool IsGuest { get; set; }

    }
}
