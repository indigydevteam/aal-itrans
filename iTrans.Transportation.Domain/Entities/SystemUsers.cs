﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Domain.Entities
{
   public  class SystemUsers : AuditableBaseEntity
    {
        public virtual Guid? EmplID { get; set; }
        public virtual string Email { get; set; }
        public virtual string Password { get; set; }
        public virtual string UserToken { get; set; }
        public virtual bool IsNaverExpire { get; set; }
        public virtual bool IsExternalUser { get; set; }
        public virtual DateTime EffectiveDate { get; set; }
        public virtual DateTime ExpiredDate { get; set; }
        public virtual bool IsActive { get; set; }
        public virtual DateTime Activated { get; set; }
        public virtual DateTime Reactivated { get; set; }
        public virtual int IncorrectLoginCount { get; set; }
        public virtual string FirstNameTH { get; set; }
        public virtual string FirstNameEN { get; set; }
        public virtual string LastNameTH { get; set; }
        public virtual string LastNameEN { get; set; }
        public virtual string MiddleNameTH { get; set; }
        public virtual string MiddleNameEN { get; set; }
        public virtual string PhoneCode { get; set; }
        public virtual string PhoneNumber { get; set; }
        public virtual string IdentityNumber { get; set; }
        public virtual Title Title { get; set; }
        public virtual DateTime Birthday { get; set; }
        public virtual string CreatedByEN { get; set; }
        public virtual string CreatedByTH { get; set; }
        public virtual string CreatedByID { get; set; } 
        public virtual string ModifiedByEN { get; set; }
        public virtual string ModifiedByTH { get; set; }
        public virtual string ModifiedByID { get; set; }
        public virtual string Imageprofile { get; set; }
        public virtual Role Role { get; set; }







    }
}
