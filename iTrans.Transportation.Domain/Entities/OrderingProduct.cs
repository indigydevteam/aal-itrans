﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Domain
{
   public class OrderingProduct : AuditableBaseEntity
    {
        public virtual int Id { get;set;}
        //public virtual Guid OrderingId { get;set;}
        public virtual int ProductId { get; set; } // use only in program not in database
        public virtual Ordering Ordering { get; set; }
        public virtual int CustomerProductId { get; set; }
        public virtual string Name { get;set;}
        public virtual string ProductType { get;set;}
        //public virtual ProductType ProductType { get; set; }
        public virtual string ProductTypeDetail { get;set;}
        public virtual string Packaging { get;set;}
        //public virtual ProductPackaging Packaging { get; set; }
        public virtual string PackagingDetail { get;set;}
        public virtual int Width { get;set;}
        public virtual int Length { get;set;}
        public virtual int Height { get;set;}
        public virtual decimal Weight { get;set;}
        public virtual int Quantity { get;set;}
        public virtual int Sequence { get; set; }
        public virtual OrderingContainer Container { get; set; }
        public virtual IList<OrderingProductFile> ProductFiles { get; set; }
    }
}
