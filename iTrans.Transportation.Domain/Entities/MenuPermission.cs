﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Domain.Entities
{
    public class MenuPermission : AuditableBaseEntity
    {
        public virtual int Id { get; set; }
        public virtual int MenuId { get; set; }
        public virtual string Permission { get; set; }

    }
}
