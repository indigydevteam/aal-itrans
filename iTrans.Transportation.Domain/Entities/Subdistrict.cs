﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Domain
{
    public class Subdistrict : AuditableBaseEntity
    {
        public virtual int Id { get; set; }
        //public virtual int DistrictId { get; set; }
        public virtual District District { get; set; }
        public virtual string Name_TH { get; set; }
        public virtual string Name_ENG { get; set; }
        public virtual string PostCode { get; set; }
        public virtual bool Active { get; set; }
    }
}
