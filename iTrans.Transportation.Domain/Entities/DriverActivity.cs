﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Domain
{
    public class DriverActivity : AuditableBaseEntity
    {
        public virtual int Id { get; set; }
        public virtual Guid DriverId { get; set; }
        public virtual string Date { get; set; }
        public virtual int Star { get; set; }
        public virtual int AcceptJob { get; set; }
        public virtual int Complaint { get; set; }
        public virtual int Reject { get; set; }
        public virtual decimal InsuranceValue { get; set; }
        public virtual int Cancel { get; set; }
    }
}