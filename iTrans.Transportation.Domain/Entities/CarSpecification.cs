﻿namespace iTrans.Transportation.Domain
{
    public class CarSpecification : AuditableBaseEntity
    {
        public virtual int Id { get; set; }
        //public virtual CarList CarList { get; set; }
        public virtual CarType CarType { get; set; }
        public virtual string Name_TH { get; set; }
        public virtual string Name_ENG { get; set; }
        public virtual int Sequence { get; set; }
        public virtual bool Active { get; set; }
        public virtual bool Specified { get; set; }
        public virtual bool SpecifiedTemperature { get; set; }
        public virtual bool SpecifiedEnergySavingDevice { get; set; }
        public virtual bool IsDelete { get; set; }
        public virtual CarSpecificationFile File { get; set; }

    }
}
