﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Domain
{
    public class DriverAnnouncementLocation : AuditableBaseEntity
    {
        public virtual int Id { get; set; }
        public virtual DriverAnnouncement DriverAnnouncement { get; set; }
        public virtual Country Country { get; set; }
        public virtual Province Province { get; set; }
        public virtual District District { get; set; }
        public virtual Subdistrict Subdistrict { get; set; }
        public virtual string LocationType { get; set; }
        public virtual string Note { get; set; }
        public virtual int Sequence { get; set; }
        public virtual DateTime SendDate { get; set; }
        public virtual DateTime ReceiveDate { get; set; }

    }
}