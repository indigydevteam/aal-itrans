﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Domain
{
    public class DriverAnnouncementCar : AuditableBaseEntity
    {
        public virtual int Id { get; set; }
        public virtual DriverAnnouncement DriverAnnouncement { get; set; }
        public virtual string CarRegistration { get; set; }
        public virtual DateTime ActExpiry{ get; set; }
        public virtual CarType CarType { get; set; }
        public virtual CarList CarList { get; set; }
        public virtual CarDescription CarDescription { get; set; }
        public virtual string CarDescriptionDetail{ get; set; }
        public virtual CarFeature CarFeature { get; set; }
        public virtual string CarFeatureDetail{ get; set; }
        public virtual CarSpecification CarSpecification { get; set; }
        public virtual string CarSpecificationDetail { get; set; }
        public virtual int Width { get; set; }
        public virtual int Length { get; set; }
        public virtual int Height { get; set; }
        public virtual string DriverName { get; set; }
        public virtual string DriverPhoneCode { get; set; }
        public virtual string DriverPhoneNumber{ get; set; }
        public virtual string DriverIdentityNumber { get; set; }
        public virtual bool ProductInsurance { get; set; }
        public virtual decimal ProductInsuranceAmount { get; set; }
        public virtual bool AllLocation{ get; set; }
        public virtual string Note{ get; set; }
        public virtual IList<DriverAnnouncementCarFile> CarFiles { get; set; }
    }
}
