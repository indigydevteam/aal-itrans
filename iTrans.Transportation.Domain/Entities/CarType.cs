﻿using System.Collections.Generic;

namespace iTrans.Transportation.Domain
{
    public class CarType : AuditableBaseEntity
    {
        public virtual int Id { get; set; }
        public virtual string Name_TH { get; set; }
        public virtual string Name_ENG { get; set; }
        public virtual int Sequence { get; set; }
        public virtual bool IsTemperatureInfo { get; set; }
        public virtual bool IsContainer { get; set; }
        public virtual bool Active { get; set; }
        public virtual bool IsDelete { get; set; }
        public virtual CarTypeFile File { get; set; }
    }
}
