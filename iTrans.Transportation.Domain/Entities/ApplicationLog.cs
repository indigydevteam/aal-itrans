﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Domain.Entities
{
   public class ApplicationLog : AuditableBaseEntity
    {
        public virtual int Id { get; set; }
        public virtual string Module { get; set; }
        public virtual string Section { get; set; }
        public virtual string Function { get; set; }
        public virtual string Content { get; set; }
    }
}
