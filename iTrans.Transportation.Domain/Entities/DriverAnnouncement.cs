﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Domain
{
    public class DriverAnnouncement : AuditableBaseEntity

    {
        public virtual int Id { get; set; }
        public virtual Driver Driver { get; set; }
        //public virtual ProductType AcceptProduct { get; set; }
        public virtual string AcceptProduct { get; set; }
        public virtual string AcceptProductDetail { get; set; }

        // public virtual ProductPackaging AcceptPackaging { get; set; }
        public virtual string AcceptPackaging { get; set; }
        public virtual string AcceptPackagingDetail { get; set; }

        //public virtual ProductType RejectProduct { get; set; }
        public virtual string RejectProduct { get; set; }
        public virtual string RejectProductDetail { get; set; }
        //public virtual ProductPackaging RejectPackaging { get; set; }
        public virtual string RejectPackaging { get; set; }
        public virtual string RejectPackagingDetail { get; set; }
        public virtual DateTime SendDate { get; set; }
        public virtual DateTime ReceiveDate { get; set; }
        public virtual string CarRegistration { get; set; }
        public virtual bool AllRent { get; set; }
        public virtual bool Additional { get; set; }
        public virtual string AdditionalDetail { get; set; }
        public virtual string Note { get; set; }
        public virtual int Status { get; set; }
        public virtual string StatusReason { get; set; }
        public virtual decimal DesiredPrice { get; set; }
        //public virtual Guid OrderId { get; set; }
        public virtual Ordering Order { get; set; }
        public virtual Guid CustomerId { get; set; }
        public virtual IList<DriverAnnouncementLocation> Locations { get; set; }

    }
}
