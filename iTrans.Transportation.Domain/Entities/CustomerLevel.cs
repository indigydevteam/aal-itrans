﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Domain
{
    public class CustomerLevel : AuditableBaseEntity
    {
        public virtual Guid CustomerId { get; set; }
        public virtual int Level { get; set; }
        public virtual int RequestCar { get; set; }
        public virtual int Star { get; set; }
        public virtual int CancelAmount { get; set; }
        public virtual decimal OrderValue { get; set; }
    }
}
