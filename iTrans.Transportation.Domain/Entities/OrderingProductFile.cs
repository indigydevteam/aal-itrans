﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Domain
{
    public class OrderingProductFile : AuditableBaseEntity
    {
        public virtual int Id { get; set; }
        //public virtual int OrderingProductId { get; set; }
        public virtual OrderingProduct OrderingProduct { get; set; }
        public virtual string FileName { get; set; }
        public virtual string ContentType { get; set; }
        public virtual string FilePath { get; set; }
        public virtual string DirectoryPath { get; set; }
        public virtual string FileEXT { get; set; }
    }
}
