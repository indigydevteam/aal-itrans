﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Domain
{
   public class CustomerPaymentTransaction : AuditableBaseEntity
    {
        public virtual int Id { get; set; }
        public virtual int Customer_PaymentHistoryId { get; set; }
        public virtual string Detail { get; set; }
    }
}
