﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Domain
{
    public class DriverCarLocation : AuditableBaseEntity
    {
        public virtual int Id { get; set; }
        public virtual DriverCar DriverCar { get; set; }
        public virtual Country Country { get; set; }
        public virtual Region Region { get; set; }
        public virtual Province Province { get; set; }
        public virtual District District { get; set; }
        public virtual string LocationType { get; set; }
        public virtual int Sequence { get; set; }

    }
}
