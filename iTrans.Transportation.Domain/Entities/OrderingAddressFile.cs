﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Domain
{
    public class OrderingAddressFile : AuditableBaseEntity
    {
        public virtual int Id { get; set; }
        public virtual OrderingAddress OrderingAddress { get; set; }
        public virtual int OrderingAddressProductId { get; set; }
        public virtual string FileName { get; set; }
        public virtual string ContentType { get; set; }
        public virtual string FilePath { get; set; }
        public virtual string DirectoryPath { get; set; }
        public virtual string FileEXT { get; set; }
        public virtual int Sequence { get; set; }
}
}
