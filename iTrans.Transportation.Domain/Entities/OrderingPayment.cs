﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Domain
{
    public  class OrderingPayment : AuditableBaseEntity
    {
        public virtual int Id { get; set; }
        public virtual Ordering Ordering { get; set; }
        public virtual string Module { get; set; }
        public virtual Guid UserId { get; set; }
        public virtual string Channel { get; set; }
        public virtual decimal Amount { get; set; }
        public virtual string Description { get; set; }
    }
}
