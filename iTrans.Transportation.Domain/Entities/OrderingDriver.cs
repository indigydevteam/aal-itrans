﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Domain
{
   public class OrderingDriver : AuditableBaseEntity
    {
        public virtual int Id { get; set; }
        public virtual Ordering Ordering { get; set; }
        public virtual Guid DriverId { get; set; }
        public virtual string DriverType { get; set; }
        public virtual CorporateType CorporateType { get; set; }
        public virtual Title Title { get; set; }
        public virtual string FirstName { get; set; }
        public virtual string MiddleName { get; set; }
        public virtual string LastName { get; set; }
        public virtual string Name { get; set; }
        public virtual string IdentityNumber { get; set; }
        public virtual Title ContactPersonTitle { get; set; }
        public virtual string ContactPersonFirstName { get; set; }
        public virtual string ContactPersonMiddleName { get; set; }
        public virtual string ContactPersonLastName { get; set; }
        public virtual string PhoneCode { get; set; }
        public virtual string PhoneNumber { get; set; }
        public virtual string Email { get; set; }
        public virtual string Facbook { get; set; }
        public virtual string Line { get; set; }
        public virtual string Twitter { get; set; }
        public virtual string Whatapp { get; set; }
        public virtual int Level { get; set; }
        public virtual string Rating { get; set; }
        public virtual string Grade { get; set; }
        public virtual Guid Corparate { get; set; }
        public virtual int Star { get; set; }
        public virtual IList<OrderingDriverFile> DriverFiles { get; set; }

    }
}
