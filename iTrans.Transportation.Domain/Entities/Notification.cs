﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace iTrans.Transportation.Domain
{
    public class Notification : AuditableBaseEntity
    {
        [Key]
        public virtual int Id { get; set; }  
        [StringLength(50)]
        public virtual string OneSignalId { get; set; } 
        public virtual Guid? UserId { get; set; } 
        public virtual Guid? OwnerId { get; set; }
        [StringLength(250)]
        public virtual string Title { get; set; }
        [StringLength(1000)]
        public virtual string Detail { get; set; } 
        public virtual bool IsRead { get; set; }
        public virtual bool IsDelete { get; set; } 
        public virtual string Payload { get; set; } 
        public virtual string ServiceCode { get; set; }
        public virtual string ReferenceContentKey { get; set; } 
    }
}
 