﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Domain
{
    public class CustomerLevelCharacteristic : AuditableBaseEntity
    {
        public virtual int Id { get; set; }
        public virtual int Level { get; set; }
        public virtual int Star { get; set; }
        public virtual int RequestCarPerWeek { get; set; }
        public virtual int RequestCarPerMonth { get; set; }
        public virtual int RequestCarPerYear { get; set; }
        public virtual int CancelPerWeek { get; set; }
        public virtual int CancelPerMonth { get; set; }
        public virtual int CancelPerYear { get; set; }
        public virtual decimal OrderingValuePerWeek { get; set; }
        public virtual decimal OrderingValuePerMonth { get; set; }
        public virtual decimal OrderingValuePerYear { get; set; }
        public virtual decimal Discount { get; set; }
        public virtual decimal Fine { get; set; }
        public virtual bool IsDelete { get; set; }
    }
}