﻿using System;
using System.Collections.Generic;

namespace iTrans.Transportation.Domain
{
    public class DriverLevelHistory : AuditableBaseEntity
    {
        public virtual int Id { get; set; }
        public virtual Guid DriverId { get; set; }
        public virtual int Level { get; set; }
        public virtual int Star { get; set; }
        public virtual int Rating { get; set; }
        public virtual int AcceptJob { get; set; }
        public virtual int Complaint { get; set; }
        public virtual int Reject { get; set; }
        public virtual int Cancel { get; set; }
        public virtual decimal InsuranceValue { get; set; }
    }
}
