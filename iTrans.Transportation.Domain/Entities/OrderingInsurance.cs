﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Domain
{
   public class OrderingInsurance : AuditableBaseEntity
    {
        public virtual int Id { get; set; }
        //public virtual int OrderingId { get; set; }
        public virtual Ordering Ordering { get; set; }
        public virtual string ProductName { get; set; }
        public virtual ProductType ProductType { get; set; }
        public virtual string WarrantyPeriod { get; set; }
        public virtual decimal InsurancePremium { get; set; }
        public virtual decimal InsuranceLimit { get; set; }
    }
}
