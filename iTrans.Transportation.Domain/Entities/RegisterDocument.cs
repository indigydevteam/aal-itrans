﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Domain.Entities
{
  public class RegisterDocument : AuditableBaseEntity
    {
        public virtual int Id { get; set; }
        public virtual string Module { get; set; }
        public virtual string Information_TH { get; set; }
        public virtual string Information_ENG { get; set; }
        public virtual int Sequence { get; set; }
        public virtual bool Active { get; set; }
    }
}
