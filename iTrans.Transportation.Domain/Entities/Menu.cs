﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Domain
{
    public class Menu : AuditableBaseEntity
    {
        public virtual int Id { get; set; }
        public virtual string NameTH { get; set; }
        public virtual string NameEN { get; set; }
        public virtual string Icon { get; set; }
        public virtual string Path { get; set; }
    }
}
