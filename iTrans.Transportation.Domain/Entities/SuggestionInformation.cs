﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Domain
{
   public class SuggestionInformation : AuditableBaseEntity
    {
        public virtual int Id { get; set; }
        public virtual string Title { get; set; }
        public virtual string Title_EN { get; set; }
        public virtual string Detail { get; set; }
        public virtual string Detail_EN { get; set; }
        public virtual string Picture { get; set; }
        public virtual string QRCode { get; set; }
    }
}
