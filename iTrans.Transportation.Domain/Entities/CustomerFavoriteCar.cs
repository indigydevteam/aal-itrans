﻿using System;

namespace iTrans.Transportation.Domain
{
    public class CustomerFavoriteCar : AuditableBaseEntity
    {
        public virtual int Id { get; set; }
        public virtual Customer Customer { get; set; }
        public virtual Driver Driver { get; set; }
        public virtual DriverCar Car { get; set; }
        //public virtual Guid CustomerId { get; set; }
        //public virtual Guid DriverId { get; set; }
        //public virtual int DriverCarId { get; set; }
    }
}
