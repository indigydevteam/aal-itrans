﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Domain
{
   public partial class OrderingContainer : AuditableBaseEntity
    {
        public virtual int Id { get; set; }
        //public virtual Guid OrderingId { get; set; }
        public virtual Ordering Ordering { get; set; }
        public virtual string Section { get; set; }
        public virtual string ContainerBooking { get; set; }
        public virtual ContainerType ContainerType { get; set; }
        public virtual ContainerSpecification ContainerSpecification { get; set; }
        public virtual string Temperature { get; set; }
        public virtual string Moisture { get; set; }
        public virtual string Ventilation { get; set; }
        public virtual EnergySavingDevice EnergySavingDevice { get; set; }
        public virtual string SpecialSpecify { get; set; }
        public virtual double Weight { get; set; }
        public virtual string Commodity { get; set; }
        public virtual DateTime DateOfBooking { get; set; }
        public virtual Country PickupPointCountry { get; set; }
        public virtual Province PickupPointProvince { get; set; }
        public virtual District PickupPointDistrict { get; set; }
        public virtual Subdistrict PickupPointSubdistrict { get; set; }
        public virtual string PickupPointPostCode { get; set; }
        public virtual string PickupPointRoad { get; set; }
        public virtual string PickupPointAlley { get; set; }
        public virtual string PickupPointAddress { get; set; }
        public virtual string PickupPointMaps { get; set; }
        public virtual DateTime PickupPointDate { get; set; }
        public virtual Country ReturnPointCountry { get; set; }
        public virtual Province ReturnPointProvince { get; set; }
        public virtual District ReturnPointDistrict { get; set; }
        public virtual Subdistrict ReturnPointSubdistrict { get; set; }
        public virtual string ReturnPointPostCode { get; set; }
        public virtual string ReturnPointRoad { get; set; }
        public virtual string ReturnPointAlley { get; set; }
        public virtual string ReturnPointAddress { get; set; }
        public virtual string ReturnPointMaps { get; set; }
        public virtual DateTime ReturnPointDate { get; set; }
        public virtual DateTime CutOffDate { get; set; }
        public virtual string ShippingContact { get; set; }
        public virtual string ShippingContactPhoneCode { get; set; }
        public virtual string ShippingContactPhoneNumber { get; set; }
        public virtual string YardContact { get; set; }
        public virtual string YardContactPhoneCode { get; set; }
        public virtual string YardContactPhoneNumber { get; set; }
        public virtual string LinerContact { get; set; }
        public virtual string LinerContactPhoneCode { get; set; }
        public virtual string LinerContactPhoneNumber { get; set; }
        public virtual string DeliveryOrderNumber { get; set; }
        public virtual string BookingNumber { get; set; }
        public virtual int ContainerAmount { get; set; }
        public virtual string SpecialOrder { get; set; }
        public virtual IList<OrderingContainerFile> Files { get; set; }

    }
}
