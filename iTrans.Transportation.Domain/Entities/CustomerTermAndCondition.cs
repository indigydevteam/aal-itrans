﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Domain
{
   public class CustomerTermAndCondition : AuditableBaseEntity
    {
        public virtual int Id { get; set; }
        public virtual Customer Customer { get; set; }
        public virtual int TermAndConditionId { get; set; }
        public virtual string Name_TH { get; set; }
        public virtual string Name_ENG { get; set; }
        public virtual string Section { get; set; }
        public virtual string Version { get; set; }
        public virtual int Sequence { get; set; }
        public virtual bool IsAccept { get; set; }
        public virtual bool IsUserAccept { get; set; }
    }
}
