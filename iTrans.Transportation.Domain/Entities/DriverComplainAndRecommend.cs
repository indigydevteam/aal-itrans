﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Domain.Entities
{
  public class DriverComplainAndRecommend : AuditableBaseEntity
    {
        public virtual int Id { get; set; } 
        public virtual Driver Driver { get; set; } 
        public virtual string Title { get; set; } 
        public virtual string Detail { get; set; }
        public virtual IList<DriverComplainAndRecommendFile> Files { get; set; }
    }
}
