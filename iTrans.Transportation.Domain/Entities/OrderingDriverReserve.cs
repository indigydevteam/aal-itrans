﻿using System;
using System.Collections.Generic;
using System.Text;


namespace iTrans.Transportation.Domain
{
    public class OrderingDriverReserve : AuditableBaseEntity
    {
        public virtual int Id { get; set; }
        public virtual Ordering Ordering { get; set; }
        public virtual Driver Driver { get; set; }
        public virtual DriverAnnouncement DriverAnnouncement { get; set; }
        public virtual decimal CustomerDesiredPrice { get; set; }
        public virtual decimal DriverOfferingPrice { get; set; }
        public virtual bool IsOrderingDriverOffer { get; set; }
        public virtual int Status { get; set; }
    }
}
