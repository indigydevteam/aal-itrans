﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Domain
{ 
  public  class DriverPaymentTransaction : AuditableBaseEntity
    {
        public virtual int Id { get; set; }
        public virtual int Driver_PaymentHistoryId { get; set; }
        public virtual string Detail { get; set; }
    }
}
