﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Domain
{
    public class Province : AuditableBaseEntity
    {
        public virtual int Id { get; set; }
        //public virtual int CountryId { get; set; }
        //public virtual int RegionId { get; set; }
        public virtual Country Country { get; set; }
        public virtual Region Region { get; set; }
        public virtual string Name_TH { get; set; }
        public virtual string Name_ENG { get; set; }
        public virtual bool Active { get; set; }
    }
}
