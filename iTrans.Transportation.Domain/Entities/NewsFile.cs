﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace iTrans.Transportation.Domain
{
    public class NewsFile : AuditableBaseEntity
    {
        public virtual Guid Id { get; set; }  
        public virtual string FileName { get; set; } 
        public virtual string ContentType { get; set; } 
        public virtual string FilePath { get; set; } 
        public virtual string FileEXT { get; set; } 
        public virtual string DocumentType { get; set; }
        public virtual int Sequence { get; set; }
    }
}
