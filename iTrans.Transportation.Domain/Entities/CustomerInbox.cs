﻿using System;

namespace iTrans.Transportation.Domain
{
    public class CustomerInbox : AuditableBaseEntity
    {
        public virtual int Id { get; set; }
        public virtual Guid CustomerId { get; set; }
        public virtual string Title { get; set; }
        public virtual string Title_ENG { get; set; }
        public virtual string Module { get; set; }
        public virtual string Module_ENG { get; set; }
        public virtual string Content { get; set; }
        public virtual string FromUser { get; set; }
        public virtual bool IsDelete { get; set; }
    }
}
