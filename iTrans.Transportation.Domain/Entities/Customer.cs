﻿using iTrans.Transportation.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Domain
{
    public class Customer : AuditableBaseEntity
    {
        public virtual Guid Id { get; set; }
        public virtual string Password { get; set; }
        public virtual string CustomerType { get; set; }
        public virtual CorporateType CorporateType { get; set; }
        public virtual Title Title { get; set; }
        public virtual string FirstName { get; set; }
        public virtual string MiddleName { get; set; }
        public virtual string LastName { get; set; }
        public virtual string Name { get; set; }
        public virtual int IdentityType { get; set; }
        public virtual string IdentityNumber { get; set; }
        public virtual Title ContactPersonTitle { get; set; }
        public virtual string ContactPersonFirstName { get; set; }
        public virtual string ContactPersonMiddleName { get; set; }
        public virtual string ContactPersonLastName { get; set; }
        public virtual DateTime Birthday { get; set; }
        public virtual string PhoneCode { get; set; }
        public virtual string PhoneNumber { get; set; }
        public virtual string Email { get; set; }
        public virtual string Facbook { get; set; }
        public virtual string Line { get; set; }
        public virtual string Twitter { get; set; }
        public virtual string Whatapp { get; set; }
        public virtual string Wechat { get; set; }
        public virtual int Level { get; set; }
        public virtual string Rating { get; set; }
        public virtual string Grade { get; set; }
        public virtual int RequestCarPerYear { get; set; }
        public virtual int RequestCarPerMonth { get; set; }
        public virtual int CancelAmountPerYear { get; set; }
        public virtual int CancelAmountPerMonth { get; set; }
        public virtual decimal OrderValuePerYear { get; set; }
        public virtual decimal OrderValuePerMonth { get; set; }
        public virtual IList<CustomerTermAndCondition> TermAndConditions { get; set; }
        public virtual IList<CustomerFavoriteCar> FavoriteCar { get; set; }
        public virtual IList<CustomerPayment> Payments { get; set; }
        public virtual IList<CustomerAddress> Addresses { get; set; }
        public virtual IList<CustomerFile> Files { get; set; }
        public virtual int Star { get; set; }
        public virtual int VerifyStatus { get; set; }
        public virtual bool IsRegister { get; set; }
        public virtual bool Status { get; set; }
        //specify customer is deleted by himseft
        public virtual bool IsDelete { get; set; }
    }
}