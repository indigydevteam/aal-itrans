﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Domain
{
    public class OrderingTrackingCode : AuditableBaseEntity
    {
        public virtual int Id { get; set; }
        public virtual string TrackingCode { get; set; }
        public virtual string Section { get; set; }
    }
}
