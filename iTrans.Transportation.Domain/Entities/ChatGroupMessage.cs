﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Domain
{
    public class ChatGroupMessage
    {
        public virtual int Id { get; set; }
        public virtual string Content { get; set; }
        public virtual string Timestamp { get; set; }
        public virtual Guid FromUserId { get; set; }
        public virtual int ToRoomId { get; set; }
        public virtual string ContentType { get; set; }
        public virtual bool IsRead { get; set; }
        public virtual bool IsDelete { get; set; }
    }
}
