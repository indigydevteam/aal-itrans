﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Domain
{
    public class District : AuditableBaseEntity
    {
        public virtual int Id { get; set; }
        //public virtual int ProvinceId { get; set; }
        public virtual Province Province { get; set; }
        public virtual string Name_TH { get; set; }
        public virtual string Name_ENG { get; set; }
        public virtual bool Active { get; set; }
    }
}
