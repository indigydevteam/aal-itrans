﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Domain
{
    public class UserLevel : AuditableBaseEntity
    {
        public virtual int Id { get; set; }
        public virtual string Module { get; set; }
        public virtual int Level { get; set; }
        public virtual string Name_TH { get; set; }
        public virtual string Name_ENG { get; set; }
        public virtual int Star { get; set; }
        public virtual bool Active { get; set; }
        public virtual bool IsDelete { get; set; }
        public virtual bool IsCalculateLevel { get; set; }
        public virtual IList<UserLevelCondition> Conditions { get; set; }
    }

}
