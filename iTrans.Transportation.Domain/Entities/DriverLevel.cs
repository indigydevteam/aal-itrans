﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Domain
{
    public class DriverLevel : AuditableBaseEntity
    {
        public virtual Guid DriverId { get; set; }
        public virtual int Level { get; set; }
        public virtual int Star { get; set; }
        public virtual int AcceptJob { get; set; }
        public virtual int ComplaintPerMonth { get; set; }
        public virtual int RejectPerMonth { get; set; }
        public virtual int CancelPerMonth { get; set; }
        public virtual int ComplaintPerYear { get; set; }
        public virtual int RejectPerYear { get; set; }
        public virtual int CancelPerYear { get; set; }
        public virtual decimal InsuranceValue { get; set; }
    }
}
