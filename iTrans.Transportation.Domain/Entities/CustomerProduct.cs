﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Domain
{
    public class CustomerProduct : AuditableBaseEntity
    {
        public virtual int Id { get; set; }
        public virtual Guid CustomerId { get; set; }
        public virtual string Name { get; set; }
        //public virtual int ProductTypeId { get; set; }
        public virtual ProductType ProductType { get; set; }
        public virtual string ProductTypeDetail { get; set; }
        //public virtual int PackagingId { get; set; }
        public virtual ProductPackaging Packaging { get; set; }
        public virtual string PackagingDetail { get; set; }
        public virtual int Width { get; set; }
        public virtual int Length { get; set; }
        public virtual int Height { get; set; }
        public virtual decimal Weight { get; set; }
        public virtual int Quantity { get; set; }
        public virtual IList<CustomerProductFile> ProductFiles { get; set; }
    }
}
