﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Domain.Entities
{
    public class Role : AuditableBaseEntity
    {
        public virtual int Id { get; set; }
        public virtual string NameTH { get; set; }
        public virtual string NameEN { get; set; }
    }
}
