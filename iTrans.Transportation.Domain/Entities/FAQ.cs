﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Domain
{
    public class FAQ : AuditableBaseEntity
    {
        public virtual int Id { get; set; }
        public virtual string Title_TH { get; set; }
        public virtual string Detail_TH { get; set; }
        public virtual string Title_ENG { get; set; }
        public virtual string Detail_ENG { get; set; }
        public virtual string Module { get; set; }
        public virtual int Sequence { get; set; }
        public virtual bool Active { get; set; }
    }
}
