﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Domain
{
   public class DriverPayment : AuditableBaseEntity
    {
        public virtual int Id { get; set; }
        public virtual Driver Driver { get; set; }
        public virtual PaymentType Type { get; set; }
        public virtual string Bank { get; set; }
        public virtual string AccountType { get; set; }
        public virtual string Value { get; set; }
        public virtual decimal Amount { get; set; }
        public virtual string Description { get; set; }
        public virtual IList<DriverPaymentHistory> Histories { get; set; }

    }
}