﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace iTrans.Transportation.Domain
{
    public class NotificationUser : AuditableBaseEntity
    {
        public virtual int Id { get; set; } 
        public virtual string OneSignalId { get; set; }
        public virtual Guid UserId { get; set; }
        public virtual string Device { get; set; }
        public virtual bool IsActive { get; set; }
    }
}
