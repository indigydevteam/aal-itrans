﻿namespace iTrans.Transportation.Domain
{
    public class CarList : AuditableBaseEntity
    {
        public virtual int Id { get; set; }
        // public virtual int CarTypeId { get; set; }
        public virtual CarType CarType { get; set; }
        public virtual string Name_TH { get; set; }
        public virtual string Name_ENG { get; set; }
        public virtual int Sequence { get; set; }
        public virtual bool Active { get; set; }
        public virtual CarListFile File { get; set; }
    }
}
