﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Domain
{
    public class ProblemTopic : AuditableBaseEntity
    {
        public virtual int Id { get; set; }
        public virtual string Module { get; set; }
        public virtual string Value_TH { get; set; }
        public virtual string Value_ENG { get; set; }
        public virtual bool Active { get; set; }
        public virtual int Sequence { get; set; }
    }
}
