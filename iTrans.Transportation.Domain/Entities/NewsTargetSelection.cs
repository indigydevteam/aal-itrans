﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Domain
{
    public class NewsTargetSelection : AuditableBaseEntity
    {
        public virtual int Id { get; set; }
        public virtual int NewsId { get; set; }
        public virtual Guid UserId { get; set; }
    }
}
