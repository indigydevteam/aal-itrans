﻿using System;

namespace iTrans.Transportation.Domain
{
    public class DummyWallet : AuditableBaseEntity
    {
        public virtual int Id { get; set; }
        public virtual Guid UserId { get; set; }
        public virtual string Module { get; set; }
        public virtual decimal Amount { get; set; }
    }
}
