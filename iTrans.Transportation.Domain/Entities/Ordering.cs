﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Domain
{
  public  class Ordering : AuditableBaseEntity
    {
        public virtual Guid Id { get; set; }
        public virtual string TrackingCode { get; set; }
        public virtual bool IsRequestTax { get; set; }
        public virtual int TransportType { get; set; }
        public virtual Driver Driver { get; set; }
        public virtual Customer Customer { get; set; }
        public virtual float ProductTotalWeight { get; set; }
        public virtual int ProductCBM { get; set; }
        public virtual decimal OrderingPrice { get; set; }
        public virtual decimal OrderingDesiredPrice { get; set; }
        public virtual decimal OrderingDriverOffering { get; set; }
        public virtual bool IsOrderingDriverOffer { get; set; }
        public virtual string AdditionalDetail  { get; set; }
        public virtual string Note { get; set; }
        public virtual int Status { get; set; }
        public virtual OrderingCancelStatus CancelStatus { get; set; }
        public virtual int CustomerRanking { get; set; }
        public virtual int DriverRanking { get; set; }
        public virtual DateTime PinnedDate { get; set; }
        public virtual string TotalDistance { get; set; }
        public virtual string TotalEstimateTime { get; set; }
        public virtual string Distance { get; set; }
        public virtual string EstimateTime { get; set; }
        public virtual bool IsDriverPay { get; set; }
        public virtual string CurrentLocation { get; set; }
        public virtual decimal GPValue { get; set; }
        public virtual decimal DriverPayValue { get; set; }
        public virtual decimal CustomerCancelValue { get; set; }
        public virtual decimal DriverCancelValue { get; set; }
        public virtual decimal CustomerCashBack { get; set; }
        public virtual decimal DriverCashBack { get; set; }
        public virtual bool IsMutipleRoutes { get; set; }
        public virtual IList<OrderingDriverReserve> DriverReserve { get; set; }
        public virtual IList<OrderingProduct> Products { get; set; }
        public virtual IList<OrderingCar> Cars { get; set; }
        public virtual IList<OrderingDriver> Drivers { get; set; }
        public virtual IList<OrderingAddress> Addresses { get; set; }
        public virtual IList<OrderingContainer> Containers { get; set; }
        public virtual IList<OrderingInsurance>  Insurances { get; set; }
        public virtual IList<OrderingPayment> Payments { get; set; }
        public virtual IList<DriverAnnouncement> DriverAnnouncements { get; set; }
        public virtual string CarRegistration_Str { get; set; }
        public virtual string DriverName_Str { get; set; }
        public virtual string StatusName_Str { get; set; }
        public virtual string CustomerName_Str { get; set; }
        public virtual string PickupPoint_Str { get; set; }
        public virtual DateTime PickupPointDate_Str { get; set; }
        public virtual string RecipientName_Str { get; set; }
        public virtual string DeliveryPoint_Str { get; set; }
        public virtual DateTime DeliveryPointDate_Str { get; set; }
    }
}
