﻿using iTrans.Transportation.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Domain
{
   public class CustomerProblem : AuditableBaseEntity
    {
        public virtual int Id { get; set; }
        public virtual Customer Customer { get; set; }
        public virtual ProblemTopic ProblemTopic { get; set; }
        public virtual string Message { get; set; }
        public virtual string Email { get; set; }
        public virtual IList<CustomerProblemFile> Files { get; set; }

    }
}
