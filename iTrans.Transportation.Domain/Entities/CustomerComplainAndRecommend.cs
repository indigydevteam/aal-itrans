﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iTrans.Transportation.Domain.Entities
{
   public  class CustomerComplainAndRecommend : AuditableBaseEntity
    {
        public virtual int Id { get; set; }
        public virtual Customer Customer { get; set; }
        public virtual string Title { get; set; }
        public virtual string Detail { get; set; }
        public virtual IList<CustomerComplainAndRecommendFile> Files { get; set; }
    }
}
