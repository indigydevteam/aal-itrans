﻿using System;

namespace iTrans.Transportation.Domain
{
    public abstract class AuditableBaseEntity
    {
        public virtual string CreatedBy { get; set; }
        public virtual DateTime Created { get; set; }
        public virtual string ModifiedBy { get; set; }
        public virtual DateTime Modified { get; set; }
    }
}
